const withPWA = require("next-pwa");
const withImages = require("next-images");
const { i18n } = require("./next-i18next.config");
const path = require('path');

module.exports = {
  ...withImages(
  withPWA({
    swcMinify: true,
    i18n,
    sassOptions: {
      includePaths: [path.join(__dirname, 'src/styles')],
    },
    serverRuntimeConfig: {
      defaultNewUserPassword: process.env.DEFAULT_USER_PASSWORD,
      hotjarProd: process.env.HOTJAR_PROD,
      appEnv: process.env.APP_ENV,
      NCTalkVersion: process.env.NEXTCLOUD_TALK_VERSION,
      adminInfo: {
        username: process.env.USERNAME_ADMIN_NC,
        password: process.env.PASSWORD_ADMIN_NC
      },
      api: {
        baseUrl: process.env.API_BASE_URL
      },
      blog: {
        baseUrl: process.env.BLOG_URL,
      },
      keycloak: {
        baseUrl: process.env.KEYCLOAK_BASE_URL,
        clientId: process.env.KEYCLOAK_CLIENT_ID,
        clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
        serviceClientId: process.env.KEYCLOAK_SERVICE_CLIENT_ID,
        serviceClientSecret: process.env.KEYCLOAK_SERVICE_CLIENT_SECRET,
        realm: process.env.KEYCLOAK_REALM,
        colmenaPwaId: process.env.KEYCLOAK_COLMENA_PWA_ID,
        collaboratorId: process.env.KEYCLOAK_COLLABORATOR_ROLE_ID,
        administratorId: process.env.KEYCLOAK_ADMINISTRATOR_ROLE_ID
      },
      meet: {
        baseUrl: process.env.MEET_BASE_URL,
      },
    },
    publicRuntimeConfig: {
      api: {
        baseUrl: process.env.API_BASE_URL
      },
      wss: {
        baseUrl: process.env.WSS_BASE_URL
      },
      NCTalkVersion: process.env.NEXTCLOUD_TALK_VERSION,
      blog: {
        baseUrl: process.env.BLOG_URL,
      },
      meet: {
        baseUrl: process.env.MEET_BASE_URL,
      },
      publicationsGraphQL: {
        baseUrl: process.env.NEXT_PUBLIC_STRAPI_URL,
        accessToken: process.env.NEXT_PUBLIC_STRAPI_API_KEY
      }
    },
    pwa: {
      dest: "public",
      disable: process.env.NODE_ENV === "development",
      cacheOnFrontEndNav: true,
      reloadOnOnline: false,
      fallbacks: {
        document: "/fallback",
      },
    },
    async redirects() {
      return [
        {
          source: '/reset',
          destination: '/',
          permanent: true,
        },
      ]
    },
  }),
),
 images: {
   remotePatterns: [
     {
       protocol: 'https',
       hostname: '**.colmena.cc'
     },
     {
       protocol: 'https',
       hostname: '**.colmena.network'
     },
     {
       protocol: 'https',
       hostname: '**.colmena.red'
     },
     {
       protocol: 'https',
       hostname: '**.colmena.media'
     }
   ],
   domains: ['localhost']
  }
};
