
module.exports = {
  i18n: {
    defaultLocale: "en",
    locales: ["en", "es", "fr", "ar", "pt_BR", "sw", "uk", "ru"],
  },
};
