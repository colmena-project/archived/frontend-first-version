## Problem Statement
<!-- What is the issue being faced and needs addressing? !-->

## User Story 
<!-- As a Colmena tester I'd like to has a sumary of incoming changes, so that other contributors can easily understand !-->

## Proposed solution
<!-- How would you like to see this issue resolved? !-->

## Wireframe Designs

## Dependencies
