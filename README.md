<div align="center">
<img src="https://git.colmena.network/maia/frontend/uploads/0ba84f96bffdc3dd7543e2b6c95dc11a/image.png" alt="Colmena" width="200">
</div>
<br>
<div align="center"><p>Colmena is a software solution to create and share content, developed together with local and community media from the Global South. The digital toolbox enables communicators to extend local offline collaborations like interviews, live broadcasting, audio and text edition and into participatory online workflows.</p></div>
<div align="center"><i>Colmena is a free to use, commonly owned and 100% open source</i></div>
<br>
<div align="center">
  <span>
    <img src="https://img.shields.io/static/v1?label=version&message=0.17.0&color=333866&style=flat" alt="Colmena version"/>
  </span>
  <a href="https://git.colmena.network/maia/frontend/-/blob/develop/LICENSE" target="blank">
    <img src="https://img.shields.io/static/v1?label=license&message=MIT&color=f5b349&style=flat" alt="Colmena license"/>
  </span>
   <span>
    <img src="https://img.shields.io/static/v1?label=language&message=typescript&color=F65B3A&style=flat" alt="Colmena version"/>
  </span>
  <span>
    <img src="https://img.shields.io/static/v1?label=status&message=WIP&color=00aeaf&style=flat" alt="Colmena version"/>
  </span>
</div>

---

<!--ts-->

- [About](#about)
- [Running the project](#running-the-project)
- [Architecture Notes](#architecture-notes)
  - [Technologies used](#technologies-used)
  - [UI/UX](#UI/UX)
  - [RTL](#RTL)
- [Building and deployment](#building-and-deployment)
- [Documentation](#documentation)
- [Roadmap](#roadmap)
- [List of contributors](#list-of-contributors)
<!--te-->

## About

Colmena bundles all essential tools for resilient local and community media productions in just one application. With our free an open source software you can: record, edit, do remote interviews, live-stream, meet and plan with your team, share files and publish from almost any digital device.

Colmena allows empowers journalists around the world with all the necessary tools they need at their fingertips. The project is open-source, which means anyone is welcome to make contributions to improve the codebase.

## Running the project

Before you begin, you will need to have the following tools installed on your machine: [Git](https://git-scm.com), [Node.js](https://nodejs.org/en/).
Besides this it is good to have an editor to work with the code like [VSCode](https://code.visualstudio.com/)

```bash
# Clone this repository
$ git clone <https://git.colmena.network/maia/frontend.git>

# Go to the project folder in
$ cd frontend
```

Copy the `.env.sample` in a new file named `.env` and set the environment variables correctly.

```bash
# Install the dependencies. We use yarn as our default javsacript package manager, but using npm would not be a problem.
$ yarn

# Run yarn dev to start the development server
$ yarn dev

# Visit http://localhost:3008 to view your application
```

To run the unit tests, just type the command:

```bash
yarn test
```

## Architecture Notes

This repository represents the frontend side of the Colmena project. Which is built with nextjs framework (A framework for reactjs with many features).

Note: As a contributor please ensure to always:

- Encapsulate your code and make it self-explanatory.
- Follow the best practices of the tool or framework we use.

### Technologies used

The number of libraries used at Colmena is extensive. This list below only represents the main libraries and frameworks used in the project:

- NextJs
- Material-UI
- tailwindCSS
- jest
- waveform-playlist
- graphql
- next-auth
- next-pwa
- redux
- formik
- tiptap

### UI/UX

Colmena is a user centered design progressive web app (PWA) solution for mobile/desktop environments that work both online and offline.

Summary on the installed tech;

- Material UI (With Extra RTL plugins)
- TailwindCSS (With Extra RTL plugins)

For user interfaces, we primarily use the official MaterialUI (MUI) framework. Please refer to [the official docs and review all the available components and UIs](https://v4.mui.com/getting-started/installation/)

When we try to create a user interface (UI) which does not have an equivalent in MUI, we then build it from scratch with framework [TailwindCSS.](https://tailwindcss.com/)

Note: TailwindCSS and MUI are already installed and configured and you can start using them directly.

### RTL

Colmena has an international audience and thus we support `Right-To-left (RTL)` languages such as Arabic, Hebrew, and Persian. So when making contributions, please be aware of that.

Please learn more about [RTL integration with MUI here](https://mui.com/guides/localization/#rtl-support)

TailwindCSS does also offer RTL features with the help of [tailwindcss-rtl plugin](https://www.npmjs.com/package/tailwindcss-rtl) which we installed already.

Both integrations rely on a special attribute called `dir="rtl"` or `dir="ltr"` in one of the parent divs. If the direction is `rtl` then MUI-RTL and TailwindCSS-RTL will render the UI from right to left instead of left to right.
The `dir="{direction}"` attribute is already placed in the `Container` component and a few other components, which means that you as a developer, in most situations assume that it is there and you build your inner UI accordingly. (You can see it if you use the chrome tool and inspect elements).

Most MUI components are good to go with RTL, but for tailwindCSS, please rely on this table to substitute classes to make your UI RTL-aware.

| TailwindCss Class | What you should use instead | Comment                                               |
| ----------------- | --------------------------- | ----------------------------------------------------- |
| pl-[X]            | ps-[X]                      | Left padding is now left on `ltr` and right on `rtl`  |
| pr-[X]            | pe-[X]                      | Right padding is now right on `ltr` and left on `rtl` |
| ml-[X]            | ms-[X]                      | Left Margin is now left on `ltr` and right on `rtl`   |
| mr-[X]            | me-[X]                      | Right Margin is now right on `ltr` and left on `rtl`  |
| ...               | ...                         | ...                                                   |

Please review the reset of the changes at [the offical tailwindcss-rtl plugin docs](https://www.npmjs.com/package/tailwindcss-rtl)

If you are curious to know how we did it, please check [the official docs](https://www.npmjs.com/package/tailwindcss-rtl)

## Building and deployment

At Colmena, we use gitlab as the main DEVOPS platform, where we build, test, and deploy each version independently and test it before merges occur.

The building process happens with the help of docker, where we use the main `Dockerfile` to containerize your codebase. (We use node:16-alpine image and as a result, we recommend that you also use node-16 on your local machine as well to ensure version compatibility.)
Your application will be tested (Work in progress) and then pushed to docker with a unique id (we call it `tag` in docker and it is the same as the git `commit_ref_id`)

## Documentation

People can get help with your project consulting [gitlab documentation](https://git.colmena.network/maia/frontend/)

Colmena initiative was born as a response to the Covid-19 pandemic. In January 2021 DW Akademie and the Mexican NGO Redes por la Diversidad, Equidad y Sustentabilidad A.C. teamed up with a group of brazilian developers to prepare the ground for the first phase of the project: a one year co-creation sprint, together with media partners from Africa, the Middle East and Latin America. The work was supported by the German Federal Ministry of Economic development and Cooperation (BMZ) as part of the Global Crisis Initiative (GKI). With the launch of the first version in April 2022.

## Roadmap

- Implement call recording in the application's backend
- Adapt CSS for the desktop environment
- Implement integration with the Facebook API
- Implement collaborative text editor editing
- Implement custom notifications
- Implement dark mode
- Export audio in MP3 and AAC

## List of contributors

(alphabetical order)

Adriana Veloso Meireles

Bruno Melo

Carlos Henrique Gontijo Paulino

Gabriel Antunes

Jean Habib

Nidhal Abidi

Nilson Rocha

Patrick Oliveira

Rafael Diniz

Vinicius Gusmão

Wafi Ben Jeddou
