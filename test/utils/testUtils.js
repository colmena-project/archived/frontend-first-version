import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { setupStore } from "../setupStore";
import { ThemeProvider } from "@material-ui/core";
import theme from "@/styles/theme";
import mocks from "@/tests/__mocks__/server/graphql/cms";
import { MockedProvider } from "@apollo/client/testing";
import ConnectionStatusProvider from "@/providers/ConnectionStatusProvider";

export function renderWithProviders(
  ui,
  {
    preloadedState = {},
    isConnected = true,
    // Automatically create a store instance if no store was passed in
    customMock = [],
    store = setupStore(preloadedState),
    ...renderOptions
  } = {},
) {
  function Wrapper({ children }) {
    return (
      <ConnectionStatusProvider initialStatus={isConnected}>
        <Provider store={store}>
          <ThemeProvider theme={theme}>
            <MockedProvider mocks={customMock.length ? customMock : mocks} addTypename={false}>
              {children}
            </MockedProvider>
          </ThemeProvider>
        </Provider>
      </ConnectionStatusProvider>
    );
  }

  return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
