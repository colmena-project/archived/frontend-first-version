import { NewPublicationFile } from "@/interfaces/index";

export const publicationMock = {
  title: "test",
  description: "test",
  content: "<p>test</p>",
  category: 9,
  rights: "BY-SA-NC",
  date: "2023-02-13T10:18:07.139Z",
  publisher: "devteam",
  userId: "gabrielantunescontato",
  status: "CREATING",
  createdAt: "2023-02-15T12:16:32.906Z",
  categoryName: "Human rights",
  mode: "DRAFT",
  language: "en",
  id: 12,
  files: [],
};

export const fileMock = {
  publicationId: 12,
  title: "1125858",
  description: "",
  creator: "gabrielantunescontato",
  format: "image/jpeg",
  language: "en",
  extentSize: 7082869,
  tags: "",
  status: "PENDING",
  localId: "cdff4a97-71c9-4ca8-a70d-3a410f0b7585",
  isFromLibrary: false,
  position: 1,
  media: new File(["(⌐□_□)"], "chucknorris.png", { type: "image/png" }),
} as NewPublicationFile;
