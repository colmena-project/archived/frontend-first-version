const wizard = {
  wizard: {
    honeycombList: true,
    honeycombChat: true,
    editAudio: true,
  },
};

export default wizard;
