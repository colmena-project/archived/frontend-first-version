const user = {
  user: {
    id: "patrick",
    name: "Patrick Oliveira",
    email: "patrickocvix@gmail.com",
    language: "en",
    country: "",
    region: "",
    role: "administrator",
    position: "",
    phone: "",
    media: {
      id: "12022ad2-9911-4061-8957-800b1e9b867e",
      name: "devteam",
      slogan: "bringing the hive to the bees",
      email: "",
      url: "http://testando.io",
      language: "",
      audio_description_url: "",
      social_media_whatsapp: "",
      social_media_twitter: "https://twitter.com/search?q=%23link",
      social_media_facebook: "http://facebook.com",
      social_media_mastodon: "http://asdasd.com",
      social_media_instagram: "",
      social_media_telegram: "",
    },
    nexcloudToken: "wgMYAW7joTTtVuWy7FVuQ3snTF37ZPhvpSdIV9XiqDkykn3jay18jlh0BAntNPVXpxTSMqXe",
    keycloakToken:
      "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI1ODk2QzFKZDFUV3lXTGN6aEs3MmNLWXB2dnBpOTZtbjBXVzhSOVVmcTl3In0.eyJleHAiOjE2NjI0OTQ0NzAsImlhdCI6MTY2MjQ1ODQ3MCwianRpIjoiNTcxZDIwYjUtZDI2OC00Y2I1LWI3ODItMWQxMDg2MmUwMTMyIiwiaXNzIjoiaHR0cHM6Ly9hdXRoLmNvbG1lbmEubmV0d29yay9hdXRoL3JlYWxtcy9jb2xtZW5hIiwic3ViIjoiMzkxMGYxZGMtNWRjNC00OGJhLTg5YWQtMzE0NGY4MDUyYThkIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiY29sbWVuYV9wd2EiLCJzZXNzaW9uX3N0YXRlIjoiZWJhOGMyMzUtMmU2Ni00ZDBjLWIxNmEtMTAyMjU3NDRmNDI1IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyJodHRwczovL2NvbG1lbmEubWVkaWEiXSwicmVzb3VyY2VfYWNjZXNzIjp7ImNvbG1lbmFfcHdhIjp7InJvbGVzIjpbImFkbWluaXN0cmF0b3IiXX19LCJzY29wZSI6ImNvdW50cnkgbGFuZ3VhZ2UgcGhvdG8gcmVnaW9uIHBvc2l0aW9uIGVtYWlsIHByb2ZpbGUiLCJzaWQiOiJlYmE4YzIzNS0yZTY2LTRkMGMtYjE2YS0xMDIyNTc0NGY0MjUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImFkZHJlc3MiOnsicmVnaW9uIjoiIiwiY291bnRyeSI6IiJ9LCJwaG9uZSI6IiIsIm5hbWUiOiJQYXRyaWNrIE9saXZlaXJhIiwiZ3JvdXBzIjpbIi9kZXZ0ZWFtIl0sInBob3RvIjoiIiwibGFuZ3VhZ2UiOiIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJwYXRyaWNrIiwicG9zaXRpb24iOiIiLCJnaXZlbl9uYW1lIjoiUGF0cmljayIsImZhbWlseV9uYW1lIjoiT2xpdmVpcmEiLCJlbWFpbCI6InBhdHJpY2tvY3ZpeEBnbWFpbC5jb20ifQ.a-S-SLiWknLK6LZ8cGXe3ogRG7_e_gcBPr4J9ari8BwUDT-2C14oavhcZgUBagXMkW8yl2rwf0JvovHPnBe5lSa7EVdMYYZv6v-MeVrsZ9Neh3mTJ-5LwGw3r32TCMgVejZVO4JDQBk2yhEKjnQvkOV9zSPAncQOhb8gP3CuLV45H2zJkGp__MEjDQ7hAMBL9elxNi4_fxWovoa8SN_G90T1S01MPAUOA3MvkZThVYEBlAFzUSGnQbpxwlqMjYyopcg07ozC6yW1Joeq52DYZ9hjFMGEIFXQRFoQJ2L0yQCXzEMuLgYhO-l2vt5udU37GMjuyEvcwd2OpLJxVZcDtw",
  },
};

export const userColaborator = {
  user: {
    id: "patrick",
    name: "Patrick Oliveira",
    email: "patrickocvix@gmail.com",
    language: "en",
    country: "",
    region: "",
    role: "collaborator",
    position: "",
    phone: "",
    media: {
      id: "12022ad2-9911-4061-8957-800b1e9b867e",
      name: "devteam",
      slogan: "bringing the hive to the bees",
      email: "",
      url: "http://testando.io",
      language: "",
      audio_description_url: "",
      social_media_whatsapp: "",
      social_media_twitter: "https://twitter.com/search?q=%23link",
      social_media_facebook: "http://facebook.com",
      social_media_mastodon: "http://asdasd.com",
      social_media_instagram: "",
      social_media_telegram: "",
    },
    nexcloudToken: "wgMYAW7joTTtVuWy7FVuQ3snTF37ZPhvpSdIV9XiqDkykn3jay18jlh0BAntNPVXpxTSMqXe",
    keycloakToken:
      "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI1ODk2QzFKZDFUV3lXTGN6aEs3MmNLWXB2dnBpOTZtbjBXVzhSOVVmcTl3In0.eyJleHAiOjE2NjI0OTQ0NzAsImlhdCI6MTY2MjQ1ODQ3MCwianRpIjoiNTcxZDIwYjUtZDI2OC00Y2I1LWI3ODItMWQxMDg2MmUwMTMyIiwiaXNzIjoiaHR0cHM6Ly9hdXRoLmNvbG1lbmEubmV0d29yay9hdXRoL3JlYWxtcy9jb2xtZW5hIiwic3ViIjoiMzkxMGYxZGMtNWRjNC00OGJhLTg5YWQtMzE0NGY4MDUyYThkIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiY29sbWVuYV9wd2EiLCJzZXNzaW9uX3N0YXRlIjoiZWJhOGMyMzUtMmU2Ni00ZDBjLWIxNmEtMTAyMjU3NDRmNDI1IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyJodHRwczovL2NvbG1lbmEubWVkaWEiXSwicmVzb3VyY2VfYWNjZXNzIjp7ImNvbG1lbmFfcHdhIjp7InJvbGVzIjpbImFkbWluaXN0cmF0b3IiXX19LCJzY29wZSI6ImNvdW50cnkgbGFuZ3VhZ2UgcGhvdG8gcmVnaW9uIHBvc2l0aW9uIGVtYWlsIHByb2ZpbGUiLCJzaWQiOiJlYmE4YzIzNS0yZTY2LTRkMGMtYjE2YS0xMDIyNTc0NGY0MjUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImFkZHJlc3MiOnsicmVnaW9uIjoiIiwiY291bnRyeSI6IiJ9LCJwaG9uZSI6IiIsIm5hbWUiOiJQYXRyaWNrIE9saXZlaXJhIiwiZ3JvdXBzIjpbIi9kZXZ0ZWFtIl0sInBob3RvIjoiIiwibGFuZ3VhZ2UiOiIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJwYXRyaWNrIiwicG9zaXRpb24iOiIiLCJnaXZlbl9uYW1lIjoiUGF0cmljayIsImZhbWlseV9uYW1lIjoiT2xpdmVpcmEiLCJlbWFpbCI6InBhdHJpY2tvY3ZpeEBnbWFpbC5jb20ifQ.a-S-SLiWknLK6LZ8cGXe3ogRG7_e_gcBPr4J9ari8BwUDT-2C14oavhcZgUBagXMkW8yl2rwf0JvovHPnBe5lSa7EVdMYYZv6v-MeVrsZ9Neh3mTJ-5LwGw3r32TCMgVejZVO4JDQBk2yhEKjnQvkOV9zSPAncQOhb8gP3CuLV45H2zJkGp__MEjDQ7hAMBL9elxNi4_fxWovoa8SN_G90T1S01MPAUOA3MvkZThVYEBlAFzUSGnQbpxwlqMjYyopcg07ozC6yW1Joeq52DYZ9hjFMGEIFXQRFoQJ2L0yQCXzEMuLgYhO-l2vt5udU37GMjuyEvcwd2OpLJxVZcDtw",
  },
};

export default user;
