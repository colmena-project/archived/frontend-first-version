const mockConnect = jest.fn();
const mockcreateMediaElementSource = jest.fn(() => {
  return {
    connect: mockConnect,
  };
});
const mockgetByteFrequencyData = jest.fn();
const mockcreateAnalyser = jest.fn(() => {
  return {
    connect: mockConnect,
    frequencyBinCount: [0, 1, 2],
    getByteFrequencyData: mockgetByteFrequencyData,
  };
});
const mockcreateOscillator = jest.fn(() => {
  return {
    channelCount: 2,
  };
});
const mockChannelSplitterConnect = jest.fn((n) => n);
const mockcreateChannelSplitter = jest.fn(() => {
  return {
    connect: mockChannelSplitterConnect,
  };
});
const mockAudioContext = jest.fn(() => {
  return {
    createAnalyser: mockcreateAnalyser,
    createMediaElementSource: mockcreateMediaElementSource,
    createOscillator: mockcreateOscillator,
    createChannelSplitter: mockcreateChannelSplitter,
    createGain: jest.fn(),
  };
});

export default mockAudioContext;
