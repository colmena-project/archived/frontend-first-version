import { setupServer } from "msw/lib/node";
import colmenaHandlers from "./colmena";

const handlers = [...colmenaHandlers];

const server = setupServer(...handlers);

export default server;
