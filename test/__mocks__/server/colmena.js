import { rest } from "msw";

const handlers = [
  rest.get("api/list-group-folder", (req, res, ctx) => {
    return res(
      ctx.json({
        success: true,
        data: [
          "devteam",
          "radio_colmena_EN",
          "radio_colmena_FR",
          "radio_colmena_ES",
          "Radio_Colmena_PT_BR",
          "Gtest",
          "dwteam",
          "DW_Akademie",
          "PodDa-lhe",
          "Radio",
          "TESTE",
          "TESTE_GROUP_XPTÓ",
          "Radio_group_tésté",
          "TESTE_NOV_G",
          "TESTE_GROUP_TESTÉIO",
        ],
      }),
    );
  }),
];

export default handlers;
