import { GET_CATEGORIES } from "@/services/cms/categories";
import { GET_COLLABORATOR } from "@/services/cms/collaborators";
import { GET_PUBLICATION_FILES } from "@/services/cms/publicationFiles";
import { GET_PUBLICATIONS_BY_ADMIN, GET_PUBLICATION } from "@/services/cms/publications";

const formatMock = {
  ext: ".jpg",
  hash: "thumbnail_download_acc3c95693",
  width: 800,
  height: 600,
  mime: "image/jpeg",
  name: "thumbnail_download.jpg",
  path: null,
  size: 8.32,
  url: "/uploads/thumbnail_download_acc3c95693.jpg",
};

const mediaMock = {
  data: {
    id: 1,
    attributes: {
      name: "image.jpg",
      alternativeText: "image.jpg",
      createdAt: "2022-10-13T10:28:32.820Z",
      updatedAt: "2022-10-13T10:28:32.820Z",
      formats: {
        large: formatMock,
        medium: formatMock,
        small: formatMock,
        thumbnail: formatMock,
      },
    },
  },
};

export const mockGetPublicationResponse = {
  publication: {
    data: {
      id: "105",
      attributes: {
        title: "Batatinha frita 123",
        description: "Batatinha frita 123",
        coverage: "Batatinha frita 123",
        publisher: "devteam",
        date: "2023-02-13T10:18:07.139Z",
        content: "<p>fdssdfdsfdsf</p>",
        rights: "BY-SA-NC",
        creator: "gabrielantunescontato",
        publishedAt: null,
        tags: null,
        files: {
          data: [
            {
              id: "162",
              attributes: {
                title: "codecracker",
                description: "",
                language: "en",
                creator: "gabrielantunescontato",
                tags: "",
                format: "image/png",
                extent_size: 95034,
                extent_duration: null,
                downloads: 0,
                media: {
                  data: {
                    id: "240",
                    attributes: {
                      name: "codecracker.png",
                      alternativeText: null,
                      formats: {
                        large: {
                          ext: ".png",
                          url: "/uploads/large_codecracker_dddfe33550.png",
                          hash: "large_codecracker_dddfe33550",
                          mime: "image/png",
                          name: "large_codecracker.png",
                          path: null,
                          size: 137.82,
                          width: 1000,
                          height: 579,
                        },
                        small: {
                          ext: ".png",
                          url: "/uploads/small_codecracker_dddfe33550.png",
                          hash: "small_codecracker_dddfe33550",
                          mime: "image/png",
                          name: "small_codecracker.png",
                          path: null,
                          size: 45.63,
                          width: 500,
                          height: 289,
                        },
                        medium: {
                          ext: ".png",
                          url: "/uploads/medium_codecracker_dddfe33550.png",
                          hash: "medium_codecracker_dddfe33550",
                          mime: "image/png",
                          name: "medium_codecracker.png",
                          path: null,
                          size: 88.12,
                          width: 750,
                          height: 434,
                        },
                        thumbnail: {
                          ext: ".png",
                          url: "/uploads/thumbnail_codecracker_dddfe33550.png",
                          hash: "thumbnail_codecracker_dddfe33550",
                          mime: "image/png",
                          name: "thumbnail_codecracker.png",
                          path: null,
                          size: 15.01,
                          width: 245,
                          height: 142,
                        },
                      },
                      createdAt: "2023-02-21T18:08:03.667Z",
                      updatedAt: "2023-02-21T18:08:03.667Z",
                      url: "/uploads/codecracker_dddfe33550.png",
                      ext: ".png",
                      size: 38.07,
                      mime: "image/png",
                      __typename: "UploadFile",
                    },
                    __typename: "UploadFileEntity",
                  },
                  __typename: "UploadFileEntityResponse",
                },
                __typename: "PublicationFile",
              },
              __typename: "PublicationFileEntity",
            },
          ],
          __typename: "PublicationFileRelationResponseCollection",
        },
        subject: {
          data: {
            id: "9",
            attributes: {
              name: "Human rights",
              language: "en",
              __typename: "Category",
            },
            __typename: "CategoryEntity",
          },
          __typename: "CategoryEntityResponse",
        },
        relation: {
          data: {
            id: "241",
            attributes: {
              name: "teahub.io-white-on-white-wallpaper-1640618.JPEG",
              alternativeText: null,
              formats: {
                large: {
                  ext: ".JPEG",
                  url: "/uploads/large_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5.JPEG",
                  hash: "large_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5",
                  mime: "image/jpeg",
                  name: "large_teahub.io-white-on-white-wallpaper-1640618.JPEG",
                  path: null,
                  size: 60.43,
                  width: 1000,
                  height: 563,
                },
                small: {
                  ext: ".JPEG",
                  url: "/uploads/small_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5.JPEG",
                  hash: "small_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5",
                  mime: "image/jpeg",
                  name: "small_teahub.io-white-on-white-wallpaper-1640618.JPEG",
                  path: null,
                  size: 20.62,
                  width: 500,
                  height: 281,
                },
                medium: {
                  ext: ".JPEG",
                  url: "/uploads/medium_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5.JPEG",
                  hash: "medium_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5",
                  mime: "image/jpeg",
                  name: "medium_teahub.io-white-on-white-wallpaper-1640618.JPEG",
                  path: null,
                  size: 38.55,
                  width: 750,
                  height: 422,
                },
                thumbnail: {
                  ext: ".JPEG",
                  url: "/uploads/thumbnail_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5.JPEG",
                  hash: "thumbnail_teahub_io_white_on_white_wallpaper_1640618_36f35c42d5",
                  mime: "image/jpeg",
                  name: "thumbnail_teahub.io-white-on-white-wallpaper-1640618.JPEG",
                  path: null,
                  size: 6.71,
                  width: 245,
                  height: 138,
                },
              },
              createdAt: "2023-02-21T18:08:06.289Z",
              updatedAt: "2023-02-21T18:08:06.289Z",
              url: "/uploads/teahub_io_white_on_white_wallpaper_1640618_36f35c42d5.JPEG",
              ext: ".JPEG",
              size: 134.7,
              mime: "image/jpeg",
              __typename: "UploadFile",
            },
            __typename: "UploadFileEntity",
          },
          __typename: "UploadFileEntityResponse",
        },
        __typename: "Publication",
      },
      __typename: "PublicationEntity",
    },
    __typename: "PublicationEntityResponse",
  },
};

export const mockGetCategoriesValues = [
  {
    id: "4",
    name: "Current affairs, politics and society",
  },
  {
    id: "5",
    name: "Economy",
  },
  {
    id: "8",
    name: "Education",
  },
  {
    id: "11",
    name: "Environment, Climate",
  },
  {
    id: "10",
    name: "Gender, diversity",
  },
  {
    id: "7",
    name: "Health",
  },
  {
    id: "1",
    name: "History and culture",
  },
  {
    id: "9",
    name: "Human rights",
  },
  {
    id: "2",
    name: "Leisure and sports",
  },
  {
    id: "3",
    name: "Music and entertainment",
  },
  {
    id: "6",
    name: "Science and technology",
  },
];

export const mockGetPublicationFiles = [
  {
    id: "163",
    attributes: {
      title: "bank-2",
      description: "one description",
      language: "en",
      creator: "gabrielantunescontato",
      tags: "",
      format: "image/jpeg",
      extent_size: 305357,
      extent_duration: null,
      downloads: 0,
      media: {
        data: {
          id: "246",
          attributes: {
            name: "bank-2.jpg",
            alternativeText: null,
            formats: {
              large: {
                ext: ".jpg",
                url: "/uploads/large_bank_2_746806d385.jpg",
                hash: "large_bank_2_746806d385",
                mime: "image/jpeg",
                name: "large_bank-2.jpg",
                path: null,
                size: 87.51,
                width: 1000,
                height: 563,
              },
              small: {
                ext: ".jpg",
                url: "/uploads/small_bank_2_746806d385.jpg",
                hash: "small_bank_2_746806d385",
                mime: "image/jpeg",
                name: "small_bank-2.jpg",
                path: null,
                size: 26.92,
                width: 500,
                height: 281,
              },
              medium: {
                ext: ".jpg",
                url: "/uploads/medium_bank_2_746806d385.jpg",
                hash: "medium_bank_2_746806d385",
                mime: "image/jpeg",
                name: "medium_bank-2.jpg",
                path: null,
                size: 52.89,
                width: 750,
                height: 422,
              },
              thumbnail: {
                ext: ".jpg",
                url: "/uploads/thumbnail_bank_2_746806d385.jpg",
                hash: "thumbnail_bank_2_746806d385",
                mime: "image/jpeg",
                name: "thumbnail_bank-2.jpg",
                path: null,
                size: 8.49,
                width: 245,
                height: 138,
              },
            },
            createdAt: "2023-03-15T22:31:24.324Z",
            updatedAt: "2023-03-15T22:31:24.324Z",
            url: "/uploads/bank_2_746806d385.jpg",
            ext: ".jpg",
            size: 305.36,
            mime: "image/jpeg",
            __typename: "UploadFile",
          },
          __typename: "UploadFileEntity",
        },
        __typename: "UploadFileEntityResponse",
      },
      __typename: "PublicationFile",
    },
    __typename: "PublicationFileEntity",
  },
  {
    id: "164",
    attributes: {
      title: "bank",
      description: "another descruiption",
      language: "en",
      creator: "gabrielantunescontato",
      tags: "",
      format: "image/jpeg",
      extent_size: 98846,
      extent_duration: null,
      downloads: 0,
      media: {
        data: {
          id: "247",
          attributes: {
            name: "bank.jpg",
            alternativeText: null,
            formats: {
              small: {
                ext: ".jpg",
                url: "/uploads/small_bank_e8d7902ca1.jpg",
                hash: "small_bank_e8d7902ca1",
                mime: "image/jpeg",
                name: "small_bank.jpg",
                path: null,
                size: 33.29,
                width: 500,
                height: 281,
              },
              medium: {
                ext: ".jpg",
                url: "/uploads/medium_bank_e8d7902ca1.jpg",
                hash: "medium_bank_e8d7902ca1",
                mime: "image/jpeg",
                name: "medium_bank.jpg",
                path: null,
                size: 65.43,
                width: 750,
                height: 422,
              },
              thumbnail: {
                ext: ".jpg",
                url: "/uploads/thumbnail_bank_e8d7902ca1.jpg",
                hash: "thumbnail_bank_e8d7902ca1",
                mime: "image/jpeg",
                name: "thumbnail_bank.jpg",
                path: null,
                size: 9.27,
                width: 245,
                height: 138,
              },
            },
            createdAt: "2023-03-15T22:31:26.186Z",
            updatedAt: "2023-03-15T22:31:26.186Z",
            url: "/uploads/bank_e8d7902ca1.jpg",
            ext: ".jpg",
            size: 98.84,
            mime: "image/jpeg",
            __typename: "UploadFile",
          },
          __typename: "UploadFileEntity",
        },
        __typename: "UploadFileEntityResponse",
      },
      __typename: "PublicationFile",
    },
    __typename: "PublicationFileEntity",
  },
];

const mocks = [
  {
    request: {
      query: GET_COLLABORATOR,
      variables: {
        name: "username",
      },
    },
    result: {
      data: {
        collabarators: {
          data: {
            id: 1,
            attributes: {
              username: "username",
              avatar: mediaMock,
            },
          },
        },
      },
    },
  },

  {
    request: {
      query: GET_CATEGORIES,
      variables: {
        language: "en",
      },
    },

    result: {
      data: {
        categories: {
          data: [
            {
              id: "4",
              attributes: {
                name: "Current affairs, politics and society",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "5",
              attributes: {
                name: "Economy",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "8",
              attributes: {
                name: "Education",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "11",
              attributes: {
                name: "Environment, Climate",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "10",
              attributes: {
                name: "Gender, diversity",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "7",
              attributes: {
                name: "Health",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "1",
              attributes: {
                name: "History and culture",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "9",
              attributes: {
                name: "Human rights",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "2",
              attributes: {
                name: "Leisure and sports",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "3",
              attributes: {
                name: "Music and entertainment",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
            {
              id: "6",
              attributes: {
                name: "Science and technology",
                __typename: "Category",
              },
              __typename: "CategoryEntity",
            },
          ],
          __typename: "CategoryEntityResponseCollection",
        },
      },
    },
  },

  {
    request: {
      query: GET_PUBLICATIONS_BY_ADMIN,
      variables: {
        publisher: "devteam",
        keyword: "",
        order: "createdAt:desc",
        page: 1,
        pageSize: 10,
      },
    },

    result: {
      data: {
        publications: {
          data: [
            {
              id: "103",
              attributes: {
                title: "test share",
                description: "test",
                publisher: "devteam",
                date: "2023-02-07T09:59:04.423Z",
                creator: "habib",
                publishedAt: "2023-02-07T09:59:26.921Z",
                createdAt: "2023-02-07T09:59:26.679Z",
                disapprovedAt: null,
                disapprovalReason: null,
                subject: {
                  data: {
                    id: "30",
                    attributes: { name: "Educação", __typename: "Category" },
                    __typename: "CategoryEntity",
                  },
                  __typename: "CategoryEntityResponse",
                },
                relation: { data: null, __typename: "UploadFileEntityResponse" },
                __typename: "Publication",
              },
              __typename: "PublicationEntity",
            },
            {
              id: "102",
              attributes: {
                title: "fsadfdsf",
                description: "dfsdsfds",
                publisher: "devteam",
                date: "2023-02-02T21:37:04.462Z",
                creator: "gabrielantunescontato",
                publishedAt: null,
                createdAt: "2023-02-02T21:40:18.619Z",
                disapprovedAt: null,
                disapprovalReason: null,
                subject: {
                  data: {
                    id: "9",
                    attributes: { name: "Human rights", __typename: "Category" },
                    __typename: "CategoryEntity",
                  },
                  __typename: "CategoryEntityResponse",
                },
                relation: { data: null, __typename: "UploadFileEntityResponse" },
                __typename: "Publication",
              },
              __typename: "PublicationEntity",
            },
          ],
          meta: {
            pagination: {
              page: 1,
              pageSize: 10,
              pageCount: 6,
              total: 56,
              __typename: "Pagination",
            },
            __typename: "ResponseCollectionMeta",
          },
          __typename: "PublicationEntityResponseCollection",
        },
      },
    },
  },

  {
    request: {
      query: GET_PUBLICATION,
      variables: {
        language: "en",
      },
    },

    result: {
      data: mockGetPublicationResponse,
    },
  },

  {
    request: {
      query: GET_PUBLICATION_FILES,
      variables: {
        publicationId: "107",
      },
    },

    result: {
      data: {
        publicationFiles: {
          data: mockGetPublicationFiles,
        },
      },
    },
  },
];

export default mocks;
