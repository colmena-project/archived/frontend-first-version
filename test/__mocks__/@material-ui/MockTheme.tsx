import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "@/styles/theme";

function MockTheme({ children }: any) {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}

export default MockTheme;
