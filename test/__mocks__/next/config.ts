const getConfig = () => {
  return {
    serverRuntimeConfig: {
      defaultNewUserPassword: process.env.DEFAULT_USER_PASSWORD,
      hotjarProd: process.env.HOTJAR_PROD,
      appEnv: process.env.APP_ENV,
      NCTalkVersion: process.env.NEXTCLOUD_TALK_VERSION,
      adminInfo: {
        username: process.env.USERNAME_ADMIN_NC,
        password: process.env.PASSWORD_ADMIN_NC
      },
      api: {
        baseUrl: process.env.API_BASE_URL
      },
      blog: {
        baseUrl: process.env.BLOG_URL,
      },
      keycloak: {
        baseUrl: process.env.KEYCLOAK_BASE_URL,
        clientId: process.env.KEYCLOAK_CLIENT_ID,
        clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
        serviceClientId: process.env.KEYCLOAK_SERVICE_CLIENT_ID,
        serviceClientSecret: process.env.KEYCLOAK_SERVICE_CLIENT_SECRET,
        realm: process.env.KEYCLOAK_REALM,
        colmenaPwaId: process.env.KEYCLOAK_COLMENA_PWA_ID,
        collaboratorId: process.env.KEYCLOAK_COLLABORATOR_ROLE_ID,
        administratorId: process.env.KEYCLOAK_ADMINISTRATOR_ROLE_ID
      }
    },
    publicRuntimeConfig: {
      api: {
        baseUrl: process.env.API_BASE_URL
      },
      wss: {
        baseUrl: process.env.WSS_BASE_URL
      },
      NCTalkVersion: process.env.NEXTCLOUD_TALK_VERSION,
      blog: {
        baseUrl: process.env.BLOG_URL,
      }
    }
  };
}

export default getConfig;