const mockaAudioBuffer = jest.fn(() => {
  return {
    copyFromChannel: jest.fn(),
    copyToChannel: jest.fn(),
    getChannelData: jest.fn(),
  };
});

export default mockaAudioBuffer;
