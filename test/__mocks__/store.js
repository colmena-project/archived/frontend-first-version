import user from "@/tests/__mocks__/data/user";

export const initializeStore = (preloadedState = {}) => {
  return {
    getState: () => ({
      user,
      ...preloadedState,
    }),
  };
};
