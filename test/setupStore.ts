import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "../src/store/reducers/index";

export const setupStore = (preloadedState = {}) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState,
  });
};
