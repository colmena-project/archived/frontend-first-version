import React from "react";
import { screen } from "@testing-library/react";
import Emergency from "@/components/statefull/Emergency";
import { renderWithProviders as render } from "@/tests/utils/testUtils";

jest.mock("@/hooks/useUsernameLocalStorage", () => {
  return jest.fn(() => ({
    username: "mockedUsername",
    setUsername: jest.fn(),
  }));
});

describe("<Emergency />", () => {
  it("Should render the component in its default form with the position property set to right", () => {
    render(<Emergency />);

    const element = screen.getByTestId("emergency-test-id");
    expect(element).toBeInTheDocument();
    expect(element).toHaveStyle("top: 50%");
    expect(screen.queryByTestId("emergency-delete-confirm")).toBeNull();
  });

  it("Should render the component with the position property at top", () => {
    render(<Emergency position="top" />);

    const element = screen.getByTestId("emergency-test-id");
    expect(element).toBeInTheDocument();
    expect(element).toHaveStyle("transform: translateX(-50%)");
    expect(screen.queryByTestId("emergency-delete-confirm")).toBeNull();
  });
});
