import React from "react";
import { screen } from "@testing-library/react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import NewFolderModal from "@/components/ui/ToolsMenu/ToolsModal/NewFolderModal";

const fn = jest.fn();

describe("NewFolderModal Component", () => {
  it("should modal to be hidden", () => {
    render(<NewFolderModal open={false} handleClose={fn} />);
    const element = screen.getByTestId("modal-new-folder");
    expect(element).toBeInvalid();
  });

  it("should modal to be shown", () => {
    render(<NewFolderModal open={true} handleClose={fn} />);
    const element = screen.getByTestId("modal-new-folder");
    expect(element).toBeDefined();
  });
});
