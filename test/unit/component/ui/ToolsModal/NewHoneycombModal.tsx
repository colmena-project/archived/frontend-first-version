import React from "react";
import { screen } from "@testing-library/react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import NewHoneycombModal from "@/components/ui/ToolsMenu/ToolsModal/NewHoneycombModal";

const fn = jest.fn();

describe("NewHoneycombModal Component", () => {
  it("should modal to be hidden", () => {
    render(<NewHoneycombModal open={false} handleClose={fn} />);
    const element = screen.getByTestId("modal-create-honeycomb");
    expect(element).toBeInvalid();
  });

  it("should modal to be shown", () => {
    render(<NewHoneycombModal open={true} handleClose={fn} />);
    const element = screen.getByTestId("modal-create-honeycomb");
    expect(element).toBeDefined();
  });
});
