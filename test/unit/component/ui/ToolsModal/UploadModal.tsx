import React from "react";
import { screen } from "@testing-library/react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import UploadModal from "@/components/ui/ToolsMenu/ToolsModal/UploadModal";

const fn = jest.fn();

describe("UploadModal Component", () => {
  it("should modal to be hidden", () => {
    render(<UploadModal open={false} handleClose={fn} />);
    const element = screen.getByTestId("modal-file-upload");
    expect(element).toBeInvalid();
  });

  it("should modal to be shown", () => {
    render(<UploadModal open={true} handleClose={fn} />);
    const element = screen.getByTestId("modal-file-upload");
    expect(element).toBeDefined();
  });
});
