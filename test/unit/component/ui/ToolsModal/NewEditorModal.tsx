import React from "react";
import { screen } from "@testing-library/react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import NewEditorModal from "@/components/ui/ToolsMenu/ToolsModal/NewEditorModal";

const fn = jest.fn();

describe("NewEditorModal Component", () => {
  it("should modal to be hidden", () => {
    render(<NewEditorModal open={false} handleClose={fn} />);
    const element = screen.getByTestId("modal-new-file");
    expect(element).toBeInvalid();
  });

  it("should modal to be shown", () => {
    render(<NewEditorModal open={true} handleClose={fn} />);
    const element = screen.getByTestId("modal-new-file");
    expect(element).toBeDefined();
  });
});
