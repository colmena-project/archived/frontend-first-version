import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen } from "@testing-library/react";
import ContextMenuOptions from "@/components/pages/library/contextMenu";
import file from "@/tests/__mocks__/data/library/file";
import { ContextMenuOptionEnum } from "@/enums/index";

import user from "@/tests/__mocks__/data/user";

const preloadStateUser = { user };

describe("Library ContextMenu Component", () => {
  it('should return "edit" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.EDIT]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("edit-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "copy" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.COPY]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("copy-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "move" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.MOVE]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("move-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "duplicate" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.DUPLICATE]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("duplicate-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "rename" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.RENAME]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("rename-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "details" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.DETAILS]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("details-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "available offline" option in library context menu', () => {
    render(
      <ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.AVAILABLE_OFFLINE]} />,
      {
        preloadedState: preloadStateUser,
      },
    );
    const contextMenuElement = screen.getByTestId("sync-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "publish" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.PUBLISH]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("publish-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "delete" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.DELETE]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("delete-item");
    expect(contextMenuElement).toBeDefined();
  });

  it('should return "download" option in library context menu', () => {
    render(<ContextMenuOptions {...file} availableOptions={[ContextMenuOptionEnum.DOWNLOAD]} />, {
      preloadedState: preloadStateUser,
    });
    const contextMenuElement = screen.getByTestId("download-item");
    expect(contextMenuElement).toBeDefined();
  });
});
