import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, act, fireEvent } from "@testing-library/react";
import ChangeFolderAudioSaved from "@/components/ui/WaveformPlaylist/ChangeFolderAudioSaved";

const preloadStateUser = {
  user: {
    user: {
      id: "vinicius",
    },
  },
};

describe("ChangeFolderAudioSaved Component", () => {
  it("should return the upload path", async () => {
    await act(() => {
      render(<ChangeFolderAudioSaved />, {
        preloadedState: preloadStateUser,
      });
    });
    const elem = screen.getByTestId("upload-location");
    expect(elem).toBeDefined();
  });

  it("should return the change upload location button", async () => {
    await act(() => {
      render(<ChangeFolderAudioSaved />, {
        preloadedState: preloadStateUser,
      });
    });
    const elem = screen.getByRole("button");
    expect(elem).toBeDefined();
  });
});
