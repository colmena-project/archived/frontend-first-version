import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import AudioControls from "@/components/ui/WaveformPlaylist/AudioControls";
import EventEmitter from "events";
import { setupStore } from "@/tests/setupStore";
import { updateRecordingState } from "@/store/actions/recordings";

const genericFn = jest.fn();
const ee = new EventEmitter();
const store = setupStore();

describe("AudioControls Component with context RECORDER", () => {
  it("should return AudioControls with rec button active", () => {
    render(
      <AudioControls
        ee={ee}
        handleForward={genericFn}
        handleRewind={genericFn}
        context="recorder"
      />,
    );
    const elem = screen.getByTestId("record-button");
    expect(elem).toBeDefined();
  });

  it("should return AudioControls with stop button active", () => {
    store.dispatch(updateRecordingState("RECORDING"));
    render(
      <AudioControls
        ee={ee}
        handleForward={genericFn}
        handleRewind={genericFn}
        context="recorder"
      />,
      { store },
    );
    const elem = screen.getByTestId("stop-button");
    expect(elem).toBeDefined();
  });
  it("should return AudioControls with timer component", () => {
    store.dispatch(updateRecordingState("RECORDING"));
    render(
      <AudioControls
        ee={ee}
        handleForward={genericFn}
        handleRewind={genericFn}
        context="recorder"
      />,
      { store },
    );
    const elem = screen.getByTestId("timer-text");
    expect(elem).toBeDefined();
  });

  it("should return AudioControls with 5 buttons", () => {
    store.dispatch(updateRecordingState("STOPPED"));
    render(
      <AudioControls
        ee={ee}
        handleForward={genericFn}
        handleRewind={genericFn}
        context="recorder"
      />,
      { store },
    );
    const elem = screen.getAllByRole("button");
    expect(elem).toHaveLength(5);
  });

  it("must disable buttons when starting recording", () => {
    // store.dispatch(updateRecordingState("RECORDING"));
    // render(
    //   <AudioControls
    //     ee={ee}
    //     handleForward={genericFn}
    //     handleRewind={genericFn}
    //     context="editing"
    //   />,
    //   { store },
    // );
    // const recordButton = screen.getByTestId("record-button");
    // fireEvent.click(recordButton);
    // const rewindButton = screen.getByTestId("rewind-button");
    // expect(rewindButton).toBeDisabled();
  });
});
