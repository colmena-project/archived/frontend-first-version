import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, act, fireEvent } from "@testing-library/react";
import DisplayInformationStopped from "@/components/ui/WaveformPlaylist/DisplayInformationStopped";

describe("DisplayInformationStopped Component", () => {
  it.skip("should render with empty span", async () => {
    render(<DisplayInformationStopped audioState="NONE" path="private" />);
    const elem = screen.getByTestId("empty");
    expect(elem).toBeDefined();
  });
});
