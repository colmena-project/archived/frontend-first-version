import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import DisplayInformationTitle from "@/components/ui/WaveformPlaylist/DisplayInformationTitle";
import userEvent from "@testing-library/user-event";

describe("DisplayInformationTitle Component", () => {
  it("should render filename title - NONE state", async () => {
    render(<DisplayInformationTitle filename="/private/vinicius.wav" saved={false} />, {
      preloadedState: {
        recording: {
          activeRecordingState: "NONE",
        },
      },
    });
    const elem = screen.getByTestId("recorder-title");
    expect(elem).toBeDefined();
  });
  it("should render filename title - RECORDING state", async () => {
    render(<DisplayInformationTitle filename="/private/vinicius.wav" saved={false} />, {
      preloadedState: {
        recording: {
          activeRecordingState: "RECORDING",
        },
      },
    });
    const elem = screen.getByTestId("recorder-title");
    expect(elem).toBeDefined();
  });
  it("should render correctly the filename title and edit button", async () => {
    render(<DisplayInformationTitle filename="/private/vinicius.wav" saved />, {
      preloadedState: {
        recording: {
          activeRecordingState: "STOPPED",
        },
      },
    });
    const elem = screen.getByTestId("recorder-title");
    const elem2 = screen.getByTestId("edit-recorder-filename-button");
    expect(elem).toBeDefined();
    expect(elem2).toBeDefined();
  });
  it("should open the edit filename modal correctly", async () => {
    render(<DisplayInformationTitle filename="/private/vinicius.wav" saved />, {
      preloadedState: {
        recording: {
          activeRecordingState: "STOPPED",
        },
      },
    });
    fireEvent.click(screen.getByTestId("edit-recorder-filename-button"));
    const elem = screen.getByTestId("edit-filename-modal");
    expect(elem).toBeDefined();
  });
});
