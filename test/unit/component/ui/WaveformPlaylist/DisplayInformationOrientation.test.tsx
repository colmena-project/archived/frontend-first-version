import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, act, fireEvent } from "@testing-library/react";
import DisplayInformationOrientation from "@/components/ui/WaveformPlaylist/DisplayInformationOrientation";

const preloadStateUser = {
  user: {
    user: {
      id: "vinicius",
    },
  },
};

describe("DisplayInformationOrientation Component", () => {
  it("should render upload path and extra informations - audioState NONE", async () => {
    await act(() => {
      render(<DisplayInformationOrientation audioState="NONE" path="private" />, {
        preloadedState: preloadStateUser,
      });
    });
    const elem = screen.getByTestId("container-change-audio-location");
    const elem2 = screen.getByTestId("recording-orientation");
    const elem3 = screen.queryByTestId("upload-location-title");
    expect(elem).toBeDefined();
    expect(elem2).toBeDefined();
    expect(elem3).toBeNull();
  });
  it("should render extra information - audioState RECORDING", async () => {
    await act(() => {
      render(<DisplayInformationOrientation audioState="RECORDING" path="private" />, {
        preloadedState: preloadStateUser,
      });
    });
    const elem = screen.queryByTestId("container-change-audio-location");
    const elem2 = screen.queryByTestId("recording-orientation");
    const elem3 = screen.getByTestId("upload-location-title");
    expect(elem).toBeNull();
    expect(elem2).toBeNull();
    expect(elem3).toBeDefined();
  });
});
