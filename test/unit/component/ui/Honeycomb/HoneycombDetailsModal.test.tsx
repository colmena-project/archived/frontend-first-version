import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, waitFor } from "@testing-library/react";
import HoneycombDetailsModal from "@/components/pages/honeycomb/HoneycombDetailsModal";
import room from "@/tests/__mocks__/data/room";
import user from "@/tests/__mocks__/data/user";
import server from "@/tests/__mocks__/server";

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

const preloadStateUser = { user };

describe("Honeycomb Details Modal Component", () => {
  it("should return modal with honeycomb details", async () => {
    render(<HoneycombDetailsModal open={true} closeProfileModal={jest.fn()} room={room} />, {
      preloadedState: preloadStateUser,
    });

    await waitFor(() => {
      const honeycombDetailsModal = screen.getByTestId("honeycomb-details-modal");
      expect(honeycombDetailsModal).toBeDefined();
      expect(honeycombDetailsModal).toContainHTML("Talk updates");
    });
  });
});
