import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, waitFor } from "@testing-library/react";
import EditHoneycombAvatar from "@/components/pages/honeycomb/RoomAvatar/EditHoneycombAvatar";
import userEvent from "@testing-library/user-event";
import { RoomInterface } from "@/interfaces/cms";

const room: RoomInterface = {} as RoomInterface;

describe("Edit Honeycomb Avatar Component", () => {
  it("should return button with icon edit", () => {
    render(<EditHoneycombAvatar room={room} token="" refetchRoom={jest.fn()} />);
    const toolsMenuElement = screen.getByTestId("edit_circle");
    expect(toolsMenuElement).toBeDefined();
  });

  it("should return upload modal when file input is changed", async () => {
    render(<EditHoneycombAvatar room={room} token="" refetchRoom={jest.fn()} />);

    const str = "this is a image";
    const blob = new Blob([str]);
    const file = new File([blob], "image.jpg", {
      type: "image/jpeg",
    });
    File.prototype.text = jest.fn().mockResolvedValueOnce(str);
    const input = screen.getByTestId("upload-file");
    userEvent.upload(input, file);

    await waitFor(() => {
      const uploadModal = screen.getByTestId("upload-honeycomb-avatar-modal");
      expect(uploadModal).toBeDefined();
    });
  });
});
