import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import FileMetadataCollapse from "@/components/ui/FileMetadataCollapse";
import user from "@/tests/__mocks__/data/user";

const preloadStateUser = { user };

const fileMock = {
  size: "65456",
  tags: "tag1, tag2, tag3",
  creator: "user99",
  language: "en",
  mime: "image/jpeg",
  title: "My Image",
};

describe("FileMetadataCollapse Component", () => {
  it("Should return file card", () => {
    render(
      <FileMetadataCollapse
        metadata={{
          size: parseInt(fileMock.size, 10),
          tags: fileMock.tags,
          mime: fileMock.mime,
          creator: fileMock.creator,
          language: fileMock.language,
          title: fileMock.title,
        }}
      />,
      {
        preloadedState: preloadStateUser,
      },
    );
    const fileMetadataComponent = screen.getByTestId("file-medatada-collapse");
    expect(fileMetadataComponent).toBeDefined();
  });

  it("Should show view metadata button", () => {
    render(
      <FileMetadataCollapse
        metadata={{
          size: parseInt(fileMock.size, 10),
          tags: fileMock.tags,
          mime: fileMock.mime,
          creator: fileMock.creator,
          language: fileMock.language,
          title: fileMock.title,
        }}
      />,
      {
        preloadedState: preloadStateUser,
      },
    );
    const fileMetadataComponent = screen.getByTestId("view-metadata");
    expect(fileMetadataComponent).toBeDefined();
  });

  it("Should show metadata info when click view metadata", () => {
    render(
      <FileMetadataCollapse
        metadata={{
          size: parseInt(fileMock.size, 10),
          tags: fileMock.tags,
          mime: fileMock.mime,
          creator: fileMock.creator,
          language: fileMock.language,
          title: fileMock.title,
        }}
      />,
      {
        preloadedState: preloadStateUser,
      },
    );
    fireEvent.click(screen.getByTestId("view-details"));
    fireEvent.click(screen.getByTestId("view-metadata"));

    const fileMetadataComponent = screen.getByTestId("file-metadata");
    expect(fileMetadataComponent).toBeDefined();
  });

  it("Should show view metadata when click view metadata", () => {
    render(
      <FileMetadataCollapse
        metadata={{
          size: parseInt(fileMock.size, 10),
          tags: fileMock.tags,
          mime: fileMock.mime,
          creator: fileMock.creator,
          language: fileMock.language,
          title: fileMock.title,
        }}
      />,
      {
        preloadedState: preloadStateUser,
      },
    );
    fireEvent.click(screen.getByTestId("view-metadata"));

    const fileMetadataComponent = screen.getByTestId("file-metadata");
    expect(fileMetadataComponent).toBeDefined();
  });
});
