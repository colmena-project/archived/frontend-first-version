import React from "react";
import { screen } from "@testing-library/react";
import Notifications from "@/components/ui/Notifications";
import { renderWithProviders as render } from "@/tests/utils/testUtils";

describe("Notifications Component", () => {
  it("should return the open notifications component", () => {
    render(<Notifications />);
    const notificationsMenuElement = screen.getByTestId("notifications");

    expect(notificationsMenuElement).toBeInTheDocument();
  });
});
