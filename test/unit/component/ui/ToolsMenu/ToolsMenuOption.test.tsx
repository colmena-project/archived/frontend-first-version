import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen } from "@testing-library/react";
import ToolsMenuOption from "@/components/ui/ToolsMenu/ToolsMenuOption";

describe("ToolsMenuOption Component", () => {
  it("should return option", () => {
    render(<ToolsMenuOption>Option</ToolsMenuOption>);
    const toolsMenuOptionElement = screen.getByTestId("tools-menu-option");
    expect(toolsMenuOptionElement).toBeDefined();
  });

  it("should return option icon", () => {
    const icon = "star";
    render(<ToolsMenuOption icon={icon}>Option</ToolsMenuOption>);
    const toolsMenuOptionElement = screen.getByTestId(`${icon}`);
    expect(toolsMenuOptionElement).toBeDefined();
  });
});
