import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import ToolsMenu, { ToolsMenuOption } from "@/components/ui/ToolsMenu";

describe("ToolsMenu Component", () => {
  it("should return tools menu icon", () => {
    render(<ToolsMenu />);
    const toolsMenuElement = screen.getByTestId("tools-menu");
    expect(toolsMenuElement).toBeDefined();
  });

  it("should return tools menu with close button", () => {
    render(
      <ToolsMenu>
        <ToolsMenuOption>Option 1</ToolsMenuOption>
        <ToolsMenuOption>Option 2</ToolsMenuOption>
      </ToolsMenu>,
    );
    fireEvent.click(screen.getByTestId("tools-menu"));

    const element = screen.getByTestId("close-tools-menu");
    expect(element).toBeDefined();
  });

  it("should return tools menu with options", () => {
    render(
      <ToolsMenu>
        <ToolsMenuOption>Option 1</ToolsMenuOption>
        <ToolsMenuOption>Option 2</ToolsMenuOption>
      </ToolsMenu>,
    );
    fireEvent.click(screen.getByTestId("tools-menu"));

    const element = screen.getAllByTestId("tools-menu-option");
    expect(element).toHaveLength(3); // The button more options always return, adding 1
  });
});
