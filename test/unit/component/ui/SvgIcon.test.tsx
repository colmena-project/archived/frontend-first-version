import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import SvgIcon from "@/components/ui/SvgIcon";

describe("SvgIcon component", () => {
  it("should return element with empty content", () => {
    render(<SvgIcon icon="homeError" />, {});
    const element = screen.getByTestId("homeError");
    expect(element.textContent).toMatch("");
  });
  it("should return svg element", () => {
    render(<SvgIcon icon="home" />, {});
    const element = screen.getByTestId("home");
    expect(element).toBeDefined();
  });
  it("should return svg element with fontSize LARGE", () => {
    render(<SvgIcon icon="home" fontSize="large" />, {});
    const element = screen.getByTestId("home");
    expect(element).toHaveClass("MuiSvgIcon-fontSizeLarge");
  });
  it("should return svg element with color #000", () => {
    render(<SvgIcon icon="home" htmlColor="#000" />, {});
    const element = screen.getByTestId("home");
    expect(element).toHaveAttribute("color");
  });
});
