import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import ChannelListItem from "@/components/pages/channel/ChannelListItem";
import room from "@/tests/__mocks__/data/room";
import user from "@/tests/__mocks__/data/user";
import server from "@/tests/__mocks__/server";

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

const preloadStateUser = { user };

describe("ChannelListItem Component", () => {
  it("should return channel card item", () => {
    render(<ChannelListItem room={room} />, {
      preloadedState: preloadStateUser,
    });
    const element = screen.getByTestId("channel-card-item");
    expect(element).toBeDefined();
  });

  it("should become visible when more details button is clicked", () => {
    render(<ChannelListItem room={room} />, {
      preloadedState: preloadStateUser,
    });
    fireEvent.click(screen.getByTestId("more-details-button"));

    const element = screen.getByTestId("more-details");
    expect(element).toBeVisible();
  });

  it("should load component with disabled join a call button", () => {
    render(<ChannelListItem room={room} />, {
      preloadedState: preloadStateUser,
    });

    const element = screen.getAllByTestId("join-channel")[0];
    expect(element).toBeDisabled();
  });

  it("should enable join a call button when user accept terms", () => {
    render(<ChannelListItem room={room} />, {
      preloadedState: preloadStateUser,
    });
    fireEvent.click(screen.getByTestId("accept-terms-input"));

    const element = screen.getAllByTestId("join-channel")[0];
    expect(element).toBeEnabled();
  });
});
