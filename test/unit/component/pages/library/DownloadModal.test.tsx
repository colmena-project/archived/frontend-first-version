import DownloadModal from "@/components/pages/library/contextMenu/DownloadModal";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen } from "@testing-library/react";

import user from "@/tests/__mocks__/data/user";

const preloadState = { user };
const genericProps = {
  open: true,
  handleOpen: jest.fn(),
  filename: "private/gfdg.md",
  basename: "gfdg.md",
  mime: "'text/markdown'",
  arrayBufferBlob: undefined,
};

describe("DownloadModal Component", () => {
  it("should render the already open DownloadModal", () => {
    render(<DownloadModal {...genericProps} />, {
      preloadedState: preloadState,
    });

    let modal = screen.getByTestId("modal-download-item");
    expect(modal).toBeInTheDocument();
  });

  it("should render two list items", () => {
    const { getAllByRole } = render(<DownloadModal {...genericProps} />, {
      preloadedState: preloadState,
    });

    const listItems = getAllByRole("menuitem");
    expect(listItems.length).toBe(2);
  });

  it("should download an MD file by clicking on the corresponding menu", async () => {
    const { getByTestId } = render(<DownloadModal {...genericProps} />, {
      preloadedState: preloadState,
    });

    const menu = getByTestId("menu-download-md");
    expect(menu).toBeEnabled();
  });

  it("should download an RTF file by clicking on the corresponding menu", async () => {
    const { getByTestId } = render(<DownloadModal {...genericProps} />, {
      preloadedState: preloadState,
    });

    const menu = getByTestId("menu-download-rtf");

    expect(menu).toBeEnabled();
  });
});
