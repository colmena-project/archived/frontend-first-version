import React from "react";
import CloudAndStorageOptions from "@/components/pages/profile/CloudAndStorageOptions";
import { screen, render, fireEvent } from "@testing-library/react";
import MockTheme from "@/tests/__mocks__/@material-ui/MockTheme";

const changePage = jest.fn();

describe("<CloudAndStorageOptions />", () => {
  it("Should render the component in its initial state", () => {
    render(
      <MockTheme>
        <CloudAndStorageOptions changePage={changePage} />
      </MockTheme>,
    );

    expect(screen.getByTestId("back-to-settings-menu")).toBeInTheDocument();
    expect(screen.getByTestId("change-profile-info")).toBeInTheDocument();
    expect(screen.queryByTestId("clear-item-confirm")).toBeNull();
  });

  it("Should show a modal when clicked", () => {
    render(
      <MockTheme>
        <CloudAndStorageOptions changePage={changePage} />
      </MockTheme>,
    );

    fireEvent.click(screen.getByTestId("change-profile-info"));
    expect(screen.getByTestId("clear-item-confirm"));
  });
});
