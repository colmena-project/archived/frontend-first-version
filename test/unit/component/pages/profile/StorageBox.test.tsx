import React from "react";
import { screen, render } from "@testing-library/react";
import StorageBox from "@/components/pages/profile/StorageBox";
import MockTheme from "@/tests/__mocks__/@material-ui/MockTheme";

const fullMock = {
  quota: 1073741824,
  usage: 36291542,
  title: "title mock",
  footer: "footer mock",
  showBrowser: true,
};

const limitedMock = {
  quota: 1073741824,
  usage: 36291542,
  title: "title mock",
  showBrowser: false,
};

describe("<StorageBox />", () => {
  it("Should render completely", () => {
    render(
      <MockTheme>
        <StorageBox {...fullMock} />
      </MockTheme>,
    );

    expect(screen.getByTestId("storage-box")).toBeInTheDocument();
    expect(screen.getByTestId("title-storage-box")).toBeInTheDocument();
    expect(screen.getByTestId("progress-contant-storage-box")).toBeInTheDocument();
    expect(screen.getByTestId("display-browser-storage-box")).toBeInTheDocument();
    expect(screen.getByTestId("footer-storage-box")).toBeInTheDocument();
  });

  it("Should render the component in a limited way, hiding the DisplayBrowser and Footer", () => {
    render(
      <MockTheme>
        <StorageBox {...limitedMock} />
      </MockTheme>,
    );

    expect(screen.getByTestId("storage-box")).toBeInTheDocument();
    expect(screen.getByTestId("title-storage-box")).toBeInTheDocument();
    expect(screen.getByTestId("progress-contant-storage-box")).toBeInTheDocument();

    expect(screen.queryByTestId("display-browser-storage-box")).toBeNull();
    expect(screen.queryByTestId("footer-storage-box")).toBeNull();
  });

  it("should render the progress bar correctly", () => {
    render(
      <MockTheme>
        <StorageBox {...fullMock} />
      </MockTheme>,
    );

    const progress = screen.getByTestId("progress-contant-storage-box").children[0];
    expect(progress).toHaveStyle("transform: translateX(-96.62%)");
  });
});
