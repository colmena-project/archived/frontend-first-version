import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import HeaderPortraitControls from "@/components/pages/edit-audio/HeaderPortraitControls";
import { prettyDOM } from "@testing-library/dom";
import $ from "jquery";

const genericFn = jest.fn();

const genericProps = {
  handleImportAudio: genericFn,
  handleZoomIn: genericFn,
  handleZoomOut: genericFn,
  handleTrim: genericFn,
  handleCut: genericFn,
  handleSplit: genericFn,
  handleSelectAudioRegion: genericFn,
  handleDownload: genericFn,
  handleSave: genericFn,
  handleSaveOption: genericFn,
  handleModalDeleteConfirm: genericFn,
  handlePublish: genericFn,
};

describe("HeaderPortraitControls Component", () => {
  it("should return 1 button - context recorder or editing", () => {
    render(<HeaderPortraitControls {...genericProps} context="recorder" />);
    const element = screen.getAllByRole("button");
    expect(element).toHaveLength(1);
  });
  it("should open the context menu when button clicked", () => {
    render(<HeaderPortraitControls {...genericProps} context="editing" />);
    fireEvent.click(screen.getByTestId("context-menu-portrait-controls-btn"));
    const element = screen.getByTestId("context-menu-portrait-controls-menu");
    expect(element).toBeDefined();
  });
  it("should render 13 buttons in context menu - recorder context", () => {
    render(<HeaderPortraitControls {...genericProps} context="recorder" />);
    fireEvent.click(screen.getByTestId("context-menu-portrait-controls-btn"));
    const element = screen.getByRole("menu");
    expect(element.querySelectorAll("li").length).toBe(13);
  });
  it("should render 15 buttons in context menu - editing context", () => {
    render(<HeaderPortraitControls {...genericProps} context="editing" />);
    fireEvent.click(screen.getByTestId("context-menu-portrait-controls-btn"));
    const element = screen.getByRole("menu");
    expect(element.querySelectorAll("li").length).toBe(15);
  });
});
