import React, { useState } from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { TourProvider } from "@reactour/tour";

import user from "@/tests/__mocks__/data/user";
import wizard from "@/tests/__mocks__/data/wizard";
import EditAudio, { stepsHeaderLandscapeControls } from "@/pages/edit-audio/[id]";

const preloadState = { user, wizard };

const mockTourProvider = {
  steps: stepsHeaderLandscapeControls,
  maskClassName: "class-for-unit-test",
  defaultOpen: true,
};

const Wrapper = ({ children }: { children: React.ReactNode }) => (
  <TourProvider {...mockTourProvider}>{children}</TourProvider>
);

const useRouter = jest.spyOn(require("next/router"), "useRouter");

describe("EditAudio Page", () => {
  beforeEach(async () => {
    useRouter.mockImplementation(() => ({
      query: { id: "asc" },
      prefetch: jest.fn(() => null),
    }));
  });

  it("It should display the screen with the wizard open", async () => {
    const { container } = render(
      <Wrapper>
        <EditAudio />
      </Wrapper>,
      {
        preloadedState: preloadState,
      },
    );

    expect(container.getElementsByClassName("class-for-unit-test")[0]).toBeInTheDocument();
  });

  it.skip("should return 1 steps", async () => {
    const { container } = render(
      <Wrapper>
        <EditAudio />
      </Wrapper>,
      {
        preloadedState: preloadState,
      },
    );

    const buttons = Array.from(container.querySelectorAll("button"));
    const buttonsFiltered = buttons.filter((button) =>
      button.getAttribute("aria-label").match(/^Go to step/),
    );

    expect(buttonsFiltered.length).toEqual(1);
  });
});
