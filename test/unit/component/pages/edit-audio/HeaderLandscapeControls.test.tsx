import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen, fireEvent } from "@testing-library/react";
import HeaderLandscapeControls from "@/components/pages/edit-audio/HeaderLandscapeControls";
import theme from "@/styles/theme";
import { prettyDOM } from "@testing-library/dom";

const genericFn = jest.fn();

const genericProps = {
  handleImportAudio: genericFn,
  handleZoomIn: genericFn,
  handleZoomOut: genericFn,
  handleTrim: genericFn,
  handleSplit: genericFn,
  handleCut: genericFn,
  handleSelectAudioRegion: genericFn,
  handleDownload: genericFn,
  handleSave: genericFn,
  handleModalDeleteConfirm: genericFn,
  handleSaveOption: genericFn,
  handlePublish: genericFn,
};

describe("HeaderLandscapeControls Component", () => {
  it("should return 11 buttons - context recorder", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="recorder" />);
    const element = screen.getAllByRole("button");
    expect(element).toHaveLength(11);
  });
  it("should return 13 buttons - context editing", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="editing" />);
    const element = screen.getAllByRole("button");
    expect(element).toHaveLength(13);
  });

  it("should validate the active color selected when clicked - select button", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="recorder" />);
    fireEvent.click(screen.getByTestId("select-btn"));
    const element = screen.getByTestId("cursor_select");
    expect(element.getAttribute("color")).toEqual(theme.palette.secondary.main);
  });
  it("should validate the active color selected when clicked - cursor button", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="recorder" />);
    fireEvent.click(screen.getByTestId("cursor-btn"));
    const element = screen.getByTestId("headphone");
    expect(element.getAttribute("color")).toEqual(theme.palette.secondary.main);
  });
  it("should validate the active color selected when clicked - shift button", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="recorder" />);
    fireEvent.click(screen.getByTestId("shift-btn"));
    const element = screen.getByTestId("audio_shift");
    expect(element.getAttribute("color")).toEqual(theme.palette.secondary.main);
  });
  it("should validate the active color selected when clicked - fadein button", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="recorder" />);
    fireEvent.click(screen.getByTestId("fadein-btn"));
    const element = screen.getByTestId("fadeIn");
    expect(element.getAttribute("color")).toEqual(theme.palette.secondary.main);
  });
  it("should validate the active color selected when clicked - fadeout button", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="recorder" />);
    fireEvent.click(screen.getByTestId("fadeout-btn"));
    const element = screen.getByTestId("fadeOut");
    expect(element.getAttribute("color")).toEqual(theme.palette.secondary.main);
  });
});
