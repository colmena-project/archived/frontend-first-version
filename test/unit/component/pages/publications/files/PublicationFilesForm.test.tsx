import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import * as dbCalls from "@/store/idb/models/publications";

import user from "@/tests/__mocks__/data/user";
import PublicationFilesForm from "@/components/pages/publications/files/PublicationFilesForm";
import { act } from "react-dom/test-utils";
import { CurrentPublication, NewPublicationFile } from "@/interfaces/index";
import { publicationMock, fileMock } from "@/tests/__mocks__/data/publications";

const preloadState = { user };

describe("PublicationFilesForm component", () => {
  let file: NewPublicationFile;
  let mock: jest.SpyInstance<any, [id?: any]>;

  global.URL.createObjectURL = jest.fn();

  beforeEach(() => {
    mock = jest.spyOn(dbCalls, "getPublication").mockResolvedValue(publicationMock);

    file = fileMock;
  });

  afterEach(() => {
    mock.mockRestore();
  });

  it("should hide deleted files button if no have selecteds files", async () => {
    act(() => {
      render(
        <PublicationFilesForm
          isLoading={false}
          onSubmit={jest.fn()}
          publicationId={12}
          currentPublication={publicationMock as CurrentPublication}
          showLibraryModal={false}
        />,
        {
          preloadedState: preloadState,
        },
      );
    });

    await waitFor(() => {
      expect(screen.getByTestId("remove-files-button-wrapper")).not.toBeVisible();

      expect(screen.getByTestId("remove-files-button")).toBeDisabled();

      expect(screen.getByTestId("remove-files-button")).not.toBeVisible();
    });
  });

  it.skip("should show deleted files button if have some selected file", async () => {
    const publication = {
      ...publicationMock,
      files: [file],
    };

    act(() => {
      render(
        <PublicationFilesForm
          isLoading={false}
          onSubmit={jest.fn()}
          publicationId={12}
          currentPublication={publication as CurrentPublication}
          showLibraryModal={false}
        />,
        {
          preloadedState: preloadState,
        },
      );
    });

    const $checkbox = screen.getByTestId("input-checkbox-file");

    userEvent.click($checkbox);

    await waitFor(() => {
      const $wrapper = screen.getByTestId("files-wrapper");

      expect($wrapper.children.length).toBe(1);

      expect(screen.getByTestId("file-medatada-collapse")).toBeInTheDocument();

      expect(screen.getByTestId("remove-files-button-wrapper")).toBeInTheDocument();

      expect(screen.getByTestId("remove-files-button")).not.toBeDisabled();

      expect(screen.getByTestId("remove-files-button")).toBeVisible();
    });
  });

  it.skip("should remove selecteds files after click in remove files button", async () => {
    const publication = {
      ...publicationMock,
      files: [file],
    };

    act(() => {
      render(
        <PublicationFilesForm
          isLoading={false}
          onSubmit={jest.fn()}
          publicationId={12}
          currentPublication={publication as CurrentPublication}
          showLibraryModal={false}
        />,
        {
          preloadedState: preloadState,
        },
      );
    });

    const $checkbox = screen.getByTestId("input-checkbox-file");

    userEvent.click($checkbox);

    await waitFor(() => {
      fireEvent.click(screen.getByTestId("remove-files-button"));

      fireEvent.click(within(document.body).getByTestId("modal-confirm-button-ok"));

      const $wrapper = screen.getByTestId("files-wrapper");

      expect($wrapper.children.length).toBe(0);
    });
  });
});
