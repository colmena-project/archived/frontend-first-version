import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, screen } from "@testing-library/react";

import SharePublicationModal from "@/components/pages/publications/common/SharePublicationModal";
import user from "@/tests/__mocks__/data/user";

const preloadState = { user };

const genericProps = {
  title: "title",
  description: "it's just a description",
  baseUrl: "/",
  publicationId: "42",
};

describe("SharePublicationModal Component", () => {
  it("should render 4 social options when is visible", () => {
    const onClose = jest.fn();

    render(<SharePublicationModal {...genericProps} onClose={onClose} open={true} />, {
      preloadedState: preloadState,
    });

    expect(screen.getAllByRole("button")).toHaveLength(4);

    expect(screen.getAllByTestId("share-option")).toHaveLength(4);
  });
});
