import { useState } from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, screen, waitFor } from "@testing-library/react";

import SearchPublicationBox from "@/components/pages/publications/management/SearchPublicationBox";

const WrapperComponent: React.FC<{
  onClose: () => void;
  onChangeText: (keyword: string) => void;
  defaultShow: boolean;
}> = ({ onChangeText, onClose, defaultShow }) => {
  const [show, setShow] = useState(defaultShow);

  const toggleShow = () => setShow((o) => !o);

  return (
    <SearchPublicationBox
      onChangeText={onChangeText}
      onClose={() => {
        toggleShow();
        onClose();
      }}
      show={show}
      placeholder=""
    />
  );
};

describe("SearchPublicationBox Component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should shows search input box if show prop is true", async () => {
    render(
      <SearchPublicationBox onChangeText={jest.fn} onClose={jest.fn} show={true} placeholder="" />,
    );

    await waitFor(() => {
      expect(screen.getByTestId("search-publication-box")).toBeVisible();

      expect(screen.getByTestId("cancel-button")).toBeVisible();

      expect(screen.getByTestId("search-input")).toBeVisible();
    });
  });

  it("should hide search input box if show prop is false", async () => {
    render(
      <SearchPublicationBox onChangeText={jest.fn} onClose={jest.fn} show={false} placeholder="" />,
    );

    await waitFor(() => {
      expect(screen.getByTestId("search-publication-box")).not.toBeVisible();

      expect(screen.getByTestId("cancel-button")).not.toBeVisible();

      expect(screen.getByTestId("search-input")).not.toBeVisible();
    });
  });

  it.skip("should call onClose after click in cancel button", async () => {
    const onClose = jest.fn();
    const onChangeText = jest.fn();

    render(<WrapperComponent onChangeText={onChangeText} onClose={onClose} defaultShow={true} />);

    await waitFor(() => {
      expect(screen.getByTestId("search-publication-box")).toBeVisible();
    });

    fireEvent.click(screen.getByTestId("cancel-button"));

    await waitFor(() => {
      expect(screen.getByTestId("search-publication-box")).not.toBeVisible();
    });
  });
});
