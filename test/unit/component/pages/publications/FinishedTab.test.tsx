import { useState } from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { mockAllIsIntersecting } from "react-intersection-observer/test-utils";

import FinishedTab from "@/components/pages/publications/management/FinishedTab";
import user from "@/tests/__mocks__/data/user";
import { GET_PUBLICATIONS_BY_ADMIN } from "@/services/cms/publications";

const mockedResponse = {
  request: {
    query: GET_PUBLICATIONS_BY_ADMIN,
    variables: {
      publisher: "devteam",
      keyword: "",
      order: "createdAt:desc",
      page: 1,
      pageSize: 10,
    },
  },

  result: {
    data: {
      publications: {
        data: [],
        meta: {
          pagination: {
            page: 1,
            pageSize: 10,
            pageCount: 6,
            total: 56,
            __typename: "Pagination",
          },
          __typename: "ResponseCollectionMeta",
        },
        __typename: "PublicationEntityResponseCollection",
      },
    },
  },
};

const WrapperComponent: React.FC<{
  onCloseFilters: () => void;
  onCloseSearch: () => void;
}> = ({ onCloseFilters, onCloseSearch }) => {
  const [show, setShow] = useState(false);

  const toggleShow = () => setShow((o) => !o);

  return (
    <>
      <button data-testid="button-test" onClick={toggleShow}>
        test
      </button>

      <FinishedTab
        onCloseFilters={onCloseFilters}
        onCloseSearch={onCloseSearch}
        openSearch={show}
        user={user.user as any}
        openFilters={false}
      />
    </>
  );
};

describe("FinishedTab Component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should shows search input box after click in button", async () => {
    mockAllIsIntersecting(true);

    render(<WrapperComponent onCloseFilters={jest.fn} onCloseSearch={jest.fn} />);

    await waitFor(() => expect(screen.getByTestId("search-publication-box")).not.toBeVisible());

    fireEvent.click(screen.getByTestId("button-test"));

    await waitFor(() => expect(screen.getByTestId("search-publication-box")).toBeVisible());
  });

  it("should shows empty publication if publications list is empty", async () => {
    mockAllIsIntersecting(true);

    render(<WrapperComponent onCloseFilters={jest.fn} onCloseSearch={jest.fn} />, {
      customMock: [mockedResponse],
    });

    fireEvent.click(screen.getByTestId("button-test"));

    await waitFor(() => expect(screen.getByTestId("no-publication")).toBeVisible());
  });

  it("should hide empty publication if publications list is not empty", async () => {
    mockAllIsIntersecting(true);

    render(<WrapperComponent onCloseFilters={jest.fn} onCloseSearch={jest.fn} />, {
      customMock: [],
    });

    await waitFor(() => expect(screen.getByTestId("no-publication")).not.toBeVisible());
  });
});
