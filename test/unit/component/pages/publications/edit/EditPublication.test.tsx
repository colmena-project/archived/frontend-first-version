import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, waitFor, screen, act } from "@testing-library/react";

import user, { userColaborator } from "@/tests/__mocks__/data/user";
import EditPublication from "@/components/pages/publications/edit/EditPublication";
import * as exporteds from "@/hooks/cms/useUpdatePublication";

import {
  mockGetCategoriesValues,
  mockGetPublicationResponse,
} from "@/tests/__mocks__/server/graphql/cms";
import { PublicationQueryInterface } from "@/interfaces/cms";
const preloadStateAdmin = { user };
const preloadStateColaborator = { user: userColaborator };

const publication = mockGetPublicationResponse.publication.data;

const useRouter = jest.spyOn(require("next/router"), "useRouter");

const useUpdatePublication = jest.spyOn(exporteds, "useUpdatePublication");

jest.mock("@/hooks/useUpdatePublicationFilesWorker", () => {
  return jest.fn(() => ({
    runWorker: jest.fn(),
  }));
});

describe("EditPublication Component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should redirect user if he doesn't have admin role", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublication
        publicationId="105"
        publication={publication as unknown as PublicationQueryInterface}
        categories={mockGetCategoriesValues}
      />,
      {
        preloadedState: preloadStateColaborator,
      },
    );

    await waitFor(() => {
      expect(replaceMock).toBeCalledWith("/publications");
    });
  });

  it("should don't redirect user if he haves admin role", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublication
        publicationId="105"
        publication={publication as unknown as PublicationQueryInterface}
        categories={mockGetCategoriesValues}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    await waitFor(() => {
      expect(replaceMock).not.toBeCalled();
    });
  });

  it("should show options save, save and finish if change", async () => {
    useRouter.mockReturnValue({
      push: jest.fn(),
      replace: jest.fn(),
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublication
        publicationId="105"
        publication={publication as unknown as PublicationQueryInterface}
        categories={mockGetCategoriesValues}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    fireEvent.change(screen.getByTestId("input-title").querySelector("input") as HTMLInputElement, {
      target: { value: "test" },
    });

    fireEvent.blur(screen.getByTestId("input-title").querySelector("input") as HTMLInputElement);

    fireEvent.click(screen.getByTestId("next-button"));

    await waitFor(() => {
      expect(screen.getAllByTestId("tools-menu-option")).toHaveLength(2);
    });
  });

  it("should save and finish publciation", async () => {
    useRouter.mockReturnValue({
      push: jest.fn(),
      replace: jest.fn(),
      prefetch: jest.fn(() => null),
    });

    const update = jest.fn();

    useUpdatePublication.mockReturnValue({
      update,
      updating: false,
      response: null,
      reset: jest.fn(),
      error: undefined,
    });

    await act(() => {
      render(
        <EditPublication
          publicationId="105"
          publication={publication as unknown as PublicationQueryInterface}
          categories={mockGetCategoriesValues}
        />,
        {
          preloadedState: preloadStateAdmin,
        },
      );
    });

    fireEvent.change(screen.getByTestId("input-title").querySelector("input") as HTMLInputElement, {
      target: { value: "test" },
    });

    fireEvent.blur(screen.getByTestId("input-title").querySelector("input") as HTMLInputElement);

    fireEvent.click(screen.getByTestId("next-button"));

    await waitFor(() => {
      fireEvent.click(screen.getAllByTestId("tools-menu-option")[1]);

      expect(update).toHaveBeenCalled();
    });
  });
});
