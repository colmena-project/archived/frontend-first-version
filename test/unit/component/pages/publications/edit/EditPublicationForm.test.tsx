import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { act, fireEvent, screen, waitFor } from "@testing-library/react";

import user from "@/tests/__mocks__/data/user";
import EditPublicationForm from "@/components/pages/publications/edit/EditPublicationForm";
import {
  mockGetCategoriesValues,
  mockGetPublicationResponse,
} from "@/tests/__mocks__/server/graphql/cms";

const preloadStateAdmin = { user };

const useRouter = jest.spyOn(require("next/router"), "useRouter");

const publication = mockGetPublicationResponse.publication.data;

describe("EditPublicationForm Component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should render title, description, category, license, content editor and date of publication fields", async () => {
    useRouter.mockReturnValue({ push: jest.fn(), replace: jest.fn() });

    const mockRef = { current: document.createElement("button") };

    render(
      <EditPublicationForm
        submitButtonRef={mockRef}
        categories={mockGetCategoriesValues}
        onNext={jest.fn()}
        language="en"
        publication={publication as any}
        userId={user.user.id}
        publicationImageUrl={publication.attributes.relation.data.attributes.url}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    await waitFor(() => {
      expect(screen.getByTestId("input-title")).toBeVisible();
      expect(screen.getByTestId("input-description")).toBeVisible();
      expect(screen.getByTestId("content-editor")).toBeVisible();
      expect(screen.getByTestId("select-category")).toBeVisible();
      expect(screen.getByTestId("select-license")).toBeVisible();
      expect(screen.getByTestId("select-date")).toBeVisible();
    });
  });

  test("title, description, category, license, and date of publication fields should be required", async () => {
    useRouter.mockReturnValue({ push: jest.fn(), replace: jest.fn() });

    const mockRef = { current: document.createElement("button") };

    const onSubmit = jest.fn();

    const { container } = render(
      <EditPublicationForm
        submitButtonRef={mockRef}
        categories={mockGetCategoriesValues}
        onNext={onSubmit}
        language="en"
        publication={publication as any}
        userId={user.user.id}
        publicationImageUrl={publication.attributes.relation.data.attributes.url}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    await waitFor(() => {
      const $errorsAlerts = container.getElementsByClassName("Mui-error");

      expect($errorsAlerts.length).toBe(0);
    });

    fireEvent.change(screen.getByTestId("input-title").querySelector("input") as HTMLInputElement, {
      target: { value: "" },
    });

    fireEvent.blur(screen.getByTestId("input-title").querySelector("input") as HTMLInputElement);

    await waitFor(() => {
      const $errorsAlerts = container.getElementsByClassName("Mui-error");

      expect($errorsAlerts.length).toBe(4);
    });
  });
});
