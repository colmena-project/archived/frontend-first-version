import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { waitFor, screen, fireEvent } from "@testing-library/react";

import user from "@/tests/__mocks__/data/user";

import { mockGetPublicationFiles } from "@/tests/__mocks__/server/graphql/cms";
import EditPublicationFiles from "@/components/pages/publications/edit/EditPublicationFiles";
import * as updateFileExports from "@/hooks/cms/useUpdatePublicationFile";
import * as deleteFile from "@/hooks/cms/useDeletePublicationFile";
import { fileMock } from "@/tests/__mocks__/data/publications";

const preloadStateAdmin = { user };

const useRouter = jest.spyOn(require("next/router"), "useRouter");

jest.spyOn(updateFileExports, "useUpdatePublicationFile");
jest.spyOn(deleteFile, "useDeletePublicationFile");

jest.mock("@/hooks/useUpdatePublicationFilesWorker", () => {
  return jest.fn(() => ({
    runWorker: jest.fn(),
  }));
});

jest.mock("@/hooks/cms/usePublicationFiles", () => {
  return jest.fn(() => ({
    loading: false,
    publicationFiles: mockGetPublicationFiles,
    refetch: jest.fn(),
  }));
});

jest.mock("@/hooks/useFileInLibrary", () => {
  return jest.fn(() => ({
    fetchFile: jest.fn(),
    fetching: false,
  }));
});

describe("EditPublicationFiles Component", () => {
  beforeEach(() => {
    window.URL.createObjectURL = jest.fn();
  });

  afterAll(() => {
    (window.URL.createObjectURL as any).mockReset();
    jest.clearAllMocks();
  });

  it("should render files items", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublicationFiles
        publicationId="105"
        localFiles={[]}
        setLocalFiles={jest.fn()}
        userId={user.user.id}
        files={[fileMock]}
        setFiles={jest.fn()}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    await waitFor(() => {
      expect(screen.getByTestId("file-edit-item")).toBeDefined();
    });
  });

  it("should render collapse item", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublicationFiles
        publicationId="105"
        localFiles={[]}
        setLocalFiles={jest.fn()}
        userId={user.user.id}
        files={[fileMock]}
        setFiles={jest.fn()}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    await waitFor(() => {
      expect(screen.getByTestId("file-medatada-collapse")).toBeInTheDocument();
      expect(screen.getByTestId("view-metadata")).toBeDefined();
    });
  });

  it("should show metadata info when click view metadata", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublicationFiles
        publicationId="105"
        localFiles={[]}
        setLocalFiles={jest.fn()}
        userId={user.user.id}
        files={[fileMock]}
        setFiles={jest.fn()}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    fireEvent.click(screen.getByTestId("view-details"));
    fireEvent.click(screen.getByTestId("view-metadata"));

    await waitFor(() => {
      expect(screen.getByTestId("file-metadata")).toBeDefined();
    });
  });

  it("should hide metadata info when click hide metadata button", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublicationFiles
        publicationId="105"
        localFiles={[]}
        setLocalFiles={jest.fn()}
        userId={user.user.id}
        files={[fileMock]}
        setFiles={jest.fn()}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    // show
    fireEvent.click(screen.getByTestId("view-details"));

    // hide
    fireEvent.click(screen.getByTestId("view-details"));

    await waitFor(() => {
      expect(screen.getByTestId("file-metadata")).not.toBeVisible();
    });
  });

  it("should show edit title modal", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublicationFiles
        publicationId="105"
        localFiles={[]}
        setLocalFiles={jest.fn()}
        userId={user.user.id}
        files={[fileMock]}
        setFiles={jest.fn()}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    fireEvent.click(screen.getByTestId("view-details"));
    fireEvent.click(screen.getAllByTestId("metadata-line")[0]);

    await waitFor(() => {
      expect(screen.getByTestId("edit-field-modal")).toBeDefined();
    });
  });

  it("should remove item after click remove", async () => {
    const pushMock = jest.fn();
    const replaceMock = jest.fn();

    const setFiles = jest.fn();

    useRouter.mockReturnValue({
      push: pushMock,
      replace: replaceMock,
      prefetch: jest.fn(() => null),
    });

    render(
      <EditPublicationFiles
        publicationId="105"
        localFiles={[]}
        setLocalFiles={jest.fn()}
        userId={user.user.id}
        files={[fileMock]}
        setFiles={setFiles}
      />,
      {
        preloadedState: preloadStateAdmin,
      },
    );

    fireEvent.click(screen.getByTestId("view-details"));
    fireEvent.click(screen.getByTestId("remove-file-button"));

    await waitFor(() => {
      expect(setFiles).toBeCalled();
    });
  });
});
