import React from "react";

import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { TourProvider } from "@reactour/tour";
import Honeycomb, { steps } from "@/pages/honeycomb";
import * as nextRouter from "next/router";

import user from "@/tests/__mocks__/data/user";
import wizard from "@/tests/__mocks__/data/wizard";

const preloadState = { user, wizard };

const mockTourProvider = {
  steps: steps,
  maskClassName: "class-for-unit-test",
  defaultOpen: true,
};

const Wrapper = ({ children }: { children: React.ReactNode }) => (
  <TourProvider {...mockTourProvider}>{children}</TourProvider>
);

const useRouter = jest.spyOn(require("next/router"), "useRouter");

describe("Honeycomb Page", () => {
  beforeEach(async () => {
    useRouter.mockImplementation(() => ({
      route: "/",
      prefetch: jest.fn(() => null),
    }));
  });
  it("It should display the screen with the wizard open", async () => {
    const { container } = render(
      <Wrapper>
        <Honeycomb />
      </Wrapper>,
      {
        preloadedState: preloadState,
      },
    );

    expect(container.getElementsByClassName("class-for-unit-test")[0]).toBeInTheDocument();
  });

  it("should return 3 steps", async () => {
    const { container } = render(
      <Wrapper>
        <Honeycomb />
      </Wrapper>,
      {
        preloadedState: preloadState,
      },
    );

    const buttons = Array.from(container.querySelectorAll("button"));
    const buttonsFiltered = buttons.filter((button) =>
      button.getAttribute("aria-label").match(/^Go to step/),
    );
    expect(buttonsFiltered.length).toEqual(3);
  });
});
