import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import rooms from "@/tests/__mocks__/data/room/rooms";
import user from "@/tests/__mocks__/data/user";
import HoneycombListActive from "@/components/pages/honeycomb/HoneycombListActive";
import { mockAllIsIntersecting } from "react-intersection-observer/test-utils";
import HoneycombListItem from "@/components/pages/honeycomb/HoneycombListItem";
import HoneycombList from "@/components/pages/honeycomb/HoneycombList";
import HeaderOptions from "@/components/pages/honeycomb/HeaderOptions";

const preloadState = { user, honeycomb: { honeycombs: rooms, honeycombsArchived: [] } };

const isConnected = false;

describe("Honeycomb offline mode", () => {
  it("should hidden search button when the connection is offline", async () => {
    mockAllIsIntersecting(true);
    render(<HeaderOptions handleOpenSearch={jest.fn()} />, {
      preloadedState: preloadState,
      isConnected,
    });
    const filterButton = screen.queryByTestId("honeycomb-search-header");
    expect(filterButton).toBeNull();
  });

  it("should hidden change directory button when the connection is offline", async () => {
    mockAllIsIntersecting(true);
    render(<HoneycombListActive data={rooms} key="list" />, {
      preloadedState: preloadState,
      isConnected,
    });
    await waitFor(() => {
      const filterButton = screen.queryByTestId("toolbar-click");
      expect(filterButton).toBeNull();
    });
  });

  it("should hidden unread message indicator when the connection is offline", async () => {
    render(<HoneycombListItem data={rooms[0]} archived={false} />, {
      preloadedState: preloadState,
      isConnected,
    });
    await waitFor(() => {
      const unreadIndicator = screen.queryByTestId("unread-message-indicator");
      expect(unreadIndicator).toBeNull();
    });
  });

  it("should hidden open more options button when the connection is offline", async () => {
    render(<HoneycombList data={rooms} />, { preloadedState: preloadState, isConnected });
    fireEvent.click(screen.getByTestId("tools-menu"));
    await waitFor(() => {
      const moreOptionsButton = screen.queryByTestId("more_vertical_icon");
      expect(moreOptionsButton).toBeNull();
    });
  });
});
