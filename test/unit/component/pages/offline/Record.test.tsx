import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { screen } from "@testing-library/react";
import ChangeFolderAudioSaved from "@/components/ui/WaveformPlaylist/ChangeFolderAudioSaved";
import user from "@/tests/__mocks__/data/user";
import HeaderLandscapeControls from "@/components/pages/edit-audio/HeaderLandscapeControls";
import HeaderPortraitControls from "@/components/pages/edit-audio/HeaderPortraitControls";

const genericFn = jest.fn();

const genericProps = {
  handleImportAudio: genericFn,
  handleZoomIn: genericFn,
  handleZoomOut: genericFn,
  handleTrim: genericFn,
  handleSplit: genericFn,
  handleCut: genericFn,
  handleSelectAudioRegion: genericFn,
  handleDownload: genericFn,
  handleSave: genericFn,
  handleModalDeleteConfirm: genericFn,
  handleSaveOption: genericFn,
  handlePublish: genericFn,
};

describe("Record offline mode", () => {
  it("should hidden change directory button when the connection is offline", () => {
    render(<ChangeFolderAudioSaved />, { isConnected: false });
    const changeDirectoryButton = screen.queryByTestId("change-directory");
    expect(changeDirectoryButton).toBeNull();
  });

  it("should disable import audio button when the connection is offline at landscape position", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="editing" />, {
      isConnected: false,
    });
    const importAudioButton = screen.getByTestId("import-audio");
    expect(importAudioButton).toBeDisabled();
  });

  it("should disable import audio button when the connection is offline at portrait position", () => {
    render(<HeaderPortraitControls {...genericProps} context="editing" />, { isConnected: false });
    const importAudioButton = screen.getByTestId("import-audio");
    expect(importAudioButton).toHaveAttribute("aria-disabled", "true");
  });

  it("should disable publish audio button when the connection is offline at landscape position", () => {
    render(<HeaderLandscapeControls {...genericProps} cursorState="cursor" context="editing" />, {
      isConnected: false,
    });
    const importAudioButton = screen.getByTestId("publish-btn");
    expect(importAudioButton).toBeDisabled();
  });

  it("should disable publish audio button when the connection is offline at portrait position", () => {
    render(<HeaderPortraitControls {...genericProps} context="editing" />, { isConnected: false });
    const importAudioButton = screen.getByTestId("publish-btn");
    expect(importAudioButton).toHaveAttribute("aria-disabled", "true");
  });
});
