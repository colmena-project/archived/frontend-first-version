import React from "react";
import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import rooms from "@/tests/__mocks__/data/room/rooms";
import room from "@/tests/__mocks__/data/room";
import user from "@/tests/__mocks__/data/user";
import ChatTitle from "@/components/pages/honeycomb/Chat/ChatTitle";
import Subtitle from "@/components/pages/honeycomb/Subtitle";
import ChatHeaderOptions from "@/components/pages/honeycomb/Chat/ChatHeaderOptions";
import HoneycombChatToolsMenu from "@/components/ui/ToolsMenu/Contexts/HoneycombChatToolsMenu";
import { ChatMemoized } from "@/components/pages/honeycomb/Chat";
import { RoomTypeEnum } from "@/enums/*";

const preloadState = { user, honeycomb: { honeycombs: rooms, honeycombsArchived: [] } };

const isConnected = false;

describe("Honeycomb Chat offline mode", () => {
  it("should hidden info button when the connection is offline", () => {
    render(<ChatTitle room={room} />, {
      preloadedState: preloadState,
      isConnected,
    });
    const button = screen.queryByTestId("honeycomb-more-info");
    expect(button).toBeNull();
  });

  it("should hidden subtitle when the connection is offline", () => {
    render(<Subtitle token={room.token} />, {
      preloadedState: preloadState,
      isConnected,
    });
    const subtitle = screen.queryByTestId("honeycomb-subtitle");
    expect(subtitle).toBeNull();
  });

  it("should hidden conference button when the connection is offline", () => {
    render(
      <ChatHeaderOptions
        displayName={room.displayName}
        token={room.token}
        room={room}
        canDeleteConversation={1}
        handleLeaveRoom={jest.fn()}
        handleJoinCall={jest.fn()}
      />,
      {
        preloadedState: preloadState,
        isConnected,
      },
    );
    const conferenceButton = screen.queryByTestId("honeycomb-conference");
    expect(conferenceButton).toBeNull();
  });

  it("should hidden context menu when the connection is offline", () => {
    render(
      <ChatHeaderOptions
        displayName={room.displayName}
        token={room.token}
        room={room}
        canDeleteConversation={1}
        handleLeaveRoom={jest.fn()}
        handleJoinCall={jest.fn()}
      />,
      {
        preloadedState: preloadState,
        isConnected,
      },
    );
    const contextMenu = screen.queryByTestId("honeycomb-context-menu");
    expect(contextMenu).toBeNull();
  });

  it("should disable tools menu options when the connection is offline", async () => {
    render(<HoneycombChatToolsMenu handleJoinCall={jest.fn()} />, {
      preloadedState: preloadState,
      isConnected,
    });
    fireEvent.click(screen.getByTestId("tools-menu"));
    await waitFor(() => {
      const uploadButton = screen.getByTestId("tools-menu-honeycomb-upload");
      expect(uploadButton).toBeDisabled();

      const editTextButton = screen.getByTestId("tools-menu-honeycomb-edit-text");
      expect(editTextButton).toBeDisabled();

      const recorderButton = screen.getByTestId("tools-menu-honeycomb-recorder");
      expect(recorderButton).toBeDisabled();
    });
  });

  it("should show unavailable content error when the connection is offline", async () => {
    render(
      <ChatMemoized
        canDeleteConversation={1}
        token={room.token}
        displayName={room.displayName}
        roomType={RoomTypeEnum.CONVERSATION}
      />,
      {
        preloadedState: preloadState,
        isConnected,
      },
    );
    fireEvent.click(screen.getByTestId("honeycomb-chat-library"));
    await waitFor(() => {
      const error = screen.getByTestId("unavailable-content-error");
      expect(error).toBeVisible();
    });
  });
});
