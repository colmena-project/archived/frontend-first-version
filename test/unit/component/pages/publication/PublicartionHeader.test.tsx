import { renderWithProviders as render } from "@/tests/utils/testUtils";
import { fireEvent, screen } from "@testing-library/react";

import PublicationHeader from "@/components/pages/publication/PublicationHeader";
import user from "@/tests/__mocks__/data/user";

const preloadState = { user };

const genericProps = {
  publisher: "unit",
  categoryName: "it`s a category",
  creator: "colmena",
  publicationTitle: "title",
  publicationDescription: "it's just a description",
  baseUrl: "/",
  publicationId: "42",
  showShareButton: true,
  showBreadcrumbs: false,
};

describe("PublicationHeader Component", () => {
  it("should render the share button in the PublicationHeader", () => {
    render(<PublicationHeader {...genericProps} />, {
      preloadedState: preloadState,
    });

    const shareIconButton = screen.getByTestId("share-icon-button");

    expect(shareIconButton).toBeVisible();
  });

  it("should shows up modal after click in share button", () => {
    render(<PublicationHeader {...genericProps} />, {
      preloadedState: preloadState,
    });

    const shareIconButton = screen.getByTestId("share-icon-button");

    fireEvent.click(shareIconButton);

    const shareModal = screen.getByTestId("share-modal-items-wrapper");

    expect(shareModal).toBeVisible();
  });

  it("should hide modal after click on backdrop", async () => {
    render(<PublicationHeader {...genericProps} />, {
      preloadedState: preloadState,
    });

    const shareIconButton = screen.getByTestId("share-icon-button");

    fireEvent.click(shareIconButton);

    expect(screen.getByTestId("share-modal-items-wrapper")).toBeVisible();

    expect(screen.getAllByTestId("share-option")).toHaveLength(4);

    fireEvent.click(shareIconButton);

    expect(screen.getByTestId("share-modal-items-wrapper")).not.toBeVisible();
  });
});
