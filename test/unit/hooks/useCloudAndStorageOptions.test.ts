import { renderHook } from "@testing-library/react-hooks";
import { waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";

import { useCloudAndStorageOptions } from "@/hooks/useCloudAndStorageOptions";

import { deleteAllMessages } from "@/store/idb/models/chat";
import { removeAllFiles } from "@/store/idb/models/files";
import { removeAllFilesQuick } from "@/store/idb/models/filesQuickBlob";

jest.mock("@/store/idb/models/chat", () => ({
  deleteAllMessages: jest.fn(),
}));

jest.mock("@/store/idb/models/files", () => ({
  removeAllFiles: jest.fn(),
}));

jest.mock("@/store/idb/models/filesQuickBlob", () => ({
  removeAllFilesQuick: jest.fn(),
}));

describe("useCloudAndStorageOptions", () => {
  it("Should return default values", () => {
    const { result } = renderHook(() => useCloudAndStorageOptions());

    expect(typeof result.current.handleCleanLocalStorage).toBe("function");
    expect(typeof result.current.confirmCleanLocalStorage).toBe("function");
    expect(typeof result.current.onClose).toBe("function");

    expect(result.current.isOpen).toBe(false);
    expect(result.current.showBackdrop).toBe(false);
    expect(result.current.isLoading).toBe(false);
  });

  it("Should reset the values", () => {
    const { result } = renderHook(() => useCloudAndStorageOptions());

    act(() => {
      result.current.onClose();
    });

    expect(result.current.isOpen).toBe(false);
    expect(result.current.showBackdrop).toBe(false);
    expect(result.current.isLoading).toBe(false);
  });

  it("Should show modal", () => {
    const { result } = renderHook(() => useCloudAndStorageOptions());

    act(() => {
      result.current.handleCleanLocalStorage();
    });

    expect(result.current.isOpen).toBe(true);
  });

  it("should confirm action", async () => {
    const { result } = renderHook(() => useCloudAndStorageOptions());

    act(() => {
      result.current.confirmCleanLocalStorage();
    });

    await waitFor(() => {
      expect(deleteAllMessages).toHaveBeenCalled();
      expect(removeAllFiles).toHaveBeenCalled();
      expect(removeAllFilesQuick).toHaveBeenCalled();
    });

    await waitFor(() => {
      expect(result.current.isOpen).toBe(false);
      expect(result.current.showBackdrop).toBe(false);
      expect(result.current.isLoading).toBe(false);
    });
  });
});
