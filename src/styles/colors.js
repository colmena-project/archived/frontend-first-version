module.exports = {
  primary: {
    50: "#ced2f2",
    100: "#b8bce5",
    200: "#999ecc",
    300: "#7d82b2",
    400: "#636999",
    500: "#4d5280",
    600: "#333866",
    700: "#1f234d",
    800: "#0f1333",
    900: "#05071a",
    main: "#4d5280",
    light: "#c9cbd9",
    dark: "#333762",
    contrastText: "#fff",
  },

  secondary: {
    50: "#ffe2b2",
    100: "#fcd697",
    200: "#faca7d",
    300: "#f7be63",
    400: "#f5b349",
    500: "#f39b10",
    600: "#e59109",
    700: "#d98707",
    800: "#cc7f04",
    900: "#bf7702",
    main: "#f39b10",
    light: "#fbe1b7",
    dark: "#ed7f09",
    contrastText: "#fff",
  },

  // ciano
  variation1: {
    light: "#b3e7e7",
    main: "#00aeaf",
    dark: "#009496",
    contrastText: "#fff",
  },

  // purple
  variation2: {
    light: "#5B2DAC",
    main: "#8E6BC5",
    dark: "#5B2DAC",
    contrastText: "#fff",
  },

  // orange
  variation3: {
    light: "#fccec4",
    main: "#F65B3A",
    dark: "#f24025",
    contrastText: "#fff",
  },

  // light green
  variation4: {
    light: "#cdf3dd",
    main: "#57D88F",
    dark: "#3cc872",
    contrastText: "#fff",
  },

  // dark blue
  variation5: {
    light: "#c0c3c5",
    main: "#2c363e",
    dark: "#1b2228",
    contrastText: "#fff",
  },

  // dark blue variation
  variation6: {
    light: "#c2c4c6",
    main: "#343a40",
    dark: "#21252a",
    contrastText: "#fff",
  },
  // dark blue variation 2
  variation7: {
    light: "#c2c3d0",
    main: "#333762",
    dark: "#202346",
    contrastText: "#fff",
  },

  variation8: {
    light: "#fbe1b7",
    main: "#4F4F4F",
    dark: "#ed7f09",
    contrastText: "#fff",
  },

  gray: {
    light: "#F4F4F4",
    main: "#9A9A9A",
    dark: "#575757",
  },

  icon: {
    light: "#c9cbd9",
    main: "#4c517f",
    dark: "#333762",
    contrastText: "#fff",
  },

  danger: {
    light: "#f94d4d",
    main: "#d10000",
    dark: "#ad0000",
  },
};
