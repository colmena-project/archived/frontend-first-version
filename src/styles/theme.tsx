import { currentDirection } from "@/utils/i18n";
import { createTheme, responsiveFontSizes } from "@material-ui/core/styles";

import colors from "./colors";

export type AllPaletteColors =
  | "primary"
  | "secondary"
  | "variation1"
  | "variation2"
  | "variation3"
  | "variation4"
  | "variation5"
  | "variation6"
  | "variation7"
  | "variation8"
  | "gray"
  | "icon"
  | "danger";

declare module "@material-ui/core/styles/createPalette" {
  interface PaletteColor {
    50?: string;
    100?: string;
    200?: string;
    300?: string;
    400?: string;
    500?: string;
    600?: string;
    700?: string;
    800?: string;
    900?: string;
  }

  interface Palette {
    variation1: Palette["primary"];
    variation2: Palette["primary"];
    variation3: Palette["primary"];
    variation4: Palette["primary"];
    variation5: Palette["primary"];
    variation6: Palette["primary"];
    variation7: Palette["primary"];
    variation8: Palette["primary"];
    gray: Palette["primary"];
    icon: Palette["primary"];
    danger: Palette["primary"];
  }
  interface PaletteOptions {
    variation1: PaletteOptions["primary"];
    variation2: PaletteOptions["primary"];
    variation3: PaletteOptions["primary"];
    variation4: PaletteOptions["primary"];
    variation5: PaletteOptions["primary"];
    variation6: PaletteOptions["primary"];
    variation7: PaletteOptions["primary"];
    variation8: PaletteOptions["primary"];
    gray: PaletteOptions["primary"];
    icon: PaletteOptions["primary"];
    danger: PaletteOptions["primary"];
  }
}

const theme = createTheme({
  typography: {
    fontFamily: "Nunito, Nunito Sans, Open Sans, sans-serif",
  },
  palette: {
    ...colors,
  },
  direction: currentDirection(),
});

const themeRelease = responsiveFontSizes(theme);

export default themeRelease;
