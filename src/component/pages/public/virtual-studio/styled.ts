import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

export const Base = styled(Box)({
  display: "flex",
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
  justifyContent: "center",
});

