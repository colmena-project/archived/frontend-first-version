import React from "react";
import Button from "@/components/ui/Button";
import { Formik, Field, FieldProps } from "formik";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import Divider from "@/components/ui/Divider";
import * as Yup from "yup";
import { ButtonVariantEnum, SelectVariantEnum } from "@/enums/*";
import { useTranslation } from "next-i18next";
import Modal from "@/components/ui/Modal";
import TextField from "@material-ui/core/TextField";
import { MyForm, ButtonsContainer } from "./styled";

type Props = {
  open: boolean;
  handleClose: () => void;
  handleSubmit: (str: string) => void
};

type MyFormValues = {
  name: string;
};

export default function FormGuestInfo({ open, handleClose, handleSubmit }: Props) {
  const { t: c } = useTranslation("common");
  const { t } = useTranslation("virtualStudio");

  const initialValues = {
    name: "",
  };

  const schemaValidation = Yup.object().shape({
    name: Yup.string()
      .min(3, c("form.passwordMinLengthTitle", { size: 6 }))
      .max(50, c("form.passwordMaxLengthTitle", { size: 30 }))
      .required(c("form.requiredTitle")),
  });

  return (
    <>
      <Modal title={t("form.title")} handleClose={handleClose} open={open}>
        <Formik
          initialValues={initialValues}
          validationSchema={schemaValidation}
          onSubmit={(values: MyFormValues) => {
            const { name } = values;
            handleSubmit(name);
          }}
        >
          {({ submitForm, errors, touched }: any) => (
            <MyForm
              autoComplete="off"
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  submitForm();
                }
              }}
            >
              <Field name="name" data-testid="guest-name" InputProps={{ notched: true }}>
                {({ field }: FieldProps) => (
                  <TextField
                    id="name"
                    inputProps={{
                      autoComplete: "off",
                      form: {
                        autoComplete: "off",
                      },
                    }}
                    variant={SelectVariantEnum.OUTLINED}
                    required
                    fullWidth
                    {...field}
                  />
                )}
              </Field>
              {errors.name && touched.name ? (
                <ErrorMessageForm message={errors.name} />
              ) : null}
              <Divider marginTop={20} />
              <ButtonsContainer>
                <Button
                  handleClick={handleClose}
                  variant={ButtonVariantEnum.OUTLINED}
                  title={c("cancel")}
                  data-testid="cancel-guest-name"
                />
                <Button
                  handleClick={submitForm}
                  variant={ButtonVariantEnum.CONTAINED}
                  style={{ float: "right" }}
                  title={t("form.submitButtonTitle")}
                  data-testid="submit-guest-name"
                />
              </ButtonsContainer>
            </MyForm>
          )}
        </Formik>
      </Modal>
    </>
  );
}
