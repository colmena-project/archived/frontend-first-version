import { Form } from "formik";
import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

export const MyForm = styled(Form)({
  "& .MuiTextField-root": {
      width: "100%",
    },
});

export const ButtonsContainer = styled(Box)({
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
})

