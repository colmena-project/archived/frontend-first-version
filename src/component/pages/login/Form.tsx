/* eslint-disable camelcase */
import React from "react";
import Button from "@/components/ui/Button";
import { InputAdornment, OutlinedInput, FormControl, InputLabel } from "@material-ui/core";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import PasswordField from "@/components/statefull/PasswordField";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { toast } from "@/utils/notifications";
import { Formik, Form, Field, FieldProps } from "formik";
import Divider from "@/components/ui/Divider";
import { ButtonVariantEnum, AuthenticationErrorEnum } from "@/enums/index";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import * as Yup from "yup";
import { signIn, getSession } from "next-auth/client";
import { setCookie } from "nookies";
import { useDispatch } from "react-redux";
import { userInfoUpdate } from "@/store/actions/users/index";
import { UserInfoInterface } from "@/interfaces/index";
import { GroupInterface } from "@/interfaces/keycloak";
import Box from "@material-ui/core/Box";
import { v4 as uuid } from "uuid";
import { makeStyles } from "@material-ui/core/styles";
import Backdrop from "@/components/ui/Backdrop";
import { trailingSlash } from "@/utils/utils";
import { show } from "@/services/internal/groups";
import useUsernameLocalStorage from "@/hooks/useUsernameLocalStorage";

type MyFormValues = {
  emlLogin: string;
  psdLogin: string;
};

export default function WrapperForm() {
  const { setUsername } = useUsernameLocalStorage();
  const dispatch = useDispatch();
  const { t: c } = useTranslation("common");
  const { t } = useTranslation("login");
  // const cookies = parseCookies();
  const router = useRouter();
  const { redirect } = router.query;

  const color = `white !important`;
  const useOutlinedInputStyles = makeStyles(() => ({
    input: {
      color,
    },
    focused: {
      borderColor: color,
    },
    notchedOutline: {
      borderColor: color,
    },
  }));
  const outlinedInputClasses = useOutlinedInputStyles();

  function trimEmail(value: string, originalValue: string) {
    return originalValue.trim();
  }

  const ValidationSchema = Yup.object().shape({
    emlLogin: Yup.string()
      .transform(trimEmail)
      .email(c("form.invalidEmailTitle"))
      .required(c("form.requiredTitle")),
    psdLogin: Yup.string()
      .min(6, c("form.passwordMinLengthTitle", { size: 6 }))
      .max(30, c("form.passwordMaxLengthTitle", { size: 30 }))
      .required(c("form.requiredTitle")),
  });

  const initialValues: MyFormValues = {
    emlLogin: "",
    psdLogin: "",
  };

  const navigateToForgotPassword = () => {
    router.push("/forgot-password");
  };

  interface PrepareGroupInterface {
    data: {
      data: GroupInterface;
    };
  }

  const loadMoreUserGroupInfo = async (user: UserInfoInterface) => {
    const res: PrepareGroupInterface = await show(user.media.name);
    const {
      data: { data },
    } = res;
    dispatch(
      userInfoUpdate({
        ...user,
        media: {
          id: data.id,
          name: data.name,
          slogan: data.attributes.slogan[0] || "",
          email: data.attributes.email[0] || "",
          url: data.attributes.url[0] || "",
          language: data.attributes.language[0] || "",
          audio_description_url: data.attributes.audio_description_url[0] || "",
          social_media_whatsapp: data.attributes.social_media_whatsapp[0] || "",
          social_media_twitter: data.attributes.social_media_twitter[0] || "",
          social_media_facebook: data.attributes.social_media_facebook[0] || "",
          social_media_mastodon: data.attributes.social_media_mastodon[0] || "",
          social_media_instagram: data.attributes.social_media_instagram[0] || "",
          social_media_telegram: data.attributes.social_media_telegram[0] || "",
        },
      }),
    );
  };

  const showErrorNotification = (error: string) => {
    switch (error) {
      case AuthenticationErrorEnum.ERROR1:
        return t("errorMessages.error1");
      case AuthenticationErrorEnum.ERROR2:
        return t("errorMessages.error2");
      case AuthenticationErrorEnum.ERROR3:
        return t("errorMessages.error3");
      case AuthenticationErrorEnum.ERROR4:
        return t("errorMessages.error4");
      case AuthenticationErrorEnum.ERROR5:
        return t("errorMessages.error5");
      case AuthenticationErrorEnum.ERROR6:
        return t("errorMessages.error6");
      default:
        return t("errorMessages.error3");
    }
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={ValidationSchema}
        onSubmit={(values: MyFormValues, { setSubmitting }: any) => {
          const { psdLogin: password, emlLogin: emailv } = values;
          const email = emailv.trim();
          setSubmitting(true);
          (async () => {
            const result: any | null = await signIn("credentials", {
              redirect: false,
              email,
              password,
            });

            if (!result.error) {
              const session: any = await getSession();
              const { user }: { user: UserInfoInterface } = session;

              setUsername(user.id);
              dispatch(
                userInfoUpdate({
                  ...user,
                }),
              );

              loadMoreUserGroupInfo(user);

              const userLang = user.language;
              setCookie(null, "NEXT_LOCALE", userLang, {
                maxAge: 30 * 24 * 60 * 60,
                path: "/",
              });
              if (!redirect) {
                router.push("/home", "", {
                  locale: userLang,
                });
              } else {
                router.push(trailingSlash(redirect.toString()), "", {
                  locale: userLang,
                });
              }

              setSubmitting(false);
              return;
            }

            setSubmitting(false);
            toast(showErrorNotification(result.error), "warning");
          })();
        }}
      >
        {({ submitForm, isSubmitting, setFieldValue, errors, touched }: any) => (
          <>
            <Backdrop open={isSubmitting} />
            <Form
              id="loginForm"
              autoComplete="off"
              style={{ width: "100%" }}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  submitForm();
                }
              }}
            >
              <Field
                name="emlLogin"
                InputProps={{
                  notched: true,
                }}
              >
                {({ field }: FieldProps) => (
                  <FormControl style={{ width: "100%" }} variant="outlined">
                    <InputLabel htmlFor={`outlined-adornment-${c("form.placeholderEmail")}`}>
                      {c("form.placeholderEmail")}
                    </InputLabel>
                    <OutlinedInput
                      id={uuid()}
                      label={c("form.placeholderEmail")}
                      classes={outlinedInputClasses}
                      inputProps={{
                        autoComplete: "off",
                      }}
                      fullWidth
                      endAdornment={
                        <InputAdornment position="end">
                          <MailOutlineIcon style={{ color: "#fff" }} />
                        </InputAdornment>
                      }
                      {...field}
                    />
                  </FormControl>
                )}
              </Field>
              {errors.emlLogin && touched.emlLogin ? (
                <ErrorMessageForm message={errors.emlLogin} color="#fff" />
              ) : null}
              <Divider marginTop={20} />
              <Field name="psdLogin" InputProps={{ notched: true }}>
                {({ field }: FieldProps) => (
                  <PasswordField
                    mainColor="#fff"
                    label={c("form.placeholderPassword")}
                    placeholder={c("form.placeholderPassword")}
                    handleChangePassword={(value: string) => {
                      setFieldValue("psdLogin", value);
                    }}
                    required
                    {...field}
                  />
                )}
              </Field>
              {errors.psdLogin && touched.psdLogin ? (
                <ErrorMessageForm message={errors.psdLogin} color="#fff" />
              ) : null}
              <Divider marginTop={20} />
              {/* {isSubmitting && <LinearProgress />} */}
              <Divider marginTop={20} />
              <Box
                flexDirection="column"
                display="flex"
                alignContent="center"
                alignItems="center"
                justifyContent="center"
                flex={1}
              >
                <Button
                  title={c("form.forgetPasswordTitle")}
                  style={{ color: "#fff" }}
                  variant={ButtonVariantEnum.TEXT}
                  handleClick={navigateToForgotPassword}
                />
                <Button
                  title={c("form.submitLoginTitle")}
                  disabled={isSubmitting}
                  handleClick={submitForm}
                  style={{
                    width: 200,
                    marginTop: 15,
                    marginBottom: 30,
                    textTransform: "uppercase",
                  }}
                />
              </Box>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
