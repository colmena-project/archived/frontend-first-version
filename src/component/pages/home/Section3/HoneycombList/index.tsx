import React from "react";
import Box from "@material-ui/core/Box";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import theme from "@/styles/theme";
import router from "next/router";
import { useSelector } from "react-redux";
import { PropsHoneycombSelector } from "@/types/*";
import { makeStyles } from "@material-ui/core";
import { TextDisplayEnum, TextVariantEnum } from "@/enums/*";
import Text from "@/components/ui/Text";
import RoomAvatar from "@/components/pages/honeycomb/RoomAvatar";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getHoneycombLink } from "@/utils/offlineNavigation";
import {
  HoneycombAvatar,
  HoneycombClickable,
  HoneycombsWrapper,
  HoneycombWrapper,
} from "@/components/pages/home/Section3/HoneycombList/styled";

const useStyles = makeStyles(() => ({
  boxClickable: {
    width: "100%",
  },
}));

export default function GalleryHorizontalScroll() {
  const match = useMediaQuery(theme.breakpoints.up("sm"));
  const classes = useStyles();
  const connectionStatus = useConnectionStatus();

  const honeycombRdx = useSelector(
    (state: { honeycomb: PropsHoneycombSelector }) => state.honeycomb,
  );

  const items = honeycombRdx.honeycombs;
  return (
    <Box width="100%">
      <HoneycombsWrapper tabIndex={0}>
        {items.slice(0, 6).map(({ displayName, token, canDeleteConversation, id, type, name }) => (
          <HoneycombWrapper
            key={`${displayName}-${token}`}
            marginRight={2}
            data-testid={`honeycomb-item-${id}`}
            style={{ width: !match ? "23vw" : 80 }}
          >
            <HoneycombClickable
              handleClick={() =>
                router.push(
                  getHoneycombLink(
                    token,
                    displayName,
                    Number(canDeleteConversation),
                    connectionStatus,
                  ),
                )
              }
              className={classes.boxClickable}
              title={displayName}
            >
              <HoneycombAvatar>
                <RoomAvatar
                  name={name}
                  displayName={displayName}
                  canDeleteConversation={canDeleteConversation}
                  token={token}
                  width={80}
                  height={80}
                  roomType={type}
                />
              </HoneycombAvatar>
              <Text
                variant={TextVariantEnum.SUBTITLE1}
                display={TextDisplayEnum.BLOCK}
                noWrap
                style={{
                  color: theme.palette.primary.dark,
                  fontWeight: "bold",
                  textAlign: "center",
                  fontSize: 14,
                }}
              >
                {displayName}
              </Text>
            </HoneycombClickable>
          </HoneycombWrapper>
        ))}
      </HoneycombsWrapper>
    </Box>
  );
}
