import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Clickable from "@/components/ui/Clickable";

export const HoneycombsWrapper = styled(Box)({
  display: "flex",
  overflowY: "auto",
});

export const HoneycombWrapper = styled(Box)({
  maxWidth: 80,
  marginLeft: 10,
  marginRight: 10,
});

export const HoneycombAvatar = styled(Box)({
  width: "100%",
  textAlign: "center",
  marginBottom: 5,
});

export const HoneycombClickable = styled(Clickable)({
  width: "100%",
});
