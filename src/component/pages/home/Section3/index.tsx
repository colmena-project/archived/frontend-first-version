import React from "react";
import Box from "@material-ui/core/Box";
import ToolbarSection from "@/components/ui/ToolbarSection";
import HoneycombList from "./HoneycombList";
import Divider from "@/components/ui/Divider";
import { useTranslation } from "react-i18next";
import { getUsersConversations } from "@/services/talk/room";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import SkeletonHoneycombs from "@/components/ui/skeleton/Honeycombs";
import { BaseLayoutWrapper, Spacing } from "@/components/pages/home/Section3/styled";

function Honeycombs() {
  const { t } = useTranslation("home");
  const { t: c } = useTranslation("common");
  const { data, error } = getUsersConversations({
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });

  if (!data && !error)
    return (
      <LayoutWrapper title={t("section3Title")}>
        <Box style={{ overflow: "auto" }}>
          <SkeletonHoneycombs amount={3} />
        </Box>
      </LayoutWrapper>
    );

  if (error || data.length === 0)
    return (
      <LayoutWrapper title={t("section3Title")}>
        <Box style={{ backgroundColor: "#fff" }} padding={2}>
          <Text style={{ textAlign: "left" }} variant={TextVariantEnum.BODY2}>
            {c("noItemsFound")}
          </Text>
        </Box>
      </LayoutWrapper>
    );

  return (
    <LayoutWrapper title={t("section3Title")}>
      <HoneycombList />
    </LayoutWrapper>
  );
}

type LayoutWrapperProps = {
  children: React.ReactNode;
  title: string;
};

function LayoutWrapper({ children, title }: LayoutWrapperProps) {
  return (
    <BaseLayoutWrapper>
      <Spacing>
        <ToolbarSection title={title} icon="panal" link="/honeycomb" noMargin />
      </Spacing>
      <Divider marginTop={10} />
      {children}
    </BaseLayoutWrapper>
  );
}

export default React.memo(Honeycombs);
