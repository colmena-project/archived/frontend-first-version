import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";

export const BaseLayoutWrapper = styled(Box)({
  flex: 1,
  display: "flex",
  flexDirection: "column",
  width: "100%",
});

export const Spacing = styled(Box)({
  padding: "0px 10px",
  width: "100%",
});
