import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

import Text from "@/components/ui/Text";

export const Header = styled(Box)(({ theme }) => ({
  paddingTop: 20,
  background: "url(/images/svg/bg-home.svg) repeat-y right",
  backgroundColor: theme.palette.secondary.main,
  width: "100%",
}));

export const Contet = styled(Box)({
  width: "100%",
  padding: "8px 0",
});

export const GreetingTitle = styled(Text)(({ theme }) => ({
  color: theme.palette.secondary.contrastText,
  fontSize: 16,
}));

export const UserName = styled(Text)(({ theme }) => ({
  color: theme.palette.secondary.contrastText,
  fontSize: 16,
  margin: 0,
  padding: 0,
  textAlign: "center",
  fontWeight: "bold",
  letterSpacing: "1px",
}));

export const MediaName = styled(Text)(({ theme }) => ({
  color: theme.palette.secondary.contrastText,
  fontSize: 13,
}));

export const BoxAvatar = styled(Box)({
  height: "60px",
  width: "100%",
  display: "flex",
  justifyContent: "center",
  paddingTop: "20px",
});

export const SubHeader = styled(Box)({
  width: "100%",
  backgroundColor: "#fff",
  borderRadius: "0px 0px 30px 30px",
  boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
  marginBottom: 10,
});

export const Logo = styled(Box)({
  width: "100%",
  display: "flex",
  justifyContent: "space-around",
  "& svg": {
    width: 120,
    marginRight: 52,
  },
});

export const Spacing = styled(Box)({
  width: "100%",
  padding: "0px 10px",
});
