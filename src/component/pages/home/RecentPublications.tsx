import { Box } from "@material-ui/core";
import PublicationList from "@/components/pages/publication/PublicationList";
import { PublicationFiltersProps } from "@/services/cms/publications";
import ToolbarSection from "@/components/ui/ToolbarSection";
import { useTranslation } from "react-i18next";
import { Spacing } from "./styled";

const filters: PublicationFiltersProps = {
  order: "publishedAt:desc",
  index: 0,
};

export default function RecentPublications() {
  const { t: homeTranslation } = useTranslation("home");

  return (
    <Box width="100%" data-testid="recent-publications">
      <Spacing>
        <ToolbarSection
          icon="publications"
          title={homeTranslation("publicLabel")}
          link="/publications"
          noMargin
        />
      </Spacing>
      <PublicationList filters={filters} height="300px" itemsPerPage={3} />
    </Box>
  );
}
