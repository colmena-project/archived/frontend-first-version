import React, { useState, useEffect, useCallback, useRef } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { InputBase } from "@material-ui/core";
import IconButton from "@/components/ui/IconButton";
import { useTranslation } from "next-i18next";
import { Field, FieldProps, Form, Formik } from "formik";
import themeRelease from "@/styles/theme";
import classNames from "classnames";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      padding: "5px",
      display: "flex",
      alignItems: "center",
      width: "100%",
      background: "rgba(255,255,255, 0)",
      marginBottom: 10,
      border: `1px solid ${theme.palette.primary.main}`,
      borderRadius: 30,
      boxShadow: "none",
    },
    form: {
      width: "100%",
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
      color: theme.palette.primary.main,
    },
    iconButton: {
      padding: 10,
      margin: 0,
      minWidth: "auto",
    },
  }),
);

type FormValues = {
  keyword: string;
};

type Props = {
  search: (keyword: string) => void;
  keyword?: string;
  onFocus?: () => void;
  onBlur?: () => void;
  cancel?: () => void;
  disabled?: boolean;
};

export default function SearchInput({
  search,
  keyword = "",
  onFocus,
  onBlur,
  cancel,
  disabled,
}: Props) {
  const inputRef = useRef<HTMLInputElement>();
  const classes = useStyles();
  const { t } = useTranslation("search");
  const { t: a } = useTranslation("accessibility");
  // eslint-disable-next-line no-undef
  const [isOnFocus, setIsOnFocus] = useState(false);
  // eslint-disable-next-line no-undef
  const [intervalSearch, setIntervalSearch] = useState<undefined | NodeJS.Timeout>();
  const [submitType, setSubmitType] = useState<"search" | "clear">("search");
  const initialValues: FormValues = {
    keyword,
  };

  const handleSearch = (values: FormValues) => {
    if (intervalSearch) {
      clearTimeout(intervalSearch);
      setIntervalSearch(undefined);
    }

    search(values.keyword);
  };

  const handleIntervalSearch = (submitForm: any) => {
    if (intervalSearch) {
      clearTimeout(intervalSearch);
    }

    setIntervalSearch(undefined);
    setIntervalSearch(
      setTimeout(() => {
        submitForm();
      }, 600),
    );
  };

  useEffect(() => {
    let buttonType: "clear" | "search" = "search";
    if (keyword !== "") {
      buttonType = "clear";
    }

    setSubmitType(buttonType);
  }, [keyword]);

  const handleOnBlur = useCallback(
    (e) => {
      const { path } = e as any;
      const totalPaths = path.length;
      let blur = true;
      // eslint-disable-next-line no-plusplus
      for (let index = 0; index < totalPaths; index++) {
        const element = path[index];
        if (element.classList?.contains("ignore-blur")) {
          blur = false;
          break;
        }
      }

      if (blur && typeof onBlur === "function") {
        setIsOnFocus(false);
        onBlur();
      }
    },
    [onBlur],
  );

  useEffect(() => {
    if (isOnFocus) {
      document.body.addEventListener("click", handleOnBlur);
    } else {
      document.body.removeEventListener("click", handleOnBlur);
    }
  }, [onBlur, isOnFocus, handleOnBlur]);

  const handleOnFocus = () => {
    if (typeof onFocus === "function") {
      setIsOnFocus(true);
      onFocus();
    }
  };

  return (
    <Formik initialValues={initialValues} onSubmit={handleSearch}>
      {({ setFieldValue, submitForm }: any) => {
        useEffect(() => {
          setFieldValue("keyword", keyword);
          // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [setFieldValue, keyword]);

        return (
          <Form className={classes.form}>
            <Paper component="div" className={classNames(classes.root, "ignore-blur")}>
              <Field name="keyword" InputProps={{ notched: true }} onChange>
                {({ field }: FieldProps) => (
                  <InputBase
                    ref={inputRef}
                    className={classes.input}
                    placeholder={t("searchLabel")}
                    value={field.value}
                    onChange={(
                      event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
                    ) => {
                      setFieldValue("keyword", event.target.value);
                      setSubmitType("search");
                      handleIntervalSearch(submitForm);
                    }}
                    onFocus={handleOnFocus}
                    disabled={disabled}
                  />
                )}
              </Field>
              {submitType === "search" && (
                <IconButton
                  aria-label={a("home.iconButton.ariaLabel")}
                  handleClick={submitForm}
                  icon="search"
                  className={classes.iconButton}
                  fontSizeIcon="small"
                  iconColor={themeRelease.palette.primary.main}
                  disabled={disabled}
                />
              )}
              {submitType === "clear" && (
                <IconButton
                  aria-label={a("home.iconButton.ariaLabelClean")}
                  handleClick={() => {
                    if (cancel) cancel();
                  }}
                  icon="delete"
                  className={classes.iconButton}
                  fontSizeIcon="small"
                  iconColor={themeRelease.palette.primary.main}
                />
              )}
            </Paper>
          </Form>
        );
      }}
    </Formik>
  );
}
