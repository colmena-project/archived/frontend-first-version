import React, { useEffect, useState } from "react";
import { v4 as uuid } from "uuid";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Chip from "@material-ui/core/Chip";
import { useTranslation } from "next-i18next";
import { TagInterface } from "@/interfaces/tags";
import TagList from "@/components/ui/skeleton/TagList";
import ToolbarSection from "@/components/ui/ToolbarSection";
import { empty } from "@/utils/utils";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      width: "100%",
      textAlign: "left",
      margin: "10px 0",
    },
    tags: {
      overflow: "auto",
      width: "100%",
      padding: "10px 0",
    },
    tagRow: {
      display: "flex",
      flexWrap: "nowrap",
    },
    tagItem: {
      marginBottom: 7,
      backgroundColor: "#fff",
      border: `1px solid ${theme.palette.gray.main}`,
      color: theme.palette.gray.dark,
    },
    activeTag: {
      backgroundColor: theme.palette.primary.main,
      color: "#fff",
      borderColor: "#fff",
    },
    sectionTitle: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    clearButton: {
      backgroundColor: `${theme.palette.gray.dark} !important`,
      color: theme.palette.gray.contrastText,
      display: "inline-block",
      borderRadius: 15,
      textTransform: "initial",
      padding: 2,
    },
    spacing: {
      padding: "0 10px",
    },
  }),
);

type Props = {
  keyword?: string;
  rawTags?: TagInterface[];
  loading?: boolean;
  currentTag?: string;
  openTag?: (tag: TagInterface) => void;
  clear?: () => void;
};

export default function Tags({
  rawTags,
  keyword,
  currentTag,
  openTag,
  loading = false,
  clear,
}: Props) {
  const [tags, setTags] = useState<Array<TagInterface[]>>([]);
  const { t } = useTranslation("search");
  const classes = useStyles();

  const handleOpenTag = (tag: TagInterface) => {
    if (typeof openTag === "function") {
      openTag(tag);
    }
  };

  const orderTags = (tags: TagInterface[]) => {
    // eslint-disable-next-line no-confusing-arrow
    tags.sort((a: TagInterface, b: TagInterface) =>
      // eslint-disable-next-line prettier/prettier
      b?.quantity && a?.quantity && b.quantity > a.quantity ? 1 : -1,
    );

    return tags;
  };

  const mergeUniqueTags = (tags: TagInterface[]) => {
    const mergedTags: TagInterface[] = [];
    tags.forEach((tag) => {
      const mergedTag = mergedTags.find((mergedTag) => mergedTag.tagName === tag.tagName);
      if (mergedTag) {
        if (mergedTag.quantity) mergedTag.quantity += 1;
      } else {
        mergedTags.push({ ...tag, quantity: 1 });
      }
    });

    return mergedTags;
  };

  const filterByKeyword = (tags: TagInterface[], keyword: string | undefined) => {
    if (!keyword) {
      return tags;
    }

    return tags.filter(
      (tag: TagInterface) =>
        tag.tagName.toString().toLowerCase().indexOf(keyword.toString().toLowerCase()) >= 0,
    );
  };

  const handleTags = (tags: TagInterface[], keyword: string | undefined) => {
    const mergedTags = orderTags(mergeUniqueTags(filterByKeyword(tags, keyword)));
    const totalRows = mergedTags.length > 10 ? 2 : 1;
    const totalTags = Math.ceil(mergedTags.length / totalRows);
    const slicedTags: Array<TagInterface[]> = [];
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < totalRows; i++) {
      const initialPosition = totalTags * i;
      const finalPosition = i > 0 ? totalTags * i + totalTags : totalTags;
      slicedTags.push(mergedTags.slice(initialPosition, finalPosition));
    }

    setTags(slicedTags);
  };

  useEffect(() => {
    if (rawTags) {
      handleTags(rawTags, keyword);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rawTags, keyword]);

  if (!loading && (!tags[0] || tags[0].length === 0)) {
    return null;
  }

  const items = tags.map((row) => (
    <Box className={classes.tagRow} key={uuid()}>
      {row.map((tag, index) => (
        <Chip
          clickable
          onClick={() => handleOpenTag(tag)}
          key={uuid()}
          className={[
            "ignore-blur",
            classes.tagItem,
            tag.tagId === currentTag ? classes.activeTag : undefined,
          ].join(" ")}
          label={
            <Box component="span" display="flex" flexDirection="row">
              {tag.tagName.toString().toLowerCase()}
            </Box>
          }
          style={{ marginRight: 5, marginLeft: index === 0 ? 10 : 0 }}
        />
      ))}
    </Box>
  ));

  return (
    <Box className={classes.root}>
      <Box className={classes.spacing}>
        <ToolbarSection
          title={t("searchByTags")}
          icon="hashtag"
          seeAllTitle={t("clear")}
          handleClick={() => clear && clear()}
          showRightButton={!empty(currentTag)}
          noMargin
        />
      </Box>
      <Box className={classes.tags}>{!loading ? items : <TagList />}</Box>
    </Box>
  );
}
