import React, { useCallback, useEffect, useState } from "react";
import { useTranslation } from "next-i18next";
import SearchInput from "./SearchInput";
import { LibraryItemInterface, TimeDescriptionInterface } from "@/interfaces/index";
import { searchByDisplayName, searchItems } from "@/services/webdav/files";
import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import ListItems from "./ListItems";
import Recents from "./Recents";
import { getAllTags } from "@/services/webdav/tags";
import { TagInterface } from "@/interfaces/tags";
import Tags from "./Tags";
import { getAllLibraryItems, setAllLibraryItems } from "@/utils/utils";
import classNames from "classnames";

const useStyles = makeStyles((theme) => ({
  topSearch: {
    padding: "0!important",
    margin: "0!important",
  },
  searchBox: {
    width: "100%",
  },
  content: {
    paddingBottom: 10,
  },
  container: {
    width: "100%",
    padding: "0 10px",
  },
  appBar: {
    display: "flex",
    justifyContent: "flex-start",
    position: "relative",
    backgroundColor: theme.palette.primary.main,
  },
  tabs: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    width: "100vw",
    color: theme.palette.icon.main,
  },
  avatar: {
    marginRight: 10,
  },
}));

type Props = {
  setIsSearching: (opt: boolean) => void;
  isSearching: boolean;
  disabled?: boolean;
};

const Search = ({ setIsSearching, isSearching, disabled = false }: Props) => {
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [rawTags, setRawTags] = useState<TagInterface[]>();
  const [rawItems, setRawItems] = useState<LibraryItemInterface[]>([]);
  const [items, setItems] = useState<LibraryItemInterface[]>([]);
  const [keyword, setKeyword] = useState<string>("");
  const [recentSearches, setRecentSearches] = useState<Array<string>>([]);
  const [isLoadingItems, setIsLoadingItems] = useState<boolean>(false);
  const [isLoadingTags, setIsLoadingTags] = useState<boolean>(true);
  const { t } = useTranslation("search");
  const { t: c } = useTranslation("common");
  const timeDescription: TimeDescriptionInterface = c("timeDescription", { returnObjects: true });
  const [tag, setTag] = useState<string>();
  const recentSearchesKey = "recentSearches";

  const search = useCallback(
    (value: string) => {
      setKeyword(value ?? "");
      if (value.length === 0) {
        return;
      }

      let items: LibraryItemInterface[] = [];
      try {
        items = searchItems(rawItems, value);
        setItems(items);
      } catch (e) {
        //
      }
    },
    [rawItems],
  );

  const getTags = (tagId?: string) => {
    if (rawTags) {
      return rawTags.filter((rawTag) => rawTag.tagId === tagId);
    }

    return [];
  };
  const filterByTag = (items: LibraryItemInterface[], tagId?: string) => {
    const tags = getTags(tagId);
    return items.filter(
      (item: LibraryItemInterface) =>
        tags &&
        tags.filter(
          (tag: TagInterface) =>
            tag.fileName.replace(/(.+?files\/|^files\/)/, "") === item.filename,
        ).length > 0,
    );
  };

  const handleInputFocus = () => {
    setIsSearching(true);
  };

  const clearSearch = useCallback(() => {
    setKeyword("");
    setRawItems([]);
    setItems([]);
    setTag(undefined);
    setIsSearching(false);
  }, [setIsSearching]);

  const handleInputBlur = useCallback(() => {
    if (keyword.length === 0) {
      clearSearch();
    }
  }, [clearSearch, keyword.length]);

  const reloadItems = async () => {
    search(keyword);
  };

  const clearRecentSearches = () => {
    setRecentSearches([]);
    localStorage.setItem(recentSearchesKey, "");
  };

  const handleClearTags = () => {
    setTag(undefined);
    if (keyword) {
      search(keyword);
    } else {
      setIsSearching(false);
    }
  };

  const saveRecentSearchesKeyword = async (keyword: string) => {
    const lastRecentSearches = await getRecentSearchesKeywords();
    if (lastRecentSearches.includes(keyword)) {
      return;
    }

    localStorage.setItem(
      recentSearchesKey,
      JSON.stringify([keyword, ...lastRecentSearches].slice(0, 5)),
    );
    loadRecentSearches();
  };

  const getRecentSearchesKeywords = async () => {
    const recentSearches = await localStorage.getItem(recentSearchesKey);
    if (!recentSearches) {
      return [];
    }

    try {
      return JSON.parse(recentSearches);
    } catch (e) {
      return [];
    }
  };

  const handleSearch = useCallback(
    async (value: string) => {
      if (!value) {
        setKeyword("");
        setItems([]);

        return;
      }

      if (value === keyword) {
        return;
      }

      search(value);
      saveRecentSearchesKeyword(value);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [search],
  );

  const loadRawTags = useCallback(async () => {
    let tags: TagInterface[] = [];
    try {
      tags = await getAllTags();
    } catch (e) {
      console.error("LOAD RAW TAGS", e);
    }

    setRawTags(tags);
  }, []);

  const loadRecentSearches = async () => {
    setRecentSearches(await getRecentSearchesKeywords());
  };

  const loadRawItems = async () => {
    let items: LibraryItemInterface[] = [];
    try {
      const result = await searchByDisplayName(userRdx.user.id, "", timeDescription, t);
      if (result) {
        items = result;
        setAllLibraryItems(items);
      }
    } catch (e) {
      //
    }

    setRawItems(items);
  };

  const handleOpenTag = (tag: TagInterface) => {
    const tagId = tag.tagId.toString();
    setTag(tagId);
    const items = filterByTag(rawItems, tagId);
    setItems(items);
  };

  const handleSearchInputCancel = () => {
    setKeyword("");
  };

  useEffect(() => {
    (async () => {
      setIsLoadingItems(true);
      const persistedRawItems = getAllLibraryItems();
      if (persistedRawItems) {
        setRawItems(persistedRawItems);
        setIsLoadingItems(false);
      }

      await loadRawItems();
      setIsLoadingItems(false);
    })();

    (async () => {
      await loadRawTags();
      setIsLoadingTags(false);
    })();

    (async () => {
      await loadRecentSearches();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (items.length === 0 && tag) {
      setTag(tag);
      const items = filterByTag(rawItems, tag);
      setItems(items);
    } else if (items.length === 0 && keyword) {
      search(keyword);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rawItems]);

  return (
    <Box className={classes.searchBox} role="search">
      <Box className={classes.container}>
        <SearchInput
          search={handleSearch}
          keyword={keyword}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          cancel={handleSearchInputCancel}
          disabled={disabled}
        />
      </Box>
      <Box
        display={!isSearching ? "none" : ""}
        className={classNames("ignore-blur", classes.content)}
      >
        <Box>
          <Tags
            currentTag={tag}
            keyword={keyword}
            rawTags={rawTags}
            openTag={handleOpenTag}
            loading={isLoadingTags}
            clear={handleClearTags}
          />
        </Box>
        <Box className={classes.container}>
          {((keyword.length > 0 || tag !== undefined) && isSearching && (
            <ListItems items={items} loading={isLoadingItems} reloadItems={reloadItems} />
          )) || (
            <Recents
              keywords={recentSearches}
              clearRecentSearches={clearRecentSearches}
              searchByKeyword={handleSearch}
            />
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default Search;
