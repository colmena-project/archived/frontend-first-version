import React, { useState } from "react";
import { useTranslation } from "next-i18next";
import Text from "@/components/ui/Text";
import { RoomOrderEnum, TextVariantEnum } from "@/enums/index";
import Box from "@material-ui/core/Box";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import FilterButton from "@/components/ui/FilterButton";
import ChannelFiltersDrawer from "./FiltersDrawer";
import theme from "@/styles/theme";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      textAlign: "left",
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(1),
      marginLeft: "10px",
      marginRight: "10px",
    },
    title: {
      color: theme.palette.variation2.main,
      fontSize: "18px",
      fontWeight: "bold",
      flex: 1,
    },
    description: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    topHeader: {
      display: "flex",
      alignItems: "center",
    },
    itemsQuantity: {
      color: theme.palette.gray.main,
    },
  }),
);

type Props = {
  itemQuantity?: number;
  setRoomOrder: (roomOrder: RoomOrderEnum) => void;
  roomOrder: RoomOrderEnum;
};

function ChannelHeader({ setRoomOrder, roomOrder, itemQuantity }: Props) {
  const classes = useStyles();
  const { t } = useTranslation("channel");

  const [filtersDrawerIsOpen, setFiltersDrawerIsOpen] = useState(false);

  const orderRooms = (order: RoomOrderEnum) => {
    setRoomOrder(order);
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.topHeader}>
        <Text variant={TextVariantEnum.H2} className={classes.title}>
          {t("exploreChannels")}
        </Text>
        {itemQuantity && (
          <Text variant={TextVariantEnum.CAPTION} className={classes.itemsQuantity}>
            {itemQuantity} {t("items")}
          </Text>
        )}
      </Box>

      <Text className={classes.description}>{t("pageDescription")}</Text>
      <Box>
        <FilterButton
          title={t("filters")}
          icon="settings_adjust"
          handleClick={() => setFiltersDrawerIsOpen(true)}
          color={theme.palette.secondary.main}
        />
      </Box>
      <ChannelFiltersDrawer
        open={filtersDrawerIsOpen}
        handleClose={() => setFiltersDrawerIsOpen(false)}
        orderItems={orderRooms}
        order={roomOrder}
      />
    </Box>
  );
}

export default ChannelHeader;
