import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import { RoomItemInterface } from "@/interfaces/talk";
import theme from "@/styles/theme";
import { Checkbox, Collapse, FormControlLabel } from "@material-ui/core";
import { useTranslation } from "next-i18next";
import { TextVariantEnum } from "@/enums/*";
import ListItem from "@material-ui/core/ListItem";
import RoomAvatar from "../honeycomb/RoomAvatar";
import SvgIconAux from "@/components/ui/SvgIcon";
import { toast } from "@/utils/notifications";
import { useRouter } from "next/router";
import Text from "@/components/ui/Text";
import { joinAconversationToCallOrChat } from "@/services/talk/call";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getHoneycombLink } from "@/utils/offlineNavigation";
import {
  Card,
  CardHeader,
  ChannelAvatar,
  Description,
  JoinButton,
  MoreDetailsWrapper,
  Options,
} from "@/components/pages/channel/styled";

type Props = {
  room: RoomItemInterface;
};

const ChannelListItem = ({ room }: Props) => {
  const { t } = useTranslation("channel");
  const { id, name, displayName, token, canDeleteConversation, unreadMessages } = room;
  const [openJoinChannel, setOpenJoinChannel] = useState(false);
  // const [description, setDescription] = useState("");
  const [acceptedTerm, setAcceptedTerm] = useState(false);
  const router = useRouter();
  const connectionStatus = useConnectionStatus();

  const joinAChannel = async () => {
    try {
      const joined = await joinAconversationToCallOrChat(token);
      if (!joined) {
        throw Error();
      }

      router.push(
        getHoneycombLink(token, room.name, !canDeleteConversation ? 0 : 1, connectionStatus),
      );
    } catch (e) {
      const msg = e.message ? e.message : t("honeycombModal.chatRoomFailed");
      toast(msg, "error");
    }
  };

  const toggleMoreDetails = async () => {
    const isOpened = !openJoinChannel;
    setOpenJoinChannel(isOpened);

    // if (isOpened && !description) {
    //   const room = await getSingleConversationAxios(token);
    //   console.log(room);
    //   if (room) {
    //     setDescription(room.data.ocs.data.description);
    //   }
    // }
  };

  return (
    <ListItem
      style={{ padding: "2px 10px" }}
      key={id}
      disableGutters
      data-testid="channel-card-item"
    >
      <Card>
        <CardHeader handleClick={toggleMoreDetails} data-testid="more-details-button">
          <ChannelAvatar data-testid={`channel-avatar-${id}`}>
            <Box>
              <RoomAvatar
                name={name}
                displayName={displayName}
                canDeleteConversation={canDeleteConversation}
                token={token}
                width={50}
                height={50}
                fontSize={18}
              />
            </Box>
          </ChannelAvatar>
          <Description
            data-testid={`channel-title-${id}`}
            primary={displayName}
            primaryTypographyProps={{
              style: {
                color: theme.palette.primary.dark,
                fontWeight: unreadMessages > 0 ? "bold" : "normal",
                whiteSpace: "normal",
              },
            }}
            // secondary={openJoinChannel && description ? description : undefined}
            secondaryTypographyProps={{
              style: {
                fontWeight: unreadMessages > 0 ? "bold" : "normal",
              },
            }}
          />
          <Options>
            <SvgIconAux
              icon={openJoinChannel ? "chevron_bottom" : "chevron_right"}
              fontSize={11}
              htmlColor={theme.palette.gray.main}
            />
          </Options>
        </CardHeader>
        <Collapse in={openJoinChannel}>
          <MoreDetailsWrapper data-testid="more-details">
            <FormControlLabel
              control={
                <Checkbox
                  onChange={(e) => setAcceptedTerm(e.target.checked)}
                  data-testid="accept-terms-input"
                />
              }
              label={
                <Text variant={TextVariantEnum.BODY2}>
                  <Box
                    dangerouslySetInnerHTML={{
                      __html: t("readAndAcceptTerms", {
                        codeOfConductLink: `<a href="#" data-testid="code-of-conduct-button"><strong>${t(
                          "codeOfConduct",
                        )}</strong></a>`,
                      }),
                    }}
                  />
                </Text>
              }
            />
            <JoinButton
              disabled={!acceptedTerm}
              title={t("join")}
              color="variation2"
              handleClick={joinAChannel}
              data-testid="join-channel"
            />
          </MoreDetailsWrapper>
        </Collapse>
      </Card>
    </ListItem>
  );
};

export default ChannelListItem;
