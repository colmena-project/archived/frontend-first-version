import React, { useEffect, useState } from "react";
import { RoomItemInterface } from "@/interfaces/talk";
import { v4 as uuid } from "uuid";
import List from "@material-ui/core/List";
import { makeStyles, Box } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { PropsChannelSelector, PropsUserSelector } from "@/types/*";
import AlertInfoCenter from "@/components/ui/AlertInfoCenter";
import { setChannels } from "@/store/actions/channel";
import ChannelListItem from "./ChannelListItem";
import ChannelsToolsMenu from "@/components/ui/ToolsMenu/Contexts/ChannelsToolsMenu";
import { searchRooms } from "@/services/talk/room";
import ChannelHeader from "./ChannelHeader";
import { RoomOrderEnum } from "@/enums/*";
import { orderItems } from "@/utils/talk";
import { useTranslation } from "next-i18next";

const useStyles = makeStyles(() => ({
  list: {
    textAlign: "left",
    alignItems: "stretch",
  },
  verticalList: {
    padding: "2px 10px",
  },
}));

type Props = {
  data: RoomItemInterface[];
  searchKeyword?: string;
};
export const orderByLastActivity = (a: RoomItemInterface, b: RoomItemInterface) =>
  b.lastActivity - a.lastActivity;

function ChannelList({ data, searchKeyword }: Props) {
  const { t } = useTranslation("common");
  const classes = useStyles();
  const dispatch = useDispatch();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const channelRdx = useSelector((state: { channel: PropsChannelSelector }) => state.channel);
  const [channelsList, setChannelsList] = useState<RoomItemInterface[]>([]);
  const [roomOrder, setRoomOrder] = useState<RoomOrderEnum>(RoomOrderEnum.NEWESTS);

  useEffect(() => {
    dispatch(setChannels(data));

    setChannelsList(handleChannels(data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, roomOrder]);

  useEffect(() => {
    const { channels } = channelRdx;
    setChannelsList(handleChannels(channels));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [channelRdx, searchKeyword]);

  const handleChannels = (channels: RoomItemInterface[]) => {
    if (!channels) {
      return [];
    }

    let filteredChannels = channels;
    if (searchKeyword) {
      filteredChannels = searchRooms(filteredChannels, searchKeyword);
    }
    return orderItems(userRdx, roomOrder, filteredChannels);
  };

  if (channelsList.length === 0) return <AlertInfoCenter title={t("noItemsFound")} />;
  return (
    <Box>
      <ChannelHeader
        setRoomOrder={setRoomOrder}
        roomOrder={roomOrder}
        itemQuantity={channelsList.length}
      />
      {channelsList.length === 0 && <AlertInfoCenter title={t("noItemsFound")} />}
      {channelsList.length > 0 && (
        <List className={classes.list}>
          {channelsList.map((item: RoomItemInterface) => (
            <ChannelListItem key={uuid()} room={item} />
          ))}
          <ChannelsToolsMenu />
        </List>
      )}
    </Box>
  );
}

export default ChannelList;
