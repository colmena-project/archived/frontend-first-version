import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import { v4 as uuid } from "uuid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ButtonVariantEnum, RoomOrderEnum } from "@/enums/index";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useTranslation } from "react-i18next";
import { Button, FormControlLabel, Radio, RadioGroup } from "@material-ui/core";
import { Formik, Form } from "formik";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiAccordionSummary-root": {
        backgroundColor: theme.palette.gray.light,
      },
      "& .MuiAccordion-root": {
        boxShadow: "none",
      },
      "& .MuiListItem-root": {
        padding: 0,
      },
      "& .MuiFormControlLabel-root": {
        width: "100%",
      },
      "& .MuiIconButton-root.Mui-checked": {
        color: theme.palette.variation1.main,
      },
    },
    filterButton: {
      borderRadius: 0,
      backgroundColor: theme.palette.variation2.main,
      color: theme.palette.variation2.contrastText,
      width: "100%",
      boxShadow: "none",
      textTransform: "inherit",
      fontSize: "1rem",
    },
  }),
);

type Props = {
  open: boolean;
  handleClose: () => void;
  orderItems: (order: RoomOrderEnum) => void;
  order: RoomOrderEnum;
};

export default function ChannelFiltersDrawer({ open, orderItems, handleClose, order }: Props) {
  const [expanded, setExpanded] = React.useState<string | boolean>("order");
  const classes = useStyles();
  const { t } = useTranslation("channel");
  const initialValues = {
    order,
  };

  const handleChange = (panel: string) => (event: any, newExpanded: string | boolean) => {
    setExpanded(newExpanded ? panel : false);
  };

  const handleSubmit = ({ order }: any) => {
    orderItems(order);
    handleClose();
  };

  return (
    <Drawer anchor="right" open={open} onClose={handleClose} className={classes.root}>
      <Formik initialValues={initialValues} onSubmit={handleSubmit}>
        {({ values, setFieldValue }: any) => (
          <Form>
            <Accordion expanded={expanded === "order"} onChange={handleChange("order")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="order-content"
                id="order-header"
              >
                <Typography>{t("sort.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <RadioGroup
                  name="order"
                  value={values.order.toString()}
                  onChange={(event) => {
                    setFieldValue("order", event.currentTarget.value);
                  }}
                >
                  <List>
                    <ListItem button key={uuid()}>
                      <FormControlLabel
                        value={RoomOrderEnum.NEWESTS}
                        control={<Radio />}
                        label={t("sort.newests")}
                      />
                    </ListItem>
                    <ListItem button key={uuid()}>
                      <FormControlLabel
                        value={RoomOrderEnum.OLDEST}
                        control={<Radio />}
                        label={t("sort.oldest")}
                      />
                    </ListItem>
                    <ListItem button key={uuid()}>
                      <FormControlLabel
                        value={RoomOrderEnum.ASC_ALPHABETICAL}
                        control={<Radio />}
                        label={t("sort.alphabeticalOrder")}
                      />
                    </ListItem>
                    <ListItem button key={uuid()}>
                      <FormControlLabel
                        value={RoomOrderEnum.DESC_ALPHABETICAL}
                        control={<Radio />}
                        label={t("sort.descendingAlphabeticalOrder")}
                      />
                    </ListItem>
                  </List>
                </RadioGroup>
              </AccordionDetails>
            </Accordion>
            <Button
              variant={ButtonVariantEnum.CONTAINED}
              className={classes.filterButton}
              type="submit"
            >
              {t("filterButton")}
            </Button>
          </Form>
        )}
      </Formik>
    </Drawer>
  );
}
