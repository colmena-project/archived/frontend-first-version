import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Clickable from "@/components/ui/Clickable";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import theme from "@/styles/theme";
import ContainedButton from "@/components/ui/Button/ContainedButton";

export const Card = styled(Box)({
  display: "flex",
  flexDirection: "column",
  width: "100%",
  background: "#fff",
  padding: 8,
  borderRadius: 5,
  border: "1px solid #eee",
});

export const CardHeader = styled(Clickable)({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  flexWrap: "nowrap",
});

export const Description = styled(ListItemText)({
  flexDirection: "column",
  flexGrow: 1,
  alignSelf: "stretch",
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
  marginLeft: 5,
  display: "flex",
  justifyContent: "center",
});

export const Options = styled(Box)({
  display: "flex",
  flexDirection: "row",
  flexWrap: "nowrap",
});

export const ChannelAvatar = styled(ListItemAvatar)({
  display: "flex",
  alignItems: "center",
  width: 50,
});

export const MoreDetailsWrapper = styled(Box)({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  marginTop: theme.spacing(2),
});

export const JoinButton = styled(ContainedButton)({
  marginTop: theme.spacing(1),
});
