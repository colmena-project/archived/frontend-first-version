/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Box from "@material-ui/core/Box";
import { TextVariantEnum } from "@/enums/*";
import ListSubheader from "@material-ui/core/ListSubheader";
import Text from "@/components/ui/Text";

type Props = {
  title: string;
  secondaryAction: React.ReactNode;
  children: React.ReactNode;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      paddingTop: 5,
    },
    titleContainer: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      paddingTop: 10,
      borderTopColor: theme.palette.primary.main,
      borderTopWidth: 1,
    },
    title: {
      fontWeight: "bold",
      color: theme.palette.primary.main,
      textAlign: "left",
      fontSize: 16,
    },
  }),
);

export default function Section({ title, secondaryAction, children }: Props) {
  const classes = useStyles();
  return (
    <List
      subheader={
        <ListSubheader>
          <Box className={classes.titleContainer}>
            <Text variant={TextVariantEnum.BODY1} className={classes.title}>
              {title}
            </Text>
            <Box>{secondaryAction}</Box>
          </Box>
        </ListSubheader>
      }
      className={classes.root}
    >
      <ListItem>{children}</ListItem>
    </List>
  );
}
