import React, { useState, useMemo } from "react";
import { LibraryItemInterface } from "@/interfaces/index";
import Section from "@/components/pages/file/Section";
import Skeleton from "@material-ui/lab/Skeleton";
import IconButton from "@/components/ui/IconButton";
import EditDescriptionModal from "./EditDescriptionModal";
import Box from "@material-ui/core/Box";
import ListItemText from "@material-ui/core/ListItemText";
import { useTranslation } from "next-i18next";
import { TextVariantEnum } from "@/enums/*";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Text from "@/components/ui/Text";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { empty } from "@/utils/utils";

type Props = {
  data: LibraryItemInterface;
  setData: React.Dispatch<React.SetStateAction<LibraryItemInterface>>;
  loading: boolean;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      color: theme.palette.primary.main,
      textAlign: "left",
      fontSize: 13,
      wordBreak: "break-all",
    },
  }),
);

export default function DescriptionSection({ data, setData, loading }: Props) {
  const { t } = useTranslation("file");
  const connectionStatus = useConnectionStatus();
  const [openModal, setOpenModal] = useState<boolean>(false);
  const showDescriptionField = () => {
    setOpenModal(!openModal);
  };
  const classes = useStyles();

  const description = useMemo(
    () => (
      <ListItemText
        id="description-item"
        primary={
          <Text variant={TextVariantEnum.BODY1} className={classes.title}>
            {!empty(data.description) ? data.description : t("addFileDescription")}
          </Text>
        }
      />
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data.description, t],
  );

  return (
    <>
      <Section
        title={t("descriptionTitle")}
        secondaryAction={
          connectionStatus && (
            <IconButton
              icon="edit"
              fontSizeIcon="small"
              handleClick={() => showDescriptionField()}
              style={{ minWidth: 25 }}
            />
          )
        }
      >
        {loading && (
          <Box display="flex" flex={1} flexDirection="column" justifyContent="space-between">
            <Skeleton width="100%" />
            <Skeleton width="70%" />
            <Skeleton width="80%" />
          </Box>
        )}
        {!loading && description}
      </Section>
      <EditDescriptionModal
        open={openModal}
        closeModal={() => setOpenModal(false)}
        data={data}
        setData={setData}
      />
    </>
  );
}
