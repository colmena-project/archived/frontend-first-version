import React, { useState } from "react";
import { LibraryItemInterface } from "@/interfaces/index";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import { Grid } from "@material-ui/core";
import { useTranslation } from "next-i18next";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import Section from "@/components/pages/file/Section";
import Clickable from "@/components/ui/Clickable";

type Props = {
  data: LibraryItemInterface;
};

const useStyles = makeStyles((theme) =>
  createStyles({
    gridRoot: {
      width: "100%",
      "& strong": {
        color: theme.palette.primary.main,
        textAlign: "left",
        fontSize: 13,
      },
      "& span": {
        color: theme.palette.primary.main,
        textAlign: "left",
        fontSize: 13,
        wordBreak: "break-all",
      },
    },
    evenRow: {
      backgroundColor: "#f9f9f9",
      paddingTop: 2,
      paddingBottom: 2,
    },
    oddRow: {
      backgroundColor: "#fff",
      paddingTop: 2,
      paddingBottom: 2,
    },
  }),
);

export default function DetailsSection({ data }: Props) {
  const classes = useStyles();
  const [showDetails, setShowDetails] = useState(false);
  const { t: l } = useTranslation("library");

  const showButton = () => {
    if (showDetails)
      return (
        <Clickable handleClick={() => setShowDetails(false)}>
          <ExpandLessIcon fontSize="large" />
        </Clickable>
      );

    return (
      <Clickable handleClick={() => setShowDetails(true)}>
        <ExpandMoreIcon fontSize="large" />
      </Clickable>
    );
  };

  return (
    <Section title={l("contextMenuOptions.details")} secondaryAction={showButton()}>
      {showDetails && (
        <Grid className={classes.gridRoot} spacing={3}>
          <Grid container spacing={2} className={classes.evenRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.name")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>{data.basename}</span>
            </Grid>
          </Grid>
          <Grid container spacing={2} className={classes.oddRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.path")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>{data.filename}</span>
            </Grid>
          </Grid>
          <Grid container spacing={2} className={classes.evenRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.size")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>{data.sizeFormatted}</span>
            </Grid>
          </Grid>
          {data.createdAt && (
            <Grid container spacing={2} className={classes.oddRow}>
              <Grid item xs={6}>
                <strong>{l("detailsModal.createdAt")}</strong>
              </Grid>
              <Grid item xs={6}>
                <span>
                  {data.createdAt?.toLocaleDateString("en-US")} - {data.createdAtDescription}
                </span>
              </Grid>
            </Grid>
          )}
          <Grid container spacing={2} className={classes.evenRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.lastUpdate")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>
                {data.updatedAt?.toLocaleDateString("en-US")} - {data.updatedAtDescription}
              </span>
            </Grid>
          </Grid>
          <Grid container spacing={2} className={classes.oddRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.creator")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>{data.ownerName}</span>
            </Grid>
          </Grid>
          {data.type === "file" && (
            <Grid container spacing={2} className={classes.evenRow}>
              <Grid item xs={6}>
                <strong>{l("detailsModal.type")}</strong>
              </Grid>
              <Grid item xs={6}>
                <span>{data.mime}</span>
              </Grid>
            </Grid>
          )}
          <Grid container spacing={2} className={classes.oddRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.fileId")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>{data.fileId ?? "-"}</span>
            </Grid>
          </Grid>
          <Grid container spacing={2} className={classes.evenRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.language")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>{data.language ?? "-"}</span>
            </Grid>
          </Grid>
          <Grid container spacing={2} className={classes.evenRow}>
            <Grid item xs={6}>
              <strong>{l("detailsModal.environment")}</strong>
            </Grid>
            <Grid item xs={6}>
              <span>{l(`environment.${data.environment}`)}</span>
            </Grid>
          </Grid>
        </Grid>
      )}
    </Section>
  );
}
