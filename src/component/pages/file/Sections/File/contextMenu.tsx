import React from "react";
/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import { ContextMenuEventEnum, ContextMenuOptionEnum } from "@/enums/*";
import ContextMenuOptions from "@/components/pages/library/contextMenu";
import { LibraryItemInterface } from "@/interfaces/index";
import { useRouter } from "next/router";
import { getPath } from "@/utils/directory";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getFileLink } from "@/utils/offlineNavigation";

type Props = {
  data: LibraryItemInterface;
  setData: React.Dispatch<React.SetStateAction<LibraryItemInterface>>;
  availableOptions?: ContextMenuOptionEnum[];
};

export default function ContextMenuFile({ data, setData, availableOptions }: Props) {
  const { basename } = data;
  const router = useRouter();
  const connectionStatus = useConnectionStatus();

  const redirectToLibrary = (aliasFilename: string) =>
    router.push(`/library/${getPath(aliasFilename)}`);

  const redirectToFile = (filename: string) => {
    router.replace(getFileLink(btoa(filename), connectionStatus), undefined, { shallow: true });
  };

  const handleContextMenuUpdate = async (
    item: LibraryItemInterface,
    event: ContextMenuEventEnum,
    option: ContextMenuOptionEnum,
  ) => {
    switch (event) {
      case ContextMenuEventEnum.UPDATE:
        setData(item);

        if (option === ContextMenuOptionEnum.RENAME) {
          redirectToFile(item.filename);
        }
        break;
      case ContextMenuEventEnum.CREATE:
        if (option === ContextMenuOptionEnum.MOVE) {
          redirectToFile(item.filename);
        }
        break;
      case ContextMenuEventEnum.DELETE:
        redirectToLibrary(item.aliasFilename);
        break;
      default:
        break;
    }
  };

  const defaultAvailableOptions = [
    ContextMenuOptionEnum.EDIT,
    ContextMenuOptionEnum.COPY,
    ContextMenuOptionEnum.MOVE,
    ContextMenuOptionEnum.AVAILABLE_OFFLINE,
    ContextMenuOptionEnum.DOWNLOAD,
    ContextMenuOptionEnum.DELETE,
    ContextMenuOptionEnum.DUPLICATE,
    ContextMenuOptionEnum.PUBLISH,
    ContextMenuOptionEnum.RENAME,
  ];

  const renderAvailableOptions = availableOptions || defaultAvailableOptions;

  return (
    <ContextMenuOptions
      key={`${basename}-more-options`}
      {...data}
      availableOptions={renderAvailableOptions}
      onChange={handleContextMenuUpdate}
    />
  );
}
