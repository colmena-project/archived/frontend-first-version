import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import FlexBox from "@/components/ui/FlexBox";

export const PageWrapper = styled(FlexBox)({
  background: "url(/images/bg_call.png) repeat-y right",
  backgroundColor: "white",
});

export const PlayerContainer = styled(Box)({
  width: "90%",
  margin: "0 auto",
});
