import React, { useEffect, useState } from "react";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { LibraryItemInterface, TimeDescriptionInterface } from "@/interfaces/index";
import { useTranslation } from "next-i18next";
import { findByFilename } from "@/store/idb/models/files";
import Library, { applyLocalItemInterface, mergeEnvItems } from "@/components/pages/library";
import { getDataFile } from "@/services/webdav/files";
import { toast } from "@/utils/notifications";
import LayoutApp from "@/components/statefull/LayoutApp";
import ContextMenuFile from "@/components/pages/file/Sections/File/contextMenu";
import { JustifyContentEnum, ListTypeEnum, PlayerTypeEnum } from "@/enums/*";
import Box from "@material-ui/core/Box";
import FileHeader from "@/components/pages/file/Header";
import { isSafari } from "@/utils/utils";
import DescriptionSection from "@/components/pages/file/Sections/Description";
import TagsSection from "@/components/pages/file/Sections/Tags";
import DetailsSection from "@/components/pages/file/Sections/Details";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { PageWrapper, PlayerContainer } from "@/components/pages/file/FilePage/styled";
import { useRouter } from "next/router";

type Props = {
  id: string;
};
export default function FilePage({ id }: Props) {
  const router = useRouter();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const connectionStatus = useConnectionStatus();
  const [filename, setFilename] = useState<string | null>(null);
  const [data, setData] = useState<LibraryItemInterface>({} as LibraryItemInterface);

  const { t: c } = useTranslation("common");
  const { t } = useTranslation("file");
  const [loading, setLoading] = useState(false);
  const timeDescription: TimeDescriptionInterface = c("timeDescription", { returnObjects: true });

  const getFile = async () => {
    if (!filename) {
      return;
    }

    setLoading(true);
    let remoteItem = null;
    let localItem = null;
    try {
      const localResult = await findByFilename(userRdx.user.id, filename);
      if (localResult) {
        localItem = applyLocalItemInterface(localResult, timeDescription);
      }
    } catch (e) {
      console.log(e);
    }

    if (connectionStatus) {
      try {
        const remoteResult = await getDataFile(userRdx.user.id, filename, timeDescription);
        if (remoteResult) {
          remoteItem = remoteResult;
        }
      } catch (e) {
        console.log(e);
      }
    }

    const item = mergeEnvItems(localItem, remoteItem);
    if (item) {
      setData(item);
      setLoading(false);
    } else {
      // errorNotFound();
    }
  };

  const errorNotFound = () => {
    toast(t("messages.fileNotFound"), "error");
    router.push("/library");
  };

  useEffect(() => {
    try {
      setFilename(atob(String(id)));
      getFile();
    } catch (e) {
      errorNotFound();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filename]);

  if (!filename) return null;

  return (
    <>
      <LayoutApp
        title=""
        back
        drawer={false}
        notifications={false}
        rightExtraElement={<ContextMenuFile key="context-menu" data={data} setData={setData} />}
      >
        <PageWrapper
          justifyContent={JustifyContentEnum.FLEXSTART}
          extraStyle={{ padding: 0, margin: 0 }}
        >
          <Box width="100%">
            <FileHeader data={data} setData={setData} loading={loading} />
            <PlayerContainer>
              <Library
                items={[data]}
                listType={ListTypeEnum.LIST}
                isLoading={loading}
                itemsQuantitySkeleton={1}
                playerType={isSafari() ? PlayerTypeEnum.WAVE : PlayerTypeEnum.CIRCLE}
              />
            </PlayerContainer>
            <DescriptionSection data={data} setData={setData} loading={loading} />
            <TagsSection data={data} />
            <DetailsSection data={data} />
          </Box>
        </PageWrapper>
      </LayoutApp>
    </>
  );
}
