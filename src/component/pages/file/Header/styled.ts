import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme.d";
import IconButton from "@/components/ui/IconButton";

export const ButtonEdit = styled(IconButton)({
  minWidth: "auto",
});

export const Wrapper = styled(Box)({
  display: "flex",
  padding: "0 16px",
  flexDirection: "row",
  marginTop: 20,
  marginBottom: 20,
  justifyContent: "flex-start",
  alignItems: "flex-start",
});

export const WrapperDescription = styled(Box)({
  display: "flex",
  flexDirection: "column",
  width: "100%",
  paddingLeft: 15,
});

export const BoxTitle = styled(Box)({
  display: "flex",
  width: "100%",
  justifyContent: "space-between",
  alignItems: "center",
  marginLeft: 8,
});

export const WrapperSubtitle = styled(Box)({
  display: "flex",
  width: "100%",
  flexDirection: "column",
  marginLeft: 8,
});

export const Filename = styled(Text)(({ theme }: { theme: Theme }) => ({
  fontSize: 16,
  fontWeight: "bold",
  color: theme.palette.primary.main,
  textAlign: "left",
}));

export const Subtitle = styled(Text)({
  fontSize: 12,
  textAlign: "left",
});
