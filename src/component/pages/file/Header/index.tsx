import React, { useState } from "react";
import { LibraryItemInterface } from "@/interfaces/index";
import Avatar from "@/components/ui/Avatar";
import DownloadModal from "@/components/pages/library/contextMenu/DownloadModal";
import EditTitleModal from "./EditTitleModal";
import { useTranslation } from "next-i18next";
import theme from "@/styles/theme";
import { empty, getFirstLettersOfTwoFirstNames } from "@/utils/utils";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import {
  BoxTitle,
  ButtonEdit,
  Filename,
  Subtitle,
  Wrapper,
  WrapperDescription,
  WrapperSubtitle,
} from "./styled";

type Props = {
  data: LibraryItemInterface;
  setData: React.Dispatch<React.SetStateAction<LibraryItemInterface>>;
  loading: boolean;
};

export default function FileHeader({ data, setData }: Props) {
  const { t } = useTranslation("file");
  const [openDownloadModal, setOpenDownloadModal] = useState(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const connectionStatus = useConnectionStatus();

  const handleOpenDownloadModal = (opt: boolean) => {
    setOpenDownloadModal(opt);
  };

  return (
    <>
      <Wrapper>
        <Avatar size={10} variant="square" name={getFirstLettersOfTwoFirstNames(data.title)} />
        <WrapperDescription>
          <BoxTitle>
            <Filename>{empty(data.title) ? t("addTitle") : data.title}</Filename>
            {connectionStatus && (
              <ButtonEdit
                icon="edit"
                handleClick={() => setOpenModal(true)}
                iconColor={theme.palette.primary.main}
                fontSizeIcon={18}
              />
            )}
          </BoxTitle>
          <WrapperSubtitle>
            <Subtitle>
              {t("byTitle")} {data.ownerName}
            </Subtitle>
            <Subtitle>
              {data.createdAt?.toLocaleDateString("en-US")} - {data.createdAtDescription}
            </Subtitle>
          </WrapperSubtitle>
        </WrapperDescription>
      </Wrapper>
      {data && (
        <>
          <DownloadModal
            key={`${data.filename}-download-modal`}
            open={openDownloadModal}
            handleOpen={() => handleOpenDownloadModal(false)}
            filename={data.filename}
            basename={data.basename}
            mime={data.mime}
            arrayBufferBlob={data.arrayBufferBlob}
            startDownloadOnOpen={data.extension !== "md"}
          />
          <EditTitleModal
            open={openModal}
            closeModal={() => setOpenModal(false)}
            data={data}
            setData={setData}
          />
        </>
      )}
    </>
  );
}
