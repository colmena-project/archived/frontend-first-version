/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useCallback, useEffect, useState } from "react";
import Image from "next/image";
import theme from "@/styles/theme";
import { LibraryCardItemInterface } from "@/interfaces/index";
import { AllIconProps, PropsUserSelector } from "@/types/*";
import { getAudioPath, getPrivatePath, getPublicPath, getTalkPath } from "@/utils/directory";
import Box from "@material-ui/core/Box";
import FileIcon from "@/components/ui/FileIcon";
import { isAudioFile, isImageFile, removeSpecialCharacters } from "@/utils/utils";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import CirclePlayer from "@/components/ui/CirclePlayer";
import { PlayerTypeEnum } from "@/enums/*";
import SvgIconAux from "@/components/ui/SvgIcon";
import { blobFile } from "@/services/webdav/files";
import { useSelector } from "react-redux";
import { BoxWrapperPlayer, ClickablePlayer, WrapperImage, ImageModal } from "./styled";

import Modal from "@/components/ui/Modal";
import {
  createFile as createQuickBlob,
  findByBasename as findQuickBlobByBasename,
} from "@/store/idb/models/filesQuickBlob";

interface LibraryCardItemAvatarInterface extends LibraryCardItemInterface {
  handleClick: () => void;
  handleAudioState: (audioState: string) => void;
  audioFinishStop: boolean;
  playerType?: PlayerTypeEnum;
  audioState?: "play" | "pause" | "stop";
}

const CardItemAvatar = (cardItem: LibraryCardItemAvatarInterface) => {
  const {
    basename,
    environment,
    extension,
    filename,
    id,
    image,
    mime,
    type,
    handleAudioState,
    audioFinishStop = false,
    arrayBufferBlob,
    playerType = PlayerTypeEnum.CIRCLE,
    audioState = "stop",
  } = cardItem;
  const connectionStatus = useConnectionStatus();
  const [imageFile, setImageFile] = useState(image);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [openModal, setOpenModal] = useState(false);

  let iconColor = theme.palette.variation1.main;
  let folderSecondIcon: AllIconProps | null | undefined = null;
  if (type === "directory") {
    iconColor = theme.palette.variation1.main;

    switch (filename) {
      case getPublicPath():
        folderSecondIcon = "global";
        break;
      case getPrivatePath():
        folderSecondIcon = "private";
        break;
      case getAudioPath():
        folderSecondIcon = "microphone";
        break;
      case getTalkPath():
        folderSecondIcon = "share";
        break;
      default:
        if (filename && filename.indexOf("/") < 0) {
          folderSecondIcon = "panal_folder";
        }
        break;
    }
  }

  useEffect(() => {
    if (audioFinishStop && audioState === "play") handleAudioState("pause");
  }, [audioFinishStop]);

  const handleToggleStateAudioPlaying = () => {
    const plPa = audioState === "play" ? "pause" : "play";
    handleAudioState(plPa);
  };

  if (type === "file" && isAudioFile(mime)) {
    switch (playerType) {
      case PlayerTypeEnum.CIRCLE:
        return (
          <CirclePlayer file={arrayBufferBlob} filename={filename} environment={environment} />
        );
      case PlayerTypeEnum.WAVE:
        return (
          <BoxWrapperPlayer>
            <ClickablePlayer handleClick={handleToggleStateAudioPlaying}>
              <SvgIconAux
                icon={audioState === "play" ? "basic_pause" : "basic_play"}
                htmlColor={theme.palette.primary.main}
                fontSize={16}
              />
            </ClickablePlayer>
          </BoxWrapperPlayer>
        );
      default:
        break;
    }
  }

  const getImage = useCallback(async () => {
    const localFile = await findQuickBlobByBasename(
      userRdx.user.id,
      removeSpecialCharacters(filename),
    );
    let fileUrl = null;
    if (!localFile) {
      const blob = await blobFile(userRdx.user.id, filename);
      if (blob instanceof Blob) {
        fileUrl = URL.createObjectURL(blob);
        await createQuickBlob({
          basename: removeSpecialCharacters(filename),
          userId: userRdx.user.id,
          arrayBufferBlob: blob,
        });
      }
    } else {
      fileUrl = URL.createObjectURL(localFile?.arrayBufferBlob);
    }

    if (fileUrl) {
      setImageFile(fileUrl);
    }
  }, []);

  useEffect(() => {
    if (imageFile === undefined && isImageFile(mime) && connectionStatus) {
      getImage();
    }
  }, []);

  return (
    <>
      {imageFile !== undefined && openModal && (
        <Modal withButtonToClose open={openModal} handleClose={() => setOpenModal(false)}>
          <ImageModal alt={`image-${basename}-${id}`} src={imageFile} />
        </Modal>
      )}
      <Box data-testid={`avatar-library-item-${basename}`}>
        {imageFile !== undefined ? (
          <WrapperImage onClick={() => setOpenModal(true)}>
            <Image alt={`image-${basename}-${id}`} width={60} height={60} src={imageFile} />
          </WrapperImage>
        ) : (
          <Box width={60} px={1}>
            <FileIcon
              iconColor={iconColor}
              folderSecondIcon={folderSecondIcon}
              extension={extension}
              environment={environment}
              mime={mime}
              type={type === "directory" ? type : "file"}
            />
          </Box>
        )}
      </Box>
    </>
  );
};

export default React.memo(CardItemAvatar);
