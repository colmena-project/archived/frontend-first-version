import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Clickable from "@/components/ui/Clickable";
import { Theme } from "@material-ui/core/styles/createTheme.d";
import Text from "@/components/ui/Text";

export const BoxWrapperPlayer = styled(Box)(({ theme }: { theme: Theme }) => ({
  position: "relative",
  display: "flex",
  width: 40,
  height: 40,
  borderRadius: "50%",
  border: "1px solid",
  borderColor: theme.palette.grey["400"],
  marginRight: 7,
}));

export const ClickablePlayer = styled(Clickable)({
  top: 0,
  left: 3,
  bottom: 0,
  right: 0,
  position: "absolute",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

export const WrapperImage = styled(Box)({
  width: "100%",
  height: "55px",
  maxWidth: "44px",
  cursor: "pointer",

  "& img": {
    borderRadius: "7px",

    height: "55px!important",
    objectFit: "cover",
  },
});

export const ImageModal = styled("img")({
  width: "100%",
  height: "auto",
});

export const TitleWrapper = styled(Clickable)({
  display: "flex",
  flexDirection: "row",
  flexWrap: "nowrap",
});

export const Title = styled(Text)({
  alignSelf: "stretch",
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
});

export const Extension = styled(Text)({
  alignSelf: "stretch",
  flexGrow: 1,
});
