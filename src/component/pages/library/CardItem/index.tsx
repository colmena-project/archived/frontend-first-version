/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { LibraryCardItemInterface } from "@/interfaces/index";
import Box from "@material-ui/core/Box";
import VerticalItemList from "@/components/pages/library/VerticalItemList";
import GridItemList from "@/components/ui/GridItemList";
import { PropsUserSelector } from "@/types/index";
import { useSelector } from "react-redux";
import CardItemSubtitle from "./Subtitle";
import CardItemAvatar from "./Avatar";
import CardItemStatus from "./ItemStatus";
import CardItemTitle from "./Title";
import Text from "@/components/ui/Text";
import { EnvironmentEnum, TextVariantEnum } from "@/enums/*";
import {
  createFile as createQuickBlob,
  findByBasename as findQuickBlobByBasename,
  removeFile,
} from "@/store/idb/models/filesQuickBlob";
import { removeSpecialCharacters } from "@/utils/utils";
import { listFile } from "@/services/webdav/files";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

const CardItem = (cardItem: LibraryCardItemInterface) => {
  const {
    id,
    basename,
    filename,
    environment,
    orientation,
    size,
    options,
    bottomOptions,
    handleOpenCard,
    isDisabled,
    arrayBufferBlob,
    subtitle,
    extension,
    playerType,
  } = cardItem;
  const handleClick = useCallback(() => {
    if (!isDisabled && typeof handleOpenCard === "function") {
      handleOpenCard(cardItem);
    }
  }, [cardItem, handleOpenCard, isDisabled]);
  const [audioState, setAudioState] = useState<"play" | "pause" | "stop">("stop");
  const [audioFinishStop, setAudioFinishStop] = useState(false);
  const [loadingAudio, setLoadingAudio] = useState(false);
  const [arrayBufferIntern, setArrayBufferIntern] = useState<ArrayBuffer | null>(null);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const connectionStatus = useConnectionStatus();

  const badgeStatusGrid = useMemo(() => <CardItemStatus {...cardItem} />, [cardItem]);

  const formatPrimaryWithSecondaryGrid = (
    <Box
      component="span"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <CardItemTitle
        title={basename}
        extension={extension}
        handleClick={handleClick}
        textStyle={{ maxWidth: "100%", textAlign: "center" }}
      />
      <CardItemSubtitle key={`title-${id}`} {...cardItem} />
    </Box>
  );

  const subtitleVerticalItem = useMemo(() => {
    if (subtitle) {
      return (
        <Text variant={TextVariantEnum.CAPTION} noWrap>
          {subtitle}
        </Text>
      );
    }

    return (
      <Box component="span" display="flex" alignItems="center">
        <CardItemSubtitle key={`title-${id}`} {...cardItem} />
        {badgeStatusGrid}
      </Box>
    );
  }, [cardItem]);

  const loadAudioBlob = async () => {
    const localFile = await findQuickBlobByBasename(
      userRdx.user.id,
      removeSpecialCharacters(filename),
    );
    if (localFile) {
      setArrayBufferIntern(localFile?.arrayBufferBlob);
      return;
    }

    if (connectionStatus && environment !== EnvironmentEnum.LOCAL) {
      try {
        setLoadingAudio(true);
        const result: any = await listFile(userRdx.user.id, filename);
        removeFile(userRdx.user.id, removeSpecialCharacters(filename));
        await createQuickBlob({
          basename: removeSpecialCharacters(filename),
          userId: userRdx.user.id,
          arrayBufferBlob: result,
        });
        setArrayBufferIntern(result);
      } catch (e) {
        console.log(e);
        setAudioState("stop");
      } finally {
        setLoadingAudio(false);
      }
    }
  };

  useEffect(() => {
    (async () => {
      if (audioState === "play" && !arrayBufferIntern) {
        setAudioFinishStop(false);
        await loadAudioBlob();
      } else if (audioState === "stop") {
        setArrayBufferIntern(null);
        setAudioFinishStop(true);
      }
    })();
  }, [audioState]);

  if (!connectionStatus && environment === EnvironmentEnum.REMOTE) {
    return null;
  }

  return (
    <>
      {orientation === "vertical" ? (
        <VerticalItemList
          key={`${basename}-card`}
          avatar={
            <CardItemAvatar
              {...cardItem}
              handleClick={handleClick}
              handleAudioState={(audioState: "play" | "pause") => setAudioState(audioState)}
              audioFinishStop={audioFinishStop}
              playerType={playerType}
              audioState={audioState}
            />
          }
          primary={
            <CardItemTitle title={basename} extension={extension} handleClick={handleClick} />
          }
          secondary={subtitleVerticalItem}
          options={options && options(cardItem)}
          handleClick={handleClick}
          audioState={audioState}
          environment={environment}
          filename={filename}
          basename={basename}
          size={size}
          arrayBufferBlob={arrayBufferBlob || arrayBufferIntern}
          handleAudioFinish={() => setAudioState("stop")}
        />
      ) : (
        <GridItemList
          key={`${basename}-card`}
          avatar={
            <CardItemAvatar
              {...cardItem}
              handleClick={handleClick}
              handleAudioState={(audioState: "play" | "pause") => setAudioState(audioState)}
              audioFinishStop={audioFinishStop}
              playerType={playerType}
              audioState={audioState}
            />
          }
          primaryFormatted={formatPrimaryWithSecondaryGrid}
          primary={basename}
          topOptions={options && options(cardItem)}
          bottomOptions={bottomOptions && bottomOptions(cardItem, badgeStatusGrid)}
          handleClick={handleClick}
          environment={environment}
          filename={filename}
          basename={basename}
          size={size}
          arrayBufferBlob={arrayBufferBlob || arrayBufferIntern}
          audioState={audioState}
          handleAudioFinish={() => setAudioState("stop")}
        />
      )}
    </>
  );
};

export default React.memo(CardItem);
