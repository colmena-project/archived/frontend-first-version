import React from "react";
import { LibraryCardItemInterface } from "@/interfaces/index";
import { EnvironmentEnum } from "@/enums/*";
import { Box, Button, ClickAwayListener, Tooltip } from "@material-ui/core";
import SvgIcon from "@/components/ui/SvgIcon";
import theme from "@/styles/theme";
import { useTranslation } from "next-i18next";
import { v4 as uuid } from "uuid";
import { AllIconProps } from "@/types/index";

const CardItemStatus = ({ environment }: LibraryCardItemInterface) => {
  const { t } = useTranslation("library");
  const [openTooltip, setOpenTooltip] = React.useState<boolean | string>(false);
  const status: Array<any> = [];

  const handleTooltipClose = () => {
    setOpenTooltip(false);
  };

  const handleTooltipOpen = (opt: boolean | string) => {
    setOpenTooltip(opt);
  };

  const renderItem = (title: string, type: string, icon: AllIconProps) => (
    <Box component="span" key={uuid()} style={{ display: "flex", padding: "0 4px" }}>
      <ClickAwayListener onClickAway={handleTooltipClose}>
        <Tooltip
          title={<>{title}</>}
          onClose={handleTooltipClose}
          open={openTooltip === type}
          disableFocusListener
          disableHoverListener
          disableTouchListener
          PopperProps={{
            disablePortal: true,
          }}
          style={{ margin: "5px 0" }}
        >
          <Button
            onClick={() => handleTooltipOpen(type)}
            variant="text"
            style={{ padding: 0, minWidth: "auto" }}
          >
            <SvgIcon icon={icon} fontSize={17} htmlColor={theme.palette.gray.light} />
          </Button>
        </Tooltip>
      </ClickAwayListener>
    </Box>
  );

  if (environment === EnvironmentEnum.LOCAL) {
    status.push(renderItem(t("itemStatus.offline"), "offline", "offline"));
  }

  if (environment === EnvironmentEnum.BOTH) {
    status.push(renderItem(t("itemStatus.availableOffline"), "sync", "sync_file"));
  }

  // if (environment === EnvironmentEnum.REMOTE) {
  //   status.push(renderItem(t("itemStatus.availableOffline"), "online", "online_file"));
  // }

  return (
    <>
      {status && (
        <Box component="span" display="flex" alignItems="center">
          {status.map((item) => item)}
        </Box>
      )}
    </>
  );
};

export default React.memo(CardItemStatus);
