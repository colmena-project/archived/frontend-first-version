import { TextVariantEnum } from "@/enums/*";
import React from "react";
import { Extension, Title, TitleWrapper } from "@/components/pages/library/CardItem/styled";

interface LibraryCardItemTitleInterface {
  title: string | React.ReactNode;
  handleClick?: (event: any) => void | undefined | undefined;
  // eslint-disable-next-line @typescript-eslint/ban-types
  textStyle?: object | undefined;
  extension?: string;
}

const CardItemTitle = ({
  title,
  extension,
  textStyle,
  handleClick,
}: LibraryCardItemTitleInterface) => {
  let titleName = title;
  if (extension && typeof titleName === "string") {
    const regex = new RegExp(`\\.${extension}$`);
    titleName = `${titleName.replace(regex, "")}.`;
  }

  return (
    <TitleWrapper handleClick={handleClick}>
      <Title variant={TextVariantEnum.BODY1} noWrap style={textStyle}>
        {titleName}
      </Title>
      {extension && (
        <Extension variant={TextVariantEnum.BODY1} style={textStyle}>
          {extension}
        </Extension>
      )}
    </TitleWrapper>
  );
};

export default React.memo(CardItemTitle);
