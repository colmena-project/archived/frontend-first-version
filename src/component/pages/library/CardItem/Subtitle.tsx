import { TextVariantEnum } from "@/enums/*";
import React from "react";
import { Box } from "@material-ui/core";
import { LibraryCardItemInterface } from "@/interfaces/index";
import Text from "@/components/ui/Text";
import { v4 as uuid } from "uuid";
import theme from "@/styles/theme";

const CardItemSubtitle = ({
  sizeFormatted,
  updatedAtDescription,
  createdAtDescription,
}: LibraryCardItemInterface) => {
  const description: Array<any> = [];
  description.push(
    <Text
      key={`sizeFormatted-${uuid()}`}
      variant={TextVariantEnum.CAPTION}
      style={{ color: theme.palette.gray.dark }}
    >
      {sizeFormatted},
    </Text>,
  );

  description.push(
    <Text
      key={`dateFormatted-${uuid()}`}
      variant={TextVariantEnum.CAPTION}
      style={{ color: theme.palette.gray.dark }}
    >
      {updatedAtDescription ?? createdAtDescription}
    </Text>,
  );

  return (
    <>
      {description && (
        <Box component="span" display="flex" alignItems="center">
          {description.map((item) => (
            <Box key={uuid()} component="span" pr={1}>
              {item}
            </Box>
          ))}
        </Box>
      )}
    </>
  );
};

export default React.memo(CardItemSubtitle);
