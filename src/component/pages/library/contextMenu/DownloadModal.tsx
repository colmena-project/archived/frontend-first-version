import React, { useState, useEffect, useCallback } from "react";
import { useTranslation } from "next-i18next";
import { blobFile } from "@/services/webdav/files";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { arrayBufferToBlob } from "blob-util";
import { downloadFile } from "@/utils/utils";
import { toast } from "@/utils/notifications";
import Modal from "@/components/ui/Modal";
import MenuItem from "@material-ui/core/MenuItem";

import showdown from "showdown";
import download from "downloadjs";
import { convertHtmlToRtf } from "@/utils/convertHtmlToRtf";
import { removeFileExtension } from "@/utils/removeFileExtension";

type Props = {
  open: boolean;
  handleOpen: (opt: boolean) => void;
  filename: string;
  basename: string;
  mime?: string;
  arrayBufferBlob?: ArrayBuffer | undefined;
  startDownloadOnOpen: boolean;
};

function DownloadModal({
  open,
  handleOpen,
  filename,
  basename,
  mime,
  arrayBufferBlob,
  startDownloadOnOpen,
}: Props) {
  const { t } = useTranslation("library");

  const [downloadError, setDownloadError] = useState<boolean>(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const [blob, setBlob] = useState<boolean | Blob>(false);

  useEffect(() => {
    if (open === true) {
      (async () => {
        setDownloadError(false);
        let dataBlob: Blob | null | boolean = null;
        if (arrayBufferBlob) {
          dataBlob = localUrlDownload(arrayBufferBlob);
        } else {
          dataBlob = await blobFile(userRdx.user.id, filename);
        }

        setBlob(dataBlob);

        if (startDownloadOnOpen) {
          if (dataBlob instanceof Blob) {
            downloadFile(dataBlob, basename, mime);
            handleOpen(false);
          } else {
            setDownloadError(true);
          }
        }
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, filename, arrayBufferBlob]);

  useEffect(() => {
    if (downloadError) {
      toast(t("messages.cannotDownloadFile"), "error");
      handleOpen(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [downloadError]);

  const localUrlDownload = (arrayBufferBlob: ArrayBuffer) => arrayBufferToBlob(arrayBufferBlob);

  const downloadMarkDown = () => {
    if (blob instanceof Blob) {
      downloadFile(blob, basename, mime);
      handleOpen(false);
    } else {
      setDownloadError(true);
    }
  };

  const downloadRtf = async () => {
    if (blob instanceof Blob) {
      const reader = new FileReader();
      reader.onload = () => {
        const markdown: any = reader.result;
        const converter = new showdown.Converter();
        const html = converter.makeHtml(markdown);

        const rtf = convertHtmlToRtf(html);

        download(String(rtf), `${removeFileExtension(basename)}.rtf`, "application/rtf");
      };

      reader.readAsText(blob);

      handleOpen(false);
    } else {
      setDownloadError(true);
    }
  };

  const handleClose = useCallback(() => {
    handleOpen(false);
  }, [handleOpen]);

  if (startDownloadOnOpen) {
    return null;
  }

  return (
    <>
      <Modal
        title={t("downloadModal.title")}
        data-testid="modal-download-item"
        open={open}
        handleClose={handleClose}
      >
        <MenuItem
          key="download-item-md"
          onClick={() => downloadMarkDown()}
          data-testid="menu-download-md"
        >
          {t("downloadModal.md")}
        </MenuItem>
        <MenuItem
          key="download-item-rtf"
          onClick={() => downloadRtf()}
          data-testid="menu-download-rtf"
        >
          {t("downloadModal.rtf")}
        </MenuItem>
      </Modal>
    </>
  );
}

export default React.memo(DownloadModal);
