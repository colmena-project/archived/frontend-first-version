import { styled } from "@material-ui/core/styles";
import { Form as FormBase } from "formik";
import Button from "@/components/ui/Button";

export const Form = styled(FormBase)({
  "& .MuiTextField-root": {
    width: "100%",
  },
});

export const SubmitButton = styled(Button)({
  marginLeft: 10,
});
