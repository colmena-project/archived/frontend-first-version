import React, { useCallback } from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { FilterEnum, OrderEnum } from "@/enums/index";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useTranslation } from "react-i18next";
import { Filter, Heading } from "@/components/pages/library/FiltersDrawer/styled";

type Props = {
  open: boolean;
  handleClose: () => void;
  filterItems: (filter: string | FilterEnum) => void;
  orderItems: (order: OrderEnum) => void;
  order: OrderEnum;
  filter: string | FilterEnum;
  isRootPath: boolean;
};

const TemporaryFiltersDrawer = ({
  open,
  filterItems,
  orderItems,
  handleClose,
  order,
  filter,
  isRootPath,
}: Props) => {
  const [expanded, setExpanded] = React.useState<string | boolean>("order");
  const { t } = useTranslation("library");

  const handleChange = (panel: string) => (event: any, newExpanded: string | boolean) => {
    setExpanded(newExpanded ? panel : false);
  };

  const filterActive = useCallback(
    (currentFilter: string | FilterEnum) => filter === currentFilter,
    [filter],
  );
  const orderActive = useCallback(
    (currentOrder: string | OrderEnum) => order === currentOrder,
    [order],
  );

  return (
    <Drawer anchor="right" open={open} onClose={handleClose}>
      <Accordion expanded={expanded === "order"} onChange={handleChange("order")}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="order-content"
          id="order-header"
        >
          <Heading>{t("sort.title")}</Heading>
        </AccordionSummary>
        <AccordionDetails>
          <List>
            {isRootPath && (
              <ListItem
                button
                key="sort-highlights"
                onClick={() => orderItems(OrderEnum.HIGHLIGHT)}
              >
                <Filter
                  primary={t("sort.highlights")}
                  isActive={orderActive(OrderEnum.HIGHLIGHT)}
                />
              </ListItem>
            )}
            <ListItem button key="sort-newest" onClick={() => orderItems(OrderEnum.LATEST_FIRST)}>
              <Filter primary={t("sort.newest")} isActive={orderActive(OrderEnum.LATEST_FIRST)} />
            </ListItem>
            <ListItem button key="sort-oldest" onClick={() => orderItems(OrderEnum.OLDEST_FIST)}>
              <Filter primary={t("sort.oldest")} isActive={orderActive(OrderEnum.OLDEST_FIST)} />
            </ListItem>
            <ListItem
              button
              key="sort-alphabetical"
              onClick={() => orderItems(OrderEnum.ASC_ALPHABETICAL)}
            >
              <Filter
                primary={t("sort.alphabeticalOrder")}
                isActive={orderActive(OrderEnum.ASC_ALPHABETICAL)}
              />
            </ListItem>
            <ListItem
              button
              key="sort-descending-alphabetical"
              onClick={() => orderItems(OrderEnum.DESC_ALPHABETICAL)}
            >
              <Filter
                primary={t("sort.descendingAlphabeticalOrder")}
                isActive={orderActive(OrderEnum.DESC_ALPHABETICAL)}
              />
            </ListItem>
          </List>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === "filter"} onChange={handleChange("filter")}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="filters-content"
          id="filters-header"
        >
          <Heading>{t("filter.title")}</Heading>
        </AccordionSummary>
        <AccordionDetails>
          <List>
            <ListItem button key="filter-all" onClick={() => filterItems("")}>
              <Filter primary={t("filter.all")} isActive={filterActive("")} />
            </ListItem>
            <ListItem
              button
              key="filter-available-offline"
              onClick={() => filterItems(FilterEnum.SYNC)}
            >
              <Filter
                primary={t("filter.availableOffline")}
                isActive={filterActive(FilterEnum.SYNC)}
              />
            </ListItem>
            <ListItem button key="filter-audios" onClick={() => filterItems(FilterEnum.AUDIO)}>
              <Filter primary={t("filter.audios")} isActive={filterActive(FilterEnum.AUDIO)} />
            </ListItem>
            <ListItem button key="filter-images" onClick={() => filterItems(FilterEnum.IMAGE)}>
              <Filter primary={t("filter.images")} isActive={filterActive(FilterEnum.IMAGE)} />
            </ListItem>
            <ListItem button key="filter-texts" onClick={() => filterItems(FilterEnum.TEXT)}>
              <Filter primary={t("filter.texts")} isActive={filterActive(FilterEnum.TEXT)} />
            </ListItem>
          </List>
        </AccordionDetails>
      </Accordion>
      {/* <Accordion expanded={expanded === "language"} onChange={handleChange("language")}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="languages-content"
          id="languages-header"
        >
          <Typography className={classes.heading}>{t("language.title")}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <List>
            {getSystemLanguages(c).map((item) => (
              <ListItem
                button
                key={uuid()}
                onClick={() => toast(c("featureUnavailable"), "warning")}
              >
                <ListItemText primary={item.language} />
              </ListItem>
            ))}
          </List>
        </AccordionDetails>
      </Accordion> */}
    </Drawer>
  );
};

export default React.memo(TemporaryFiltersDrawer);
