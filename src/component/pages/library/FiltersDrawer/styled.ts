import { styled } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Theme } from "@material-ui/core/styles/createTheme.d";
import ListItemText, { ListItemTextProps } from "@material-ui/core/ListItemText";

export const Heading = styled(Typography)(({ theme }: { theme: Theme }) => ({
  fontSize: theme.typography.pxToRem(15),
  fontWeight: "normal",
}));

interface FilterInterface extends ListItemTextProps {
  isActive: boolean;
}

export const Filter = styled(ListItemText)(({ isActive }: FilterInterface) => ({
  "& .MuiTypography-root": { fontWeight: isActive ? "bold" : undefined },
}));
