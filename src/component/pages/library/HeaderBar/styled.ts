import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { JustifyContentEnum } from "@/enums/*";

export const HeaderBarWrapper = styled(Box)({
  justifyContent: JustifyContentEnum.SPACEBETWEEN,
  flexDirection: "row",
  display: "flex",
  width: "100%",
});

export const BreadcrumbWrapper = styled(Box)({
  flex: 1,
  textAlign: "left",
  overflow: "hidden",
  width: "100%",
});

export const Options = styled(Box)({
  display: "flex",
  flexWrap: "nowrap",
  flexDirection: "row",
});
