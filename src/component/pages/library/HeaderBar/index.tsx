import React, { useState, useEffect, useCallback } from "react";
import IconButton from "@material-ui/core/IconButton";
import SvgIcon from "@/components/ui/SvgIcon";
import { FilterEnum, ListTypeEnum, OrderEnum } from "@/enums/index";
import { generateBreadcrumb, removeCornerSlash } from "@/utils/utils";
import { BreadcrumbItemInterface } from "@/interfaces/index";
import Breadcrumb from "@/components/ui/Breadcrumb";
import { AllIconProps } from "@/types/index";
import TemporaryFiltersDrawer from "../FiltersDrawer";
import { getPublicPath, getTalkPath, isRootPath } from "@/utils/directory";
import { useDispatch } from "react-redux";
import { setCurrentAudioPlaying } from "@/store/actions/library/index";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import {
  BreadcrumbWrapper,
  HeaderBarWrapper,
  Options,
} from "@/components/pages/library/HeaderBar/styled";

type Props = {
  path: string | string[] | undefined;
  currentPath: string;
  listType: string;
  setListType: React.Dispatch<React.SetStateAction<ListTypeEnum>>;
  setFilter: React.Dispatch<React.SetStateAction<string | FilterEnum>>;
  setOrder: React.Dispatch<React.SetStateAction<OrderEnum>>;
  order: OrderEnum;
  filter: string | FilterEnum;
  pathExists: boolean;
  handleNavigate: (dir: BreadcrumbItemInterface) => void;
  hasFilter?: boolean;
  canChangeList?: boolean;
  firstBreadcrumbItem?: BreadcrumbItemInterface;
  isDisabled?: boolean;
  rootPath?: string;
  hasSearch?: boolean;
};

const defineIconListType = (type: string) => (type === ListTypeEnum.LIST ? "grid" : "checklist");

function HeaderBar({
  path,
  currentPath,
  listType,
  setListType,
  setFilter,
  setOrder,
  order,
  filter,
  pathExists,
  handleNavigate,
  hasFilter = true,
  canChangeList = true,
  firstBreadcrumbItem,
  isDisabled = false,
  rootPath = "/",
  hasSearch = true,
}: Props) {
  const [openFilterDrawer, setOpenFilterDrawer] = useState(false);
  const [iconListType, setIconListType] = useState<AllIconProps>(defineIconListType(listType));
  const router = useRouter();
  const dispatch = useDispatch();
  const { t } = useTranslation("library");
  const [breadcrumb, setBreadcrumb] = useState<Array<BreadcrumbItemInterface>>(
    [] as Array<BreadcrumbItemInterface>,
  );

  useEffect(() => {
    let currentPath = path;
    if (rootPath !== "/" && currentPath) {
      if (typeof currentPath === "object") {
        currentPath = currentPath.join("/");
      }

      const regex = new RegExp(`^${rootPath}`);
      currentPath = currentPath.replace(regex, "").split("/");
    }

    if (firstBreadcrumbItem) {
      const generatedBreadcrumb = generateBreadcrumb(currentPath, firstBreadcrumbItem.path);
      let paths = generatedBreadcrumb;
      if (firstBreadcrumbItem.path !== "/library") {
        const newBreadcrumbItem = {
          ...firstBreadcrumbItem,
          isCurrent: generatedBreadcrumb.length === 0,
        };

        paths = [newBreadcrumbItem, ...generatedBreadcrumb];
      }

      if (paths.length > 2) {
        const firstPath: BreadcrumbItemInterface = paths.slice(
          paths.length - 3,
          paths.length - 2,
        )[0];
        firstPath.icon = "dots_three_horizontal";
        paths = [firstPath, ...paths.slice(paths.length - 2, undefined)];
      }

      setBreadcrumb(handleBreadcrumb(paths));
    } else {
      const generatedBreadcrumb = generateBreadcrumb(currentPath);
      setBreadcrumb(handleBreadcrumb(generatedBreadcrumb));
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [path]);

  const handleBreadcrumb = (items: Array<BreadcrumbItemInterface>) =>
    items.map((item) => {
      const newItem = item;
      const path = removeCornerSlash(newItem.path.replace(/^\/library\//, ""));
      if (path === getTalkPath()) {
        newItem.description = t("talkFolderName");
      } else if (path === getPublicPath()) {
        newItem.description = t("publicFolderName");
      }

      return newItem;
    });

  const changeListType = useCallback(() => {
    dispatch(setCurrentAudioPlaying(""));
    setIconListType(defineIconListType(listType));
    setListType(listType === ListTypeEnum.LIST ? ListTypeEnum.GRID : ListTypeEnum.LIST);
  }, [dispatch, listType, setListType]);

  const filterItems = (filter: FilterEnum) => {
    setFilter(filter);
    handleCloseFilterDrawer();
  };

  const orderItems = (order: OrderEnum) => {
    setOrder(order);
    handleCloseFilterDrawer();
  };

  const handleCloseFilterDrawer = () => {
    setOpenFilterDrawer(false);
  };

  const handleOpenFilterDrawer = () => {
    setOpenFilterDrawer(true);
  };

  return (
    <>
      <HeaderBarWrapper>
        {pathExists && canChangeList && (
          <IconButton
            aria-label="change-list"
            color="primary"
            component="span"
            onClick={changeListType}
            disabled={isDisabled}
          >
            <SvgIcon icon={iconListType} htmlColor="#292929" fontSize="small" />
          </IconButton>
        )}
        {breadcrumb.length > 0 && (
          <BreadcrumbWrapper>
            <Breadcrumb
              breadcrumbs={breadcrumb}
              handleNavigate={handleNavigate}
              isDisabled={isDisabled}
            />
          </BreadcrumbWrapper>
        )}
        {pathExists && (
          <Options>
            {hasSearch && (
              <IconButton
                color="primary"
                component="span"
                onClick={() => router.push("/search")}
                aria-haspopup="true"
                aria-label="search"
                disabled={isDisabled}
              >
                <SvgIcon icon="search" htmlColor="#292929" fontSize="small" />
              </IconButton>
            )}

            {hasFilter && (
              <IconButton
                aria-label="filter"
                color="primary"
                component="span"
                onClick={handleOpenFilterDrawer}
                aria-haspopup="true"
                disabled={isDisabled}
              >
                <SvgIcon icon="settings_adjust" htmlColor="#292929" fontSize="small" />
              </IconButton>
            )}
          </Options>
        )}

        <TemporaryFiltersDrawer
          filterItems={filterItems}
          orderItems={orderItems}
          open={openFilterDrawer}
          handleClose={handleCloseFilterDrawer}
          filter={filter}
          order={order}
          isRootPath={isRootPath(currentPath)}
        />
      </HeaderBarWrapper>
    </>
  );
}

export default React.memo(HeaderBar);
