import FlexBox from "@/components/ui/FlexBox";
import { FilterEnum, JustifyContentEnum, ListTypeEnum, OrderEnum } from "@/enums/*";
import HeaderBar from "@/components/pages/library/HeaderBar";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";
import LibraryToolsMenu from "@/components/ui/ToolsMenu/Contexts/LibraryToolsMenu";
import LayoutApp from "@/components/statefull/LayoutApp";
import React, { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { BreadcrumbItemInterface, LibraryItemInterface } from "@/interfaces/index";
import { useRouter } from "next/router";

type Props = {
  path?: string | string[];
  currentPath: string;
  handleFilter: (filter: FilterEnum) => void;
  handleOrder: (filter: OrderEnum) => void;
  order: OrderEnum;
  filter: FilterEnum;
  items: LibraryItemInterface[];
  listType: ListTypeEnum;
  setListType: (listType: ListTypeEnum) => void;
  children: React.ReactNode;
};

const LibraryWrapper = ({
  path,
  currentPath,
  items,
  children,
  handleOrder,
  handleFilter,
  order,
  filter,
  listType,
  setListType,
}: Props) => {
  const router = useRouter();
  const { t: l } = useTranslation("library");

  const firstBreadcrumbMenu: BreadcrumbItemInterface = useMemo(
    () => ({
      icon: "library",
      description: l("title"),
      path: "/library",
    }),
    [l],
  );

  const handleBreadcrumbNavigate = useCallback(
    (dir: BreadcrumbItemInterface) => {
      router.push(dir.path);
    },
    [router],
  );

  return (
    <LayoutApp title={l("title")} drawer={!path} back={!!path} notifications={!path}>
      <FlexBox justifyContent={JustifyContentEnum.FLEXSTART} component="main">
        <HeaderBar
          key="library-modal"
          path={path}
          currentPath={currentPath}
          listType={listType}
          setListType={setListType}
          setOrder={handleOrder}
          setFilter={handleFilter}
          order={order}
          filter={filter}
          pathExists={Boolean(items)}
          handleNavigate={handleBreadcrumbNavigate}
          firstBreadcrumbItem={firstBreadcrumbMenu}
        />
        {children}
      </FlexBox>
      <OnlineOnly>
        <LibraryToolsMenu />
      </OnlineOnly>
    </LayoutApp>
  );
};

export default React.memo(LibraryWrapper);
