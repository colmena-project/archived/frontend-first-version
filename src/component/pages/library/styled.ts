import { styled } from "@material-ui/core/styles";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import List from "@material-ui/core/List";

//
// export const LibraryItemWrapper = styled(Box)(({ position }: LibraryItemWrapperInterface) => {
//   let styles = {
//     paddingTop: "4px",
//     paddingBottom: 0,
//     width: "100%",
//   };
//   if (position === "false") {
//     styles = Object.assign(styles, {
//       width: "50%",
//       display: "inline-flex",
//       padding: 4,
//     });
//   }
//
//   return styles;
// });

export const Avatar = styled(ListItemAvatar)({
  minHeight: 50,
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around",
});

export const LibraryItems = styled(List)({
  display: "flex",
  flexWrap: "wrap",
  textAlign: "left",
  alignItems: "stretch",
  width: "100%",
});
