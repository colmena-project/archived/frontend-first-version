import React, { useState, useEffect, useCallback } from "react";
import { InView } from "react-intersection-observer";
import { LibraryCardItemInterface, LibraryItemInterface } from "@/interfaces/index";
import CardItem from "@/components/pages/library/CardItem";
import { LibraryItems } from "@/components/pages/library/styled";
import { PlayerTypeEnum } from "@/enums/*";
import ListItemBase from "@material-ui/core/ListItem";

type Props = {
  data: LibraryItemInterface[];
  isVerticalList: boolean;
  handleItemClick?: (item: LibraryItemInterface) => void;
  isDisabled?: boolean;
  options?: (cardItem: LibraryCardItemInterface) => React.ReactNode;
  bottomOptions?: (
    cardItem: LibraryCardItemInterface,
    badgeStatusGrid?: React.ReactNode | undefined,
  ) => React.ReactNode;
  playerType?: PlayerTypeEnum;
};

export default function LibraryInView({
  data,
  isVerticalList,
  handleItemClick,
  isDisabled,
  options,
  bottomOptions,
  playerType,
}: Props) {
  const ITEMS_PER_PAGE = 10;
  const [items, setItems] = useState<LibraryItemInterface[]>([]);

  const orientation = isVerticalList ? "vertical" : "horizontal";

  const handleMore = (inView: boolean) => {
    if (inView) {
      const files = getData();
      const pos = items.length;
      setItems([...items, ...files.slice(pos, pos + ITEMS_PER_PAGE)]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  };

  const getData = useCallback(
    () => data.filter((item) => item && JSON.stringify(item) !== "{}"),
    [data],
  );

  useEffect(() => {
    const files = getData();
    const arr = files.length < ITEMS_PER_PAGE ? files : files.slice(0, ITEMS_PER_PAGE);
    setItems(arr);
  }, [data, getData]);

  type LibraryItemWrapperInterface = {
    isVerticalList?: boolean;
    children: React.ReactNode;
  };

  const ListItem = ({ isVerticalList, children }: LibraryItemWrapperInterface) => (
    <ListItemBase
      disableGutters
      style={
        !isVerticalList
          ? { width: "50%", display: "inline-flex", padding: 4 }
          : { paddingTop: "4px", paddingBottom: 0, width: "100%" }
      }
    >
      {children}
    </ListItemBase>
  );

  return (
    <LibraryItems>
      {items.map((item: LibraryItemInterface) => (
        <ListItem key={item.nextcloudId ?? item.basename} isVerticalList={isVerticalList}>
          <CardItem
            {...item}
            arrayBufferBlob={
              item?.arrayBufferBlob instanceof ArrayBuffer ? item.arrayBufferBlob : undefined
            }
            handleOpenCard={handleItemClick}
            isDisabled={isDisabled}
            orientation={orientation}
            options={options}
            bottomOptions={bottomOptions}
            playerType={playerType}
          />
        </ListItem>
      ))}
      <InView as="div" onChange={handleMore} />
    </LibraryItems>
  );
}

export const LibraryInViewMemoized = React.memo(LibraryInView);
