import React, { useMemo, useEffect } from "react";
import {
  LibraryInterface,
  LibraryItemInterface,
  TimeDescriptionInterface,
  UserInfoInterface,
} from "@/interfaces/index";
import { getTreeByPath } from "@/store/idb/models/files";
import { dateDescription, formatBytes, getExtensionFilename } from "@/utils/utils";
import {
  EnvironmentEnum,
  OrderEnum,
  FilterEnum,
  ListTypeEnum,
  PlayerTypeEnum,
} from "@/enums/index";
import {
  getPrivatePath,
  convertUsernameToPrivate,
  getFilename,
  getTalkPath,
  isPanal,
  convertPrivateToUsername,
  getPathName,
} from "@/utils/directory";
import DirectoryList from "@/components/ui/skeleton/DirectoryList";
import { setCurrentAudioPlaying } from "@/store/actions/library";
import { getFiles, getCurrentFile } from "@/services/webdav/files";
import { TFunction } from "next-i18next";
import { LibraryInViewMemoized } from "@/components/pages/library/LibraryInView";

export function getCurrentItem(): null | LibraryItemInterface {
  return getCurrentFile();
}

export async function getWebDavDirectories(
  userId: string,
  currentDirectory: string,
  timeDescription: TimeDescriptionInterface,
  libraryTranslation: TFunction,
) {
  try {
    const ncItems: false | LibraryItemInterface[] = await getFiles(
      userId,
      currentDirectory,
      timeDescription,
      libraryTranslation,
    );
    if (!ncItems) {
      return [];
    }

    return ncItems;
  } catch (e) {
    return false;
  }
}

export async function getLocalFiles(
  userId: string,
  currentDirectory: string,
  timeDescription: TimeDescriptionInterface,
) {
  const items: LibraryItemInterface[] = [];

  const localFiles = await getTreeByPath(userId, currentDirectory);
  if (localFiles.length > 0) {
    let dir = currentDirectory;
    if (dir) {
      dir += "/";
    }

    localFiles.forEach((file: any) => {
      if (!file.filename) {
        return;
      }

      const regex = new RegExp(`^${dir}.+?/`, "i");
      const isDirectory = regex.test(file.filename);
      let item: LibraryItemInterface;
      if (isDirectory) {
        const reg = new RegExp(`(${dir}.+?)/.+?$`, "gi");
        const directory = file;
        directory.filename = file.filename.replace(reg, "$1");
        directory.aliasFilename = convertPrivateToUsername(file.filename, userId);
        directory.basename = getPathName(directory.aliasFilename);
        item = applyLocalDirectoryInterface(file, timeDescription);
      } else {
        item = applyLocalItemInterface(file, timeDescription);
      }

      const exists = items.find((file) => file.filename === item.filename);
      if (!exists) {
        items.push(item);
      } else {
        let size = 0;
        size += exists.size ?? 0;
        size += item.size ?? 0;
        exists.size = size;
        exists.sizeFormatted = exists?.size ? formatBytes(exists.size) : undefined;
      }
    });
  }

  return items;
}

export function applyLocalItemInterface(file: any, timeDescription: TimeDescriptionInterface) {
  const item: LibraryItemInterface = {
    filename: file.filename,
    basename: getFilename(file.filename),
    aliasFilename: file.aliasFilename,
    id: file.id,
    type: "file",
    environment: EnvironmentEnum.LOCAL,
    createdAt: file.createdAt,
    createdAtDescription: dateDescription(file.createdAt, timeDescription),
    updatedAt: file.createdAt,
    updatedAtDescription: dateDescription(file.createdAt, timeDescription),
    size: file?.size,
    sizeFormatted: file?.size ? formatBytes(file.size) : undefined,
    mime: file?.type,
    extension: getExtensionFilename(file.filename),
    arrayBufferBlob: file.arrayBufferBlob,
    ownerId: file?.userId,
    ownerName: file?.userId,
    fileId: file?.nextcloudId,
    language: file?.language,
    tags: [],
  };

  if (file?.title) {
    item.title = decodeURI(file?.title);
  }

  if (file?.description) {
    item.description = decodeURI(file?.description);
  }

  return item;
}

export function applyLocalDirectoryInterface(
  directory: any,
  timeDescription: TimeDescriptionInterface,
) {
  const item: LibraryItemInterface = {
    id: directory.filename,
    filename: directory.filename,
    basename: directory.basename,
    aliasFilename: directory.aliasFilename,
    type: "directory",
    environment: EnvironmentEnum.LOCAL,
    createdAt: directory.createdAt,
    createdAtDescription: dateDescription(directory.createdAt, timeDescription),
    updatedAt: directory.createdAt,
    updatedAtDescription: dateDescription(directory.createdAt, timeDescription),
    size: directory?.size,
    sizeFormatted: directory?.size ? formatBytes(directory.size) : undefined,
    ownerId: directory?.userId,
    ownerName: directory?.userId,
    tags: [],
  };

  return item;
}

export function filterItems(filter: string, items: Array<LibraryItemInterface>) {
  if (items.length === 0 || filter === "") {
    return items;
  }

  const audioExtensions: Array<string> = ["mp3", "ogg", "wav", "opus", "weba"];
  const textExtensions: Array<string> = ["md", "txt", "pdf"];
  const imageExtensions: Array<string> = ["jpg", "png", "gif", "jpeg"];

  return items.filter((item: LibraryItemInterface) => {
    const extension = item.extension === undefined ? "" : item.extension.toLowerCase();

    switch (filter) {
      case FilterEnum.OFFLINE:
        return item.environment === EnvironmentEnum.LOCAL;
      case FilterEnum.SYNC:
        return item.environment === EnvironmentEnum.BOTH;
      case FilterEnum.AUDIO:
        return item.type === "audio" || audioExtensions.includes(extension);
      case FilterEnum.IMAGE:
        return imageExtensions.includes(extension);
      case FilterEnum.TEXT:
        return textExtensions.includes(extension);
      default:
        return true;
    }
  });
}

export function orderItems(
  order: OrderEnum | string,
  items: Array<LibraryItemInterface>,
  user: UserInfoInterface,
) {
  if (items.length === 0 || order === "") {
    return items;
  }

  items.sort((a, b) => {
    switch (order) {
      case OrderEnum.OLDEST_FIST:
        return a.updatedAt !== undefined && b.updatedAt !== undefined && a.updatedAt > b.updatedAt
          ? 1
          : -1;
      case OrderEnum.HIGHLIGHT:
      case OrderEnum.LATEST_FIRST:
        return a.updatedAt !== undefined && b.updatedAt !== undefined && a.updatedAt < b.updatedAt
          ? 1
          : -1;
      case OrderEnum.ASC_ALPHABETICAL:
        return a.basename !== undefined &&
          b.basename !== undefined &&
          a.basename.toLowerCase() > b.basename.toLowerCase()
          ? 1
          : -1;
      case OrderEnum.DESC_ALPHABETICAL:
        return a.basename !== undefined &&
          b.basename !== undefined &&
          a.basename.toLowerCase() < b.basename.toLowerCase()
          ? 1
          : -1;
      default:
        return 0;
    }
  });

  if (order === OrderEnum.HIGHLIGHT) {
    return orderHighlights(items, user);
  }

  return items;
}

function orderHighlights(items: Array<LibraryItemInterface>, user: UserInfoInterface) {
  const highlightItems: LibraryItemInterface[] = [];
  items.forEach((item, index) => {
    switch (item.filename) {
      case getPrivatePath():
        highlightItems[0] = item;
        break;
      case getTalkPath():
        highlightItems[1] = item;
        break;
      default:
        if (isPanal(item.filename)) {
          if (user.media && user.media.name === item.filename) {
            highlightItems[2] = item;

            return;
          }
        }

        highlightItems[index + 3] = item;
        break;
    }
  });

  return highlightItems;
}

export async function getItems(
  path: string,
  userId: string,
  timeDescription: TimeDescriptionInterface,
  libraryTranslation: TFunction,
) {
  const realPath = convertUsernameToPrivate(path, userId);
  const localItems = await getLocalFiles(userId, realPath, timeDescription);
  const remoteItems = await getWebDavDirectories(
    userId,
    realPath,
    timeDescription,
    libraryTranslation,
  );
  const ncItems: LibraryItemInterface[] = remoteItems === false ? [] : remoteItems;
  const items = [...localItems, ...ncItems];
  const deleteItems: Array<string> = [];

  let mountedItems = items.map((item: LibraryItemInterface) => {
    let updatedItem = item;
    if (item.environment === EnvironmentEnum.LOCAL) {
      const remoteItem = ncItems.find((remoteItem) => item.filename === remoteItem.filename);

      if (remoteItem) {
        const mergeItems = mergeEnvItems(item, remoteItem);
        if (mergeItems) {
          updatedItem = mergeItems;
          deleteItems.push(remoteItem.id);
        }
      }
    }

    return updatedItem;
  });

  if (deleteItems.length > 0) {
    mountedItems = mountedItems.filter(
      (item: LibraryItemInterface) =>
        !deleteItems.some(
          (itemId) => item.environment === EnvironmentEnum.REMOTE && item.id === itemId,
        ),
    );
  }

  let libraryPathExists = true;
  if (!remoteItems || (!remoteItems && localItems.length === 0)) {
    libraryPathExists = false;
  }

  return {
    items: mountedItems,
    libraryPathExists,
  };
}

export function mergeEnvItems(
  localItem: LibraryItemInterface | null,
  remoteItem: LibraryItemInterface | null,
) {
  if (localItem && remoteItem) {
    return {
      ...localItem,
      ...remoteItem,
      id: localItem.id,
      environment: EnvironmentEnum.BOTH,
    };
  }

  if (localItem) {
    return localItem;
  }

  if (remoteItem) {
    return remoteItem;
  }

  return false;
}

function Library({
  items = [],
  listType = ListTypeEnum.LIST,
  isLoading = false,
  isDisabled = false,
  options,
  bottomOptions,
  handleItemClick,
  itemsQuantitySkeleton = 4,
  playerType = PlayerTypeEnum.CIRCLE,
}: LibraryInterface) {
  useEffect(() => {
    setCurrentAudioPlaying("");
  }, []);

  const isVerticalList = useMemo(() => listType === ListTypeEnum.LIST, [listType]);

  return (
    <>
      <>
        {isLoading && <DirectoryList quantity={itemsQuantitySkeleton} />}
        {!isLoading && items?.length > 0 && (
          <LibraryInViewMemoized
            data={items}
            isVerticalList={isVerticalList}
            handleItemClick={handleItemClick}
            options={options}
            isDisabled={isDisabled}
            bottomOptions={bottomOptions}
            playerType={playerType}
          />
        )}
      </>
    </>
  );
}

export default React.memo(Library);
