import React from "react";
import { VerticalItemListInterface } from "@/interfaces/index";
import AudioItemPreview from "@/components/pages/library/AudioFile/AudioItemPreview";
import HorizontalCard from "../../ui/HorizontalCard";
import { Avatar } from "@/components/pages/library/styled";

const VerticalItemList = (cardItem: VerticalItemListInterface) => {
  const {
    avatar,
    primary,
    secondary,
    options,
    handleClick,
    filename,
    environment,
    size = 0,
    arrayBufferBlob,
    basename,
    audioState = "stop",
    handleAudioFinish,
  } = cardItem;
  let cardPrimary = primary;
  let cardSecondary = secondary;
  let cardAvatar = avatar;
  if (avatar) {
    cardAvatar = <Avatar>{avatar}</Avatar>;
  }

  if (audioState !== "stop") {
    cardPrimary = (
      <AudioItemPreview
        primary={primary}
        size={size}
        filename={filename}
        environment={environment}
        type="vertical"
        arrayBufferBlob={arrayBufferBlob}
        audioState={audioState}
        handleAudioFinish={handleAudioFinish}
      />
    );
    cardSecondary = "";
  }

  return (
    <HorizontalCard
      avatar={cardAvatar}
      primary={cardPrimary}
      secondary={cardSecondary}
      handleClick={audioState !== "stop" ? handleClick : undefined}
      data-testid={`library-item-${basename}`}
      options={options}
    />
  );
};

export default React.memo(VerticalItemList);
