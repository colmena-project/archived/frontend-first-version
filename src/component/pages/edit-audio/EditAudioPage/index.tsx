/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState } from "react";
import LayoutApp from "@/components/statefull/LayoutApp";
import { useTranslation } from "next-i18next";
import { WizardFeaturesEnum } from "@/enums/index";
import { useSelector, useDispatch } from "react-redux";
import { PropsUserSelector, PropsWizardSelector } from "@/types/index";
import { listFile } from "@/services/webdav/files";
import { toast } from "@/utils/notifications";
import { findByFilename, createFile } from "@/store/idb/models/files";
import { setFilenameRecorder, setPathRecorder, shortStringByNumber } from "@/utils/utils";
import { arrayBufferToBlob, createObjectURL } from "blob-util";
import { RotateYourDeviceMemoized } from "@/components/pages/edit-audio/RotateYourDevice";
import { PlaylistMemoized } from "@/components/ui/WaveformPlaylist/WaveformPlaylist";
import { useTheme } from "@material-ui/core/styles";
import Backdrop from "@/components/ui/Backdrop";
import EventEmitter from "events";
import HeaderControls from "@/components/pages/edit-audio/HeaderControls";
import { setCursorSelected } from "@/store/actions/audio-editor/index";
import { convertPrivateToUsername, removeExtension, splitFilename } from "@/utils/directory";
import WizardWrapper from "@/components/ui/Wizard/WizardWrapper";
import { useTour } from "@reactour/tour";
import { useVerifyScreenOrientation } from "@/hooks/useVerifyScreenOrientation";
import { FrameWaveformBox } from "@/components/pages/edit-audio/EditAudioPage/styled";
import Script from "next/script";

const stepsProps = {
  feature: WizardFeaturesEnum.EDIT_AUDIO,
};

const stepsHeaderPortraitControls = [
  {
    selector: `[data-tut="edit-audio-context-menu-portrait-controls"]`,
    content: (
      <WizardWrapper
        {...stepsProps}
        content="CONTEXT MENU First Text lore, A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo real. Wikipédia"
      />
    ),
  },
];

export const stepsHeaderLandscapeControls = [
  {
    selector: `[data-tut="edit-audio-context-menu-landscape-Controls"]`,
    content: (
      <WizardWrapper
        {...stepsProps}
        content="CONTEXT MENU 2 First Text lore, A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo real. Wikipédia"
      />
    ),
  },
];

type Props = {
  id: string;
};

function EditAudioPage({ id }: Props) {
  const { isPortraitView } = useVerifyScreenOrientation();
  const { wizard } = useSelector((state: { wizard: PropsWizardSelector }) => state.wizard);
  const { setSteps, setIsOpen } = useTour();
  const theme = useTheme();
  const dispatch = useDispatch();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;
  const [urlBlob, setUrlBlob] = useState<string | null>(null);
  const [ee] = useState(new EventEmitter());
  const [path, setPach] = useState("");
  const { t: c } = useTranslation("common");
  const { t } = useTranslation("editAudio");
  const [loading, setLoading] = useState(false);
  const [filename, setFilename] = useState<string>(atob(String(id)));
  const [dynamicWidth, setDynamicWidth] = useState(400);

  useEffect(() => {
    setSteps(isPortraitView ? stepsHeaderPortraitControls : stepsHeaderLandscapeControls);
  }, []);

  useEffect(() => {
    if (wizard.editAudio) {
      setIsOpen(true);
    }
  }, [wizard.editAudio, setIsOpen]);

  async function prepareAudioBlob(content: ArrayBuffer) {
    const blob = arrayBufferToBlob(content);
    const urlBlob = createObjectURL(blob);
    setUrlBlob(urlBlob);
  }

  const init = async () => {
    try {
      const filen = filename.split("/");

      setFilenameRecorder(removeExtension(filen[filen.length - 1]));
      const path = convertPrivateToUsername(filen.slice(0, filen.length - 1).join("/"), userId);

      setPach(path);
      setPathRecorder(path);

      const localFile = await findByFilename(userId, filename);

      if (!localFile) {
        setLoading(true);
        const result: any = await listFile(userId, filename);
        const blob = arrayBufferToBlob(result);
        await createFile({
          title: filen[filen.length - 1],
          basename: filen[filen.length - 1],
          filename,
          arrayBufferBlob: result,
          type: blob?.type,
          size: blob?.size,
          createdAt: new Date(),
          userId,
        });
        await prepareAudioBlob(result);
      } else {
        await prepareAudioBlob(localFile?.arrayBufferBlob);
      }
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "error");
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    init();
    dispatch(setCursorSelected(false));

    document.addEventListener(
      "update-filename-recorder",
      (e: CustomEvent<{ filename: string }>) => {
        if (!e.detail) return;
        setFilename(e.detail.filename);
      },
    );
  }, []);

  useEffect(() => {
    setDynamicWidth(window.innerWidth);
  }, [window.innerHeight, window.innerWidth]);

  return (
    <LayoutApp
      templateHeader="variation2"
      showFooter={false}
      title={t("title")}
      subtitle={shortStringByNumber(
        splitFilename(removeExtension(filename)),
        dynamicWidth > 700 ? 30 : 10,
      )}
      back
      drawer={false}
      notifications={false}
      rightExtraElement={
        <HeaderControls
          key="header-controls"
          ee={ee}
          audioState="STOPPED"
          context="editing"
          filename={filename}
          path={path}
        />
      }
      backgroundColor={theme.palette.primary.main}
    >
      <FrameWaveformBox>
        <RotateYourDeviceMemoized title={t("rotateYourDeviceTitle")} />
        {loading || !urlBlob ? (
          <Backdrop open={loading} />
        ) : (
          <PlaylistMemoized urlBlob={urlBlob} filename={filename} ee={ee} context="editing" />
        )}
      </FrameWaveformBox>
      <Script src="https://kit.fontawesome.com/ef69927139.js" />
    </LayoutApp>
  );
}

export default EditAudioPage;
