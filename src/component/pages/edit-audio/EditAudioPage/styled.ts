import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";

export const FrameWaveformBox = styled(Box)(({ theme }) => ({
  width: "100%",
  display: "flex",
  flex: 1,
  justifyContent: "flex-start",
  paddingBottom: 86,
  flexDirection: "column",
  "& .device-orientation-edit-audio": {
    width: "100%",
    backgroundColor: theme.palette.primary.main,
  },
  backgroundColor: theme.palette.primary.main,
}));

export const ExtraElementWrapper = styled(Box)({
  padding: 0,
  margin: 0,
  display: "flex",
  // flex: 1,
  flexDirection: "column",
  alignItems: "flex-start",
});
