import React, { useEffect, useState } from "react";
import { TextVariantEnum } from "@/enums/*";
import Image from "next/image";
import { Root, Bar, Title } from "@/components/pages/edit-audio/RotateYourDevice/styled";

type Props = {
  title: string;
};

export default function RotateYourDevice({ title }: Props) {
  const [isPortraitView, setIsPortraitView] = useState(true);

  const verifyScreenOrientation = () => {
    if (window.innerHeight > window.innerWidth) {
      setIsPortraitView(true);
    } else {
      setIsPortraitView(false);
    }
  };

  useEffect(() => {
    verifyScreenOrientation();
    window.addEventListener(
      "resize",
      () => {
        verifyScreenOrientation();
      },
      false,
    );
  }, []);

  if (!isPortraitView) return null;

  return (
    <Root>
      <Bar>
        <Title variant={TextVariantEnum.BODY2}>{title}</Title>
        <Image src="/images/rotate_device.gif" width={40} height={40} />
      </Bar>
    </Root>
  );
}

export const RotateYourDeviceMemoized = React.memo(RotateYourDevice);
