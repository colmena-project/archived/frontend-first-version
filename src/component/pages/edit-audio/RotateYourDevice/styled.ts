import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Text from "@/components/ui/Text";

export const Root = styled(Box)({
  top: 72,
  display: "flex",
  flexDirection: "column",
  width: "100%",
});

export const Bar = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.secondary.main,
  flex: 1,
  display: "flex",
  padding: 5,
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
}));

export const Title = styled(Text)({
  color: "#fff",
});
