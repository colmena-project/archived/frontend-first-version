/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/jsx-no-bind */
import React, { useState, useRef, ReactNode, useMemo } from "react";
import { useRouter } from "next/router";
import EventEmitter from "events";
import HeaderPortraitControls from "../HeaderPortraitControls";
import HeaderLandscapeControls from "../HeaderLandscapeControls";
import { useTranslation } from "next-i18next";

import {
  AudioStateProps,
  AudioStateWaveformProps,
  PropsAudioSave,
  PropsUserSelector,
  AudioExportTypesProps,
} from "@/types/*";
import Modal from "@/components/ui/Modal";
import LibraryModal from "@/components/ui/LibraryModal";
import { LibraryItemInterface } from "@/interfaces/index";
import {
  isAudioFile,
  removeSpecialCharacters,
  setFilenameRecorder,
  setPathRecorder,
  shortStringByNumber,
  awaiting,
  getAccessedPages,
} from "@/utils/utils";
import {
  convertPrivateToUsername,
  convertUsernameToPrivate,
  getFilename,
  isHoneycombUrl,
  isLibraryUrl,
  removeExtension,
  isPrivate,
  splitFilename,
} from "@/utils/directory";

import {
  ButtonSizeEnum,
  ButtonVariantEnum,
  AudioExportTypesEnum,
  TextVariantEnum,
} from "@/enums/index";
import { toast } from "@/utils/notifications";
import { useSelector, useDispatch } from "react-redux";
import { arrayBufferToBlob } from "blob-util";
import { useVerifyScreenOrientation } from "@/hooks/useVerifyScreenOrientation";
import ActionConfirm from "@/components/ui/ActionConfirm";
import { remove as removeLocalFile, findByFilename, createFile } from "@/store/idb/models/files";
import { listFile, existFile, deleteFile } from "@/services/webdav/files";
import { updateRecordingState, setRecordingCounter } from "@/store/actions/recordings/index";
import { saveFile } from "@/utils/recorder";
import { parseCookies } from "nookies";
import Backdrop from "@/components/ui/Backdrop";
import DialogExtraInfoAudio from "@/components/ui/WaveformPlaylist/DialogExtraInfoAudio";
import { isFileOwner } from "@/services/share/share";
import usePublishingFromAudioFile from "@/utils/usePublishingFromAudioFile";
import { ExtraElementWrapper, EditFileButton, BoxAvailableItemText } from "./styled";
import ButtonV2 from "@/components/ui/ButtonV2";

type Props = {
  ee: EventEmitter;
  audioState: AudioStateProps;
  context: "recorder" | "editing";
  filename?: string;
  path?: string;
  saved?: boolean;
};

declare global {
  interface Window {
    AudioEncoder: any;
  }
}

export default function HeaderControls({ ee, audioState, context, filename, path, saved }: Props) {
  const cookies = parseCookies();
  const language = cookies.NEXT_LOCALE || "en";
  const [showBackdrop, setShowBackdrop] = useState(false);
  const fileTitle = useMemo(() => getFilename(String(filename)), [filename]);
  const defaultAudioType = AudioExportTypesEnum.WAV;
  const router = useRouter();
  const dispatch = useDispatch();
  const { t } = useTranslation("editAudio");
  const { t: c } = useTranslation("common");
  const { t: r } = useTranslation("recording");
  const { t: l } = useTranslation("library");
  const { isPortraitView } = useVerifyScreenOrientation();
  const [cursorState, setCursorState] = useState<AudioStateWaveformProps>("cursor");
  const { initPublishing } = usePublishingFromAudioFile();

  const [handleContentModal, setHandleContentModal] = useState<ReactNode | null>(null);

  const [isOpen, setIsOpen] = useState(false);
  const handleClose = () => setIsOpen(false);
  const [rendeModal, setRendeModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [openSaveAudio, setOpenSaveAudio] = useState(false);

  const onCloseModal = () => {
    setIsLoading(false);
    setRendeModal(false);
  };

  const inputFileRef = useRef(null);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;

  const [libraryOpen, setLibraryOpen] = useState(false);
  const [loading, setLoading] = useState<string | undefined>();

  const isChromiumBrowserBased = typeof window.AudioEncoder !== "undefined";

  const openModal = () => {
    setLibraryOpen(true);
  };

  const filterItems = (items: LibraryItemInterface[]) =>
    items.filter((item) => item.type !== "file" || isAudioFile(item.mime));

  const handleImport = async (item: LibraryItemInterface) => {
    try {
      let blob: any = "";
      const filename = removeSpecialCharacters(item.filename);
      setLoading(filename);

      const filen = filename.split("/");
      const name = filen[filen.length - 1];
      const localFile = await findByFilename(userId, filename);

      if (!localFile) {
        const result: any = await listFile(userId, filename);
        blob = arrayBufferToBlob(result, "audio/*");
        blob.name = shortStringByNumber(splitFilename(filename), 10);

        ee.emit("newtrack", blob);

        await createFile({
          title: filen[filen.length - 1],
          basename: name,
          filename,
          arrayBufferBlob: result,
          type: blob?.type,
          size: blob?.size,
          createdAt: new Date(),
          userId,
        });
      } else {
        blob = arrayBufferToBlob(localFile.arrayBufferBlob, "audio/*");
        blob.name = shortStringByNumber(splitFilename(filename), 10);
        ee.emit("newtrack", blob);
      }
    } catch (e) {
      toast(c("genericErrorMessage"), "error");
    } finally {
      setLoading(undefined);
      handleClose();
      setLibraryOpen(false);
    }
  };

  const handlePublish = async () => {
    try {
      setShowBackdrop(true);
      await initPublishing(String(filename));
      router.push({
        pathname: "/publications/new",
      });
    } catch (e) {
      console.log(e);
    } finally {
      setShowBackdrop(false);
    }
  };

  const handleZoomIn = () => {
    ee.emit("zoomin");
  };

  const handleZoomOut = () => {
    ee.emit("zoomout");
  };

  const handleTrim = () => {
    ee.emit("trim");
  };

  const handleDownload = (type: AudioExportTypesProps) => {
    ee.emit("startaudiorendering", type);
  };

  const handleSave = () => {
    ee.emit("startaudiorendering", "buffer");
  };

  const handleCut = () => {
    ee.emit("cut");
  };

  const handleSplit = () => {
    ee.emit("split");
  };

  const handleStateChangeAudio = (type: AudioStateWaveformProps) => {
    setCursorState(type);
    ee.emit("statechange", type);
  };

  if (["RECORDING", "NONE"].includes(audioState)) return null;

  const onBtnClick = () => {
    if (!inputFileRef || !inputFileRef.current) return;
    const clk: { click: () => void } = inputFileRef?.current;
    clk.click();
  };

  const handleSaveLocally = (showNotification = true) => {
    ee.emit("startaudiorendering", "buffer");
    if (showNotification) toast(c("successMessageLocalAudioSaved"), "success");

    handleClose();
    setHandleContentModal(null);
  };

  const handleSaveFile = async (values: PropsAudioSave) => {
    try {
      const lastPages = await getAccessedPages();
      const { path } = values;
      const pathUsrToPvt = convertUsernameToPrivate(path, userId);
      const currentFilename = `${pathUsrToPvt}/${removeSpecialCharacters(
        fileTitle,
      )}.${defaultAudioType}`;
      const audioLocalFile = await findByFilename(userId, currentFilename);
      const messages = {
        genericError: c("genericErrorMessage"),
        audioSavedSuccessfully: r("audioSavedSuccessfully"),
        storageError: r("errorStorageMessage"),
      };

      handleClose();
      handleCloseModalSaveAudio();
      setShowBackdrop(true);
      if (audioLocalFile) {
        await saveFile(values, userId, removeExtension(fileTitle), language, messages);

        if (isHoneycombUrl(lastPages[1]) || isLibraryUrl(lastPages[1])) {
          router.push(lastPages[1]);
        }
      } else {
        handleSaveLocally(false);
        await awaiting(1000);
        await saveFile(values, userId, removeExtension(fileTitle), language, messages);
      }
    } catch (e) {
      console.log(e);
    } finally {
      setShowBackdrop(false);
    }
  };

  const onSelectFile = (e: any) => {
    if (e.target.files && e.target.files.length > 0) {
      let blob: any = "";
      const file = e.target.files[0];
      const { size } = file;

      if (size > 20971520) {
        //   toast(c("uploadRuleFileSize"), "warning");
        //   return;
      }

      blob = arrayBufferToBlob(file, "audio/*");
      blob.name = shortStringByNumber(splitFilename(file.name), 10);
      ee.emit("newtrack", blob);
      handleClose();
    }
  };

  const actions = (item: LibraryItemInterface) => {
    if (item.type === "file" && isAudioFile(item.mime)) {
      return (
        <EditFileButton
          handleClick={() => handleImport(item)}
          title={t("import")}
          size={ButtonSizeEnum.SMALL}
          isLoading={loading === item.filename}
          disabled={loading !== undefined}
        />
      );
    }

    return null;
  };

  const handleConfirmDelete = async () => {
    try {
      if (filename) {
        setIsLoading(true);

        if (!isPrivate(filename)) {
          const canDelete = await isFileOwner(filename);
          if (!canDelete) {
            toast(l("messages.noRemovePermission"), "error");
            return;
          }
        }

        const localFile = await findByFilename(userId, filename);
        if (localFile?.id) await removeLocalFile(localFile.id, userId);
        const exists = await existFile(userId, filename);
        if (exists) deleteFile(userId, filename);
        toast(r("audioRemovedSuccessfully"), "success");
        ee.emit("clear");
        dispatch(updateRecordingState("NONE"));
        dispatch(setRecordingCounter(0));

        router.push("/home");
      }
    } catch (e) {
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };

  const handleModalDeleteConfirm = () => {
    setRendeModal(true);
  };

  const handleDownloadOptions = () => {
    setIsOpen(true);

    setHandleContentModal(
      <ExtraElementWrapper>
        <ButtonV2
          title={AudioExportTypesEnum.WAV}
          handleClick={() => handleDownload(AudioExportTypesEnum.WAV)}
          startIcon="wav_icon"
          variant={ButtonVariantEnum.TEXT}
        />
        {/* <ButtonV2
          title="aac"
          handleClick={() => handleDownload("aac")}
          startIcon="webm_icon"
          variant={ButtonVariantEnum.TEXT}
        /> */}
        <ButtonV2
          title={`${AudioExportTypesEnum.OPUS} ${!isChromiumBrowserBased ? "*" : ""}`}
          handleClick={() => handleDownload(AudioExportTypesEnum.OPUS)}
          startIcon="webm_icon"
          variant={ButtonVariantEnum.TEXT}
          disabled={!isChromiumBrowserBased}
        />
        {!isChromiumBrowserBased && (
          <BoxAvailableItemText variant={TextVariantEnum.CAPTION} fontSize={10}>
            {`* ${c("availableOnlyInChromeMessage")}`}
          </BoxAvailableItemText>
        )}
      </ExtraElementWrapper>,
    );
  };

  const handleImportAudio = () => {
    setIsOpen(true);

    setHandleContentModal(
      <ExtraElementWrapper>
        <ButtonV2
          title={t("contextOptions.uploadFromDevice")}
          handleClick={onBtnClick}
          startIcon="upload"
          variant={ButtonVariantEnum.TEXT}
        />
        <ButtonV2
          title={t("contextOptions.searchInLibrary")}
          handleClick={openModal}
          startIcon="search_in_library"
          variant={ButtonVariantEnum.TEXT}
        />
      </ExtraElementWrapper>,
    );
  };

  const handleCloseModalSaveAudio = () => {
    setOpenSaveAudio(false);
  };

  const handleSaveOption = () => {
    setIsOpen(true);

    setHandleContentModal(
      <ExtraElementWrapper>
        <ButtonV2
          title={t("contextOptions.saveLocally")}
          handleClick={() => handleSaveLocally()}
          startIcon="save"
          variant={ButtonVariantEnum.TEXT}
        />
        <ButtonV2
          title={t("contextOptions.saveInCloudTitle")}
          handleClick={() => setOpenSaveAudio(true)}
          startIcon="upload"
          variant={ButtonVariantEnum.TEXT}
        />
      </ExtraElementWrapper>,
    );
  };

  const renderControls = () => {
    if (isPortraitView) {
      return (
        <HeaderPortraitControls
          handlePublish={handlePublish}
          handleImportAudio={handleImportAudio}
          handleZoomIn={handleZoomIn}
          handleZoomOut={handleZoomOut}
          handleTrim={handleTrim}
          handleCut={handleCut}
          handleSplit={handleSplit}
          handleSelectAudioRegion={handleStateChangeAudio}
          handleDownload={handleDownloadOptions}
          handleSave={handleSave}
          handleModalDeleteConfirm={handleModalDeleteConfirm}
          handleSaveOption={() => setOpenSaveAudio(true)}
          context={context}
          saved={saved}
        />
      );
    }

    return (
      <HeaderLandscapeControls
        handlePublish={handlePublish}
        handleImportAudio={handleImportAudio}
        handleTrim={handleTrim}
        handleCut={handleCut}
        handleSplit={handleSplit}
        handleSelectAudioRegion={handleStateChangeAudio}
        handleDownload={handleDownloadOptions}
        cursorState={cursorState}
        handleModalDeleteConfirm={handleModalDeleteConfirm}
        handleSaveOption={() => setOpenSaveAudio(true)}
        context={context}
        saved={saved}
      />
    );
  };

  return (
    <>
      <Backdrop open={showBackdrop} />
      <input
        type="file"
        ref={inputFileRef}
        onChange={onSelectFile}
        style={{ display: "none" }}
        accept="audio/*"
      />
      {rendeModal ? (
        <ActionConfirm
          data-testid="delete-item-confirm"
          onOk={handleConfirmDelete}
          onClose={onCloseModal}
          isLoading={isLoading}
          title={r("deleteAudio")}
          message={r("deleteAudioDetailed")}
        />
      ) : null}
      {renderControls()}
      <Modal
        dialogTitleStyle={{ margin: "0", padding: "0" }}
        handleClose={handleClose}
        open={isOpen}
        fullWidth={false}
      >
        {handleContentModal}
      </Modal>
      <LibraryModal
        title={t("selectAudioFile")}
        handleClose={() => setLibraryOpen(false)}
        open={libraryOpen}
        onlyDirectories={false}
        options={actions}
        filterItems={filterItems}
      />
      {openSaveAudio && (
        <DialogExtraInfoAudio
          open={openSaveAudio}
          handleClose={handleCloseModalSaveAudio}
          handleSubmit={handleSaveFile}
          pathLocationSave={String(path)}
          handleBack={() => setOpenSaveAudio(false)}
          tempFileName={removeExtension(fileTitle)}
        />
      )}
    </>
  );
}

export const HeaderControlsMemoized = React.memo(HeaderControls);
