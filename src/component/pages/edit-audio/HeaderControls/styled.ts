import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Button from "@/components/ui/Button";
import Text from "@/components/ui/Text";

export const ExtraElementWrapper = styled(Box)({
  padding: 0,
  margin: 0,
  display: "flex",
  flex: 1,
  flexDirection: "column",
  alignItems: "flex-start",
});

export const EditFileButton = styled(Button)({
  marginLeft: 10,
});

export const BoxAvailableItem = styled(Box)({
  display: "flex",
  margin: 0,
  padding: 0,
  flexDirection: "column",
  alignItems: "flex-start",
});

export const BoxAvailableItemText = styled(Text)(({ theme }) => ({
  color: theme.palette.gray.dark,
  fontSize: 12,
  margin: 0,
  padding: 0,
  marginTop: 10,
}));
