/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/jsx-no-bind */
import React, { useState } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Box from "@material-ui/core/Box";
import IconButton from "@/components/ui/IconButton";
import { useTranslation } from "react-i18next";
import { v4 as uuid } from "uuid";
import ContextMenuItem from "@/components/ui/ContextMenuItem";
import theme from "@/styles/theme";
import { AudioStateWaveformProps, PropsRecordingSelector } from "@/types/*";
import Divider from "@material-ui/core/Divider";
import { useSelector } from "react-redux";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type PositionProps = {
  mouseX: null | number;
  mouseY: null | number;
};

type Props = {
  handleImportAudio: () => void;
  handleZoomIn: () => void;
  handleZoomOut: () => void;
  handleTrim: () => void;
  handleSplit: () => void;
  handleCut: () => void;
  handleSelectAudioRegion: (type: string) => void;
  handleDownload: () => void;
  handleSave: () => void;
  context: "recorder" | "editing";
  handleModalDeleteConfirm: () => void;
  handleSaveOption: () => void;
  handlePublish: () => void;
  saved?: boolean;
};

const HeaderPortraitControls = ({
  handleImportAudio,
  handleZoomIn,
  handleZoomOut,
  handleSelectAudioRegion,
  handleTrim,
  handleSplit,
  handleCut,
  handleDownload,
  context,
  handleModalDeleteConfirm,
  handleSaveOption,
  handlePublish,
  saved = true,
}: Props) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const connectionStatus = useConnectionStatus();
  const { t } = useTranslation("editAudio");
  const [position, setPosition] = useState<PositionProps>({
    mouseX: null,
    mouseY: null,
  });

  const recordingRdx = useSelector(
    (state: { recording: PropsRecordingSelector }) => state.recording,
  );

  const audioState = recordingRdx.activeRecordingState || "NONE";

  const handleOpenContextMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
    setPosition({
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4,
    });
  };

  const handleCloseContextMenu = () => {
    setAnchorEl(null);
  };

  const handleStateChangeAudio = (state: AudioStateWaveformProps) => {
    handleSelectAudioRegion(state);
    handleCloseContextMenu();
  };

  const handleSaveIntern = () => {
    handleSaveOption();
    handleCloseContextMenu();
  };

  const handleDownloadIntern = () => {
    handleDownload();
    handleCloseContextMenu();
  };

  const handlePublishIntern = () => {
    handleCloseContextMenu();
    handlePublish();
  };

  const handleTrimIntern = () => {
    handleTrim();
    handleCloseContextMenu();
  };

  const handleSplitIntern = () => {
    handleSplit();
    handleCloseContextMenu();
  };

  const startModalImportAudio = () => {
    handleImportAudio();
    handleCloseContextMenu();
  };

  const onConfirmDelete = () => {
    handleModalDeleteConfirm();
    handleCloseContextMenu();
  };

  const fontSizeIcon = { fontSize: 15 };

  return (
    <Box display="flex" justifyContent="flex-end">
      <IconButton
        key={uuid()}
        data-tut="edit-audio-context-menu-portrait-controls"
        data-testid="context-menu-portrait-controls-btn"
        icon="audio_settings"
        style={{ padding: 0, margin: 0, minWidth: 30 }}
        iconColor="#fff"
        fontSizeIcon="small"
        handleClick={handleOpenContextMenu}
      />
      <Menu
        key={uuid()}
        data-testid="context-menu-portrait-controls-menu"
        id="context-menu-portrait-controls-btn"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        anchorReference="anchorPosition"
        anchorPosition={
          position.mouseY !== null && position.mouseX !== null
            ? { top: position.mouseY, left: position.mouseX }
            : undefined
        }
        onClose={handleCloseContextMenu}
      >
        <MenuItem
          key="import-audio"
          data-testid="import-audio"
          onClick={startModalImportAudio}
          disabled={audioState === "RECORDING" || !connectionStatus}
        >
          <ContextMenuItem
            icon="import_audio"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.audioImport")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <Divider />
        <MenuItem
          key="cut-audio-button"
          data-testid="cut-audio-button"
          onClick={handleCut}
          disabled={audioState === "RECORDING"}
        >
          <ContextMenuItem
            icon="cut"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.cutAudioRegionTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="trim-audio-button"
          data-testid="trim-audio-button"
          onClick={handleTrimIntern}
        >
          <ContextMenuItem
            icon="trim"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.trimAudioRegionTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="split-audio-button"
          data-testid="split-audio-button"
          onClick={handleSplitIntern}
        >
          <ContextMenuItem
            icon="split"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.splitAudioRegionTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <Divider />
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="cursor-audio-button"
          data-testid="cursor-audio-button"
          onClick={() => handleStateChangeAudio("cursor")}
        >
          <ContextMenuItem
            icon="headphone"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.cursorAudioRegionTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="select-audio-button"
          data-testid="select-audio-button"
          onClick={() => handleStateChangeAudio("select")}
        >
          <ContextMenuItem
            icon="cursor_select"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.selectAudioRegionTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="shift-audio-button"
          data-testid="shift-audio-button"
          onClick={() => handleStateChangeAudio("shift")}
        >
          <ContextMenuItem
            icon="audio_shift"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.shiftAudioTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="fade-in-audio-button"
          data-testid="fade-in-audio-button"
          onClick={() => handleStateChangeAudio("fadein")}
        >
          <ContextMenuItem
            icon="fadeIn"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.fadeInTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="fade-out-audio-button"
          data-testid="fade-out-audio-button"
          onClick={() => handleStateChangeAudio("fadeout")}
        >
          <ContextMenuItem
            icon="fadeOut"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.fadeOutTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <Divider />
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="zoom-in-button"
          data-testid="zoom-in-button"
          onClick={handleZoomIn}
        >
          <ContextMenuItem
            icon="zoomin"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.zoomInTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="zoom-out-button"
          data-testid="zoom-out-button"
          onClick={handleZoomOut}
        >
          <ContextMenuItem
            icon="zoomout"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.zoomOutTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>

        {context === "editing" && (
          <>
            <Divider />
            <MenuItem
              disabled={audioState === "RECORDING"}
              key="trash-audio-button"
              data-testid="trash-audio-button"
              onClick={onConfirmDelete}
            >
              <ContextMenuItem
                icon="trash"
                iconColor={theme.palette.variation6.main}
                title={t("contextOptions.trashAudioTitle")}
                style={fontSizeIcon}
              />
            </MenuItem>
          </>
        )}

        {context === "editing" && (
          <MenuItem
            disabled={audioState === "RECORDING"}
            key="save-audio-button"
            data-testid="save-audio-button"
            onClick={handleSaveIntern}
          >
            <ContextMenuItem
              icon="save"
              iconColor={theme.palette.variation6.main}
              title={t("contextOptions.saveTitle")}
              style={fontSizeIcon}
            />
          </MenuItem>
        )}
        <MenuItem
          disabled={audioState === "RECORDING"}
          key="download-audio-button"
          data-testid="download-audio-button"
          onClick={handleDownloadIntern}
        >
          <ContextMenuItem
            icon="download"
            iconColor={theme.palette.variation6.main}
            title={t("contextOptions.downloadTitle")}
            style={fontSizeIcon}
          />
        </MenuItem>

        {saved && (
          <MenuItem
            disabled={audioState === "RECORDING" || !connectionStatus}
            key="publish-btn"
            data-testid="publish-btn"
            onClick={handlePublishIntern}
          >
            <ContextMenuItem
              icon="use_in_publication"
              iconColor={theme.palette.variation6.main}
              title={t("contextOptions.publishTitle")}
              style={fontSizeIcon}
            />
          </MenuItem>
        )}
      </Menu>
    </Box>
  );
};

export default HeaderPortraitControls;
