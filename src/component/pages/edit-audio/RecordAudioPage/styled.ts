import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Text from "@/components/ui/Text";
import Button from "@/components/ui/Button";
import Clickable from "@/components/ui/Clickable";

export const FrameWaveform = styled(Box)(({ theme }) => ({
  width: "100%",
  "& .device-orientation-edit-audio": {
    width: "100%",
    backgroundColor: theme.palette.primary.main,
  },
}));

export const FlexboxWrapper = styled(Box)(({ theme }) => ({
  display: "flex",
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  padding: "0 !important",
  margin: "0 !important",
  backgroundColor: theme.palette.primary.main,
}));

export const Description = styled(Text)(({ theme }) => ({
  color: theme.palette.primary.light,
  marginBottom: theme.spacing(2),
}));

export const MyClickable = styled(Clickable)(({ theme }) => ({
  border: "2px dashed",
  borderColor: theme.palette.primary.dark,
  display: "block",
  padding: theme.spacing(2),
  margin: "0 20px",
}));

export const MyButton = styled(Button)({
  marginLeft: 10,
});
