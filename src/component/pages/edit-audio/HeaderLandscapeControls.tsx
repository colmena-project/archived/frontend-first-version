/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import { makeStyles, Theme } from "@material-ui/core/styles";
import theme from "@/styles/theme";
import IconButton from "@material-ui/core/IconButton";
import SvgIcon from "@/components/ui/SvgIcon";
import { AudioStateWaveformProps, PropsRecordingSelector } from "@/types/index";
import Divider from "@material-ui/core/Divider";
import { useSelector } from "react-redux";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

const useStyles = makeStyles((theme: Theme) => ({
  extraElementWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    "& > *": {
      margin: theme.spacing(0.8),
    },
    "& button:disabled": {
      opacity: "0.4",
    },
  },
  dividerColor: {
    backgroundColor: theme.palette.variation7.light,
  },
  actionsButton: {
    padding: 0,
    "& svg": {
      width: 22,
      padding: 0,
    },
  },
}));

type Props = {
  handleImportAudio: (event: React.MouseEvent<HTMLButtonElement>) => void;
  handleTrim: () => void;
  handleCut: () => void;
  handleSplit: () => void;
  handleSelectAudioRegion: (type: string) => void;
  handleDownload: () => void;
  context: "recorder" | "editing";
  cursorState: AudioStateWaveformProps;
  handleModalDeleteConfirm: () => void;
  handleSaveOption: () => void;
  handlePublish: () => void;
  saved?: boolean;
};

export default function HeaderLandscapeControls({
  handleImportAudio,
  handleTrim,
  handleCut,
  handleSplit,
  handleSelectAudioRegion,
  handleDownload,
  handlePublish,
  cursorState,
  handleSaveOption,
  context,
  handleModalDeleteConfirm,
  saved = true,
}: Props) {
  const classes = useStyles();
  const [menuSelected, setMenuSelected] = useState(cursorState);
  const connectionStatus = useConnectionStatus();

  const recordingRdx = useSelector(
    (state: { recording: PropsRecordingSelector }) => state.recording,
  );

  const audioState = recordingRdx.activeRecordingState || "NONE";

  const handleStateChangeAudio = (state: AudioStateWaveformProps) => {
    setMenuSelected(menuSelected !== state ? state : "cursor");
    handleSelectAudioRegion(menuSelected !== state ? state : "cursor");
  };

  const handleDownloadIntern = () => {
    handleDownload();
  };

  return (
    <Box
      className={classes.extraElementWrapper}
      data-tut="edit-audio-context-menu-landscape-Controls"
    >
      <IconButton
        disabled={audioState === "RECORDING" || !connectionStatus}
        onClick={handleImportAudio}
        className={classes.actionsButton}
        data-testid="import-audio"
      >
        <SvgIcon icon="import_audio" htmlColor={theme.palette.primary.contrastText} />
      </IconButton>
      <Divider orientation="vertical" light flexItem className={classes.dividerColor} />
      <IconButton
        disabled={audioState === "RECORDING"}
        onClick={handleCut}
        data-testid="cut-btn"
        className={classes.actionsButton}
      >
        <SvgIcon icon="cut" htmlColor={theme.palette.primary.contrastText} />
      </IconButton>
      <IconButton
        disabled={audioState === "RECORDING"}
        onClick={handleTrim}
        data-testid="trim-btn"
        className={classes.actionsButton}
      >
        <SvgIcon icon="trim" htmlColor={theme.palette.primary.contrastText} />
      </IconButton>

      <IconButton
        disabled={audioState === "RECORDING"}
        onClick={handleSplit}
        data-testid="split-btn"
        className={classes.actionsButton}
      >
        <SvgIcon icon="split" htmlColor={theme.palette.primary.contrastText} />
      </IconButton>

      <Divider orientation="vertical" light flexItem className={classes.dividerColor} />
      <IconButton
        disabled={audioState === "RECORDING"}
        onClick={() => handleStateChangeAudio("cursor")}
        className={classes.actionsButton}
        data-testid="cursor-btn"
      >
        <SvgIcon
          icon="headphone"
          htmlColor={
            menuSelected === "cursor"
              ? theme.palette.secondary.main
              : theme.palette.primary.contrastText
          }
        />
      </IconButton>
      <IconButton
        disabled={audioState === "RECORDING"}
        onClick={() => handleStateChangeAudio("select")}
        className={classes.actionsButton}
        data-testid="select-btn"
      >
        <SvgIcon
          icon="cursor_select"
          htmlColor={
            menuSelected === "select"
              ? theme.palette.secondary.main
              : theme.palette.primary.contrastText
          }
        />
      </IconButton>
      <IconButton
        disabled={audioState === "RECORDING"}
        data-testid="shift-btn"
        onClick={() => handleStateChangeAudio("shift")}
        className={classes.actionsButton}
      >
        <SvgIcon
          icon="audio_shift"
          htmlColor={
            menuSelected === "shift"
              ? theme.palette.secondary.main
              : theme.palette.primary.contrastText
          }
        />
      </IconButton>
      <IconButton
        disabled={audioState === "RECORDING"}
        onClick={() => handleStateChangeAudio("fadein")}
        className={classes.actionsButton}
        data-testid="fadein-btn"
      >
        <SvgIcon
          icon="fadeIn"
          htmlColor={
            menuSelected === "fadein"
              ? theme.palette.secondary.main
              : theme.palette.primary.contrastText
          }
        />
      </IconButton>
      <IconButton
        disabled={audioState === "RECORDING"}
        data-testid="fadeout-btn"
        onClick={() => handleStateChangeAudio("fadeout")}
        className={classes.actionsButton}
      >
        <SvgIcon
          icon="fadeOut"
          htmlColor={
            menuSelected === "fadeout"
              ? theme.palette.secondary.main
              : theme.palette.primary.contrastText
          }
        />
      </IconButton>

      {context === "editing" && (
        <>
          <Divider orientation="vertical" light flexItem className={classes.dividerColor} />
          <IconButton
            disabled={audioState === "RECORDING"}
            onClick={handleModalDeleteConfirm}
            data-testid="remove-btn"
            className={classes.actionsButton}
          >
            <SvgIcon icon="trash" htmlColor={theme.palette.primary.contrastText} />
          </IconButton>
        </>
      )}

      {context === "editing" && (
        <IconButton
          disabled={audioState === "RECORDING"}
          onClick={handleSaveOption}
          data-testid="save-btn"
          className={classes.actionsButton}
        >
          <SvgIcon icon="save" htmlColor={theme.palette.primary.contrastText} />
        </IconButton>
      )}
      <Divider orientation="vertical" light flexItem className={classes.dividerColor} />
      <IconButton
        disabled={audioState === "RECORDING"}
        onClick={handleDownloadIntern}
        data-testid="download-btn"
        className={classes.actionsButton}
      >
        <SvgIcon icon="download" htmlColor={theme.palette.primary.contrastText} />
      </IconButton>

      {saved && (
        <IconButton
          disabled={audioState === "RECORDING" || !connectionStatus}
          onClick={handlePublish}
          data-testid="publish-btn"
          className={classes.actionsButton}
        >
          <SvgIcon
            icon="use_in_publication"
            htmlColor={theme.palette.primary.contrastText}
            fontSize="small"
          />
        </IconButton>
      )}
    </Box>
  );
}
