import { styled, Box } from "@material-ui/core";

export const EditorWrapper = styled(Box)(({ theme }) => ({
  "& .toolbarCustomClass": {
    border: "none",
    borderBottom: "1px solid rgba(0, 0, 0, 0.23)",
    display: "flex",
    flexWrap: "nowrap",

    "& .rdw-colorpicker-modal": {
      left: "auto",
      right: "5px",
    },

    "& .rdw-link-modal": {
      left: "auto",
      right: "5px",
    },
  },
  border: "1px solid rgba(0, 0, 0, 0.23)",
  borderRadius: 4,
  minHeight: "25vh",
  position: "relative",

  "& .wrapper": {
    minHeight: "25vh",
  },

  "& .editorCustomClass": {
    padding: "0 14px",
    "& h1": {
      ...theme.typography.h1,
    },
    "& h2": {
      ...theme.typography.h2,
    },
    "& h3": {
      ...theme.typography.h3,
    },
    "& h4": {
      ...theme.typography.h4,
    },
    "& h5": {
      ...theme.typography.h5,
    },
    "& h6": {
      ...theme.typography.h6,
    },
    "& a": {
      ...theme.typography.body1,
      color: theme.palette.text.primary,
      textDecoration: "underline",
    },
  },
}));
