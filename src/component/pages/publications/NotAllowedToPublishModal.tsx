import React from "react";

import { useTranslation } from "next-i18next";
import Modal from "@/components/ui/Modal";
import Text from "@/components/ui/Text";
import { TextAlignEnum, TextVariantEnum, ButtonColorEnum, ButtonVariantEnum } from "@/enums/index";
import Button from "@/components/ui/Button";
import Box from "@material-ui/core/Box";
import SvgIconAux from "@/components/ui/SvgIcon";
import { useTheme } from "@material-ui/styles";
import { makeStyles, Theme } from "@material-ui/core";

type NotAllowedToPublishModalProps = {
  open: boolean;
  onCancel: () => void;
  onSaveAsDraft: () => void;
};

const NotAllowedToPublishModal: React.FC<NotAllowedToPublishModalProps> = ({
  open,
  onCancel,
  onSaveAsDraft,
}) => {
  const { t } = useTranslation("publications");
  const { t: c } = useTranslation("common");

  const theme: Theme = useTheme();
  const classes = useClasses();

  return (
    <Modal
      open={open}
      handleClose={onCancel}
      actions={
        <Box
          display="flex"
          flex="1"
          flexDirection="row"
          justifyContent="space-between"
          color={theme.palette.danger.main}
        >
          <Button
            handleClick={onCancel}
            style={{ margin: 8 }}
            variant={ButtonVariantEnum.TEXT}
            color={ButtonColorEnum.INHERIT}
            title={c("form.cancelButton")}
          />
          <Button
            handleClick={onSaveAsDraft}
            color={ButtonColorEnum.PRIMARY}
            style={{ margin: 8 }}
            variant={ButtonVariantEnum.OUTLINED}
            title={t("preview.draft")}
          />
        </Box>
      }
      className={classes.backdrop}
    >
      <Box textAlign="center" mb={2}>
        <SvgIconAux icon="warning_outline" htmlColor={theme.palette.danger.main} fontSize={60} />
      </Box>

      <Text variant={TextVariantEnum.BODY1} align={TextAlignEnum.LEFT} paragraph>
        {t("preview.notAllowedToPublishModal.notPermitedMessage")}
      </Text>

      <Text variant={TextVariantEnum.BODY1} align={TextAlignEnum.LEFT} paragraph>
        {t("preview.notAllowedToPublishModal.notification")}
      </Text>
    </Modal>
  );
};

const useClasses = makeStyles(() => ({
  backdrop: {
    zIndex: "999999 !important" as unknown as number,

    "& .MuiPaper-rounded": {
      borderRadius: 9,
    },
  },
}));

export default NotAllowedToPublishModal;
