import { Box, makeStyles } from "@material-ui/core";
import { Editor } from "@tiptap/react";
import { useState } from "react";

import FormatBold from "@material-ui/icons/FormatBold";
import RefreshIcon from "@material-ui/icons/Refresh";
import Replay from "@material-ui/icons/Replay";
import FormatItalic from "@material-ui/icons/FormatItalic";
import StrikethroughS from "@material-ui/icons/StrikethroughS";
import FormatListNumbered from "@material-ui/icons/FormatListNumbered";
import FormatListBulleted from "@material-ui/icons/FormatListBulleted";
import HorizontalRules from "@material-ui/icons/Remove";
import FormatQuote from "@material-ui/icons/FormatQuote";
import Code from "@material-ui/icons/Code";

import ToolbarTextEditorItem from "@/components/ui/ToolbarTextEditor/components/ToolbarTextEditorItem";
import OptionsFormatAlignText from "@/components/ui/ToolbarTextEditor/components/OptionsFormatAlignText";
import OptionsFormatSize from "@/components/ui/ToolbarTextEditor/components/OptionsFormatSize";
import OptionToogleLink from "@/components/ui/ToolbarTextEditor/components/OptionToogleLink";
import OptionsHighlight from "@/components/ui/ToolbarTextEditor/components/OptionsHighlight";

type PublicationEditorToolbarProps = {
  editor: Editor;
};

const useStyles = makeStyles(() => ({
  richTextOptionsWrapper: {
    display: "flex",
    flexWrap: "nowrap",
    overflowX: "scroll",
    bottom: "0",
    zIndex: 1,
    background: "#FFF",
    left: 0,
    width: "100%",

    border: `2px solid #E5E5E5`,
    borderBottom: "none",

    "&::-webkit-scrollbar": {
      width: "0px",
      height: "0px",
    },

    "& button": {
      width: "30px",
      height: "35px",
      padding: "5px",
      margin: "1px",
      background: "#f7f7f7",
      marginLeft: 2,
    },

    "& .MuiButton-root": {
      minWidth: "10px !important",
    },
  },
  wrapper: {
    display: "flex",
    width: "100%",
    minWidth: "500px",
    justifyContent: "center",
    background: "#F7f7f7",
  },
}));

export default function PublicationEditorToolbar({ editor }: PublicationEditorToolbarProps) {
  const [openOptionToogleLink, setOpenOptionToogleLink] = useState(false);

  const classes = useStyles();
  return (
    <Box className={classes.richTextOptionsWrapper}>
      <Box className={classes.wrapper}>
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().undo().run()}
          label={<Replay />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().redo().run()}
          label={<RefreshIcon />}
        />

        <ToolbarTextEditorItem label={<OptionsFormatSize editor={editor} />} />
        <ToolbarTextEditorItem label={<OptionsHighlight editor={editor} />} />

        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleBold().run()}
          label={<FormatBold />}
        />

        <ToolbarTextEditorItem
          label={
            <OptionToogleLink
              editor={editor}
              open={openOptionToogleLink}
              onOpen={() => setOpenOptionToogleLink(true)}
              onClose={() => setOpenOptionToogleLink(false)}
            />
          }
        />

        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleItalic().run()}
          label={<FormatItalic />}
        />

        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleBulletList().run()}
          label={<FormatListBulleted />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleOrderedList().run()}
          label={<FormatListNumbered />}
        />

        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleStrike().run()}
          label={<StrikethroughS />}
        />

        <ToolbarTextEditorItem
          onClick={() => editor.commands.toggleBlockquote()}
          label={<FormatQuote />}
        />

        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().setHorizontalRule().run()}
          label={<HorizontalRules />}
        />

        <ToolbarTextEditorItem label={<OptionsFormatAlignText editor={editor} />} />

        <ToolbarTextEditorItem onClick={() => editor.commands.toggleCodeBlock()} label={<Code />} />
      </Box>
    </Box>
  );
}
