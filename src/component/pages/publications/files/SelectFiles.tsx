import { makeStyles, Menu, MenuItem } from "@material-ui/core";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import Button from "@material-ui/core/Button";
import React, { useState } from "react";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import { useTranslation } from "next-i18next";
import ContextMenuItem from "@/components/ui/ContextMenuItem";

const SelectFiles: React.FC<{ htmlFor: string; onSelectFromLibrary: () => void }> = ({
  htmlFor,
  onSelectFromLibrary,
}) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const { t } = useTranslation("publications");
  const classes = useClasses();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button className={classes.uploadFile} disableElevation onClick={handleClick}>
        <AddCircleOutlineIcon color="inherit" fontSize="medium" />
        <Text variant={TextVariantEnum.CAPTION}>{t("fileManager.uploadFile.description")}</Text>
      </Button>

      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center",
        }}
      >
        <MenuItem
          onClick={handleClose}
          className={classes.menuItem}
          component="label"
          htmlFor={htmlFor}
        >
          <ContextMenuItem title={t("fileManager.fromDevice")} icon="upload" />
        </MenuItem>
        <MenuItem
          onClick={() => {
            onSelectFromLibrary();
            handleClose();
          }}
          className={classes.menuItem}
        >
          <ContextMenuItem title={t("fileManager.fromLibrary")} icon="library" />
        </MenuItem>
      </Menu>
    </>
  );
};

const useClasses = makeStyles((theme) => ({
  helperText: {
    textAlign: "right",
  },
  editorLabel: {
    marginBottom: 6,
  },
  uploadFile: {
    display: "flex",
    height: 94,
    width: "100%",
    border: `1px dashed ${theme.palette.gray.main}`,
    background: theme.palette.background.default,
    marginBottom: 4,
    justifyContent: "center",
    alignItems: "center",
    color: theme.palette.gray.main,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    flexDirection: "column",
    "& .MuiButton-label": {
      flexDirection: "column",
      "& span": {
        textTransform: "capitalize",
      },
    },
  },
  fileWrapper: {
    padding: 10,
    backgroundColor: theme.palette.background.default,
    borderColor: theme.palette.grey[200],
    borderWidth: 1,
    borderRadius: 4,
  },
  menuItem: {
    color: theme.palette.gray.main,
    "& .MuiListItemIcon-root": {
      minWidth: "auto",
      marginRight: 2,
    },
  },
}));

export default SelectFiles;
