/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-plusplus */
import React, { useCallback, useEffect, useState } from "react";
import { useTranslation } from "next-i18next";
import { Box, makeStyles, useTheme } from "@material-ui/core";

import SelectFiles from "./SelectFiles";
import {
  AlignItemsEnum,
  ButtonSizeEnum,
  CurrentPublicationStateEnum,
  JustifyContentEnum,
  TextAlignEnum,
  TextVariantEnum,
} from "@/enums/*";
import FlexBox from "@/components/ui/FlexBox";
import PublicationFooterComponent from "../common/PublicationFooter";
import Text from "@/components/ui/Text";
import { useRouter } from "next/router";
import FileListItemComponent from "./FirstListItem";
import LibraryModal from "@/components/ui/LibraryModal";
import {
  getAudioDuration,
  isAudioFile,
  isAvalableFileExtension,
  reorderItemList,
} from "@/utils/utils";
import { useSelector } from "react-redux";
import {
  DragDropContext,
  Droppable,
  Draggable,
  DropResult,
  DroppableProvided,
  DroppableStateSnapshot,
  DraggableProvided,
  DraggableStateSnapshot,
} from "react-beautiful-dnd";
import { PropsUserSelector } from "@/types/*";
import { toast } from "@/utils/notifications";
import { updateCurrentPublicationFiles } from "@/store/idb/models/publications";
import {
  CurrentPublication,
  LibraryItemInterface,
  NewPublicationFile,
  TimeDescriptionInterface,
} from "@/interfaces/index";
import { v4 as uuid } from "uuid";
import Button from "@/components/ui/Button";
import { getDataFile, listFile } from "@/services/webdav/files";
import { arrayBufferToBlob } from "blob-util";
import { createFile, findByFilename } from "@/store/idb/models/files";
import ActionConfirm from "@/components/ui/ActionConfirm";
import DraggableItemWrapper from "@/components/ui/DraggableItemWrapper";
import EditFieldModal, { ResetForm } from "./EditFieldModal";
import * as yup from "yup";
import OutlinedButton from "@/components/ui/Button/OutlinedButton";

type PublicationFilesFormProps = {
  onSubmit: (values: NewPublicationFile[]) => void;
  isLoading: boolean;
  publicationId: number;
  currentPublication: CurrentPublication | null;
  showLibraryModal?: boolean;
};

const PublicationFilesForm: React.FC<PublicationFilesFormProps> = ({
  onSubmit,
  isLoading,
  publicationId,
  currentPublication,
  showLibraryModal = true,
}) => {
  const [fileToEditTitle, setFileToEditTitle] = useState<NewPublicationFile | null>(null);
  const [fileToEditDescription, setFileToEditDescription] = useState<NewPublicationFile | null>(
    null,
  );
  const [loadingFile, setLoadingFile] = useState<string | boolean>(false);
  const [files, setFiles] = useState<NewPublicationFile[]>([]);
  const [openLibraryModal, setOpenLibraryModal] = useState(false);
  const [fileIdToDelete, setFileIdToDelete] = useState<null | string>(null);
  const [filesSelecteds, setFilesSelecteds] = useState<string[]>([]);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const { t } = useTranslation("publications");

  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const theme = useTheme();

  const timeDescription: TimeDescriptionInterface = c("timeDescription", { returnObjects: true });

  const updatePublication = useCallback(
    async (data: NewPublicationFile[]) => {
      const newFiles: NewPublicationFile[] = [];

      for (let counter = 0; counter < data.length; counter++) {
        const file = data[counter];

        file.position = counter;

        if (isAudioFile(file.media.type)) {
          // eslint-disable-next-line no-await-in-loop
          const duration = await getAudioDuration(file.media);

          file.duration = duration;

          newFiles[counter] = file;
        } else {
          newFiles[counter] = file;
        }
      }

      await updateCurrentPublicationFiles(Number(publicationId), newFiles);
      return newFiles;
    },
    [publicationId],
  );

  const updateStateFiles = useCallback(
    (newFiles: FileList | null) => {
      if (!newFiles) return;

      const arrayFiles: NewPublicationFile[] = Array.from(newFiles)
        .filter((file) => {
          const isAvalable = isAvalableFileExtension(file.name);

          if (!isAvalable) {
            toast(t("fileManager.messages.notAllowed"), "warning");
          }

          return isAvalable;
        })
        .map((file) => {
          const onlyName = file.name.replace(/^(.*)\..*$/, "$1");

          return {
            publicationId,
            media: file,
            title: onlyName,
            description: "",
            creator: userRdx.user.id,
            format: file.type,
            language: userRdx.user.language,
            extentSize: file.size,
            tags: "",
            status: CurrentPublicationStateEnum.PENDING,
            localId: uuid(),
            isFromLibrary: false,
          };
        });

      setFiles((prev) => {
        const filesToInsert = arrayFiles.filter(
          (newFile) => !prev.find((file) => file.title === newFile.title),
        );

        return [...prev, ...filesToInsert];
      });
    },
    [publicationId, userRdx.user, t],
  );

  useEffect(() => {
    (async () => {
      let fls =
        currentPublication?.files && currentPublication.files.length > 0
          ? currentPublication?.files
          : [];
      fls = await updatePublication(fls);
      setFiles(fls);
    })();
  }, [currentPublication]);

  useEffect(() => {
    (async () => {
      await updatePublication(files);
    })();
  }, [files]);

  const onChoseItem = useCallback(
    async (file: LibraryItemInterface) => {
      setLoadingFile(file.id);

      let fileData: false | LibraryItemInterface;

      try {
        fileData = await getDataFile(userRdx.user.id, file.filename, timeDescription);
      } catch (err) {
        toast(t("fileManager.messages.notAllowed"), "warning");

        setLoadingFile(false);
        setOpenLibraryModal(false);

        return;
      }

      if (!fileData) {
        toast(t("fileManager.messages.notAllowed"), "warning");

        setLoadingFile(false);
        setOpenLibraryModal(false);

        return;
      }

      const hasAdded = files.find((file) => file.localId === (fileData as LibraryItemInterface).id);

      if (hasAdded) {
        toast(t("fileManager.form.alreadyBeenChose"));

        setLoadingFile(false);
        setOpenLibraryModal(false);

        return;
      }

      const { filename, basename } = fileData as LibraryItemInterface;
      const filen = filename.split("/");
      const defaultTitle = filen[filen.length - 1];
      const localFile = await findByFilename(userRdx.user.id, filename);

      let blob: Blob;

      try {
        if (!localFile) {
          const result: any = await listFile(userRdx.user.id, filename);

          blob = arrayBufferToBlob(result);

          await createFile({
            title: defaultTitle,
            basename,
            filename,
            arrayBufferBlob: result,
            type: blob?.type,
            size: blob?.size,
            createdAt: new Date(),
            userId: userRdx.user.id,
          });
        } else {
          blob = arrayBufferToBlob(localFile?.arrayBufferBlob);
        }
      } catch (err) {
        toast(err.message, "error");

        setLoadingFile(false);
        setOpenLibraryModal(false);
        return;
      }

      let tags = "";

      if (fileData.tags) {
        tags = fileData.tags.join(", ");
      }

      const mediaFile = new File([blob], fileData.filename, { type: fileData.mime as string });

      const title = fileData.title ?? fileData.basename.split(".")[0].replace(/-/g, " ");

      const newFile: NewPublicationFile = {
        description: fileData.description ?? "",
        title,
        localId: fileData.id,
        publicationId,
        status: CurrentPublicationStateEnum.PENDING,
        extentSize: fileData.size,
        isFromLibrary: true,
        format: fileData.sizeFormatted,
        language: fileData.language ?? userRdx.user.language,
        creator: fileData.ownerId,
        tags,
        media: mediaFile,
        creatorName: fileData.ownerName,
      };

      setFiles((prev) => [...prev, newFile]);

      setLoadingFile(false);
      setOpenLibraryModal(false);
    },
    [userRdx.user.id, timeDescription, t, files, publicationId, userRdx.user.language],
  );

  const handleCloseModalRemoveFile = useCallback(() => {
    setFileIdToDelete(null);
    setOpenDeleteModal(false);
  }, []);

  const handleRemoveFile = useCallback((fileId: string | string[]) => {
    if (Array.isArray(fileId)) {
      setFiles((prevState) => prevState.filter((file) => !fileId.includes(file.localId)));

      return;
    }

    setFiles((prevState) => prevState.filter((file) => file.localId !== fileId));
  }, []);

  const handleSelectFile = useCallback((publicationId: string) => {
    setFilesSelecteds((curr) => {
      const hasIncluded = curr.find((includedId) => includedId === publicationId);

      if (!hasIncluded) {
        return [...curr, publicationId];
      }

      const newState = curr.filter((included) => included !== publicationId);

      return newState;
    });
  }, []);

  const router = useRouter();

  const classes = useClasses();

  const handleUpdateTitle = useCallback(
    (values: { [key: string]: string }, resetForm: ResetForm) => {
      setFiles((currentState) =>
        currentState.map((file) => {
          if (file.localId === fileToEditTitle?.localId) {
            return {
              ...file,
              title: values.title,
            };
          }

          return file;
        }),
      );

      resetForm();
      setFileToEditTitle(null);
    },
    [fileToEditTitle],
  );

  const handleUpdateDescription = useCallback(
    (values: { [key: string]: string }, resetForm: ResetForm) => {
      setFiles((currentState) =>
        currentState.map((file) => {
          if (file.localId === fileToEditDescription?.localId) {
            return {
              ...file,
              description: values.description,
            };
          }

          return file;
        }),
      );

      resetForm();
      setFileToEditDescription(null);
    },
    [fileToEditDescription],
  );

  const validationSchemaTitle = yup.object().shape({
    title: yup
      .string()
      .max(60, `60 ${t("fileManager.validation.maxCharacters")}`)
      .required(t("fileManager.validation.required")),
  });

  const validationSchemaDescription = yup.object().shape({
    description: yup.string().max(400, `400 ${t("fileManager.validation.maxCharacters")}`),
  });

  const actions = (item: LibraryItemInterface) => {
    if (item.type === "file" && isAvalableFileExtension(item.filename)) {
      return (
        <Button
          handleClick={() => onChoseItem(item)}
          title={t("fileManager.form.chooseFile")}
          size={ButtonSizeEnum.SMALL}
          className={classes.editFileButton}
          isLoading={loadingFile === item.id}
        />
      );
    }

    return null;
  };

  const onDragEnd = useCallback((result: DropResult) => {
    if (!result?.destination?.index) {
      return;
    }

    setFiles((fileList) =>
      reorderItemList(fileList, result.source.index, result.destination?.index as number),
    );
  }, []);

  return (
    <>
      <FlexBox
        justifyContent={JustifyContentEnum.FLEXSTART}
        alignItems={AlignItemsEnum.STRETCH}
        textAlign={TextAlignEnum.LEFT}
      >
        <Box pb={20}>
          <SelectFiles htmlFor="files" onSelectFromLibrary={() => setOpenLibraryModal(true)} />
          <input
            name="files"
            id="files"
            type="file"
            data-testid="input-file-select"
            hidden
            multiple
            onChange={($event) => {
              updateStateFiles($event?.currentTarget?.files);
            }}
          />
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable-file-list">
              {(provided: DroppableProvided, snapshot: DroppableStateSnapshot) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  className={snapshot.isDraggingOver ? classes.isDraggingOver : ""}
                  data-testid="files-wrapper"
                >
                  {files.map((file, index) => (
                    <Draggable
                      key={file.title}
                      draggableId={file.localId}
                      index={index}
                      data-testid="file-item"
                    >
                      {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
                        <DraggableItemWrapper provided={provided} snapshot={snapshot}>
                          <FileListItemComponent
                            file={file}
                            user={userRdx.user}
                            onClickRemove={() => {
                              setFileIdToDelete(file.localId);
                              setOpenDeleteModal(true);
                            }}
                            onClickInDescription={() => setFileToEditDescription(file)}
                            onClickInTitle={() => setFileToEditTitle(file)}
                            onSelectFile={() => handleSelectFile(file.localId)}
                          />
                        </DraggableItemWrapper>
                      )}
                    </Draggable>
                  ))}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </Box>
      </FlexBox>

      {Boolean(fileToEditTitle) && (
        <EditFieldModal
          fieldName="title"
          handleClose={() => setFileToEditTitle(null)}
          handlerSave={handleUpdateTitle}
          initialValues={{ title: fileToEditTitle?.title ?? "" }}
          open={Boolean(fileToEditTitle)}
          loading={false}
          title={t("fileManager.editTitle")}
          validationSchema={validationSchemaTitle}
          maxLength={60}
        />
      )}

      {Boolean(fileToEditDescription) && (
        <EditFieldModal
          fieldName="description"
          handleClose={() => setFileToEditDescription(null)}
          handlerSave={handleUpdateDescription}
          initialValues={{ description: fileToEditDescription?.description ?? "" }}
          open
          loading={false}
          title={t("fileManager.editDescription")}
          validationSchema={validationSchemaDescription}
          maxLength={400}
        />
      )}

      {showLibraryModal && (
        <LibraryModal
          title={t("fileManager.form.selectFileModalTitle")}
          handleClose={() => setOpenLibraryModal(false)}
          open={openLibraryModal}
          onlyDirectories={false}
          options={actions}
        />
      )}

      <ActionConfirm
        data-testid="delete-file-modal-confirm"
        onOk={() => {
          if (fileIdToDelete) {
            handleRemoveFile(fileIdToDelete);
            handleCloseModalRemoveFile();
            return;
          }

          if (filesSelecteds.length) {
            handleRemoveFile(filesSelecteds);
            setFilesSelecteds([]);
            setOpenDeleteModal(false);
          }
        }}
        open={openDeleteModal}
        onClose={() => {
          handleCloseModalRemoveFile();
          setOpenDeleteModal(false);
        }}
        showMessage={false}
        title={t("fileManager.removeConfirmMessage")}
      />

      <PublicationFooterComponent
        nextLabel={c("form.nextIntro")}
        cancelLabel={c("form.backButton")}
        onClickCancel={() => router.back()}
        currentStep={2}
        totalStep={3}
        onClickNext={() => onSubmit(files)}
        paddingBottom={58}
        loading={isLoading}
      >
        <Box
          display={filesSelecteds.length ? "flex" : "none"}
          data-testid="remove-files-button-wrapper"
          justifyContent="center"
          alignItems="center"
          pt={1}
        >
          <OutlinedButton
            data-testid="remove-files-button"
            key="remove-all-button"
            title={t("fileManager.uploadFile.actions.removeAll")}
            iconColor={theme.palette.danger.main}
            handleClick={() => {
              setOpenDeleteModal(true);
            }}
            color={theme.palette.danger.main}
            startIcon="trash"
            size={ButtonSizeEnum.SMALL}
            disabled={!filesSelecteds.length}
          />
        </Box>

        {!files.length && (
          <Box className={classes.messageBox}>
            <Text variant={TextVariantEnum.CAPTION}>
              <Box
                component="span"
                dangerouslySetInnerHTML={{
                  __html: t("fileManager.infoMessage", {
                    isNotMandatoryMessage: `<strong>${t(
                      "fileManager.isNotMandatoryMessage",
                    )}</strong>`,
                  }),
                }}
              />
            </Text>
          </Box>
        )}
      </PublicationFooterComponent>
    </>
  );
};

const useClasses = makeStyles((theme) => ({
  fileWrapper: {
    padding: 10,
    backgroundColor: theme.palette.background.default,
    borderColor: theme.palette.grey[200],
    borderWidth: 1,
    borderRadius: 4,
  },
  helperText: {
    textAlign: "right",
  },
  editorLabel: {
    marginBottom: 6,
  },
  messageBox: {
    backgroundColor: theme.palette.variation1.light,
    color: theme.palette.variation6.main,
    padding: "10px 20px",
  },
  editFileButton: {
    marginLeft: 10,
  },
  isDraggingOver: {
    backgroundColor: theme.palette.background.paper,
  },
}));

export default PublicationFilesForm;
