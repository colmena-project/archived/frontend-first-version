import React, { memo, useMemo } from "react";
import { useTheme } from "@material-ui/core";
import { useTranslation } from "next-i18next";

import { ButtonSizeEnum } from "@/enums/*";
import { EditPublicationFile, NewPublicationFile, UserInfoInterface } from "@/interfaces/index";
import FileMetadataCollapse from "@/components/ui/FileMetadataCollapse";
import MetaDataField from "@/components/pages/publications/files/MetadataField";
import { MediaInterface } from "@/interfaces/cms";

import { ActionButtonOutline } from "./styled";

type FileListItemProps = {
  file: NewPublicationFile | EditPublicationFile;
  user: UserInfoInterface;
  onClickRemove: () => void;
  onClickInTitle: () => void;
  onClickInDescription: () => void;
  onSelectFile?: () => void;
  hideCheckBox?: boolean;
};

const FileListItem: React.FC<FileListItemProps> = ({
  file,
  user,
  onClickRemove,
  onClickInTitle,
  onClickInDescription,
  onSelectFile,
  hideCheckBox,
}) => {
  const { t } = useTranslation("publications");

  const theme = useTheme();
  const { media, localId, tags, language, description, title, creator, extentSize } = file;

  const contentFile = useMemo(
    () => (
      <>
        <MetaDataField
          label={t("fileManager.uploadFile.form.title.label")}
          value={title || ""}
          onClick={onClickInTitle}
          noWrap={false}
        />

        <MetaDataField
          label={t("fileManager.uploadFile.form.description.label")}
          value={
            description
              ? (description as string)
              : t("fileManager.uploadFile.form.description.placeholder")
          }
          onClick={onClickInDescription}
          noWrap={false}
        />
      </>
    ),
    [t, title, onClickInTitle, description, onClickInDescription],
  );

  return (
    <>
      <FileMetadataCollapse
        data-testid="file-list-item"
        key={localId}
        hideCheckBox={hideCheckBox}
        content={contentFile}
        onSelectFile={onSelectFile}
        metadata={{
          file: file.isFromCMS ? undefined : (media as File),
          fileId: localId,
          size: extentSize,
          tags,
          mime: (media as any).type
            ? (media as File).type
            : (media as MediaInterface).data.attributes.mime,
          producedBy: user.media.name,
          creator,
          language,
          description,
          title,
          media: file.isFromCMS ? (media as MediaInterface) : undefined,
        }}
        actions={[
          <ActionButtonOutline
            key={`${localId}button`}
            title={t("fileManager.uploadFile.actions.remove")}
            iconColor={theme.palette.danger.main}
            handleClick={onClickRemove}
            color={theme.palette.danger.main}
            startIcon="trash"
            data-testid="remove-file-button"
            size={ButtonSizeEnum.SMALL}
          />,
        ]}
      />
    </>
  );
};

export default memo(FileListItem);
