import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@/components/ui/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@/components/ui/Button";
import { Formik, Form, Field, FieldProps, ErrorMessage, FormikState } from "formik";
import * as Yup from "yup";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import { useTranslation } from "next-i18next";
import { Box } from "@material-ui/core";
import { Assign, ObjectShape } from "yup/lib/object";

export type ResetForm = (
  nextState?:
    | Partial<
        FormikState<{
          [key: string]: string;
        }>
      >
    | undefined,
) => void;

type EditFieldModalProps = {
  open: boolean;
  handleClose: () => void;
  handlerSave: (values: { [key: string]: string }, resetForm: ResetForm) => void;
  loading: boolean;
  validationSchema: Yup.ObjectSchema<Assign<ObjectShape, any>>;
  initialValues: { [key: string]: string };
  title: string;
  fieldName: string;
  maxLength: number;
};

const EditFieldModal: React.FC<EditFieldModalProps> = ({
  open,
  handleClose,
  loading,
  handlerSave,
  validationSchema,
  initialValues,
  title,
  fieldName,
  maxLength,
}) => {
  const { t } = useTranslation("publications");

  const { t: c } = useTranslation("common");

  const classes = useStyles();

  return (
    <Modal
      data-testid="edit-field-modal"
      title={title}
      open={open}
      handleClose={loading ? undefined : handleClose}
    >
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, { resetForm }) => handlerSave(values, resetForm)}
      >
        {({ setFieldValue, submitForm, errors }: any) => (
          <Form
            className={classes.form}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                submitForm();
              }
            }}
          >
            <Field name={fieldName} InputProps={{ notched: true }}>
              {({ field }: FieldProps) => {
                const remaining = maxLength - field.value.length;

                return (
                  <TextField
                    error={Boolean(errors[fieldName])}
                    inputProps={{
                      maxLength,
                      autoComplete: "off",
                      form: {
                        autoComplete: "off",
                      },
                    }}
                    variant="outlined"
                    {...field}
                    onChange={(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>
                      setFieldValue(fieldName, event.target.value)
                    }
                    multiline
                    rows={5}
                    helperText={`${remaining} ${
                      remaining > 1
                        ? t("createPublication.form.description.helper.plural")
                        : t("createPublication.form.description.helper.singular")
                    }`}
                    FormHelperTextProps={{ className: classes.helperText }}
                  />
                );
              }}
            </Field>
            <ErrorMessage name={fieldName}>
              {(msg) => <ErrorMessageForm message={msg} />}
            </ErrorMessage>

            <Box display="flex" justifyContent="flex-end" width="100%" mt={2}>
              <Button
                title={c("form.submitSaveTitle")}
                type="submit"
                className={classes.submit}
                disabled={loading}
                isLoading={loading}
              />
            </Box>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

const useStyles = makeStyles(() => ({
  form: {
    "& .MuiTextField-root": {
      width: "100%",
    },
  },
  submit: {
    marginLeft: 10,
  },
  helperText: {
    textAlign: "right",
    marginRight: 0,
  },
}));

export default EditFieldModal;
