import React from "react";
import { makeStyles, Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { TextAlignEnum, TextColorEnum, TextVariantEnum } from "@/enums/*";
import classnames from "classnames";

const useClasses = makeStyles((theme) => ({
  wrapper: {
    padding: "3px 16px",
    position: "relative",
  },
  textField: {
    maxWidth: "100%",
  },
  withEllipsis: {
    maxWidth: "100%",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
  textFieldValue: {
    color: theme.palette.gray.main,
    textTransform: "capitalize",
  },
}));

type Props = {
  label: string;
  value: string;
  className?: string;
  wrapperStyle?: string;
  onClick?: () => void;
  noWrap?: boolean;
  labelAs?: string;
};

const MetaDataField = ({
  label,
  value,
  className,
  wrapperStyle,
  onClick,
  noWrap = true,
  labelAs = "span",
}: Props) => {
  const classes = useClasses();
  return (
    <Box
      className={classnames(classes.wrapper, wrapperStyle)}
      onClick={onClick}
      data-testid="metadata-line"
    >
      <Text
        paragraph={false}
        align={TextAlignEnum.LEFT}
        color={TextColorEnum.TEXTPRIMARY}
        className={classnames(
          classes.textField,
          className,
          noWrap ? classes.withEllipsis : undefined,
        )}
        variant={TextVariantEnum.BODY2}
      >
        <Text color={TextColorEnum.INHERIT} component={labelAs} variant={TextVariantEnum.INHERIT}>
          {label}:{" "}
        </Text>
        <Text
          paragraph={false}
          align={TextAlignEnum.LEFT}
          className={classes.textFieldValue}
          component="span"
          variant={TextVariantEnum.BODY2}
        >
          {value}
        </Text>
      </Text>
    </Box>
  );
};

export default MetaDataField;
