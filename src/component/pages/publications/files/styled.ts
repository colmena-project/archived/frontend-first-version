import OutlinedButton from "@/components/ui/Button/OutlinedButton";
import { styled } from "@material-ui/core";

export const ActionButtonOutline = styled(OutlinedButton)({
  borderRadius: 5,
  padding: "3px 4px !important",
});
