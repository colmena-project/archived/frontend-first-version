/* eslint-disable react/prop-types */
import React from "react";
import { Box, Fade } from "@material-ui/core";
import theme from "@/styles/theme";
import {
  FacebookShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";
import { useTranslation } from "next-i18next";
import Text from "@/components/ui/Text";
import { TextAlignEnum, TextColorEnum, TextVariantEnum } from "@/enums/*";
import SvgIconAux from "@/components/ui/SvgIcon";

import { DialogStyled, WrapperLinks } from "./styled";

type SharePublicationModalProps = {
  children?: React.ReactNode;
  open: boolean;
  onClose: () => void;
  title: string;
  publicationId: string;
  description: string;
  baseUrl: string;
};

const SharePublicationModal: React.FC<SharePublicationModalProps> = ({
  open,
  onClose,
  baseUrl,
  description,
  publicationId,
  title,
}) => {
  const { t } = useTranslation("publications");

  return (
    <DialogStyled
      open={open}
      onClose={onClose}
      fullWidth
      fullScreen
      BackdropProps={{
        id: "backdrop",
      }}
    >
      <Fade in={open}>
        <WrapperLinks data-testid="share-modal-items-wrapper">
          <Box display="flex" flexDirection="row" justifyContent="flex-end">
            <SvgIconAux
              icon="share_circle"
              htmlColor={theme.palette.primary.contrastText}
              fontSize={20}
            />
          </Box>
          <FacebookShareButton
            title={title}
            url={`${baseUrl}/public/publications/${publicationId}`}
            quote={description}
            beforeOnClick={onClose}
            data-testid="share-option"
          >
            <Text
              paragraph={false}
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.INHERIT}
              variant={TextVariantEnum.BODY2}
            >
              {t("commons.share.facebook")}
            </Text>

            <SvgIconAux
              icon="facebook_outline"
              htmlColor={theme.palette.primary.contrastText}
              fontSize={20}
            />
          </FacebookShareButton>

          <TwitterShareButton
            title={title}
            url={`${baseUrl}/public/publications/${publicationId}`}
            beforeOnClick={onClose}
            data-testid="share-option"
          >
            <Text
              paragraph={false}
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.INHERIT}
              variant={TextVariantEnum.BODY2}
            >
              {t("commons.share.twitter")}
            </Text>

            <SvgIconAux
              icon="twitter_outline"
              htmlColor={theme.palette.primary.contrastText}
              fontSize={20}
            />
          </TwitterShareButton>

          <WhatsappShareButton
            title={title}
            url={`${baseUrl}/public/publications/${publicationId}`}
            beforeOnClick={onClose}
            data-testid="share-option"
          >
            <Text
              paragraph={false}
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.INHERIT}
              variant={TextVariantEnum.BODY2}
            >
              {t("commons.share.whatsapp")}
            </Text>

            <SvgIconAux
              icon="whatsapp_outline"
              htmlColor={theme.palette.primary.contrastText}
              fontSize={20}
            />
          </WhatsappShareButton>

          <TelegramShareButton
            title={title}
            url={`${baseUrl}/public/publications/${publicationId}`}
            beforeOnClick={onClose}
            data-testid="share-option"
          >
            <Text
              paragraph={false}
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.INHERIT}
              variant={TextVariantEnum.BODY2}
            >
              {t("commons.share.telegram")}
            </Text>

            <SvgIconAux
              icon="telegram_outline"
              htmlColor={theme.palette.primary.contrastText}
              fontSize={20}
            />
          </TelegramShareButton>
        </WrapperLinks>
      </Fade>
    </DialogStyled>
  );
};

export default SharePublicationModal;
