import { Box, Dialog, styled } from "@material-ui/core";
import theme from "@/styles/theme";

export const DialogStyled = styled(Dialog)({
  "& .MuiPaper-root": {
    background: "none",
    boxShadow: "none",
  },
  "& .MuiDialog-container ": {
    height: "auto",
  },
  display: "flex",
  alignItems: "center",
  height: "100vh",
  justifyContent: "flex-end",
  flexDirection: "row",
  zIndex: theme.zIndex.drawer + theme.zIndex.appBar + 1,
});

export const WrapperLinks = styled(Box)({
  display: "flex",
  flexDirection: "column",
  gap: 10,
  marginRight: 11,
  color: theme.palette.primary.contrastText,
  "& button": {
    textAlign: "left",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
  },

  "& p": {
    marginRight: 10,
  },
});
