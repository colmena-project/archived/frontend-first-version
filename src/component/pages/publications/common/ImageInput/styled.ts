import { Button, styled, Theme } from "@material-ui/core";

export const Container = styled(Button)<Theme, any>(({ theme }) => ({
  display: "flex",
  height: 94,
  width: "100%",
  border: `1px dashed ${theme.palette.gray.main}`,
  background: theme.palette.background.default,
  marginBottom: 4,
  justifyContent: "center",
  alignItems: "center",
  color: theme.palette.gray.main,
  backgroundSize: "cover",
  backgroundPosition: "center center",
  flexDirection: "column",
  "& .MuiButton-label": {
    flexDirection: "column",
    "& span": {
      textTransform: "capitalize",
    },
  },
}));
