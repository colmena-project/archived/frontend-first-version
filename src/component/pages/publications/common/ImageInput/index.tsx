import React, { useEffect, useState } from "react";
import Text from "@/components/ui/Text";
import AddPhotoAlternateIcon from "@material-ui/icons/AddPhotoAlternate";
import { TextVariantEnum } from "@/enums/*";
import { FieldInputProps } from "formik";

import { Container } from "./styled";

type ImageInputProps = {
  field: FieldInputProps<any>;
  description: string;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
};

const ImageInput: React.FC<ImageInputProps> = ({ field, description, setFieldValue }) => {
  const [imgUri, setImgUri] = useState<string | undefined>();

  useEffect(() => {
    if (typeof field?.value === "string") {
      setImgUri(`url('${field.value}')`);
      return;
    }

    const hasImage = field?.value && field?.value.name;

    if (hasImage) {
      setImgUri(`url('${URL.createObjectURL(field.value)}')`);
    } else {
      setFieldValue("image", null);
    }
  }, [field.value, setFieldValue]);

  return (
    <Container
      disableElevation
      style={imgUri ? { backgroundImage: imgUri } : undefined}
      component="label"
    >
      <AddPhotoAlternateIcon color="inherit" fontSize="medium" />
      <Text variant={TextVariantEnum.CAPTION}>{description}</Text>
      <input
        type="file"
        hidden
        accept="image/jpg, image/png, image/jpeg"
        onChange={($event) => {
          const files = $event?.currentTarget?.files;

          if (files?.length) {
            setFieldValue("image", Array.from(files).pop());
          }
        }}
      />
    </Container>
  );
};

export default ImageInput;
