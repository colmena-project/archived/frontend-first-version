import LayoutApp from "@/components/statefull/LayoutApp";
import { Box, Button, styled } from "@material-ui/core";

export const PublicationLayout = styled(LayoutApp)({
  "& .MuiTypography-h3": { fontSize: "20px !important", fontWeight: 900 },
});

export const PublicationMenuOption = styled(Box)({
  alignSelf: "flex-end",
});

export const NextButton = styled(Button)({
  padding: 0,
  margin: 0,
  weight: 700,
  color: "#1976D2 !important",
  fontWeight: 600,
});

export const CancelButton = styled(Button)({
  padding: 0,
  margin: 0,
  fontWeight: 600,
});

export const FooterItemStatic = styled(Box)({
  display: "flex",
  flexDirection: "row",
  flexWrap: "nowrap",
  justifyContent: "space-around",
  padding: 10,
  position: "relative",
  margin: 0,
});

export const FooterContainer = styled(Box)(({ theme }) => ({
  margin: 0,
  padding: 0,
  left: 0,
  bottom: 0,
  width: "100%",
  fontSize: 12,
  background: theme.palette.common.white,
}));

export const FooterWrapper = styled(Box)(({ loading }: { loading: boolean }) => ({
  alignSelf: "stretch",
  zIndex: loading ? 1 : 99999,
  position: "fixed",
  bottom: 0,
  left: 0,
  right: 0,
}));
