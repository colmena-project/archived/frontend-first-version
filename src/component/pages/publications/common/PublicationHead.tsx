import React from "react";
import { NextSeo } from "next-seo";
import { PublicationQueryInterface } from "@/interfaces/cms";
import { getExtensionFilename } from "@/utils/utils";
import CONSTANTS from "@/constants/index";

const PublicationHead: React.FC<{
  publication: PublicationQueryInterface;
  baseUrl: string;
  imageUrl: string;
}> = ({ publication, baseUrl, imageUrl }) => {
  const extension = getExtensionFilename(imageUrl) ?? "";

  return (
    <NextSeo
      title={publication.attributes.title}
      description={publication.attributes.description}
      openGraph={{
        url: `${baseUrl}/public/publications/${publication.id}`,
        title: publication.attributes.title,
        description: publication.attributes.description,
        images: [
          {
            url: imageUrl,
            alt: publication.attributes.title,
            type: `image/${extension.toLowerCase()}`,
          },
        ],
        siteName: CONSTANTS.APP_NAME,
      }}
      twitter={{
        cardType: "summary_large_image",
      }}
    />
  );
};

export default PublicationHead;
