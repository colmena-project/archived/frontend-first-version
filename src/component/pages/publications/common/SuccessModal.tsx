import React from "react";

import Modal from "@/components/ui/Modal";
import { ButtonColorEnum, ButtonVariantEnum } from "@/enums/index";
import Button from "@/components/ui/Button";
import Box from "@material-ui/core/Box";
import SvgIconAux from "@/components/ui/SvgIcon";
import { useTheme } from "@material-ui/styles";
import { makeStyles, Theme } from "@material-ui/core";

type SuccessModalProps = {
  open: boolean;
  onClose: () => void;
  onClickPrimaryAction: () => void;
  onClickSecondaryAction: () => void;
  primaryLabel: string;
  secondaryLabel: string;
};

const SuccessModal: React.FC<SuccessModalProps> = ({
  open,
  onClickPrimaryAction,
  onClickSecondaryAction,
  onClose,
  primaryLabel,
  secondaryLabel,
}) => {
  const theme: Theme = useTheme();
  const classes = useClasses();

  return (
    <Modal
      open={open}
      handleClose={onClose}
      actions={
        <Box display="flex" flex="1" flexDirection="column" color={theme.palette.danger.main}>
          <Button
            handleClick={onClickPrimaryAction}
            variant={ButtonVariantEnum.TEXT}
            color={ButtonColorEnum.PRIMARY}
            title={primaryLabel}
            className={classes.buttonText}
            style={{ color: theme.palette.gray.main }}
          />
          <Button
            handleClick={onClickSecondaryAction}
            variant={ButtonVariantEnum.TEXT}
            title={secondaryLabel}
            style={{ color: theme.palette.error.main }}
            className={classes.buttonText}
          />
        </Box>
      }
      className={classes.backdrop}
    >
      <Box textAlign="center">
        <SvgIconAux icon="success_circle" htmlColor={theme.palette.success.main} fontSize={60} />
      </Box>
    </Modal>
  );
};

const useClasses = makeStyles(() => ({
  backdrop: {
    zIndex: "999999 !important" as unknown as number,

    "& .MuiPaper-rounded": {
      borderRadius: 9,
    },
  },

  buttonText: {
    "& .MuiButton-label": {
      textTransform: "capitalize",
    },
  },
}));

export default SuccessModal;
