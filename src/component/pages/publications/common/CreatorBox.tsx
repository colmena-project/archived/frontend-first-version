import React from "react";

import Avatar from "@/components/pages/profile/AvatarWithContextMenu";
import {
  AlignItemsEnum,
  FlexDirectionEnum,
  JustifyContentEnum,
  TextAlignEnum,
  TextDisplayEnum,
  TextVariantEnum,
} from "@/enums/*";
import { Box, makeStyles } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { PropsUserSelector } from "@/types/*";
import FlexBox from "@/components/ui/FlexBox";

const CreatorBoxComponent: React.FC<{ user: PropsUserSelector["user"]; publicationDate?: string }> =
  ({ user, publicationDate }) => {
    const classes = useClasses();

    return (
      <FlexBox
        alignItems={AlignItemsEnum.CENTER}
        flexDirection={FlexDirectionEnum.ROW}
        justifyContent={JustifyContentEnum.FLEXSTART}
        padding={0}
      >
        <Avatar size={4} showEditImage={false} />
        <Box className={classes.createWrapper} textAlign={TextAlignEnum.LEFT}>
          <Text
            variant={TextVariantEnum.CAPTION}
            align={TextAlignEnum.LEFT}
            className={classes.creator}
            display={TextDisplayEnum.BLOCK}
          >
            {user.media?.name} {user?.region ? `, ${user?.region}` : ""} | {user.name}
          </Text>

          {publicationDate && (
            <Text
              variant={TextVariantEnum.CAPTION}
              align={TextAlignEnum.LEFT}
              className={classes.creator}
            >
              {publicationDate}
            </Text>
          )}
        </Box>
      </FlexBox>
    );
  };

const useClasses = makeStyles((theme) => ({
  creator: {
    color: theme.palette.gray.main,
    fontSize: 10,
    margin: 0,
    padding: 0,
    letterSpacing: "1px",
    lineHeight: "10px",
  },
  createWrapper: {
    marginLeft: "10px",
  },
}));

export default CreatorBoxComponent;
