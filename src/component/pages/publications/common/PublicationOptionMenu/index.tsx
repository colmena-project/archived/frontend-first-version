import React, { ReactChildren } from "react";
import { AllIconProps, FontSizeIconProps } from "@/types/*";
import { OptionButton, OptionName, OptionIcon, OptionItem } from "./styled";
import SvgIconAux from "@/components/ui/SvgIcon";

type PublicationOptionMenuProps = {
  children: ReactChildren | string;
  onClick?: () => void;
  icon?: AllIconProps;
  iconSize?: number | FontSizeIconProps;
  iconColor?: string;
  textColor?: string;
  "data-testid"?: string;
  disabled?: boolean;
};

type PublicationOptionMenuChildren = Pick<PublicationOptionMenuProps, "children">;

function PublicationOptionMenu({
  children,
  onClick,
  icon,
  iconSize = 18,
  textColor,
  iconColor,
  disabled = false,
  "data-testid": dataTestid,
}: PublicationOptionMenuProps) {
  const handleClick = typeof onClick === "function" ? onClick : undefined;

  const handleChildren = ({ children }: PublicationOptionMenuChildren) => {
    if (typeof children === "string") {
      return (
        <OptionItem
          onClick={disabled ? undefined : handleClick}
          data-testid={dataTestid ?? "tools-menu-option"}
          style={{ opacity: disabled ? 0.5 : 1 }}
        >
          {icon && (
            <OptionIcon data-testid={`${icon}_icon`}>
              {icon && <SvgIconAux icon={icon} fontSize={iconSize} htmlColor={iconColor} />}
            </OptionIcon>
          )}
          <OptionName style={{ color: textColor }}>{children}</OptionName>
        </OptionItem>
      );
    }

    return children;
  };

  return <OptionButton>{handleChildren({ children })}</OptionButton>;
}

export default PublicationOptionMenu;
