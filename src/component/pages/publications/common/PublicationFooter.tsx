import React, { useState } from "react";
import BackIcon from "@material-ui/icons/ChevronLeft";
import NextIcon from "@material-ui/icons/ChevronRight";
import { ToolsMenuOption } from "@/components/ui/ToolsMenu";

import Text from "@/components/ui/Text";
import FlexBox from "@/components/ui/FlexBox";
import { AlignItemsEnum, FlexDirectionEnum, JustifyContentEnum, TextVariantEnum } from "@/enums/*";
import FooterApp from "@/components/ui/FooterApp";
import { useTranslation } from "next-i18next";
import PublicationToolsMenu from "../PublicationToolsMenu";

import {
  PublicationMenuOption,
  NextButton,
  CancelButton,
  FooterItemStatic,
  FooterContainer,
  FooterWrapper,
} from "./styled";

type PublicationFooterComponentProps = {
  currentStep: number;
  totalStep: number;
  onClickNext?: () => void | Promise<void>;
  onClickCancel: () => void | Promise<void>;
  children?: React.ReactNode;
  cancelLabel: string;
  nextLabel: string;
  paddingBottom?: number;
  loading?: boolean;
  onSaveDraft?: () => void;
  onPublish?: () => void;
  className?: string;
};

const PublicationFooterComponent: React.FC<PublicationFooterComponentProps> = ({
  currentStep,
  onClickNext,
  totalStep,
  children,
  onClickCancel,
  cancelLabel,
  nextLabel,
  loading = false,
  onPublish,
  onSaveDraft,
  className,
}) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const { t } = useTranslation("publications");

  const toggleMenu = () => {
    setMenuOpen((old) => !old);
  };

  return (
    <FooterWrapper loading={loading || menuOpen} className={className}>
      <FooterContainer>
        {children}
        <FooterItemStatic>
          <FlexBox
            alignItems={AlignItemsEnum.CENTER}
            flexDirection={FlexDirectionEnum.ROW}
            justifyContent={JustifyContentEnum.SPACEBETWEEN}
            padding={0}
          >
            <CancelButton
              color="default"
              startIcon={<BackIcon />}
              onClick={onClickCancel}
              disabled={loading}
            >
              {cancelLabel}
            </CancelButton>

            <Text variant={TextVariantEnum.CAPTION}>
              {currentStep}/{totalStep}
            </Text>

            <PublicationToolsMenu onClose={toggleMenu} open={menuOpen}>
              <PublicationMenuOption>
                <ToolsMenuOption icon="draft" onClick={onSaveDraft}>
                  {t("preview.draft")}
                </ToolsMenuOption>
              </PublicationMenuOption>

              <PublicationMenuOption>
                <ToolsMenuOption icon="publish" onClick={onPublish}>
                  {t("preview.publish")}
                </ToolsMenuOption>
              </PublicationMenuOption>
            </PublicationToolsMenu>

            <NextButton
              color="primary"
              endIcon={<NextIcon />}
              onClick={currentStep === totalStep ? toggleMenu : onClickNext}
              disabled={loading}
            >
              {nextLabel}
            </NextButton>
          </FlexBox>
        </FooterItemStatic>
      </FooterContainer>
      <FooterApp />
    </FooterWrapper>
  );
};

export default PublicationFooterComponent;
