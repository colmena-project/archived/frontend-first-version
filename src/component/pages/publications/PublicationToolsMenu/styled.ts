import { styled, Dialog } from "@material-ui/core";

export const Modal = styled(Dialog)({
  "& .MuiPaper-root": {
    background: "none",
    boxShadow: "none",
    position: "fixed",
    bottom: 110,
    right: 0,
    margin: "5px 0px",
    padding: "0 15px",
    width: "100%",
  },
  zIndex: 99999999,
});
