import React, { ReactElement } from "react";

import { Modal } from "./styled";

type PublicationToolsMenuProps = {
  children?: ReactElement | ReactElement[];
  open: boolean;
  onClose: () => void;
};

function PublicationToolsMenu({ children, onClose, open }: PublicationToolsMenuProps) {
  return (
    <Modal open={open} onClose={onClose} fullWidth data-testid="publication-tool-menu">
      {children}
    </Modal>
  );
}

export default PublicationToolsMenu;
