import React, { useCallback, useEffect, useState } from "react";
import { makeStyles, Box, IconButton, useTheme, Collapse } from "@material-ui/core";
import FlexBox from "@/components/ui/FlexBox";
import {
  AlignItemsEnum,
  JustifyContentEnum,
  FlexDirectionEnum,
  TextAlignEnum,
  TextColorEnum,
  TextVariantEnum,
  CurrentPublicationStateEnum,
} from "@/enums/*";
import SvgIcon from "@/components/ui/SvgIcon";
import Text from "@/components/ui/Text";
import classnames from "classnames";
import { useTranslation } from "next-i18next";
import { CurrentPublication } from "@/interfaces/index";
import Image from "next/image";
import MediaProfileImage from "./MediaProfileImage";
import MetaDataField from "../files/MetadataField";
import UploadListItemComponent from "./UploadListItem";
import Divider from "@/components/ui/Divider";
import { format } from "date-fns";
import { getPublication } from "@/store/idb/models/publications";
import { toast } from "@/utils/notifications";
import { reduceImageSize } from "@/utils/utils";

type PublicationItemProps = {
  publication: CurrentPublication;
  mediaName: string;
  onConfirmeDelete: (publicationId: number, fileId: string) => void;
};

const PublicationItem: React.FC<PublicationItemProps> = ({
  publication,
  mediaName,
  onConfirmeDelete,
}) => {
  const [uploadedCount, setUploadedCount] = useState(0);
  const [image, setImage] = useState<string | null>(null);
  const [isOpen, setIsOpen] = useState(false);
  const { t } = useTranslation("publications");

  const [mode, setMode] = useState(publication.mode);

  const classes = useClasses();
  const theme = useTheme();

  const fetchPublication = useCallback(async () => {
    try {
      const data: CurrentPublication = await getPublication(publication.id);

      const count =
        data.files?.filter((file) => file.status === CurrentPublicationStateEnum.UPLOADED).length ??
        0;

      setUploadedCount(data.files?.length ? count : (publication.files?.length as number));
      setMode(data.mode);
    } catch (err) {
      toast(err.message, "error");
    }
  }, [publication]);

  useEffect(() => {
    fetchPublication();

    const signer = setInterval(fetchPublication, 3000);

    return () => clearInterval(signer);
  }, [fetchPublication]);

  const getPublicationImage = useCallback(async () => {
    if (!publication.thumbnail) {
      setImage(null);
      return;
    }

    const optimized = await reduceImageSize(publication.thumbnail, 70, 59, 59);
    const imageBase64 = URL.createObjectURL(optimized);

    setImage(imageBase64);
  }, [publication.thumbnail]);

  useEffect(() => {
    getPublicationImage();
  }, [getPublicationImage]);

  const onToggleCollapse = () => setIsOpen((prevState) => !prevState);

  return (
    <Box className={classes.wrapper} marginTop={2}>
      <FlexBox
        flexDirection={FlexDirectionEnum.ROW}
        alignItems={AlignItemsEnum.CENTER}
        justifyContent={JustifyContentEnum.SPACEBETWEEN}
        padding={10}
        extraStyle={{ margin: 0 }}
      >
        <Box
          textAlign={TextAlignEnum.LEFT}
          flexDirection={FlexDirectionEnum.ROW}
          alignItems={AlignItemsEnum.CENTER}
          justifyContent={JustifyContentEnum.FLEXSTART}
          height={59}
          flex={1}
          display="flex"
        >
          <Box className={classes.imageWrapper}>
            {mode === CurrentPublicationStateEnum.DRAFT && (
              <Text
                align={TextAlignEnum.LEFT}
                color={TextColorEnum.TEXTPRIMARY}
                variant={TextVariantEnum.BODY2}
                className={classes.draftTag}
              >
                {t("management.draftTag")}
              </Text>
            )}
            {image ? (
              <Image height={59} width={59} className={classes.image} src={image} loading="eager" />
            ) : (
              <MediaProfileImage size={59} className={classes.image} mediaName={mediaName} />
            )}
          </Box>

          <Box ml={2.5}>
            <Text
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.TEXTPRIMARY}
              variant={TextVariantEnum.SUBTITLE2}
              className={classes.title}
            >
              {publication.title}
            </Text>

            <Text
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.TEXTPRIMARY}
              variant={TextVariantEnum.BODY2}
              className={classes.category}
            >
              {publication.categoryName}
            </Text>
          </Box>
        </Box>

        <IconButton
          className={classnames(
            classes.iconButton,
            isOpen ? classes.iconButtonActive : classes.iconButtonDisable,
          )}
          onClick={onToggleCollapse}
        >
          <SvgIcon icon="chevron_right" htmlColor={theme.palette.gray.main} fontSize={20} />
        </IconButton>
      </FlexBox>
      <Collapse in={isOpen}>
        <MetaDataField
          label={t("management.sentIn")}
          value={format(publication.sentAt as Date, "dd/MM/yyyy HH:mm:ss")}
          wrapperStyle={classes.label}
        />

        <MetaDataField
          className={classes.description}
          label={t("management.shotDescription")}
          value={publication.description}
          wrapperStyle={classes.label}
        />

        {publication.files?.length ? (
          <>
            <Box
              display="flex"
              flexDirection={FlexDirectionEnum.ROW}
              alignItems={AlignItemsEnum.CENTER}
              justifyContent={JustifyContentEnum.SPACEBETWEEN}
              paddingX={1}
              paddingY="3px"
            >
              <Text
                paragraph={false}
                align={TextAlignEnum.LEFT}
                color={TextColorEnum.TEXTPRIMARY}
                variant={TextVariantEnum.BODY2}
              >
                {t("management.uploadFileList")}
              </Text>

              <Text
                paragraph={false}
                align={TextAlignEnum.LEFT}
                className={classes.textFieldValue}
                component="span"
                variant={TextVariantEnum.BODY2}
              >
                {uploadedCount}/{publication.files.length}
              </Text>
            </Box>

            <Divider marginTop={6} />

            {publication.files.map((file) => (
              <UploadListItemComponent
                key={file.localId}
                file={file}
                publicationId={publication.id as number}
                onConfirmeDelete={() => onConfirmeDelete(publication.id as number, file.localId)}
              />
            ))}
          </>
        ) : (
          <></>
        )}
      </Collapse>
    </Box>
  );
};

const useClasses = makeStyles((theme) => ({
  wrapper: {
    backgroundColor: theme.palette.gray.light,
    borderColor: theme.palette.grey[200],
    borderWidth: 2,
    borderRadius: 4,
  },
  iconButton: {
    transition: "all 0.4s ease-in",
  },

  iconButtonActive: {
    transform: "rotate(90deg)",
  },

  iconButtonDisable: {
    transform: "rotate(0deg)",
  },

  imageWrapper: {
    height: 59,
    width: 59,
    borderRadius: 4,
    border: `1px solid ${theme.palette.primary.main}`,
    position: "relative",
  },
  image: {
    height: 59,
    width: 59,
    borderRadius: 4,
  },
  title: {
    fontSize: 14,
    color: "#343A40",
  },
  category: {
    fontSize: 10,
    color: theme.palette.gray.main,
  },
  draftTag: {
    position: "absolute",
    left: 0,
    right: 0,
    borderRadius: 4,
    backgroundColor: "rgba(52, 58, 64, 0.7)",
    color: theme.palette.common.white,
    zIndex: 9999,
    fontSize: 10,
    lineHeight: "16px",
    textAlign: "center",
    top: "calc(40px / 2)",
    textTransform: "uppercase",
  },

  description: {
    whiteSpace: "normal",
    overflow: "visible",
  },

  label: {
    padding: "3px 10px",
  },

  textFieldValue: {
    color: theme.palette.gray.main,
    textTransform: "capitalize",
  },
}));

export default PublicationItem;
