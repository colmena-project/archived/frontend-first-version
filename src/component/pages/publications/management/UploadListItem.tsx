import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Box, makeStyles, useTheme } from "@material-ui/core";
import Text from "@/components/ui/Text";
import {
  TextAlignEnum,
  TextColorEnum,
  TextVariantEnum,
  AlignItemsEnum,
  JustifyContentEnum,
  FlexDirectionEnum,
} from "@/enums/*";
import ActionConfirm from "@/components/ui/ActionConfirm";
import { getPublication } from "@/store/idb/models/publications";
import { NewPublicationFile } from "@/interfaces/index";
import { toast } from "@/utils/notifications";
import FlexBox from "@/components/ui/FlexBox";
import SvgIconAux from "@/components/ui/SvgIcon";
import { formatBytes, isAudioFile, isImageFile } from "@/utils/utils";
import UploadStatusComponent from "./UploadStatus";
import { useTranslation } from "next-i18next";

const UploadListItemComponent: React.FC<{
  file: NewPublicationFile;
  publicationId: number;
  onConfirmeDelete: () => void;
}> = ({ file, publicationId, onConfirmeDelete }) => {
  const [fileUpdated, setFileUpdated] = useState(file);
  const [modalConfirmOpen, setModalConfirmeOpen] = useState(false);

  const theme = useTheme();
  const { t } = useTranslation("publications");
  const classes = useClasses();

  const fetchPublication = useCallback(async () => {
    try {
      const data = await getPublication(publicationId);

      const fileRecovered: NewPublicationFile = data.files.find(
        (f: NewPublicationFile) => f.localId === file.localId,
      );

      setFileUpdated(fileRecovered);
    } catch (err) {
      toast(err.message, "error");
    }
  }, [publicationId, file]);

  useEffect(() => {
    fetchPublication();

    const signer = setInterval(fetchPublication, 6000);

    return () => clearInterval(signer);
  }, [fetchPublication]);

  const fileIcon = useMemo(() => {
    if (isImageFile(file.media.type)) {
      return "art_gallery";
    }

    if (isAudioFile(file.media.type)) {
      return "audio_file";
    }

    return "document";
  }, [file]);

  const handleModalConfirm = () => {
    setModalConfirmeOpen((prev) => !prev);
  };

  return (
    <FlexBox
      flexDirection={FlexDirectionEnum.ROW}
      alignItems={AlignItemsEnum.CENTER}
      justifyContent={JustifyContentEnum.SPACEBETWEEN}
      padding={10}
      extraStyle={{ margin: 0 }}
    >
      <Box
        display="flex"
        alignItems={AlignItemsEnum.CENTER}
        justifyContent={JustifyContentEnum.FLEXSTART}
      >
        <SvgIconAux icon={fileIcon} fontSize={30} htmlColor={theme.palette.gray.main} />

        <Box ml={1.5}>
          <Text
            align={TextAlignEnum.LEFT}
            color={TextColorEnum.TEXTPRIMARY}
            variant={TextVariantEnum.SUBTITLE2}
            className={classes.title}
          >
            {file.title}
          </Text>

          <Text
            align={TextAlignEnum.LEFT}
            color={TextColorEnum.TEXTPRIMARY}
            variant={TextVariantEnum.BODY2}
            className={classes.size}
          >
            {formatBytes(file.extentSize as number, 2)}
          </Text>
        </Box>
      </Box>

      <ActionConfirm
        data-testid="delete-file-confirm"
        onOk={() => {
          handleModalConfirm();
          onConfirmeDelete();
        }}
        open={modalConfirmOpen}
        onClose={handleModalConfirm}
        isLoading={false}
        showMessage={false}
        title={t("management.confirmeQuestion")}
      />

      <UploadStatusComponent file={fileUpdated} onClickRemove={handleModalConfirm} />
    </FlexBox>
  );
};

const useClasses = makeStyles((theme) => ({
  title: {
    fontSize: 14,
    color: "#343A40",
  },
  size: {
    fontSize: 10,
    color: theme.palette.gray.main,
  },
  loading: {
    height: 30,
    width: 30,
  },
}));

export default UploadListItemComponent;
