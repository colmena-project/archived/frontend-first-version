import React from "react";
import { useTranslation } from "next-i18next";
import ActionConfirm from "@/components/ui/ActionConfirm";

type DeletePublicationModalProps = {
  open: boolean;
  closeModal: () => void;
  onConfirm: () => void;
};

function DeletePublicationModal({ open, closeModal, onConfirm }: DeletePublicationModalProps) {
  const { t } = useTranslation("publications");

  return (
    <ActionConfirm
      onOk={onConfirm}
      onClose={closeModal}
      isLoading={false}
      title={t("management.deleteModal.title")}
      open={open}
    />
  );
}

export default DeletePublicationModal;
