/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { listFile } from "@/services/webdav/files";
import { PropsUserSelector } from "@/types/index";
import { useSelector } from "react-redux";
import { ConfigFilesNCEnum } from "@/enums/*";
import Loading from "@/components/ui/Loading";
import { arrayBufferToBlob, blobToDataURL } from "blob-util";
import { create as createMedia, findByName } from "@/store/idb/models/media";
import Image from "next/image";
import { Box, makeStyles } from "@material-ui/core";
import classNames from "classnames";
import { toast } from "@/utils/notifications";

type MediaProfileImageProps = {
  size: number;
  mediaName: string;
  className?: string;
};

const MediaProfileImage: React.FC<MediaProfileImageProps> = ({ mediaName, size, className }) => {
  const [file, setFile] = useState<string | undefined>(undefined);
  const [loading, setLoading] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const classes = useClasses();

  const init = async () => {
    try {
      setLoading(true);
      const media = await findByName(mediaName);
      let blob: Blob;
      let file: string | null = null;
      if (!media) {
        const blobRes: any = await listFile(
          userRdx.user.id,
          `${mediaName}/${ConfigFilesNCEnum.MEDIA_PROFILE_AVATAR}`,
        );
        blob = await arrayBufferToBlob(blobRes);
        await createMedia({ name: mediaName, image: blobRes });
      } else {
        blob = await arrayBufferToBlob(media.image);
      }
      file = await blobToDataURL(blob);
      setFile(file);
    } catch (e) {
      toast(e.message, "error");
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    init();
  }, []);

  if (loading) return <Loading />;

  if (!file) {
    return <Box className={classNames(classes.empty, className)} />;
  }

  return (
    <Image
      alt={`Media profile ${mediaName}`}
      src={file as string}
      className={className}
      height={size}
      width={size}
    />
  );
};

const useClasses = makeStyles((theme) => ({
  empty: {
    backgroundColor: theme.palette.gray.contrastText,
  },
}));

export default MediaProfileImage;
