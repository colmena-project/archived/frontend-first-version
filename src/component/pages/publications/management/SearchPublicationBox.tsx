import { Box } from "@material-ui/core";
import React from "react";
import SearchInput from "@/components/ui/SearchInput";

const SearchPublicationBox: React.FC<{
  onChangeText: (keyword: string) => void;
  onClose: () => void;
  show: boolean;
  placeholder: string;
}> = ({ show, onChangeText, onClose, placeholder }) => (
  <Box
    data-testid="search-publication-box"
    marginBottom={2}
    width="100%"
    display={show ? "block" : "none"}
  >
    <SearchInput
      search={onChangeText}
      cancel={onClose}
      placeholder={placeholder}
      searchIntervalMs={700}
      focus
    />
  </Box>
);

export default SearchPublicationBox;
