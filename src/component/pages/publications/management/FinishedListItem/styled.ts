import OutlinedButton from "@/components/ui/Button/OutlinedButton";
import Text from "@/components/ui/Text";
import {
  Avatar,
  Box,
  Button,
  IconButton,
  ListItem,
  MenuItem,
  styled,
  Theme,
} from "@material-ui/core";
import MetaDataField from "../../files/MetadataField";

export const ListItemWrapper = styled(ListItem)<Theme, any>(({ theme, isOpen }) => ({
  backgroundColor: isOpen ? theme.palette.gray.light : "transparent",
  borderColor: theme.palette.grey[200],
  borderWidth: 2,
  borderRadius: 4,
  marginTop: 10,
  display: "block !important",
}));

export const Header = styled(Box)({
  padding: 10,
  display: "flex",
  alignItems: "center",
  position: "relative",
  overflowX: "hidden",
});

export const ImageWrapper = styled(Box)({
  minHeight: 50,
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around",
  position: "relative",
});

export const DraftTag = styled(Text)(({ theme }) => ({
  position: "absolute",
  left: 0,
  right: 0,
  borderRadius: 4,
  backgroundColor: "rgba(52, 58, 64, 0.7)",
  color: theme.palette.common.white,
  zIndex: 99,
  fontSize: 10,
  lineHeight: "16px",
  textAlign: "center",
  top: "calc(40px / 2)",
  textTransform: "uppercase",
}));

export const PublicationThumb = styled(Avatar)(({ theme }) => ({
  backgroundColor: theme.palette.gray.light,
  width: 60,
  height: 60,
  border: `1px solid ${theme.palette.primary.main}`,
}));

export const Title = styled(Text)({
  fontSize: 14,
  color: "#343A40",
  display: "-webkit-box",
  WebkitLineClamp: 3,
  lineClamp: 3,
  WebkitBoxOrient: "vertical",
  overflow: "hidden",
  lineHeight: "1.2",
  overflowWrap: "break-word",
  wordBreak: "break-all",
});

export const Category = styled(Text)(({ theme }) => ({
  fontSize: 10,
  color: theme.palette.gray.main,
}));

export const PeddingApproval = styled(Text)(({ theme }) => ({
  fontSize: 10,
  color: theme.palette.warning.main,
}));

export const ApprovalDenied = styled(Text)(({ theme }) => ({
  fontSize: 10,
  color: theme.palette.error.main,
}));

export const ChevronButton = styled(IconButton)<Theme, any>(({ isOpen }) => ({
  transition: "all 0.4s ease-in",
  transform: isOpen ? "rotate(90deg)" : "rotate(0deg)",
}));

export const DataField = styled(MetaDataField)({
  padding: "3px 10px",
  "& span": {
    whiteSpace: "normal",
    overflow: "visible",
  },
});

export const DataFieldReasson = styled(MetaDataField)(({ theme }) => ({
  padding: "3px 10px",
  color: theme.palette.error.main,

  "& span": {
    whiteSpace: "normal",
    overflow: "visible",
    color: theme.palette.error.main,
  },
}));

export const ActionsWrapper = styled(Box)({
  padding: 10,
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
});

export const ActionButton = styled(Button)(({ theme }) => ({
  color: theme.palette.warning.main,
  backgroundColor: "transparent",
  boxShadow: "none",
  textTransform: "none",
  borderColor: theme.palette.warning.main,
  textAlign: "left",
  borderRadius: 5,
  padding: "3px 5px !important",
}));

export const MenuDropdownItem = styled(MenuItem)<Theme, any>(({ theme }) => ({
  color: theme.palette.gray.main,
  fontSize: 10,
  minHeight: 30,
  paddingLeft: 10,
  paddingRight: 10,
  "& .MuiListItemIcon-root": {
    minWidth: "auto",
    marginRight: 2,
  },
}));

export const RemoveButton = styled(OutlinedButton)(({ theme }) => ({
  borderRadius: 5,
  padding: "3px 5px !important",
  color: theme.palette.error.main,
}));
