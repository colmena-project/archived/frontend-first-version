import React, { useMemo, useState } from "react";
import { Box, Collapse, useTheme, Menu, Checkbox } from "@material-ui/core";

import {
  TextAlignEnum,
  TextColorEnum,
  TextVariantEnum,
  AlignItemsEnum,
  JustifyContentEnum,
  FlexDirectionEnum,
  ButtonSizeEnum,
  ButtonVariantEnum,
  RoleUserEnum,
} from "@/enums/*";

import SvgIcon from "@/components/ui/SvgIcon";

import { useTranslation } from "next-i18next";
import { PublicationQueryInterface } from "@/interfaces/cms";
import { formatDateToLanguage, getUrlStrapiImage } from "@/utils/utils";
import { parseISO } from "date-fns";
import { UserInfoInterface } from "@/interfaces/index";
import ContextMenuItem from "@/components/ui/ContextMenuItem";

import Clickable from "@/components/ui/Clickable";
import Divider from "@/components/ui/Divider";
import {
  ActionButton,
  ActionsWrapper,
  ApprovalDenied,
  Category,
  ChevronButton,
  DataField,
  DataFieldReasson,
  DraftTag,
  Header,
  ImageWrapper,
  ListItemWrapper,
  MenuDropdownItem,
  PeddingApproval,
  PublicationThumb,
  RemoveButton,
  Title,
} from "./styled";
import { useRouter } from "next/router";
import ActionConfirm from "@/components/ui/ActionConfirm";

const FinishedListItem: React.FC<{
  publication: PublicationQueryInterface;
  user: UserInfoInterface;
  onApprove: (publication: PublicationQueryInterface) => void;
  onDisapprove: (publication: PublicationQueryInterface) => void;
  onUnpublish: (publicationId: string) => void;
  onClickRemove: () => void;
  onSelectItem: ($e: React.ChangeEvent<HTMLInputElement>) => void;
}> = ({ publication, user, onApprove, onDisapprove, onUnpublish, onClickRemove, onSelectItem }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [openInfoModal, setOpenInfoModal] = useState(false);

  const theme = useTheme();
  const { t } = useTranslation("publications");

  const onToggleCollapse = () => setIsOpen((prevState) => !prevState);
  const router = useRouter();

  const imageURL = getUrlStrapiImage(publication.attributes.relation, "thumbnail");
  const isAdmin = user.role === RoleUserEnum.ADMIN;

  const createdAtFormated = useMemo(() => {
    const { createdAt } = publication.attributes;

    if (!createdAt) {
      return null;
    }

    return formatDateToLanguage(parseISO(createdAt), user.language);
  }, [publication.attributes, user]);

  const isPendingApproval = useMemo(() => {
    if (publication.attributes.disapprovedAt) {
      return false;
    }

    if (publication.attributes.publishedAt) {
      return false;
    }

    return isAdmin;
  }, [publication, isAdmin]);

  const isDisapproval = useMemo(() => Boolean(publication.attributes.disapprovedAt), [publication]);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <ListItemWrapper isOpen={isOpen}>
      <Header>
        <ImageWrapper>
          {!publication.attributes.publishedAt && (
            <DraftTag
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.TEXTPRIMARY}
              variant={TextVariantEnum.BODY2}
            >
              {t("management.draftTag")}
            </DraftTag>
          )}

          <Clickable handleClick={onToggleCollapse}>
            <PublicationThumb src={imageURL || undefined} variant="rounded">
              <SvgIcon fontSize={20} icon="newspaper" />
            </PublicationThumb>
          </Clickable>
        </ImageWrapper>

        <Box
          textAlign={TextAlignEnum.LEFT}
          flexDirection={FlexDirectionEnum.ROW}
          alignItems={AlignItemsEnum.CENTER}
          justifyContent={JustifyContentEnum.SPACEBETWEEN}
          minHeight={60}
          flex={1}
          display="flex"
          position="relative"
          overflow="hidden"
          maxWidth="100%"
        >
          <Box ml={2.5} flex={1} position="relative" maxWidth="100%">
            <Title
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.TEXTPRIMARY}
              variant={TextVariantEnum.SUBTITLE2}
              onClick={onToggleCollapse}
            >
              {publication.attributes.title}
            </Title>

            <Category
              align={TextAlignEnum.LEFT}
              color={TextColorEnum.TEXTPRIMARY}
              variant={TextVariantEnum.BODY2}
            >
              {publication.attributes.subject.data.attributes.name} | {createdAtFormated}
            </Category>

            {isPendingApproval && (
              <PeddingApproval
                align={TextAlignEnum.LEFT}
                color={TextColorEnum.TEXTPRIMARY}
                variant={TextVariantEnum.BODY2}
              >
                {t("management.pendingApproval")}
              </PeddingApproval>
            )}

            {isDisapproval && (
              <ApprovalDenied
                align={TextAlignEnum.LEFT}
                color={TextColorEnum.TEXTPRIMARY}
                variant={TextVariantEnum.BODY2}
              >
                {t("management.denied")}
              </ApprovalDenied>
            )}
          </Box>

          {isPendingApproval && isAdmin && (
            <Checkbox onChange={onSelectItem} value={publication.id} />
          )}

          <ChevronButton onClick={onToggleCollapse} isOpen={isOpen}>
            <SvgIcon icon="chevron_right" htmlColor={theme.palette.gray.main} fontSize={20} />
          </ChevronButton>
        </Box>
      </Header>

      <Collapse in={isOpen}>
        {isAdmin && (
          <DataField label={t("management.author")} value={publication.attributes.creator} />
        )}

        <DataField
          label={t("management.shotDescription")}
          value={publication.attributes.description}
        />

        <Divider marginTop={7} />

        {isDisapproval && Boolean(publication.attributes.disapprovalReason) && (
          <DataFieldReasson
            label={t("management.reasonLabel")}
            value={publication.attributes.disapprovalReason as string}
            labelAs="strong"
          />
        )}

        <ActionsWrapper mt={1}>
          <ActionButton
            variant={ButtonVariantEnum.OUTLINED}
            onClick={handleClick}
            endIcon={
              <SvgIcon
                icon="arrow_down"
                htmlColor={theme.palette.warning.main}
                fontSize={ButtonSizeEnum.SMALL}
              />
            }
            size="small"
          >
            {t("management.actions.actionsLabel")}
          </ActionButton>

          <Menu
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "center",
              horizontal: "center",
            }}
          >
            <MenuDropdownItem
              onClick={() => {
                handleClose();
                window.open(`/publications/show/${publication.id}`, "_blank");
              }}
            >
              <ContextMenuItem title={t("management.actions.preview")} />
            </MenuDropdownItem>

            <MenuDropdownItem onClick={handleClose}>
              <ContextMenuItem title={t("management.actions.select")} />
            </MenuDropdownItem>

            <MenuDropdownItem
              onClick={() => {
                handleClose();

                if (isAdmin) {
                  router.push(`/publications/edit/${publication.id}`);
                } else {
                  setOpenInfoModal(true);
                }
              }}
            >
              <ContextMenuItem title={t("management.actions.edit")} />
            </MenuDropdownItem>

            {isPendingApproval && (
              <MenuDropdownItem
                onClick={() => {
                  onApprove(publication);
                  handleClose();
                }}
              >
                <ContextMenuItem title={t("management.actions.approve")} />
              </MenuDropdownItem>
            )}

            {isPendingApproval && (
              <MenuDropdownItem
                onClick={() => {
                  onDisapprove(publication);
                  handleClose();
                }}
              >
                <ContextMenuItem title={t("management.actions.disapprove")} />
              </MenuDropdownItem>
            )}

            {Boolean(publication.attributes.publishedAt) && (
              <MenuDropdownItem
                onClick={() => {
                  onUnpublish(publication.id);
                  handleClose();
                }}
              >
                <ContextMenuItem title={t("management.actions.unpublish")} />
              </MenuDropdownItem>
            )}
          </Menu>

          <RemoveButton
            title={t("management.actions.remove")}
            data-testid="actions-button"
            iconColor={theme.palette.error.main}
            color={theme.palette.error.main}
            handleClick={onClickRemove}
            size={ButtonSizeEnum.SMALL}
            startIcon="trash"
          />
        </ActionsWrapper>
      </Collapse>

      <ActionConfirm
        onOk={() => {
          setOpenInfoModal(false);
        }}
        open={openInfoModal}
        onClose={() => {
          setOpenInfoModal(false);
        }}
        message={t("management.modalInfoEdit.message")}
        showMessage
        title={t("management.modalInfoEdit.title")}
        showActionsButtons={false}
      />
    </ListItemWrapper>
  );
};

export default FinishedListItem;
