import React from "react";
import { Formik, Form, Field, FieldProps } from "formik";
import Modal from "@/components/ui/Modal";
import Button from "@/components/ui/Button";
import TextField from "@material-ui/core/TextField";
import Divider from "@/components/ui/Divider";
import { useTranslation } from "next-i18next";
import { Grid, makeStyles, useTheme } from "@material-ui/core";
import { ButtonVariantEnum } from "@/enums/*";

const INITIAL_VALUES = {
  reason: "",
};

type DisapprovalReasonModalProps = {
  open: boolean;
  closeModal: () => void;
  onConfirm: (values: typeof INITIAL_VALUES) => void;
};

function DisapprovalReasonModal({ open, closeModal, onConfirm }: DisapprovalReasonModalProps) {
  const { t } = useTranslation("publications");
  const { t: c } = useTranslation("common");

  const classes = useClasses();
  const theme = useTheme();

  return (
    <Modal title={t("management.disapproveModal.title")} handleClose={closeModal} open={open}>
      <Formik initialValues={INITIAL_VALUES} onSubmit={onConfirm}>
        <Form>
          <Field name="reason" InputProps={{ notched: true }}>
            {({ field }: FieldProps) => {
              const remaining = 1000 - (field?.value?.length ?? 0);

              return (
                <TextField
                  label=""
                  variant="outlined"
                  inputProps={{ maxLength: 1000 }}
                  style={{
                    width: "100%",
                  }}
                  {...field}
                  multiline
                  rows={5}
                  helperText={`${remaining} ${
                    remaining > 1
                      ? t("createPublication.form.description.helper.plural")
                      : t("createPublication.form.description.helper.singular")
                  }`}
                  FormHelperTextProps={{ className: classes.helperText }}
                />
              );
            }}
          </Field>
          <Divider marginTop={20} />
          <Grid container justifyContent="space-between">
            <Button
              title={c("form.cancelButton")}
              type="button"
              variant={ButtonVariantEnum.OUTLINED}
              handleClick={closeModal}
              borderColor={theme.palette.warning.main}
            />

            <Button title={c("form.submitSaveTitle")} type="submit" />
          </Grid>
          <Divider marginTop={10} />
        </Form>
      </Formik>
    </Modal>
  );
}

const useClasses = makeStyles(() => ({
  form: {
    "& .MuiTextField-root": {
      width: "100%",
    },
  },
  submit: {
    marginLeft: 10,
  },
  dialogContentStyle: {
    borderTop: "none",
    marginTop: 0,
  },
  helperText: {
    textAlign: "right",
    marginRight: 0,
  },
}));

export default DisapprovalReasonModal;
