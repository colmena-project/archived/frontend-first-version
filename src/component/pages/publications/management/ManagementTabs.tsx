import React from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useTranslation } from "next-i18next";
import { makeStyles, useTheme, withStyles } from "@material-ui/core/styles";
import SvgIconAux from "@/components/ui/SvgIcon";

function a11yProps(index: number) {
  return {
    id: `profile-tab-${index}`,
    "aria-controls": `profile-tab-tabpanel-${index}`,
  };
}

const useTabStyles = makeStyles((theme) => ({
  root: {
    textTransform: "none",
    color: theme.palette.gray.main,
    opacity: 1,
    fontSize: "1rem",
    "&:focus": {
      opacity: 1,
      color: theme.palette.primary.main,
    },
  },
  wrapper: {
    display: "flex",
    flexDirection: "row",
    gap: 10,
  },
  selected: { color: theme.palette.primary.main },
  labelIcon: {
    "& .MuiTab-wrapper > *:first-child": {
      margin: 0,
    },
  },
}));

interface StyledTabsProps {
  value: number;
  onChange: (event: React.ChangeEvent, newValue: number) => void;
  icon?: string | React.ReactElement<any, string | React.JSXElementConstructor<any>> | undefined;
  variant?: "scrollable" | "standard" | "fullWidth" | undefined;
}

const StyledTabs = withStyles((theme) => ({
  indicator: {
    height: 2,
    backgroundColor: theme.palette.primary.main,
  },
  root: {
    backgroundColor: theme.palette.background.default,
  },
}))((props: StyledTabsProps) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const ManagementTabs: React.FC<{ activeTab: number; onChangeTab: (newValue: number) => void }> = ({
  activeTab,
  onChangeTab,
}) => {
  const { t } = useTranslation("publications");
  const classesTab = useTabStyles();
  const theme = useTheme();
  const onChangeTabs = (_: React.ChangeEvent, newValue: number) => {
    onChangeTab(newValue);
  };

  return (
    <StyledTabs variant="fullWidth" value={activeTab} onChange={onChangeTabs}>
      <Tab
        icon={
          <SvgIconAux
            fontSize={20}
            icon="pending_list"
            htmlColor={activeTab === 0 ? theme.palette.primary.main : theme.palette.gray.main}
          />
        }
        label={t("management.pending")}
        {...a11yProps(0)}
        classes={classesTab}
      />
      <Tab
        icon={
          <SvgIconAux
            fontSize={20}
            icon="finished_list"
            htmlColor={activeTab === 1 ? theme.palette.primary.main : theme.palette.gray.main}
          />
        }
        label={t("management.finished")}
        {...a11yProps(1)}
        classes={classesTab}
      />
    </StyledTabs>
  );
};

export default ManagementTabs;
