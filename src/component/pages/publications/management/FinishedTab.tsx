import useAllPublicationsByRole from "@/hooks/cms/useAllPublicationsByRole";
import { UserInfoInterface } from "@/interfaces/index";
import { Box, List, makeStyles, useTheme } from "@material-ui/core";
import React, { useCallback, useEffect, useState } from "react";
import Skeleton from "@material-ui/lab/Skeleton";
import { useMutation } from "@apollo/client";
import OutlinedButton from "@/components/ui/Button/OutlinedButton";
import { InView } from "react-intersection-observer";

import {
  APPROVE_PUBLICATION,
  DISAPPROVE_PUBLICATION,
  DELETE_PUBLICATION,
  UNPUBLISH_PUBLICATION,
  PublicationFiltersProps,
} from "@/services/cms/publications";
import Loading from "@/components/ui/Loading";
import { useTranslation } from "next-i18next";
import { toast } from "@/utils/notifications";
import SimpleBackdrop from "@/components/ui/Backdrop";
import { PublicationQueryInterface } from "@/interfaces/cms";
import { ButtonSizeEnum, PublicationFilterStatusEnum } from "@/enums/*";

import NoPublication from "../../publication/NoPublication";
import FinishedListItem from "./FinishedListItem";
import DisapprovalReasonModal from "./DisapprovalReasonModal";
import DeletePublicationModal from "./DeletePublicationModal";
import PublicationsManagementFiltersDrawer from "./PublicationsManagementFiltersDrawer";
import SearchPublicationBox from "./SearchPublicationBox";

const FinishedTab: React.FC<{
  user: UserInfoInterface;
  openSearch: boolean;
  onCloseSearch: () => void;
  openFilters: boolean;
  onCloseFilters: () => void;
}> = ({ user, openSearch, onCloseSearch, openFilters, onCloseFilters }) => {
  const [filtering, setFiltering] = useState(false);
  const [currentItem, setCurrentItem] = useState<null | PublicationQueryInterface>(null);
  const [filters, setFilters] = useState<PublicationFiltersProps>({
    order: "createdAt:desc",
    category: undefined,
  });
  const [currentItemToDelete, setCurrentItemToDelete] = useState<null | PublicationQueryInterface>(
    null,
  );
  const [loadingAll, setLoadingAll] = useState(false);
  const { publications, loading, refetch, pagination, fetchMore } = useAllPublicationsByRole(user);
  const [selecteds, setSelecteds] = useState<PublicationQueryInterface["id"][]>([]);
  const [findKeywords, setFindKeywords] = useState<string>("");
  const [fetchingMore, setFetchingMore] = useState(false);

  const [destroyPublication, { loading: loadingPublicationAction }] =
    useMutation(DELETE_PUBLICATION);

  const { t } = useTranslation("publications");

  const theme = useTheme();

  const [approvePublication, { loading: loadingMutation }] = useMutation(APPROVE_PUBLICATION, {
    onError() {
      toast(t("management.actionGeneralErrorMessage"), "error");
    },
    onCompleted() {
      refetch();
    },
  });

  const [disapprovePublication, { loading: disaproving }] = useMutation(DISAPPROVE_PUBLICATION, {
    onError() {
      toast(t("management.actionGeneralErrorMessage"), "error");
    },
    onCompleted() {
      refetch();
    },
  });

  const [unpublish, { loading: unpublishing }] = useMutation(UNPUBLISH_PUBLICATION);

  const classes = useClasses();

  const handleClickApprove = useCallback(
    (publication: PublicationQueryInterface) => {
      const publishedAt = new Date().toISOString();

      approvePublication({
        variables: {
          id: publication.id,
          publishedAt,
        },
      });
    },
    [approvePublication],
  );

  const handleOnClickUnpublish = useCallback(
    (publicationId: string) => {
      unpublish({
        variables: {
          id: publicationId,
        },
        onCompleted: () => {
          refetch();
        },
      });
    },
    [unpublish, refetch],
  );

  const handleConfirmDisapprove = useCallback(
    (publication: PublicationQueryInterface, reason) => {
      const disapprovedAt = new Date().toISOString();

      setCurrentItem(null);

      disapprovePublication({
        variables: {
          id: publication.id,
          disapprovedAt,
          disapprovalReason: reason,
        },
      });
    },
    [disapprovePublication],
  );

  const handleSelectItem = useCallback(($e: React.ChangeEvent<HTMLInputElement>) => {
    const { value, checked } = $e.target;

    setSelecteds((old) => {
      if (checked) {
        return [...old, value];
      }

      return old.filter((selected) => selected !== value);
    });
  }, []);

  const handleConfirmDelete = useCallback(() => {
    setCurrentItemToDelete(null);

    destroyPublication({
      variables: {
        id: currentItemToDelete?.id as string,
      },
      onCompleted: () => {
        refetch();
      },
    });
  }, [destroyPublication, currentItemToDelete, refetch]);

  const handleFetchMore = useCallback(
    async (inView: boolean) => {
      if (
        !loading &&
        publications.length &&
        inView &&
        pagination?.page &&
        pagination.page < pagination.pageCount
      ) {
        setFetchingMore(true);

        await fetchMore({
          variables: {
            page: pagination.page + 1,
          },
          updateQuery: (prev, { fetchMoreResult }) => {
            if (!fetchMoreResult) return prev;

            return {
              publications: {
                data: [...prev.publications.data, ...fetchMoreResult.publications.data],
                meta: fetchMoreResult.publications.meta,
              },
            };
          },
        });

        setFetchingMore(false);
      }
    },
    [loading, publications, pagination, fetchMore],
  );

  useEffect(() => {
    if (findKeywords.length < 3) {
      return;
    }

    setFiltering(true);

    refetch({
      keyword: findKeywords,
    }).finally(() => setFiltering(false));
  }, [refetch, findKeywords]);

  useEffect(() => {
    if (
      filters.status === PublicationFilterStatusEnum.PENDING ||
      filters.status === PublicationFilterStatusEnum.DRAFT
    ) {
      setFiltering(true);

      refetch({
        order: filters.order,
        category: filters.category ? filters.category : undefined,
        published: true,
        disapproved: true,
      }).finally(() => setFiltering(false));

      return;
    }

    if (filters.status === PublicationFilterStatusEnum.PUBLISHED) {
      setFiltering(true);

      refetch({
        order: filters.order,
        category: filters.category ? filters.category : undefined,
        published: false,
        disapproved: true,
      }).finally(() => setFiltering(false));

      return;
    }

    if (filters.status === PublicationFilterStatusEnum.DENIED) {
      setFiltering(true);

      refetch({
        order: filters.order,
        category: filters.category ? filters.category : undefined,
        published: true,
        disapproved: false,
      }).finally(() => setFiltering(false));

      return;
    }

    setFiltering(true);

    refetch({
      order: filters.order,
      category: filters.category ? filters.category : undefined,
      published: undefined,
      disapproved: undefined,
    }).finally(() => setFiltering(false));
  }, [refetch, filters]);

  const handleSearch = (keywords: string) => {
    setFindKeywords(keywords);
  };

  const handleApproveAll = async () => {
    setLoadingAll(true);

    const promises = selecteds.map((publicationId) => {
      const publishedAt = new Date().toISOString();

      return approvePublication({
        variables: {
          id: publicationId,
          publishedAt,
        },
      });
    });

    await Promise.all(promises);

    setSelecteds([]);

    setLoadingAll(false);
  };

  const handleFilter = (filters: PublicationFiltersProps) => {
    setFilters(filters);
  };

  return (
    <Box>
      <SearchPublicationBox
        onChangeText={handleSearch}
        onClose={() => {
          setFindKeywords("");
          setFiltering(true);
          refetch({
            keyword: "",
          }).finally(() => setFiltering(false));
          onCloseSearch();
        }}
        placeholder={t("management.searchPublicationLabel")}
        show={openSearch}
      />

      {filtering && !loading && (
        <Box padding={1}>
          <Loading />
        </Box>
      )}

      {loading && (
        <>
          <Skeleton width="100%" height={80} className={classes.skeleton} variant="rect" />
          <Skeleton width="100%" height={80} className={classes.skeleton} variant="rect" />
          <Skeleton width="100%" height={80} className={classes.skeleton} variant="rect" />
          <Skeleton width="100%" height={80} className={classes.skeleton} variant="rect" />
          <Skeleton width="100%" height={80} className={classes.skeleton} variant="rect" />
        </>
      )}

      <SimpleBackdrop
        open={
          loadingMutation || disaproving || loadingPublicationAction || unpublishing || loadingAll
        }
      />

      {currentItem && (
        <DisapprovalReasonModal
          open={Boolean(currentItem)}
          onConfirm={({ reason }) => handleConfirmDisapprove(currentItem, reason)}
          closeModal={() => setCurrentItem(null)}
        />
      )}

      <DeletePublicationModal
        open={Boolean(currentItemToDelete)}
        onConfirm={handleConfirmDelete}
        closeModal={() => setCurrentItemToDelete(null)}
      />

      <Box
        width="100%"
        data-testid="no-publication"
        display={!loading && !publications.length ? "block" : "none"}
      >
        <NoPublication />
      </Box>

      <List>
        {!filtering &&
          publications.map((publication) => (
            <FinishedListItem
              key={publication.id}
              publication={publication}
              user={user}
              onApprove={handleClickApprove}
              onUnpublish={handleOnClickUnpublish}
              onDisapprove={setCurrentItem}
              onClickRemove={() => setCurrentItemToDelete(publication)}
              onSelectItem={handleSelectItem}
            />
          ))}
      </List>

      {publications && !filtering && <InView onChange={handleFetchMore} />}

      {fetchingMore && (
        <Box padding={1}>
          <Loading />
        </Box>
      )}

      {Boolean(selecteds.length) && (
        <Box
          position="fixed"
          bottom={70}
          width="100%"
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <OutlinedButton
            title={t("management.actions.approveAll")}
            iconColor={theme.palette.variation1.main}
            handleClick={handleApproveAll}
            color={theme.palette.variation1.main}
            className={classes.action}
            size={ButtonSizeEnum.SMALL}
            startIcon="doublecheck"
          />
        </Box>
      )}

      <PublicationsManagementFiltersDrawer
        open={openFilters}
        handleClose={onCloseFilters}
        filterPublications={handleFilter}
        initialFilters={filters}
        language={user.language || "en"}
      />
    </Box>
  );
};

const useClasses = makeStyles((theme) => ({
  skeleton: {
    marginBottom: 14,
    borderRadius: 4,
  },
  action: {
    borderRadius: 5,
    padding: "3px 5px",
    backgroundColor: theme.palette.background.paper,
  },
  searchWrapper: {
    width: "100%",
    marginBottom: 18,
  },
}));

export default FinishedTab;
