import React, { useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  ClickAwayListener,
  Popover,
  Tooltip,
  useTheme,
} from "@material-ui/core";
import { ButtonSizeEnum, CurrentPublicationStateEnum } from "@/enums/*";
import { NewPublicationFile } from "@/interfaces/index";
import SvgIconAux from "@/components/ui/SvgIcon";
import ButtonWithIcon from "@/components/ui/ButtonV2";
import { useTranslation } from "next-i18next";

const UploadStatusComponent: React.FC<{ file: NewPublicationFile; onClickRemove: () => void }> = ({
  file,
  onClickRemove,
}) => {
  const [openErrorMessage, setOpenErrorMessage] = useState(false);
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const { t } = useTranslation("publications");

  const theme = useTheme();

  const onCloseErrorMessage = () => setOpenErrorMessage(false);

  const onOpenErrorMessage = () => setOpenErrorMessage(true);

  const handleClickRemove = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseRemovePopover = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  if (!file) {
    return <SvgIconAux icon="check_outline" fontSize={30} htmlColor={theme.palette.success.main} />;
  }

  return (
    <>
      {file.status === CurrentPublicationStateEnum.IN_PROGRESS ? (
        <CircularProgress style={{ color: theme.palette.primary.main }} size={25} />
      ) : (
        <>
          {file.status === CurrentPublicationStateEnum.ERROR && (
            <ClickAwayListener onClickAway={onCloseErrorMessage}>
              <Box width={30}>
                <Tooltip
                  PopperProps={{
                    disablePortal: true,
                  }}
                  title={file.statusMessage ?? ""}
                  disableFocusListener
                  disableHoverListener
                  disableTouchListener
                  open={openErrorMessage}
                  placement="left"
                  arrow
                  onClose={onCloseErrorMessage}
                >
                  <Button
                    onClick={onOpenErrorMessage}
                    style={{ padding: 0, minWidth: "auto", width: 30, height: 30 }}
                  >
                    <SvgIconAux
                      icon="error_outline"
                      fontSize={30}
                      htmlColor={theme.palette.danger.main}
                    />
                  </Button>
                </Tooltip>
              </Box>
            </ClickAwayListener>
          )}

          {file.status === CurrentPublicationStateEnum.PENDING && (
            <Box>
              <Button
                onClick={handleClickRemove}
                style={{ padding: 0, minWidth: "auto", width: 30, height: 30 }}
              >
                <SvgIconAux
                  icon="peding_outline"
                  fontSize={30}
                  htmlColor={theme.palette.warning.main}
                />
              </Button>
              <Popover
                open={open}
                anchorEl={anchorEl}
                onClose={handleCloseRemovePopover}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                transformOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
              >
                <ButtonWithIcon
                  startIcon="trash"
                  color={theme.palette.danger.main}
                  backgroundColor="transparent"
                  iconColor={theme.palette.danger.main}
                  title={t("management.delete")}
                  size={ButtonSizeEnum.SMALL}
                  handleClick={() => {
                    handleCloseRemovePopover();
                    onClickRemove();
                  }}
                />
              </Popover>
            </Box>
          )}

          {file.status === CurrentPublicationStateEnum.UPLOADED && (
            <SvgIconAux icon="check_outline" fontSize={30} htmlColor={theme.palette.success.main} />
          )}
        </>
      )}
    </>
  );
};

export default UploadStatusComponent;
