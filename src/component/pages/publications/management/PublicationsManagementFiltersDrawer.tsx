import React, { useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import { v4 as uuid } from "uuid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ButtonVariantEnum, PublicationFilterStatusEnum } from "@/enums/*";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useTranslation } from "react-i18next";
import {
  Button,
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { Formik, Form } from "formik";
import Select from "@material-ui/core/Select";
import useCategories from "@/hooks/cms/useCategories";
import { PublicationFiltersProps } from "@/services/cms/publications";

type PublicationsManagementFiltersDrawerProps = {
  open: boolean;
  handleClose: () => void;
  filterPublications: (filters: PublicationFiltersProps) => void;
  initialFilters: PublicationFiltersProps;
  language: string;
};

function PublicationsManagementFiltersDrawer({
  open,
  filterPublications,
  handleClose,
  initialFilters,
  language,
}: PublicationsManagementFiltersDrawerProps) {
  const classes = useStyles();
  const { t } = useTranslation("publications");
  const [expanded, setExpanded] = useState<string | boolean>("filter");

  const { categories, loading: loadingCategories } = useCategories(language);

  const handleChange = (panel: string) => (event: any, newExpanded: string | boolean) => {
    setExpanded(newExpanded ? panel : false);
  };

  const handleSubmit = async (filters: PublicationFiltersProps) => {
    filterPublications(filters);
    handleClose();
  };

  return (
    <Drawer anchor="right" open={open} onClose={handleClose} className={classes.root}>
      <Formik initialValues={initialFilters ?? {}} onSubmit={handleSubmit}>
        {({ values, setFieldValue }: any) => (
          <Form>
            <Accordion expanded={expanded === "filter"} onChange={handleChange("filter")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="filters-content"
                id="filters-header"
              >
                <Typography>{t("management.filters.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <List>
                  <ListItem key={uuid()}>
                    <FormControl variant="outlined">
                      <InputLabel>{t("management.filters.category")}</InputLabel>
                      <Select
                        value={values?.category}
                        onChange={(e) => setFieldValue("category", e.target.value)}
                        label={t("management.filters.category")}
                        className={classes.select}
                        disabled={loadingCategories}
                      >
                        <MenuItem value="" key={uuid()}>
                          <em>{t("management.filters.all")}</em>
                        </MenuItem>
                        {categories &&
                          categories.map((category) => (
                            <MenuItem value={category.id} key={uuid()}>
                              {category.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </ListItem>

                  <ListItem key={uuid()}>
                    <FormControl variant="outlined">
                      <InputLabel>{t("management.filters.status")}</InputLabel>
                      <Select
                        value={values?.status}
                        onChange={(e) => setFieldValue("status", e.target.value)}
                        label={t("management.filters.status")}
                        className={classes.select}
                        disabled={loadingCategories}
                      >
                        <MenuItem value="" key={uuid()}>
                          {t("management.filters.all")}
                        </MenuItem>

                        <MenuItem value={PublicationFilterStatusEnum.PENDING} key={uuid()}>
                          {t("management.pendingApproval")}
                        </MenuItem>

                        <MenuItem value={PublicationFilterStatusEnum.DENIED} key={uuid()}>
                          {t("management.denied")}
                        </MenuItem>

                        <MenuItem value={PublicationFilterStatusEnum.DRAFT} key={uuid()}>
                          {t("management.draftTag")}
                        </MenuItem>

                        <MenuItem value={PublicationFilterStatusEnum.PUBLISHED} key={uuid()}>
                          {t("management.filters.published")}
                        </MenuItem>
                      </Select>
                    </FormControl>
                  </ListItem>
                </List>
              </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === "order"} onChange={handleChange("order")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="order-content"
                id="order-header"
              >
                <Typography>{t("management.sort.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <RadioGroup
                  name="order"
                  value={values?.order}
                  onChange={(event) => {
                    setFieldValue("order", event.currentTarget.value);
                  }}
                >
                  <List>
                    <ListItem button key={uuid()}>
                      <FormControlLabel
                        value="createdAt:desc"
                        control={<Radio />}
                        label={t("management.sort.newest")}
                      />
                    </ListItem>

                    <ListItem button key={uuid()}>
                      <FormControlLabel
                        value="createdAt:asc"
                        control={<Radio />}
                        label={t("management.sort.oldest")}
                      />
                    </ListItem>
                  </List>
                </RadioGroup>
              </AccordionDetails>
            </Accordion>
            <Button
              variant={ButtonVariantEnum.CONTAINED}
              className={classes.filterButton}
              type="submit"
            >
              {t("management.filters.filter")}
            </Button>
          </Form>
        )}
      </Formik>
    </Drawer>
  );
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiAccordionSummary-root": {
        backgroundColor: theme.palette.gray.light,
      },
      "& .MuiAccordion-root": {
        boxShadow: "none",
      },
      "& .MuiListItem-root": {
        padding: 0,
      },
      "& .MuiFormControlLabel-root": {
        width: "100%",
      },
      "& .MuiIconButton-root.Mui-checked": {
        color: theme.palette.variation1.main,
      },
      "& .MuiFormControl-root": {
        marginBottom: theme.spacing(3),
      },
    },
    filterButton: {
      borderRadius: 0,
      backgroundColor: theme.palette.variation2.main,
      color: theme.palette.variation2.contrastText,
      width: "100%",
      boxShadow: "none",
      textTransform: "inherit",
      fontSize: "1rem",
    },
    select: {
      minWidth: 200,
      width: "100%",
    },
  }),
);

export default PublicationsManagementFiltersDrawer;
