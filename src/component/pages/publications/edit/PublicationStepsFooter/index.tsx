import React from "react";
import BackIcon from "@material-ui/icons/ChevronLeft";
import NextIcon from "@material-ui/icons/ChevronRight";

import Text from "@/components/ui/Text";
import FlexBox from "@/components/ui/FlexBox";
import { AlignItemsEnum, FlexDirectionEnum, JustifyContentEnum, TextVariantEnum } from "@/enums/*";
import FooterApp from "@/components/ui/FooterApp";
import { FooterWrapper, Container, FooterItems, BackButton, NextButton } from "./styled";

type PublicationStepsFooterProps = {
  currentStep: number;
  totalStep: number;
  onClickNext?: () => void | Promise<void>;
  onClickBack: () => void | Promise<void>;
  children?: React.ReactNode;
  backLabel: string;
  nextLabel: string;
  loading?: boolean;
  menuOpened: boolean;
};

const PublicationStepsFooter: React.FC<PublicationStepsFooterProps> = ({
  currentStep,
  onClickNext,
  totalStep,
  children,
  onClickBack,
  backLabel,
  nextLabel,
  loading,
}) => (
  <Container>
    <FooterWrapper>
      {children}
      <FooterItems>
        <FlexBox
          alignItems={AlignItemsEnum.CENTER}
          flexDirection={FlexDirectionEnum.ROW}
          justifyContent={JustifyContentEnum.SPACEBETWEEN}
          padding={0}
        >
          <BackButton
            color="default"
            startIcon={<BackIcon />}
            onClick={onClickBack}
            disabled={loading}
          >
            {backLabel}
          </BackButton>

          <Text variant={TextVariantEnum.CAPTION}>
            {currentStep}/{totalStep}
          </Text>

          <NextButton
            color="primary"
            endIcon={<NextIcon />}
            onClick={onClickNext}
            disabled={loading}
            data-testid="next-button"
          >
            {nextLabel}
          </NextButton>
        </FlexBox>
      </FooterItems>
    </FooterWrapper>
    <FooterApp />
  </Container>
);

export default PublicationStepsFooter;
