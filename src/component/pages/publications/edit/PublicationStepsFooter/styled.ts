import { Box, Button, styled } from "@material-ui/core";

export const Container = styled(Box)({
  zIndex: 999,
  alignSelf: "stretch",
  position: "fixed",
  bottom: 0,
  left: 0,
  right: 0,
});

export const FooterWrapper = styled(Box)(({ theme }) => ({
  margin: 0,
  padding: 0,
  left: 0,
  bottom: 0,
  width: "100%",
  fontSize: 12,
  background: theme.palette.common.white,
}));

export const FooterItems = styled(Box)({
  display: "flex",
  flexDirection: "row",
  flexWrap: "nowrap",
  justifyContent: "space-around",
  padding: 10,
  position: "relative",
  margin: 0,
});

export const BackButton = styled(Button)({
  padding: 0,
  margin: 0,
  fontWeight: 600,
});

export const NextButton = styled(Button)({
  padding: 0,
  margin: 0,
  weight: 700,
  color: "#1976D2 !important",
  fontWeight: 600,
});
