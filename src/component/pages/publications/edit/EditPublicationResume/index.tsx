import React, { useMemo } from "react";

import PublicationContent from "@/components/pages/publication/PublicationContent";
import PublicationHeader from "@/components/pages/publication/PublicationHeader";
import PublicationTags from "@/components/pages/publication/PublicationTags";
import FlexBox from "@/components/ui/FlexBox";
import { JustifyContentEnum, TextVariantEnum } from "@/enums/*";
import { MediaInterface, PublicationQueryInterface } from "@/interfaces/cms";
import { EditPublicationFile, NewPublicationFile } from "@/interfaces/index";
import { formatDateToLanguage, getUrlStrapiImage } from "@/utils/utils";
import { parseISO } from "date-fns";
import { useTranslation } from "next-i18next";
import FileMetadataCollapse from "@/components/ui/FileMetadataCollapse";

import { EditPublicationFormValues } from "../EditPublicationForm";

import { RightsText, FilesContainer } from "./styled";

type EditPublicationResumeProps = {
  files: (NewPublicationFile | EditPublicationFile)[];
  publicationPayload: EditPublicationFormValues | null;
  publication: PublicationQueryInterface;
  categoryName: string;
  language: string;
};

const EditPublicationResume: React.FC<EditPublicationResumeProps> = ({
  publicationPayload,
  publication,
  categoryName,
  language,
  files,
}) => {
  const { t } = useTranslation("publications");

  const { creator, publisher, tags, title, description, content, rights } = publication.attributes;

  const publishedAt = useMemo(() => {
    if (!publicationPayload) {
      const date = parseISO(publication.attributes.publishedAt);

      return formatDateToLanguage(date, language);
    }

    return formatDateToLanguage(publicationPayload.publicationDate, language);
  }, [publication, language, publicationPayload]);

  const imageUrl = useMemo(() => {
    if ((publicationPayload?.image as File)?.name) {
      return URL.createObjectURL(publicationPayload?.image as File);
    }

    if (publication.attributes.relation) {
      return getUrlStrapiImage(publication.attributes.relation, "medium");
    }

    return undefined;
  }, [publicationPayload, publication.attributes.relation]);

  return (
    <FlexBox
      justifyContent={JustifyContentEnum.FLEXSTART}
      padding={0}
      extraStyle={{ alignItems: "flex-start", textAlign: "left" }}
      paddingBottom={48}
    >
      <PublicationHeader
        image={imageUrl}
        creator={creator}
        publishedAt={publishedAt}
        categoryName={categoryName}
        publisher={publisher}
        publicationsUrl="#"
        baseUrl=""
        showShareButton={false}
        publicationDescription={publicationPayload?.description ?? description}
        publicationId={publication.id}
        publicationTitle={publicationPayload?.title ?? title}
      />

      <PublicationContent
        title={publicationPayload?.title ?? title}
        content={publicationPayload?.content ?? content}
      />
      <PublicationTags tags={tags} />

      <FilesContainer>
        {files &&
          files.map(
            ({
              localId,
              media,
              tags,
              extentSize,
              creator,
              language,
              description,
              title,
              isFromCMS,
            }) => (
              <FileMetadataCollapse
                isCollapsed={false}
                showCollapseButton={false}
                key={localId}
                metadata={{
                  file: isFromCMS ? undefined : (media as File),
                  fileId: localId,
                  size: extentSize,
                  tags,
                  mime: isFromCMS
                    ? (media as MediaInterface).data.attributes.mime
                    : (media as File).type,
                  producedBy: creator,
                  creator,
                  language,
                  description,
                  title,
                  media: isFromCMS ? (media as MediaInterface) : undefined,
                }}
              />
            ),
          )}
      </FilesContainer>

      <RightsText variant={TextVariantEnum.CAPTION}>
        {t("createPublication.form.license.label")}: {rights}
      </RightsText>
    </FlexBox>
  );
};

export default EditPublicationResume;
