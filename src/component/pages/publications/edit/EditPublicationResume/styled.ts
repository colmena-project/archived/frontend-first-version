import { styled, Box } from "@material-ui/core";
import Text from "@/components/ui/Text";

export const RightsText = styled(Text)(({ theme }) => ({
  color: theme.palette.gray.main,
  margin: "10px",
}));

export const FilesContainer = styled(Box)({
  padding: "0 10px",
  width: "100%",
});
