/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from "react";

import { EditPublicationFile, LibraryItemInterface, NewPublicationFile } from "@/interfaces/index";
import SelectFiles from "../../files/SelectFiles";
import usePublicationFiles from "@/hooks/cms/usePublicationFiles";
import SimpleBackdrop from "@/components/ui/Backdrop";
import EditFieldModal from "../../files/EditFieldModal";
import * as yup from "yup";
import { useTranslation } from "next-i18next";
import LibraryModal from "@/components/ui/LibraryModal";
import { getOnlyFilename, isAvalableFileExtension, reorderItemList } from "@/utils/utils";
import { ButtonSizeEnum, CurrentPublicationStateEnum } from "@/enums/*";

import useFileInLibrary from "@/hooks/useFileInLibrary";
import { Box } from "@material-ui/core";
import FileListItemComponent from "../../files/FirstListItem";
import { v4 as uuid } from "uuid";
import {
  DragDropContext,
  Droppable,
  DropResult,
  DroppableProvided,
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
} from "react-beautiful-dnd";

import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { LibrarySelectButton, FileItem } from "./styled";
import { useUpdatePublicationFile } from "@/hooks/cms/useUpdatePublicationFile";
import ActionConfirm from "@/components/ui/ActionConfirm";
import { useDeletePublicationFile } from "@/hooks/cms/useDeletePublicationFile";
import { toast } from "@/utils/notifications";
import DraggableItemWrapper from "@/components/ui/DraggableItemWrapper";

type EditPublicationFilesProps = {
  publicationId: string;
  userId: string;
  setLocalFiles: React.Dispatch<React.SetStateAction<NewPublicationFile[]>>;
  localFiles: NewPublicationFile[];
  files: (NewPublicationFile | EditPublicationFile)[];
  setFiles: React.Dispatch<React.SetStateAction<(NewPublicationFile | EditPublicationFile)[]>>;
};

const EditPublicationFiles: React.FC<EditPublicationFilesProps> = ({
  publicationId,
  userId,
  localFiles,
  setLocalFiles,
  files,
  setFiles,
}) => {
  const [cmsFiles, setCmsFiles] = useState<EditPublicationFile[]>([]);
  const [fileSelected, setFileSelected] = useState<NewPublicationFile | EditPublicationFile>();

  const [modals, setModals] = useState({
    delete: false,
    editTitle: false,
    editDescription: false,
    library: false,
  });

  const { loading, publicationFiles, refetch } = usePublicationFiles(publicationId);
  const { t } = useTranslation("publications");
  const { fetchFile, fetching } = useFileInLibrary(userId);
  const { user } = useSelector((state: { user: PropsUserSelector }) => state.user);

  const { updating, update } = useUpdatePublicationFile();
  const { deleteFile, deleting } = useDeletePublicationFile();

  useEffect(() => {
    if (!publicationFiles) {
      setCmsFiles([]);
      return;
    }

    const normalized = publicationFiles.map((file, idx) => ({
      id: file.id,
      title: file.attributes.title,
      description: file.attributes.description,
      format: file.attributes.format,
      extentSize: String(file.attributes.extent_size),
      extentDuration: file.attributes.extent_duration,
      media: file.attributes.media,
      isFromCMS: true,
      position: file.attributes?.position ?? idx,
      language: file.attributes.language,
      creator: file.attributes.creator,
      publicationId: 0,
      tags: file.attributes.tags,
      status: CurrentPublicationStateEnum.UPLOADED,
      localId: file.id,
      statusMessage: "",
      duration: 0,
      isFromLibrary: false,
      creatorName: file.attributes.creator,
    })) as unknown as EditPublicationFile[];

    setCmsFiles(normalized);
  }, [publicationFiles]);

  useEffect(() => {
    const together = [...cmsFiles, ...localFiles];

    setFiles(together);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cmsFiles, localFiles]);

  const toggleModal = useCallback(
    (modal: "delete" | "editTitle" | "editDescription" | "library") => {
      setModals((currState) => ({
        ...currState,
        [modal]: !currState[modal],
      }));
    },
    [],
  );

  const onSelectFileFromLibrary = useCallback(
    async (filePayload: LibraryItemInterface) => {
      const file = await fetchFile(filePayload);

      if (!file) {
        toast(t("edit.notAllowed"), "warning");
        toggleModal("library");
        return;
      }

      const exist = localFiles.find((i) => i.localId === file.id);

      if (exist) {
        toast(t("fileManager.form.alreadyBeenChose"));
      }

      const newFile: NewPublicationFile = {
        description: file.description,
        title: file.title,
        localId: file.id,
        publicationId: Number(publicationId),
        status: CurrentPublicationStateEnum.PENDING,
        extentSize: file.size,
        isFromLibrary: true,
        format: file.format,
        language: file.language ?? user.language,
        creator: file.creator,
        tags: file.tags,
        media: file.media,
        creatorName: file.creatorName,
      };

      setLocalFiles((old) => [...old, newFile]);

      toggleModal("library");
    },
    [fetchFile, t, localFiles, publicationId, user, setLocalFiles, toggleModal],
  );

  const onDragEnd = useCallback((result: DropResult) => {
    if (!result?.destination?.index) {
      return;
    }

    setFiles((fileList) =>
      reorderItemList(fileList, result.source.index, result.destination?.index as number),
    );
  }, []);

  const onSelectLocalFile = (newFiles: FileList | null) => {
    if (!newFiles) {
      return;
    }

    const arrayFiles = Array.from(newFiles)
      .filter((file) => {
        const isAvalable = isAvalableFileExtension(file.name);

        if (!isAvalable) {
          toast(t("fileManager.messages.notAllowed"), "warning");
        }

        return isAvalable;
      })
      .map((file) => {
        const onlyName = getOnlyFilename(file.name);

        return {
          publicationId,
          media: file,
          title: onlyName,
          description: "",
          creator: user.id,
          format: file.type,
          language: user.language,
          extentSize: file.size,
          tags: "",
          status: CurrentPublicationStateEnum.PENDING,
          localId: uuid(),
          isFromLibrary: false,
          isFromCMS: false,
        };
      }) as unknown as NewPublicationFile[];

    setLocalFiles((prev) => {
      const filesToInsert = arrayFiles.filter(
        (newFile) => !prev.find((file) => file.title === newFile.title),
      );

      return [...prev, ...filesToInsert];
    });
  };

  const handleUpdateLocalFile = (values: { [key: string]: string }, fileId: string) => {
    setLocalFiles((currentState) =>
      currentState.map((file) => {
        if (file.localId === fileId) {
          return {
            ...file,
            ...values,
          };
        }

        return file;
      }),
    );

    setFileSelected(undefined);
  };

  const handleRemoveFile = (fileId: string) => {
    setLocalFiles((prevState) => prevState.filter((file) => file.localId !== fileId));
  };

  const validationSchemaTitle = yup.object().shape({
    title: yup
      .string()
      .max(60, `60 ${t("fileManager.validation.maxCharacters")}`)
      .required(t("fileManager.validation.required")),
  });

  const validationSchemaDescription = yup.object().shape({
    description: yup.string().max(400, `400 ${t("fileManager.validation.maxCharacters")}`),
  });

  return (
    <Box flex={1}>
      <SimpleBackdrop open={loading || updating || deleting} />
      <SelectFiles htmlFor="files" onSelectFromLibrary={() => toggleModal("library")} />
      <input
        name="files"
        id="files"
        type="file"
        data-testid="input-file-select"
        hidden
        multiple
        onChange={($event) => {
          onSelectLocalFile($event?.currentTarget?.files);
        }}
      />

      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable-file-list">
          {(provided: DroppableProvided) => (
            <div {...provided.droppableProps} ref={provided.innerRef} data-testid="files-wrapper">
              {files.map((file, idx) => (
                <Draggable
                  key={file.localId}
                  draggableId={file.localId}
                  index={idx}
                  data-testid="file-item"
                >
                  {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
                    <DraggableItemWrapper provided={provided} snapshot={snapshot}>
                      <FileItem data-testid="file-edit-item">
                        <FileListItemComponent
                          file={file}
                          user={user}
                          hideCheckBox
                          onClickRemove={() => {
                            setFileSelected(file);
                            toggleModal("delete");
                          }}
                          onClickInDescription={() => {
                            setFileSelected(file);
                            toggleModal("editDescription");
                          }}
                          onClickInTitle={() => {
                            setFileSelected(file);
                            toggleModal("editTitle");
                          }}
                          onSelectFile={() => setFileSelected(file)}
                        />
                      </FileItem>
                    </DraggableItemWrapper>
                  )}
                </Draggable>
              ))}
            </div>
          )}
        </Droppable>
      </DragDropContext>

      {modals.editTitle && (
        <EditFieldModal
          fieldName="title"
          handleClose={() => toggleModal("editTitle")}
          handlerSave={(values, resetForm) => {
            toggleModal("editTitle");

            if (fileSelected?.isFromCMS) {
              update(
                fileSelected.localId,
                {
                  description: fileSelected.description,
                  title: values.title,
                },
                () => {
                  resetForm();
                  setFileSelected(undefined);
                  refetch();
                },
              );

              return;
            }

            resetForm();
            handleUpdateLocalFile(values, fileSelected?.localId as string);
          }}
          initialValues={{ title: fileSelected?.title ?? "" }}
          open
          loading={false}
          title={t("fileManager.editTitle")}
          validationSchema={validationSchemaTitle}
          maxLength={60}
        />
      )}

      {modals.editDescription && (
        <EditFieldModal
          fieldName="description"
          handleClose={() => toggleModal("editDescription")}
          handlerSave={(values, resetForm) => {
            toggleModal("editDescription");

            if (fileSelected?.isFromCMS) {
              update(
                fileSelected.localId,
                {
                  description: values.description,
                  title: fileSelected.title,
                },
                () => {
                  resetForm();
                  setFileSelected(undefined);
                  refetch();
                },
              );

              return;
            }

            resetForm();
            handleUpdateLocalFile(values, fileSelected?.localId as string);
          }}
          initialValues={{ description: fileSelected?.description ?? "" }}
          open
          loading={false}
          title={t("fileManager.editDescription")}
          validationSchema={validationSchemaDescription}
          maxLength={400}
        />
      )}

      {modals.library && (
        <LibraryModal
          title={t("fileManager.form.selectFileModalTitle")}
          handleClose={() => toggleModal("library")}
          open
          onlyDirectories={false}
          options={(item: LibraryItemInterface) => {
            if (item.type === "file" && isAvalableFileExtension(item.filename)) {
              return (
                <LibrarySelectButton
                  handleClick={() => onSelectFileFromLibrary(item)}
                  title={t("fileManager.form.chooseFile")}
                  size={ButtonSizeEnum.SMALL}
                  isLoading={fetching === item.id}
                />
              );
            }

            return null;
          }}
        />
      )}

      <ActionConfirm
        data-testid="delete-file-modal-confirm"
        onOk={() => {
          toggleModal("delete");

          if (fileSelected?.isFromCMS) {
            deleteFile(fileSelected.localId, () => {
              setFileSelected(undefined);
              refetch();
            });
          }

          if (fileSelected) {
            handleRemoveFile(fileSelected.localId);
            setFileSelected(undefined);
          }
        }}
        open={modals.delete}
        onClose={() => () => toggleModal("delete")}
        showMessage={false}
        title={t("fileManager.removeConfirmMessage")}
      />
    </Box>
  );
};

export default EditPublicationFiles;
