import Button from "@/components/ui/Button";
import { styled, ListItem, Theme } from "@material-ui/core";

export const LibrarySelectButton = styled(Button)({
  marginLeft: 10,
});

export const FileItem = styled(ListItem)<Theme, any>({
  paddingLeft: 0,
  paddingRight: 0,
  marginBottom: 2,
  padding: 0,
});
