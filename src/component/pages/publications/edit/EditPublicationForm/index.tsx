import React, { memo, useMemo } from "react";
import { Formik, Form, Field, FieldProps } from "formik";
import { useTranslation } from "next-i18next";
import TextField from "@material-ui/core/TextField";
import FormHelperText from "@material-ui/core/FormHelperText";
import { MenuItem } from "@material-ui/core";
import * as yup from "yup";
import FormControl from "@material-ui/core/FormControl";
import Divider from "@/components/ui/Divider";
import ImageInputComponent from "../../common/ImageInput";
import PublicationEditorComponent from "../../PublicationEditor";
import DateFnsUtils from "@date-io/date-fns";
import { PublicationQueryInterface } from "@/interfaces/cms";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { getLanguageDateFormat, getLocale } from "@/utils/utils";
import { format, parseISO } from "date-fns";
import {
  EditorWrapper,
  FieldEditorLabel,
  FieldLabel,
  DescriptionInput,
  HiddenButton,
  SelectField,
} from "./styled";

export type EditPublicationFormValues = {
  title: string;
  image?: File | string | null;
  description: string;
  content: string;
  category: string;
  license: string;
  publicationDate: Date;
  relation?: number;
};

type EditPublicationFormProps = {
  submitButtonRef: React.MutableRefObject<HTMLButtonElement | null>;
  onNext: (values: EditPublicationFormValues, isChanged: boolean) => void | Promise<void>;
  language: string;
  userId: number | string;
  publication: PublicationQueryInterface;
  publicationImageUrl: string | null;
  categories: {
    id: string;
    name: string;
  }[];
};

const EditPublicationForm: React.FC<EditPublicationFormProps> = ({
  submitButtonRef,
  onNext,
  language,
  publication,
  publicationImageUrl,
  categories,
}) => {
  const { t } = useTranslation("publications");

  const INITIAL_VALUES = useMemo(
    () => ({
      title: publication.attributes.title ?? "",
      image: publicationImageUrl,
      description: publication.attributes.description ?? "",
      content: publication.attributes.content ?? "",
      category: publication.attributes.subject.data.id,
      license: publication.attributes.rights,
      publicationDate: publication.attributes.date
        ? parseISO(publication.attributes.date)
        : new Date(),
    }),
    [publication, publicationImageUrl],
  );

  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        title: yup.string().trim().required(t("createPublication.form.title.validation")).max(100),
        image: yup.mixed().optional(),
        description: yup
          .string()
          .trim()
          .required()
          .max(200, t("createPublication.form.description.helper")),
        content: yup
          .string()
          .trim()
          .required(t("createPublication.form.content.validation"))
          .test(
            "createPublication.isEmpty",
            t("createPublication.form.content.validation"),
            (value = "") => Boolean(value.replace(/<\/?[^>]+(>|$)/g, "")),
          ),
        category: yup.string().required(t("createPublication.form.category.validation")),
        license: yup.string().required(t("createPublication.form.license.validation")),
        publicationDate: yup
          .date()
          .required(t("createPublication.form.publicationDate.validation")),
      }),
    [t],
  );

  const handlerOnSubmit = (values: EditPublicationFormValues) => {
    const keys = Object.keys(values);

    const isChanged = keys.some((key) => {
      if (key === "image" && INITIAL_VALUES.image) {
        return typeof values[key] !== "string";
      }

      if (key === "publicationDate") {
        const oldDate = format(INITIAL_VALUES.publicationDate, "yyyy-MM-dd");
        const currentDate = format(values.publicationDate, "yyyy-MM-dd");

        return oldDate !== currentDate;
      }

      return (values as any)[key] !== (INITIAL_VALUES as any)[key];
    });

    onNext(values, isChanged);
  };

  const prepareTermHelper = (remaining: number) => {
    const translateTermPlural = "createPublication.form.description.helper.plural";
    const translateTermSingular = "createPublication.form.description.helper.singular";
    return `${remaining} ${remaining > 1 ? t(translateTermPlural) : t(translateTermSingular)}`;
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={getLocale(language)}>
      <Formik
        validationSchema={validationSchema}
        initialValues={INITIAL_VALUES}
        onSubmit={handlerOnSubmit}
      >
        {({ errors, touched, setFieldValue }) => (
          <Form>
            <Field name="image" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <ImageInputComponent
                  field={field}
                  description={t("createPublication.form.image.placeholder")}
                  setFieldValue={setFieldValue}
                />
              )}
            </Field>
            {touched.content && Boolean(errors.content) && (
              <FormHelperText error>{t("createPublication.form.image.validation")}</FormHelperText>
            )}
            <Divider marginTop={20} />

            <Field name="title" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <TextField
                  id="title"
                  data-testid="input-title"
                  label={t("createPublication.form.title.label")}
                  placeholder={t("createPublication.form.title.placeholder")}
                  variant="outlined"
                  fullWidth
                  required
                  error={touched.title && Boolean(errors.title)}
                  helperText={touched.title && errors.title}
                  inputProps={{ maxLength: 100 }}
                  {...field}
                />
              )}
            </Field>
            <Divider marginTop={20} />

            <Field name="description" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => {
                const remaining = 200 - field.value.length;

                return (
                  <DescriptionInput
                    id="description"
                    data-testid="input-description"
                    label={t("createPublication.form.description.label")}
                    placeholder={t("createPublication.form.description.placeholder")}
                    required
                    variant="outlined"
                    multiline
                    fullWidth
                    error={touched.description && Boolean(errors.description)}
                    helperText={prepareTermHelper(remaining)}
                    FormHelperTextProps={{ className: "helper-text" }}
                    inputProps={{ maxLength: 200 }}
                    {...field}
                  />
                );
              }}
            </Field>
            <Divider marginTop={20} />

            <EditorWrapper>
              <FieldEditorLabel>{t("createPublication.form.content.label")}</FieldEditorLabel>
              <Field name="content" InputProps={{ notched: true }}>
                {({ field }: FieldProps) => (
                  <PublicationEditorComponent
                    content={field.value}
                    save={(content: string) => {
                      setFieldValue("content", content);
                    }}
                    onBlur={field.onBlur}
                    data-testid="input-content"
                  />
                )}
              </Field>

              {Boolean(errors.content) && (
                <FormHelperText error>
                  {t("createPublication.form.content.validation")}
                </FormHelperText>
              )}
            </EditorWrapper>
            <Divider marginTop={20} />
            <Field name="category" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <FormControl variant="outlined" fullWidth>
                  <FieldLabel htmlFor="category">
                    {t("createPublication.form.category.label")}
                  </FieldLabel>
                  <SelectField
                    placeholder={t("createPublication.form.category.placeholder")}
                    id="category"
                    required
                    data-testid="select-category"
                    {...field}
                  >
                    <MenuItem value="">{t("createPublication.form.category.placeholder")}</MenuItem>

                    {Boolean(categories.length) &&
                      categories.map((category) => (
                        <MenuItem key={`category_${category.id}`} value={category.id}>
                          {category.name}
                        </MenuItem>
                      ))}
                  </SelectField>
                  {touched.category && Boolean(errors.category) && (
                    <FormHelperText error>
                      {t("createPublication.form.category.validation")}
                    </FormHelperText>
                  )}
                </FormControl>
              )}
            </Field>
            <Divider marginTop={20} />
            <Field name="license" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <FormControl variant="outlined" fullWidth>
                  <FieldLabel htmlFor="license">
                    {t("createPublication.form.license.label")}
                  </FieldLabel>
                  <SelectField required id="license" {...field} data-testid="select-license">
                    <MenuItem value="BY-SA-NC">BY-SA-NC</MenuItem>
                  </SelectField>
                  {touched.license && Boolean(errors.license) && (
                    <FormHelperText error>
                      {t("createPublication.form.license.validation")}
                    </FormHelperText>
                  )}
                </FormControl>
              )}
            </Field>
            <Divider marginTop={20} />
            <Field name="publicationDate" InputProps={{ notched: true, require: true }}>
              {({ field }: FieldProps) => (
                <KeyboardDatePicker
                  data-testid="select-date"
                  disableToolbar
                  variant="dialog"
                  inputVariant="outlined"
                  fullWidth
                  margin="normal"
                  required
                  id="publicationDate"
                  {...field}
                  label={t("createPublication.form.publicationDate.label")}
                  onChange={(date) => setFieldValue("publicationDate", date)}
                  helperText={errors.publicationDate}
                  error={touched.publicationDate && Boolean(errors.publicationDate)}
                  disablePast
                  format={getLanguageDateFormat(language)}
                />
              )}
            </Field>
            <HiddenButton type="submit" title="" ref={submitButtonRef} />
          </Form>
        )}
      </Formik>
    </MuiPickersUtilsProvider>
  );
};

export default memo(EditPublicationForm);
