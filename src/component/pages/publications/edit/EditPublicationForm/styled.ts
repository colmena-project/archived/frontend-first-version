import { Box, InputLabel, styled, TextField, Button, Select } from "@material-ui/core";

export const FieldLabel = styled(InputLabel)(({ theme }) => ({
  backgroundColor: theme.palette.common.white,
  padding: "0 4px",
}));

export const FieldEditorLabel = styled(InputLabel)({
  marginBottom: 6,
});

export const EditorWrapper = styled(Box)(() => ({
  "& .ProseMirror": {
    minHeight: "20vh",
    marginBottom: 0,
    padding: "15px",
    "&:focus, img": {
      outline: "none",
    },
    border: "2px solid #e5e5e5",
  },
  width: "100%",
  margin: "auto",
  "& .ProseMirror .ProseMirror-selectednode": {
    outline: "2px solid #68cef8",
  },
  "& .ProseMirror p.is-editor-empty:first-child::before": {
    float: "left",
    color: "#ced4da",
    pointerEvents: "none",
    height: 0,
  },
  textAlign: "left",
}));

export const DescriptionInput = styled(TextField)({
  "& .helper-text": {
    textAlign: "right",
  },
});

export const HiddenButton = styled(Button)({
  display: "none",
});

export const SelectField = styled(Select)({
  textAlign: "left",
});
