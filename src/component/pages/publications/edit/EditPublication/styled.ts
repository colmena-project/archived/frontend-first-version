import { Box, styled } from "@material-ui/core";

export const MenuOptionWrapper = styled(Box)({
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-end",
});

export const StepsContent = styled(Box)({
  padding: "10px 10px 180px",
  width: "100%",
  flex: 1,
});
