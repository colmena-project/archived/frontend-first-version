/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable indent */
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useTranslation } from "next-i18next";
import { ContextMenuOptionEnum, CurrentPublicationStateEnum, EnvironmentEnum } from "@/enums/*";
import { useRouter } from "next/router";

import SimpleBackdrop from "@/components/ui/Backdrop";
import ContextMenuOptions from "@/components/pages/library/contextMenu";
import { PublicationLayout } from "@/components/pages/publications/common/styled";
import useIsAdmin from "@/hooks/useIsAdmin";
import PublicationStepsFooter from "@/components/pages/publications/edit/PublicationStepsFooter";
import PublicationToolsMenu from "@/components/pages/publications/PublicationToolsMenu";
import EditPublicationForm, {
  EditPublicationFormValues,
} from "@/components/pages/publications/edit/EditPublicationForm";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import { getUrlStrapiImage } from "@/utils/utils";
import { useUpdatePublication } from "@/hooks/cms/useUpdatePublication";
import { toast } from "@/utils/notifications";
import { PublicationQueryInterface } from "@/interfaces/cms";
import { EditPublicationFile, NewPublicationFile } from "@/interfaces/index";

import { MenuOptionWrapper, StepsContent } from "./styled";
import PublicationOptionMenu from "../../common/PublicationOptionMenu";
import EditPublicationFiles from "../EditPublicationFiles";
import { createPublication } from "@/store/idb/models/publications";
import useUpdatePublicationFilesWorker from "@/hooks/useUpdatePublicationFilesWorker";
import EditPublicationResume from "../EditPublicationResume";

type EditPublicationProps = {
  publicationId: string;
  publication: PublicationQueryInterface;
  categories: {
    id: string;
    name: string;
  }[];
};

function EditPublication({ publicationId, publication, categories }: EditPublicationProps) {
  const [publicationPayload, setPublicationPayload] = useState<EditPublicationFormValues | null>(
    null,
  );
  const [step, setStep] = useState(1);
  const [files, setFiles] = useState<(NewPublicationFile | EditPublicationFile)[]>([]);
  const [menuOpen, setMenuOpen] = useState(false);
  const [localFiles, setLocalFiles] = useState<NewPublicationFile[]>([]);

  const { t } = useTranslation("publications");
  const { t: c } = useTranslation("common");

  const submitButtonRef1 = useRef<HTMLButtonElement | null>(null);

  const router = useRouter();

  const { user } = useSelector((state: { user: PropsUserSelector }) => state.user);

  const { update, updating } = useUpdatePublication();
  const { runWorker } = useUpdatePublicationFilesWorker();

  const categoryName = useMemo(() => {
    if (!publicationPayload) {
      const finded = categories.find((i) => i.id === publication.attributes.subject.data.id);

      return finded?.name ?? "";
    }

    const finded = categories.find((i) => i.id === publicationPayload.category);

    return finded?.name ?? "";
  }, [categories, publicationPayload, publication]);

  const isAdmin = useIsAdmin();

  const pageTitle = useMemo(() => {
    if (step === 1) {
      return t("edit.titles.step1");
    }

    if (step === 2) {
      return t("edit.titles.step2");
    }

    return t("preview.title");
  }, [step, t]);

  useEffect(() => {
    if (!isAdmin) {
      router.replace("/publications");
    }
  }, [router, isAdmin]);

  const publicationImageUrl = useMemo(() => {
    if (publication) {
      return getUrlStrapiImage(publication.attributes.relation, "thumbnail");
    }

    return null;
  }, [publication]);

  const togggleMenu = useCallback(() => {
    setMenuOpen((s) => !s);
  }, []);

  const handleSaveAndFinish = async () => {
    if (publicationPayload) {
      await update(
        publicationId,
        {
          ...publicationPayload,
          relation: publication?.attributes?.relation?.data?.id,
        },
        () => {
          toast(t("edit.publicationUpdated"), "success");

          router.replace("/publications/management");
        },
      );
    }
  };

  const handleSaveAndFinishStep2 = async () => {
    if (localFiles.length) {
      const localPublicationId = (await createPublication({
        title: publication.attributes.title,
        description: publication.attributes.description,
        refId: publicationId,
        status: CurrentPublicationStateEnum.EDIT,
        files: localFiles,
      })) as number;

      runWorker({ publicationId: localPublicationId });

      if (!publicationPayload) {
        toast(t("edit.publicationUpdated"), "success");

        router.replace("/publications/management");
        return;
      }
    }

    if (publicationPayload) {
      await update(
        publicationId,
        {
          ...publicationPayload,
          relation: publication?.attributes?.relation?.data?.id,
        },
        () => {
          toast(t("edit.publicationUpdated"), "success");

          router.replace("/publications/management");
        },
      );
    }
  };

  if (!isAdmin) {
    return (
      <PublicationLayout title={pageTitle} showFooter={false} drawer={false}>
        <SimpleBackdrop open />
      </PublicationLayout>
    );
  }

  return (
    <PublicationLayout
      title={pageTitle}
      showFooter={false}
      drawer
      notifications={false}
      rightExtraElement={
        <ContextMenuOptions
          basename={router.pathname}
          aliasFilename=""
          filename=""
          environment={"" as EnvironmentEnum}
          id=""
          availableOptions={[ContextMenuOptionEnum.MANAGE_PUBLICATIONS]}
          onChange={() => {
            setMenuOpen(true);
          }}
        />
      }
    >
      <SimpleBackdrop open={updating} />

      <StepsContent>
        {step === 1 && (
          <EditPublicationForm
            submitButtonRef={submitButtonRef1}
            categories={categories}
            onNext={(values, isChanged) => {
              setPublicationPayload(values);

              if (isChanged) {
                setMenuOpen(true);
                return;
              }

              setStep(2);
            }}
            language={user.language}
            publication={publication}
            userId={user.id}
            publicationImageUrl={publicationImageUrl}
          />
        )}

        {step === 2 && (
          <EditPublicationFiles
            publicationId={publicationId}
            userId={user.id}
            localFiles={localFiles}
            setLocalFiles={setLocalFiles}
            files={files}
            setFiles={setFiles}
          />
        )}

        {step === 3 && (
          <EditPublicationResume
            categoryName={categoryName}
            language={user.language}
            publication={publication}
            publicationPayload={publicationPayload}
            files={files}
          />
        )}
      </StepsContent>

      <PublicationToolsMenu onClose={togggleMenu} open={menuOpen}>
        <MenuOptionWrapper>
          <PublicationOptionMenu
            icon="save_and_next"
            onClick={() => {
              if (step === 1) {
                setStep(2);
              }

              if (step === 2) {
                setStep(3);
              }

              setMenuOpen(false);
            }}
          >
            {t("edit.menu.saveAndContinue")}
          </PublicationOptionMenu>
        </MenuOptionWrapper>

        <MenuOptionWrapper>
          <PublicationOptionMenu
            icon="save"
            onClick={() => {
              setMenuOpen(false);

              if (step === 1) {
                handleSaveAndFinish();
              }

              if (step === 2) {
                handleSaveAndFinishStep2();
              }
            }}
          >
            {t("edit.menu.saveAndFinish")}
          </PublicationOptionMenu>
        </MenuOptionWrapper>
      </PublicationToolsMenu>

      <PublicationStepsFooter
        currentStep={step}
        totalStep={3}
        backLabel={step === 1 ? c("form.cancelButton") : c("form.backButton")}
        nextLabel={c("form.nextIntro")}
        onClickBack={() => {
          if (step === 1) {
            router.back();
            return;
          }

          setStep((s) => s - 1);
        }}
        onClickNext={() => {
          if (step === 1) {
            submitButtonRef1?.current?.click();

            return;
          }

          if (step === 2) {
            setMenuOpen(true);
          }

          if (step === 3) {
            handleSaveAndFinishStep2();
          }
        }}
        menuOpened={menuOpen}
      />
    </PublicationLayout>
  );
}

export default EditPublication;
