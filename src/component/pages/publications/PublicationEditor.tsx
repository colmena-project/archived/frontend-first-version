/* eslint-disable @typescript-eslint/ban-ts-comment */
import useDebounce from "@/hooks/useDebounce";
import React, { useEffect } from "react";
import { EditorState, ContentState, convertFromHTML } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import dynamic from "next/dynamic";
import CONSTANTS from "@/constants/index";
import { convertToHTML } from "draft-convert";

import { EditorWrapper } from "./styled";

const DraftEditor = dynamic(() => import("react-draft-wysiwyg").then((mod) => mod.Editor), {
  ssr: false,
});

type PublicationEditorProps = {
  content: string;
  save: (editorHtmlContent: string) => void;
  onBlur?: React.FocusEventHandler<HTMLDivElement> | undefined;
  name?: string;
  placeholder?: string;
  onFocus?: React.FocusEventHandler<HTMLDivElement> | undefined;
};

export default function PublicationEditorComponent({ save, content }: PublicationEditorProps) {
  const [editorState, setEditorState] = React.useState(() =>
    EditorState.createWithContent(
      ContentState.createFromBlockArray(convertFromHTML(content).contentBlocks),
    ),
  );

  const debouncedValue = useDebounce<EditorState>(
    editorState,
    CONSTANTS.DEBOUNCE_TIME_FILE_AUTO_SAVE,
  );

  useEffect(() => {
    const currentContentAsHTML = convertToHTML(debouncedValue.getCurrentContent());

    save(currentContentAsHTML);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedValue]);

  return (
    <EditorWrapper data-testid="content-editor">
      <DraftEditor
        // @ts-ignore
        editorState={editorState}
        toolbarClassName="toolbarCustomClass"
        wrapperClassName="wrapper"
        editorClassName="editorCustomClass"
        onEditorStateChange={setEditorState}
        toolbar={{
          options: ["inline", "blockType", "list", "colorPicker", "link"],
          inline: { inDropdown: true },
          list: { inDropdown: true },
          textAlign: { inDropdown: true },
          link: { inDropdown: true },
          history: { inDropdown: true },
        }}
      />
    </EditorWrapper>
  );
}
