import { Formik, Form, Field, FieldProps, FormikHelpers } from "formik";
import { useTranslation } from "next-i18next";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import { Box, Button, makeStyles, MenuItem } from "@material-ui/core";
import classNames from "classnames";
import * as yup from "yup";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import Divider from "@/components/ui/Divider";
import { useStyles } from "@/utils/textEditor";
import React, { memo, useMemo } from "react";
import ImageInputComponent from "./common/ImageInput";
import PublicationEditorComponent from "./PublicationEditor";
import useCategories from "@/hooks/cms/useCategories";
import DateFnsUtils from "@date-io/date-fns";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { getLanguageDateFormat, getLocale } from "@/utils/utils";
import { CurrentPublication } from "@/interfaces/index";
import { parseISO } from "date-fns";

export type FormValues = {
  title: string;
  image: null | FileList | File;
  description: string;
  content: string;
  category: string;
  license: string;
  publicationDate: Date;
};

type NewPublicationFormProps = {
  submitButtonRef: React.MutableRefObject<HTMLButtonElement | null>;
  onSubmit: (values: FormValues, formikHelpers: FormikHelpers<FormValues>) => void | Promise<void>;
  language: string;
  userId: number | string;
  currentPublication: CurrentPublication | null;
};

const NewPublicationForm: React.FC<NewPublicationFormProps> = ({
  submitButtonRef,
  onSubmit,
  language,
  currentPublication,
}) => {
  const { t } = useTranslation("publications");

  const { categories, loading: loadingCategories } = useCategories(language);

  const INITIAL_VALUES = useMemo(
    () => ({
      title: currentPublication?.title ?? "",
      image: currentPublication?.thumbnail ? (currentPublication?.thumbnail as File) : null,
      description: currentPublication?.description ?? "",
      content: currentPublication?.content ?? "",
      category: currentPublication?.category
        ? `${currentPublication?.category}-${currentPublication?.categoryName}`
        : "",
      license: currentPublication?.rights ?? "BY-SA-NC",
      publicationDate: currentPublication?.date ? parseISO(currentPublication?.date) : new Date(),
    }),
    [currentPublication],
  );

  const validationSchema = useMemo(
    () =>
      yup.object().shape({
        title: yup.string().trim().required(t("createPublication.form.title.validation")).max(100),
        image: yup.mixed().optional(),
        description: yup
          .string()
          .trim()
          .optional()
          .max(200, t("createPublication.form.description.helper")),
        content: yup
          .string()
          .trim()
          .required(t("createPublication.form.content.validation"))
          .test(
            "createPublication.isEmpty",
            t("createPublication.form.content.validation"),
            (value = "") => Boolean(value.replace(/<\/?[^>]+(>|$)/g, "")),
          ),
        category: yup.string().required(t("createPublication.form.category.validation")),
        license: yup.string().required(t("createPublication.form.license.validation")),
        publicationDate: yup
          .date()
          .required(t("createPublication.form.publicationDate.validation")),
      }),
    [t],
  );

  const classes = useClasses();
  const editorStyle = useStyles();

  const singularName = "createPublication.form.description.helper.singular";
  const pluralName = "createPublication.form.description.helper.plural";

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={getLocale(language)}>
      <Formik
        validationSchema={validationSchema}
        initialValues={INITIAL_VALUES}
        onSubmit={onSubmit}
      >
        {({ errors, touched, setFieldValue }) => (
          <Form>
            <Field name="image" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <ImageInputComponent
                  field={field}
                  description={t("createPublication.form.image.placeholder")}
                  setFieldValue={setFieldValue}
                />
              )}
            </Field>
            {touched.content && Boolean(errors.content) && (
              <FormHelperText error>{t("createPublication.form.image.validation")}</FormHelperText>
            )}
            <Divider marginTop={20} />

            <Field name="title" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <TextField
                  id="title"
                  label={t("createPublication.form.title.label")}
                  placeholder={t("createPublication.form.title.placeholder")}
                  variant="outlined"
                  fullWidth
                  error={touched.title && Boolean(errors.title)}
                  helperText={touched.title && errors.title}
                  inputProps={{ maxLength: 100 }}
                  {...field}
                />
              )}
            </Field>
            <Divider marginTop={20} />

            <Field name="description" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => {
                const remaining = 200 - field.value.length;

                return (
                  <TextField
                    id="description"
                    label={t("createPublication.form.description.label")}
                    placeholder={t("createPublication.form.description.placeholder")}
                    variant="outlined"
                    multiline
                    fullWidth
                    error={touched.description && Boolean(errors.description)}
                    helperText={`${remaining} ${remaining > 1 ? t(pluralName) : t(singularName)}`}
                    FormHelperTextProps={{ className: classes.helperText }}
                    inputProps={{ maxLength: 200 }}
                    {...field}
                  />
                );
              }}
            </Field>
            <Divider marginTop={20} />

            <Box
              className={classNames(editorStyle.editorWrapper, classes.editorWrapper)}
              textAlign="left"
            >
              <InputLabel className={classes.editorLabel}>
                {t("createPublication.form.content.label")}
              </InputLabel>
              <Field name="content" InputProps={{ notched: true }}>
                {({ field }: FieldProps) => (
                  <PublicationEditorComponent
                    content={field.value}
                    save={(content: string) => {
                      setFieldValue("content", content);
                    }}
                    onBlur={field.onBlur}
                  />
                )}
              </Field>

              {Boolean(errors.content) && (
                <FormHelperText error>
                  {t("createPublication.form.content.validation")}
                </FormHelperText>
              )}
            </Box>
            <Divider marginTop={20} />
            <Field name="category" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <FormControl variant="outlined" fullWidth>
                  <InputLabel className={classes.label} htmlFor="category">
                    {t("createPublication.form.category.label")}
                  </InputLabel>
                  <Select
                    placeholder={t("createPublication.form.category.placeholder")}
                    id="category"
                    {...field}
                  >
                    <MenuItem value="">{t("createPublication.form.category.placeholder")}</MenuItem>

                    {!loadingCategories &&
                      Boolean(categories.length) &&
                      categories.map((category) => (
                        <MenuItem
                          key={`category_${category.id}`}
                          value={`${category.id}-${category.name}`}
                        >
                          {category.name}
                        </MenuItem>
                      ))}
                  </Select>
                  {touched.category && Boolean(errors.category) && (
                    <FormHelperText error>
                      {t("createPublication.form.category.validation")}
                    </FormHelperText>
                  )}
                </FormControl>
              )}
            </Field>
            <Divider marginTop={20} />
            <Field name="license" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <FormControl variant="outlined" fullWidth>
                  <InputLabel className={classes.label} htmlFor="license">
                    {t("createPublication.form.license.label")}
                  </InputLabel>
                  <Select id="license" {...field}>
                    <MenuItem value="BY-SA-NC">BY-SA-NC</MenuItem>
                  </Select>
                  {touched.license && Boolean(errors.license) && (
                    <FormHelperText error>
                      {t("createPublication.form.license.validation")}
                    </FormHelperText>
                  )}
                </FormControl>
              )}
            </Field>
            <Divider marginTop={20} />
            <Field name="publicationDate" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <KeyboardDatePicker
                  disableToolbar
                  variant="dialog"
                  inputVariant="outlined"
                  fullWidth
                  margin="normal"
                  id="publicationDate"
                  {...field}
                  label={t("createPublication.form.publicationDate.label")}
                  onChange={(date) => setFieldValue("publicationDate", date)}
                  helperText={errors.publicationDate}
                  error={touched.publicationDate && Boolean(errors.publicationDate)}
                  disablePast
                  format={getLanguageDateFormat(language)}
                />
              )}
            </Field>
            <Button type="submit" title="" ref={submitButtonRef} className={classes.hiddenButton} />
          </Form>
        )}
      </Formik>
    </MuiPickersUtilsProvider>
  );
};

const useClasses = makeStyles((theme) => ({
  helperText: {
    textAlign: "right",
  },
  editorLabel: {
    marginBottom: 6,
  },
  editorWrapper: {
    "& .ProseMirror": {
      minHeight: "20vh",
      marginBottom: 0,
    },
  },
  creator: {
    color: theme.palette.gray.main,
    fontSize: 10,
    margin: 0,
    padding: 0,
    letterSpacing: "1px",
    marginLeft: 10,
  },
  label: {
    backgroundColor: theme.palette.common.white,
    padding: "0 4px",
  },
  uploadFile: {
    display: "flex",
    height: 94,
    width: "100%",
    border: `1px dashed ${theme.palette.gray.main}`,
    background: "#FAFAFA",
    marginBottom: 4,
    justifyContent: "center",
    alignItems: "center",
    color: theme.palette.gray.main,
    backgroundSize: "cover",
    backgroundPosition: "center center",
  },
  hiddenButton: {
    display: "none",
  },
}));

export default memo(NewPublicationForm);
