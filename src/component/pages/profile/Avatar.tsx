/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { getFirstLettersOfTwoFirstNames } from "@/utils/utils";
import { Theme, makeStyles, createStyles, useTheme } from "@material-ui/core/styles";
import Badge from "@material-ui/core/Badge";
import { PropsUserSelector } from "@/types/index";
import { useSelector } from "react-redux";
import SvgIcon from "@/components/ui/SvgIcon";
import classNames from "classnames";
import useCollaboratorAvatar from "@/hooks/cms/useCollaboratorAvatar";

type Props = {
  size: number;
  userId?: string | null;
  userName?: string | null;
  showEditImage?: boolean;
  className?: string;
};

function AvatarAux({
  size,
  showEditImage = false,
  userId = "",
  userName = "",
  className = "",
}: Props) {
  const theme = useTheme();
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      size: {
        width: theme.spacing(size),
        height: theme.spacing(size),
      },
      baseAvatar: { background: theme.palette.variation2.main },
    }),
  );
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const usrId = !userId ? userRdx.user.id : userId;
  const usrName = !userName ? userRdx.user.name : userName;
  const { thumbnail: avatar } = useCollaboratorAvatar(usrId);

  return (
    <Badge
      overlap="circular"
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      badgeContent={
        showEditImage ? (
          <SvgIcon icon="edit_circle" htmlColor={theme.palette.variation4.main} fontSize="medium" />
        ) : null
      }
    >
      {!navigator.onLine || !avatar ? (
        <Avatar className={classNames(classes.size, classes.baseAvatar, className)}>
          {getFirstLettersOfTwoFirstNames(usrName)}
        </Avatar>
      ) : (
        <Avatar
          alt={`Avatar ${usrName}`}
          src={`${avatar}`}
          className={classNames(classes.size, classes.baseAvatar, className)}
        >
          {getFirstLettersOfTwoFirstNames(usrName)}
        </Avatar>
      )}
    </Badge>
  );
}

export default AvatarAux;

export const AvatarMemoized = React.memo(AvatarAux);
