import { useMemo } from "react";

import { useTranslation } from "next-i18next";
import { getBrowserName, getBrowserVersion } from "@/utils/browser";

import {
  ContainerStorageBox,
  TitleStorageBox,
  ContantStorageBox,
  TitleContantStorageBox,
  ProgressContantStorageBox,
  DisplayBrowser,
  Footer,
} from "./styled";
import { formatBytes, getUsagePercent } from "@/utils/utils";

interface StorageBoxInterface {
  quota: number | undefined;
  usage: number | undefined;
  title: string;
  showBrowser?: boolean;
  footer?: string;
}

export default function StorageBox({
  quota,
  usage,
  title,
  showBrowser = false,
  footer,
  ...rest
}: StorageBoxInterface) {
  const { t } = useTranslation("profile");

  const { userAgent } = navigator;

  const remaining = useMemo(() => Number(quota) - Number(usage), [quota, usage]);
  const percent = useMemo(() => getUsagePercent(Number(quota), Number(usage)), [quota, usage]);
  const browserName = useMemo(() => getBrowserName(userAgent), [userAgent]);
  const browserVersion = useMemo(() => getBrowserVersion(userAgent), [userAgent]);

  return (
    <ContainerStorageBox {...rest} data-testid="storage-box">
      <TitleStorageBox data-testid="title-storage-box">{title}</TitleStorageBox>
      <ContantStorageBox>
        <TitleContantStorageBox>
          {t("storage.globaQuote", {
            available: formatBytes(remaining),
            usage: formatBytes(Number(usage)),
            percent,
          })}
          <ProgressContantStorageBox
            data-testid="progress-contant-storage-box"
            variant="determinate"
            value={Number(percent)}
          />
          {showBrowser && (
            <DisplayBrowser data-testid="display-browser-storage-box">
              {t("storage.browserDescription")} {browserName || "-"} {browserVersion || "-"}
            </DisplayBrowser>
          )}
          {footer && <Footer data-testid="footer-storage-box">{footer}</Footer>}
        </TitleContantStorageBox>
      </ContantStorageBox>
    </ContainerStorageBox>
  );
}
