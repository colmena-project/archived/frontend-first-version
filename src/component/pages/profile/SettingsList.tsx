import { Backdrop } from "@material-ui/core";
import { signOut } from "next-auth/client";

import ListOptionsSettings from "./ListOptionsSettings";
import { useState } from "react";
import { useRouter } from "next/router";
import ChangeLanguage from "./Language";
import { PossibleOptionsInProfileProps } from "@/types/*";
import CloudAndStorageOptions from "./CloudAndStorageOptions";
import { PageOptionsForSettingsEnum } from "@/enums/*";
import { Wrapper } from "./styled";

export default function SettingsList() {
  const router = useRouter();
  const [showBackdrop, setShowBackdrop] = useState(false);
  const [currentPage, setCurrentPage] = useState<PossibleOptionsInProfileProps>(
    PageOptionsForSettingsEnum.INITIAL,
  );

  const logoutHandler = async () => {
    if (navigator.onLine) {
      try {
        setShowBackdrop(true);
        await signOut({ redirect: false });
      } finally {
        setShowBackdrop(false);
        router.push("/login");
      }
    }
  };

  const changePage = (page: PossibleOptionsInProfileProps) => {
    setCurrentPage(page);
  };

  const pages = {
    [PageOptionsForSettingsEnum.INITIAL]: (
      <ListOptionsSettings logoutHandler={logoutHandler} changePage={changePage} />
    ),
    [PageOptionsForSettingsEnum.LANGUAGE]: <ChangeLanguage changePage={changePage} />,
    [PageOptionsForSettingsEnum.CLOUD_AND_STORAGE]: (
      <CloudAndStorageOptions changePage={changePage} />
    ),
  };

  return (
    <Wrapper component="section">
      <Backdrop open={showBackdrop} />
      {pages[currentPage]}
    </Wrapper>
  );
}
