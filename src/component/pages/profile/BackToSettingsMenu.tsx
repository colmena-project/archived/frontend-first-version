import { TextAlignEnum, TextVariantEnum } from "@/enums/*";
import { Box } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import SvgIcon from "@/components/ui/SvgIcon";

import { ReactNode } from "react";
import { Display } from "./styled";

interface BackToSettingsMenuInterface {
  action: () => void;
  titlePage: string;
  elementToTight?: ReactNode;
}

export default function BackToSettingsMenu({
  action,
  titlePage,
  elementToTight,
  ...rest
}: BackToSettingsMenuInterface) {
  return (
    <Box
      {...rest}
      display="flex"
      key="container-title"
      flexDirection="row"
      justifyContent="space-between"
      alignItems="center"
    >
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <IconButton
          key="back-btn"
          className="rtl:-scale-x-100"
          edge="start"
          color="inherit"
          aria-label="back"
          data-testid="back-btn"
          onClick={action}
        >
          <SvgIcon icon="back" />
        </IconButton>
        <Display variant={TextVariantEnum.H6} align={TextAlignEnum.LEFT}>
          {titlePage}
        </Display>
      </Box>
      {elementToTight && <Box>{elementToTight}</Box>}
    </Box>
  );
}
