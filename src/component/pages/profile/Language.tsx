import React, { useState } from "react";
import { setCookie, parseCookies } from "nookies";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { Box } from "@material-ui/core";
import Backdrop from "@/components/ui/Backdrop";
import {
  PossibleOptionsInProfileProps,
  LanguageProps,
  LanguagesAvailableProps,
  PropsUserSelector,
} from "@/types/*";
import { getSystemLanguages, prepareLanguageToNextcloud } from "@/utils/utils";
import { deleteAllMessages as deleteAllChatMessages } from "@/store/idb/models/chat";
import { update as updateUser } from "@/services/internal/users";
import { useSelector, useDispatch } from "react-redux";
import { v4 as uuid } from "uuid";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Check from "@material-ui/icons/Check";
import Text from "@/components/ui/Text";
import BackToSettingsMenu from "./BackToSettingsMenu";
import { toast } from "@/utils/notifications";
import { userInfoUpdate } from "@/store/actions/users/index";
import { PageOptionsForSettingsEnum } from "@/enums/*";
import {
  Cation,
  InputSearch,
  ListItemLanguage,
  ListLanguages,
  LanguageInformation,
} from "./styled";

interface ChangeLanguageInterface {
  changePage: (page: PossibleOptionsInProfileProps) => void;
}

export default function ChangeLanguage({ changePage }: ChangeLanguageInterface) {
  const [searchText, setSearchText] = useState("");
  const { t } = useTranslation("common");
  const { t: p } = useTranslation("profile");
  const [languagesList, setLanguagesList] = useState(getSystemLanguages(t));
  const router = useRouter();
  const [showBackdrop, setShowBackdrop] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const cookies = parseCookies();
  const defaultLang = cookies.NEXT_LOCALE;
  const backUrl = router.pathname;

  const dispatch = useDispatch();

  const searchLanguage = (text: string) => {
    if (text !== "") {
      const filter = getSystemLanguages(t).filter((row) => row.language.includes(searchText));
      setLanguagesList(filter);
      return;
    }

    setLanguagesList(getSystemLanguages(t));
  };

  const changeLanguageHandler = async (locale: LanguagesAvailableProps) => {
    if (defaultLang !== locale) {
      setShowBackdrop(true);

      const response = await updateUser({
        user: {
          ...userRdx.user,
          language: prepareLanguageToNextcloud(locale),
        },
        updatedName: false,
        updatedEmail: false,
        updatedLanguage: true,
      });

      if (response.data.success) {
        setCookie(null, "NEXT_LOCALE", locale, {
          maxAge: 30 * 24 * 60 * 60,
          path: "/",
        });

        dispatch(
          userInfoUpdate({
            ...userRdx.user,
            language: locale,
          }),
        );

        await localStorage.setItem("isChangedLanguage", "yes");
        await deleteAllChatMessages();

        setShowBackdrop(false);
        await router.push(backUrl, "", {
          locale,
        });
        router.reload();
      } else {
        console.log(response.data.data);
        toast(t("genericErrorMessage"), "error");
        setShowBackdrop(false);
      }
    }
  };

  return (
    <Box>
      <Backdrop open={showBackdrop} />
      <BackToSettingsMenu
        action={() => changePage(PageOptionsForSettingsEnum.INITIAL)}
        titlePage={p("boxLanguage.title")}
      />
      <Box display="flex" flexDirection="row" justifyContent="flex-start" className="mb-5">
        <InputSearch
          placeholder={p("boxLanguage.placeholderSearch")}
          id="outlined-search"
          value={searchText}
          onChange={(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
            setSearchText(event.target.value);
            searchLanguage(event.target.value);
          }}
        />
      </Box>
      <Box display="flex" key="container-title" flexDirection="row" justifyContent="flex-start">
        <Cation>{p("boxLanguage.caption")}</Cation>
      </Box>
      <ListLanguages>
        {languagesList.length === 0 && <Text>{p("boxLanguage.noFound")}</Text>}
        {languagesList.map((item: LanguageProps) => (
          <ListItemLanguage button onClick={() => changeLanguageHandler(item.abbr)} key={uuid()}>
            <ListItemText primary={item.language} />
            {!!defaultLang && defaultLang === item.abbr && (
              <ListItemSecondaryAction>
                <Check style={{ color: "green" }} />
              </ListItemSecondaryAction>
            )}
          </ListItemLanguage>
        ))}
      </ListLanguages>
      <LanguageInformation>{t("languageLegalInformatinoMessage")}</LanguageInformation>
    </Box>
  );
}
