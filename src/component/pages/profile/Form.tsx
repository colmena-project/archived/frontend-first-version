/* eslint-disable camelcase */
import React, { useState, useEffect } from "react";
import { useTranslation } from "next-i18next";
import Button from "@/components/ui/Button";
import ButtonV2 from "@/components/ui/ButtonV2";
import { toast } from "@/utils/notifications";
import { SelectVariantEnum, ButtonVariantEnum } from "@/enums/index";
import { makeStyles } from "@material-ui/core/styles";
import { Formik, Form, Field, FieldProps } from "formik";
import Divider from "@/components/ui/Divider";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import * as Yup from "yup";
import TextField from "@material-ui/core/TextField";
import { useSelector, useDispatch } from "react-redux";
import { PropsUserSelector } from "@/types/index";
import ResetPasswordModal from "./ResetPasswordModal";
import { update as updateUser } from "@/services/internal/users";
import BackdropModal from "@/components/ui/Backdrop";
import { currentDirection } from "@/utils/i18n";
import Box from "@material-ui/core/Box";
import { userInfoUpdate } from "@/store/actions/users";
import theme from "@/styles/theme";

type MyFormValues = {
  user_name: string;
  emlUser: string;
  lastname: string;
};

const useStyles = makeStyles((theme) => ({
  buttonsContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  resetButton: {
    color: theme.palette.variation1.main,
  },
  form: {
    margin: "auto",
  },
}));

export default function FormProfile() {
  const [language, setLanguage] = useState("");
  const { t } = useTranslation("profile");
  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const dispatch = useDispatch();
  const classes = useStyles();
  const [openResetPasswordModal, setOpenResetPasswordModal] = useState(false);
  const [showBackdrop, setShowBackdrop] = useState(false);

  const ValidationSchema = Yup.object().shape({
    user_name: Yup.string().required(c("form.requiredTitle")),
    emlUser: Yup.string().email(c("form.invalidEmailTitle")).required(c("form.requiredTitle")),
    lastname: Yup.string().required(c("form.requiredTitle")),
  });
  useEffect(() => {
    setLanguage(currentDirection());
  }, [language]);
  const initialValues: MyFormValues = {
    user_name: userRdx.user.name.split(" ")[0],
    emlUser: userRdx.user.email,
    lastname: userRdx.user.name
      .split(" ")
      .filter((_, idx) => idx !== 0)
      .join(" "),
  };

  const handleCloseResetPasswordModal = () => {
    setOpenResetPasswordModal(false);
  };

  return (
    <>
      {showBackdrop && <BackdropModal open={showBackdrop} />}
      <Formik
        initialValues={initialValues}
        validationSchema={ValidationSchema}
        onSubmit={(values: MyFormValues, { setSubmitting }: any) => {
          const { user_name, lastname, emlUser: email } = values;
          const fullnameRdx = userRdx.user.name;
          const emailRdx = userRdx.user.email;
          const fullname = `${user_name} ${lastname}`;
          setShowBackdrop(true);
          (async () => {
            try {
              const updatedName = fullname !== fullnameRdx;
              const updatedEmail = emailRdx !== email;

              const response = await updateUser({
                user: {
                  ...userRdx.user,
                  name: fullname,
                  email,
                },
                updatedName,
                updatedEmail,
              });
              const userUpdated = response.data.data.user;
              if (response.data.success) {
                if (!response.data.data.alreadyUpdated) {
                  toast(c("form.successMessageFormSave"), "success");
                  dispatch(userInfoUpdate({ ...userUpdated }));
                } else toast(c("form.dataAlreadyUpdated"), "success");
              } else {
                toast(response.data.data, "error");
              }
            } catch (e) {
              console.log(e);
              const msg = e.message ? e.message : c("messages.unableToUpdatePassword");
              toast(msg, "error");
            } finally {
              setShowBackdrop(false);
              setSubmitting(false);
            }
          })();
        }}
      >
        {({ submitForm, isSubmitting, errors, touched }: any) => (
          <Form autoComplete="off" className={classes.form}>
            <Field name="user_name" data-testid="first-name-field" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <TextField
                  id="user_name"
                  inputProps={{
                    autoComplete: "off",
                    form: {
                      autoComplete: "off",
                    },
                  }}
                  label={t("nameField")}
                  variant={SelectVariantEnum.OUTLINED}
                  required
                  fullWidth
                  {...field}
                />
              )}
            </Field>
            {errors.user_name && touched.user_name ? (
              <ErrorMessageForm message={errors.user_name} />
            ) : null}
            <Divider marginTop={20} />
            <Field
              name="lastname"
              data-testid="first-lastname-field"
              InputProps={{ notched: true }}
            >
              {({ field }: FieldProps) => (
                <TextField
                  id="lastname"
                  inputProps={{
                    autoComplete: "off",
                    form: {
                      autoComplete: "off",
                    },
                  }}
                  label={t("lastnameField")}
                  variant={SelectVariantEnum.OUTLINED}
                  required
                  fullWidth
                  {...field}
                />
              )}
            </Field>
            {errors.lastname && touched.lastname ? (
              <ErrorMessageForm message={errors.lastname} />
            ) : null}
            <Divider marginTop={20} />
            <Field name="emlUser" data-testid="email-field" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <TextField
                  id="emlUser"
                  type="email"
                  label={t("emailField")}
                  variant={SelectVariantEnum.OUTLINED}
                  required
                  inputProps={{
                    autoComplete: "off",
                    form: {
                      autoComplete: "off",
                    },
                  }}
                  fullWidth
                  {...field}
                />
              )}
            </Field>
            {errors.emlUser && touched.emlUser ? (
              <ErrorMessageForm message={errors.emlUser} />
            ) : null}
            <Divider marginTop={20} />
            <Box className={classes.buttonsContainer}>
              <Button
                title={t("saveButton")}
                data-testid="submit-profile-form"
                disabled={isSubmitting}
                handleClick={submitForm}
              />
            </Box>
          </Form>
        )}
      </Formik>
      <ButtonV2
        variant={ButtonVariantEnum.TEXT}
        color={theme.palette.secondary.main}
        title={t("resetPasswordButton")}
        data-testid="reset-password-button"
        handleClick={() => setOpenResetPasswordModal(true)}
      />
      {openResetPasswordModal && (
        <ResetPasswordModal
          open={openResetPasswordModal}
          handleClose={handleCloseResetPasswordModal}
        />
      )}
    </>
  );
}
