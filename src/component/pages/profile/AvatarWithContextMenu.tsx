/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useCallback, useRef, useState } from "react";
import Backdrop from "@/components/ui/Backdrop";
import { PropsUserSelector } from "@/types/index";
import { useSelector } from "react-redux";
import Box from "@material-ui/core/Box";
import Avatar from "./Avatar";
import { useTranslation } from "next-i18next";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ContextMenuItem from "@/components/ui/ContextMenuItem";
import { v4 as uuid } from "uuid";
import { toast } from "@/utils/notifications";
import Slider from "@material-ui/core/Slider";
import Cropper from "react-easy-crop";
import Modal from "@/components/ui/Modal";
import { ButtonVariantEnum } from "@/enums/*";
import Button from "@/components/ui/Button";
import Loading from "@/components/ui/Loading";
import { isPNGImage, isJPGImage } from "@/utils/utils";
import { currentDirection } from "@/utils/i18n";
import { removeFile, uploadFileToCMS } from "@/services/cms/file";
import { useMutation } from "@apollo/client";
import { CREATE_COLLABORATOR, UPDATE_COLLABORATOR } from "@/services/cms/collaborators";
import useCollaborator from "@/hooks/cms/useCollaborator";
import getCroppedImg from "@/utils/cropImage";

type Props = {
  size: number;
  showEditImage?: boolean;
  className?: string;
};

type PositionProps = {
  mouseX: null | number;
  mouseY: null | number;
};

type CroppedAreaProps = {
  width: number;
  height: number;
  x: number;
  y: number;
};

type FileProps = {
  bits: string;
  name: string;
  type: string;
};

function AvatarChangePicture({ size, showEditImage = true, className }: Props) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const formRef = useRef(null);
  const inputFileRef = useRef(null);
  const [showBackdrop, setShowBackdrop] = useState(false);
  const [file, setFile] = useState<FileProps>({
    bits: "",
    name: "",
    type: "",
  });
  const [showModal, setShowModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const { t: c } = useTranslation("common");
  const [position, setPosition] = useState<PositionProps>({
    mouseX: null,
    mouseY: null,
  });
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState<CroppedAreaProps>({
    width: 0,
    height: 0,
    x: 0,
    y: 0,
  });
  const { collaborator, loading, refetch } = useCollaborator(userRdx.user.id);
  const [createCollaboratorAvatar] = useMutation(CREATE_COLLABORATOR);
  const [updateCollaboratorAvatar] = useMutation(UPDATE_COLLABORATOR);

  const handleOpenContextMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
    setPosition({
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4,
    });
  };

  const handleCloseContextMenu = () => {
    setAnchorEl(null);
  };

  const handleCloseModal = () => {
    setFile({} as FileProps);
    setShowModal(false);
  };

  const featureUnavailable = () => {
    toast(c("featureUnavailable"), "warning");
  };

  const onSelectFile = (e: any) => {
    if (e.target.files && e.target.files.length > 0) {
      const { type, size, name } = e.target.files[0];

      if (!isPNGImage(type) && !isJPGImage(type)) {
        toast(c("uploadRuleFileType"), "warning");
        return;
      }

      // 20MB limite do NC
      if (size > 20971520) {
        toast(c("uploadRuleFileSize"), "warning");
        return;
      }

      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setFile({
          bits: String(reader.result),
          name,
          type,
        });
      });

      setShowModal(true);
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const uploadThumb = useCallback(
    async (image: string, fileName: string, type: string) => {
      const croppedImage = await getCroppedImg(image, croppedAreaPixels, 0);
      const file = new File([croppedImage], fileName, { type });

      const formData = new FormData();
      formData.append("files", file);

      const [response] = await uploadFileToCMS(formData);

      return response.id;
    },
    [croppedAreaPixels],
  );

  const submitCrop = async () => {
    if (!file.bits) return;

    try {
      setShowBackdrop(true);
      const avatar = await uploadThumb(file.bits, file.name, file.type);
      const username = userRdx.user.id;
      if (collaborator) {
        if (collaborator.attributes.avatar.data?.id) {
          await removeFile(collaborator.attributes.avatar.data.id);
        }

        await updateCollaboratorAvatar({
          variables: { id: collaborator.id, username, avatar },
        });
      } else {
        const collaboratorCreated = await createCollaboratorAvatar({
          variables: { username, avatar },
        });
        if (!collaboratorCreated) {
          throw new Error();
        }

        await refetch(collaboratorCreated.data.createCollaborator.data);
      }

      toast(c("uploadUserAvatarSuccessfully"), "success");
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "warning");
    } finally {
      setShowBackdrop(false);
      setShowModal(false);
    }
  };

  const onBtnClick = () => {
    handleCloseContextMenu();
    if (!inputFileRef || !inputFileRef.current) return;
    if (!formRef || !formRef.current) return;

    const input: HTMLInputElement = inputFileRef.current;
    const form: HTMLFormElement = formRef.current;

    form.reset();
    const clk: { click: () => void } = input;

    clk.click();
  };

  const onCropChange = (crop: { x: number; y: number }) => {
    setCrop(crop);
  };

  const onCropComplete = (_: any, croppedAreaPixels: CroppedAreaProps) => {
    setCroppedAreaPixels(croppedAreaPixels);
  };

  const onZoomChange = (zoom: number) => {
    setZoom(zoom);
  };

  return (
    <>
      <Backdrop open={showBackdrop} />
      <form ref={formRef}>
        <input type="file" ref={inputFileRef} onChange={onSelectFile} style={{ display: "none" }} />
      </form>
      <Box
        onClick={handleOpenContextMenu}
        data-testid="avatar-open-context"
        style={{ cursor: "pointer" }}
      >
        {loading ? (
          <Loading />
        ) : (
          <Avatar size={size} showEditImage={showEditImage} className={className} />
        )}
      </Box>
      <Box display="flex" justifyContent="flex-end">
        <Menu
          dir={currentDirection()}
          key={uuid()}
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          anchorReference="anchorPosition"
          anchorPosition={
            position.mouseY !== null && position.mouseX !== null
              ? { top: position.mouseY, left: position.mouseX }
              : undefined
          }
          onClose={handleCloseContextMenu}
        >
          <MenuItem key="upload" data-testid="upload-menu-option" onClick={onBtnClick}>
            <ContextMenuItem
              icon="camera"
              title={c("contextMenuUserAvatar.upload")}
              className="m-1.5"
            />
          </MenuItem>
          <MenuItem
            key="remove"
            data-testid="remove-menu-option"
            onClick={featureUnavailable}
            style={{ color: "#ff6347" }}
          >
            <ContextMenuItem
              className="m-1.5"
              icon="trash"
              iconColor="#ff6347"
              title={c("contextMenuUserAvatar.remove")}
            />
          </MenuItem>
        </Menu>
      </Box>

      <Modal
        open={showModal}
        handleClose={handleCloseModal}
        actions={
          <Box display="flex" flex="1" flexDirection="row" justifyContent="space-between">
            <Button
              handleClick={handleCloseModal}
              style={{ margin: 8 }}
              variant={ButtonVariantEnum.OUTLINED}
              title={c("form.cancelButton")}
              data-testid="close-modal-crop"
            />
            <Button
              handleClick={submitCrop}
              style={{ margin: 8 }}
              variant={ButtonVariantEnum.CONTAINED}
              title={c("form.submitSaveTitle")}
              data-testid="submit-modal-crop"
            />
          </Box>
        }
      >
        <Box>
          <Box width="100%" height={280}>
            <Cropper
              image={file.bits}
              crop={crop}
              zoom={zoom}
              aspect={4 / 3}
              cropShape="round"
              showGrid
              style={{
                containerStyle: { width: "100%", height: 300 },
                cropAreaStyle: { width: 250, height: 250 },
                // mediaStyle: { width: "100%", height: "100%", margin: "0 auto" },
              }}
              cropSize={{ width: 250, height: 250 }}
              onCropChange={onCropChange}
              onCropComplete={onCropComplete}
              onZoomChange={onZoomChange}
              objectFit="vertical-cover"
            />
          </Box>
          <Box height={30}>
            <Slider
              data-testid="slider-modal-crop"
              value={zoom}
              min={1}
              max={3}
              step={0.1}
              aria-labelledby="Zoom"
              onChange={(e, zoom: number) => onZoomChange(zoom)}
              style={{ paddingTop: 22, paddingBottom: 22, paddingLeft: 0, paddingRight: 0 }}
            />
          </Box>
        </Box>
      </Modal>
    </>
  );
}

export default AvatarChangePicture;
