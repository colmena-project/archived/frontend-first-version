import { useEffect, useState } from "react";
import { useTranslation } from "next-i18next";

import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import NotificationsIcon from "@material-ui/icons/Notifications";
import LanguageIcon from "@material-ui/icons/Language";
import { parseCookies } from "nookies";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { currentDirection } from "@/utils/i18n";
import theme from "@/styles/theme";
import getDescriptionCurrentLanguage from "@/utils/getDescriptionCurrentLanguage";
import Security from "@material-ui/icons/Security";
import SvgIconAux from "@/components/ui/SvgIcon";
import { toast } from "@/utils/notifications";
import { PageOptionsForSettingsEnum } from "@/enums/*";
import { PossibleOptionsInProfileProps } from "@/types/*";
import { ListCloudAndStorageOptions as WrapperList } from "./styled";

interface ListOptionsSettingsInterface {
  logoutHandler: () => void;
  changePage: (page: PossibleOptionsInProfileProps) => void;
}

export default function ListOptionsSettings({
  logoutHandler,
  changePage,
}: ListOptionsSettingsInterface) {
  const cookies = parseCookies();
  const { t } = useTranslation("settings");
  const { t: c } = useTranslation("common");
  const { t: d } = useTranslation("drawer");
  const [language, setLanguage] = useState("");

  useEffect(() => {
    setLanguage(currentDirection());
  }, [language]);
  const unavailable = () => {
    toast(c("featureUnavailable"), "warning");
  };

  const textAlign = language === "rtl" ? "right" : "left";

  return (
    <WrapperList style={{ textAlign }}>
      <ListItem divider button onClick={() => changePage(PageOptionsForSettingsEnum.LANGUAGE)}>
        <ListItemIcon>
          <LanguageIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText
          id="change-profile-info"
          primary={t("changeLangTitle")}
          secondary={getDescriptionCurrentLanguage(c, cookies.NEXT_LOCALE)}
        />
        <ListItemSecondaryAction>
          <ArrowForwardIosIcon fontSize="small" style={{ color: theme.palette.icon.dark }} />
        </ListItemSecondaryAction>
      </ListItem>
      <ListItem divider button onClick={unavailable}>
        <ListItemIcon>
          <SvgIconAux icon="contrast" htmlColor="#757575" fontSize={23} />
        </ListItemIcon>
        <ListItemText id="change-profile-info" primary={t("appearanceAndAccessibilityTitle")} />
        <ListItemSecondaryAction>
          <ArrowForwardIosIcon fontSize="small" style={{ color: theme.palette.icon.dark }} />
        </ListItemSecondaryAction>
      </ListItem>
      <ListItem divider button onClick={unavailable}>
        <ListItemIcon>
          <Security fontSize="small" />
        </ListItemIcon>
        <ListItemText id="change-profile-info" primary={t("privacyAndSecurityTitle")} />
        <ListItemSecondaryAction>
          <ArrowForwardIosIcon fontSize="small" style={{ color: theme.palette.icon.dark }} />
        </ListItemSecondaryAction>
      </ListItem>
      <ListItem divider button onClick={unavailable}>
        <ListItemIcon>
          <NotificationsIcon />
        </ListItemIcon>
        <ListItemText id="notifications" primary={t("notificationTitle")} />
        <ListItemSecondaryAction>
          <ArrowForwardIosIcon fontSize="small" style={{ color: theme.palette.icon.dark }} />
        </ListItemSecondaryAction>
      </ListItem>
      <ListItem
        divider
        button
        onClick={() => changePage(PageOptionsForSettingsEnum.CLOUD_AND_STORAGE)}
      >
        <ListItemIcon>
          <SvgIconAux icon="storage_solid" htmlColor="#757575" fontSize={23} />
        </ListItemIcon>
        <ListItemText id="change-profile-info" primary={t("cloudAndStorageOptionsTitle")} />
        <ListItemSecondaryAction>
          <ArrowForwardIosIcon fontSize="small" style={{ color: theme.palette.icon.dark }} />
        </ListItemSecondaryAction>
      </ListItem>
      <ListItem button onClick={logoutHandler}>
        <ListItemIcon>
          <ExitToAppIcon fontSize="small" style={{ color: "tomato" }} />
        </ListItemIcon>
        <ListItemText
          id="switch-list-label-bluetooth"
          primaryTypographyProps={{ style: { color: "tomato" } }}
          primary={d("logoutTitle")}
        />
      </ListItem>
    </WrapperList>
  );
}
