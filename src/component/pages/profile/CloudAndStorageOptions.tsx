import React from "react";
import { useTranslation } from "next-i18next";
import { Box, useTheme } from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import Backdrop from "@/components/ui/Backdrop";
import { PossibleOptionsInProfileProps } from "@/types/*";
import ListItemText from "@material-ui/core/ListItemText";
import BackToSettingsMenu from "./BackToSettingsMenu";
import { PageOptionsForSettingsEnum } from "@/enums/*";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import SvgIcon from "@/components/ui/SvgIcon";
import ActionConfirm from "@/components/ui/ActionConfirm";
import { useCloudAndStorageOptions } from "@/hooks/useCloudAndStorageOptions";
import { ListCloudAndStorageOptions } from "./styled";
import StorageBox from "./StorageBox";
import useLocalStorageBox from "@/hooks/useLocalStorageBox";
import { getUserInfo } from "@/services/ocs/users";

import StorageBoxSkeleton from "@/components/ui/skeleton/StorageBox";
import useSWR from "swr";

interface ChangeLanguageInterface {
  changePage: (page: PossibleOptionsInProfileProps) => void;
}

export default function CloudAndStorageOptions({ changePage }: ChangeLanguageInterface) {
  const theme = useTheme();
  const { t: p } = useTranslation("profile");
  const { t } = useTranslation("settings");
  const { quota: localQuota, usage: localUsage } = useLocalStorageBox();
  const {
    isOpen,
    showBackdrop,
    handleCleanLocalStorage,
    confirmCleanLocalStorage,
    onClose,
    isLoading,
  } = useCloudAndStorageOptions();

  const { data } = useSWR("/user", getUserInfo);
  const quota = data?.data?.ocs.data.quota;

  const renderLocalStorageBox =
    localQuota && localUsage === undefined ? (
      <StorageBoxSkeleton />
    ) : (
      <StorageBox
        quota={localQuota}
        usage={localUsage}
        title={p("storage.local.title")}
        footer={p("storage.local.footer")}
        showBrowser
      />
    );

  const renderCloudStorageBox = !data ? (
    <StorageBoxSkeleton />
  ) : (
    <StorageBox quota={quota?.total} usage={quota?.used} title={p("storage.cloud.title")} />
  );

  return (
    <Box>
      <Backdrop open={showBackdrop} />
      <BackToSettingsMenu
        data-testid="back-to-settings-menu"
        action={() => changePage(PageOptionsForSettingsEnum.INITIAL)}
        titlePage={t("cloudAndStorageOptionsTitle")}
      />
      <ListCloudAndStorageOptions>
        <ListItem divider button onClick={() => handleCleanLocalStorage()}>
          <SvgIcon icon="clean" />
          <ListItemText
            data-testid="change-profile-info"
            id="change-profile-info"
            primary={t("cloudAndStorageOptions.storage.primary")}
            secondary={t("cloudAndStorageOptions.storage.secondary")}
          />
          <ListItemSecondaryAction>
            <ArrowForwardIosIcon fontSize="small" style={{ color: theme.palette.icon.dark }} />
          </ListItemSecondaryAction>
        </ListItem>
      </ListCloudAndStorageOptions>
      {renderLocalStorageBox}
      {renderCloudStorageBox}
      {isOpen && (
        <ActionConfirm
          open={isOpen}
          data-testid="clear-item-confirm"
          onOk={() => confirmCleanLocalStorage()}
          onClose={() => onClose()}
          isLoading={isLoading}
          showMessage={false}
          title={t("confirmAction")}
        />
      )}
    </Box>
  );
}
