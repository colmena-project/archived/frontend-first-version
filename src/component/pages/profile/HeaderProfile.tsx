import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import UserAvatar from "@/components/pages/profile/AvatarWithContextMenu";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/index";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "next-i18next";
import IconButton from "@/components/ui/IconButton";
import { isSubadminProfile } from "@/utils/permissions";
import ProfileInformationModal from "./ProfileInformationModal";

const useStyles = makeStyles((theme) => ({
  header: {
    padding: "11px 20px",
    background: "url(/images/svg/bg-home.svg) repeat-y right",
    backgroundColor: theme.palette.secondary.main,
    width: "100%",
    marginBottom: 0,
    display: "flex",
    gap: 16,
    position: "relative",
  },
  userName: {
    color: theme.palette.secondary.contrastText,
    fontSize: 16,
    textAlign: "left",
    fontWeight: "bold",
    letterSpacing: 1,
  },
  userMedia: {
    fontSize: 14,
    fontWeight: 400,
    color: theme.palette.secondary.contrastText,
    textAlign: "left",
  },
  userRoleBox: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  userRole: {
    fontSize: 12,
    fontWeight: 400,
    fontStyle: "italic",
    margin: 0,
    color: theme.palette.secondary.contrastText,
    textAlign: "left",
  },
  userRoleInfo: {
    padding: 0,
    minWidth: "auto",
    marginLeft: 4,
  },
  avatarBox: {
    position: "absolute",
    bottom: -10,
  },
  avatar: {
    borderWidth: 2,
    borderStyle: "solid",
    borderColor: theme.palette.common.white,
  },
  emptyBox: {
    height: 2,
    width: 80,
  },
}));

export default function HeaderProfile() {
  const [openProfileInformation, setOpenProfileInformation] = useState(false);
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { t: c } = useTranslation("common");

  const group = userRdx.user.media.name;

  const handleProfileInformation = () => {
    setOpenProfileInformation(false);
  };

  return (
    <Box className={classes.header} data-testid="header-profile">
      <ProfileInformationModal open={openProfileInformation} onClose={handleProfileInformation} />
      <Box className={classes.emptyBox} />
      <Box className={classes.avatarBox}>
        <UserAvatar
          data-testid="header-profile-avatar"
          size={10}
          showEditImage
          className={classes.avatar}
        />
      </Box>

      <Box>
        <Text
          variant={TextVariantEnum.BODY1}
          className={classes.userName}
          data-testid="header-profile-user-name"
        >
          {userRdx && userRdx.user.name}
        </Text>
        <Text
          variant={TextVariantEnum.BODY2}
          className={classes.userMedia}
          data-testid="header-profile-user-media"
        >
          {group}
        </Text>
        <Box className={classes.userRoleBox}>
          <Text
            variant={TextVariantEnum.BODY2}
            className={classes.userRole}
            data-testid="header-profile-user-role"
          >
            {isSubadminProfile() ? c("administratorTitle") : c("collaboratorTitle")}
          </Text>

          <IconButton
            handleClick={() => setOpenProfileInformation(true)}
            icon="info_circle"
            fontSizeIcon="small"
            iconColor="#fff"
            className={classes.userRoleInfo}
          />
        </Box>
      </Box>
    </Box>
  );
}
