import React, { useState } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useTranslation } from "next-i18next";
import SettingsOutlinedIcon from "@material-ui/icons/SettingsOutlined";
import PersonOutlineOutlinedIcon from "@material-ui/icons/PersonOutlineOutlined";
import { makeStyles, withStyles } from "@material-ui/core/styles";

import FlexBox from "@/components/ui/FlexBox";
import { JustifyContentEnum } from "@/enums/index";
import Form from "@/components/pages/profile/Form";
import SettingsList from "@/components/pages/profile/SettingsList";

import WhiteSpaceFooter from "@/components/ui/WhiteSpaceFooter";
import { Box } from "@material-ui/core";

function a11yProps(index: number) {
  return {
    id: `profile-tab-${index}`,
    "aria-controls": `profile-tab-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  tabsWrapper: {
    width: "100%",
    background: theme.palette.primary.contrastText,
  },
}));

const useTabStyles = makeStyles((theme) => ({
  root: {
    textTransform: "none",
    color: theme.palette.gray.main,
    opacity: 1,
    fontSize: "1rem",
    "&:focus": {
      opacity: 1,
      color: theme.palette.primary.main,
    },
  },
  wrapper: {
    display: "flex",
    flexDirection: "row",
    gap: 10,
  },
  selected: { color: theme.palette.primary.main },
  labelIcon: {
    "& .MuiTab-wrapper > *:first-child": {
      margin: 0,
    },
  },
}));

interface StyledTabsProps {
  value: number;
  onChange: (event: React.ChangeEvent, newValue: number) => void;
  icon?: string | React.ReactElement<any, string | React.JSXElementConstructor<any>> | undefined;
  variant?: "scrollable" | "standard" | "fullWidth" | undefined;
}

const StyledTabs = withStyles((theme) => ({
  indicator: {
    height: 2,
    backgroundColor: theme.palette.primary.main,
  },
  root: {
    backgroundColor: theme.palette.background.default,
  },
}))((props: StyledTabsProps) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const ProfileTabs = () => {
  const [activeTab, setActiveTab] = useState(0);

  const { t } = useTranslation("profile");
  const classes = useStyles();
  const classesTab = useTabStyles();

  const onChangeTabs = (_: React.ChangeEvent, newValue: number) => {
    setActiveTab(newValue);
  };

  return (
    <Box component="section" className={classes.tabsWrapper}>
      <StyledTabs variant="fullWidth" value={activeTab} onChange={onChangeTabs}>
        <Tab
          icon={
            <PersonOutlineOutlinedIcon scale={20} color={activeTab === 0 ? "primary" : "inherit"} />
          }
          label={t("tabs.profile")}
          {...a11yProps(0)}
          classes={classesTab}
        />
        <Tab
          icon={<SettingsOutlinedIcon scale={20} color={activeTab === 1 ? "primary" : "inherit"} />}
          label={t("tabs.settings")}
          {...a11yProps(1)}
          classes={classesTab}
        />
      </StyledTabs>

      {activeTab === 0 && (
        <FlexBox
          justifyContent={JustifyContentEnum.FLEXSTART}
          extraStyle={{ paddingTop: 25, marginTop: 0 }}
        >
          <Box component="section" style={{ width: "90%" }}>
            <Form />
          </Box>
          <WhiteSpaceFooter />
        </FlexBox>
      )}

      {activeTab === 1 && (
        <FlexBox
          justifyContent={JustifyContentEnum.FLEXSTART}
          extraStyle={{ paddingTop: 0, marginTop: 0 }}
        >
          <Box component="section" style={{ width: "90%" }}>
            <SettingsList />
          </Box>
          <WhiteSpaceFooter />
        </FlexBox>
      )}
    </Box>
  );
};

export default ProfileTabs;
