import { styled } from "@material-ui/core/styles";
import Text from "@/components/ui/Text";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Box } from "@material-ui/core";
import LinearProgress from "@material-ui/core/LinearProgress";

export const Footer = styled(Text)({
  fontStyle: "normal",
  fontWeight: 400,
  fontSize: "9px",
  lineHeight: "12px",
  display: "flex",
  alignItems: "center",
  letterSpacing: "0.15px",
  color: "#8E8E8E",
});

export const DisplayBrowser = styled(Text)(({ theme }) => ({
  fontStyle: "normal",
  fontWeight: 700,
  fontSize: "9px",
  lineHeight: "24px",
  letterSpacing: "0.15px",
  color: theme.palette.variation1.main,
}));

export const ProgressContantStorageBox = styled(LinearProgress)(({ theme }) => ({
  height: "15px",
  background: "#E2E3E9",

  "& .MuiLinearProgress-barColorPrimary": {
    background: theme.palette.variation3.main,
  },
}));

export const TitleContantStorageBox = styled(Text)({
  fontStyle: "normal",
  fontWeight: 400,
  fontSize: "12px",
  lineHeight: "24px",
  letterSpacing: "0.15px",
  color: "#4C517F",
});

export const ContantStorageBox = styled(Box)({
  background: "#FAFAFA",
  border: "1px solid #E9E9E9",
  borderRadius: "7px",
  padding: "10px",
});

export const ContainerStorageBox = styled(Box)({
  margin: "20px 0 40px 0",
  textAlign: "left",
});

export const TitleStorageBox = styled(Text)({
  fontStyle: "normal",
  fontWeight: 700,
  fontSize: "10px",
  lineHeight: "0px",
  marginBottom: "10px",
  color: "#343A40",
});

export const Wrapper = styled(Box)({
  width: "100%",
});

export const Display = styled(Text)({
  fontSize: "15px",
  fontWeight: 900,
});

export const Cation = styled(Text)({
  fontWeight: "bold",
});

export const ListCloudAndStorageOptions = styled(List)(({ theme }) => ({
  width: "100%",
  textAlign: "left",
  "& .MuiListItem-root": {
    backgroundColor: theme.palette.background.paper,
  },
}));

export const InputSearch = styled("input")(({ theme }) => ({
  backgroundColor: "#F9F9F9",
  border: `1px solid ${theme.palette.grey[300]}`,
  borderRadius: "7px",
  width: "100%",
  height: "50px",
  textAlign: "center",
}));

export const ListLanguages = styled(List)({
  border: "1px solid #E9E9E9",
  borderRadius: "10px",
  padding: 0,
});

export const ListItemLanguage = styled(ListItem)({
  backgroundColor: "#FAFAFA",
  borderBottom: "1px solid #E9E9E9",
});

export const LanguageInformation = styled(Text)({
  fontWeight: 500,
  textAlign: "justify",
  marginTop: 15,
});
