import { useTranslation } from "next-i18next";
import Modal from "@/components/ui/Modal";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { makeStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { isSubadminProfile } from "@/utils/permissions";
import { RoleUserEnum } from "@/enums/*";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  title: {
    marginBottom: theme.spacing(2),
    color: theme.palette.icon.main,
  },
  list: {},
  item: {
    color: theme.palette.gray.main,
    padding: "0",
  },
}));

type Props = {
  open: boolean;
  onClose: () => void;
};

export default function ProfileInformationModal({ open, onClose }: Props) {
  const { t } = useTranslation("profile");
  const classes = useStyles();

  const getProfileType = isSubadminProfile() ? RoleUserEnum.ADMIN : RoleUserEnum.COLLABORATOR;

  const rules: string[] = t(`${getProfileType}.rules`, { returnObjects: true });

  return (
    <Modal open={open} handleClose={onClose}>
      <Box>
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
        <Text className={classes.title}>{t("descriptionModal", { profile: getProfileType })}</Text>
        <List className={classes.list}>
          {rules.map((rule) => (
            <ListItem key={rule} className={classes.item}>
              - {rule}
            </ListItem>
          ))}
        </List>
      </Box>
    </Modal>
  );
}
