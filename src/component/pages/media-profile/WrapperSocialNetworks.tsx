import { useState } from "react";
import { Box, FormLabel } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useTranslation } from "next-i18next";
import SvgIconAux from "@/components/ui/SvgIcon";
import SocialMediaList from "./SocialMedia";
import Create from "./SocialMedia/create";
import { isSubadminProfile } from "@/utils/permissions";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    position: "relative",
    marginTop: "20px",
    border: `1px solid ${theme.palette.variation5.light}`,
    padding: "18.5px 14px",
    borderRadius: "5px",
    textAlign: "start",
    height: "100px",
    "&:hover": {
      border: "1px solid #212121",
    },
  },
  guide: {
    display: "flex",
    gap: "10px",
    position: "absolute",
    transform: "translate(-22px, -30px) scale(0.75)",
    background: theme.palette.primary.contrastText,
    padding: "2px 8px",
  },
  action: {
    cursor: "pointer",
  },

  content: {},
}));

function WrapperSocialNetworks() {
  const theme = useTheme();
  const { t } = useTranslation("mediaProfile");
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Box className={classes.guide}>
        <FormLabel>{t("editForm.social")}</FormLabel>
        {isSubadminProfile() && (
          <SvgIconAux
            className={classes.action}
            onClick={() => setOpenCreateModal(true)}
            icon="add_circle"
            htmlColor={theme.palette.variation1.main}
            fontSize="small"
          />
        )}
      </Box>
      <Box className={classes.content}>
        <SocialMediaList />
      </Box>
      <Create
        title={t("addSocialMediaTitle")}
        open={openCreateModal}
        handleClose={() => setOpenCreateModal(false)}
      />
    </Box>
  );
}

export default WrapperSocialNetworks;
