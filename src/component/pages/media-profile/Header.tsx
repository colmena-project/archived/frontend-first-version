import React from "react";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { ButtonVariantEnum } from "@/enums/*";
// import IconButton from "@/components/ui/IconButton";
// import { useTranslation } from "next-i18next";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/index";
import { isSubadminProfile } from "@/utils/permissions";
import Button from "@/components/ui/Button";
import AvatarWithContextMenu from "@/components/pages/media-profile/AvatarWithContextMenu";
import { makeStyles } from "@material-ui/core/styles";
import SvgIcon from "@/components/ui/SvgIcon";

const useStyles = makeStyles(() => ({
  container: {
    padding: "10px",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    background: "url(/images/svg/bg-media-profile.svg) repeat-y right",
    backgroundColor: "#F65B3A",
    width: "100%",
    marginBottom: 30,
  },
  containerMedia: {
    display: "flex",
    flexDirection: "column",
    marginLeft: "20px",
    flex: 1,
    justifyContent: "flex-start",
  },
  containerMediaUrl: {
    display: "flex",
    marginTop: 5,
    flexDirection: "row",
    alignItems: "center",
  },
  mediaTitle: {
    color: "#fff",
    fontSize: 16,
    fontWeight: 600,
    textAlign: "left",
  },
  mediaSubtitle: {
    color: "#fff",
    fontSize: 14,
    textAlign: "left",
  },
  mediaUrl: {
    color: "#fff",
    fontSize: 15,
    marginLeft: 0,
    paddingLeft: 5,
    fontWeight: 600,
  },
  avatar: {
    marginBottom: -20,
  },
}));

export default function HeaderMediaProfile() {
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  return (
    <Box className={classes.container}>
      <Box className={classes.avatar}>
        <AvatarWithContextMenu size={14} showEditImage={isSubadminProfile()} />
      </Box>
      <Box className={classes.containerMedia}>
        <Text className={classes.mediaTitle}>{userRdx.user.media?.name}</Text>
        <Text className={classes.mediaSubtitle}>{userRdx.user.media?.slogan}</Text>
        <Box className={classes.containerMediaUrl}>
          <SvgIcon icon="global" htmlColor="#fff" fontSize={18} />
          <Button
            component="a"
            className={classes.mediaUrl}
            title={userRdx.user.media?.url}
            target="blank"
            url={userRdx.user.media?.url}
            variant={ButtonVariantEnum.TEXT}
          />
        </Box>
      </Box>
    </Box>
  );
}
