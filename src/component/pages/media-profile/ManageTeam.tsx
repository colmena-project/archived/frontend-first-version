import { useState } from "react";
import BackToSettingsMenu from "../profile/BackToSettingsMenu";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { PossibleOptionsInMediaProfileProps, PropsUserSelector } from "@/types/*";
import Text from "@/components/ui/Text";
import { useTranslation } from "next-i18next";
import SvgIconAux from "@/components/ui/SvgIcon";
import { useSelector } from "react-redux";
import { v4 as uuid } from "uuid";
import Invite from "@/components/pages/media-profile/Members/Invite";
import { members as getMembers } from "@/services/internal/groups";
import MembersSkeleton from "@/components/ui/skeleton/MembersList";
import MemberItem from "./Members/MemberItem";
import { RoleUserEnum } from "@/enums/*";
import Clickable from "@/components/ui/Clickable";
import { isSubadminProfile } from "@/utils/permissions";

interface ManageTeamInterface {
  changePage: (page: PossibleOptionsInMediaProfileProps) => void;
}

const useStyles = makeStyles((theme) => ({
  caption: {
    fontWeight: "bold",
    fontSize: "14px",
  },
  addUser: {
    cursor: "pointer",
    background: "#F1F2F2",
    borderRadius: "50%",
    padding: "10px 10px 15px",
  },
  info: {
    color: theme.palette.gray.main,
    fontWeight: 400,
    fontSize: "10px",
    lineHeight: "0px",
  },
  teamList: {
    display: "grid",
    gridTemplateColumns: "repeat(3, minmax(0, 1fr))",
    border: "1px solid #E9E9E9",
    borderRadius: "7px",
    padding: "20px",
    gap: "20px",

    "@media (min-width:1024px)": {
      gridTemplateColumns: "repeat(5, minmax(0, 1fr))",
    },
  },
}));

interface MemberInterface {
  createdTimestamp: number;
  disableableCredentialTypes: [];
  email: string;
  emailVerified: boolean;
  enabled: boolean;
  firstName: string;
  permission: RoleUserEnum.ADMIN | RoleUserEnum.COLLABORATOR;
  id: string;
  lastName: string;
  notBefore: number;
  requiredActions: [];
  totp: boolean;
  username: string;
}

export default function ManageTeam({ changePage }: ManageTeamInterface) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [openInvite, setOpenInvite] = useState(false);
  const [selectedUser, setSelectedUser] = useState<MemberInterface | null>(null);
  const { t } = useTranslation("mediaProfile");
  const classes = useStyles();

  const { data, mutate, isValidating } = getMembers(userRdx.user.media.id, {
    revalidateOnFocus: false,
  });

  const members = data?.data ?? [];

  const renderFormModal = (user: MemberInterface) => {
    setSelectedUser(user);
    setOpenInvite(true);
  };

  return (
    <Box>
      <BackToSettingsMenu
        action={() => changePage("initial")}
        titlePage={t("settingsOptions.pages.manageTeam.title")}
        elementToTight={
          isSubadminProfile() ? (
            <Clickable
              className={classes.addUser}
              handleClick={() => {
                setSelectedUser(null);
                setOpenInvite(true);
              }}
            >
              <SvgIconAux icon="add_user" fontSize="medium" htmlColor="#00AFB0" />
            </Clickable>
          ) : undefined
        }
      />
      <Box
        display="flex"
        key="container-title"
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        className="mt-5"
      >
        <Text className={classes.caption}>
          {t("settingsOptions.pages.manageTeam.label", { team: userRdx.user.media?.name })}
        </Text>
        <Text className={classes.info}>
          {t("settingsOptions.pages.manageTeam.totalRegistered", {
            total: members ? members.length : 0,
          })}
        </Text>
      </Box>
      {isValidating && <MembersSkeleton />}
      {!isValidating && members && (
        <Box className={classes.teamList} key={uuid()}>
          {members.map((member: MemberInterface) => (
            <MemberItem key={member.id} member={member} openModal={renderFormModal} />
          ))}
        </Box>
      )}
      {openInvite && (
        <Invite
          openInviteForm={openInvite}
          handleCloseInviteForm={() => setOpenInvite(false)}
          member={selectedUser}
          mutate={mutate}
          resetSelectedUser={() => setSelectedUser(null)}
        />
      )}
    </Box>
  );
}
