/* eslint-disable max-len */
/* eslint-disable camelcase */
import React, { useState } from "react";
import Button from "@/components/ui/Button";
import { TextField, Box } from "@material-ui/core";
import Select from "@/components/ui/Select";
import { useTranslation } from "next-i18next";
import { toast } from "@/utils/notifications";
import {
  SelectVariantEnum,
  RoleUserEnum,
  ButtonVariantEnum,
  CreateUserErrorEnum,
} from "@/enums/index";
import { Formik, Form, Field, FieldProps } from "formik";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import * as Yup from "yup";
import { create as createUser, updateUserMember } from "@/services/internal/users";
import Backdrop from "@/components/ui/Backdrop";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/index";
import Divider from "@/components/ui/Divider";
import Modal from "@/components/ui/Modal";
import { parseCookies } from "nookies";
import { MemberInterface } from "@/interfaces/index";
import { splitName } from "@/utils/directory";

type Props = {
  openInviteForm: boolean;
  handleCloseInviteForm: () => void;
  member?: MemberInterface | null;
  mutate: (data?: any, shouldRevalidate?: boolean | undefined) => Promise<any>;
  resetSelectedUser: () => void;
};

type MyFormValues = {
  name: string;
  emailCol: string;
  permission: RoleUserEnum.ADMIN | RoleUserEnum.COLLABORATOR;
};

export default function InviteForm({
  openInviteForm,
  handleCloseInviteForm,
  member,
  mutate,
  resetSelectedUser,
}: Props) {
  const { t } = useTranslation("mediaProfile");
  const { t: c } = useTranslation("common");
  const [showBackdrop, setShowBackdrop] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const cookies = parseCookies();
  const groupId = userRdx.user.media.id;

  const ValidationSchema = Yup.object().shape({
    name: Yup.string().required(c("form.requiredTitle")),
    emailCol: Yup.string().email(c("form.invalidEmailTitle")).required(c("form.requiredTitle")),
    permission: Yup.string().required(c("form.requiredTitle")),
  });

  const initialValues: MyFormValues = {
    name: member?.firstName ? `${member?.firstName} ${member?.lastName}` : "",
    emailCol: member?.email ?? "",
    permission:
      member?.permission === "collaborator" ? RoleUserEnum.COLLABORATOR : RoleUserEnum.ADMIN ?? "",
  };

  const showErrorNotification = (error: string) => {
    switch (error) {
      case CreateUserErrorEnum.ERROR1:
      case CreateUserErrorEnum.ERROR2:
        return t("errorMessages.error1");
      case CreateUserErrorEnum.ERROR3:
        return t("errorMessages.error2");
      default:
        return t("errorMessages.error1");
    }
  };

  const onSubmitCreate = async (values: MyFormValues, setSubmitting: any) => {
    setShowBackdrop(true);
    setSubmitting(false);
    const { name, emailCol: email, permission } = values;
    (async () => {
      try {
        const response = await createUser({
          name,
          email,
          language: cookies.NEXT_LOCALE || "en",
          role: permission,
          user: userRdx.user,
        });
        if (response.data.success) {
          handleCloseInviteForm();
          toast(t("messageOkModalDialogInvite"), "success");
          mutate(`/groups/${groupId}/members`);
        } else {
          toast(showErrorNotification(response.data.data), "warning");
        }
      } catch (e) {
        console.log(e);
        handleCloseInviteForm();
        toast(t("messageErrorModalDialogInvite"), "warning");
      } finally {
        setShowBackdrop(false);
      }
    })();
  };

  const onSubmitUpdate = async (values: MyFormValues, setSubmitting: any) => {
    setShowBackdrop(true);
    setSubmitting(false);
    const { name, emailCol: email, permission } = values;

    const updatedName = name !== `${member?.firstName} ${member?.lastName}`;
    const updatedEmail = email !== member?.email;
    const updatePermission = permission !== member?.permission;

    try {
      const response = await updateUserMember({
        user: {
          id: member?.id,
          username: member?.username,
          permission,
          firstName: splitName(name, "firstName"),
          lastName: splitName(name, "lastName"),
          email,
        },
        groupid: userRdx.user.media.name,
        updatedName,
        updatedEmail,
        updatePermission,
      });

      if (response.data.success) {
        resetSelectedUser();
        handleCloseInviteForm();
        toast(t("messageOkModalDialogUpdateMember"), "success");
        mutate(`/groups/${groupId}/members`);
      } else {
        toast(showErrorNotification(response.data.data), "warning");
      }
    } catch (e) {
      handleCloseInviteForm();
    } finally {
      setShowBackdrop(false);
    }
  };

  const handleClose = () => {
    handleCloseInviteForm();
    resetSelectedUser();
  };

  return (
    <Modal
      title={t(member ? "editTeamMember" : "textInviteCollaborators")}
      description={member ? null : t("descriptionModalDialogInvite")}
      handleClose={handleClose}
      open={openInviteForm}
    >
      <div>
        <Backdrop open={showBackdrop} />
        <Formik
          initialValues={initialValues}
          validationSchema={ValidationSchema}
          onSubmit={(values: MyFormValues, { setSubmitting }: any) => {
            if (member?.id) {
              onSubmitUpdate(values, setSubmitting);
              return;
            }

            onSubmitCreate(values, setSubmitting);
          }}
        >
          {({ submitForm, isSubmitting, errors, touched }: any) => (
            <>
              <Form autoComplete="off">
                <Field name="name" data-testid="media-name" InputProps={{ notched: true }}>
                  {({ field }: FieldProps) => (
                    <TextField
                      id="name"
                      variant="outlined"
                      inputProps={{
                        autoComplete: "off",
                        form: {
                          autoComplete: "off",
                        },
                      }}
                      label={t("placeholderName")}
                      type="text"
                      fullWidth
                      {...field}
                    />
                  )}
                </Field>
                {errors.name && touched.name ? <ErrorMessageForm message={errors.name} /> : null}
                <Divider marginTop={20} />
                <Field name="emailCol" data-testid="media-email" InputProps={{ notched: true }}>
                  {({ field }: FieldProps) => (
                    <TextField
                      variant="outlined"
                      inputProps={{
                        autoComplete: "off",
                        form: {
                          autoComplete: "off",
                        },
                      }}
                      id="emailCol"
                      label={t("placeholderEmail")}
                      type="email"
                      fullWidth
                      {...field}
                    />
                  )}
                </Field>
                {errors.emailCol && touched.emailCol ? (
                  <ErrorMessageForm message={errors.emailCol} />
                ) : null}
                <Divider marginTop={20} />
                {/* <Field name="group" data-testid="media-group" InputProps={{ notched: true }}>
                  {({ field }: FieldProps) => (
                    <Select
                      label={t("placeholderGroup")}
                      variant={SelectVariantEnum.OUTLINED}
                      options={userRdx.user.groups.map((item) => ({
                        id: item,
                        value: item,
                      }))}
                      id="group"
                      {...field}
                    />
                  )}
                </Field>
                {errors.group && touched.group ? <ErrorMessageForm message={errors.group} /> : null} */}
                <Divider marginTop={20} />

                <Field
                  name="permission"
                  data-testid="media-permission"
                  InputProps={{ notched: true }}
                >
                  {({ field }: FieldProps) => (
                    <Select
                      label={t("placeholderPermission")}
                      variant={SelectVariantEnum.OUTLINED}
                      options={[
                        {
                          id: RoleUserEnum.COLLABORATOR,
                          value: t("inviteCollaboratorTitle"),
                        },
                        {
                          id: RoleUserEnum.ADMIN,
                          value: t("inviteAdministratorTitle"),
                        },
                      ]}
                      id="permission"
                      {...field}
                    />
                  )}
                </Field>
                {errors.permission && touched.permission ? (
                  <ErrorMessageForm message={errors.permission} />
                ) : null}
              </Form>
              <Box
                display="flex"
                marginTop={2}
                flex={1}
                flexDirection="row"
                justifyContent="space-between"
              >
                <Button
                  handleClick={handleClose}
                  title={t("buttonCancelModalDialogInvite")}
                  data-testid="close-modal-invite"
                  variant={ButtonVariantEnum.OUTLINED}
                />
                <Button
                  handleClick={submitForm}
                  title={t("buttonOkModalDialogInvite")}
                  disabled={isSubmitting}
                  isLoading={isSubmitting}
                  type="submit"
                  data-testid="submit-invite"
                />
              </Box>
            </>
          )}
        </Formik>
      </div>
    </Modal>
  );
}
