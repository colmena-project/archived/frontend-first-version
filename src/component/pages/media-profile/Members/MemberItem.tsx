import { useState } from "react";
import { makeStyles } from "@material-ui/core";
import { AvatarMemoized } from "@/components/pages/profile/Avatar";
import Text from "@/components/ui/Text";
import Clickable from "@/components/ui/Clickable";

import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { v4 as uuid } from "uuid";
import ContextMenuItem from "@/components/ui/ContextMenuItem";
import { useTranslation } from "next-i18next";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { RoleUserEnum } from "@/enums/*";
import { MemberInterface } from "@/interfaces/index";

const useStyles = makeStyles((theme) => ({
  member: {
    flexDirection: "column",
  },
  user: {
    color: theme.palette.icon.main,
    fontWeight: 400,
    fontSize: "14px",
    lineHeight: "14px",
    marginTop: "10px",
  },
  permission: {
    color: theme.palette.icon.main,
    fontWeight: 400,
    fontSize: "10px",
    lineHeight: "10px",
    marginTop: "5px",
  },
  iconRemove: {
    color: theme.palette.danger.light,
  },
}));

interface MemberItemInterface {
  member: MemberInterface;
  openModal: (member: MemberInterface) => void;
}

type PositionProps = {
  mouseX: null | number;
  mouseY: null | number;
};

export default function MemberItem({ member, openModal }: MemberItemInterface) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { t } = useTranslation("mediaProfile");
  const { t: p } = useTranslation("profile");
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [position, setPosition] = useState<PositionProps>({
    mouseX: null,
    mouseY: null,
  });

  const handleOpenContextMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
    setPosition({
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4,
    });
  };

  const handleCloseContextMenu = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Clickable
        className={classes.member}
        handleClick={
          userRdx.user.role === RoleUserEnum.ADMIN ? handleOpenContextMenu : () => undefined
        }
      >
        <AvatarMemoized size={10} userId={member.username} userName={member.firstName} />
        <Text className={classes.user}>{member.firstName}</Text>
        <Text className={classes.permission}>{p(`${member.permission}.title`)}</Text>
      </Clickable>
      <Menu
        key={uuid()}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        anchorReference="anchorPosition"
        anchorPosition={
          position.mouseY !== null && position.mouseX !== null
            ? { top: position.mouseY, left: position.mouseX }
            : undefined
        }
        onClose={handleCloseContextMenu}
      >
        <MenuItem
          key="remove"
          data-testid="social-media-remove"
          onClick={() => {
            handleCloseContextMenu();
            openModal(member);
          }}
          className={classes.iconRemove}
        >
          <ContextMenuItem icon="edit" title={t("socialMediaContextMenuOptions.edit")} />
        </MenuItem>
      </Menu>
    </>
  );
}
