/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import GalleryHorizontalScroll from "@/components/ui/GalleryHorizontalScroll";
import { useTranslation } from "react-i18next";
import theme from "@/styles/theme";
import SocialMediaItem from "./SocialMediaItem";
import { makeStyles } from "@material-ui/core/styles";
import Edit from "./edit";
import { useSelector, useDispatch } from "react-redux";
import { PropsUserSelector, SocialMediasAvailableKeys } from "@/types/index";
import { SocialMediaInfoInterface } from "@/interfaces/index";
import { getAvailableSocialMediasKeysObject, getMediaGroupIcon } from "@/utils/utils";
import { toast } from "@/utils/notifications";
import Backdrop from "@/components/ui/Backdrop";
import { mediaInfoUpdate } from "@/store/actions/users/index";
import { update as updateGroup } from "@/services/internal/groups";
import { v4 as uuid } from "uuid";

const useStyles = makeStyles(() => ({
  container: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: 1,
    backgroundColor: "#fff",
  },
  mediaDescription: {
    backgroundColor: "#fff",
    textAlign: "left",
    width: "100%",
    padding: 10,
    paddingLeft: 15,
  },
  plusButton: {
    marginRight: 8,
    marginBottom: 25,
  },
}));

export default function SocialMedia() {
  const classes = useStyles();
  const { t } = useTranslation("mediaProfile");
  const { t: c } = useTranslation("common");
  const [openEditModal, setOpenEditModal] = useState(false);
  const [socialMediaSelected, setSocialMediaSelected] = useState<SocialMediaInfoInterface | null>(
    null,
  );
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [loadingRemoveSocialMedia, setLoadingRemoveSocialMedia] = useState(false);
  const dispatch = useDispatch();

  const removeSocialMedia = async (name: string) => {
    try {
      setLoadingRemoveSocialMedia(true);
      const key = `social_media_${name}`;
      const mediaObj = {
        ...userRdx.user.media,
        [key]: "",
      };
      const res = await updateGroup(userRdx.user.media.id, mediaObj);
      if (res.data.success) {
        dispatch(mediaInfoUpdate(mediaObj));
        toast(t("socialMediaInfoRemoved"), "success");
      } else {
        toast(c("genericErrorMessage"), "error");
      }
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "error");
    } finally {
      setLoadingRemoveSocialMedia(false);
    }
  };

  const handleShow = (url: string) => {
    const anchor = document.createElement("a");
    anchor.setAttribute("href", url);
    anchor.setAttribute("target", "blank");
    anchor.click();
  };

  return (
    <Box width="100%">
      <Backdrop open={loadingRemoveSocialMedia} />
      <Box className={classes.container}>
        <GalleryHorizontalScroll>
          {getAvailableSocialMediasKeysObject()
            .filter((item: SocialMediasAvailableKeys) => userRdx.user.media[item] !== "")
            .map((item: SocialMediasAvailableKeys) => (
              <Box key={uuid()}>
                <SocialMediaItem
                  title={getMediaGroupIcon(item)}
                  icon={getMediaGroupIcon(item)}
                  fontSize={50}
                  iconColor={theme.palette.primary.main}
                  handleDelete={removeSocialMedia}
                  handleEdit={() => {
                    setSocialMediaSelected({
                      name: getMediaGroupIcon(item),
                      url: userRdx.user.media[item],
                    });
                    setOpenEditModal(true);
                  }}
                  handleShow={() => handleShow(userRdx.user.media[item])}
                />
              </Box>
            ))}
        </GalleryHorizontalScroll>
        <Edit
          title={t("updateSocialMediaTitle")}
          open={openEditModal}
          handleClose={() => setOpenEditModal(false)}
          socialMediaSelected={socialMediaSelected}
        />
      </Box>
    </Box>
  );
}
