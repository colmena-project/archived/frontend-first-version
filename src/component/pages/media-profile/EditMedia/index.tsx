/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useState } from "react";
import { useTranslation } from "next-i18next";
import Button from "@/components/ui/Button";
import TextField from "@material-ui/core/TextField";
import Divider from "@/components/ui/Divider";
import { Formik, Form, Field, FieldProps } from "formik";
import { Box } from "@material-ui/core";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { PropsUserSelector } from "@/types/index";
import { mediaInfoUpdate } from "@/store/actions/users/index";
import { toast } from "@/utils/notifications";
import { validateURLMediaSocialGroup } from "@/utils/utils";
import { update as updateGroup } from "@/services/internal/groups";
import { makeStyles } from "@material-ui/core/styles";
import WrapperSocialNetworks from "../WrapperSocialNetworks";
import { isSubadminProfile } from "@/utils/permissions";

interface MyFormValues {
  name: string;
  description: string;
  url: string;
  audio: string;
}

const useStyles = makeStyles((theme) => ({
  buttonsContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  resetButton: {
    color: theme.palette.variation1.main,
  },
  form: {
    margin: "auto",
  },
}));

export default function EditMedia() {
  const classes = useStyles();
  const { t } = useTranslation("mediaProfile");
  const dispatch = useDispatch();
  const { t: c } = useTranslation("common");
  const [errorUrlMessage, setErrorUrlMessage] = useState("");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const initialValues = {
    name: userRdx.user.media.name,
    description: userRdx.user.media?.slogan,
    url: userRdx.user.media?.url,
    audio: "",
  };

  const maxLengthName = 100;
  const maxLengthDescription = 200;

  const schemaValidation = Yup.object().shape({
    description: Yup.string()
      .required(c("form.requiredTitle"))
      .max(maxLengthDescription, c("form.passwordMaxLengthTitle", { size: maxLengthDescription })),
    audio: Yup.string().nullable(),
    // url: Yup.string()
    //   .test("url", c("form.invalidURLTitle"), (url) => isValidUrl(String(url)))
    //   .required(c("form.requiredTitle")),
  });

  const handleSubmit = async (values: MyFormValues, setSubmitting: (flag: boolean) => void) => {
    const { description, url } = values;
    if (url) {
      if (!validateURLMediaSocialGroup(url)) {
        setSubmitting(false);
        setErrorUrlMessage(c("form.invalidURLTitle"));
        return;
      }
    }

    setSubmitting(true);
    try {
      const mediaObj = {
        ...userRdx.user.media,
        slogan: description,
        url,
      };
      const res = await updateGroup(userRdx.user.media.id, mediaObj);
      if (res.data.success) {
        dispatch(mediaInfoUpdate(mediaObj));
        toast(t("mediaProfileInfoSaved"), "success");
      } else {
        toast(t("genericErrorMessage"), "error");
      }
    } catch (e) {
      console.log(e);
      toast(t("genericErrorMessage"), "error");
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <Formik
      validationSchema={schemaValidation}
      initialValues={initialValues}
      onSubmit={(values: MyFormValues, { setSubmitting }: any) => {
        handleSubmit(values, setSubmitting);
      }}
    >
      {({ submitForm, isSubmitting, errors, touched }: any) => (
        <Form>
          <Field name="name" data-testid="media-name" InputProps={{ notched: true }}>
            {({ field }: FieldProps) => (
              <TextField
                id="name"
                label={t("editForm.name")}
                variant="outlined"
                disabled
                inputProps={{
                  autoComplete: "off",
                  maxLength: maxLengthName,
                }}
                fullWidth
                {...field}
              />
            )}
          </Field>
          {errors.name && touched.name ? <ErrorMessageForm message={errors.name} /> : null}
          <Divider marginTop={20} />
          <Field name="description" data-testid="media-description" InputProps={{ notched: true }}>
            {({ field }: FieldProps) => (
              <TextField
                id="description"
                label={t("editForm.slogan")}
                variant="outlined"
                disabled={!isSubadminProfile()}
                inputProps={{
                  autoComplete: "off",
                  maxLength: maxLengthDescription,
                }}
                fullWidth
                multiline
                {...field}
              />
            )}
          </Field>
          {errors.description && touched.description ? (
            <ErrorMessageForm message={errors.description} />
          ) : null}
          <Divider marginTop={20} />
          <Field name="url" data-testid="media-url" InputProps={{ notched: true }}>
            {({ field }: FieldProps) => (
              <TextField
                id="url"
                label={t("editForm.url")}
                variant="outlined"
                fullWidth
                disabled={!isSubadminProfile()}
                inputProps={{
                  autoComplete: "off",
                }}
                multiline
                {...field}
              />
            )}
          </Field>
          {errorUrlMessage ? <ErrorMessageForm message={errorUrlMessage} /> : null}
          {/* <Divider marginTop={20} />
          <Field name="audio" data-testid="media-audio" InputProps={{ notched: true }}>
            {({ field }: FieldProps) => (
              <TextField
                id="audio"
                label={t("editForm.audio")}
                variant="outlined"
                fullWidth
                disabled={!isSubadminProfile()}
                inputProps={{
                  autoComplete: "off",
                }}
                multiline
                {...field}
              />
            )}
          </Field> */}
          <WrapperSocialNetworks />
          {isSubadminProfile() && (
            <>
              <Divider marginTop={20} />
              <Box className={classes.buttonsContainer}>
                <Button
                  handleClick={submitForm}
                  title={c("form.submitSaveTitle")}
                  disabled={isSubmitting}
                  isLoading={isSubmitting}
                  data-testid="submit-edit-media"
                  type="submit"
                />
              </Box>
            </>
          )}
        </Form>
      )}
    </Formik>
  );
}
