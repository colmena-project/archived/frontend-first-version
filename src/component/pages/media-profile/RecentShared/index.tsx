/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect, useCallback } from "react";
import {
  LibraryCardItemInterface,
  LibraryItemInterface,
  TimeDescriptionInterface,
} from "@/interfaces/index";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import { ContextMenuOptionEnum, ListTypeEnum, OrderEnum, TextVariantEnum } from "@/enums/index";
import Library, { orderItems } from "@/components/pages/library";
import { PropsUserSelector } from "@/types/*";
import {
  removeCornerSlash,
  // isAudioFile,
  getAllLibraryItems,
  setAllLibraryItems,
} from "@/utils/utils";
// import { v4 as uuid } from "uuid";
import ContextMenuOptions from "@/components/pages/library/contextMenu";
import { getAudioPath, hasExclusivePath, pathIsInFilename } from "@/utils/directory";
import IconButton from "@/components/ui/IconButton";
import Text from "@/components/ui/Text";
import { Grid, Box } from "@material-ui/core";
import { toast } from "@/utils/notifications";
import { searchByDisplayName } from "@/services/webdav/files";
import ToolbarSection from "@/components/ui/ToolbarSection";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getFileLink } from "@/utils/offlineNavigation";

const RecentShared: React.FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [data, setData] = useState<Array<LibraryItemInterface>>();
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { t } = useTranslation("common");
  const timeDescription: TimeDescriptionInterface = t("timeDescription", { returnObjects: true });

  const { t: m } = useTranslation("mediaProfile");

  const filtersItems = useCallback(
    (items: LibraryItemInterface[]) =>
      orderItems(OrderEnum.LATEST_FIRST, items, userRdx.user)
        .filter((item) => item.type !== "directory")
        .slice(0, 2),
    [],
  );

  const revalidateItems = useCallback(async () => {
    const items = await searchByDisplayName(userRdx.user.id, "", timeDescription, t);
    if (items) {
      setAllLibraryItems(items);
      setData(filtersItems(items));
    }
  }, []);

  const mountItems = useCallback(async () => {
    try {
      setIsLoading(true);
      const items = getAllLibraryItems();
      if (items) {
        setIsLoading(false);
        setData(filtersItems(items));
      }
    } catch (e) {
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  }, []);

  useEffect(() => {
    mountItems();
    revalidateItems();
    return () => {
      setIsLoading(false);
    };
  }, []);

  const handleItemClick = ({ type, aliasFilename, filename }: LibraryCardItemInterface) => {
    if (type === "directory" && router.query.path !== aliasFilename) {
      router.push(`/library/${aliasFilename}`);
    } else if (type === "file") {
      router.push(getFileLink(btoa(filename), connectionStatus));
    }
  };

  const unavailable = () => {
    toast(t("featureUnavailable"), "warning");
  };

  const handleContextMenuUpdate = async () => {
    setIsLoading(true);
    await revalidateItems();
    setIsLoading(false);
  };

  const options = (
    cardItem: LibraryCardItemInterface,
    playIconComp: React.ReactNode | undefined = undefined,
  ) => {
    const { filename, basename } = cardItem; // , orientation, mime
    const options = [];
    // const shareOption = (
    //   <IconButton
    //     key={`${uuid()}-share`}
    //     icon="share"
    //     color="#9A9A9A"
    //     style={{ padding: 0, margin: 0, minWidth: 30 }}
    //     fontSizeIcon="small"
    //     handleClick={unavailable}
    //   />
    // );

    if (playIconComp) options.push(playIconComp);

    if (!hasExclusivePath(filename) && removeCornerSlash(filename).split("/").length > 1) {
      // if (!pathIsInFilename(getAudioPath(), filename) && orientation === "vertical") {
      //   if (!isAudioFile(mime)) {
      //     options.push(shareOption);
      //   }
      // }

      options.push(
        <ContextMenuOptions
          key={`${basename}-more-options`}
          {...cardItem}
          availableOptions={[
            ContextMenuOptionEnum.EDIT,
            ContextMenuOptionEnum.COPY,
            ContextMenuOptionEnum.MOVE,
            ContextMenuOptionEnum.DETAILS,
            ContextMenuOptionEnum.AVAILABLE_OFFLINE,
            ContextMenuOptionEnum.DOWNLOAD,
            ContextMenuOptionEnum.DELETE,
            ContextMenuOptionEnum.DUPLICATE,
            ContextMenuOptionEnum.PUBLISH,
            ContextMenuOptionEnum.RENAME,
          ]}
          onChange={handleContextMenuUpdate}
        />,
      );
    }

    return options;
  };

  const bottomOptions = (
    cardItem: LibraryCardItemInterface,
    playIconComp: React.ReactNode | undefined = undefined,
    badgeStatusGrid: React.ReactNode | undefined = undefined,
  ) => {
    const { filename, basename, orientation } = cardItem;
    const options = [];
    if (playIconComp) options.push(playIconComp);

    const shareOption = (
      <IconButton
        key={`${basename}-share`}
        icon="share"
        color="#9A9A9A"
        style={{ padding: 0, margin: 0, minWidth: 30 }}
        fontSizeIcon="small"
        handleClick={unavailable}
      />
    );

    if (!hasExclusivePath(filename) && removeCornerSlash(filename).split("/").length > 1) {
      if (!pathIsInFilename(getAudioPath(), filename) && orientation === "horizontal") {
        options.push(shareOption);
      }
    }

    if (badgeStatusGrid) options.push(badgeStatusGrid);

    return options;
  };

  return (
    <Box width="100%" data-testid="ui-recent-files">
      <Grid>
        <ToolbarSection
          icon="recently_viewed"
          title={m("recentShared")}
          link={`/library/${userRdx.user.id}`}
          noMargin
        />
      </Grid>
      <Library
        items={data}
        options={options}
        bottomOptions={bottomOptions}
        handleItemClick={handleItemClick}
        listType={ListTypeEnum.LIST}
        isLoading={isLoading}
        itemsQuantitySkeleton={3}
      />
      {!isLoading && data && data?.length === 0 && (
        <Box style={{ backgroundColor: "#fff" }} padding={2}>
          <Text style={{ textAlign: "left" }} variant={TextVariantEnum.BODY2}>
            {t("noItemsFound")}
          </Text>
        </Box>
      )}
    </Box>
  );
};

export default RecentShared;
