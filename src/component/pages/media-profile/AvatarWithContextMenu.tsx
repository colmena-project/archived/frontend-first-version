/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useRef, useState, useCallback } from "react";
import Backdrop from "@/components/ui/Backdrop";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import Box from "@material-ui/core/Box";
import { MediaGroupAvatarMemoized } from "./Avatar";
import { useTranslation } from "next-i18next";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ContextMenuItem from "@/components/ui/ContextMenuItem";
import { v4 as uuid } from "uuid";
import { toast } from "@/utils/notifications";
import Slider from "@material-ui/core/Slider";
import Cropper from "react-easy-crop";
import Modal from "@/components/ui/Modal";
import { ButtonColorEnum, ButtonVariantEnum } from "@/enums/*";
import Button from "@/components/ui/Button";
import Loading from "@/components/ui/Loading";
import { isPNGImage, isJPGImage } from "@/utils/utils";
import getCroppedImg from "@/utils/cropImage";

import { makeStyles } from "@material-ui/core/styles";
import { useMutation } from "@apollo/client";
import useMediaGroup from "@/hooks/cms/useMediaGroup";
import { CREATE_MEDIA_GROUP, UPDATE_MEDIA_GROUP } from "@/services/cms/mediaGroups";
import { removeFile, uploadFileToCMS } from "@/services/cms/file";

type Props = {
  size: number;
  showEditImage?: boolean;
};

type PositionProps = {
  mouseX: null | number;
  mouseY: null | number;
};

type CroppedAreaProps = {
  width: number;
  height: number;
  x: number;
  y: number;
};

type FileProps = {
  bits: string;
  name: string;
  type: string;
};

const useStyles = makeStyles((theme) => ({
  iconRemove: {
    color: theme.palette.danger.light,
  },
}));

function AvatarChangePicture({ size, showEditImage = true }: Props) {
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const inputFileRef = useRef(null);
  const [showBackdrop, setShowBackdrop] = useState(false);
  const [file, setFile] = useState<FileProps>({
    bits: "",
    name: "",
    type: "",
  });
  const [showModal, setShowModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const { t: c } = useTranslation("common");
  const { t } = useTranslation("mediaProfile");
  const [position, setPosition] = useState<PositionProps>({
    mouseX: null,
    mouseY: null,
  });
  const [loadingRemove, setLoadingRemove] = useState(false);
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState<CroppedAreaProps>({
    width: 0,
    height: 0,
    x: 0,
    y: 0,
  });

  const mediaName = userRdx.user.media.name;
  const mediaGroupId = userRdx.user.media.id;
  const { mediaGroup, refetch, loading } = useMediaGroup(mediaGroupId);
  const [createMediaGroup] = useMutation(CREATE_MEDIA_GROUP);
  const [updateMediaGroup] = useMutation(UPDATE_MEDIA_GROUP);

  const handleOpenContextMenu = (event: any) => {
    if (!showEditImage) return;
    setAnchorEl(event.currentTarget);
    setPosition({
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4,
    });
  };

  const handleCloseContextMenu = () => {
    setAnchorEl(null);
  };

  const handleCloseModal = () => {
    setFile({} as FileProps);
    setShowModal(false);
  };

  const removeMediaAvatar = async () => {
    try {
      setLoadingRemove(true);
      handleCloseContextMenu();
      if (mediaGroup && mediaGroup.attributes.avatar.data?.id) {
        await removeFile(mediaGroup.attributes.avatar.data.id);
      }
      toast(t("mediaProfileAvatarRemoved"), "success");
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "warning");
    } finally {
      setLoadingRemove(false);
    }
  };

  const onSelectFile = (e: any) => {
    if (e.target.files && e.target.files.length > 0) {
      const { type, size, name } = e.target.files[0];

      if (!isPNGImage(type) && !isJPGImage(type)) {
        toast(c("uploadRuleFileType"), "warning");
        return;
      }

      // 20MB limite do NC
      if (size > 20971520) {
        toast(c("uploadRuleFileSize"), "warning");
        return;
      }

      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setFile({
          bits: String(reader.result),
          name,
          type,
        });
      });
      setShowModal(true);
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const uploadThumb = useCallback(
    async (image: string, fileName: string, type: string) => {
      const croppedImage = await getCroppedImg(image, croppedAreaPixels, 0);
      const file = new File([croppedImage], fileName, { type });
      console.log(file);

      const formData = new FormData();
      formData.append("files", file);

      const [response] = await uploadFileToCMS(formData);

      return response.id;
    },
    [croppedAreaPixels],
  );

  const showCroppedImage = useCallback(async () => {
    try {
      setShowBackdrop(true);
      const avatar = await uploadThumb(file.bits, file.name, file.type);
      if (mediaGroup) {
        if (mediaGroup.attributes.avatar?.data?.id) {
          await removeFile(mediaGroup.attributes.avatar.data.id);
        }

        await updateMediaGroup({
          variables: { id: mediaGroup.id, token: mediaGroupId, avatar },
        });
      } else {
        const mediaGroupCreated = await createMediaGroup({
          variables: { token: mediaGroupId, avatar },
        });
        if (!mediaGroupCreated) {
          throw new Error();
        }

        await refetch(mediaGroupCreated.data.createMediaGroup.data);
      }

      toast(t("mediaProfileAvatarSaved"), "success");
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "warning");
    } finally {
      setShowBackdrop(false);
      setShowModal(false);
    }
  }, [croppedAreaPixels, file]);

  const onBtnClick = () => {
    handleCloseContextMenu();
    if (!inputFileRef || !inputFileRef.current) return;

    const clk: { click: () => void } = inputFileRef?.current;

    clk.click();
  };

  const onCropChange = (crop: { x: number; y: number }) => {
    setCrop(crop);
  };

  const onCropComplete = (_: any, croppedAreaPixels: CroppedAreaProps) => {
    setCroppedAreaPixels(croppedAreaPixels);
  };

  const onZoomChange = (zoom: number) => {
    setZoom(zoom);
  };

  return (
    <>
      <Backdrop open={loadingRemove} />
      <Backdrop open={showBackdrop} />
      <input type="file" ref={inputFileRef} onChange={onSelectFile} style={{ display: "none" }} />
      <Box
        onClick={handleOpenContextMenu}
        data-testid="media-avatar-open-context"
        style={{ cursor: "pointer" }}
      >
        {loading ? (
          <Loading />
        ) : (
          <MediaGroupAvatarMemoized
            size={size}
            mediaName={mediaName}
            token={mediaGroupId}
            showEditImage={showEditImage}
          />
        )}
      </Box>
      <Box display="flex" justifyContent="flex-end">
        <Menu
          key={uuid()}
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          anchorReference="anchorPosition"
          anchorPosition={
            position.mouseY !== null && position.mouseX !== null
              ? { top: position.mouseY, left: position.mouseX }
              : undefined
          }
          onClose={handleCloseContextMenu}
        >
          <MenuItem key="upload" data-testid="upload-media-avatar-menu-option" onClick={onBtnClick}>
            <ContextMenuItem icon="camera" title={c("contextMenuUserAvatar.upload")} />
          </MenuItem>
          <MenuItem
            key="remove"
            data-testid="remove-media-avatar-menu-option"
            onClick={removeMediaAvatar}
            className={classes.iconRemove}
          >
            <ContextMenuItem icon="trash" danger title={c("contextMenuUserAvatar.remove")} />
          </MenuItem>
        </Menu>
      </Box>

      <Modal
        open={showModal}
        handleClose={handleCloseModal}
        actions={
          <Box display="flex" flex="1" flexDirection="row" justifyContent="space-between">
            <Button
              handleClick={handleCloseModal}
              style={{ margin: 8 }}
              variant={ButtonVariantEnum.OUTLINED}
              color={ButtonColorEnum.SECONDARY}
              title={c("form.cancelButton")}
              data-testid="close-modal-crop-media-avatar"
            />
            <Button
              handleClick={showCroppedImage}
              style={{ margin: 8 }}
              variant={ButtonVariantEnum.CONTAINED}
              title={c("form.submitSaveTitle")}
              data-testid="submit-modal-crop-media-avatar"
            />
          </Box>
        }
      >
        <Box>
          <Box width="100%" height={280}>
            <Cropper
              image={file.bits}
              crop={crop}
              zoom={zoom}
              aspect={4 / 3}
              cropShape="round"
              showGrid
              style={{
                containerStyle: { width: "100%", height: 300 },
                cropAreaStyle: { width: 250, height: 250 },
                // mediaStyle: { width: "100%", height: "100%", margin: "0 auto" },
              }}
              cropSize={{ width: 250, height: 250 }}
              onCropChange={onCropChange}
              onCropComplete={onCropComplete}
              onZoomChange={onZoomChange}
              objectFit="vertical-cover"
            />
          </Box>
          <Box height={30}>
            <Slider
              data-testid="slider-modal-crop-media-avatar"
              value={zoom}
              min={1}
              max={3}
              step={0.1}
              aria-labelledby="Zoom"
              onChange={(e, zoom: number) => onZoomChange(zoom)}
              style={{ paddingTop: 22, paddingBottom: 22, paddingLeft: 0, paddingRight: 0 }}
            />
          </Box>
        </Box>
      </Modal>
    </>
  );
}

export default AvatarChangePicture;
