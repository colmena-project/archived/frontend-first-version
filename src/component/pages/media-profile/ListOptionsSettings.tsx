import { useTranslation } from "next-i18next";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import { makeStyles } from "@material-ui/core/styles";
import theme from "@/styles/theme";
import { toast } from "@/utils/notifications";
import SvgIconAux from "@/components/ui/SvgIcon";
import { AllIconProps, PossibleOptionsInMediaProfileProps } from "@/types/*";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    textAlign: "left",
    "& .MuiListItem-root": {
      backgroundColor: theme.palette.background.paper,
    },
  },
}));

interface ListOptionsSettingsInterface {
  changePage: (page: PossibleOptionsInMediaProfileProps) => void;
}

interface OptionsInterface {
  id: string;
  icon: AllIconProps;
  label: string;
  handleClick: () => void;
}

export default function ListOptionsSettings({ changePage }: ListOptionsSettingsInterface) {
  const { t: c } = useTranslation("common");
  const { t: m } = useTranslation("mediaProfile");
  const classes = useStyles();

  const unavailable = () => {
    toast(c("featureUnavailable"), "warning");
  };

  const options: OptionsInterface[] = [
    {
      id: "my-media-stats",
      icon: "monitoring",
      label: m("settingsOptions.myMediaStats"),
      handleClick: unavailable,
    },
    {
      id: "general-configurations",
      icon: "settings_applications",
      label: m("settingsOptions.generalConfigurations"),
      handleClick: unavailable,
    },
    {
      id: "edit-team",
      icon: "group_add",
      label: m("settingsOptions.editTeam"),
      handleClick: () => changePage("editTeam"),
    },
    {
      id: "connect-for-publish-in-external-sites",
      icon: "hub",
      label: m("settingsOptions.connectForPublishInExternalSites"),
      handleClick: unavailable,
    },
    {
      id: "tags",
      icon: "sell",
      label: m("settingsOptions.tags"),
      handleClick: unavailable,
    },
    {
      id: "publication-settings",
      icon: "podcasts",
      label: m("settingsOptions.publicationSettings"),
      handleClick: unavailable,
    },
  ];

  return (
    <List className={classes.root}>
      {options.map((item) => (
        <ListItem key={item.id} divider button onClick={item.handleClick}>
          <ListItemIcon>
            <SvgIconAux icon={item.icon} fontSize="medium" htmlColor={theme.palette.gray.dark} />
          </ListItemIcon>
          <ListItemText id={item.id} primary={item.label} />
          <ListItemSecondaryAction>
            <ArrowForwardIosIcon fontSize="small" style={{ color: theme.palette.icon.dark }} />
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
}
