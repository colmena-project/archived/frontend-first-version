import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { getFirstLettersOfTwoFirstNames } from "@/utils/utils";
import { Theme, makeStyles, createStyles, useTheme } from "@material-ui/core/styles";
import Badge from "@material-ui/core/Badge";
import SvgIcon from "@/components/ui/SvgIcon";
import Loading from "@/components/ui/Loading";
import useMediaGroupAvatar from "@/hooks/cms/useMediaGroupAvatar";

type Props = {
  size: number;
  token: string;
  mediaName: string;
  showEditImage?: boolean;
};

function AvatarAux({ size, showEditImage = false, mediaName, token }: Props) {
  const theme = useTheme();
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      size: {
        border: "3px solid #fff",
        width: theme.spacing(size),
        height: theme.spacing(size),
      },
    }),
  );
  const { thumbnail: avatar, loading } = useMediaGroupAvatar(token);

  const classes = useStyles();

  if (loading) return <Loading />;

  return (
    <Badge
      overlap="circular"
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      badgeContent={
        showEditImage ? (
          <SvgIcon icon="edit_circle" htmlColor={theme.palette.variation4.main} fontSize="small" />
        ) : null
      }
    >
      {!navigator.onLine ? (
        <Avatar className={classes.size}>{getFirstLettersOfTwoFirstNames(mediaName)}</Avatar>
      ) : (
        <Avatar
          alt={`Media avatar ${mediaName}`}
          src={avatar ?? undefined}
          className={classes.size}
        >
          {getFirstLettersOfTwoFirstNames(mediaName)}
        </Avatar>
      )}
    </Badge>
  );
}

export default AvatarAux;
export const MediaGroupAvatarMemoized = React.memo(AvatarAux);
