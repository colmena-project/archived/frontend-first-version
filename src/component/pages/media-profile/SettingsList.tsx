import { makeStyles } from "@material-ui/core/styles";
import { Backdrop, Box } from "@material-ui/core";
import { useState } from "react";
import { PossibleOptionsInMediaProfileProps } from "@/types/*";
import ListOptionsSettings from "./ListOptionsSettings";
import ManageTeam from "./ManageTeam";

const useStyles = makeStyles(() => ({
  tabsWrapper: {
    width: "100%",
  },
}));

export default function SettingsList() {
  const classes = useStyles();
  const [showBackdrop] = useState(false);
  const [currentPage, setCurrentPage] = useState<PossibleOptionsInMediaProfileProps>("initial");

  const changePage = (page: PossibleOptionsInMediaProfileProps) => {
    setCurrentPage(page);
  };

  const pages: any = {
    initial: <ListOptionsSettings changePage={changePage} />,
    editTeam: <ManageTeam changePage={changePage} />,
  };

  return (
    <Box component="section" className={classes.tabsWrapper}>
      <Backdrop open={showBackdrop} />
      {pages[currentPage]}
    </Box>
  );
}
