import React, { useState } from "react";
import { Box, styled } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { TextAlignEnum, TextColorEnum, TextVariantEnum } from "@/enums/*";
import Badge from "@/components/ui/Badge";
import { useTheme } from "@material-ui/core/styles";
import TitleBox from "@/components/ui/TitleBox";
import Breadcrumb from "@/components/ui/Breadcrumb";
import Avatar from "@/components/ui/Avatar";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import IconButtonCtr from "@/components/ui/IconButton";

import SharePublicationModal from "../../publications/common/SharePublicationModal";
import { DraftTarget, AuthorAvatar, AuthorWrapper, HeaderContent, Banner } from "./styled";

type Props = {
  publisher: string;
  categoryName?: string;
  publishedAt?: string;
  creator: string;
  image?: string;
  publicationsUrl?: string;
  publicationTitle: string;
  publicationId: string;
  publicationDescription: string;
  baseUrl: string;
  showShareButton?: boolean;
  showBreadcrumbs?: boolean;
  onCloseShareModal?: () => void;
};

export default function PublicationHeader({
  publisher,
  categoryName,
  publishedAt,
  creator,
  image,
  publicationsUrl = "/publications",
  baseUrl,
  publicationId,
  showShareButton,
  publicationDescription,
  publicationTitle,
  showBreadcrumbs = true,
}: Props) {
  const [openShareModal, setOpenShareModal] = useState(false);

  const theme = useTheme();
  const router = useRouter();

  const { t } = useTranslation("publication");
  const { t: c } = useTranslation("publications");

  const breadcrumbs = [
    {
      description: t("publicationsTitle"),
      path: publicationsUrl,
      isCurrent: false,
    },
    {
      description: publisher,
      path: `${publicationsUrl}/media/${publisher}`,
      isCurrent: false,
    },
  ];

  const toggleOpenShareModal = () => {
    setOpenShareModal((currentState) => !currentState);
  };
  console.log(image);
  return (
    <>
      {showShareButton && (
        <SharePublicationModal
          publicationId={publicationId}
          open={openShareModal}
          onClose={toggleOpenShareModal}
          baseUrl={baseUrl}
          description={publicationDescription}
          title={publicationTitle}
          data-testid="share-modal"
        />
      )}

      {image ? (
        <Banner style={{ background: `url(${image})` }}>
          <Badge
            description={<Text variant={TextVariantEnum.CAPTION}>{categoryName}</Text>}
            bgColor={theme.palette.primary.main}
            textColor={theme.palette.primary.contrastText}
            borderRadius={0}
            padding={10}
          />
        </Banner>
      ) : (
        <TitleBox title={categoryName ?? ""} icon="global" />
      )}
      {!publishedAt && (
        <DraftTarget>
          <Text variant={TextVariantEnum.CAPTION} align={TextAlignEnum.CENTER}>
            {c("preview.situationDraft")}
          </Text>
        </DraftTarget>
      )}
      <HeaderContent data-testid="publication-header-info">
        <Box display="flex" alignItems="center" justifyContent="space-between">
          {showBreadcrumbs && (
            <Breadcrumb
              breadcrumbs={breadcrumbs}
              handleNavigate={(item) => {
                router.push(item.path);
              }}
            />
          )}
          {showShareButton && (
            <ShareButton
              aria-label="share-icon-button"
              data-testid="share-icon-button"
              icon="share_circle"
              handleClick={toggleOpenShareModal}
            />
          )}
        </Box>

        <AuthorWrapper>
          <AuthorAvatar>
            <Avatar image="" size={5} variant="circular" name={creator}></Avatar>
          </AuthorAvatar>
          <Box>
            <Text color={TextColorEnum.PRIMARY}>{creator}</Text>
            {Boolean(publishedAt) && (
              <Text variant={TextVariantEnum.CAPTION}>
                {t("publishedAt")} {publishedAt}
              </Text>
            )}
          </Box>
        </AuthorWrapper>
      </HeaderContent>
    </>
  );
}

const ShareButton = styled(IconButtonCtr)({
  height: 21,
  width: 21,

  "& svg": {
    height: 21,
    width: 21,
  },
});
