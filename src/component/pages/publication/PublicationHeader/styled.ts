import { Box, styled } from "@material-ui/core";

export const DraftTarget = styled(Box)(({ theme }) => ({
  pading: 10,
  backgroundColor: theme.palette.gray.dark,
  width: "100%",
  textAlign: "center",
  color: "white",
}));

export const HeaderContent = styled(Box)(({ theme }) => ({
  padding: "5px 10px",
  boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
  borderBottomRightRadius: 20,
  borderBottomLeftRadius: 20,
  marginBottom: theme.spacing(1),
  width: "100%",
}));

export const AuthorWrapper = styled(Box)({
  display: "flex",
  alignItems: "center",
  paddingBottom: 8,
});

export const AuthorAvatar = styled(Box)({
  marginRight: 8,
});

export const Banner = styled(Box)({
  display: "flex",
  width: "100%",
  height: 200,
  backgroundSize: "cover!important",
  alignItems: "flex-end",
  justifyContent: "flex-end",
});
