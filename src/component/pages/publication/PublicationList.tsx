import React, {
  memo,
  ReactElement,
  ReactNode,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";

import { Box, useTheme } from "@material-ui/core";
import HorizontalCard from "@/components/ui/HorizontalCard";
import Badge from "@/components/ui/Badge";
import Text from "@/components/ui/Text";

import { TextVariantEnum } from "@/enums/*";
import SvgIconAux from "@/components/ui/SvgIcon";
import { useRouter } from "next/router";
import { PublicationQueryInterface } from "@/interfaces/cms";
import { dateDescription, empty, getUrlStrapiImage } from "@/utils/utils";
import { useTranslation } from "next-i18next";
import { Virtuoso, VirtuosoHandle } from "react-virtuoso";
import Loading from "@/components/ui/Loading";
import { PublicationFiltersProps } from "@/services/cms/publications";
import NoPublication from "@/components/pages/publication/NoPublication";
import getPublications from "@/services/cms/publicationsSSQuery";
import {
  AvatarImage,
  DateDescription,
  Title,
  VerticalList,
  WrapperLoading,
  WrapperVirtuoso,
} from "@/components/pages/publication/styled";
import PublicationItemSkeleton from "@/components/ui/skeleton/PublicationItem";

type Props = {
  baseUrl?: string;
  options?: ReactElement | ReactNode;
  filters: PublicationFiltersProps;
  height?: string;
  itemsPerPage?: number;
};

const ITEMS_PER_PAGE = 9;

function PublicationList({
  itemsPerPage = ITEMS_PER_PAGE,
  baseUrl = "/publications/show",
  options,
  filters = {
    keyword: undefined,
    group: undefined,
    category: undefined,
    order: "publishedAt:desc",
  },
  height,
}: Props) {
  const [items, setItems] = useState<Array<PublicationQueryInterface>>([]);
  const [loadingPublications, setLoadingPublications] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);
  const [totalPage, setTotalPage] = useState<null | number>(0);
  const [page, setPage] = useState(1);
  const [nextPage, setNextPage] = useState(1);

  const theme = useTheme();
  const router = useRouter();
  const { t } = useTranslation("common");

  const virtuosoRef = useRef<VirtuosoHandle>(null);

  const getItems = useCallback(
    async (filters: PublicationFiltersProps): Promise<PublicationQueryInterface[]> => {
      try {
        const publications = await getPublications({
          itemsPerPage,
          ...filters,
        });

        if (!publications) {
          return [];
        }

        setTotalPage(publications?.data?.publications?.meta?.pagination?.pageCount ?? 1);

        setPage(publications.data.publications.meta.pagination.page);
        setNextPage(publications.data.publications.meta.pagination.page + 1);

        return publications.data.publications.data;
      } catch (e) {
        console.log(e);
        return [];
      }
    },
    [itemsPerPage],
  );

  const handleFilters = useCallback(
    (filters: PublicationFiltersProps) => ({
      ...filters,
      group: !empty(filters.group) ? filters.group : undefined,
      category: !empty(filters.category) ? filters.category : undefined,
      order: !empty(filters.order) ? filters.order : undefined,
      keyword: !empty(filters.keyword) ? filters.keyword : undefined,
    }),
    [],
  );

  const appendItems = useCallback(
    async (index: number) => {
      if (index < ITEMS_PER_PAGE - 1 || loadingMore || loadingPublications) {
        return;
      }

      if (totalPage === page) {
        return;
      }

      setLoadingMore(true);

      const newItems = await getItems(
        handleFilters({
          group: filters.group,
          category: filters.category,
          order: filters.order,
          keyword: filters.keyword,
          index: nextPage,
        }),
      );

      setItems((currentItems) => [...currentItems, ...newItems]);

      setLoadingMore(false);
    },
    [filters, getItems, loadingMore, loadingPublications, handleFilters, page, totalPage, nextPage],
  );

  useEffect(() => {
    (async () => {
      setLoadingPublications(true);
      const rawItems = await getItems(
        handleFilters({
          index: 0,
          group: filters.group,
          category: filters.category,
          order: filters.order,
          keyword: filters.keyword,
        }),
      );
      setItems(rawItems);

      setLoadingPublications(false);
    })();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters]);

  if (loadingPublications) {
    return (
      <WrapperLoading>
        <PublicationItemSkeleton amount={3} />
      </WrapperLoading>
    );
  }

  if (!loadingPublications && items.length === 0) {
    return <NoPublication />;
  }

  return (
    <WrapperVirtuoso role="main">
      <Virtuoso
        as="ul"
        ref={virtuosoRef}
        style={{ height, width: "100%" }}
        data={items}
        endReached={appendItems}
        components={{
          Footer: () => <Box padding={1}>{loadingMore && <Loading />}</Box>,
        }}
        fixedItemHeight={82}
        followOutput="auto"
        itemContent={(index, { id, attributes }: PublicationQueryInterface) => {
          const { title, subject, publishedAt, relation } = attributes;
          let categoryName;
          if (subject?.data?.attributes?.name) {
            categoryName = subject.data.attributes.name;
          }

          const publicationUrl = `${baseUrl}/${id}`;
          const thumbnail = getUrlStrapiImage(relation, "thumbnail");

          let formattedPublishedAt;
          if (publishedAt) {
            formattedPublishedAt = dateDescription(
              new Date(publishedAt),
              t("timeDescription", { returnObjects: true }),
            );
          }
          return (
            <VerticalList
              key={index}
              button
              onClick={() => router.push(publicationUrl)}
              disableGutters
            >
              <HorizontalCard
                avatar={
                  <AvatarImage
                    alt="image-publication"
                    src={thumbnail || undefined}
                    variant="rounded"
                  >
                    <SvgIconAux fontSize={20} icon="newspaper" />
                  </AvatarImage>
                }
                primary={<Title variant={TextVariantEnum.BODY1}>{title}</Title>}
                secondary={
                  <Box>
                    <Badge
                      description={<Text variant={TextVariantEnum.CAPTION}>{categoryName}</Text>}
                      bgColor={theme.palette.primary.main}
                      textColor={theme.palette.primary.contrastText}
                    />
                    <DateDescription variant={TextVariantEnum.CAPTION}>
                      {formattedPublishedAt}
                    </DateDescription>
                  </Box>
                }
                options={options}
              />
            </VerticalList>
          );
        }}
      />
    </WrapperVirtuoso>
  );
}

export default memo(PublicationList);
