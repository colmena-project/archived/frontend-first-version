import React from "react";
import HorizontalCard from "@/components/ui/HorizontalCard";
import Text from "@/components/ui/Text";
import { Box, makeStyles } from "@material-ui/core";
import MediaGroupAvatar from "@/components/pages/honeycomb/RoomAvatar/MediaGroup";
import { GroupInterface } from "@/interfaces/keycloak";
import { empty, getLanguageTranslate } from "@/utils/utils";
import { useTranslation } from "next-i18next";
import Clickable from "@/components/ui/Clickable";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  media: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%",
    marginBottom: theme.spacing(1),
  },
  mediaName: {
    color: theme.palette.primary.main,
  },
  avatar: {
    marginRight: theme.spacing(1),
  },
}));

type Props = {
  media: GroupInterface;
  mediaPublicationsUrl?: string;
};
export default function MediaItem({ media, mediaPublicationsUrl = "/publications/media" }: Props) {
  const classes = useStyles();
  const router = useRouter();
  const { t } = useTranslation("common");
  const { id, name, attributes } = media;
  let language;
  if (attributes.language && attributes.language.length > 0 && !empty(attributes.language[0])) {
    const translatedLanguage = getLanguageTranslate(t, attributes.language[0]);
    if (translatedLanguage) {
      language = translatedLanguage.language;
    }
  }

  return (
    <Clickable
      handleClick={() => router.push(`${mediaPublicationsUrl}/${name}`)}
      className={classes.media}
    >
      <HorizontalCard
        primary={<Text className={classes.mediaName}>{name}</Text>}
        secondary={language}
        avatar={
          <Box className={classes.avatar}>
            <MediaGroupAvatar
              token={id}
              name={name}
              displayName={name}
              canDeleteConversation={false}
              width={64}
              height={64}
            />
          </Box>
        }
      />
    </Clickable>
  );
}
