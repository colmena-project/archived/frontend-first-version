import React from "react";
import { GroupInterface } from "@/interfaces/keycloak";
import { Box, makeStyles } from "@material-ui/core";
import MediaItem from "@/components/pages/publication/media/MediaItem";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    flexWrap: "wrap",
    justifyContent: "center",
    paddingTop: theme.spacing(2),
    padding: "0 10px",
  },
}));

type Props = {
  medias: GroupInterface[];
  mediaPublicationsUrl?: string;
};
export default function MediaList({ medias, mediaPublicationsUrl }: Props) {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {medias &&
        medias.map((media) => (
          <MediaItem media={media} mediaPublicationsUrl={mediaPublicationsUrl} />
        ))}
    </Box>
  );
}
