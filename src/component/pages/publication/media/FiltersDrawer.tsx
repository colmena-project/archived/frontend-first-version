import React, { useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import { v4 as uuid } from "uuid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ButtonVariantEnum } from "@/enums/*";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useTranslation } from "react-i18next";
import { Button, FormControl, InputLabel, MenuItem } from "@material-ui/core";
import { Formik, Form } from "formik";
import Select from "@material-ui/core/Select";
import { MediaFiltersProps } from "@/interfaces/index";
import { LanguageProps } from "@/types/*";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiAccordionSummary-root": {
        backgroundColor: theme.palette.gray.light,
      },
      "& .MuiAccordion-root": {
        boxShadow: "none",
      },
      "& .MuiListItem-root": {
        padding: 0,
      },
      "& .MuiFormControlLabel-root": {
        width: "100%",
      },
      "& .MuiIconButton-root.Mui-checked": {
        color: theme.palette.variation1.main,
      },
      "& .MuiFormControl-root": {
        marginBottom: theme.spacing(3),
      },
    },
    filterButton: {
      borderRadius: 0,
      backgroundColor: theme.palette.variation2.main,
      color: theme.palette.variation2.contrastText,
      width: "100%",
      boxShadow: "none",
      textTransform: "inherit",
      fontSize: "1rem",
    },
    select: {
      minWidth: 200,
      width: "100%",
    },
  }),
);

type Props = {
  open: boolean;
  handleClose: () => void;
  filterMedias: (filters: MediaFiltersProps) => void;
  initialFilters: MediaFiltersProps;
  languages?: LanguageProps[];
  countries?: string[];
};

export default function MediaFiltersDrawer({
  open,
  filterMedias,
  handleClose,
  initialFilters,
  languages,
  countries,
}: Props) {
  const classes = useStyles();
  const { t } = useTranslation("publication");
  const [expanded, setExpanded] = useState<string | boolean>("filter");

  const handleChange = (panel: string) => (event: any, newExpanded: string | boolean) => {
    setExpanded(newExpanded ? panel : false);
  };
  const handleSubmit = async (filters: MediaFiltersProps) => {
    filterMedias(filters);
    handleClose();
  };

  return (
    <Drawer anchor="right" open={open} onClose={handleClose} className={classes.root}>
      <Formik initialValues={initialFilters ?? {}} onSubmit={handleSubmit}>
        {({ values, setFieldValue }: any) => (
          <Form>
            <Accordion expanded={expanded === "filter"} onChange={handleChange("filter")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="filters-content"
                id="filters-header"
              >
                <Typography>{t("filters.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <List>
                  {countries && (
                    <ListItem key={uuid()}>
                      <FormControl variant="outlined">
                        <InputLabel>{t("country")}</InputLabel>
                        <Select
                          value={values?.country}
                          onChange={(e) => setFieldValue("country", e.target.value)}
                          label={t("country")}
                          className={classes.select}
                        >
                          <MenuItem value="" key={uuid()}>
                            <em>{t("filters.all")}</em>
                          </MenuItem>
                          {countries &&
                            countries.map((country) => (
                              <MenuItem value={country} key={uuid()}>
                                {country}
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </ListItem>
                  )}

                  {languages && (
                    <ListItem key={uuid()}>
                      <FormControl variant="outlined">
                        <InputLabel>{t("filters.language")}</InputLabel>
                        <Select
                          value={values?.language}
                          onChange={(e) => setFieldValue("language", e.target.value)}
                          label={t("filters.language")}
                          className={classes.select}
                        >
                          <MenuItem value="" key={uuid()}>
                            <em>{t("filters.all")}</em>
                          </MenuItem>
                          {languages &&
                            languages.map((language) => (
                              <MenuItem value={language.abbr} key={uuid()}>
                                {language.language}
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </ListItem>
                  )}
                </List>
              </AccordionDetails>
            </Accordion>
            <Button
              variant={ButtonVariantEnum.CONTAINED}
              className={classes.filterButton}
              type="submit"
            >
              {t("filters.filter")}
            </Button>
          </Form>
        )}
      </Formik>
    </Drawer>
  );
}
