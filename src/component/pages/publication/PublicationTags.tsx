import React from "react";
import { Box, makeStyles } from "@material-ui/core";
import Badge from "@/components/ui/Badge";
import { useTheme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  tags: {
    padding: "0 10px",
    marginBottom: theme.spacing(1),
  },
  tag: {
    marginRight: 4,
  },
}));

type Props = {
  tags?: string;
};

export default function PublicationTags({ tags }: Props) {
  const theme = useTheme();
  const classes = useStyles();

  return (
    <Box className={classes.tags}>
      {tags &&
        tags
          .split(",")
          .map((tag: string) => (
            <Badge
              description={tag.trim()}
              borderWidth={1}
              borderColor={theme.palette.grey["300"]}
              className={classes.tag}
            />
          ))}
    </Box>
  );
}
