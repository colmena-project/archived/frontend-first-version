import React from "react";
import { Box, makeStyles } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: "1.3em",
    fontWeight: "bold",
    marginBottom: theme.spacing(1),
  },
  content: {
    padding: 10,
    marginBottom: theme.spacing(1),
    "& p": {
      margin: "8px 0",
    },
    overflow: "hidden",
    width: "100%",
  },
}));

type Props = {
  title: string;
  content: string;
};

export default function PublicationContent({ title, content }: Props) {
  const classes = useStyles();

  return (
    <Box className={classes.content}>
      <Text variant={TextVariantEnum.H1} className={classes.title}>
        {title}
      </Text>
      <Box
        component="span"
        dangerouslySetInnerHTML={{
          __html: content,
        }}
      />
    </Box>
  );
}
