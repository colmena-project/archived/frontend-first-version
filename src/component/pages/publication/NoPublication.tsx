import React from "react";
import { useTranslation } from "next-i18next";
import Text from "@/components/ui/Text";

import Image from "next/image";
import { Root } from "./styled";

export default function NoPublication() {
  const { t } = useTranslation("publication");

  return (
    <Root>
      <Image alt="No publications" src="/images/no_publication.png" width={120} height={120} />
      <Text>{t("noPublication")}</Text>
    </Root>
  );
}
