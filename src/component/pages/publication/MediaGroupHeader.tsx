import React from "react";
import TitleBox from "@/components/ui/TitleBox";
import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { getAvailableSocialMediasKeysObject, getMediaGroupIcon } from "@/utils/utils";
import { v4 as uuid } from "uuid";
import SvgIcon from "@/components/ui/SvgIcon";
import Clickable from "@/components/ui/Clickable";
import { useTheme } from "@material-ui/core/styles";
import MediaGroupAvatar from "@/components/pages/honeycomb/RoomAvatar/MediaGroup";
import { GroupAttributesInterface } from "@/interfaces/keycloak";

type Props = {
  mediaName: string;
  token: string;
  socialMedias: GroupAttributesInterface;
};

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(1),
    width: "100%",
  },
  socialMedias: {
    display: "flex",
    padding: 10,
    boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    justifyContent: "center",
    width: "100%",
  },
  socialMedia: {
    padding: "0 5px",
  },
  avatar: {
    marginRight: theme.spacing(1),
  },
}));

export default function MediaGroupHeader({ mediaName, token, socialMedias }: Props) {
  const classes = useStyles();
  const theme = useTheme();
  const medias = socialMedias as any;

  const allSocialMedias = getAvailableSocialMediasKeysObject().filter(
    (item: any) => medias?.[item]?.[0],
  );

  const openSocialMedia = (socialMediaUrl: string) => {
    window.open(socialMediaUrl, "_blank");
  };

  return (
    <Box className={classes.root} id="media-group-header" component="nav">
      <TitleBox
        title={mediaName}
        align="center"
        avatar={
          <MediaGroupAvatar
            token={token}
            name={mediaName}
            displayName={mediaName}
            canDeleteConversation={false}
            width={45}
            height={45}
            className={classes.avatar}
          />
        }
      />
      {allSocialMedias && (
        <Box className={classes.socialMedias}>
          {allSocialMedias.map((socialMedia: any) => (
            <Box key={uuid()} className={classes.socialMedia}>
              <Clickable handleClick={() => openSocialMedia(medias[socialMedia])}>
                <SvgIcon
                  icon={getMediaGroupIcon(socialMedia)}
                  style={{ fontSize: 24 }}
                  htmlColor={theme.palette.primary.main}
                />
              </Clickable>
            </Box>
          ))}
        </Box>
      )}
    </Box>
  );
}
