import { styled, Box, Avatar, ListItem } from "@material-ui/core";
import Text from "@/components/ui/Text";

export const Root = styled(Box)({
  width: "100%",
  height: "100%",
  display: "flex",
  flex: 1,
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
});

export const WrapperLoading = styled(Box)({
  width: "100%",
  padding: "10px",
});

export const Title = styled(Text)(() => ({
  paddingBottom: 4,
  whiteSpace: "break-spaces",
}));

export const DateDescription = styled(Text)(({ theme }) => ({
  marginLeft: 8,
  color: theme.palette.variation5.dark,
}));

export const AvatarImage = styled(Avatar)(({ theme }) => ({
  marginRight: 8,
  backgroundColor: theme.palette.gray.light,
  width: 60,
  height: 60,
}));

export const WrapperVirtuoso = styled(Box)({
  textAlign: "left",
  alignItems: "stretch",
  width: "100%",
  padding: 3,
  marginTop: 5,
});

export const SearchWrapper = styled(Box)({
  width: "100%",
});

export const SearchRoot = styled(Box)({
  display: "flex",
  padding: "10px",
  width: "100%",
});

export const VerticalList = styled(ListItem)({
  padding: "4px 10px",
});

export const PublicationFilesContainer = styled(Box)(({ theme }) => ({
  padding: "0 10px",
  marginBottom: theme.spacing(1),
  width: "100%",
}));
