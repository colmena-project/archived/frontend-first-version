import React, { useCallback, useState } from "react";
import OutlinedButton from "@/components/ui/Button/OutlinedButton";
import FileMetadataCollapse from "@/components/ui/FileMetadataCollapse";
import { MediaInterface, PublicationFileQueryInterface } from "@/interfaces/cms";
import axios from "axios";
import { downloadFile } from "@/utils/utils";
import getConfig from "next/config";
import { useTranslation } from "next-i18next";
import { v4 as uuid } from "uuid";

const { publicRuntimeConfig } = getConfig();

interface PublicationFileInterface extends PublicationFileQueryInterface {
  file?: File;
}

export default function PublicationFile({ attributes, file }: PublicationFileInterface) {
  const [isDownloading, setIsDownloading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [t] = useTranslation("publication");

  const download = useCallback(async (media: MediaInterface) => {
    setIsDownloading(true);
    setIsOpen(true);
    const baseUrl = publicRuntimeConfig?.publicationsGraphQL?.baseUrl;
    const url = baseUrl + media.data.attributes.url;
    const file = await axios.get(url, { responseType: "blob" });
    downloadFile(file.data, media.data.attributes.name, media.data.attributes.mime);
    setIsDownloading(false);
  }, []);

  return (
    <FileMetadataCollapse
      open={isOpen}
      isCollapsed={false}
      showCollapseButton={false}
      alignActions="center"
      metadata={{
        file,
        size: parseInt(String(attributes.extent_size), 10),
        tags: attributes.tags,
        mime: attributes.media.data.attributes.mime,
        creator: attributes.creator,
        language: attributes.language,
        description: attributes.description,
        title: attributes.title,
      }}
      actions={[
        <OutlinedButton
          title={isDownloading ? `${t("downloading")}..` : t("download")}
          startIcon="download"
          handleClick={() => download(attributes.media)}
          disabled={isDownloading}
          key={uuid()}
        />,
      ]}
    />
  );
}
