import React, { useEffect, useState } from "react";
import { PublicationFileQueryInterface } from "@/interfaces/cms";
import getConfig from "next/config";
import { isAudioFile, isImageFile, removeSpecialCharacters } from "@/utils/utils";
import axios from "axios";
import { createFile as createQuickBlob, findByBasename } from "@/store/idb/models/filesQuickBlob";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import DirectoryList from "@/components/ui/skeleton/DirectoryList";
import PublicationFile from "@/components/pages/publication/PublicationFile";
import { UserIdEnum } from "@/enums/*";
import { PublicationFilesContainer } from "./styled";

const { publicRuntimeConfig } = getConfig();

interface PublicationFileInterface extends PublicationFileQueryInterface {
  file?: File;
}

type Props = {
  files?: Array<PublicationFileQueryInterface>;
};

export default function PublicationFiles({ files }: Props) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx?.user?.id || UserIdEnum.PUBLIC;

  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const items: PublicationFileInterface[] = [];
      if (files) {
        // eslint-disable-next-line no-plusplus
        for (let count = 0; count < files.length; count++) {
          const file: PublicationFileInterface = files[count];
          // eslint-disable-next-line camelcase
          const { media, title, extent_size: extentSize } = file.attributes;
          const name = `${title}-${extentSize}`;
          const basename = `publication-file-${removeSpecialCharacters(name)}`;
          if (
            media &&
            (isImageFile(media.data.attributes.mime) || isAudioFile(media.data.attributes.mime))
          ) {
            let fileContent;
            // eslint-disable-next-line no-await-in-loop
            const cachedFile = await findByBasename(userId, basename);
            if (cachedFile) {
              fileContent = new File([cachedFile.arrayBufferBlob], title ?? "file", {
                type: media.data.attributes.mime,
              });
            } else {
              const baseUrl = publicRuntimeConfig?.publicationsGraphQL?.baseUrl;
              const url = baseUrl + media.data.attributes.url;
              // eslint-disable-next-line no-await-in-loop
              const file = await axios.get(url, { responseType: "arraybuffer" });
              if (file) {
                const arrayBufferBlob = file.data;
                // eslint-disable-next-line no-await-in-loop
                await createQuickBlob({
                  basename,
                  userId,
                  arrayBufferBlob,
                });

                fileContent = new File([arrayBufferBlob], title ?? "file", {
                  type: media.data.attributes.mime,
                });
              }
            }

            items.push({
              ...file,
              file: fileContent,
            } as PublicationFileInterface);
          } else {
            items.push(file);
          }
        }
      }

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      setItems(items);
      setLoading(false);
    })();
  }, [files, userId]);

  return (
    <PublicationFilesContainer>
      {loading && <DirectoryList />}
      {!loading &&
        files &&
        items &&
        items.map((item) => (
          <PublicationFile
            {...(item as unknown as any)}
            key={`PublicationFiles_${(item as File).name}`}
          />
        ))}
    </PublicationFilesContainer>
  );
}
