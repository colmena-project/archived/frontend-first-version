import React, { useEffect, useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import { v4 as uuid } from "uuid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ButtonVariantEnum, PublicationFiltersEnum } from "@/enums/*";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useTranslation } from "react-i18next";
import {
  Button,
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { Formik, Form } from "formik";
import Select from "@material-ui/core/Select";
import { GroupInterface } from "@/interfaces/keycloak";
import useCategories from "@/hooks/cms/useCategories";
import { getGroups } from "@/services/internal/groups";
import { PublicationFiltersProps } from "@/services/cms/publications";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiAccordionSummary-root": {
        backgroundColor: theme.palette.gray.light,
      },
      "& .MuiAccordion-root": {
        boxShadow: "none",
      },
      "& .MuiListItem-root": {
        padding: 0,
      },
      "& .MuiFormControlLabel-root": {
        width: "100%",
      },
      "& .MuiIconButton-root.Mui-checked": {
        color: theme.palette.variation1.main,
      },
      "& .MuiFormControl-root": {
        marginBottom: theme.spacing(3),
      },
    },
    filterButton: {
      borderRadius: 0,
      backgroundColor: theme.palette.variation2.main,
      color: theme.palette.variation2.contrastText,
      width: "100%",
      boxShadow: "none",
      textTransform: "inherit",
      fontSize: "1rem",
    },
    select: {
      minWidth: 200,
      width: "100%",
    },
  }),
);

type Props = {
  open: boolean;
  handleClose: () => void;
  filterPublications: (filters: PublicationFiltersProps) => void;
  initialFilters: PublicationFiltersProps;
  hideFilters?: PublicationFiltersEnum[];
  language: string;
};

export default function PublicationFiltersDrawer({
  open,
  filterPublications,
  handleClose,
  initialFilters,
  hideFilters = [],
  language,
}: Props) {
  const classes = useStyles();
  const { t } = useTranslation("publication");
  const [expanded, setExpanded] = useState<string | boolean>("filter");
  const [groups, setGroups] = useState<GroupInterface[]>([]);
  const [loadingGroups, setLoadingGroups] = useState(false);

  const { categories, loading: loadingCategories } = useCategories(language);
  const handleChange = (panel: string) => (event: any, newExpanded: string | boolean) => {
    setExpanded(newExpanded ? panel : false);
  };
  const handleSubmit = async (filters: PublicationFiltersProps) => {
    filterPublications(filters);
    handleClose();
  };

  useEffect(() => {
    (async () => {
      if (groups.length === 0) {
        setLoadingGroups(true);
        const groups = await getGroups();
        if (groups && groups.data.success) {
          setGroups(groups.data.data);
        }

        setLoadingGroups(false);
      }
    })();
  }, [groups]);

  return (
    <Drawer anchor="right" open={open} onClose={handleClose} className={classes.root}>
      <Formik initialValues={initialFilters ?? {}} onSubmit={handleSubmit}>
        {({ values, setFieldValue }: any) => (
          <Form>
            <Accordion expanded={expanded === "filter"} onChange={handleChange("filter")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="filters-content"
                id="filters-header"
              >
                <Typography>{t("filters.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <List>
                  {!hideFilters?.includes(PublicationFiltersEnum.GROUP) && (
                    <ListItem key={uuid()}>
                      <FormControl variant="outlined">
                        <InputLabel>{t("filters.mediaGroup")}</InputLabel>
                        <Select
                          value={values?.group}
                          onChange={(e) => setFieldValue("group", e.target.value)}
                          label={t("filters.mediaGroup")}
                          className={classes.select}
                          disabled={loadingGroups}
                        >
                          <MenuItem value="" key={uuid()}>
                            <em>{t("filters.all")}</em>
                          </MenuItem>
                          {groups &&
                            groups.map((group) => (
                              <MenuItem value={group.name} key={uuid()}>
                                {group.name}
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </ListItem>
                  )}
                  {!hideFilters?.includes(PublicationFiltersEnum.CATEGORY) && (
                    <ListItem key={uuid()}>
                      <FormControl variant="outlined">
                        <InputLabel>{t("filters.category")}</InputLabel>
                        <Select
                          value={values?.category}
                          onChange={(e) => setFieldValue("category", e.target.value)}
                          label={t("filters.category")}
                          className={classes.select}
                          disabled={loadingCategories}
                        >
                          <MenuItem value="" key={uuid()}>
                            <em>{t("filters.all")}</em>
                          </MenuItem>
                          {categories &&
                            categories.map((category) => (
                              <MenuItem value={category.id} key={uuid()}>
                                {category.name}
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </ListItem>
                  )}
                </List>
              </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === "order"} onChange={handleChange("order")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="order-content"
                id="order-header"
              >
                <Typography>{t("sort.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <RadioGroup
                  name="order"
                  value={values?.order}
                  onChange={(event) => {
                    setFieldValue("order", event.currentTarget.value);
                  }}
                >
                  <List>
                    {!hideFilters?.includes(PublicationFiltersEnum.LATEST_FIRST) && (
                      <ListItem button key={uuid()}>
                        <FormControlLabel
                          value="publishedAt:desc"
                          control={<Radio />}
                          label={t("sort.newest")}
                        />
                      </ListItem>
                    )}
                    {!hideFilters?.includes(PublicationFiltersEnum.OLDEST_FIST) && (
                      <ListItem button key={uuid()}>
                        <FormControlLabel
                          value="publishedAt:asc"
                          control={<Radio />}
                          label={t("sort.oldest")}
                        />
                      </ListItem>
                    )}
                  </List>
                </RadioGroup>
              </AccordionDetails>
            </Accordion>
            <Button
              variant={ButtonVariantEnum.CONTAINED}
              className={classes.filterButton}
              type="submit"
            >
              {t("filters.filter")}
            </Button>
          </Form>
        )}
      </Formik>
    </Drawer>
  );
}
