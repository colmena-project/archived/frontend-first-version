/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import React, { useState, useCallback } from "react";
import { RoomItemInterface } from "@/interfaces/talk";
import { useRouter } from "next/router";
import theme from "@/styles/theme";
import Chip from "@material-ui/core/Chip";
import { markChatAsRead } from "@/services/talk/chat";
import { useTranslation } from "next-i18next";
import { PropsUserSelector } from "@/types/index";
import { useDispatch, useSelector } from "react-redux";
import { ContextMenuOptionsMemoized } from "@/components/pages/honeycomb/Chat/ContextMenu";
import { HoneycombContextOptions, RoomTypeEnum, TextVariantEnum } from "@/enums/*";
import Clickable from "@/components/ui/Clickable";
import ListItem from "@material-ui/core/ListItem";

import {
  addHoneycombArchived,
  editHoneycomb,
  removeHoneycomb,
  removeHoneycombArchived,
} from "@/store/actions/honeycomb";
import SvgIconAux from "@/components/ui/SvgIcon";
import { RoomAvatarMemoized } from "../RoomAvatar";
import { getRoomSubtitle } from "@/utils/talk";
import { Card, CardOptions, CardTitle, FavoriteIcon, Avatar, HoneycombLabel } from "./styled";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getHoneycombLink } from "@/utils/offlineNavigation";

type Props = {
  data: RoomItemInterface;
  archived: boolean;
};

const HoneycombListItem = ({ data, archived = false }: Props) => {
  const { t } = useTranslation("honeycomb");
  const dispatch = useDispatch();
  const [removeItem, setRemoveItem] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const {
    displayName,
    token,
    canDeleteConversation,
    unreadMessages,
    isFavorite,
    type,
    listable,
    lastMessage: { id: lastMessageId },
  } = data;

  const subtitleRoom = getRoomSubtitle(userRdx.user.id, data, t);

  const navigateTo = async () => {
    if (unreadMessages > 0 && connectionStatus) markChatAsRead(token, lastMessageId);
    router.push(
      getHoneycombLink(token, displayName, Number(canDeleteConversation), connectionStatus),
    );
  };

  const handleArchiveConversation = useCallback(() => {
    if (!archived) dispatch(addHoneycombArchived(token));
    else dispatch(removeHoneycombArchived(token));
  }, []);

  const handleLeaveConversation = useCallback(() => {
    setRemoveItem(true);
    dispatch(removeHoneycomb(token));
    if (archived) {
      dispatch(removeHoneycombArchived(token));
    }
  }, []);

  const group = userRdx.user.media.name;

  const blackList = [HoneycombContextOptions.ADD_PARTICIPANT];
  if (group === data.displayName) {
    blackList.push(HoneycombContextOptions.ARCHIVE_CONVERSATION);
  }

  const handleFavorite = useCallback((isFavorite: boolean) => {
    dispatch(editHoneycomb({ ...data, isFavorite }));
  }, []);

  if (removeItem) return null;

  return (
    <ListItem
      key={data.id}
      disableGutters
      style={removeItem ? { display: "none", padding: 0 } : { padding: "2px 10px" }}
    >
      <Card>
        {isFavorite && (
          <FavoriteIcon>
            <SvgIconAux icon="star" fontSize={14} htmlColor="#ffe925" />
          </FavoriteIcon>
        )}
        <Avatar data-testid={`honeycomb-avatar-${data.id}`}>
          <Clickable handleClick={navigateTo} className="text-center">
            <RoomAvatarMemoized
              displayName={displayName}
              name={data.name}
              canDeleteConversation={canDeleteConversation}
              token={token}
              roomType={type}
              width={60}
              height={60}
            />
            {type === RoomTypeEnum.GROUP && (
              <HoneycombLabel variant={TextVariantEnum.CAPTION}>
                {listable <= 0 ? t("roomType.group") : t("roomType.channel")}
              </HoneycombLabel>
            )}
          </Clickable>
        </Avatar>
        <CardTitle
          data-testid={`honeycomb-title-${data.id}`}
          primary={displayName}
          onClick={navigateTo}
          primaryTypographyProps={{
            style: {
              color: theme.palette.primary.dark,
              fontWeight: connectionStatus && unreadMessages > 0 ? "bold" : "normal",
              marginBottom: 2,
              whiteSpace: "normal",
            },
          }}
          secondary={subtitleRoom}
          secondaryTypographyProps={{
            style: {
              fontWeight: connectionStatus && unreadMessages > 0 ? "bold" : "normal",
            },
          }}
        />
        <CardOptions>
          <OnlineOnly>
            {unreadMessages > 0 && (
              <Chip
                label={unreadMessages}
                size="small"
                color="primary"
                data-testid="unread-message-indicator"
              />
            )}
            <ContextMenuOptionsMemoized
              token={token}
              iconColor={theme.palette.gray.dark}
              blackList={blackList}
              handleFallbackLeaveConversation={handleLeaveConversation}
              handleFallbackArchiveConversation={handleArchiveConversation}
              archived={archived}
              displayName={displayName}
              canDeleteConversation={canDeleteConversation}
              isFavorite={isFavorite}
              setIsFavorite={handleFavorite}
              roomType={data.type}
              origin="honeycombs"
            />
          </OnlineOnly>
        </CardOptions>
      </Card>
    </ListItem>
  );
};

export default HoneycombListItem;

export const HoneycombListItemMemoized = React.memo(HoneycombListItem);
