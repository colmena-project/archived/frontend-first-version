/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Card = styled(Box)({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  flexWrap: "nowrap",
  width: "100%",
  background: "#fff",
  padding: 8,
  borderRadius: 5,
  border: "1px solid #eee",
});

export const FavoriteIcon = styled(Box)({
  position: "absolute",
  top: "-8px",
  left: "5px",
});

export const Avatar = styled(ListItemAvatar)({
  display: "flex",
  alignItems: "center",
  width: 65,
});

export const HoneycombLabel = styled(Text)(({ theme }: StyledProps) => ({
  color: theme.palette.variation7.dark,
}));

export const CardTitle = styled(ListItemText)({
  flexDirection: "column",
  flexGrow: 1,
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
  marginLeft: 5,
  marginRight: 5,
});

export const CardOptions = styled(Box)({
  display: "flex",
  flexDirection: "row",
  flexWrap: "nowrap",
});
