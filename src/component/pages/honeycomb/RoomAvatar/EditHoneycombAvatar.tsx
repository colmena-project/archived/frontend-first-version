import React, { useRef, useState, useCallback } from "react";
import Box from "@material-ui/core/Box";
import { useTranslation } from "next-i18next";
import { toast } from "@/utils/notifications";
import Slider from "@material-ui/core/Slider";
import Cropper from "react-easy-crop";
import Modal from "@/components/ui/Modal";
import { ButtonColorEnum, ButtonVariantEnum } from "@/enums/*";
import Button from "@/components/ui/Button";
import { isPNGImage, isJPGImage } from "@/utils/utils";
import getCroppedImg from "@/utils/cropImage";
import SimpleBackdrop from "@/components/ui/Backdrop";
import { Fade, Dialog, Input, makeStyles, useTheme } from "@material-ui/core";
import Clickable from "@/components/ui/Clickable";
import SvgIconAux from "@/components/ui/SvgIcon";
import { removeFile, uploadFileToCMS } from "@/services/cms/file";
import { useMutation } from "@apollo/client";
import { CREATE_ROOM, UPDATE_ROOM } from "@/services/cms/rooms";
import { RoomInterface } from "@/interfaces/cms";

const useStyles = makeStyles(() => ({
  editAvatar: {
    position: "absolute",
    bottom: "0",
    right: "0",
  },
}));

type Props = {
  room: false | RoomInterface;
  refetchRoom: (room: any) => Promise<any> | false;
  token: string;
};

type CroppedAreaProps = {
  width: number;
  height: number;
  x: number;
  y: number;
};

type FileProps = {
  bits: string;
  name: string;
  type: string;
};

export default function EditHoneycombAvatar({ room, refetchRoom, token }: Props) {
  const classes = useStyles();
  const theme = useTheme();
  const [file, setFile] = useState<FileProps>({
    bits: "",
    name: "",
    type: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const inputFileRef = useRef(null);
  const formRef = useRef(null);
  const { t } = useTranslation("honeycomb");
  const { t: c } = useTranslation("common");
  const [showModal, setShowModal] = useState(false);
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState<CroppedAreaProps>({
    width: 0,
    height: 0,
    x: 0,
    y: 0,
  });
  const [createRoomAvatar] = useMutation(CREATE_ROOM);
  const [updateRoomAvatar] = useMutation(UPDATE_ROOM);

  const onSelectFile = (e: any) => {
    if (e.target.files && e.target.files.length > 0) {
      const { type, size, name } = e.target.files[0];

      if (!isPNGImage(type) && !isJPGImage(type)) {
        toast(c("uploadRuleFileType"), "warning");
        return;
      }

      // 20MB limite do NC
      if (size > 20971520) {
        toast(c("uploadRuleFileSize"), "warning");
        return;
      }

      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setFile({
          bits: String(reader.result),
          name,
          type,
        });
      });
      setShowModal(true);
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const handleCloseModal = () => {
    setFile({} as FileProps);
    setShowModal(false);
  };

  const onCropChange = (crop: { x: number; y: number }) => {
    setCrop(crop);
  };

  const onCropComplete = (_: any, croppedAreaPixels: CroppedAreaProps) => {
    setCroppedAreaPixels(croppedAreaPixels);
  };

  const onZoomChange = (zoom: number) => {
    setZoom(zoom);
  };

  const uploadThumb = useCallback(async (image: Blob, name: string, type: string) => {
    const file = new File([image], name, { type });
    const formData = new FormData();

    formData.append("files", file);

    const [response] = await uploadFileToCMS(formData);

    return response.id;
  }, []);

  const showCroppedImage = useCallback(async () => {
    try {
      setIsLoading(true);
      const croppedImage = await getCroppedImg(file.bits, croppedAreaPixels, 0);
      const avatar = await uploadThumb(croppedImage, file.name, file.type);
      if (room) {
        if (room.attributes.avatar.data?.id) {
          await removeFile(room.attributes.avatar.data.id);
        }

        await updateRoomAvatar({
          variables: { id: room.id, token, avatar },
        });
      } else {
        const roomCreated = await createRoomAvatar({
          variables: { token, avatar },
        });
        if (!roomCreated) {
          throw new Error();
        }

        await refetchRoom(roomCreated.data.createRoom.data);
      }

      toast(t("messages.honeycombAvatarSaved"), "success");
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "warning");
    } finally {
      setShowModal(false);
      setIsLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [croppedAreaPixels, file]);

  const openSelectFile = () => {
    if (!formRef || !formRef.current) return;
    if (!inputFileRef || !inputFileRef.current) return;

    const input: HTMLInputElement = inputFileRef.current;
    const form: HTMLFormElement = formRef.current;

    form.reset();
    input.click();
  };

  return (
    <>
      <Clickable
        handleClick={openSelectFile}
        className={classes.editAvatar}
        data-testid="open-select-file"
      >
        <SvgIconAux icon="edit_circle" htmlColor={theme.palette.variation4.main} fontSize={20} />
      </Clickable>
      <form ref={formRef}>
        <Input
          type="file"
          inputRef={inputFileRef}
          onChange={onSelectFile}
          style={{ display: "none" }}
          inputProps={{ "data-testid": "upload-file" }}
        />
      </form>
      <Modal
        open={showModal}
        handleClose={handleCloseModal}
        data-testid="upload-honeycomb-avatar-modal"
        actions={
          <Box display="flex" flex="1" flexDirection="row" justifyContent="space-between">
            <Button
              handleClick={handleCloseModal}
              style={{ margin: 8 }}
              variant={ButtonVariantEnum.OUTLINED}
              color={ButtonColorEnum.SECONDARY}
              title={c("form.cancelButton")}
              data-testid="close-modal-crop-media-avatar"
            />
            <Button
              handleClick={showCroppedImage}
              style={{ margin: 8 }}
              variant={ButtonVariantEnum.CONTAINED}
              title={c("form.submitSaveTitle")}
              data-testid="submit-modal-crop-media-avatar"
              isLoading={isLoading}
            />
          </Box>
        }
      >
        <Box>
          <Box width="100%" height={280}>
            <Cropper
              image={file.bits}
              crop={crop}
              zoom={zoom}
              aspect={4 / 3}
              cropShape="round"
              showGrid
              style={{
                containerStyle: { width: "100%", height: 300 },
                cropAreaStyle: { width: 250, height: 250 },
                // mediaStyle: { width: "100%", height: "100%", margin: "0 auto" },
              }}
              cropSize={{ width: 250, height: 250 }}
              onCropChange={onCropChange}
              onCropComplete={onCropComplete}
              onZoomChange={onZoomChange}
              objectFit="vertical-cover"
            />
          </Box>
          <Box height={30}>
            <Slider
              data-testid="slider-modal-crop-media-avatar"
              value={zoom}
              min={1}
              max={3}
              step={0.1}
              aria-labelledby="Zoom"
              onChange={(e, zoom: number) => onZoomChange(zoom)}
              style={{ paddingTop: 22, paddingBottom: 22, paddingLeft: 0, paddingRight: 0 }}
            />
          </Box>
        </Box>
      </Modal>
      <Dialog open={isLoading} fullWidth>
        <Fade in={isLoading}>
          <SimpleBackdrop open={isLoading} />
        </Fade>
      </Dialog>
    </>
  );
}
