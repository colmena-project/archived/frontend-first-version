import React, { useMemo } from "react";
import Box from "@material-ui/core/Box";
import { makeStyles, useTheme } from "@material-ui/core";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import EditHoneycombAvatar from "./EditHoneycombAvatar";
import { RoomAvatarInterface } from "@/interfaces/index";
import useRoom from "@/hooks/cms/useRoom";
import getConfig from "next/config";
import PolygonAvatar from "@/components/ui/PolygonAvatar";

const { publicRuntimeConfig } = getConfig();

const useStyles = makeStyles(() => ({
  baseAvatar: {
    position: "relative",
  },
}));

const HoneycombAvatar = ({
  token,
  displayName,
  name,
  fontSize,
  width = 64,
  height = 64,
  className,
  canChangeAvatar = false,
}: RoomAvatarInterface) => {
  const baseUrl = publicRuntimeConfig.publicationsGraphQL?.baseUrl;
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { room, refetch } = useRoom(token);
  const theme = useTheme();
  const isMediaGroup = userRdx.user.media && userRdx.user.media.name === displayName;

  const honeycombAvatar = useMemo(() => {
    if (room) {
      const { avatar } = room.attributes;
      try {
        if (typeof avatar?.data?.attributes.formats.thumbnail.url === "string") {
          return `${baseUrl}${avatar.data.attributes.formats.thumbnail.url}`;
        }
      } catch (e) {
        console.log(e);
      }

      return null;
    }

    return null;
  }, [baseUrl, room]);
  return (
    <Box key={`${token}-honeycomb-avatar`} className={classes.baseAvatar}>
      <PolygonAvatar
        name={name}
        className={className}
        width={width}
        height={height}
        url={honeycombAvatar || undefined}
        fontSize={fontSize}
        borderColor={theme.palette.variation5.main}
      />
      {canChangeAvatar && !isMediaGroup && (
        <EditHoneycombAvatar room={room} refetchRoom={refetch} token={token} />
      )}
    </Box>
  );
};

export default HoneycombAvatar;

export const HoneycombAvatarMemoized = React.memo(HoneycombAvatar);
