import React from "react";
import Box from "@material-ui/core/Box";
import { useTheme } from "@material-ui/core";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { RoomAvatarInterface } from "@/interfaces/index";
import useMediaGroupAvatar from "@/hooks/cms/useMediaGroupAvatar";
import PolygonAvatar from "@/components/ui/PolygonAvatar";

const MediaGroupAvatar = ({
  token,
  name,
  fontSize,
  width = 64,
  height = 64,
  className,
}: RoomAvatarInterface) => {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { thumbnail } = useMediaGroupAvatar(token ?? userRdx.user.media.id);
  const theme = useTheme();

  return (
    <Box key={`${token}-media-avatar`}>
      <PolygonAvatar
        name={name}
        className={className}
        width={width}
        height={height}
        url={thumbnail || undefined}
        fontSize={fontSize}
        borderColor={theme.palette.variation5.main}
      />
    </Box>
  );
};

export default MediaGroupAvatar;

export const MediaGroupAvatarMemoized = React.memo(MediaGroupAvatar);
