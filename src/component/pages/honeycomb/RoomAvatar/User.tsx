import React from "react";
import Text from "@/components/ui/Text";
import { RoomAvatarInterface } from "@/interfaces/index";
import { getFirstLettersOfTwoFirstNames } from "@/utils/utils";
import { makeStyles } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
  avatar: {
    position: "relative",
    backgroundColor: theme.palette.variation2.main,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    flex: 2,
    border: "2px solid",
    borderColor: theme.palette.variation2.main,
  },
  letters: {
    display: "flex",
    alignContent: "center",
    alignItems: "center",
    textAlign: "center",
    justifyContent: "space-around",
    color: "#000",
    fontWeight: 600,
    letterSpacing: 1,
  },
}));

type UserAvatarInterface = Pick<
  RoomAvatarInterface,
  "displayName" | "fontSize" | "width" | "height" | "name" | "url"
>;
function UserAvatar({
  displayName,
  fontSize = 23,
  width = 64,
  height = 64,
  name,
  url,
}: UserAvatarInterface) {
  const classes = useStyles();

  return (
    <Avatar alt={`Avatar ${name}`} src={url} className={classes.avatar} style={{ width, height }}>
      <Text className={classes.letters} style={{ height, fontSize }}>
        {getFirstLettersOfTwoFirstNames(displayName)}
      </Text>
    </Avatar>
  );
}

export default UserAvatar;

export const UserAvatarMemoized = React.memo(UserAvatar);
