import React from "react";
import { UserAvatarMemoized } from "./User";
import { HoneycombAvatarMemoized } from "./Honeycomb";
import { RoomAvatarInterface } from "@/interfaces/index";
import { RoomTypeEnum } from "@/enums/*";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { MediaGroupAvatarMemoized } from "@/components/pages/honeycomb/RoomAvatar/MediaGroup";

function RoomAvatar({
  fontSize = 23,
  width = 64,
  height = 64,
  roomType,
  ...props
}: RoomAvatarInterface) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const isMediaGroup = userRdx.user.media && userRdx.user.media.name === props.displayName;

  if (roomType === RoomTypeEnum.CONVERSATION) {
    return <UserAvatarMemoized fontSize={fontSize} width={width} height={height} {...props} />;
  }

  if (isMediaGroup) {
    return (
      <MediaGroupAvatarMemoized fontSize={fontSize} width={width} height={height} {...props} />
    );
  }

  return <HoneycombAvatarMemoized fontSize={fontSize} width={width} height={height} {...props} />;
}

export default RoomAvatar;

export const RoomAvatarMemoized = React.memo(RoomAvatar);
