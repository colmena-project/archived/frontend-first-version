import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Section = styled(Box)(({ theme }: StyledProps) => ({
  marginBottom: theme.spacing(2),
  "& h6": {
    color: theme.palette.variation2.main,
  },
}));

export const SectionItems = styled(Box)(({ theme }: StyledProps) => ({
  marginTop: theme.spacing(1),
}));

export const ItemsNotFound = styled(Text)(({ theme }: StyledProps) => ({
  color: theme.palette.gray.main,
  textAlign: "left",
  fontSize: 12,
  marginLeft: 15,
}));
