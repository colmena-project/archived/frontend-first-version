import React, { useEffect, useState } from "react";
import { RoomItemInterface } from "@/interfaces/talk";
import { ToolbarSectionMemoized } from "@/components/ui/ToolbarSection";
import { searchRooms } from "@/services/talk/room";
import { RoomTypeEnum } from "@/enums/*";
import { useTranslation } from "next-i18next";
import { search } from "@/services/internal/users";
import { MemberInterface } from "@/interfaces/index";
import { CollaboratorListMemoized } from "@/components/pages/honeycomb/CollaboratorList";
import { MembersListMemoized } from "@/components/ui/skeleton/MembersList";
import { Section, SectionItems, ItemsNotFound } from "./styled";
import { HoneycombInViewMemoized } from "@/components/pages/honeycomb/HoneycombInView";

interface honeycombsBySectionInterface {
  groups: RoomItemInterface[];
  channels: RoomItemInterface[];
}

type Props = {
  honeycombs: RoomItemInterface[];
  searchKeyword: string;
};

export default function HoneycombSearch({ honeycombs, searchKeyword }: Props) {
  const { t } = useTranslation("honeycomb");
  const { t: c } = useTranslation("common");
  const [honeycombsBySection, setHoneycombsBySection] = useState(
    {} as honeycombsBySectionInterface,
  );
  const [foundedUsers, setFoundedUsers] = useState<MemberInterface[]>([]);
  const [loadingUsers, setLoadingUsers] = useState(false);

  function searchHoneycomb() {
    const roomsBySection = {} as honeycombsBySectionInterface;
    let filteredHoneycombs = honeycombs;
    if (searchKeyword) {
      filteredHoneycombs = searchRooms(filteredHoneycombs, searchKeyword);
    }

    if (filteredHoneycombs.length > 0) {
      roomsBySection.groups = filteredHoneycombs.filter(
        (item) => item.type === RoomTypeEnum.GROUP && item.listable <= 0,
      );

      roomsBySection.channels = filteredHoneycombs.filter(
        (item) => item.type === RoomTypeEnum.GROUP && item.listable > 0,
      );
    }
    setHoneycombsBySection(roomsBySection);
  }

  useEffect(() => {
    searchHoneycomb();
    (async () => {
      setLoadingUsers(true);
      try {
        const users = await search({
          keyword: searchKeyword,
        });
        setFoundedUsers(users);
      } catch (e) {
        console.log(e);
      } finally {
        setLoadingUsers(false);
      }
    })();
    // ignore
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchKeyword]);

  function WrapperUsers({ children }: { children: React.ReactNode }) {
    return (
      <Section>
        <ToolbarSectionMemoized title={t("conversations")} showRightButton={false} />
        <SectionItems>{children}</SectionItems>
      </Section>
    );
  }

  type WrapperHoneycombsProps = {
    title: string;
    seeAllTitle?: string;
    link?: string;
    children: React.ReactNode;
    type?: "channels" | "groups";
  };

  function WrapperHoneycombs({
    title,
    seeAllTitle = "",
    link = "",
    children,
    type = "groups",
  }: WrapperHoneycombsProps) {
    return (
      <Section>
        <ToolbarSectionMemoized
          link={link}
          title={title}
          seeAllTitle={seeAllTitle}
          showRightButton={type === "channels"}
        />
        <SectionItems>{children}</SectionItems>
      </Section>
    );
  }

  function renderUsers() {
    if (loadingUsers)
      return (
        <WrapperUsers>
          <MembersListMemoized quantity={3} width={64} height={64} />
        </WrapperUsers>
      );

    if (foundedUsers.length === 0)
      return (
        <WrapperUsers>
          <ItemsNotFound>{c("noItemsFound")}</ItemsNotFound>
        </WrapperUsers>
      );

    return (
      <WrapperUsers>
        <CollaboratorListMemoized users={foundedUsers} />
      </WrapperUsers>
    );
  }

  function renderHoneycombs(
    title: string,
    type: "channels" | "groups",
    link = "",
    seeAllTitle = "",
  ) {
    const items = type === "groups" ? honeycombsBySection.groups : honeycombsBySection.channels;

    if (!items)
      return (
        <WrapperHoneycombs title={title}>
          <ItemsNotFound>{c("noItemsFound")}</ItemsNotFound>
        </WrapperHoneycombs>
      );

    if (items.length === 0)
      return (
        <WrapperHoneycombs title={title}>
          <ItemsNotFound>{c("noItemsFound")}</ItemsNotFound>
        </WrapperHoneycombs>
      );

    return (
      <WrapperHoneycombs title={title} seeAllTitle={seeAllTitle} link={link} type={type}>
        <HoneycombInViewMemoized data={items} archived={false} type={type} />
      </WrapperHoneycombs>
    );
  }

  return (
    <>
      {renderUsers()}
      {renderHoneycombs(t("groups"), "groups", "/channels", t("channelsExplore"))}
      {renderHoneycombs(t("channels"), "channels", "/channels", t("channelsExplore"))}
    </>
  );
}

export const HoneycombSearchMemoized = React.memo(HoneycombSearch);
