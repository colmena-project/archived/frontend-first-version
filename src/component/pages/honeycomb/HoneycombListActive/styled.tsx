import { Box, styled } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Header = styled(Box)(({ theme }: StyledProps) => ({
  margin: "0px 10px",
  marginBottom: theme.spacing(2),
  textAlign: "left",
}));
