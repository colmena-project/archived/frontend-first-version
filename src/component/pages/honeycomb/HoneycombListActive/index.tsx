import React, { useState, useEffect } from "react";
import { RoomItemInterface } from "@/interfaces/talk";
import { useSelector } from "react-redux";
import { PropsHoneycombSelector, PropsUserSelector } from "@/types/*";
import { filterHoneycombs } from "@/pages/honeycomb";
import AlertInfoCenter from "@/components/ui/AlertInfoCenter";
import { HoneycombFiltersDrawerMemoized } from "../FiltersDrawer";
import { RoomFilterEnum, RoomOrderEnum } from "@/enums/*";
import { useTranslation } from "next-i18next";
import { FilterButtonMemoized } from "@/components/ui/FilterButton";
import { orderItems, filterItems } from "@/utils/talk";
import { HoneycombSearchMemoized } from "../HoneycombSearch";
import { Header } from "./styled";
import theme from "@/styles/theme";
import HoneycombInView from "@/components/pages/honeycomb/HoneycombInView";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";

type Props = {
  data: RoomItemInterface[];
  searchKeyword?: string;
};
function HoneycombListActive({ data, searchKeyword }: Props) {
  const { t } = useTranslation("honeycomb");
  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const honeycombRdx = useSelector(
    (state: { honeycomb: PropsHoneycombSelector }) => state.honeycomb,
  );
  const honeycombsTokenArchive = honeycombRdx.honeycombsArchived;
  const [honeycombsList, setHoneycombsList] = useState<RoomItemInterface[]>([]);
  const [filtersDrawerIsOpen, setFiltersDrawerIsOpen] = useState(false);
  const [roomFilter, setRoomFilter] = useState<RoomFilterEnum>(RoomFilterEnum.ALL);
  const [roomOrder, setRoomOrder] = useState<RoomOrderEnum>(RoomOrderEnum.HIGHLIGHT);

  useEffect(() => {
    setHoneycombsList(handleHoneycombs(filterHoneycombs(data)));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    const { honeycombs } = honeycombRdx;
    setHoneycombsList(handleHoneycombs(filterHoneycombs(honeycombs)));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [honeycombsTokenArchive, honeycombRdx, roomFilter, roomOrder]);

  useEffect(() => {
    setHoneycombsList(handleHoneycombs(filterHoneycombs(data)));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, searchKeyword, roomFilter, roomOrder]);

  const handleHoneycombs = (honeycombs: RoomItemInterface[]) => {
    if (!honeycombs) {
      return [];
    }

    return orderItems(
      userRdx,
      roomOrder,
      filterItems(
        roomFilter,
        honeycombs.filter((item) => !honeycombsTokenArchive.includes(item.token)),
      ),
    );
  };

  const filterRooms = (filter: RoomFilterEnum) => {
    setRoomFilter(filter);
  };

  const orderRooms = (order: RoomOrderEnum) => {
    setRoomOrder(order);
  };

  return (
    <>
      <OnlineOnly>
        <Header>
          <FilterButtonMemoized
            dataTut="honeycomb-filter"
            title={t("filters")}
            icon="settings_adjust"
            handleClick={() => setFiltersDrawerIsOpen(true)}
            color={theme.palette.secondary.main}
          />
        </Header>
        <HoneycombFiltersDrawerMemoized
          open={filtersDrawerIsOpen}
          handleClose={() => setFiltersDrawerIsOpen(false)}
          filterItems={filterRooms}
          orderItems={orderRooms}
          filter={roomFilter}
          order={roomOrder}
        />
      </OnlineOnly>
      {honeycombsList.length === 0 && <AlertInfoCenter title={c("noItemsFound")} />}

      {!searchKeyword && <HoneycombInView data={honeycombsList} archived={false} />}
      {searchKeyword && (
        <HoneycombSearchMemoized honeycombs={honeycombsList} searchKeyword={searchKeyword} />
      )}
    </>
  );
}

export default HoneycombListActive;

export const HoneycombListActiveMemoized = React.memo(HoneycombListActive);
