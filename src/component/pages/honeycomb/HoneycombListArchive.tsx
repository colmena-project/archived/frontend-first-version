import React, { useState, useEffect } from "react";
import { RoomItemInterface } from "@/interfaces/talk";
import { useSelector } from "react-redux";
import { PropsHoneycombSelector } from "@/types/*";
import { filterHoneycombs } from "@/pages/honeycomb";
import { orderByLastActivity } from "./HoneycombList";
import { searchRooms } from "@/services/talk/room";
import { HoneycombListItemMemoized } from "./HoneycombListItem";

type Props = {
  data: RoomItemInterface[];
  searchKeyword?: string;
};
function HoneycombListArchive({ data, searchKeyword }: Props) {
  const honeycombRdx = useSelector(
    (state: { honeycomb: PropsHoneycombSelector }) => state.honeycomb,
  );
  const [honeycombsList, setHoneycombsList] = useState<RoomItemInterface[]>([]);
  const honeycombsTokenArchive = honeycombRdx.honeycombsArchived;

  useEffect(() => {
    const items = handleHoneycombs(filterHoneycombs(data))
      .sort(orderByLastActivity)
      .filter((item) => honeycombsTokenArchive.includes(item.token));
    setHoneycombsList(items);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [honeycombsTokenArchive]);

  const handleHoneycombs = (honeycombs: RoomItemInterface[]) => {
    let filteredHoneycombs = honeycombs;
    if (searchKeyword) {
      filteredHoneycombs = searchRooms(filteredHoneycombs, searchKeyword);
    }

    return filterHoneycombs(filteredHoneycombs);
  };

  return (
    <>
      {honeycombsList.map((item: RoomItemInterface) => (
        <HoneycombListItemMemoized key={`item-archive-${item.token}`} data={item} archived />
      ))}
    </>
  );
}

export default HoneycombListArchive;

export const HoneycombListArchiveMemoized = React.memo(HoneycombListArchive);
