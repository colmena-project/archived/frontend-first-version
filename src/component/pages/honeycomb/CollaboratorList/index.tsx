import React from "react";
import Box from "@material-ui/core/Box";
import { CollaboratorListItemMemoized } from "@/components/pages/honeycomb/CollaboratorListItem";
import { MemberInterface } from "@/interfaces/index";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { UsersBox } from "./styled";

type Props = {
  users: Array<MemberInterface>;
};

export default function CollaboratorList({ users }: Props) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  return (
    <Box width="100%">
      <UsersBox>
        {users.map((user) => (
          <CollaboratorListItemMemoized
            firstName={user.firstName}
            lastName={user.lastName}
            username={user.username}
            loggedUserId={userRdx.user.id}
          />
        ))}
      </UsersBox>
    </Box>
  );
}

export const CollaboratorListMemoized = React.memo(CollaboratorList);
