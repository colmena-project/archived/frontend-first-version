/* eslint-disable @typescript-eslint/ban-types */
import React, { useState, useEffect } from "react";
import getConfig from "next/config";
import { useSelector } from "react-redux";
import { PropsUserSelector, CallTypesProps } from "@/types/*";
import { removeProtocolFromURL } from "@/utils/utils";
import { toast } from "@/utils/notifications";
import { generateTokenJwt } from "@/services/internal/jitsi";
import { useTranslation } from "next-i18next";
import SmartLoadingJitsi from "./SmartLoadingJitsi";
import constants from "@/constants/index";

const { publicRuntimeConfig } = getConfig();

type Props = {
  roomName: string;
  guestUsername?: string;
  moderator: boolean;
  type?: CallTypesProps;
  handleClose: () => void;
};

interface JitsiOptionsInterface {
  roomName: string;
  height: number;
  parentNode: HTMLDivElement;
  configOverwrite: {
    disableSimulcast: boolean;
    disableDeepLinking: boolean;
    prejoinPageEnabled: boolean;
    enableWelcomePage: boolean;
    toolbarButtons: string[];
    hideAddRoomButton: boolean;
    defaultRemoteDisplayName: string;
    inviteAppName: string;
    disabledSounds: string[];
    disableReactions: boolean;
    startWithAudioMuted: boolean;
    startAudioOnly: boolean;
    hiddenPremeetingButtons: string[];
  };
  jwt?: string;
  userInfo?: {
    displayName: string;
    email: string;
  };
}

declare global {
  interface Window {
    JitsiMeetExternalAPI: any;
  }
}

export default function JitsiAPIConference({
  roomName,
  handleClose,
  moderator,
  type = "call",
  guestUsername,
}: Props) {
  const [loading, setLoading] = useState(true);
  const { t } = useTranslation("call");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const domain = removeProtocolFromURL(publicRuntimeConfig.meet.baseUrl);
  const heightReduced = constants.HEIGHT_REDUCED_HEADER_JITSI;

  const containerStyle = {
    width: "100vw",
    height: `calc(100vh-${heightReduced}px)`,
  };

  const jitsiContainerStyle = {
    display: loading ? "none" : "block",
    ...containerStyle,
  };

  const handleEndingCall = () => {
    handleClose();
    toast(t("messages.callEnded"), "success");
  };

  async function startConference() {
    const jitsiContainerElem = document.getElementById("jitsi-container") as HTMLDivElement;
    const exists = document.body.contains(jitsiContainerElem);
    if (exists && !loading) {
      return;
    }

    try {
      let jwt = "";
      if (!guestUsername) {
        const { id, name, email } = userRdx.user;
        const user = {
          id,
          name,
          email,
          moderator,
        };
        const response = await generateTokenJwt(user, roomName);
        if (!response.data.success) {
          toast(t("messages.userTokenError"), "error");
          console.log(response.data.data);
          setLoading(false);
          handleClose();
          return;
        }
        jwt = response.data.data;
      }

      let options: JitsiOptionsInterface = {
        roomName,
        height: window.innerHeight - heightReduced,
        parentNode: jitsiContainerElem,
        configOverwrite: {
          disableSimulcast: false,
          disableDeepLinking: true,
          prejoinPageEnabled: false,
          enableWelcomePage: false,
          toolbarButtons: [
            "chat",
            "fullscreen",
            "participants-pane",
            "noisesuppression",
            "microphone",
            "profile",
            "hangup",
            "raisehand",
          ],
          hideAddRoomButton: true,
          defaultRemoteDisplayName: "Colmena",
          inviteAppName: "Colmena Conference",
          disabledSounds: ["REACTION_SOUND"],
          disableReactions: true,
          startWithAudioMuted: true,
          startAudioOnly: true,
          hiddenPremeetingButtons: ["microphone"],
        },
      };

      if (type === "virtual-studio") {
        const toolbarButtonsArr = options.configOverwrite.toolbarButtons;
        toolbarButtonsArr.push("recording");
        toolbarButtonsArr.push("livestreaming");
        options = {
          ...options,
          configOverwrite: {
            ...options.configOverwrite,
            toolbarButtons: toolbarButtonsArr,
          },
        };
      }

      if (guestUsername) {
        options = {
          ...options,
          userInfo: {
            displayName: guestUsername,
            email: "",
          },
        };
      } else options.jwt = jwt;

      const api = new window.JitsiMeetExternalAPI(domain, options);
      api.addEventListener("videoConferenceJoined", () => {
        setLoading(false);
      });
      api.addEventListener("errorOccurred", () => {
        setLoading(false);
        toast(t("messages.genericError"), "error");
        handleClose();
      });
      api.addEventListener("readyToClose", () => {
        handleEndingCall();
      });
    } catch (error) {
      toast(t("messages.genericError"), "error");
      console.log(error);
      setLoading(false);
      handleClose();
    }
  }

  useEffect(() => {
    (async () => {
      if (window.JitsiMeetExternalAPI) await startConference();
      else {
        toast(t("messages.genericError"), "error");
        handleClose();
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={containerStyle}>
      {loading && <SmartLoadingJitsi />}
      <div id="jitsi-container" style={jitsiContainerStyle} />
    </div>
  );
}

export const JitsiAPIConferenceMemoized = React.memo(JitsiAPIConference);
