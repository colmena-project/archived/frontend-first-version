/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState } from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core";
import { AvatarMemoized } from "@/components/pages/profile/Avatar";
import { verifySocketIdExists } from "@/services/wss/room";
import { useSelector } from "react-redux";
import { PropsConferenceSelector, PropsUserSelector, ParticipantTypeProps } from "@/types/*";
import { RoomParticipant } from "@/interfaces/talk";
import IconButton from "@/components/ui/IconButton";
import theme from "@/styles/theme";
import { useTranslation } from "next-i18next";
import { PermissionTalkMemberEnum } from "@/enums/*";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 4,
    margin: 0,
  },
  card: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "nowrap",
    width: "100%",
    background: "#EDEDF2",
    margin: 0,
    padding: 4,
    borderRadius: 35,
    border: "1px solid #eee",
    "& .MuiListItemText-primary": {
      color: theme.palette.primary.main,
      fontSize: 17,
      fontWeight: "bold",
    },
  },
  options: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
  },
  avatar: {
    minHeight: 50,
    display: "flex",
    alignItems: "center",
  },
}));

export interface ParticipantAddedConference {
  userId?: string;
  displayName?: string;
  profile: ParticipantTypeProps;
  muted: boolean;
  socketId?: string;
}

function Participant({
  userId,
  displayName,
  profile,
  muted,
  socketId,
}: ParticipantAddedConference) {
  const { t } = useTranslation("honeycomb");
  const [user, setUser] = useState({
    userId,
    displayName,
    profile,
    muted,
    socketId,
  });
  const classes = useStyles();
  const conferenceRdx = useSelector(
    (state: { conference: PropsConferenceSelector }) => state.conference,
  );
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const handleMuted = () => {
    const mutedCtx = !user.muted;
    const audioId = !user.socketId ? `${user.userId}-audio` : `${user.socketId}-audio`;
    const audioElm = document.getElementById(audioId) as HTMLAudioElement;
    audioElm.muted = mutedCtx;
    setUser((user) => ({ ...user, muted: mutedCtx }));
  };

  useEffect(() => {
    if (socketId) {
      (async () => {
        const result = await verifySocketIdExists(socketId);
        const user = result.data.user || null;
        if (user) {
          const participant = conferenceRdx.participantsNC.find(
            (item: RoomParticipant) => item.actorId === user.identity,
          );
          if (participant) {
            setUser({
              userId: user.identity,
              displayName: participant.displayName,
              profile: participant.participantType,
              muted,
              socketId,
            });
          }
        }
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [socketId]);

  return (
    <>
      <ListItem key={user.userId} disableGutters className={classes.root}>
        <Box className={classes.card}>
          <ListItemAvatar className={classes.avatar}>
            <AvatarMemoized size={6} userName={user.displayName} userId={user.userId} />
          </ListItemAvatar>
          <ListItemText
            primary={user.displayName}
            secondary={
              [
                PermissionTalkMemberEnum.OWNER,
                PermissionTalkMemberEnum.MODERATOR,
                PermissionTalkMemberEnum.GUEST_WITH_MODERATOR_PERMISSIONS,
              ].includes(profile)
                ? t("moderatorTitle")
                : ""
            }
          />
          {userRdx.user.id !== user.userId && (
            <Box className={classes.options}>
              <IconButton
                icon={user.muted ? "speaker_fill_disable" : "speaker_fill_enable"}
                handleClick={handleMuted}
                fontSizeIcon="small"
                iconColor={theme.palette.variation5.main}
              />
            </Box>
          )}
        </Box>
      </ListItem>
    </>
  );
}

export const ParticipantMemoized = React.memo(Participant);
