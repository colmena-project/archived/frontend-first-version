import React, { useState, useEffect } from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { currentDirection } from "@/utils/i18n";
import { makeStyles } from "@material-ui/styles";
import SvgIcon from "@/components/ui/SvgIcon";
import Clickable from "@/components/ui/Clickable";
// import { useSelector } from "react-redux";
// import { PropsUserSelector } from "@/types/*";
import { toggleMic } from "@/utils/webRTCHandler";

type StyleProps = {
  width?: string;
  margin?: string;
};

const useStyles = makeStyles({
  footerItemsRtl: {
    display: "flex",
    flexWrap: "nowrap",
    flexDirection: "row-reverse",
    justifyContent: "center",
  },
  footerItemsLtr: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    justifyContent: "center",
  },
  divider: {
    marginLeft: 15,
    marginRight: 15,
  },
  footer: {
    position: "fixed",
    left: 0,
    bottom: 20,
    width: "100%",
    padding: "10px 0px 10px 0px",
    fontSize: 12,
    textAlign: "center",
    "& svg": {
      padding: 10,
      borderRadius: "50%",
      fontSize: 60,
    },
  },
  endCall: {
    backgroundColor: "#F55B3A",
  },
  muteMic: {
    backgroundColor: "#E5E5E5",
  },
});

type Props = {
  handleLeaveCall: () => void;
};

function FooterCall({ handleLeaveCall }: Props) {
  // const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const classes = useStyles();
  const [enableMic, setEnableMic] = useState(false);
  const [language, setLanguage] = useState("");
  const theme = useTheme();
  const match = useMediaQuery(theme.breakpoints.up("sm"));
  const style: StyleProps = {};

  const handleMuted = () => {
    const muted = enableMic;
    toggleMic(!muted);
    setEnableMic(!muted);
  };

  useEffect(() => {
    setLanguage(currentDirection());
  }, [language]);
  if (match) {
    style.width = "65vw";
    style.margin = "0 auto";
  }

  return (
    <>
      <Box style={{ width: "100%", paddingBottom: 48 }} />
      <Box className={classes.footer}>
        <Box
          style={{ margin: 0, ...style }}
          className={language === "rtl" ? classes.footerItemsRtl : classes.footerItemsLtr}
        >
          <Clickable handleClick={handleMuted}>
            <SvgIcon
              icon={enableMic ? "microphone_fill_enable" : "microphone_fill_disable"}
              htmlColor={theme.palette.primary.main}
              className={classes.muteMic}
            />
          </Clickable>
          <Box className={classes.divider}></Box>
          <Clickable handleClick={handleLeaveCall}>
            <SvgIcon icon="end_call" htmlColor="#E5E5E5" className={classes.endCall} />
          </Clickable>
        </Box>
      </Box>
    </>
  );
}

export default FooterCall;
