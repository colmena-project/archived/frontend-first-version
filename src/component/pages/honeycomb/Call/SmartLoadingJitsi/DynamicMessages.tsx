import React, { useEffect, useState, useRef } from "react";
import { MyText } from "./styled";
import { TextVariantEnum } from "@/enums/*";
import { useTranslation } from "next-i18next";

export default function DynamicMessages() {
  const { t } = useTranslation("call");
  const [message, setMessage] = useState("");
  const count = useRef(0);
  const arrMessages = [
    t("awaitingMessages.message1"),
    t("awaitingMessages.message2"),
    t("awaitingMessages.message3"),
    t("awaitingMessages.message4"),
  ];

  useEffect(() => {
    setMessage(arrMessages[count.current]);
    const interval = setInterval(() => {
      let ctr = count.current;
      if (!arrMessages[ctr]) return;

      ctr += 1;
      setMessage(arrMessages[ctr]);
    }, 4000);
    return () => clearInterval(interval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <MyText variant={TextVariantEnum.BODY1}>{message}</MyText>;
}
