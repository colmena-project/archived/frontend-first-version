import { styled } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles/createTheme.d";
import Text from "@/components/ui/Text";
import Box from "@material-ui/core/Box";

export const Base = styled(Box)(({ height, theme }: { height: number; theme: Theme }) => ({
  display: "flex",
  flex: 1,
  height,
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  margin: 0,
  gap: 15,
  padding: 0,
  backgroundColor: theme.palette.primary.dark,
}));

export const MyText = styled(Text)(({ theme }: { theme: Theme }) => ({
  color: theme.palette.primary.light,
}));
