import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Base } from "./styled";
import DynamicMessages from "./DynamicMessages";
import constants from "@/constants/index";

export default function SmartLoadingJitsi() {
  return (
    <Base height={window.innerHeight - constants.HEIGHT_REDUCED_HEADER_JITSI}>
      <DynamicMessages />
      <CircularProgress color="secondary" />
    </Base>
  );
}
