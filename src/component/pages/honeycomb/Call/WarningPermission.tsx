import SvgIcon from "@/components/ui/SvgIcon";
import theme from "@/styles/theme";
import { makeStyles, Theme } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";

const useStyles = makeStyles((theme: Theme) => ({
  permissionInformation: {
    color: theme.palette.primary.main,
    fontSize: 16,
    width: "100%",
  },
  recordingStateTitle: {
    color: theme.palette.primary.main,
    fontSize: 16,
    width: "100%",
  },
  boxInformationPermission: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
}));

type Props = {
  title: string;
};

export default function WarningPermission({ title }: Props) {
  const classes = useStyles();

  return (
    <Box className={classes.boxInformationPermission}>
      <SvgIcon icon="warning" htmlColor={theme.palette.primary.main} />
      <Text variant={TextVariantEnum.CAPTION} className={classes.permissionInformation}>
        {title}
      </Text>
    </Box>
  );
}
