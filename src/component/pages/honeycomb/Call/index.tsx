/* eslint-disable react/button-has-type */
/* eslint-disable @typescript-eslint/no-empty-function */
import React, { useEffect, useState } from "react";
import FlexBox from "@/components/ui/FlexBox";
import { AlignItemsEnum, JustifyContentEnum, TextVariantEnum } from "@/enums/index";
import * as webRTCHandler from "@/utils/webRTCHandler";
import { useSelector } from "react-redux";
import { PropsUserSelector, PropsConferenceSelector, ParticipantConference } from "@/types/*";
import { makeStyles } from "@material-ui/core";
import List from "@material-ui/core/List";
import { ParticipantMemoized, ParticipantAddedConference } from "./Participant";
import Footer from "./Footer";
import ContextualHeader from "@/components/ui/ContextualHeader";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import WarningPermission from "./WarningPermission";
import { useTranslation } from "next-i18next";

const useStyles = makeStyles(() => ({
  root: {
    background: "url(/images/bg_call.png) repeat-y right",
    backgroundColor: "white",
    width: "100%",
  },
  list: {
    textAlign: "left",
    alignItems: "stretch",
    width: "100%",
    padding: 0,
    margin: 0,
  },
  participantContainer: {
    padding: 0,
    margin: 0,
    width: "100%",
  },
  peopleOnCallContainer: {
    marginTop: 10,
    padding: 4,
    display: "flex",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    "& .MuiTypography-caption": {
      color: "gray",
      fontWeight: "bold",
      fontSize: 15,
    },
  },
}));

type Props = {
  isRoomHost: boolean;
  roomId: string;
  handleLeaveCall: () => void;
};

function Call({ isRoomHost, roomId, handleLeaveCall }: Props) {
  const classes = useStyles();
  const [participants, setParticipants] = useState<ParticipantAddedConference[]>([]);
  const conferenceRdx = useSelector(
    (state: { conference: PropsConferenceSelector }) => state.conference,
  );
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [errorPermission, setErrorPermission] = useState(false);
  const { t } = useTranslation("honeycomb");

  useEffect(() => {
    (async () => {
      await webRTCHandler.getLocalAudioPreview(
        isRoomHost,
        userRdx.user.id,
        roomId,
        conferenceRdx.participantsNC,
      );
    })();

    document.addEventListener("error-permission-audio", (e: CustomEvent<{ error: boolean }>) => {
      if (!e.detail) return;
      setErrorPermission(e.detail.error);
    });

    document.addEventListener(
      "room-update-rdx",
      (e: CustomEvent<{ connectedUsers: ParticipantConference[] }>) => {
        if (!e.detail) return;

        const participantsCtx: ParticipantAddedConference[] = [];
        e.detail.connectedUsers.forEach((item) => {
          const part = conferenceRdx.participantsNC.find(
            (item2) => item2.actorId === item.identity,
          );
          if (part) {
            participantsCtx.push({
              userId: item.identity,
              displayName: part.displayName,
              profile: part.participantType,
              muted: false,
              socketId: userRdx.user.id === item.identity ? "" : item.socketId,
            });
          }
        });
        setParticipants(participantsCtx);
      },
    );

    document.addEventListener(
      "add-local-participant",
      (e: CustomEvent<ParticipantAddedConference>) => {
        if (!e.detail) return;
        setParticipants((participants) => [...participants, ...[e.detail]]);
      },
    );

    //   document.addEventListener(
    //     "add-remote-participant",
    //     (e: CustomEvent<ParticipantAddedConference>) => {
    //       if (!e.detail) return;
    //       setTimeout(() => {
    //         console.log("add-remote-participant");
    //         console.log("participants", participants);
    //         const participantsCtx = participants.map((item) => {
    //           if (item.socketId === e.detail.socketId) {
    //             // eslint-disable-next-line no-param-reassign
    //             item = {
    //               ...item,
    //               muted: e.detail.muted,
    //             };
    //           }
    //           return item;
    //         });
    //         console.log("participantsCtx", participantsCtx);
    //         setParticipants((participants) => [...participants, ...participantsCtx]);
    //       }, 1000);
    //     },
    //   );

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <FlexBox
      justifyContent={JustifyContentEnum.FLEXSTART}
      alignItems={AlignItemsEnum.CENTER}
      extraStyle={{ padding: 0, margin: 0 }}
      className={classes.root}
    >
      {errorPermission ? (
        <WarningPermission title={t("conference.errorPermission")} />
      ) : (
        <>
          <ContextualHeader type="INFO" title={t("conference.callInProgress")} />
          <Box className={classes.peopleOnCallContainer}>
            <Text variant={TextVariantEnum.CAPTION}>
              {participants.length > 1
                ? t("conference.peopleOnCall", { qty: participants.length })
                : t("conference.personOnCall")}
            </Text>
          </Box>
          <Box className={classes.participantContainer}>
            <List className={classes.list}>
              {participants.map((item) => (
                <ParticipantMemoized {...item} />
              ))}
            </List>
          </Box>
          <Footer handleLeaveCall={handleLeaveCall} />
        </>
      )}
    </FlexBox>
  );
}

export default Call;
