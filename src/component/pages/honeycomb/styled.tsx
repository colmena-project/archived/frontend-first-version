import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Content = styled(Box)(({ theme }: StyledProps) => ({
  marginTop: theme.spacing(2),
}));

export const SearchContent = styled(Box)({
  marginLeft: "10px",
  marginRight: "10px",
});
