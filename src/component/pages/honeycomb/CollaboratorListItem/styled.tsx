import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import UserAvatar from "@/components/pages/profile/Avatar";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Avatar = styled(UserAvatar)({
  width: "100%",
  textAlign: "center",
  marginBottom: 5,
});

export const UserBox = styled(Box)({
  width: 90,
  paddingLeft: 10,
  paddingRight: 10,
  textAlign: "center",
});

export const AvatarName = styled(Text)(({ theme }: StyledProps) => ({
  color: theme.palette.primary.dark,
  textAlign: "center",
}));
