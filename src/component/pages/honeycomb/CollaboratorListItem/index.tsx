import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import { TextDisplayEnum, TextVariantEnum } from "@/enums/*";
import { ProfileModalMemoized } from "@/components/ui/ProfileModal";
import Clickable from "@/components/ui/Clickable";
import { Avatar, UserBox, AvatarName } from "./styled";

type Props = {
  firstName: string;
  lastName: string;
  username: string;
  loggedUserId: string;
};

export default function CollaboratorListItem({
  firstName,
  lastName,
  username,
  loggedUserId,
}: Props) {
  const [openProfileModal, setOpenProfileModal] = useState(false);
  const fullName = `${firstName} ${lastName}`;
  return (
    <Box
      key={username}
      justifyContent="center"
      alignItems="center"
      display="flex"
      flexDirection="column"
    >
      <Clickable handleClick={() => setOpenProfileModal(true)}>
        <UserBox>
          <Avatar size={8} userId={username} userName={fullName} />
          <AvatarName variant={TextVariantEnum.BODY1} display={TextDisplayEnum.BLOCK} noWrap>
            {firstName}
          </AvatarName>
        </UserBox>
      </Clickable>
      <ProfileModalMemoized
        open={openProfileModal}
        userName={fullName}
        conversationName={fullName}
        userId={username}
        showTalkOption={loggedUserId !== username}
        closeProfileModal={() => setOpenProfileModal(false)}
      />
    </Box>
  );
}

export const CollaboratorListItemMemoized = React.memo(CollaboratorListItem);
