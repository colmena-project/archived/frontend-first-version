import React, { useState } from "react";
import { RoomItemInterface } from "@/interfaces/talk";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import { useSelector } from "react-redux";
import { PropsHoneycombSelector } from "@/types/*";
import { ArchiveButtonMemoized } from "./ArchiveButton";
import { HoneycombListActiveMemoized } from "./HoneycombListActive";
import { HoneycombListArchiveMemoized } from "./HoneycombListArchive";
import HoneycombsToolsMenu from "@/components/ui/ToolsMenu/Contexts/HoneycombsToolsMenu";
import Box from "@material-ui/core/Box";

type Props = {
  data: RoomItemInterface[];
  searchKeyword?: string;
};

export const orderByLastActivity = (a: RoomItemInterface, b: RoomItemInterface) =>
  b.lastActivity - a.lastActivity;

function HoneycombList({ data, searchKeyword }: Props) {
  const [showArchiveItems, setShowArchiveItems] = useState(false);
  const honeycombRdx = useSelector(
    (state: { honeycomb: PropsHoneycombSelector }) => state.honeycomb,
  );

  const archLength = honeycombRdx.honeycombsArchived.length;
  if (showArchiveItems && archLength === 0) {
    setShowArchiveItems(false);
  }

  return (
    <Box>
      <List>
        {archLength > 0 && showArchiveItems && (
          <ListItem key="archive-button-top" data-testid="archive-honeycomb-back" disableGutters>
            <ArchiveButtonMemoized
              handleClick={() => setShowArchiveItems(!showArchiveItems)}
              back
              amount={archLength}
            />
          </ListItem>
        )}
        {!showArchiveItems && (
          <HoneycombListActiveMemoized data={data} searchKeyword={searchKeyword} />
        )}
        {showArchiveItems && (
          <HoneycombListArchiveMemoized data={data} searchKeyword={searchKeyword} />
        )}
        {archLength > 0 && !showArchiveItems && (
          <ListItem
            key="archive-button-bottom"
            data-testid="archive-honeycomb-enter"
            disableGutters
          >
            <ArchiveButtonMemoized
              handleClick={() => setShowArchiveItems(!showArchiveItems)}
              back={false}
              amount={archLength}
            />
          </ListItem>
        )}
      </List>
      <HoneycombsToolsMenu />
    </Box>
  );
}

export default HoneycombList;

export const HoneycombListMemoized = React.memo(HoneycombList);
