import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ButtonVariantEnum, RoomFilterEnum, RoomOrderEnum } from "@/enums/index";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useTranslation } from "react-i18next";
import { FormControlLabel, Radio, RadioGroup } from "@material-ui/core";
import { Formik, Form } from "formik";
import { Root, FilterButton } from "./styled";

type Props = {
  open: boolean;
  handleClose: () => void;
  filterItems: (filter: null | RoomFilterEnum) => void;
  orderItems: (order: RoomOrderEnum) => void;
  order: RoomOrderEnum;
  filter: RoomFilterEnum;
};

export default function HoneycombFiltersDrawer({
  open,
  filterItems,
  orderItems,
  handleClose,
  order,
  filter,
}: Props) {
  const [expanded, setExpanded] = React.useState<string | boolean>("order");
  const { t } = useTranslation("honeycomb");
  const initialValues = {
    filter,
    order,
  };

  const handleChange = (panel: string) => (event: any, newExpanded: string | boolean) => {
    setExpanded(newExpanded ? panel : false);
  };

  const handleSubmit = ({ filter, order }: any) => {
    filterItems(filter);
    orderItems(order);
    handleClose();
  };

  return (
    <Root anchor="right" open={open} onClose={handleClose}>
      <Formik initialValues={initialValues} onSubmit={handleSubmit}>
        {({ values, setFieldValue }: any) => (
          <Form>
            <Accordion expanded={expanded === "filter"} onChange={handleChange("filter")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="filters-content"
                id="filters-header"
              >
                <Typography>{t("filter.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <RadioGroup
                  name="filter"
                  value={values.filter.toString()}
                  onChange={(event) => {
                    setFieldValue("filter", event.currentTarget.value);
                  }}
                >
                  <List>
                    <ListItem button key={`${RoomFilterEnum.ALL}-key`}>
                      <FormControlLabel
                        value={RoomFilterEnum.ALL}
                        control={<Radio />}
                        label={t("filter.all")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomFilterEnum.UNREADED}-key`}>
                      <FormControlLabel
                        value={RoomFilterEnum.UNREADED}
                        control={<Radio />}
                        label={t("filter.unreaded")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomFilterEnum.ONLY_CONVERSATIONS}-key`}>
                      <FormControlLabel
                        value={RoomFilterEnum.ONLY_CONVERSATIONS}
                        control={<Radio />}
                        label={t("filter.onlyConversations")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomFilterEnum.ONLY_GROUPS}-key`}>
                      <FormControlLabel
                        value={RoomFilterEnum.ONLY_GROUPS}
                        control={<Radio />}
                        label={t("filter.onlyGroups")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomFilterEnum.ONLY_CHANNELS}-key`}>
                      <FormControlLabel
                        value={RoomFilterEnum.ONLY_CHANNELS}
                        control={<Radio />}
                        label={t("filter.onlyChannels")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomFilterEnum.FAVORITES}-key`}>
                      <FormControlLabel
                        value={RoomFilterEnum.FAVORITES}
                        control={<Radio />}
                        label={t("filter.favorites")}
                      />
                    </ListItem>
                  </List>
                </RadioGroup>
              </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === "order"} onChange={handleChange("order")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="order-content"
                id="order-header"
              >
                <Typography>{t("sort.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <RadioGroup
                  name="order"
                  value={values.order.toString()}
                  onChange={(event) => {
                    setFieldValue("order", event.currentTarget.value);
                  }}
                >
                  <List>
                    <ListItem button key={`${RoomOrderEnum.HIGHLIGHT}-key`}>
                      <FormControlLabel
                        value={RoomOrderEnum.HIGHLIGHT}
                        control={<Radio />}
                        label={t("sort.highlights")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomOrderEnum.NEWESTS}-key`}>
                      <FormControlLabel
                        value={RoomOrderEnum.NEWESTS}
                        control={<Radio />}
                        label={t("sort.newests")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomOrderEnum.OLDEST}-key`}>
                      <FormControlLabel
                        value={RoomOrderEnum.OLDEST}
                        control={<Radio />}
                        label={t("sort.oldest")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomOrderEnum.ASC_ALPHABETICAL}-key`}>
                      <FormControlLabel
                        value={RoomOrderEnum.ASC_ALPHABETICAL}
                        control={<Radio />}
                        label={t("sort.alphabeticalOrder")}
                      />
                    </ListItem>
                    <ListItem button key={`${RoomOrderEnum.DESC_ALPHABETICAL}-key`}>
                      <FormControlLabel
                        value={RoomOrderEnum.DESC_ALPHABETICAL}
                        control={<Radio />}
                        label={t("sort.descendingAlphabeticalOrder")}
                      />
                    </ListItem>
                  </List>
                </RadioGroup>
              </AccordionDetails>
            </Accordion>
            <FilterButton variant={ButtonVariantEnum.CONTAINED} type="submit">
              {t("filterButton")}
            </FilterButton>
          </Form>
        )}
      </Formik>
    </Root>
  );
}

export const HoneycombFiltersDrawerMemoized = React.memo(HoneycombFiltersDrawer);
