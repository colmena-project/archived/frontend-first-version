import { styled } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Root = styled(Drawer)(({ theme }: StyledProps) => ({
  "& .MuiAccordionSummary-root": {
    backgroundColor: theme.palette.gray.light,
  },
  "& .MuiAccordion-root": {
    boxShadow: "none",
  },
  "& .MuiListItem-root": {
    padding: 0,
  },
  "& .MuiFormControlLabel-root": {
    width: "100%",
  },
  "& .MuiIconButton-root.Mui-checked": {
    color: theme.palette.variation1.main,
  },
}));

export const FilterButton = styled(Button)(({ theme }: StyledProps) => ({
  borderRadius: 0,
  backgroundColor: theme.palette.variation2.main,
  color: theme.palette.variation2.contrastText,
  width: "100%",
  boxShadow: "none",
  textTransform: "inherit",
  fontSize: "1rem",
}));
