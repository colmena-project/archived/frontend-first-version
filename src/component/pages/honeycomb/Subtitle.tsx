import { getRoomParticipants } from "@/services/talk/room";
import { useTranslation } from "next-i18next";
import Box from "@material-ui/core/Box";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type Props = {
  token: string;
};

export default function Subtitle({ token }: Props) {
  const connectionStatus = useConnectionStatus();
  if (!connectionStatus) return null;

  const { t } = useTranslation("honeycomb");
  const { data } = getRoomParticipants(token, "", {
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });

  function prepareParticipantsString(qty: number) {
    if (qty === 0) return t("noMemberTitle");

    if (qty === 1) return `1 ${t("member")}`;

    return `${qty} ${t("members")}`;
  }

  return (
    <Box component="span" data-testid="honeycomb-subtitle">
      {!data ? "..." : prepareParticipantsString(data.ocs.data.length)}
    </Box>
  );
}
