import React from "react";
import IconButton from "@material-ui/core/IconButton";
import SvgIconAux from "@/components/ui/SvgIcon";
import theme from "@/styles/theme";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";

type Props = {
  handleOpenSearch: () => any;
};

function HeaderOptions({ handleOpenSearch }: Props) {
  return (
    <OnlineOnly key="search">
      <IconButton
        data-tut="honeycomb-search-header"
        onClick={handleOpenSearch}
        aria-label="search"
        color="primary"
        style={{ padding: "0" }}
        data-testid="honeycomb-search-header"
      >
        <SvgIconAux icon="search" htmlColor={theme.palette.primary.main} fontSize={20} />
      </IconButton>
    </OnlineOnly>
  );
}

export default HeaderOptions;
