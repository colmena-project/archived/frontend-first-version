/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-no-bind */
import React from "react";
import Box from "@material-ui/core/Box";
import { PropsUserSelector } from "@/types/*";
import { makeStyles } from "@material-ui/core";
import { getRoomParticipants } from "@/services/talk/room";
import { RoomParticipant } from "@/interfaces/talk";
import { listUsersByGroup } from "@/services/ocs/groups";
import { isModerator } from "@/utils/permissions";
import { useSelector, useDispatch } from "react-redux";
import { setParticipantsNC } from "@/store/actions/conference";
import { v4 as uuid } from "uuid";
import RoomAvatar from "./RoomAvatar";

type Props = {
  token: string;
  displayName: string;
  name: string;
  canDeleteConversation: number;
  roomType?: number;
};

const useStyles = makeStyles(() => ({
  avatar: {
    marginRight: 10,
  },
}));

export default function HoneycombHeader({
  token,
  displayName,
  name,
  canDeleteConversation,
  roomType,
}: Props) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const dispatch = useDispatch();
  const classes = useStyles();

  const group = userRdx.user.media.name;
  const { data } = listUsersByGroup(group, "", {
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });
  const { data: part } = getRoomParticipants(token, "", {
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });

  let participantsAddedHoneycomb: RoomParticipant[] = [];

  if (data && data.ocs && part && part.ocs) {
    participantsAddedHoneycomb = part.ocs.data;
  } else return null;

  dispatch(setParticipantsNC(participantsAddedHoneycomb));

  return (
    <Box className={classes.avatar} key={uuid()}>
      <RoomAvatar
        fontSize={18}
        width={46}
        height={46}
        displayName={displayName}
        canDeleteConversation={canDeleteConversation > 0}
        token={token}
        canChangeAvatar={isModerator(participantsAddedHoneycomb, userRdx.user.id) !== undefined}
        roomType={roomType}
        name={name}
      />
    </Box>
  );
}
