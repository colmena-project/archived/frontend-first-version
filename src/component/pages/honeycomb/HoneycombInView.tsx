import React, { useState, useEffect } from "react";
import { RoomItemInterface } from "@/interfaces/talk";
import { HoneycombListItemMemoized } from "./HoneycombListItem";
import List from "@material-ui/core/List";
import { InView } from "react-intersection-observer";

type Props = {
  data: RoomItemInterface[];
  archived: boolean;
  type?: "groups" | "channels" | "all";
};

export default function HoneycombInView({ data, archived, type = "all" }: Props) {
  const ITEMS_PER_PAGE = 10;
  const [items, setItems] = useState<RoomItemInterface[]>([]);

  const handleMore = (inView: boolean) => {
    if (inView) {
      const pos = items.length;
      setItems([...items, ...data.slice(pos, pos + ITEMS_PER_PAGE)]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  };

  useEffect(() => {
    const arr = data.length < ITEMS_PER_PAGE ? data : data.slice(0, ITEMS_PER_PAGE);
    setItems(arr);
  }, [data]);

  return (
    <List>
      {items.map((item: RoomItemInterface) => (
        <HoneycombListItemMemoized
          key={`${item.token}-item-${type}`}
          data={item}
          archived={archived}
        />
      ))}
      <InView as="div" onChange={handleMore} />
    </List>
  );
}

export const HoneycombInViewMemoized = React.memo(HoneycombInView);
