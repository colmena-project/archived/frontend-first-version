import React from "react";
import Box from "@material-ui/core/Box";
import theme from "@/styles/theme";
import ContextMenu from "@/components/pages/honeycomb/Chat/ContextMenu";
import { HoneycombContextOptions } from "@/enums/*";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";
import { RoomItemInterface } from "@/interfaces/talk";
import IconButtonV2 from "@/components/ui/IconButtonV2";
import { CallTypesProps } from "@/types/*";

type Props = {
  displayName: string;
  token: string;
  room?: RoomItemInterface;
  canDeleteConversation: number;
  handleJoinCall: (isRoomHost: CallTypesProps) => void;
  handleLeaveRoom: () => void;
};

export default function ChatHeaderOptions({
  displayName,
  room,
  token,
  handleJoinCall,
  handleLeaveRoom,
  canDeleteConversation,
}: Props) {
  return (
    <OnlineOnly>
      <Box display="flex" flex={1} justifyContent="flex-end" key="app-bar-menu">
        <IconButtonV2
          icon="call"
          color="#fff"
          handleClick={() => handleJoinCall("call")}
          fontSize={24}
        />
        {room && (
          <ContextMenu
            iconColor={theme.palette.variation5.contrastText}
            token={token}
            handleFallbackLeaveConversation={handleLeaveRoom}
            blackList={[
              HoneycombContextOptions.ARCHIVE_CONVERSATION,
              HoneycombContextOptions.FAVORITE_CONVERSATION,
            ]}
            displayName={displayName}
            canDeleteConversation={canDeleteConversation > 0}
            roomType={room.type}
            key="context-menu-icon"
          />
        )}
      </Box>
    </OnlineOnly>
  );
}
