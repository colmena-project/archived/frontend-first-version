// import React, { useContext } from "react";
import React from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { useTranslation } from "next-i18next";
import IconButtonV2 from "@/components/ui/IconButtonV2";
import theme from "@/styles/theme";
import { useSelector } from "react-redux";
import { ChatMessageItemInterfaceCustom } from "@/interfaces/talk";
import { PropsUserSelector } from "@/types/index";
import { addSingleMessage } from "@/store/idb/models/chat";
import { v4 as uuid } from "uuid";
import classNames from "classnames";
import { Container, BoxForm, Textarea, FormWrapper } from "./styled";

type MyFormValues = {
  message: string;
};

type Props = {
  token: string;
  handleSendMessage: (message: string, referenceId: string) => void;
};

export default function InputSendMessage({ handleSendMessage, token }: Props) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { t: c } = useTranslation("common");

  const initialValues: MyFormValues = {
    message: "",
  };

  const ValidationSchema = Yup.object().shape({
    message: Yup.string().required(c("form.requiredTitle")),
  });

  return (
    <Container>
      <BoxForm>
        <Formik
          initialValues={initialValues}
          validationSchema={ValidationSchema}
          onSubmit={(values: MyFormValues, { setSubmitting, resetForm }: any) => {
            (async () => {
              setSubmitting(true);
              const referenceId = uuid();
              const { message } = values;
              const messageObj: ChatMessageItemInterfaceCustom = {
                token,
                actorType: "users",
                actorId: userRdx.user.id,
                actorDisplayName: userRdx.user.name,
                timestamp: new Date().getTime() / 1000,
                message,
                systemMessage: "",
                messageType: "comment",
                referenceId,
              };
              await addSingleMessage(messageObj);
              document.dispatchEvent(
                new CustomEvent("new-message", {
                  detail: { message: messageObj },
                }),
              );
              await handleSendMessage(message, referenceId);
              setSubmitting(false);
            })();
            resetForm();
          }}
        >
          {({ submitForm, isSubmitting, handleChange, handleBlur, values }: any) => (
            <Form
              onKeyDown={(e) => {
                if (e.ctrlKey && e.key === "Enter") {
                  submitForm();
                }
              }}
              style={{ width: "100%" }}
            >
              <FormWrapper>
                <Textarea
                  aria-label="send message"
                  name="message"
                  id="message"
                  placeholder={c("typeYourMessage")}
                  autoComplete="off"
                  maxRows={5}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.message}
                />
                <IconButtonV2
                  icon="send"
                  fontSize={35}
                  color={!isSubmitting ? theme.palette.primary.main : theme.palette.icon.main}
                  handleClick={submitForm}
                  disabled={isSubmitting}
                  className={classNames("rtl:-scale-x-100")}
                />
              </FormWrapper>
            </Form>
          )}
        </Formik>
      </BoxForm>
    </Container>
  );
}
