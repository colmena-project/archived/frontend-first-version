import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import { TextareaAutosize } from "@material-ui/core";

export const Container = styled(Box)({
  flexDirection: "row",
  width: "100%",
  marginTop: 1,
  marginLeft: 1,
  marginRight: 1,
  paddingBottom: 1,
  backgroundColor: "#fff",
});

export const FormWrapper = styled(Box)({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "flex-end",
  margin: 10,
  flex: 1,
});

export const BoxForm = styled(Box)({
  width: "calc(100% - 50px)",
});

export const Textarea = styled(TextareaAutosize)(({ theme }) => ({
  marginRight: 10,
  width: "100%",
  border: `1px solid ${theme.palette.gray.main}`,
  padding: 10,
  resize: "none",
}));
