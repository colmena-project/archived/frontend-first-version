import React, { useState, useMemo, useCallback } from "react";
import { TextVariantEnum, DirectoryNamesNCEnum } from "@/enums/*";
import FileIcon from "@/components/ui/FileIcon";
import { formatBytes, getExtensionFilename, downloadFile } from "@/utils/utils";
import Text from "@/components/ui/Text";
import theme from "@/styles/theme";
import IconButton from "@/components/ui/IconButton";
import { useRouter } from "next/router";
import { blobFile } from "@/services/webdav/files";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { toast } from "@/utils/notifications";
import CircularProgress from "@material-ui/core/CircularProgress";
import { getFilename } from "@/utils/directory";
import { useTranslation } from "next-i18next";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { Avatar, Card, Description } from "@/components/pages/honeycomb/Chat/Files/styled";

type Props = {
  filename: string;
  size: number;
  mimetype?: string;
  name: string;
  canDeleteConversation: number;
};

export default function Default({ filename, mimetype, name, size, canDeleteConversation }: Props) {
  const { t } = useTranslation("honeycomb");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [loadingDownload, setLoadingDownload] = useState(false);
  const extension = useMemo(() => getExtensionFilename(filename), [filename]);
  const isFile = useMemo(() => extension, [extension]);
  const router = useRouter();
  const connectionStatus = useConnectionStatus();

  const handleOpen = useCallback(() => {
    if (!isFile) {
      router.push(`/library/${filename}`);
    }
  }, [isFile, filename, router]);

  const avatar = useMemo(
    () => (
      <Avatar px={1} onClick={handleOpen}>
        <FileIcon extension={extension} mime={mimetype} type={isFile ? "file" : "directory"} />
      </Avatar>
    ),
    [handleOpen, extension, mimetype, isFile],
  );

  const download = useCallback(async () => {
    try {
      setLoadingDownload(true);
      let fileN = filename;
      if (canDeleteConversation === 1) {
        fileN = fileN.replace(`${DirectoryNamesNCEnum.TALK}/`, "");
      }

      const dataBlob = await blobFile(userRdx.user.id, fileN);
      if (dataBlob instanceof Blob) {
        downloadFile(dataBlob, getFilename(fileN), mimetype);
      }
    } catch (e) {
      console.log(e);
      toast(t("downloadFileFailed"), "error");
    } finally {
      setLoadingDownload(false);
    }
  }, [canDeleteConversation, filename, mimetype, t, userRdx.user.id]);

  return (
    <Card component="span">
      {avatar}
      <Description component="span" onClick={handleOpen}>
        <Text variant={TextVariantEnum.CAPTION}>{name}</Text>
        {isFile && <Text variant={TextVariantEnum.CAPTION}>{formatBytes(size)}</Text>}
      </Description>
      {isFile && !loadingDownload && (
        <IconButton
          icon="download"
          iconStyle={{ fontSize: 20 }}
          handleClick={download}
          iconColor={theme.palette.secondary.main}
          disabled={!connectionStatus}
        />
      )}
      {isFile && loadingDownload && (
        <CircularProgress style={{ marginTop: 10, marginRight: 16 }} size={20} />
      )}
    </Card>
  );
}
