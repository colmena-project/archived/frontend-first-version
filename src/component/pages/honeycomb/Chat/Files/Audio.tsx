/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useMemo, useState } from "react";
import theme from "@/styles/theme";
import Box from "@material-ui/core/Box";
import IconButton from "@/components/ui/IconButton";
import { formatBytes, getExtensionFilename, removeSpecialCharacters } from "@/utils/utils";
import Text from "@/components/ui/Text";
import { TextVariantEnum, EnvironmentEnum } from "@/enums/*";
import CirclePlayer from "@/components/ui/CirclePlayer";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { findByBasename as findQuickBlobByBasename } from "@/store/idb/models/filesQuickBlob";
import FileIcon from "@/components/ui/FileIcon";
import { AvatarWrapper } from "@/components/pages/honeycomb/Chat/Files/styled";

type Props = {
  userId: string;
  filename: string;
  size: number;
  name: string;
  canDeleteConversation: number;
  mimetype: string;
};

export function Audio({ userId, filename, size, name, mimetype }: Props) {
  const [download, setDownload] = useState(false);
  const [file, setFile] = useState();
  const connectionStatus = useConnectionStatus();
  const extension = useMemo(() => getExtensionFilename(filename), [filename]);
  const acessibleFile = useMemo(() => connectionStatus || file, [connectionStatus, file]);
  useEffect(() => {
    (async () => {
      if (file) return;
      const localFile = await findQuickBlobByBasename(userId, removeSpecialCharacters(filename));
      if (localFile) {
        setFile(localFile?.arrayBufferBlob);
      }
    })();
  }, [connectionStatus]);

  return (
    <Box
      component="span"
      display="flex"
      flex={1}
      flexDirection="row"
      padding={1}
      borderRadius={4}
      bgcolor="#f5f5f5"
      alignContent="center"
      justifyContent="space-around"
      height={70}
    >
      {acessibleFile ? (
        <CirclePlayer
          filename={filename}
          environment={EnvironmentEnum.REMOTE}
          download={download}
          setDownload={setDownload}
          file={file}
        />
      ) : (
        <AvatarWrapper>
          <FileIcon extension={extension} mime={mimetype} type="file" />
        </AvatarWrapper>
      )}

      <Box component="span" display="flex" flex={1} flexDirection="column" marginLeft={2}>
        <Box
          component="span"
          display="flex"
          flex={1}
          flexDirection="column"
          justifyContent="center"
        >
          <Text variant={TextVariantEnum.CAPTION}>{name}</Text>
          <Text variant={TextVariantEnum.CAPTION}>{formatBytes(size)}</Text>
        </Box>
      </Box>
      <IconButton
        icon="download"
        iconStyle={{ fontSize: 20 }}
        handleClick={() => setDownload(true)}
        iconColor={theme.palette.secondary.main}
        disabled={!acessibleFile}
      />
    </Box>
  );
}

export const MemoizedAudio = React.memo(Audio);
