import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";

export const AvatarWrapper = styled(Box)({
  width: 40,
  "& .file-icon": {
    width: 35,
  },
});

export const Card = styled(Box)({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  flexWrap: "nowrap",
  width: "100%",
  minHeight: 50,
  borderRadius: 5,
  boxShadow: "none",
  "& .MuiCardContent-root:last-child": {
    padding: 0,
  },
});

export const Description = styled(Box)({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  marginLeft: 5,
  padding: 0,
  overflow: "hidden",
});

export const Avatar = styled(Box)({
  width: 60,
  "& .file-icon": {
    width: 35,
  },
});
