/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import React from "react";
import { ChatMessageItemInterface } from "@/interfaces/talk";
import { parseCookies } from "nookies";
import UserMessage from "./UserMessage";
import InfoMessage from "./InfoMessage";
import { RoomTypeEnum } from "@/enums/*";
import { Box } from "@material-ui/core";

type Props = {
  conversationName?: string;
  item: ChatMessageItemInterface | undefined;
  prevItem: ChatMessageItemInterface | null | undefined;
  canDeleteConversation: number;
  userId: string;
  roomType: RoomTypeEnum;
};

export const ChatListItem = ({
  item,
  prevItem,
  canDeleteConversation,
  userId,
  conversationName,
  roomType,
}: Props) => {
  const cookies = parseCookies();
  const lang = cookies.NEXT_LOCALE || "en";

  if (!item) return null;

  const { systemMessage, actorId } = item;

  const prevMessageIsSameUser = Boolean(
    prevItem &&
      prevItem.actorId === actorId &&
      systemMessage === "" &&
      prevItem?.systemMessage === "",
  );

  return (
    <Box className="w-full py-2">
      {systemMessage === "" ? (
        <UserMessage
          conversationName={conversationName}
          userId={userId}
          prevItem={prevItem}
          prevMessageIsSameUser={prevMessageIsSameUser}
          item={item}
          lang={lang}
          canDeleteConversation={canDeleteConversation}
          roomType={roomType}
        />
      ) : (
        <InfoMessage item={item} lang={lang} />
      )}
    </Box>
  );
};

export const MemoizedChatListItem = React.memo(ChatListItem);
