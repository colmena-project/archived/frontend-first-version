import React, { useState } from "react";
import Clickable from "@/components/ui/Clickable";
import { ChatMessageItemInterface } from "@/interfaces/talk";
import { ListItemAvatar, makeStyles } from "@material-ui/core";
import { AvatarMemoized } from "@/components/pages/profile/Avatar";
import ProfileModal from "@/components/ui/ProfileModal";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

const useStyles = makeStyles(() => ({
  avatar: {
    width: 30,
    height: 30,
    display: "flex",
    minWidth: 46,
  },
}));

type Props = {
  item: ChatMessageItemInterface;
  avatarDirection: "flex-start" | "flex-end";
  conversationName?: string;
  showTalkOption?: boolean;
  prevMessageIsSameUser: boolean;
};

const UserAvatar = ({
  item,
  avatarDirection,
  conversationName,
  showTalkOption = true,
  prevMessageIsSameUser,
}: Props) => {
  const classes = useStyles();
  const connectionStatus = useConnectionStatus();
  const { actorId, id, actorDisplayName } = item;
  const [openProfileModal, setOpenProfileModal] = useState(false);
  if (!prevMessageIsSameUser) {
    return (
      <>
        <ListItemAvatar
          key={id}
          className={classes.avatar}
          style={{ justifyContent: avatarDirection }}
        >
          <Clickable handleClick={connectionStatus ? () => setOpenProfileModal(true) : undefined}>
            <AvatarMemoized size={5} userName={actorDisplayName} userId={actorId} />
          </Clickable>
        </ListItemAvatar>
        <ProfileModal
          open={openProfileModal}
          userName={actorDisplayName}
          conversationName={conversationName}
          userId={actorId}
          showTalkOption={showTalkOption}
          closeProfileModal={() => setOpenProfileModal(false)}
        />
      </>
    );
  }

  return (
    <ListItemAvatar key={id} className={classes.avatar} style={{ justifyContent: avatarDirection }}>
      <span></span>
    </ListItemAvatar>
  );
};

export default UserAvatar;
