import React from "react";
import {
  ChatMessageItemInterface,
  ChatMessageItemMessageParameterInterface,
} from "@/interfaces/talk";
import { makeStyles } from "@material-ui/core";
import { getFormattedDistanceDateFromNow, getFirstname } from "@/utils/utils";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import { currentDirection } from "@/utils/i18n";
import classNames from "classnames";

const useStyles = makeStyles((theme) => ({
  message: {
    color: theme.palette.gray.main,
    position: "relative",
    overflowWrap: "break-word",
    whiteSpace: "pre-wrap",
    display: "inline",
  },
  userName: {
    color: theme.palette.gray.main,
    margin: "0 5px",
    fontWeight: "bold",
  },
  noLeftMargin: {
    marginLeft: 0,
  },
  hourDescription: {
    color: theme.palette.gray.main,
    float: "right",
    display: "inline",
  },
}));

type Props = {
  item: ChatMessageItemInterface;
  lang: string;
};

export default function InfoMessage({ item, lang }: Props) {
  const classes = useStyles();
  const { message, timestamp, referenceId, actorId, id, messageParameters } = item;

  const prepareUserName = (
    item: string,
    messageParameters: ChatMessageItemMessageParameterInterface | any | undefined,
  ) => {
    if (!messageParameters) return "";

    const ctx = item.substr(0, item.length - 1).substr(1, item.length);
    return messageParameters[ctx]?.name;
  };

  const generateActorsAndUsers = () => {
    const arr = ["{user}", "{actor}"];
    Array.from({ length: 20 }).forEach((_, idx) => {
      if (idx !== 0) {
        arr.push(`{user${idx}}`);
        arr.push(`{actor${idx}}`);
      }
    });
    return arr;
  };

  const prepareInfoMessage = (
    message: string,
    messageParameters: ChatMessageItemMessageParameterInterface | undefined,
  ) => {
    const arr = message.split(" ");
    const messageArr: React.ReactNode[] = [];
    arr.forEach((item, idx) => {
      if (generateActorsAndUsers().includes(item))
        messageArr.push(
          <Text
            variant={TextVariantEnum.INHERIT}
            key={`chip${id}-${actorId}-${referenceId}-${item}`}
            className={classNames(classes.userName, idx === 0 ? classes.noLeftMargin : undefined)}
          >
            {getFirstname(prepareUserName(item, messageParameters))}
          </Text>,
        );
      else messageArr.push(`${String(item)} `);
    });

    return (
      <Text variant={TextVariantEnum.BODY2} className={classes.message}>
        {messageArr}
      </Text>
    );
  };

  return (
    <Box style={{ direction: currentDirection() }} className="mt-0 w-full" key={id}>
      <Box component="span">{prepareInfoMessage(message, messageParameters)}</Box>
      <Text variant={TextVariantEnum.CAPTION} className={classes.hourDescription}>
        {getFormattedDistanceDateFromNow(timestamp, lang)}
      </Text>
    </Box>
  );
}
