import React from "react";
import ListItemText from "@material-ui/core/ListItemText";
import {
  ChatMessageItemInterface,
  ChatMessageItemMessageParameterInterface,
} from "@/interfaces/talk";
import { makeStyles } from "@material-ui/core";
import { getFormattedDistanceDateFromNow, isAudioFile } from "@/utils/utils";
import theme from "@/styles/theme";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { RoomTypeEnum, TextVariantEnum } from "@/enums/*";
import { MemoizedAudio } from "@/components/pages/honeycomb/Chat/Files/Audio";
import Default from "../Files/Default";
import { currentDirection } from "@/utils/i18n";

import classNames from "classnames";
import UserAvatar from "./UserAvatar";

const useStyles = makeStyles((theme) => ({
  description: {
    flexDirection: "column",
    flexGrow: 1,
    alignSelf: "stretch",
    overflow: "hidden",
    margin: 0,
    padding: 0,
  },
  messageContent: {
    position: "relative",
    overflowWrap: "break-word",
    whiteSpace: "pre-wrap",
  },
  message: {
    overflowWrap: "anywhere",
    "& a": {
      color: theme.palette.primary.main,
    },
  },
  sameUser: {
    marginTop: "-12px",
  },
  otherUser: {},
  hourDescription: {
    float: "right",
    paddingLeft: theme.spacing(2),
  },
  bgPrimary: {
    backgroundColor: theme.palette.grey[200],
  },
  bgSecondary: {
    backgroundColor: theme.palette.grey[100],
  },
}));

type Props = {
  item: ChatMessageItemInterface;
  prevItem: ChatMessageItemInterface | null | undefined;
  userId: string;
  lang: string;
  canDeleteConversation: number;
  conversationName?: string;
  prevMessageIsSameUser: boolean;
  roomType: RoomTypeEnum;
};

export default function UserMessage({
  userId,
  item,
  lang,
  canDeleteConversation,
  conversationName,
  prevMessageIsSameUser,
  roomType,
}: Props) {
  const classes = useStyles();
  const { message, timestamp, actorDisplayName, actorId, id, messageParameters } = item;

  const prepareCommentWithFile = (
    message: string,
    messageParameters: ChatMessageItemMessageParameterInterface | undefined,
  ): string | React.ReactNode => {
    if (message !== "{file}") return handleMessage(message);

    if (messageParameters) {
      const messageFile = renderMessageFile(messageParameters);
      if (message) {
        return messageFile;
      }
    }

    return message;
  };

  const handleMessage = (message: string) => {
    const handledMessage = message.replace(
      /((?:http:|https:)\/\/\S+\.\S[^.\s,]+)/gi,
      "<a href='$1' target='_blank'>$1</a>",
    );

    if (message === handledMessage) return message;

    return (
      <Box
        component="span"
        className={classes.message}
        dangerouslySetInnerHTML={{
          __html: handledMessage,
        }}
      />
    );
  };

  const renderMessageFile = (messageParameters: ChatMessageItemMessageParameterInterface) => {
    const { file } = messageParameters;
    if (!file) return false;

    const { path, name, size } = file;
    const mimetype = file?.mimetype;
    if (mimetype && isAudioFile(mimetype)) {
      return (
        <MemoizedAudio
          key={`${path}-audio`}
          filename={path}
          name={name}
          size={size}
          canDeleteConversation={canDeleteConversation}
          mimetype={mimetype}
          userId={userId}
        />
      );
    }

    return (
      <Default
        key={`${path}-file`}
        mimetype={mimetype}
        filename={path}
        name={name}
        size={size}
        canDeleteConversation={canDeleteConversation}
      />
    );
  };

  return (
    <div className="w-full" dir={currentDirection()}>
      <Box
        className={classNames(
          "flex w-full",
          prevMessageIsSameUser ? classes.sameUser : classes.otherUser,
        )}
        key={id}
        // flexDirection={userId === actorId ? "row" : "row-reverse"}
        flexDirection="row"
      >
        <UserAvatar
          item={item}
          avatarDirection="flex-start"
          showTalkOption={userId !== actorId && roomType === RoomTypeEnum.GROUP}
          conversationName={roomType === RoomTypeEnum.GROUP ? conversationName : undefined}
          prevMessageIsSameUser={prevMessageIsSameUser}
        />
        <Box
          // className={classNames(
          //   "bg-gray-100 flex w-full p-2 rounded-b-xl",
          //   {
          //     "rounded-ts-xl": userId !== actorId,
          //     "rounded-te-xl": userId === actorId,
          //   },
          //   userId !== actorId ? classes.otherUser : undefined,
          // )}
          className={classNames(
            "bg-gray-100 flex w-full p-3 overflow-hidden",
            prevMessageIsSameUser ? "rounded-xl" : "rounded-b-xl rounded-te-xl",
            userId === actorId ? classes.bgPrimary : classes.bgSecondary,
          )}
        >
          <ListItemText
            data-testid="title"
            className={classes.description}
            primary={
              !prevMessageIsSameUser &&
              userId !== actorId && (
                <Box
                  display="flex"
                  flex={1}
                  justifyContent="space-between"
                  alignItems="center"
                  // flexDirection={userId === actorId ? "row" : "row-reverse"}
                  flexDirection="row"
                  pb={1}
                >
                  <Text variant={TextVariantEnum.BODY2} style={{ fontWeight: "bold" }}>
                    {actorDisplayName}
                  </Text>
                </Box>
              )
            }
            secondary={
              <Box component="span" className={classes.messageContent}>
                {prepareCommentWithFile(message, messageParameters)}
                <Box className={classes.hourDescription}>
                  <Text variant={TextVariantEnum.CAPTION} style={{ color: "#9A9A9A" }}>
                    {getFormattedDistanceDateFromNow(timestamp, lang)}
                  </Text>
                </Box>
              </Box>
            }
            style={{ paddingTop: 0, marginTop: 0 }}
            primaryTypographyProps={{
              style: {
                fontSize: 14,
                fontWeight: "bold",
                color: theme.palette.primary.main,
              },
            }}
            secondaryTypographyProps={{ style: { fontSize: 14, color: "#858585" } }}
          />
        </Box>
      </Box>
    </div>
  );
}
