import React, { useState } from "react";
import MainTitle from "@/components/ui/MainTitle";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";
import Clickable from "@/components/ui/Clickable";
import SvgIconAux from "@/components/ui/SvgIcon";
import Box from "@material-ui/core/Box";
import HoneycombDetailsModal from "@/components/pages/honeycomb/HoneycombDetailsModal";
import { RoomItemInterface } from "@/interfaces/talk";

type Props = {
  room?: RoomItemInterface;
};

export default function ChatTitle({ room }: Props) {
  const [openHoneycombDetailsModal, setOpenHoneycombDetailsModal] = useState(false);
  const openHoneycombDetails = () => {
    setOpenHoneycombDetailsModal(true);
  };

  return (
    <Box display="flex" width="11rem">
      <MainTitle
        textColor="#fff"
        title={room?.displayName ?? ""}
        fontSizeTitle={16}
        data-testid="honeycomb-title"
      />
      {room && (
        <OnlineOnly>
          <Clickable
            handleClick={openHoneycombDetails}
            style={{ marginLeft: 5 }}
            data-testid="honeycomb-more-info"
          >
            <SvgIconAux icon="info_circle" fontSize={14} htmlColor="#fff" />
          </Clickable>
          <HoneycombDetailsModal
            open={openHoneycombDetailsModal}
            closeProfileModal={() => setOpenHoneycombDetailsModal(false)}
            room={room}
          />
        </OnlineOnly>
      )}
    </Box>
  );
}
