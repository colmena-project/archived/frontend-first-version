import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import FlexBox from "@/components/ui/FlexBox";
import AppBarBase from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import theme from "@/styles/theme";

export const Container = styled(FlexBox)({
  paddingTop: "0px !important",
  margin: "0px !important",
  backgroundColor: "#fff",
});

export const ChatWrapper = styled(Box)({
  paddingTop: 40,
  display: "flex",
  flexDirection: "column",
  width: "100vw",
  maxHeight: "calc(100vh - 127px)",
  overflow: "hidden",
});

export const AppBar = styled(AppBarBase)({
  marginTop: 70,
  height: 40,
  backgroundColor: "white",
});

export const ChatTabs = styled(Tabs)({
  backgroundColor: "#F5F5F5",
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
  width: "100vw",
  color: theme.palette.icon.main,
  minHeight: "auto",
});
