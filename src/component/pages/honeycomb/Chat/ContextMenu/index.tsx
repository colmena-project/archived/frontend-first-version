/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/jsx-no-bind */
import React, { useState } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Box from "@material-ui/core/Box";
import { useTranslation } from "react-i18next";
import { toast } from "@/utils/notifications";
import ContextMenuItem from "@/components/ui/ContextMenuItem";
import { HoneycombContextOptions, RoomTypeEnum } from "@/enums/*";
import { PropsUserSelector } from "@/types/index";
import { useSelector } from "react-redux";
import {
  addToFavorite,
  removeConversation,
  removeFromFavorites,
  removeYourselfFromAConversation,
} from "@/services/talk/room";
import Backdrop from "@/components/ui/Backdrop";
import theme from "@/styles/theme";
import { ModalShareLinkMemoized } from "./ModalShareLink";
import { getHoneycombUrl } from "@/services/talk/chat";
import { getBaseUrl } from "@/utils/utils";
import { ModalParticipantsMemoized } from "./ModalParticipants";
import IconButton from "@/components/ui/Button/IconButton";

type PositionProps = {
  mouseX: null | number;
  mouseY: null | number;
};

type Props = {
  token: string;
  displayName: string;
  canDeleteConversation: boolean;
  handleFallbackLeaveConversation?: (() => void) | null;
  handleFallbackArchiveConversation?: (() => void) | null;
  iconColor?: string;
  blackList?: string[];
  archived?: boolean;
  isFavorite?: boolean;
  setIsFavorite?: ((isFavorite: boolean) => void) | null;
  roomType: RoomTypeEnum;
  origin?: "honeycombs" | "chat";
};

const ContextMenuOptions = ({
  token,
  handleFallbackLeaveConversation = null,
  handleFallbackArchiveConversation = null,
  iconColor = "#fff",
  blackList = [],
  archived = false,
  displayName,
  canDeleteConversation,
  isFavorite = false,
  setIsFavorite = null,
  roomType,
  origin = "chat",
}: Props) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const honeycombUrl =
    getBaseUrl() + getHoneycombUrl(token, displayName, canDeleteConversation ? "1" : "0");
  const { t } = useTranslation("honeycomb");
  const { t: c } = useTranslation("common");
  const [openAddParticipant, setOpenAddParticipant] = useState(false);
  const [openShareLinkModal, setOpenShareLinkModal] = useState(false);
  const [showBackdrop, setShowBackdrop] = useState(false);
  const [position, setPosition] = useState<PositionProps>({
    mouseX: null,
    mouseY: null,
  });
  const handleOpenContextMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
    setPosition({
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4,
    });
  };

  const handleCloseContextMenu = () => {
    setAnchorEl(null);
  };

  async function handleLeaveConversation() {
    try {
      setShowBackdrop(true);
      handleCloseContextMenu();
      if (roomType === RoomTypeEnum.CONVERSATION || !canDeleteConversation) {
        await removeYourselfFromAConversation(token);
      } else {
        await removeConversation(token);
      }

      toast(c("doneMessage"), "success");
      if (handleFallbackLeaveConversation) handleFallbackLeaveConversation();
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "error");
    } finally {
      setShowBackdrop(false);
    }
  }

  function handleFavoriteConversation() {
    if (isFavorite) {
      removeFromFavorites(token);
      toast(t("messages.favoriteRemoved"), "success");
    } else {
      addToFavorite(token);
      toast(t("messages.conversationFavorited"), "success");
    }

    handleCloseContextMenu();
    if (setIsFavorite) setIsFavorite(!isFavorite);
  }

  async function handleArchiveConversation() {
    handleCloseContextMenu();
    if (handleFallbackArchiveConversation) handleFallbackArchiveConversation();
  }

  const handleOpenParticipantModal = () => {
    handleCloseContextMenu();
    setOpenAddParticipant(true);
  };

  return (
    <Box display="flex" justifyContent="flex-end">
      <Backdrop open={showBackdrop} />
      <IconButton
        key="more_vertical_contextual_menu"
        icon="more_vertical"
        handleClick={handleOpenContextMenu}
        aria-label="context-menu"
        iconSize={24}
        color={origin === "honeycombs" ? theme.palette.gray.dark : "white"}
      />
      <Menu
        key="simple_menu_contextual_menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        anchorReference="anchorPosition"
        anchorPosition={
          position.mouseY !== null && position.mouseX !== null
            ? { top: position.mouseY, left: position.mouseX }
            : undefined
        }
        onClose={handleCloseContextMenu}
      >
        {!blackList.includes(HoneycombContextOptions.FAVORITE_CONVERSATION) && (
          <MenuItem
            key={HoneycombContextOptions.FAVORITE_CONVERSATION}
            data-testid={HoneycombContextOptions.FAVORITE_CONVERSATION}
            onClick={handleFavoriteConversation}
          >
            {isFavorite ? (
              <ContextMenuItem
                icon="heart_outlined"
                iconColor={theme.palette.variation6.main}
                title={t("contextMenuOptions.removeFromFavorites")}
              />
            ) : (
              <ContextMenuItem
                icon="heart"
                iconColor={theme.palette.variation6.main}
                title={t("contextMenuOptions.addToFavorite")}
              />
            )}
          </MenuItem>
        )}
        {!blackList.includes(HoneycombContextOptions.ADD_PARTICIPANT) && (
          <MenuItem
            key={HoneycombContextOptions.ADD_PARTICIPANT}
            data-testid={HoneycombContextOptions.ADD_PARTICIPANT}
            onClick={handleOpenParticipantModal}
          >
            <ContextMenuItem
              icon="user"
              iconColor={theme.palette.variation6.main}
              title={t("contextMenuOptions.addParticipantContextTitle")}
            />
          </MenuItem>
        )}
        {!blackList.includes(HoneycombContextOptions.ARCHIVE_CONVERSATION) && (
          <MenuItem
            key={HoneycombContextOptions.ARCHIVE_CONVERSATION}
            data-testid={HoneycombContextOptions.ARCHIVE_CONVERSATION}
            onClick={handleArchiveConversation}
          >
            <ContextMenuItem
              icon="archive"
              iconColor={theme.palette.variation6.main}
              title={t(
                !archived
                  ? "contextMenuOptions.archiveConversationContextTitle"
                  : "contextMenuOptions.dearchiveConversationContextTitle",
              )}
            />
          </MenuItem>
        )}
        {!blackList.includes(HoneycombContextOptions.LEAVE_CONVERSATION) && (
          <MenuItem
            key={HoneycombContextOptions.LEAVE_CONVERSATION}
            data-testid={HoneycombContextOptions.LEAVE_CONVERSATION}
            onClick={handleLeaveConversation}
          >
            <ContextMenuItem
              icon="close"
              iconColor={theme.palette.variation6.main}
              title={t("contextMenuOptions.leaveConversationContextTitle")}
              style={{ fontSize: 15 }}
            />
          </MenuItem>
        )}
        {/*! blackList.includes(HoneycombContextOptions.REMOVE_CONVERSATION) &&
          isModerator(participantsIn, userRdx.user.id) && (
            <MenuItem
              key={HoneycombContextOptions.REMOVE_CONVERSATION}
              data-testid={HoneycombContextOptions.REMOVE_CONVERSATION}
              onClick={unavailable}
            >
              <ContextMenuItem
                icon="trash"
                iconColor="tomato"
                title={t("contextMenuOptions.removeConversationContextTitle")}
              />
            </MenuItem>
          ) */}
      </Menu>
      {origin === "chat" && (
        <>
          <ModalShareLinkMemoized
            link={honeycombUrl}
            open={openShareLinkModal}
            handleClose={() => setOpenShareLinkModal(false)}
          />
          {openAddParticipant && (
            <ModalParticipantsMemoized
              userId={userRdx.user.id}
              token={token}
              handleCloseModal={() => setOpenAddParticipant(false)}
            />
          )}
        </>
      )}
    </Box>
  );
};

export default ContextMenuOptions;

export const ContextMenuOptionsMemoized = React.memo(ContextMenuOptions);
