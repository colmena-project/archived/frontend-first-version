import React from "react";
import { getRoomParticipants } from "@/services/talk/room";
import Drawer from "@material-ui/core/Drawer";
// eslint-disable-next-line import/no-cycle
import { ParticipantsMemoized } from "./Participants";
import { RoomParticipant } from "@/interfaces/talk";
import { allUsers } from "@/services/internal/users";
import { MemberInterface } from "@/interfaces/index";

type Props = {
  userId: string;
  token: string;
  handleCloseModal: () => void;
};

export default function ModalParticipants({ token, handleCloseModal, userId }: Props) {
  const { data } = allUsers({
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });

  const { data: part } = getRoomParticipants(token, undefined, {
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  });

  let participantsIn: RoomParticipant[] = [];
  let participantsOut: MemberInterface[] = [];
  if (data && part && part.ocs) {
    participantsIn = part.ocs.data;
    const honeycombParticipants = part.ocs.data.map((item) => item.actorId);
    participantsOut = data.filter(
      (item: MemberInterface) =>
        ![userId, "admin", ...honeycombParticipants].includes(item.username),
    );
  }

  return (
    <Drawer anchor="right" open onClose={handleCloseModal}>
      <ParticipantsMemoized
        token={token}
        participantsIn={participantsIn}
        participantsOut={participantsOut}
      />
    </Drawer>
  );
}

export const ModalParticipantsMemoized = React.memo(ModalParticipants);
