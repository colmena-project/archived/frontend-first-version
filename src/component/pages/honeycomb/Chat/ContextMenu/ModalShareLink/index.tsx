import React from "react";
import Modal from "@/components/ui/Modal";
import { useTranslation } from "next-i18next";
import {
  EmailShareButton,
  EmailIcon,
  TelegramShareButton,
  TelegramIcon,
  WhatsappShareButton,
  WhatsappIcon,
} from "react-share";
import Text from "@/components/ui/Text";
import { Container, MediaTitle, Root } from "./styled";

type Props = {
  open: boolean;
  handleClose: () => void;
  link: string;
};

export default function ModalShareLink({ open, handleClose, link }: Props) {
  const { t } = useTranslation("honeycomb");
  return (
    <Modal
      data-testid="modal-share-link"
      title={t("shareLinkTitle")}
      open={open}
      handleClose={handleClose}
    >
      <Root>
        <EmailShareButton url={link} onShareWindowClose={handleClose}>
          <Container>
            <EmailIcon size={32} round />
            <MediaTitle>
              <Text>E-mail</Text>
            </MediaTitle>
          </Container>
        </EmailShareButton>
        <TelegramShareButton url={link} onShareWindowClose={handleClose}>
          <Container>
            <TelegramIcon size={32} round />
            <MediaTitle>
              <Text>Telegram</Text>
            </MediaTitle>
          </Container>
        </TelegramShareButton>
        <WhatsappShareButton url={link} onShareWindowClose={handleClose}>
          <Container>
            <WhatsappIcon size={32} round />
            <MediaTitle>
              <Text>Whatsapp</Text>
            </MediaTitle>
          </Container>
        </WhatsappShareButton>
      </Root>
    </Modal>
  );
}

export const ModalShareLinkMemoized = React.memo(ModalShareLink);
