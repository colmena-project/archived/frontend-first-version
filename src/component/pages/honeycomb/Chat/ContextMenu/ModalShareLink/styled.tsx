import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";

export const Root = styled(Box)({
  display: "flex",
  flexDirection: "column",
});

export const MediaTitle = styled(Box)({
  display: "flex",
  flexDirection: "column",
});

export const Container = styled(Box)({
  display: "flex",
  flexDirection: "row",
  padding: "6px 4px!important",
});
