/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { ParticipantMemoized } from "./Participant";
import ListItem from "@material-ui/core/ListItem";
import TextField from "@material-ui/core/TextField";
import { useTranslation } from "next-i18next";
import InputAdornment from "@material-ui/core/InputAdornment";
import SvgIcon from "@/components/ui/SvgIcon";
import { RoomParticipant } from "@/interfaces/talk";
import Box from "@material-ui/core/Box";
import { toast } from "@/utils/notifications";
import { useSelector } from "react-redux";
import { addParticipantToConversation } from "@/services/talk/room";
import { TextVariantEnum } from "@/enums/*";
import Backdrop from "@/components/ui/Backdrop";
import { UserListMemoized } from "@/components/ui/skeleton/UsersList";
import { isModerator } from "@/utils/permissions";
import { PropsUserSelector } from "@/types/index";
import { MemberInterface } from "@/interfaces/index";
import { prepareSearchString } from "@/utils/utils";
import { Container, SubheaderTitle, ParticipantTitle, MainList } from "./styled";

type Props = {
  token: string;
  participantsIn: RoomParticipant[];
  participantsOut: MemberInterface[];
};

function Participants({ token, participantsOut, participantsIn }: Props) {
  const [participantsInArr, setParticipantsInArr] = useState<RoomParticipant[]>(participantsIn);
  const [participantsOutArr, setParticipantsOutArr] = useState(participantsOut);
  const [search, setSearch] = useState("");
  const [showBackdrop, setShowBackdrop] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { t } = useTranslation("honeycomb");
  const { t: c } = useTranslation("common");

  useEffect(() => {
    if (search) {
      const partIn = participantsIn.filter(
        (item) =>
          item.actorId.toString().indexOf(search) !== -1 ||
          item.displayName.toString().indexOf(search) !== -1,
      );
      const partOut = participantsOut.filter((item) => {
        const username = prepareSearchString(item.username);
        const name = prepareSearchString(`${item.firstName} ${item.lastName}`);
        const value = prepareSearchString(search);

        return username.indexOf(value) >= 0 || name.indexOf(value) >= 0;
      });
      setParticipantsInArr(partIn);
      setParticipantsOutArr(partOut);
    } else {
      setParticipantsInArr(participantsIn);
      setParticipantsOutArr(participantsOut);
    }
  }, [search]);

  useEffect(() => {
    setParticipantsInArr(participantsIn);
    setParticipantsOutArr(participantsOut);
  }, [participantsIn, participantsOut]);

  const handleInviteParticipantIntern = (user: string, newUser: boolean) => {
    if (newUser) {
      if (!isModerator(participantsIn, userRdx.user.id)) {
        toast(t("onlyModeratorProfileAddParticipants"), "warning");
        return;
      }

      setSearch("");
      handleInviteParticipant(user);
    }
  };

  async function handleInviteParticipant(user: string) {
    try {
      setShowBackdrop(true);
      setSearch("");
      await addParticipantToConversation(token, user);
      setParticipantsOutArr((participantsOut) =>
        participantsOut.filter((item) => item.username !== user),
      );
      setParticipantsInArr((participantsIn) => [
        ...participantsIn,
        ...[{ actorId: user, displayName: user, participantType: 3 }],
      ]);
      toast(t("contextMenuOptions.participantsAddedSuccessfully"), "success");
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "error");
    } finally {
      setShowBackdrop(false);
    }
  }

  const mountListItems = (
    items: string[] | MemberInterface[],
    newUser = false,
    subheaderTitle = "",
  ) => (
    <MainList
      key="box-participants-modal"
      subheader={subheaderTitle ? <SubheaderTitle>{subheaderTitle}</SubheaderTitle> : <span></span>}
    >
      {items.map((item: string | MemberInterface, idx: number) => {
        let username: string;
        if (typeof item === "string") {
          username = item;
        } else {
          username = item.username;
        }

        return (
          <ListItem
            button
            onClick={() => handleInviteParticipantIntern(username, newUser)}
            key={`${username}-participant`}
            disableGutters
            style={{ padding: 1 }}
          >
            <ParticipantMemoized
              user={item}
              secondary={isModerator(participantsInArr, username) ? t("moderatorTitle") : ""}
              backgroundColor={idx % 2 === 0 ? "#fff" : "#F9F9F9"}
            />
          </ListItem>
        );
      })}
    </MainList>
  );

  const showUsersList = () => {
    const lists = [];

    if (!search) {
      const aux = participantsInArr.map((item) => item.actorId);
      lists.push(mountListItems(aux));
    } else {
      if (participantsInArr.length > 0) {
        const aux = participantsInArr.map((item) => item.actorId);
        lists.push(mountListItems(aux, false, t("membersTitle")));
      }
      if (participantsOutArr.length > 0) {
        lists.push(mountListItems(participantsOutArr, true, t("addParticipantsTitle")));
      }
    }

    return lists;
  };

  return (
    <Container>
      <Backdrop open={showBackdrop} />
      <ParticipantTitle variant={TextVariantEnum.H5}>
        {t("contextMenuOptions.addParticipantContextTitle")}
      </ParticipantTitle>
      <Box>
        <TextField
          label={c("honeycombModal.searchUserPlaceholder")}
          variant="outlined"
          onChange={(event) => setSearch(event.target.value)}
          value={search}
          fullWidth
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon icon="search" fontSize="small" htmlColor="#777777" />
              </InputAdornment>
            ),
          }}
          size="small"
        />

        {participantsInArr.length === 0 && !search ? (
          <UserListMemoized />
        ) : (
          <Box style={{ height: "calc(100vh - 130px)", overflow: "scroll" }}>{showUsersList()}</Box>
        )}
      </Box>
    </Container>
  );
}

export default Participants;

export const ParticipantsMemoized = React.memo(Participants);
