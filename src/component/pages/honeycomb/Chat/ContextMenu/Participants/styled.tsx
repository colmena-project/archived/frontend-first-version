import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { styled } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Container = styled(Box)(({ theme }: StyledProps) => ({
  flex: 1,
  display: "flex",
  padding: theme.spacing(2),
  flexDirection: "column",
}));

export const SubheaderTitle = styled(ListSubheader)(({ theme }: StyledProps) => ({
  fontWeight: "bold",
  color: theme.palette.variation2.dark,
  backgroundColor: "#fff",
  paddingLeft: 2,
  paddingTop: 5,
  paddingBottom: 5,
  fontSize: 15,
}));

export const ParticipantTitle = styled(Text)({
  fontWeight: "bold",
  marginBottom: 30,
});

export const MainList = styled(List)({
  textAlign: "left",
  alignItems: "stretch",
  marginTop: 5,
});
