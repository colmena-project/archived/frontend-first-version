/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import React from "react";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import theme from "@/styles/theme";
import { AvatarMemoized } from "@/components/pages/profile/Avatar";
import SvgIcon from "@/components/ui/SvgIcon";
import { MemberInterface } from "@/interfaces/index";
import { Card, Description, Options } from "./styled";

type Props = {
  user: string | MemberInterface;
  backgroundColor: string;
  secondary?: string;
  selected?: boolean;
};

const Participant = ({ user, backgroundColor, selected = false, secondary = "" }: Props) => {
  let name;
  let username;
  if (typeof user === "string") {
    name = user;
    username = user;
  } else {
    name = `${user.firstName} ${user.lastName}`;
    username = user.username;
  }

  return (
    <Card style={{ backgroundColor }}>
      <ListItemAvatar>
        <AvatarMemoized size={5} userId={username} userName={name} />
      </ListItemAvatar>
      <Description
        data-testid={`user-${username}`}
        primary={name}
        secondary={secondary}
        primaryTypographyProps={{ style: { color: theme.palette.primary.dark } }}
      />
      <Options>
        {selected && <SvgIcon icon="tick" htmlColor={theme.palette.secondary.main} />}
      </Options>
    </Card>
  );
};

export default Participant;

export const ParticipantMemoized = React.memo(Participant);
