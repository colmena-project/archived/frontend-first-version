import { styled } from "@material-ui/core/styles";
import { Box, ListItemText } from "@material-ui/core";

export const Card = styled(Box)({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  flexWrap: "nowrap",
  width: "100%",
  padding: 8,
});

export const Description = styled(ListItemText)({
  flexDirection: "column",
  flexGrow: 1,
  alignSelf: "stretch",
  overflow: "hidden",
  marginLeft: 5,
});

export const Options = styled(Box)({
  display: "flex",
  flexDirection: "row",
  flexWrap: "nowrap",
});
