/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-no-bind */
import React, { useCallback, useState } from "react";
import Box from "@material-ui/core/Box";
import { useTranslation } from "next-i18next";
import { JustifyContentEnum, RoomTypeEnum, TextVariantEnum } from "@/enums/index";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views";
import theme from "@/styles/theme";
import InputSendMessage from "@/components/pages/honeycomb/Chat/InputSendMessage";
import { MemoizedChat } from "@/components/pages/honeycomb/Chat/ChatList";
import ReloadChatMessages from "@/components/pages/honeycomb/Chat/ReloadChatMessages";
import { sendChatMessage } from "@/services/talk/chat";
import HoneycombLibrary from "@/components/pages/honeycomb/HoneycombLibrary";
import SvgIcon from "@/components/ui/SvgIcon";
import Text from "@/components/ui/Text";
import { AllIconProps } from "@/types/*";
import classNames from "classnames";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";
import OfflineOnly from "@/components/ui/ConnectionStatus/OfflineOnly";
import { Alert } from "@material-ui/lab";
import { AppBar, ChatTabs, ChatWrapper, Container } from "@/components/pages/honeycomb/Chat/styled";

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
}

type Props = {
  token: string;
  displayName: string;
  canDeleteConversation: number;
  roomType: RoomTypeEnum;
};

function Chat({ token, displayName, canDeleteConversation, roomType }: Props) {
  const { t } = useTranslation("honeycomb");

  const [value, setValue] = useState(0);
  const [showInputMessage, setShowInputMessage] = useState(true);
  const [showReloadMessages, setShowReloadMessages] = useState(false);

  // eslint-disable-next-line @typescript-eslint/ban-types
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setShowInputMessage(newValue === 0);
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  function a11yProps(index: any) {
    return {
      id: `full-width-tab-${index}`,
      "aria-controls": `full-width-tabpanel-${index}`,
    };
  }

  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && <Box>{children}</Box>}
      </div>
    );
  }

  async function sendMessageAPI(message: string, referenceId: string) {
    await sendChatMessage(token, message, referenceId);
  }

  const getTabOption = (title: string, icon: AllIconProps) => (
    <Box className={classNames("flex justify-center items-center space-s-2")}>
      <SvgIcon icon={icon} htmlColor="#727272" fontSize="small" />
      <Text variant={TextVariantEnum.CAPTION}>{title}</Text>
    </Box>
  );

  const handleShowReloadMessages = useCallback(() => {
    setShowReloadMessages(true);
  }, []);

  return (
    <>
      <Container justifyContent={JustifyContentEnum.FLEXSTART}>
        <ChatWrapper>
          <AppBar position="fixed" elevation={0}>
            <ChatTabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              variant="fullWidth"
            >
              <Tab label={getTabOption(t("tab1Title"), "chat")} {...a11yProps(0)} />
              <Tab
                data-testid="honeycomb-chat-library"
                label={getTabOption(t("tab2Title"), "library")}
                {...a11yProps(1)}
              />
            </ChatTabs>
          </AppBar>

          <SwipeableViews
            axis={theme.direction === "rtl" ? "x-reverse" : "x"}
            index={value}
            onChangeIndex={handleChangeIndex}
          >
            <TabPanel value={value} index={0}>
              {showReloadMessages && <ReloadChatMessages token={token} />}
              <MemoizedChat
                token={token}
                conversationName={displayName}
                canDeleteConversation={canDeleteConversation}
                handleShowReloadMessages={handleShowReloadMessages}
                roomType={roomType}
              />
            </TabPanel>
            <TabPanel value={value} index={1}>
              {value === 1 && (
                <>
                  <OnlineOnly>
                    <HoneycombLibrary
                      token={token}
                      conversationName={displayName}
                      canDeleteConversation={canDeleteConversation}
                      roomType={roomType}
                    />
                  </OnlineOnly>
                  <OfflineOnly>
                    <Alert severity="error" data-testid="unavailable-content-error">
                      {t("messages.unavailableContent")}
                    </Alert>
                  </OfflineOnly>
                </>
              )}
            </TabPanel>
          </SwipeableViews>
          {showInputMessage && (
            <InputSendMessage token={token} handleSendMessage={sendMessageAPI} />
          )}
        </ChatWrapper>
      </Container>
    </>
  );
}

export const ChatMemoized = React.memo(Chat);
