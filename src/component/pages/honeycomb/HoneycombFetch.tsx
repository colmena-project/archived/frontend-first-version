import React, { useState, useEffect } from "react";
import { FileListMemoized } from "@/components/ui/skeleton/FileList";
import { getUsersConversations } from "@/services/talk/room";
import AlertInfoCenter from "@/components/ui/AlertInfoCenter";
import { useTranslation } from "next-i18next";
import { HoneycombListMemoized } from "@/components/pages/honeycomb/HoneycombList";

type Props = {
  searchKeyword?: string;
};

function HoneycombFetch({ searchKeyword = "" }: Props) {
  const { t: c } = useTranslation("common");
  const [tokenUuid, setTokenUuid] = useState("");

  const { data, error } = getUsersConversations(
    {
      revalidateIfStale: true,
      revalidateOnMount: true,
      revalidateOnFocus: false,
    },
    tokenUuid,
  );

  useEffect(() => {
    document.addEventListener("reload-honeycombs", (e: CustomEvent<{ uuid: string }>) => {
      if (!e.detail) return;
      setTokenUuid(e.detail.uuid);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!data && !error) return <FileListMemoized />;

  if (error) return <AlertInfoCenter title={c("noItemsFound")} />;

  return <HoneycombListMemoized data={data} searchKeyword={searchKeyword} />;
}

export default HoneycombFetch;

export const HoneycombFetchMemoized = React.memo(HoneycombFetch);
