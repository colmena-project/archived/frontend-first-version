import React from "react";
import SvgIcon from "@/components/ui/SvgIcon";
import { TextVariantEnum } from "@/enums/*";
import theme from "@/styles/theme";
import { useTranslation } from "next-i18next";
import Clickable from "@/components/ui/Clickable";
import { Card, CardContainer, ArchivedTitle, AmountTitle } from "./styled";

type Props = {
  handleClick: () => void;
  back: boolean;
  amount: number;
};

export default function ArchiveButton({ handleClick, back = false, amount }: Props) {
  const { t } = useTranslation("honeycomb");

  return (
    <Card>
      <Clickable handleClick={handleClick}>
        <CardContainer>
          <SvgIcon
            icon={`${!back ? "archive" : "back"}`}
            htmlColor={theme.palette.primary.dark}
            style={{ fontSize: back ? 24 : 60 }}
          />
          <ArchivedTitle variant={TextVariantEnum.CAPTION}>{t("archivedTitle")}</ArchivedTitle>
        </CardContainer>
      </Clickable>
      <AmountTitle variant={TextVariantEnum.CAPTION}>{amount}</AmountTitle>
    </Card>
  );
}

export const ArchiveButtonMemoized = React.memo(ArchiveButton);
