import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Card = styled(Box)({
  display: "flex",
  flex: 1,
  flexDirection: "row",
  alignItems: "center",
  flexWrap: "nowrap",
  width: "100%",
  background: "#fff",
  padding: 4,
  borderRadius: 5,
  border: `1px solid #e2e2e2`,
  justifyContent: "space-between",
});

export const CardContainer = styled(Box)({
  flex: 1,
  display: "flex",
  flexDirection: "row",
  justifyContent: "flex-start",
  paddingLeft: 10,
  alignContent: "center",
  alignItems: "center",
});

export const ArchivedTitle = styled(Text)(({ theme }: StyledProps) => ({
  color: theme.palette.primary.dark,
  fontWeight: "bold",
  fontSize: 16,
  marginLeft: 10,
}));

export const AmountTitle = styled(Text)(({ theme }: StyledProps) => ({
  color: theme.palette.primary.dark,
  fontWeight: "bold",
  fontSize: 16,
  marginLeft: 10,
}));
