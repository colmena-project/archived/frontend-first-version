import React from "react";
import Modal from "@/components/ui/Modal";
import Text from "@/components/ui/Text";
import { Box, makeStyles } from "@material-ui/core";
import { RoomAvatarMemoized } from "./RoomAvatar";
import { RoomItemInterface } from "@/interfaces/talk";
import Subtitle from "./Subtitle";
import { TextVariantEnum } from "@/enums/*";
import useCollaboratorAvatar from "@/hooks/cms/useCollaboratorAvatar";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  honeycombName: {
    color: theme.palette.gray.dark,
    fontSize: "1.1rem",
    fontWeight: "bold",
    textAlign: "center",
  },
  avatar: {
    marginBottom: theme.spacing(1),
    "& .MuiAvatar-root": {
      border: "3px solid",
      borderColor: theme.palette.variation4.main,
    },
  },
  description: {
    color: theme.palette.gray.dark,
    marginTop: theme.spacing(1),
  },
  membersQty: {
    color: theme.palette.gray.main,
    fontWeight: "bold",
  },
}));

type Props = {
  open: boolean;
  room: RoomItemInterface;
  closeProfileModal: () => void;
};

export default function HoneycombDetailsModal({ open, room, closeProfileModal }: Props) {
  const classes = useStyles();
  const { displayName, name, token, canDeleteConversation, description, type } = room;
  const { thumbnail: avatar } = useCollaboratorAvatar(displayName);

  return (
    <Modal data-testid="honeycomb-details-modal" open={open} handleClose={closeProfileModal}>
      <Box className={classes.root}>
        <Box className={classes.avatar}>
          <RoomAvatarMemoized
            width={90}
            height={90}
            name={name}
            displayName={displayName}
            token={token}
            canDeleteConversation={canDeleteConversation}
            canChangeAvatar={canDeleteConversation}
            url={avatar ?? undefined}
            roomType={type}
          />
        </Box>
        <Text className={classes.honeycombName}>{displayName}</Text>
        <Text className={classes.membersQty}>
          <Subtitle token={token} />
        </Text>
        {description && (
          <Text variant={TextVariantEnum.CAPTION} className={classes.description}>
            {description}
          </Text>
        )}
      </Box>
    </Modal>
  );
}
