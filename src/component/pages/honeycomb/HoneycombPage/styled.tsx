import { styled } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles/createTheme";
import LayoutAppBase from "@/components/statefull/LayoutApp";

type StyledProps = {
  theme: Theme;
};

export const LayoutApp = styled(LayoutAppBase)(({ theme }: StyledProps) => ({
  "& header.MuiAppBar-root": {
    background: "url(/images/svg/bg-home.svg) repeat-y right",
    backgroundColor: theme.palette.secondary.main,
    width: "100%",
  },
  "& .MuiTabs-root": {
    minHeight: 40,
    "& .MuiTabs-indicator": {
      display: "none",
    },
    "& .MuiTab-root": {
      color: theme.palette.gray.main,
      minHeight: 40,
    },
  },
  "& .Mui-selected": {
    background: "#fff",
    borderTopRightRadius: "20px",
    borderTopLeftRadius: "20px",
    color: theme.palette.gray.main,
  },
  "& .MuiDialogContent-root:first-child": {
    paddingTop: 0,
  },
}));
