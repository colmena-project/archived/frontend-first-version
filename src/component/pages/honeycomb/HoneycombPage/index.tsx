import React, { useState, useEffect, useCallback } from "react";
import { useTranslation } from "next-i18next";
import { RoomTypeEnum } from "@/enums/*";
import { useRouter } from "next/router";
import Subtitle from "@/components/pages/honeycomb/Subtitle";
import {
  CallTypesProps,
  PropsHoneycombSelector,
  PropsLibrarySelector,
  PropsUserSelector,
} from "@/types/*";
import { useSelector, useDispatch } from "react-redux";
import { setLibraryPath, setLibraryPathExists } from "@/store/actions/library";
import {
  findGroupFolderByPath,
  generateVirtualStudioRoomHash,
  generateVirtualStudioPublicLink,
} from "@/utils/utils";
import IconButtonV2 from "@/components/ui/IconButtonV2";
import { ChatMemoized } from "@/components/pages/honeycomb/Chat";
import Backdrop from "@/components/ui/Backdrop";
import HoneycombChatToolsMenu from "@/components/ui/ToolsMenu/Contexts/HoneycombChatToolsMenu";
import { RoomItemInterface } from "@/interfaces/talk";
import { getSingleConversationAxios } from "@/services/talk/room";
import { v4 as uuid } from "uuid";
import { ModalConferenceJitsiAPIMemoized } from "@/components/ui/ModalConference/ModalConferenceJitsiAPI";
import { handlePathLocalization } from "@/utils/directory";
import Script from "next/script";
import ChatTitle from "@/components/pages/honeycomb/Chat/ChatTitle";
import ChatHeaderOptions from "@/components/pages/honeycomb/Chat/ChatHeaderOptions";
import getConfig from "next/config";
import { LayoutApp } from "@/components/pages/honeycomb/HoneycombPage/styled";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

const { publicRuntimeConfig } = getConfig();

type Props = {
  token: string;
  displayName: string;
  canDeleteConversation: number;
};

function Honeycomb({ token, displayName, canDeleteConversation }: Props) {
  const dispatch = useDispatch();
  const { t: l } = useTranslation("library");
  const { t } = useTranslation("honeycomb");
  const { t: v } = useTranslation("virtualStudio");
  const connectionStatus = useConnectionStatus();
  const [showCall, setShowCall] = useState(false);
  const [loadingPathLocalization, setPathLoadingLocalization] = useState(true);
  const [typeCall, setTypeCall] = useState<CallTypesProps>("call");
  const [roomName, setRoomName] = useState("");
  const [publicVirtualStudioLink, setPublicVirtualStudioLink] = useState("");
  const [loading, setLoading] = useState(false);
  const [room, setRoom] = useState<RoomItemInterface>();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;
  const library = useSelector((state: { library: PropsLibrarySelector }) => state.library);
  const honeycombRdx = useSelector(
    (state: { honeycomb: PropsHoneycombSelector }) => state.honeycomb,
  );
  const pathExists = library.currentPathExists;
  const router = useRouter();
  const path = library.currentPath;
  const urlPath = router.asPath;

  const initTalkFolderName = async (room: RoomItemInterface) => {
    let path = displayName;
    if (room.type === RoomTypeEnum.CONVERSATION) {
      path = `${l("talkFolderName")}`;
    } else if (!canDeleteConversation) {
      const isGroupFolder = await findGroupFolderByPath(displayName);

      if (!isGroupFolder) {
        path = `${l("talkFolderName")}/${displayName}`;
      }
    }

    dispatch(setLibraryPathExists(true));
    dispatch(setLibraryPath(path));
    setPathLoadingLocalization(false);
  };

  const getRoom = useCallback(async () => {
    if (connectionStatus) {
      try {
        const room = await getSingleConversationAxios(token);
        return room.data.ocs.data;
      } catch (e) {
        console.log(e);
        return null;
      }
    }

    return honeycombRdx.honeycombs.find((room) => room.token === token);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connectionStatus]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const room = await getRoom();
      if (!room) {
        await router.push("/");
        return;
      }

      setRoom(room);
      if (connectionStatus) {
        await initTalkFolderName(room);
      } else {
        setPathLoadingLocalization(false);
      }

      setLoading(false);
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleJoinCall = useCallback((type: CallTypesProps) => {
    setTypeCall(type);
    handleSetRoomName(type);
    setShowCall(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleEndCall = useCallback(() => {
    setShowCall(false);
  }, []);

  const handleLeaveRoom = () => {
    document.dispatchEvent(
      new CustomEvent("reload-honeycombs", {
        detail: { uuid: uuid() },
      }),
    );
    router.push("/honeycomb");
  };

  const handleSetRoomName = (type: CallTypesProps) => {
    if (type === "call") {
      setPublicVirtualStudioLink("");
      setRoomName(`${displayName}-${token}`);
      return;
    }

    const pathRdr = handlePathLocalization(userId, path, pathExists, urlPath);
    const virtualStudioHash = generateVirtualStudioRoomHash(token, pathRdr);
    const link = generateVirtualStudioPublicLink(virtualStudioHash, displayName);
    const pvsl = v("inviteMessage", { link });
    setPublicVirtualStudioLink(pvsl);
    setRoomName(virtualStudioHash);
  };

  return (
    <LayoutApp
      back={!showCall}
      title={<ChatTitle room={room} />}
      showFooter
      fontSizeTitle={16}
      subtitle={<Subtitle token={token} />}
      fontSizeSubtitle={12}
      drawer={false}
      notifications={false}
      templateHeader="variation4"
      rightExtraElement={
        <ChatHeaderOptions
          key="options"
          displayName={displayName}
          token={token}
          room={room}
          canDeleteConversation={canDeleteConversation}
          handleJoinCall={handleJoinCall}
          handleLeaveRoom={handleLeaveRoom}
        />
      }
    >
      <Backdrop open={loading} />
      {room && (
        <>
          <ChatMemoized
            token={token}
            displayName={displayName}
            canDeleteConversation={canDeleteConversation}
            roomType={room.type}
          />
          <HoneycombChatToolsMenu
            handleJoinCall={() => handleJoinCall("virtual-studio")}
            loading={loadingPathLocalization}
          />
        </>
      )}
      <ModalConferenceJitsiAPIMemoized
        handleClose={handleEndCall}
        modalTitle={typeCall === "call" ? t("conference.title") : v("title")}
        modalSubtitle={displayName}
        open={showCall}
        type={typeCall}
        moderator={canDeleteConversation > 0}
        rightElement={
          <IconButtonV2 icon="close" color="#fff" handleClick={handleEndCall} fontSize={20} />
        }
        roomName={roomName}
        publicVirtualStudioLink={publicVirtualStudioLink}
      />
      <Script src={`${publicRuntimeConfig.meet.baseUrl}/external_api.js`}></Script>
    </LayoutApp>
  );
}

export default Honeycomb;
