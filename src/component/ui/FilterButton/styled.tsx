import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import Button from "@/components/ui/Button";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Root = styled(Box)({
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
});

export const Icon = styled(Box)(({ theme }: StyledProps) => ({
  marginRight: theme.spacing(1),
}));

export const MyButton = styled(Button)({
  borderRadius: "20px",
  padding: "0px 10px",
});

export const FilterTitle = styled(Text)({
  fontSize: "0.8em",
});
