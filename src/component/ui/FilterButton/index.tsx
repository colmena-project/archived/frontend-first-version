import React from "react";
import { ButtonVariantEnum } from "@/enums/*";
import SvgIconAux from "@/components/ui/SvgIcon";
import { AllIconProps } from "@/types/*";
import { Root, Icon, MyButton, FilterTitle } from "./styled";

type Props = {
  dataTut?: string;
  title: string;
  icon?: AllIconProps;
  handleClick?: () => void;
  color?: string;
};

export default function FilterButton({ dataTut, title, handleClick, icon, color }: Props) {
  const boxTitle = (
    <Root>
      {icon && (
        <Icon>
          <SvgIconAux icon={icon} fontSize={12} htmlColor={color} />
        </Icon>
      )}
      <FilterTitle>{title}</FilterTitle>
    </Root>
  );

  return (
    <MyButton
      data-tut={dataTut}
      title={boxTitle}
      variant={ButtonVariantEnum.TEXT}
      data-testid="toolbar-click"
      handleClick={() => (handleClick ? handleClick() : undefined)}
      style={{
        color,
        border: `1px solid ${color}`,
        textTransform: "inherit",
      }}
    />
  );
}

export const FilterButtonMemoized = React.memo(FilterButton);
