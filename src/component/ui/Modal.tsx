import React from "react";
import { withStyles, createStyles } from "@material-ui/core/styles";

import DialogContentText from "@material-ui/core/DialogContentText";
import Typography from "@material-ui/core/Typography";
import DialogActions from "@material-ui/core/DialogActions";
import { currentDirection } from "@/utils/i18n";
import Box from "@material-ui/core/Box";
import theme from "@/styles/theme";

import { ButtonCloseModal, DialogContentStyle, Subtitle, Title, WrapperDialog } from "./styled";

const styles = () => createStyles({});

const DialogTitle = withStyles(styles)((props: any) => {
  const { children } = props;
  return (
    <Title disableTypography>
      <Typography variant="h5">{children}</Typography>
    </Title>
  );
});

const DialogSubTitle = withStyles(styles)((props: any) => {
  const { children, ...other } = props;
  return (
    <Subtitle disableTypography {...other}>
      <Typography variant="h6">{children}</Typography>
    </Subtitle>
  );
});

const DialogContent = withStyles(styles)((props: any) => {
  const { children, ...other } = props;
  return <DialogContentStyle {...other}>{children}</DialogContentStyle>;
});

type Props = {
  withButtonToClose?: boolean;
  title?: string;
  subtitle?: string;
  description?: string | null;
  open: boolean;
  handleClose?: (event: any, reason: string) => void | undefined;
  children: React.ReactNode;
  actions?: React.ReactNode;
  disableBackdropClick?: boolean;
  fullScreen?: boolean;
  // eslint-disable-next-line @typescript-eslint/ban-types
  dialogTitleStyle?: any | undefined;
  rightElement?: React.ReactNode | undefined;
  [x: string]: any;
};

function Modal({
  withButtonToClose = false,
  title,
  subtitle,
  description,
  open,
  handleClose,
  children,
  actions,
  fullScreen = false,
  dialogTitleStyle = undefined,
  rightElement = undefined,
  ...props
}: Props) {
  return (
    <WrapperDialog
      dir={currentDirection()}
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={open}
      fullWidth
      fullScreen={fullScreen}
      maxWidth="xs"
      {...props}
    >
      {withButtonToClose && (
        <ButtonCloseModal
          onClick={handleClose}
          icon="close"
          htmlColor={theme.palette.primary.contrastText}
          fontSize={16}
        />
      )}

      <Box display="flex" flex={1} flexDirection="row">
        <Box display="flex" flex={1} flexDirection="column">
          {title && (
            <DialogTitle
              className={dialogTitleStyle}
              id="customized-dialog-title"
              onClose={handleClose}
            >
              {title}
            </DialogTitle>
          )}
          {subtitle && <DialogSubTitle>{subtitle}</DialogSubTitle>}
        </Box>
        {rightElement && (
          <Box
            className={dialogTitleStyle}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            {rightElement}
          </Box>
        )}
      </Box>
      <DialogContent>
        {description && <DialogContentText>{description}</DialogContentText>}
        {children}
      </DialogContent>
      {actions && <DialogActions>{actions}</DialogActions>}
    </WrapperDialog>
  );
}

export default Modal;
