import React from "react";
import FlexBox from "@/components/ui/FlexBox";
import { AlignItemsEnum, JustifyContentEnum } from "@/enums/index";
import CircularProgress from "@material-ui/core/CircularProgress";

type Props = {
  backgroundColor?: string;
  color?: "primary" | "secondary";
};

export default function CenterProgress({ backgroundColor = "#fff", color = "primary" }: Props) {
  return (
    <FlexBox
      extraStyle={{ height: "100vh", backgroundColor, margin: 0, padding: 0 }}
      justifyContent={JustifyContentEnum.CENTER}
      alignItems={AlignItemsEnum.CENTER}
    >
      <CircularProgress color={color} />
    </FlexBox>
  );
}
