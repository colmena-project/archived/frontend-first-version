import React from "react";
import Modal from "@/components/ui/Modal";
import { useTranslation } from "next-i18next";
import { UserAvatarMemoized } from "../../pages/honeycomb/RoomAvatar/User";
import Text from "../Text";
import { toast } from "@/utils/notifications";
import { createConversation, getUsersConversationsAxios } from "@/services/talk/room";
import { useDispatch } from "react-redux";
import { addHoneycomb } from "@/store/actions/honeycomb";
import { useRouter } from "next/router";
import useCollaboratorAvatar from "@/hooks/cms/useCollaboratorAvatar";
import { Avatar, Root, Name, TalkButtonBox } from "./styled";
import OutlinedButton from "../Button/OutlinedButton";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getHoneycombLink } from "@/utils/offlineNavigation";

type Props = {
  open: boolean;
  userName: string;
  userId: string;
  conversationName?: string;
  roomRole?: string;
  showTalkOption?: boolean;
  closeProfileModal: () => void;
};

export default function ProfileModal({
  open,
  userName,
  userId,
  showTalkOption = true,
  conversationName,
  roomRole,
  closeProfileModal,
}: Props) {
  const { t } = useTranslation("common");
  const dispatch = useDispatch();
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const { thumbnail: avatar } = useCollaboratorAvatar(userId);

  const talkToUser = async () => {
    try {
      const conversation = await createConversation(userId);
      const { token, canDeleteConversation } = conversation.data.ocs.data;

      const conversations = await getUsersConversationsAxios();
      const tokens = conversations.data.ocs.data.map((conversation) => conversation.token);
      if (!tokens || !tokens.find((conversationToken) => conversationToken === token)) {
        dispatch(addHoneycomb(conversation.data.ocs.data));
        toast(t("honeycombModal.chatRoomSuccess"), "success");
      }

      closeProfileModal();

      router.push(
        getHoneycombLink(token, userId, !canDeleteConversation ? 0 : 1, connectionStatus),
      );
    } catch (e) {
      const msg = e.message ? e.message : t("honeycombModal.chatRoomFailed");
      toast(msg, "error");
    }
  };

  return (
    <Modal data-testid="modal-profile" open={open} handleClose={closeProfileModal}>
      <Root>
        <Avatar>
          <UserAvatarMemoized
            width={120}
            height={120}
            name={userId}
            displayName={userName}
            url={avatar ?? undefined}
          />
        </Avatar>
        <Name>{userName}</Name>
        {conversationName && <Text>{conversationName}</Text>}
        {roomRole && <Text>{roomRole}</Text>}
        {showTalkOption && (
          <TalkButtonBox>
            <OutlinedButton
              handleClick={talkToUser}
              title={`${t("talkTo")} ${userName}`}
              color="variation4"
              startIcon="chat"
            />
          </TalkButtonBox>
        )}
      </Root>
    </Modal>
  );
}

export const ProfileModalMemoized = React.memo(ProfileModal);
