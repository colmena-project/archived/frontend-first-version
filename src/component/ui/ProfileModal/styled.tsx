import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Root = styled(Box)({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
});

export const Avatar = styled(Box)(({ theme }: StyledProps) => ({
  marginBottom: theme.spacing(1),
  "& .MuiAvatar-root": {
    border: "3px solid",
    borderColor: theme.palette.variation4.main,
  },
}));

export const Name = styled(Text)({
  fontSize: "1.1rem",
  fontWeight: "bold",
});

export const TalkButtonBox = styled(Box)(({ theme }: StyledProps) => ({
  marginTop: theme.spacing(1),
}));
