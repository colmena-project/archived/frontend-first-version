import React, { useRef, useEffect } from "react";
import { BreadcrumbItemInterface } from "@/interfaces/index";
import { moveScrollToRight } from "@/utils/utils";
import SvgIconAux from "../SvgIcon";
import theme from "@/styles/theme";
import {
  BreadcrumbItemButton,
  BreadcrumbWrapper,
  FirstBreadcrumbItemButton,
} from "@/components/ui/Breadcrumb/styled";

type Props = {
  breadcrumbs: Array<BreadcrumbItemInterface>;
  handleNavigate: (dir: BreadcrumbItemInterface) => void;
  isDisabled?: boolean;
  showRootPath?: boolean;
};

function Breadcrumb({
  breadcrumbs,
  handleNavigate,
  isDisabled = false,
  showRootPath = true,
}: Props) {
  const breadcrumbRef = useRef<HTMLDivElement>(null);

  useEffect(() => moveScrollToRight(breadcrumbRef), [breadcrumbRef.current?.scrollWidth]);

  return (
    <BreadcrumbWrapper
      aria-label="breadcrumb"
      separator={<SvgIconAux icon="chevron_right" fontSize={9} htmlColor="#999" />}
      ref={breadcrumbRef}
    >
      {breadcrumbs.map((dir: BreadcrumbItemInterface, index: number) => {
        if (index === 0 && !showRootPath) {
          return null;
        }

        if (dir.icon !== undefined) {
          return (
            <FirstBreadcrumbItemButton
              key={dir.path}
              color="primary"
              disabled={dir.isCurrent || isDisabled || dir.path === "#"}
              onClick={() => handleNavigate(dir)}
            >
              <SvgIconAux icon={dir.icon} htmlColor={theme.palette.primary.main} fontSize="small" />
            </FirstBreadcrumbItemButton>
          );
        }

        return (
          <BreadcrumbItemButton
            key={dir.path}
            disabled={dir.isCurrent || isDisabled || dir.path === "#"}
            onClick={() => handleNavigate(dir)}
            isActive={dir.isCurrent}
          >
            {dir.description}
          </BreadcrumbItemButton>
        );
      })}
    </BreadcrumbWrapper>
  );
}

export default React.memo(Breadcrumb);
