import { styled, withStyles } from "@material-ui/core/styles";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Button from "@material-ui/core/Button";

export const BreadcrumbWrapper = styled(Breadcrumbs)({
  overflowY: "hidden",
  "& .MuiBreadcrumbs-ol": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flexWrap: "nowrap",
    alignContent: "center",
    padding: 0,
    margin: 0,
    alignSelf: "stretch",
    flexGrow: 1,
    "& .MuiBreadcrumbs-li": {
      whiteSpace: "nowrap",
    },
    "& .MuiBreadcrumbs-separator": {
      marginLeft: "2px",
      marginRight: "2px",
    },
    "& .MuiButton-root": {
      textTransform: "inherit",
    },
  },
});

const ButtonBase = ({ isActive, ...props }: any) => (
  <Button {...props} component="a" styles={{ color: isActive ? "#999" : undefined }} />
);

export const FirstBreadcrumbItemButton = withStyles(() => ({
  root: {
    padding: "8px 12px",
    minWidth: "auto",
  },
}))(ButtonBase);

export const BreadcrumbItemButton = withStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
    fontSize: 16,
    fontWeight: 700,
  },
}))(ButtonBase);
