import { AllIconProps, FontSizeIconProps } from "@/types/*";
import SvgIconAux from "../SvgIcon";
import { MyIconButton } from "./styled";

type Props = {
  color: string;
  icon: AllIconProps;
  fontSize?: FontSizeIconProps | number;
  handleClick: ((event?: any) => void) | (() => void) | undefined;
  [x: string]: any;
};

export default function IconButtonV2({ color, icon, fontSize = 20, handleClick, ...props }: Props) {
  return (
    <MyIconButton onClick={handleClick} {...props}>
      <SvgIconAux icon={icon} fontSize={fontSize} htmlColor={color} />
    </MyIconButton>
  );
}
