import { styled } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";

export const MyIconButton = styled(IconButton)({
  padding: 2,
});
