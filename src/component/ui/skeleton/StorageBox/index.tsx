import {
  ContainerStorageBox,
  ContantStorageBox,
  TitleContantStorageBox,
  ProgressContantStorageBox,
} from "@/components/pages/profile/styled";

import Skeleton from "@material-ui/lab/Skeleton";

export default function StorageBoxSkeleton() {
  return (
    <ContainerStorageBox data-testid="storage-box-skeleton">
      <Skeleton variant="text" width={100} />
      <ContantStorageBox>
        <TitleContantStorageBox>
          <ProgressContantStorageBox variant="determinate" value={10} />
        </TitleContantStorageBox>
      </ContantStorageBox>
    </ContainerStorageBox>
  );
}
