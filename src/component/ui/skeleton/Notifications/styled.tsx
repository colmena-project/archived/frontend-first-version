import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Skeleton from "@material-ui/lab/Skeleton";

export const Wrapper = styled(Box)({
  display: "flex",
  width: "100%",
  gap: "20px",
  borderTop: "1px solid #EEEEEE",
  borderBottom: "1px solid #EEEEEE",
  padding: "10px 15px",
});

export const WrapperIcon = styled(Box)({
  maxWidth: "40px",
  display: "flex",
  flexGrow: 1,
  justifyContent: "center",
});

export const WrapperContent = styled(Box)({
  display: "flex",
  flexGrow: 1,
  flexDirection: "column",
});

export const MySkeleton = styled(Skeleton)({
  display: "flex",
  width: "100%",
});
