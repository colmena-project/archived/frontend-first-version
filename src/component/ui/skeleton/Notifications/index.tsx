import React from "react";
import Box from "@material-ui/core/Box";
import Skeleton from "@material-ui/lab/Skeleton";
import { Wrapper, WrapperIcon, WrapperContent, MySkeleton } from "./styled";

export default function NotificationsLoading({ amount = 1 }: { amount?: number }) {
  const array = new Array(amount).fill(null);

  return (
    <Box>
      {array.map((row) => (
        <Item key={row} />
      ))}
    </Box>
  );
}

const Item = () => (
  <Wrapper>
    <WrapperIcon>
      <Skeleton animation="wave" />
    </WrapperIcon>
    <WrapperContent>
      <MySkeleton animation="wave" />
      <MySkeleton animation="wave" />
      <MySkeleton animation="wave" />
    </WrapperContent>
  </Wrapper>
);
