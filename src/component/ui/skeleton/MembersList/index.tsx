/* eslint-disable react/no-array-index-key */
import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";
import { Typography } from "@material-ui/core";
import { Wrapper, Card } from "./styled";

type Props = {
  quantity?: number;
  width?: number;
  height?: number;
};

export default function MembersList({ quantity = 3, width = 65, height = 65 }: Props) {
  return (
    <Wrapper>
      {Array.from({ length: quantity }).map((_, key) => (
        <Card key={`member-list-${key}`} margin={1}>
          <Skeleton variant="circle" width={width} height={height} />
          <Typography variant="body1">
            <Skeleton width={50} />
          </Typography>
        </Card>
      ))}
    </Wrapper>
  );
}

export const MembersListMemoized = React.memo(MembersList);
