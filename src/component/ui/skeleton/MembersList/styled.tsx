import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";

export const Wrapper = styled(Box)({
  display: "grid",
  gridTemplateColumns: "repeat(3, minmax(0, 1fr))",
  border: "1px solid #E9E9E9",
  borderRadius: "7px",
  padding: "20px",
  gap: "20px",

  "@media (min-width:1024px)": {
    gridTemplateColumns: "repeat(5, minmax(0, 1fr))",
  },
});

export const Card = styled(Box)({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  flexWrap: "nowrap",
  width: "100%",
});
