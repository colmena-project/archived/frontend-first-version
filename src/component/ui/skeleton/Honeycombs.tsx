import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    width: "110px",
    padding: "0 10px",
    animation: "flash 1500ms linear 0s infinite normal forwards",
  },

  label: {
    backgroundColor: theme.palette.gray.light,
    width: "100%",
    height: "20px",
    marginTop: "10px",
  },

  honeycomb: {
    display: "flex",
  },

  left: {
    float: "left",
    borderRight: `20px solid ${theme.palette.gray.light}`,
    borderTop: "40px solid transparent",
    borderBottom: "40px solid transparent",
  },
  center: {
    float: "left",
    width: "50px",
    height: "80px",
    backgroundColor: theme.palette.gray.light,
  },
  right: {
    float: "left",
    borderLeft: `20px solid ${theme.palette.gray.light}`,
    borderTop: "40px solid transparent",
    borderBottom: "40px solid transparent",
  },
}));

export default function Honeycombs({ amount = 1 }: { amount?: number }) {
  const array = new Array(amount).fill(null);

  return (
    <Box display="flex" justifyContent="center">
      {array.map((row, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Item key={`item-${index}`} />
      ))}
    </Box>
  );
}

const Item = () => {
  const classes = useStyles();
  return (
    <Box className={classes.wrapper}>
      <Box className={classes.honeycomb}>
        <Box className={classes.left}></Box>
        <Box className={classes.center}></Box>
        <Box className={classes.right}></Box>
      </Box>
      <Box className={classes.label}></Box>
    </Box>
  );
};
