import React from "react";
import Box from "@material-ui/core/Box";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles } from "@material-ui/core";
import { v4 as uuid } from "uuid";
import { getRandomInt } from "@/utils/utils";

const useStyles = makeStyles(() => ({
  base: {
    display: "flex",
    flexWrap: "nowrap",
  },
  tag: {
    marginRight: 5,
  },
}));

export default function TagList({ quantity = 5 }: { quantity?: number }) {
  const classes = useStyles();
  return (
    <Box className={classes.base}>
      {Array.from({ length: quantity }).map(() => (
        <Skeleton
          width={100 - getRandomInt(0, 50)}
          height={50}
          className={classes.tag}
          key={uuid()}
        />
      ))}
    </Box>
  );
}
