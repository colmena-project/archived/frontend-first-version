import Box from "@material-ui/core/Box";
import ListItemText from "@material-ui/core/ListItemText";
import { styled } from "@material-ui/core/styles";

export const Description = styled(ListItemText)({
  flexDirection: "column",
  flexGrow: 1,
  alignSelf: "stretch",
  overflow: "hidden",
  marginLeft: 5,
});

export const Card = styled(Box)({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  flexWrap: "nowrap",
  width: "100%",
  background: "#fff",
  padding: 3,
});
