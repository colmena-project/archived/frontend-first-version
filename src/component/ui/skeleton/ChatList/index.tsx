/* eslint-disable react/no-array-index-key */
import React from "react";
import Box from "@material-ui/core/Box";
import Skeleton from "@material-ui/lab/Skeleton";
import { Typography } from "@material-ui/core";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import { Card, Description } from "./styled";

export default function ChatList() {
  return (
    <Box marginTop={2}>
      {[1, 2, 3].map((_, key) => (
        <Card key={`chat-list-item-${key}`}>
          <Description
            data-testid="title"
            primary={
              <Typography variant="body1">
                <Skeleton width="50%" />
              </Typography>
            }
            secondary={
              <Typography variant="caption">
                <Skeleton width="80%" />
              </Typography>
            }
          />
        </Card>
      ))}
      {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((_, key) => (
        <Card key={`messages-${key}`}>
          <ListItemAvatar style={{ paddingLeft: 10 }}>
            <Skeleton variant="circle" width={40} height={40} />
          </ListItemAvatar>
          <Description
            data-testid="title"
            primary={
              <Typography variant="body1">
                <Skeleton width="50%" />
              </Typography>
            }
            secondary={
              <Typography variant="caption">
                <Skeleton width="80%" />
              </Typography>
            }
          />
        </Card>
      ))}
    </Box>
  );
}

export const ChatListMemoized = React.memo(ChatList);
