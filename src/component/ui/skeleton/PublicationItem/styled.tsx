import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

export const Wrapper = styled(Box)(({ theme }) => ({
  display: "flex",
  width: "100% !important",
  minWidth: "100%",
  flexWrap: "wrap",
  background: "#fff",
  padding: 8,
  borderRadius: 5,
  border: "1px solid",
  borderColor: theme.palette.grey[200],
  boxShadow: "none",

  "& .MuiCardContent-root:last-child": {
    padding: 0,
  },

  animation: "flash 1500ms linear 0s infinite normal forwards",
  marginBottom: "8px",
  gap: 10,
}));

export const Image = styled(Box)(({ theme }) => ({
  display: "flex",
  width: "50px",
  height: "50px",
  background: theme.palette.grey[100],
  borderRadius: 5,
}));

export const Content = styled(Box)({
  display: "flex",
  flexDirection: "column",
  gap: 10,
});

export const Text = styled(Box)(({ theme }) => ({
  display: "flex",
  width: "100px",
  height: "20px",
  background: theme.palette.grey[200],
}));

export const Action = styled(Box)(({ theme }) => ({
  display: "flex",
  width: "200px",
  height: "20px",
  background: theme.palette.grey[200],
}));
