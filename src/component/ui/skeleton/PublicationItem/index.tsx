import Box from "@material-ui/core/Box";
import { Wrapper, Image, Content, Text, Action } from "./styled";

export default function PublicationItemSkeleton({ amount = 1 }: { amount?: number }) {
  const array = new Array(amount).fill(null);

  return (
    <Box>
      {array.map((row, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Wrapper key={`item-${index}`}>
          <Image />
          <Content>
            <Text />
            <Action />
          </Content>
        </Wrapper>
      ))}
    </Box>
  );
}
