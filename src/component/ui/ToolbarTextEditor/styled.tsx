import theme from "@/styles/theme";
import { styled } from "@material-ui/core/styles";
import Button from "@/components/ui/Button";
import IconButton from "@material-ui/core/IconButton";
import Box from "@material-ui/core/Box";

export const WrapperEditorContent = styled(Box)({
  "& .my-custom-class-image": {
    width: "100%",
    borderRadius: "4px",
  },

  "& > ol, ul": {
    listStyle: "inherit",
    marginLeft: "20px",
    padding: "none",
  },

  "& .ProseMirror": {
    "& pre": {
      background: "#0d0d0d",
      borderRadius: "0.5rem",
      color: "#fff",
      padding: "0.75rem 1rem",
    },

    "& code": {
      background: "none",
      color: "inherit",
      fontSize: "0.8rem",
      padding: "0",

      "& .hljs-comment, .hljs-quote": {
        color: "#616161",
      },

      "& .hljs-variable, .hljs-template-variable, .hljs-attribute, .hljs-tag, .hljs-name, .hljs-regexp, .hljs-link, .hljs-name, .hljs-selector-id, .hljs-selector-class ":
        {
          color: "#f98181",
        },

      "& .hljs-number, .hljs-meta, .hljs-built_in, .hljs-builtin-name, .hljs-literal, .hljs-type, .hljs-params":
        {
          color: "#fbbc88",
        },

      "& .hljs-string, .hljs-symbol, .hljs-bullet": {
        color: "#b9f18d",
      },

      "& .hljs-title, .hljs-section": {
        color: "#faf594",
      },

      "& .hljs-keyword, .hljs-selector-tag": {
        color: "#70cff8",
      },

      "& .hljs-emphasis": {
        fontStyle: "italic",
      },

      "& .hljs-strong": {
        fontWeight: "700",
      },
    },

    "& ol": {
      marginLeft: "20px",
      listStyle: "auto",
    },

    "& hr": {
      margin: "10px 0",
    },

    "& a": {
      color: theme.palette.primary.dark,
      textDecoration: "underline",

      "&:hover": {
        color: theme.palette.primary.light,
        cursor: "pointer",
      },
    },

    "& .custom-blockquote": {
      borderLeft: `2px solid ${theme.palette.primary.light}`,
      paddingLeft: "1rem",
    },

    "& h1": {
      fontSize: "2em",
    },
    "& h2": {
      fontSize: "1.5em",
    },
    "& h3": {
      fontSize: "1.17em",
    },
    "& h4": {
      fontSize: "1em",
    },
    "& h5": {
      fontSize: ".83em",
    },
    "& h6": {
      fontSize: ".67em",
    },
  },
});

export const ImportButton = styled(Button)({
  marginLeft: 10,
});

export const ItemMenuOptionsToImportImage = styled(IconButton)({
  display: "flex",
  fontSize: "18px",
  color: theme.palette.primary.main,
  padding: 0,

  "&:first-child": {
    marginBottom: "10px",
  },

  "&:hover": {
    background: "none",
  },

  "& svg": {
    width: 25,
    padding: 0,
    marginRight: "10px",
    color: theme.palette.primary.main,
  },
});

export const ContainerToolbarTextEditor = styled(Box)({
  display: "flex",
  flexWrap: "nowrap",
  position: "fixed",
  overflowX: "scroll",
  bottom: "0",
  zIndex: 1,
  background: "#FFF",
  left: 0,
  width: "100%",

  borderTop: `2px solid #E5E5E5`,

  "&::-webkit-scrollbar": {
    width: "0px",
    height: "0px",
  },

  "& button": {
    width: "30px",
    height: "35px",
    padding: "5px",
    margin: "1px",
    background: "#f7f7f7",
  },

  "& .MuiButton-root": {
    minWidth: "10px !important",
  },
});

export const WrapperToolbarTextEditor = styled(Box)({
  display: "flex",
  width: "100%",
  minWidth: "500px",
  justifyContent: "center",
  background: "#FFF",
});
