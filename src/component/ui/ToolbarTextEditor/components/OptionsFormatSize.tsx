import * as React from "react";
import { Button, Menu, MenuItem, Box } from "@material-ui/core";

import FormatSize from "@material-ui/icons/FormatSize";
import { Editor } from "@tiptap/react";

type Level = 1 | 2 | 3 | 4 | 5 | 6;
interface OptionsFormatSizeProps {
  editor: Editor;
}

export default function OptionsFormatSize({ editor }: OptionsFormatSizeProps) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onChange = (level: Level) => {
    editor.chain().focus().toggleHeading({ level }).run();
    setAnchorEl(null);
  };

  return (
    <Box>
      <Button
        id="basic-button"
        aria-controls={open ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        <FormatSize />
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={() => setAnchorEl(null)}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <MenuItem onClick={() => onChange(1)}>H1</MenuItem>
        <MenuItem onClick={() => onChange(2)}>H2</MenuItem>
        <MenuItem onClick={() => onChange(3)}>H3</MenuItem>
        <MenuItem onClick={() => onChange(4)}>H4</MenuItem>
        <MenuItem onClick={() => onChange(5)}>H5</MenuItem>
        <MenuItem onClick={() => onChange(6)}>H6</MenuItem>
      </Menu>
    </Box>
  );
}
