import * as React from "react";
import { Editor } from "@tiptap/react";
import { Formik, Form, Field, FieldProps } from "formik";
import { Box, makeStyles, Button as ButtonMaterial, TextField } from "@material-ui/core";
import Button from "@/components/ui/Button";
import Modal from "@/components/ui/Modal";
import { useTranslation } from "next-i18next";
import InsertLink from "@material-ui/icons/InsertLink";
import Divider from "@/components/ui/Divider";

import * as Yup from "yup";

interface OptionToogleLinkProps {
  editor: Editor;
  open: boolean;
  onClose: () => void;
  onOpen: () => void;
}

const useStyles = makeStyles((theme) => ({
  title: {
    marginBottom: theme.spacing(2),
    color: theme.palette.icon.main,
  },
  form: {
    "& .MuiTextField-root": {
      width: "100%",
    },
  },
  submit: {
    marginLeft: 10,
  },
}));

export default function OptionToogleLink({ editor, open, onClose, onOpen }: OptionToogleLinkProps) {
  const { t } = useTranslation("file");
  const classes = useStyles();

  const onSubmit = (newLink: string) => {
    if (newLink) {
      editor
        .chain()
        .focus()
        .extendMarkRange("link")
        .setLink({ href: newLink, target: "_blank" })
        .run();

      onClose();
    } else {
      removeLink();
    }
  };

  const removeLink = () => {
    /* eslint-disable */
    editor.chain().focus().extendMarkRange("link").unsetLink().run();
    onClose();
  };

  const linkSchema = Yup.object().shape({
    href: Yup.string().required(),
  });

  if (!editor) {
    return null;
  }

  return (
    <Box>
      <Modal open={open} handleClose={onClose} title={t("optionToogleLink.title")}>
        <Formik
          initialValues={{ href: editor.getAttributes("link").href }}
          validationSchema={linkSchema}
          onSubmit={(values) => onSubmit(values.href)}
        >
          {({ submitForm, errors, setFieldValue }) => (
            <Form
              className={classes.form}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  submitForm();
                }
              }}
            >
              <Field name="href" InputProps={{ notched: true }}>
                {({ field }: FieldProps) => (
                  <TextField
                    error={Boolean(errors.href)}
                    id="outlined-search"
                    inputProps={{
                      maxLength: 60,
                      autoComplete: "off",
                      form: {
                        autoComplete: "off",
                      },
                    }}
                    label={t("optionToogleLink.form.input")}
                    variant="outlined"
                    {...field}
                    onChange={(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>
                      setFieldValue("href", event.target.value)
                    }
                    onKeyUp={(event: any) => event.target.value}
                  />
                )}
              </Field>
              <Divider marginTop={20} />

              <Box display="flex" justifyContent="flex-end" width="100%">
                <ButtonMaterial
                  id="basic-button-option-toogle-link"
                  aria-haspopup="true"
                  onClick={removeLink}
                >
                  {t("optionToogleLink.form.remove")}
                </ButtonMaterial>
                <Button
                  data-testid="submit"
                  title={t("optionToogleLink.form.save")}
                  type="submit"
                  className={classes.submit}
                />
              </Box>
            </Form>
          )}
        </Formik>
      </Modal>

      <InsertLink onClick={onOpen} />
    </Box>
  );
}
