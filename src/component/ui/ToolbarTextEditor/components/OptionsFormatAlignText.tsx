import * as React from "react";
import { Button, Menu, MenuItem, Box } from "@material-ui/core";
import { useTranslation } from "next-i18next";
import FormatAlignCenter from "@material-ui/icons/FormatAlignCenter";
import { Editor } from "@tiptap/react";

type Direction = "left" | "center" | "right" | "justify";
interface OptionsFormatSizeProps {
  editor: Editor;
}

export default function OptionsFormatAlignText({ editor }: OptionsFormatSizeProps) {
  const { t } = useTranslation("file");
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onChange = (direction: Direction) => {
    editor.chain().focus().setTextAlign(direction).run();
    setAnchorEl(null);
  };

  return (
    <Box>
      <Button
        id="basic-button"
        aria-controls={open ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        <FormatAlignCenter />
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={() => setAnchorEl(null)}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <MenuItem onClick={() => onChange("left")}>
          {t("OptionsFormatAlignText.options.left")}
        </MenuItem>
        <MenuItem onClick={() => onChange("right")}>
          {t("OptionsFormatAlignText.options.right")}
        </MenuItem>
        <MenuItem onClick={() => onChange("center")}>
          {t("OptionsFormatAlignText.options.center")}
        </MenuItem>
        <MenuItem onClick={() => onChange("justify")}>
          {t("OptionsFormatAlignText.options.justify")}
        </MenuItem>
      </Menu>
    </Box>
  );
}
