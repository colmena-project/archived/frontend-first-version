import React, { useState } from "react";
import { Editor } from "@tiptap/react";
import ColorLens from "@material-ui/icons/ColorLens";
import { Button, Menu, Box, Divider, makeStyles } from "@material-ui/core";
import { useTranslation } from "next-i18next";
import Text from "../../Text";

import Close from "@material-ui/icons/Close";

interface OptionsHighlightProps {
  editor: Editor;
}

const useStyles = makeStyles(() => ({
  contentBox: {
    width: "100%",
    padding: "0px 10px",
  },
  listColors: {
    display: "flex",
    gap: "5px",
    margin: "10px 0 20px 0",
  },
  color: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "20px",
    height: "20px",
    cursor: "pointer",

    "&:hover": {
      border: "1px solid #000",
    },
  },
  footer: {
    display: "flex",
    marginTop: "10px",
    justifyContent: "center",
  },
}));

export default function OptionsHighlight({ editor }: OptionsHighlightProps) {
  const { t } = useTranslation("file");
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const classes = useStyles();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onChangeBgColor = (color: string) => {
    editor.commands.toggleHighlight({ color });
    setAnchorEl(null);
  };

  const onChangeColor = (color: string) => {
    editor.commands.setColor(color);
    setAnchorEl(null);
  };

  const arrayColors = [
    { value: "#FF0000" },
    { value: "#0000FF" },
    { value: "#00FF00" },
    { value: "#A020F0" },
    { value: "#FFFF00" },
    { value: "#000000" },
    { value: "#FFFFFF" },
  ];

  return (
    <Box>
      <Button
        id="basic-button-highlight"
        aria-controls={open ? "basic-menu-highlight" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        <ColorLens />
      </Button>
      <Menu
        id="basic-menu-highlight"
        anchorEl={anchorEl}
        open={open}
        onClose={() => setAnchorEl(null)}
        MenuListProps={{
          "aria-labelledby": "basic-button-highlight",
        }}
      >
        <Box className={classes.contentBox}>
          <Text>{t("optionsHighlight.titleChangeBgColor")}</Text>
          <Divider />
          <Box className={classes.listColors}>
            {arrayColors.map((color) => (
              <Box
                key={color.value}
                className={classes.color}
                onClick={() => onChangeBgColor(color.value)}
                style={{ backgroundColor: color.value }}
              />
            ))}
            <Box className={classes.color} onClick={() => editor.commands.unsetHighlight()}>
              <Close />
            </Box>
          </Box>

          <Text>{t("optionsHighlight.titleChangeTextColor")}</Text>
          <Divider />
          <Box className={classes.listColors}>
            {arrayColors.map((color) => (
              <Box
                key={color.value}
                className={classes.color}
                onClick={() => onChangeColor(color.value)}
                style={{ backgroundColor: color.value }}
              />
            ))}
            <Box className={classes.color} onClick={() => editor.commands.unsetColor()}>
              <Close />
            </Box>
          </Box>
        </Box>
      </Menu>
    </Box>
  );
}
