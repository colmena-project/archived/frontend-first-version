import React, { useState, useRef } from "react";
import { Box } from "@material-ui/core";
import Modal from "@/components/ui/Modal";
import { useTranslation } from "next-i18next";
import ImageIcon from "@material-ui/icons/Image";
import getConfig from "next/config";
import SvgIconAux from "@/components/ui/SvgIcon";
import LibraryModal from "@/components/ui/LibraryModal";
import { LibraryItemInterface } from "@/interfaces/index";
import { ButtonSizeEnum } from "@/enums/index";
import { isImageFile } from "@/utils/utils";
import { uploadFileToCMS } from "@/services/cms/file";
import Backdrop from "@/components/ui/Backdrop";
import { toast } from "@/utils/notifications";
import { listFile } from "@/services/webdav/files";
import { arrayBufferToBlob } from "blob-util";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { ImportButton, ItemMenuOptionsToImportImage } from "../styled";

interface ToolbarTextEditorItemWithImageProps {
  setImage: (image: string) => boolean;
}

export default function OptionsToImportImage({ setImage }: ToolbarTextEditorItemWithImageProps) {
  const { t: c } = useTranslation("common");
  const { t } = useTranslation("file");
  const [loading, setLoading] = useState(false);
  const [loadingLibrary, setLoadingLibrary] = useState<string | undefined>();
  const [libraryOpen, setLibraryOpen] = useState(false);
  const [open, setOpen] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;
  const inputFileRef = useRef(null);
  const onClose = () => setOpen(false);
  const onOpen = () => setOpen(true);

  const onBtnClick = () => {
    if (!inputFileRef || !inputFileRef.current) return;
    const clk: { click: () => void } = inputFileRef?.current;
    clk.click();
  };

  const openLibraryModal = () => setLibraryOpen(true);

  const closeLibraryModal = () => setLibraryOpen(false);

  const actions = (item: LibraryItemInterface) => {
    if (item.type === "file" && isImageFile(item.mime)) {
      return (
        <ImportButton
          handleClick={() => handleClick(item)}
          title={t("import")}
          size={ButtonSizeEnum.SMALL}
          isLoading={loadingLibrary === item.filename}
        />
      );
    }

    return null;
  };

  const filterItems = (items: LibraryItemInterface[]) =>
    items.filter((item) => item.type !== "file" || isImageFile(item.mime));

  const handleClick = async (item: LibraryItemInterface) => {
    try {
      let filename = "";
      let blob: any = "";
      filename = item.filename;
      setLoadingLibrary(filename);

      const result: any = await listFile(userId, filename);
      blob = arrayBufferToBlob(result, "image/*");
      const file = new File([blob], filename);
      const urlImage = await getUrlViaFile(file);
      setImage(urlImage);
      setLoading(false);
    } catch (e) {
      toast(c("genericErrorMessage"), "error");
    } finally {
      setLoading(false);
      closeLibraryModal();
      onClose();
    }
  };

  const onSelectFile = async (e: any) => {
    try {
      setLoading(true);
      onClose();
      if (e.target.files && e.target.files.length > 0) {
        const file = e.target.files[0];

        const urlImage = await getUrlViaFile(file);
        setImage(urlImage);
        setLoading(false);
      }
    } catch (e) {
      toast(c("genericErrorMessage"), "error");
    }
  };

  const getUrlViaFile = async (file: any) => {
    const formData = new FormData();
    formData.append("files", file);
    const [response] = await uploadFileToCMS(formData);

    const { publicRuntimeConfig } = getConfig();
    const urlImage = `${publicRuntimeConfig.publicationsGraphQL.baseUrl}${response.url}`;

    return urlImage;
  };

  return (
    <>
      <Backdrop open={loading} />
      <Box>
        <input
          type="file"
          ref={inputFileRef}
          onChange={onSelectFile}
          style={{ display: "none" }}
          accept="image/*"
        />
        <LibraryModal
          title={t("selectImageFile")}
          handleClose={() => setLibraryOpen(false)}
          open={libraryOpen}
          onlyDirectories={false}
          options={actions}
          filterItems={filterItems}
        />
        <Modal open={open} handleClose={onClose}>
          <Box>
            <ItemMenuOptionsToImportImage onClick={onBtnClick}>
              <SvgIconAux icon="upload" />
              {t("importImage.uploadFromDevice")}
            </ItemMenuOptionsToImportImage>
            <ItemMenuOptionsToImportImage onClick={openLibraryModal}>
              <SvgIconAux icon="search_in_library" />
              {t("importImage.searchInLibrary")}
            </ItemMenuOptionsToImportImage>
          </Box>
        </Modal>

        <ImageIcon onClick={onOpen} />
      </Box>
    </>
  );
}
