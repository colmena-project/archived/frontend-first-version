import { Button } from "@material-ui/core";
import { ReactNode } from "react";

interface ToggleBoldProps {
  onClick?: () => void;
  label: string | ReactNode;
}

export default function ToolbarTextEditorItem({ onClick, label }: ToggleBoldProps) {
  return (
    <Button type="button" onClick={onClick}>
      {label}
    </Button>
  );
}
