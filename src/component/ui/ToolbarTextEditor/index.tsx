import { Editor } from "@tiptap/react";
import ToolbarTextEditorItem from "./components/ToolbarTextEditorItem";
import FormatBold from "@material-ui/icons/FormatBold";
import RefreshIcon from "@material-ui/icons/Refresh";
import Replay from "@material-ui/icons/Replay";
import FormatItalic from "@material-ui/icons/FormatItalic";
import StrikethroughS from "@material-ui/icons/StrikethroughS";
import FormatListNumbered from "@material-ui/icons/FormatListNumbered";
import FormatListBulleted from "@material-ui/icons/FormatListBulleted";
import HorizontalRules from "@material-ui/icons/Remove";
import FormatQuote from "@material-ui/icons/FormatQuote";
import Code from "@material-ui/icons/Code";

import OptionsFormatSize from "./components/OptionsFormatSize";
import OptionsHighlight from "./components/OptionsHighlight";
import OptionsFormatAlignText from "./components/OptionsFormatAlignText";
import OptionToogleLink from "./components/OptionToogleLink";
import { useState } from "react";
import OptionsToImportImage from "./components/OptionsToImportImage";
import { ContainerToolbarTextEditor, WrapperToolbarTextEditor } from "./styled";

interface ToolbarProps {
  editor: Editor;
}

export default function ToolbarTextEditor({ editor }: ToolbarProps) {
  const [openOptionToogleLink, setOpenOptionToogleLink] = useState(false);

  return (
    <ContainerToolbarTextEditor>
      <WrapperToolbarTextEditor>
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().undo().run()}
          label={<Replay />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().redo().run()}
          label={<RefreshIcon />}
        />
        <ToolbarTextEditorItem label={<OptionsFormatSize editor={editor} />} />
        <ToolbarTextEditorItem label={<OptionsHighlight editor={editor} />} />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleBold().run()}
          label={<FormatBold />}
        />

        <ToolbarTextEditorItem
          label={
            <OptionsToImportImage setImage={(image) => editor.commands.setImage({ src: image })} />
          }
        />
        <ToolbarTextEditorItem
          label={
            <OptionToogleLink
              editor={editor}
              open={openOptionToogleLink}
              onOpen={() => setOpenOptionToogleLink(true)}
              onClose={() => setOpenOptionToogleLink(false)}
            />
          }
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleItalic().run()}
          label={<FormatItalic />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleBulletList().run()}
          label={<FormatListBulleted />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleOrderedList().run()}
          label={<FormatListNumbered />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().toggleStrike().run()}
          label={<StrikethroughS />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.commands.toggleBlockquote()}
          label={<FormatQuote />}
        />
        <ToolbarTextEditorItem
          onClick={() => editor.chain().focus().setHorizontalRule().run()}
          label={<HorizontalRules />}
        />
        <ToolbarTextEditorItem label={<OptionsFormatAlignText editor={editor} />} />
        <ToolbarTextEditorItem onClick={() => editor.commands.toggleCodeBlock()} label={<Code />} />
      </WrapperToolbarTextEditor>
    </ContainerToolbarTextEditor>
  );
}
