import React, { ReactChildren } from "react";
import SvgIconAux from "../../SvgIcon";
import { AllIconProps, FontSizeIconProps } from "@/types/*";
import { OptionButton, OptionName, OptionIcon, OptionItem } from "./styled";

type ToolsMenuOptionProps = {
  children: ReactChildren | string;
  onClick?: () => void;
  icon?: AllIconProps;
  iconSize?: number | FontSizeIconProps;
  iconColor?: string;
  textColor?: string;
  "data-testid"?: string;
  disabled?: boolean;
  loading?: boolean;
};

type ToolsMenuOptionChildren = Pick<ToolsMenuOptionProps, "children">;

function ToolsMenuOption({
  children,
  onClick,
  icon,
  iconSize = 18,
  textColor,
  iconColor,
  disabled = false,
  "data-testid": dataTestid,
}: ToolsMenuOptionProps) {
  const handleClick = typeof onClick === "function" ? onClick : undefined;
  const handleChildren = ({ children }: ToolsMenuOptionChildren) => {
    if (typeof children === "string") {
      return (
        <OptionItem
          onClick={handleClick}
          data-testid={dataTestid ?? "tools-menu-option"}
          disabled={disabled}
          style={{ opacity: disabled ? 0.5 : 1 }}
        >
          {icon && (
            <OptionIcon data-testid={`${icon}_icon`}>
              {icon && <SvgIconAux icon={icon} fontSize={iconSize} htmlColor={iconColor} />}
            </OptionIcon>
          )}
          <OptionName style={{ color: textColor }}>{children}</OptionName>
        </OptionItem>
      );
    }

    return children;
  };

  return <OptionButton>{handleChildren({ children })}</OptionButton>;
}

export default ToolsMenuOption;
