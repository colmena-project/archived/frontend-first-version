import { styled } from "@material-ui/core/styles";
import { Box, ButtonBase } from "@material-ui/core";
import Text from "@/components/ui/Text";

export const OptionButton = styled(Box)({
  display: "block",
  background: "#fff",
  padding: "5px 8px",
  marginBottom: 8,
  borderRadius: 10,
});

export const OptionName = styled(Text)({
  flexDirection: "column",
  flexGrow: 1,
  alignSelf: "stretch",
  overflow: "hidden",
  marginLeft: 5,
});

export const OptionItem = styled(ButtonBase)({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
});

export const OptionIcon = styled(Box)({
  padding: "0 8px 0 0",
});
