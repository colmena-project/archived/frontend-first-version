import React, { useState, useCallback, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@/components/ui/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@/components/ui/Button";
import Text from "@/components/ui/Text";
// import { PropsUserSelector } from "@/types/index";
// import { useSelector } from "react-redux";
import { Formik, Form, Field, FieldProps, ErrorMessage } from "formik";
import Divider from "@/components/ui/Divider";
import * as Yup from "yup";
import { toast } from "@/utils/notifications";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import { useTranslation } from "next-i18next";
import router from "next/router";
import { Box } from "@material-ui/core";
import {
  ButtonVariantEnum,
  ButtonColorEnum,
  TextVariantEnum,
  ButtonSizeEnum,
  FileExtensionEnum,
} from "@/enums/*";
import ActionConfirm from "@/components/ui/ActionConfirm";

import { existFile, putFile } from "@/services/webdav/files";
import { PropsLibrarySelector, PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import LibraryModal from "../../LibraryModal";
import { LibraryItemInterface } from "@/interfaces/index";
import {
  convertUsernameToPrivate,
  handleFileName,
  handlePathLocalization,
  inHoneycomb,
} from "@/utils/directory";
import DirectoryPathArea from "@/components/ui/DirectoryPathArea";

const useStyles = makeStyles(() => ({
  form: {
    "& .MuiTextField-root": {
      width: "100%",
    },
  },
  submit: {
    marginLeft: 10,
  },
}));

type Props = {
  open: boolean;
  handleClose: () => void;
};

interface IForm {
  filename: string;
}

export default function NewEditorModal({ open, handleClose }: Props) {
  const { t } = useTranslation("common");
  const { t: l } = useTranslation("library");
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirmCancel, setShowConfirmCancel] = useState(false);
  const [cancelIsLoading, setCancelIsLoading] = useState(false);
  const initialValues = { filename: "" };

  const library = useSelector((state: { library: PropsLibrarySelector }) => state.library);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;

  const [canChangeLocation, setCanChangeLocation] = useState(true);

  const pathExists = library.currentPathExists;
  const currentLibraryPath = library.currentPath;
  const [path, setPath] = useState(currentLibraryPath);
  const [openLibrary, setOpenLibrary] = useState(false);
  const [finalPath, setFinalPath] = useState(path);
  const [fileName, setFileName] = useState("");
  const [chooseLocationSettled, setChooseLocationSettled] = useState(false);

  const urlPath = router.asPath;

  const defineFinalPath = useCallback(() => {
    let finalPath = path;
    if (!chooseLocationSettled) {
      finalPath = handlePathLocalization(userId, path, pathExists, urlPath);
    }

    setFinalPath(fileName ? `${finalPath}/${fileName}.${FileExtensionEnum.MD}` : finalPath);
  }, [path, chooseLocationSettled, fileName, userId, pathExists, urlPath]);

  const createFile = async () => {
    try {
      setIsLoading(true);
      await putFile(userId, convertUsernameToPrivate(finalPath, userId), "");
      router.replace(`/text-editor/${btoa(convertUsernameToPrivate(finalPath, userId))}`);
    } catch (error) {
      toast(error.message, "error");
    } finally {
      setIsLoading(false);
    }
  };

  const handleCreateFileText = async (values: IForm, setFieldError: any) => {
    setIsLoading(true);
    const exists = await existFile(userId, convertUsernameToPrivate(finalPath, userId));
    if (!exists) {
      await createFile();
      return;
    }

    setFieldError("filename", l("messages.fileAlreadyExists"));
    setIsLoading(false);
  };

  // eslint-disable-next-line no-unused-vars
  const NewFolderSchema = Yup.object().shape({
    filename: Yup.string().required(t("form.requiredTitle")),
  });

  const confirmClose = useCallback(() => {
    if (isLoading) {
      setShowConfirmCancel(true);
    } else {
      handleClose();
    }
  }, [handleClose, isLoading]);

  const requestAbortUpload = () => {
    setCancelIsLoading(true);
    toast(t("messages.textFileCreationCanceledSuccessfully"), "success");
    handleClose();
  };

  const handleName = (name: string) => setFileName(treatName(name.trim()));

  const treatName = (name: string) => handleFileName(name);

  const handleChangeLocation = (path: string) => {
    setPath(path);
    setChooseLocationSettled(true);
    setOpenLibrary(false);
  };

  const footerActions = (item: LibraryItemInterface) => (
    <Button
      handleClick={() => handleChangeLocation(item.aliasFilename)}
      title={t("chooseLocationButton")}
      size={ButtonSizeEnum.SMALL}
    />
  );

  useEffect(() => {
    defineFinalPath();

    return () => {
      setFinalPath("");
      setIsLoading(false);
    };
  }, [path, defineFinalPath]);

  useEffect(() => {
    if (inHoneycomb(urlPath)) {
      setCanChangeLocation(false);
    } else {
      setCanChangeLocation(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Modal
        data-testid="modal-new-file"
        title={t("newFileTitle")}
        open={open}
        handleClose={isLoading ? undefined : handleClose}
      >
        <Formik
          initialValues={initialValues}
          validationSchema={NewFolderSchema}
          onSubmit={(values, { setFieldError }) => handleCreateFileText(values, setFieldError)}
        >
          {({ setFieldValue, submitForm, errors }: any) => (
            <Form
              className={classes.form}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  submitForm();
                }
              }}
            >
              <Field name="filename" InputProps={{ notched: true }}>
                {({ field }: FieldProps) => (
                  <TextField
                    error={Boolean(errors.filename)}
                    id="outlined-search"
                    inputProps={{
                      maxLength: 60,
                      autoComplete: "off",
                      form: {
                        autoComplete: "off",
                      },
                    }}
                    label={t("form.fields.name")}
                    variant="outlined"
                    {...field}
                    onChange={(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>
                      setFieldValue("filename", treatName(event.target.value))
                    }
                    onKeyUp={(event: any) => handleName(event.target.value)}
                  />
                )}
              </Field>
              <ErrorMessage name="filename">
                {(msg) => <ErrorMessageForm message={msg} />}
              </ErrorMessage>
              <Divider marginTop={20} />
              <>
                <Box
                  display="flex"
                  flexDirection="row"
                  justifyContent="space-between"
                  alignItems="center"
                  paddingLeft={1}
                  paddingTop={1}
                >
                  <Box display="flex" flexDirection="column" overflow="hidden">
                    <Text variant={TextVariantEnum.BODY1} style={{ fontWeight: "bold" }}>
                      {t("form.location")}
                    </Text>
                    <DirectoryPathArea path={`/${finalPath}`} />
                  </Box>
                  {canChangeLocation && (
                    <Button
                      handleClick={() => setOpenLibrary(true)}
                      style={{ margin: 8 }}
                      variant={ButtonVariantEnum.TEXT}
                      color={ButtonColorEnum.PRIMARY}
                      title={t("changeLocationButton")}
                      size={ButtonSizeEnum.SMALL}
                    />
                  )}
                </Box>
                <Divider marginTop={20} />
              </>
              <Box display="flex" justifyContent="flex-end" width="100%">
                {isLoading && (
                  <Button
                    handleClick={confirmClose}
                    title={t("form.cancelButton")}
                    data-testid="cancel"
                    color={ButtonColorEnum.DEFAULT}
                    variant={ButtonVariantEnum.OUTLINED}
                  />
                )}
                <Button
                  data-testid="submit"
                  title={t("form.create")}
                  type="submit"
                  className={classes.submit}
                  disabled={isLoading}
                  isLoading={isLoading}
                />
              </Box>
            </Form>
          )}
        </Formik>
      </Modal>

      {canChangeLocation && (
        <LibraryModal
          title={t("changeLocationModalTitle")}
          handleClose={() => setOpenLibrary(false)}
          open={openLibrary}
          footerActions={footerActions}
        />
      )}

      {showConfirmCancel && (
        <ActionConfirm
          title={t("confirmCancelFolderCreation")}
          onOk={requestAbortUpload}
          onClose={() => setShowConfirmCancel(false)}
          isLoading={cancelIsLoading}
        />
      )}
    </>
  );
}
