import React, { useCallback, useMemo, useState } from "react";
import Modal from "@/components/ui/Modal";
import { useTranslation } from "next-i18next";
import UsersList from "../NewHoneycombModal/UsersList";
import UserListSkeleton from "@/components/ui/skeleton/UsersList";
import { useDispatch, useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { createConversation, getUsersConversationsAxios } from "@/services/talk/room";
import { addHoneycomb } from "@/store/actions/honeycomb";
import { toast } from "@/utils/notifications";
import { useRouter } from "next/router";
import Loading from "@/components/ui/Loading";
import { MemberInterface } from "@/interfaces/index";
import { allUsers } from "@/services/internal/users";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getHoneycombLink } from "@/utils/offlineNavigation";

type Props = {
  open: boolean;
  handleClose: () => void;
};
export default function NewConversationModal({ open, handleClose }: Props) {
  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;
  const dispatch = useDispatch();
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const [isLoading, setIsLoading] = useState(false);
  const [participants, setParticipants] = useState<MemberInterface[]>([]);
  const [search, setSearch] = useState("");

  const { data } = allUsers({
    revalidateOnFocus: false,
  });

  const chooseParticipant = useCallback(
    async (participants: MemberInterface[]) => {
      const participant = participants[0];
      setIsLoading(true);
      setParticipants([participant]);
      try {
        const conversation = await createConversation(participant.username);
        const { token, canDeleteConversation } = conversation.data.ocs.data;

        const conversations = await getUsersConversationsAxios();
        const tokens = conversations.data.ocs.data.map((conversation) => conversation.token);
        if (!tokens || !tokens.find((conversationToken) => conversationToken === token)) {
          dispatch(addHoneycomb(conversation.data.ocs.data));
          toast(c("honeycombModal.chatRoomSuccess"), "success");
        }
        setIsLoading(false);
        handleClose();

        router.push(
          getHoneycombLink(
            token,
            participant.username,
            Number(canDeleteConversation),
            connectionStatus,
          ),
        );
      } catch (e) {
        const msg = e.message ? e.message : c("honeycombModal.chatRoomFailed");
        toast(msg, "error");
      }
    },
    [c, dispatch, handleClose, router, connectionStatus],
  );

  const usersList = useMemo(() => {
    if (!data) {
      return <UserListSkeleton />;
    }

    const users: MemberInterface[] = [];
    if (data) {
      data.forEach((member: MemberInterface) => {
        if (member.username !== userId) {
          users.push(member);
        }
      });
    }

    return (
      <UsersList
        participants={participants}
        updateParticipants={chooseParticipant}
        users={users}
        search={search}
        setSearch={setSearch}
        userId={userId}
      />
    );
  }, [chooseParticipant, data, participants, search, userId]);

  return (
    <>
      <Modal
        data-testid="modal-create-conversation"
        title={c("addHoneycombTitle")}
        subtitle={c("conversationOneToOne")}
        open={open}
        handleClose={isLoading ? undefined : handleClose}
      >
        {isLoading ? <Loading /> : usersList}
      </Modal>
    </>
  );
}
