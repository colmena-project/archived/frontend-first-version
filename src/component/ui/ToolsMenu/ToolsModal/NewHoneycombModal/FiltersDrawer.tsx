import React, { useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import { v4 as uuid } from "uuid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { ButtonVariantEnum } from "@/enums/*";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useTranslation } from "react-i18next";
import { Button, FormControl, InputLabel, MenuItem } from "@material-ui/core";
import { Formik, Form } from "formik";
import Select from "@material-ui/core/Select";
import { GroupInterface } from "@/interfaces/keycloak";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiAccordionSummary-root": {
        backgroundColor: theme.palette.gray.light,
      },
      "& .MuiAccordion-root": {
        boxShadow: "none",
      },
      "& .MuiListItem-root": {
        padding: 0,
      },
      "& .MuiFormControlLabel-root": {
        width: "100%",
      },
      "& .MuiIconButton-root.Mui-checked": {
        color: theme.palette.variation1.main,
      },
    },
    filterButton: {
      borderRadius: 0,
      backgroundColor: theme.palette.variation2.main,
      color: theme.palette.variation2.contrastText,
      width: "100%",
      boxShadow: "none",
      textTransform: "inherit",
      fontSize: "1rem",
    },
    select: {
      minWidth: 200,
      width: "100%",
    },
  }),
);

export interface FiltersProps {
  group?: string;
}

type Props = {
  open: boolean;
  handleClose: () => void;
  groups: GroupInterface[];
  filterUsers: (filters: FiltersProps) => void;
  initialFilters: FiltersProps;
};

export default function UserFiltersDrawer({
  open,
  groups,
  filterUsers,
  handleClose,
  initialFilters,
}: Props) {
  const [expanded, setExpanded] = useState<string | boolean>("filter");
  const classes = useStyles();
  const { t } = useTranslation("common");

  const handleChange = (panel: string) => (event: any, newExpanded: string | boolean) => {
    setExpanded(newExpanded ? panel : false);
  };

  const handleSubmit = (filters: FiltersProps) => {
    filterUsers(filters);
    handleClose();
  };

  return (
    <Drawer anchor="right" open={open} onClose={handleClose} className={classes.root}>
      <Formik initialValues={initialFilters ?? {}} onSubmit={handleSubmit}>
        {({ values, setFieldValue }: any) => (
          <Form>
            <Accordion expanded={expanded === "filter"} onChange={handleChange("filter")}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="filters-content"
                id="filters-header"
              >
                <Typography>{t("newHoneycombFilter.title")}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <List>
                  <ListItem key={uuid()}>
                    <FormControl variant="outlined">
                      <InputLabel>{t("newHoneycombFilter.mediaGroup")}</InputLabel>
                      <Select
                        value={values?.group}
                        onChange={(e) => setFieldValue("group", e.target.value)}
                        label={t("newHoneycombFilter.mediaGroup")}
                        className={classes.select}
                      >
                        <MenuItem value="all" key={uuid()}>
                          <em>{t("newHoneycombFilter.all")}</em>
                        </MenuItem>
                        {groups &&
                          groups.map((group) => (
                            <MenuItem
                              value={`{"id": "${group.id}", "name":"${group.name}"}`}
                              key={uuid()}
                            >
                              {group.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </ListItem>
                </List>
              </AccordionDetails>
            </Accordion>
            <Button
              variant={ButtonVariantEnum.CONTAINED}
              className={classes.filterButton}
              type="submit"
            >
              {t("newHoneycombFilter.filter")}
            </Button>
          </Form>
        )}
      </Formik>
    </Drawer>
  );
}
