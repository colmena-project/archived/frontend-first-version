import React, { useState } from "react";
import Button from "@/components/ui/Button";
import { ButtonVariantEnum } from "@/enums/*";
import { useTranslation } from "next-i18next";
import Box from "@material-ui/core/Box";
import { UsersMemoized } from "../Users";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { MemberInterface } from "@/interfaces/index";

type Props = {
  handleBackStep: () => void;
  updateParticipants: (part: MemberInterface[]) => void;
  participants: MemberInterface[];
  submitting: boolean;
  handleSubmit: () => void;
};

export default function SecondStep({
  handleBackStep,
  participants,
  updateParticipants,
  submitting,
  handleSubmit,
}: Props) {
  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [search, setSearch] = useState("");

  return (
    <>
      <UsersMemoized
        participants={participants}
        updateParticipants={updateParticipants}
        userId={userRdx.user.id}
        setSearch={setSearch}
        search={search}
        isSubmitting={submitting}
      />
      <Box display="flex" flex="1" flexDirection="row" justifyContent="space-between" marginTop={1}>
        <Button
          handleClick={handleBackStep}
          variant={ButtonVariantEnum.OUTLINED}
          title={c("form.backButton")}
        />
        <Button
          handleClick={handleSubmit}
          // disabled={isSubmitting}
          // title={
          //   isSubmitting ? (
          //     <>
          //       <CircularProgress
          //         color="secondary"
          //         size={16}
          //         style={{ marginRight: 8 }}
          //       />{" "}
          //       {c("form.loadingTitle")}
          //     </>
          //   ) : (
          //     c("honeycombModal.buttonStep2")
          //   )
          // }
          title={c("honeycombModal.buttonStep2")}
        />
      </Box>
    </>
  );
}
