import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@/components/ui/Button";
import { Formik, Form, Field, FieldProps } from "formik";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import Divider from "@/components/ui/Divider";
import * as Yup from "yup";
import { useTranslation } from "next-i18next";
import Box from "@material-ui/core/Box";

type MyFormValues = {
  room: string;
  description: string;
};

const useStyles = makeStyles(() => ({
  form: {
    "& .MuiTextField-root": {
      width: "100%",
    },
  },
}));

type Props = {
  room: string;
  description?: string;
  handleNextStep: (room: string, description: string) => void;
};

export default function First({ room, description = "", handleNextStep }: Props) {
  const { t: c } = useTranslation("common");
  const classes = useStyles();

  const initialValues = {
    room,
    description,
  };

  const schemaValidation = Yup.object().shape({
    room: Yup.string().required(c("form.requiredTitle")),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={schemaValidation}
      onSubmit={(values: MyFormValues) => {
        const { room, description } = values;
        handleNextStep(room, description);
      }}
    >
      {({ submitForm, errors, touched, values }: any) => (
        <Form className={classes.form} autoComplete="off">
          <Box>
            <Field name="room" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <TextField
                  id="room"
                  inputProps={{
                    maxLength: 60,
                    autoComplete: "off",
                    form: {
                      autoComplete: "off",
                    },
                  }}
                  label={c("form.fields.name")}
                  variant="outlined"
                  {...field}
                />
              )}
            </Field>
            {errors.room && touched.room ? <ErrorMessageForm message={errors.room} /> : null}
            <Divider marginTop={20} />
            <Field name="description">
              {({ field }: FieldProps) => (
                <TextField
                  id="description"
                  inputProps={{
                    maxLength: 60,
                    autoComplete: "off",
                    form: {
                      autoComplete: "off",
                    },
                  }}
                  label={c("form.fields.description")}
                  variant="outlined"
                  {...field}
                />
              )}
            </Field>
            <Divider marginTop={20} />
            <Box display="flex" flex={1} justifyContent="flex-end">
              <Button
                handleClick={submitForm}
                title={c("honeycombModal.buttonStep1")}
                disabled={!values.room}
              />
            </Box>
          </Box>
        </Form>
      )}
    </Formik>
  );
}
