import React, { useState } from "react";
import Modal from "@/components/ui/Modal";
import { EnvironmentEnum } from "@/enums/*";
import { useTranslation } from "next-i18next";
import {
  addParticipantToConversation,
  listableRoom,
  addDescriptionConversation,
  createGroup,
} from "@/services/talk/room";
import { useRouter } from "next/router";
import { toast } from "@/utils/notifications";
import { useDispatch, useSelector } from "react-redux";
import { addHoneycomb } from "@/store/actions/honeycomb";
import { PropsUserSelector } from "@/types/*";
import { createDirectory, existDirectory } from "@/services/webdav/directories";
import { addLibraryFile } from "@/store/actions/library";
import {
  LibraryItemInterface,
  MemberInterface,
  TimeDescriptionInterface,
} from "@/interfaces/index";
import { dateDescription } from "@/utils/utils";
import { createShare } from "@/services/share/share";
import SimpleBackdrop from "@/components/ui/Backdrop";
import FirstStep from "./Steps/First";
import SecondStep from "./Steps/Second";
import { getHoneycombLink } from "@/utils/offlineNavigation";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type Props = {
  open: boolean;
  handleClose: () => void;
  isChannel?: boolean;
};

export default function NewHoneycombModal({ open, handleClose, isChannel = false }: Props) {
  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;
  const [step, setStep] = useState(1);
  const [participants, setParticipants] = useState<MemberInterface[]>([]);
  const dispatch = useDispatch();
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const timeDescription: TimeDescriptionInterface = c("timeDescription", { returnObjects: true });
  const [submitting, setSubmitting] = useState(false);
  const [room, setRoom] = useState("");
  const [description, setDescription] = useState("");

  const handleSubmit = async () => {
    await (async () => {
      try {
        setSubmitting(true);

        if (userRdx.user.id === room) {
          throw new Error(c("reservedNameError"));
        }

        if (room.indexOf("/") !== -1) {
          throw new Error(
            c("form.slashNotAllowed", {
              field: c("form.fields.name"),
            }),
          );
        }

        const directoryExists = await existDirectory(userId, room);
        if (directoryExists) {
          throw new Error(c("honeycombModal.errorCreatePanal"));
        }

        const conversation = await createGroup(room);
        const { token, canDeleteConversation } = conversation.data.ocs.data;

        if (description) {
          await addDescriptionConversation(token, description.trim());
        }

        // eslint-disable-next-line no-restricted-syntax
        for (const participant of participants) {
          try {
            // eslint-disable-next-line no-await-in-loop
            await addParticipantToConversation(token, participant.username);
          } catch (e) {
            console.log("error adding participant", participant.username);
          }
        }

        if (isChannel) {
          await listableRoom(token);
        }

        dispatch(addHoneycomb(conversation.data.ocs.data));

        const folderName = conversation.data.ocs.data.displayName;

        const create = await createDirectory(userId, folderName);
        if (create) {
          const date = new Date();
          const item: LibraryItemInterface = {
            basename: folderName,
            id: "",
            filename: "",
            aliasFilename: "",
            type: "directory",
            environment: EnvironmentEnum.REMOTE,
            createdAt: date,
            createdAtDescription: dateDescription(date, timeDescription),
          };
          dispatch(addLibraryFile(item));
          await createShare(token, folderName);
        }

        toast(c("honeycombModal.chatRoomSuccess"), "success");
        handleClose();
        await router.push(
          getHoneycombLink(token, room, Number(canDeleteConversation), connectionStatus),
        );
      } catch (e) {
        console.log(e);
        const msg = e.message ? e.message : c("honeycombModal.chatRoomFailed");
        toast(msg, "error");
      } finally {
        setSubmitting(false);
      }
    })();
  };

  const handleNextStep = (room: string, description: string) => {
    setRoom(room);
    setDescription(description);
    setStep(2);
  };

  return (
    <Modal
      data-testid="modal-create-honeycomb"
      title={c("addHoneycombTitle")}
      subtitle={isChannel ? c("channel") : c("groupConversation")}
      open={open}
      handleClose={submitting ? undefined : handleClose}
    >
      {step === 1 && (
        <FirstStep room={room} description={description} handleNextStep={handleNextStep} />
      )}
      {step === 2 && (
        <SecondStep
          handleBackStep={() => setStep(1)}
          updateParticipants={(part: MemberInterface[]) => setParticipants(part)}
          participants={participants}
          handleSubmit={handleSubmit}
          submitting={submitting}
        />
      )}
      <SimpleBackdrop open={submitting} />
    </Modal>
  );
}
