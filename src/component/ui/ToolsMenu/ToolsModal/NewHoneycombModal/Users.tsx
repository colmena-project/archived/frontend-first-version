/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { MemberInterface } from "@/interfaces/index";
import UserListSkeleton from "@/components/ui/skeleton/UsersList";
import { allUsers } from "@/services/internal/users";
import UsersList from "./UsersList";

type Props = {
  userId: string;
  participants: MemberInterface[];
  updateParticipants: (part: MemberInterface[]) => void;
  search: string;
  setSearch: (keyword: string) => void;
  isSubmitting: boolean;
};

export default function Users({
  userId,
  participants,
  updateParticipants,
  search,
  setSearch,
  isSubmitting,
}: Props) {
  const { data } = allUsers({
    revalidateOnFocus: false,
  });

  const users: MemberInterface[] = [];
  if (data) {
    data.forEach((member: MemberInterface) => {
      if (member.username !== userId) {
        users.push(member);
      }
    });
  }

  if (!data || isSubmitting) return <UserListSkeleton />;

  return (
    <UsersList
      participants={participants}
      updateParticipants={updateParticipants}
      users={users}
      setSearch={setSearch}
      search={search}
      userId={userId}
    />
  );
}

export const UsersMemoized = React.memo(Users);
