/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from "react";
import { UsersListItemMemoized } from "./UsersListItem";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import { makeStyles } from "@material-ui/core";
import Chip from "@material-ui/core/Chip";
import Box from "@material-ui/core/Box";
import { useTranslation } from "next-i18next";
import { MemberInterface } from "@/interfaces/index";
import { prepareSearchString } from "@/utils/utils";
import SearchInput from "@/components/ui/SearchInput";
import SvgIconAux from "@/components/ui/SvgIcon";
import Clickable from "@/components/ui/Clickable";
import { useTheme } from "@material-ui/core/styles";
import UserFiltersDrawer, {
  FiltersProps,
} from "@/components/ui/ToolsMenu/ToolsModal/NewHoneycombModal/FiltersDrawer";
import { getGroups, getMembers } from "@/services/internal/groups";
import { GroupInterface } from "@/interfaces/keycloak";
import UserListSkeleton from "@/components/ui/skeleton/UsersList";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  list: {
    textAlign: "left",
    alignItems: "stretch",
    height: 250,
  },
  verticalList: {
    padding: 1,
  },
  searchRoot: {
    display: "flex",
    marginBottom: theme.spacing(2),
  },
  searchWrapper: {
    width: "100%",
  },
  filter: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "0 5px 0 13px",
  },
  filtersDescription: {
    color: theme.palette.secondary.main,
  },
}));

type Props = {
  users: MemberInterface[];
  participants: MemberInterface[];
  updateParticipants: (part: MemberInterface[]) => void;
  search: string;
  setSearch: (keyword: string) => void;
  userId: string;
};

function UsersList({ users, participants, updateParticipants, search, setSearch, userId }: Props) {
  const { t: c } = useTranslation("common");
  const classes = useStyles();
  const theme = useTheme();
  const [openFilters, setOpenFilters] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadingOpenFilters, setLoadingOpenFilters] = useState(false);
  const [groups, setGroups] = useState<GroupInterface[]>([]);
  const [items, setItems] = useState(users);
  const [usersSelected, setUsersSelected] = useState<MemberInterface[]>(participants);
  const [filterDescriptions, setFilterDescriptions] = useState<string[]>([]);
  const [userFilters, setUserFilters] = useState<FiltersProps>({});

  function selectUser(user: MemberInterface) {
    const find = usersSelected.find((item) => item.username === user.username);
    if (!find) {
      setUsersSelected(usersSelected?.concat(user));
      updateParticipants(usersSelected?.concat(user));
    } else {
      const aux = usersSelected.filter((item) => item.username !== user.username);
      setUsersSelected(aux);
      updateParticipants(aux);
    }
  }

  const filterItems = async (users: MemberInterface[], { group }: FiltersProps) => {
    setLoading(true);

    let filteredUsers = users;
    const newFilters: string[] = [];

    if (group) {
      const parsedGroup = JSON.parse(group);
      const members = await getMembers(parsedGroup.id);
      if (members) {
        filteredUsers = members.data.data;
        filteredUsers = filteredUsers.filter((item) => item.username !== userId);
      }

      newFilters.push(parsedGroup.name);
    }
    setFilterDescriptions(newFilters);
    setLoading(false);

    return filteredUsers;
  };

  const searchItems = (users: MemberInterface[], keyword: string) =>
    users.filter((item: MemberInterface) => {
      const value = prepareSearchString(keyword);
      const username = prepareSearchString(item.username);
      const firstName = prepareSearchString(item.firstName);
      const lastName = prepareSearchString(item.lastName);
      return (
        username.indexOf(value) >= 0 ||
        firstName.indexOf(value) >= 0 ||
        lastName.indexOf(value) >= 0
      );
    });

  useEffect(() => {
    (async () => {
      let filteredUsers = users;
      if (userFilters.group !== "all") {
        filteredUsers = await filterItems(filteredUsers, userFilters);
      } else setFilterDescriptions([]);

      if (search) {
        filteredUsers = searchItems(filteredUsers, search);
      }

      setItems(filteredUsers);
    })();
  }, [users, search, userFilters]);

  useEffect(() => {
    if (!search) {
      setItems(users);
      (async () => {
        const groups = await getGroups();
        if (groups && groups.data.success) {
          setGroups(groups.data.data);
        }
      })();
    }
  }, []);

  const handleOpenFilters = useCallback(async () => {
    setLoadingOpenFilters(true);
    if (!groups) {
      const groups = await getGroups();
      if (groups && groups.data.success) {
        setGroups(groups.data.data);
      }
    }

    setLoadingOpenFilters(false);
    setOpenFilters(true);
  }, []);

  return (
    <Box>
      <Box className={classes.searchRoot}>
        <Box className={classes.searchWrapper}>
          <SearchInput
            search={(keyword) => setSearch(keyword)}
            keyword={search}
            placeholder={c("honeycombModal.searchUserPlaceholder")}
          />
        </Box>
        {loadingOpenFilters ? (
          <Box className={classes.filter}>
            <CircularProgress variant="indeterminate" size={22} />
          </Box>
        ) : (
          <Clickable className={classes.filter} handleClick={handleOpenFilters}>
            <SvgIconAux icon="settings_adjust" fontSize={22} htmlColor={theme.palette.gray.main} />
          </Clickable>
        )}
      </Box>
      {Boolean(filterDescriptions.length) && (
        <Text className={classes.filtersDescription} variant={TextVariantEnum.BODY2}>
          <strong>{c("newHoneycombFilter.activeFilters")}:</strong> {filterDescriptions.join(", ")}
        </Text>
      )}
      {usersSelected.length > 0 && (
        <Box
          display="flex"
          flex="1"
          flexDirection="row"
          flexWrap="wrap"
          style={{
            maxHeight: 70,
            overflow: "scroll",
            paddingBottom: 12,
            marginTop: 8,
            marginBottom: 8,
          }}
        >
          {usersSelected.reverse().map((item) => (
            <Chip
              key={`chip-user-${item.id}`}
              label={`${item.firstName} ${item.lastName}`}
              style={{ margin: 2 }}
              onDelete={() => selectUser(item)}
            />
          ))}
        </Box>
      )}
      {loading && <UserListSkeleton />}
      {!loading && (
        <Box style={{ height: "50vh", overflow: "auto" }}>
          <List className={classes.list}>
            {items.length > 0 &&
              items.map((item: MemberInterface, idx: number) => (
                <ListItem
                  button
                  onClick={() => selectUser(item)}
                  key={`list-item-users-list-${item.id}`}
                  disableGutters
                  className={classes.verticalList}
                >
                  <UsersListItemMemoized
                    selected={Boolean(
                      usersSelected.find((userSelected) => userSelected.username === item.username),
                    )}
                    user={item}
                    backgroundColor={idx % 2 === 0 ? "#fff" : "#F9F9F9"}
                  />
                </ListItem>
              ))}
          </List>
        </Box>
      )}
      <UserFiltersDrawer
        open={openFilters}
        handleClose={() => setOpenFilters(false)}
        groups={groups}
        filterUsers={setUserFilters}
        initialFilters={userFilters}
      />
    </Box>
  );
}

export default UsersList;

export const UsersListMemoized = React.memo(UsersList);
