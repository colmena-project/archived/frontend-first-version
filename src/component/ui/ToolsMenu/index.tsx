import React, { ReactNode, useState } from "react";
import Fade from "@material-ui/core/Fade";
import { useTheme } from "@material-ui/core/styles";
import DrawerMoreOptions from "./DrawerMoreOptions";
import ToolsMenuOption from "./ToolsMenuOption";
import { ToolsMenuButton, BoxOptions, MyDialog } from "./styled";
import { useTranslation } from "next-i18next";

type ToolsMenuProps = {
  children?: ReactNode;
  showMoreButton?: boolean;
  loading?: boolean;
};

function ToolsMenu({ children, showMoreButton = true, loading = false }: ToolsMenuProps) {
  const theme = useTheme();
  const { t } = useTranslation("common");
  const [open, setOpen] = useState(false);
  const [openDrawer, setOpenDrawer] = useState(false);
  const openDirectMoreOptions = !children;

  const handleOpen = () => {
    if (openDirectMoreOptions) {
      openDrawerMoreOptions();
      return;
    }

    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const closeDrawerMoreOptions = () => {
    setOpenDrawer(false);
  };

  const openDrawerMoreOptions = () => {
    setOpenDrawer(true);
  };

  return (
    <>
      {!open && (
        <ToolsMenuButton
          aria-label="tools"
          handleClick={handleOpen}
          icon={loading ? "dots_three_horizontal" : "plus"}
          iconSize={20}
          data-testid="tools-menu"
          iconColor="#fff"
          backgroundColor={loading ? "gray.main" : "secondary.main"}
          disabled={loading}
        />
      )}
      <MyDialog open={open} onClose={handleClose} fullWidth>
        <Fade in={open}>
          <BoxOptions onClick={handleClose}>
            {children}
            {showMoreButton && !openDirectMoreOptions && (
              <ToolsMenuOption
                onClick={openDrawerMoreOptions}
                icon="more_vertical"
                iconSize={12}
                iconColor={theme.palette.gray.main}
                textColor={theme.palette.gray.main}
              >
                {t("toolsMenu.more")}
              </ToolsMenuOption>
            )}
            <ToolsMenuButton
              handleClick={handleClose}
              icon="close"
              iconSize={15}
              data-testid="close-tools-menu"
              iconColor={theme.palette.gray.main}
              backgroundColor={theme.palette.gray.light}
            />
          </BoxOptions>
        </Fade>
      </MyDialog>
      <DrawerMoreOptions
        open={openDrawer}
        handleOpen={openDrawerMoreOptions}
        handleClose={closeDrawerMoreOptions}
      />
    </>
  );
}

export { ToolsMenuOption };

export default ToolsMenu;

export const ToolsMenuMemoized = React.memo(ToolsMenu);
