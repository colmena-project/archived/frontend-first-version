import React, { useState } from "react";
import ToolsMenu, { ToolsMenuOption } from "@/components/ui/ToolsMenu";
import { isSubadminProfile } from "@/utils/permissions";
import NewHoneycombModal from "../ToolsModal/NewHoneycombModal";
import NewConversationModal from "../ToolsModal/NewConversationModal";
import { toast } from "@/utils/notifications";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export default function HoneycombToolsMenu() {
  const { t } = useTranslation("common");
  const [openNewHoneycombModal, setOpenNewHoneycombModal] = useState(false);
  const [openNewConversationModal, setOpenNewConversationModal] = useState(false);
  const [isChannel, setIsChannel] = useState(false);
  const router = useRouter();
  const connectionStatus = useConnectionStatus();

  const handleOpenNewHoneycombModal = () => {
    if (isSubadminProfile()) {
      setOpenNewHoneycombModal(true);
      setIsChannel(false);
    } else {
      toast(t("noPrivilegesAccessTitle"), "error");
    }
  };

  const handleOpenNewChannelModal = () => {
    if (isSubadminProfile()) {
      setOpenNewHoneycombModal(true);
      setIsChannel(true);
    } else {
      toast(t("noPrivilegesAccessTitle"), "error");
    }
  };

  const handleCloseNewHoneycombModal = () => {
    setOpenNewHoneycombModal(false);
  };

  const handleOpenNewConversationModal = () => {
    setOpenNewConversationModal(true);
  };

  const handleCloseNewConversationModal = () => {
    setOpenNewConversationModal(false);
  };

  return (
    <>
      <ToolsMenu showMoreButton={connectionStatus}>
        <ToolsMenuOption
          icon="two_users"
          onClick={handleOpenNewConversationModal}
          iconSize={16}
          disabled={!connectionStatus}
        >
          {t("toolsMenu.createConversation")}
        </ToolsMenuOption>
        <ToolsMenuOption
          icon="user_group"
          onClick={handleOpenNewHoneycombModal}
          disabled={!connectionStatus}
        >
          {t("toolsMenu.createGroup")}
        </ToolsMenuOption>
        <ToolsMenuOption
          icon="new_channel"
          onClick={handleOpenNewChannelModal}
          iconSize={13}
          disabled={!connectionStatus}
        >
          {t("toolsMenu.createChannel")}
        </ToolsMenuOption>
        <ToolsMenuOption
          icon="channel"
          onClick={() => router.push("/channels")}
          iconSize={12}
          disabled={!connectionStatus}
        >
          {t("toolsMenu.exploreChannels")}
        </ToolsMenuOption>
      </ToolsMenu>

      {openNewHoneycombModal && (
        <NewHoneycombModal
          open={openNewHoneycombModal}
          isChannel={isChannel}
          handleClose={handleCloseNewHoneycombModal}
        />
      )}

      {openNewConversationModal && (
        <NewConversationModal
          open={openNewConversationModal}
          handleClose={handleCloseNewConversationModal}
        />
      )}
    </>
  );
}
