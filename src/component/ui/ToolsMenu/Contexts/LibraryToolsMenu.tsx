import React, { useState } from "react";
import ToolsMenu, { ToolsMenuOption } from "@/components/ui/ToolsMenu";
import NewEditorModal from "../ToolsModal/NewEditorModal";
import NewFolderModal from "../ToolsModal/NewFolderModal";
import UploadModal from "../ToolsModal/UploadModal";
import { useTranslation } from "next-i18next";
import { convertUsernameToPrivate, getTalkPath, isPrivate } from "@/utils/directory";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";

export default function LibraryToolsMenu() {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const router = useRouter();
  const path = router.asPath.replace(/\/library(\/|$)/, "");
  const isPrivateFolder = Boolean(isPrivate(convertUsernameToPrivate(path, userRdx.user.id)));
  const isLibraryRootPath = path === "";
  const isTalkPath = path === getTalkPath();
  const { t } = useTranslation("common");
  const [openNewFolderModal, setOpenNewFolderModal] = useState(false);
  const [openUploadModal, setOpenUploadModal] = useState(false);
  const [openNewEditorModal, setOpenNewEditorModal] = useState(false);

  const handleOpenNewFolderModal = () => {
    setOpenNewFolderModal(true);
  };
  const handleCloseNewFolderModal = () => {
    setOpenNewFolderModal(false);
  };

  const handleOpenEditorModal = () => {
    setOpenNewEditorModal(true);
  };

  const handleCloseNewEditorModal = () => {
    setOpenNewEditorModal(false);
  };

  const handleOpenUploadModal = () => {
    setOpenUploadModal(true);
  };

  const handleCloseUploadModal = () => {
    setOpenUploadModal(false);
  };

  if (isLibraryRootPath || isTalkPath) {
    return null;
  }

  return (
    <>
      <ToolsMenu showMoreButton={isPrivateFolder}>
        <ToolsMenuOption icon="upload" onClick={handleOpenUploadModal}>
          {t("toolsMenu.upload")}
        </ToolsMenuOption>
        {isPrivateFolder && (
          <>
            <ToolsMenuOption icon="add_folder" onClick={handleOpenNewFolderModal}>
              {t("toolsMenu.createFolder")}
            </ToolsMenuOption>
            <ToolsMenuOption icon="edit_text" onClick={handleOpenEditorModal}>
              {t("toolsMenu.createFile")}
            </ToolsMenuOption>
          </>
        )}
      </ToolsMenu>

      {openUploadModal && (
        <UploadModal open={openUploadModal} handleClose={handleCloseUploadModal} />
      )}

      {openNewFolderModal && (
        <NewFolderModal open={openNewFolderModal} handleClose={handleCloseNewFolderModal} />
      )}

      {openNewEditorModal && (
        <NewEditorModal open={openNewEditorModal} handleClose={handleCloseNewEditorModal} />
      )}
    </>
  );
}
