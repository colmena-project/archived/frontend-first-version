import React, { useState } from "react";
import ToolsMenu, { ToolsMenuOption } from "@/components/ui/ToolsMenu";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import NewEditorModal from "../ToolsModal/NewEditorModal";
import UploadModal from "../ToolsModal/UploadModal";
import { PropsLibrarySelector } from "@/types/index";
import { useSelector } from "react-redux";
import { DirectoryNamesNCEnum } from "@/enums/*";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type Props = {
  handleJoinCall: () => void;
  loading: boolean;
};

export default function HoneycombChatToolsMenu({ handleJoinCall, loading }: Props) {
  const { t } = useTranslation("common");
  const { t: l } = useTranslation("library");
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const [openUploadModal, setOpenUploadModal] = useState(false);
  const [openNewEditorModal, setOpenNewEditorModal] = useState(false);
  const libraryRdx = useSelector((state: { library: PropsLibrarySelector }) => state.library);

  const handleOpenUploadModal = () => {
    setOpenUploadModal(true);
  };

  const handleCloseUploadModal = () => {
    setOpenUploadModal(false);
  };

  const handleOpenEditorModal = () => {
    setOpenNewEditorModal(true);
  };

  const handleCloseNewEditorModal = () => {
    setOpenNewEditorModal(false);
  };

  const openRecordPage = () => {
    const { currentPath } = libraryRdx;
    const chatPath = currentPath.replace(l("talkFolderName"), DirectoryNamesNCEnum.TALK);
    const arrChatPath = chatPath.split("/");
    if (arrChatPath.length === 1) router.push(`/record?chatPath=${chatPath}`);
    else if (arrChatPath.length > 1) router.push(`/record?chatPath=${arrChatPath[1]}`);
  };

  return (
    <>
      <ToolsMenu loading={loading}>
        <ToolsMenuOption
          icon="upload"
          onClick={handleOpenUploadModal}
          disabled={!connectionStatus}
          data-testid="tools-menu-honeycomb-upload"
        >
          {t("toolsMenu.upload")}
        </ToolsMenuOption>
        <ToolsMenuOption
          icon="edit_text"
          onClick={handleOpenEditorModal}
          disabled={!connectionStatus}
          data-testid="tools-menu-honeycomb-edit-text"
        >
          {t("toolsMenu.createFile")}
        </ToolsMenuOption>
        <ToolsMenuOption
          icon="microphone"
          onClick={openRecordPage}
          disabled={!connectionStatus}
          data-testid="tools-menu-honeycomb-recorder"
        >
          {t("toolsMenu.recordAudio")}
        </ToolsMenuOption>
        <ToolsMenuOption icon="call_headphone" onClick={handleJoinCall}>
          {t("toolsMenu.recordInterview")}
        </ToolsMenuOption>
      </ToolsMenu>

      {openUploadModal && (
        <UploadModal open={openUploadModal} handleClose={handleCloseUploadModal} />
      )}

      {openNewEditorModal && (
        <NewEditorModal open={openNewEditorModal} handleClose={handleCloseNewEditorModal} />
      )}
    </>
  );
}
