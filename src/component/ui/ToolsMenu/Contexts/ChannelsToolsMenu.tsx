import React, { useState } from "react";
import ToolsMenu, { ToolsMenuOption } from "@/components/ui/ToolsMenu";
import { isSubadminProfile } from "@/utils/permissions";
import NewHoneycombModal from "../ToolsModal/NewHoneycombModal";
import { toast } from "@/utils/notifications";
import { useTranslation } from "next-i18next";

export default function ChannelsToolsMenu() {
  const { t } = useTranslation("common");
  const [openNewHoneycombModal, setOpenNewHoneycombModal] = useState(false);
  const [isChannel, setIsChannel] = useState(false);

  const handleOpenNewHoneycombModal = () => {
    if (isSubadminProfile()) {
      setOpenNewHoneycombModal(true);
      setIsChannel(false);
    } else {
      toast(t("noPrivilegesAccessTitle"), "error");
    }
  };

  const handleOpenNewChannelModal = () => {
    if (isSubadminProfile()) {
      setOpenNewHoneycombModal(true);
      setIsChannel(true);
    } else {
      toast(t("noPrivilegesAccessTitle"), "error");
    }
  };

  const handleCloseNewHoneycombModal = () => {
    setOpenNewHoneycombModal(false);
  };

  return (
    <>
      <ToolsMenu>
        <ToolsMenuOption icon="user_group" onClick={handleOpenNewHoneycombModal}>
          {t("toolsMenu.createGroup")}
        </ToolsMenuOption>
        <ToolsMenuOption icon="new_channel" onClick={handleOpenNewChannelModal} iconSize={13}>
          {t("toolsMenu.createChannel")}
        </ToolsMenuOption>
      </ToolsMenu>

      {openNewHoneycombModal && (
        <NewHoneycombModal
          open={openNewHoneycombModal}
          isChannel={isChannel}
          handleClose={handleCloseNewHoneycombModal}
        />
      )}
    </>
  );
}
