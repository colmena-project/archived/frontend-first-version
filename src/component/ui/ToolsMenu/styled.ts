import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@/components/ui/Button/IconButton";

export const ToolsMenuButton = styled(IconButton)({
  position: "fixed",
  right: 8,
  bottom: 68,
  borderRadius: "50%!important",
  minWidth: 40,
  width: 40,
  height: 40,
});

export const BoxOptions = styled(Box)({
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-end",
});

export const MyDialog = styled(Dialog)({
  "& .MuiPaper-root": {
    background: "none",
    boxShadow: "none",
    position: "fixed",
    bottom: 110,
    right: 0,
    margin: "5px 0px",
    padding: "0 15px",
    width: "100%",
  },
});
