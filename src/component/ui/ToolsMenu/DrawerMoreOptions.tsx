/* eslint-disable prettier/prettier */
import React, { useState } from "react";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import { useRouter } from "next/router";
import { v4 as uuid } from "uuid";
import { useTranslation } from "next-i18next";
import NewFolderModal from "./ToolsModal/NewFolderModal";
import UploadModal from "./ToolsModal/UploadModal";
import { makeStyles, Theme } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import IconButton from "@/components/ui/IconButton";
import theme from "@/styles/theme";
import Box from "@material-ui/core/Box";
import { ButtonColorEnum, ButtonSizeEnum, ButtonVariantEnum } from "@/enums/index";
import Button from "@/components/ui/Button";
import NewEditorModal from "./ToolsModal/NewEditorModal";
import { DefaultConfigButtonInterface } from "@/interfaces/index";
// import { generateVirtualStudioHash } from "@/utils/utils";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type Props = {
  open: boolean;
  handleClose: () => void;
  handleOpen: () => void;
};

const useStyles = makeStyles((theme: Theme) => ({
  wrapperContentDrawer: {
    "& .MuiDrawer-paper": {
      borderRadius: "30px 30px 0 0",
      background: `${theme.palette.gray.light}`,
    },
  },
  buttonCancel: {
    color: theme.palette.variation3.main,
    border: `1px solid ${theme.palette.variation3.main}`,
  },
}));

export default function DrawerMoreOptions({ open, handleOpen, handleClose }: Props) {
  const [openNewFolderModal, setOpenNewFolderModal] = useState(false);
  const [openUploadModal, setOpenUploadModal] = useState(false);
  const [openNewEditorModal, setOpenNewEditorModal] = useState(false);
  const connectionStatus = useConnectionStatus();

  const { t } = useTranslation("common");
  const router = useRouter();
  const navigate = (page: string) => {
    router.push(page);
  };

  const classes = useStyles();

  const handleOpenNewFolderModal = () => {
    setOpenNewFolderModal(true);
    handleClose();
  };

  const handleOpenEditorModal = () => {
    setOpenNewEditorModal(true);
    handleClose();
  };

  const handleCloseNewFolderModal = () => {
    setOpenNewFolderModal(false);
  };

  const handleOpenUploadModal = () => {
    setOpenUploadModal(true);
    handleClose();
  };

  const handleCloseUploadModal = () => {
    setOpenUploadModal(false);
  };

  const handleCloseNewEditorModal = () => {
    setOpenNewEditorModal(false);
    handleClose();
  };

  const handleOpenPublicationPage = () => {
    navigate("/publications/new");
  };

  const defaultConfigButton: DefaultConfigButtonInterface = {
    color: {
      main: theme.palette.variation3.main,
      light: theme.palette.variation3.light,
    },
    fontSizeIcon: "large",
    fontSize: 12,
  };

  const handleOpenVirtualStudioPage = () => {
    // navigate(`/virtual-studio/${generateVirtualStudioHash()}`);
  };

  return (
    <>
      <SwipeableDrawer
        anchor="bottom"
        open={open}
        onClose={handleClose}
        onOpen={handleOpen}
        className={classes.wrapperContentDrawer}
      >
        <Box padding={2}>
          <Grid container spacing={2}>
            <Grid item xs={3} key={uuid()}>
              <IconButton
                icon="upload"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("uploadFileDrawerBottomTitle")}
                handleClick={handleOpenUploadModal}
                disabled={!connectionStatus}
              />
            </Grid>
            <Grid item xs={3}>
              <IconButton
                icon="add_folder"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("addFolderDrawerBottomTitle")}
                handleClick={handleOpenNewFolderModal}
                disabled={!connectionStatus}
              />
            </Grid>
            <Grid item xs={3}>
              <IconButton
                icon="edit_outlined"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("newPublicationDrawerBottomTitle")}
                handleClick={handleOpenPublicationPage}
                disabled={!connectionStatus}
              />
            </Grid>
            <Grid item xs={3}>
              <IconButton
                icon="edit_text"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("textDrawerBottomTitle")}
                handleClick={handleOpenEditorModal}
                disabled={!connectionStatus}
              />
            </Grid>
            <Grid item xs={3}>
              <IconButton
                icon="call_headphone"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("studioDrawerBottomTitle")}
                handleClick={handleOpenVirtualStudioPage}
                disabled
              />
            </Grid>
            <Grid item xs={3} key={uuid()}>
              <IconButton
                icon="microphone"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("audioDrawerBottomTitle")}
                handleClick={() => navigate("/record")}
              />
            </Grid>
            <Grid item xs={3} key={uuid()}>
              <IconButton
                icon="stream"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("streamingDrawerBottomTitle")}
                handleClick={() => navigate("/stream")}
                disabled={!connectionStatus}
              />
            </Grid>
            <Grid item xs={3} key={uuid()}>
              <IconButton
                icon="audio_editor"
                iconColor={defaultConfigButton.color.main}
                fontSizeIcon={defaultConfigButton.fontSizeIcon}
                textStyle={{
                  fontSize: defaultConfigButton.fontSize,
                  color: defaultConfigButton.color.main,
                }}
                title={t("audioEditorDrawerBottomTitle")}
                handleClick={() => navigate("/edit-audio")}
              />
            </Grid>
          </Grid>
        </Box>
        <Box margin={1}>
          <Button
            size={ButtonSizeEnum.SMALL}
            title={t("form.cancelButton")}
            handleClick={handleClose}
            variant={ButtonVariantEnum.TEXT}
            color={ButtonColorEnum.DEFAULT}
            className={classes.buttonCancel}
            fullWidth
          />
        </Box>
      </SwipeableDrawer>

      {openUploadModal && (
        <UploadModal open={openUploadModal} handleClose={handleCloseUploadModal} />
      )}

      {openNewFolderModal && (
        <NewFolderModal open={openNewFolderModal} handleClose={handleCloseNewFolderModal} />
      )}

      {openNewEditorModal && (
        <NewEditorModal open={openNewEditorModal} handleClose={handleCloseNewEditorModal} />
      )}
    </>
  );
}
