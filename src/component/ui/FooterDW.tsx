import React from "react";
import Image from "next/image";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";

const useStyles = makeStyles((theme) => ({
  root: {
    top: "auto",
    bottom: 0,
    backgroundColor: "transparent",
    border: "none",
  },
  banner: {
    display: "flex",
    padding: 0.7,
    justifyContent: "center",
    backgroundColor: "#fff",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: theme.palette.gray.light,
    borderTop: 1,
    paddingTop: 4,
    borderStyle: "solid",
    boxShadow: "0px -2px 5px rgba(149, 149, 149, 0.4)",
  },
}));

type Props = {
  position?: "fixed" | "relative";
};

export default function FooterDW({ position = "fixed" }: Props) {
  const classes = useStyles();
  return (
    <AppBar position={position} className={classes.root}>
      <Box className={classes.banner}>
        <Image
          src="/images/rodape_DW.png"
          layout="fixed"
          width={346}
          height={80}
          aria-label="footer-image"
        />
      </Box>
    </AppBar>
  );
}
