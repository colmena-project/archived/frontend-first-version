import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import Button from "@/components/ui/Button";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Icon = styled(Box)(({ theme }: StyledProps) => ({
  marginRight: theme.spacing(1),
}));

export const ToolbarTitle = styled(Text)(({ theme }: StyledProps) => ({
  flexGrow: 1,
  color: theme.palette.primary.main,
  fontSize: 18,
  fontWeight: "bold",
  textAlign: "left",
}));

export const ToolbarButton = styled(Button)(({ theme }: StyledProps) => ({
  color: theme.palette.variation1.main,
  border: `1px solid ${theme.palette.variation1.main}`,
  borderRadius: "20px",
  padding: "0px 10px",
}));
