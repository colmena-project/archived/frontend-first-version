/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import Box from "@material-ui/core/Box";
import { ButtonVariantEnum, TextVariantEnum } from "@/enums/*";
import { useTranslation } from "react-i18next";
import router from "next/router";
import SvgIconAux from "@/components/ui/SvgIcon";
import theme from "@/styles/theme";
import { AllIconProps } from "@/types/*";
import { Icon, ToolbarTitle, ToolbarButton } from "./styled";

type Props = {
  title: string;
  icon?: AllIconProps;
  link?: string;
  seeAllTitle?: string | undefined;
  showRightButton?: boolean;
  noMargin?: boolean;
  handleClick?: () => void;
};

export default function ToolbarSection({
  icon,
  title,
  link = "",
  seeAllTitle,
  showRightButton = true,
  noMargin = false,
  handleClick,
}: Props) {
  const goTo = () => {
    if (!link) return;

    router.push(link);
  };
  const { t } = useTranslation("common");
  return (
    <Box
      display="flex"
      marginLeft={!noMargin ? 2 : undefined}
      marginRight={!noMargin ? 2 : undefined}
      flexDirection="row"
      justifyContent={showRightButton ? "space-between" : "flex-start"}
      alignItems="center"
    >
      {icon && (
        <Icon>
          <SvgIconAux icon={icon} fontSize={20} htmlColor={theme.palette.primary.main} />
        </Icon>
      )}

      <ToolbarTitle variant={TextVariantEnum.H6}>{title}</ToolbarTitle>
      {showRightButton && (
        <ToolbarButton
          title={!seeAllTitle ? t("seeAllTitle") : seeAllTitle}
          variant={ButtonVariantEnum.TEXT}
          data-testid="toolbar-click"
          handleClick={() => (handleClick ? handleClick() : goTo())}
        />
      )}
    </Box>
  );
}

export const ToolbarSectionMemoized = React.memo(ToolbarSection);
