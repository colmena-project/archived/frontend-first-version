import { WizardFeatures } from "@/interfaces/index";
import { updateWizard } from "@/store/actions/wizard";
import { PropsWizardSelector } from "@/types/*";
import { Box, Checkbox, FormControlLabel, FormGroup, makeStyles } from "@material-ui/core";
import { useTranslation } from "next-i18next";
import { useDispatch, useSelector } from "react-redux";
import Text from "../Text";

const useStyles = makeStyles(() => ({
  wrapper: {},
  checkbox: {
    marginTop: "20px",
  },
}));

interface WizardWrapperProps {
  content: string;

  feature: WizardFeatures;
}

function WizardWrapper({ content, feature }: WizardWrapperProps) {
  const { t } = useTranslation("common");
  const { wizard } = useSelector((state: { wizard: PropsWizardSelector }) => state.wizard);
  const dispatch = useDispatch();
  const classes = useStyles();

  const onDispatch = () => dispatch(updateWizard(feature));

  return (
    <Box className={classes.wrapper}>
      <Text>{content}</Text>
      <Box className={classes.checkbox}>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                checked={!wizard[feature]}
                onChange={onDispatch}
                inputProps={{ "aria-label": "controlled" }}
              />
            }
            label={t("wizard.action.labelChecked")}
          />
        </FormGroup>
      </Box>
    </Box>
  );
}

export default WizardWrapper;
