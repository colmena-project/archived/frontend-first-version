import React, { ReactElement } from "react";
import { BadgeVariantEnum } from "@/enums/*";
import { Box } from "@material-ui/core";
import theme from "@/styles/theme";

type Props = {
  description?: string | ReactElement;
  bgColor?: string;
  textColor?: string;
  fontSize?: string | number;
  padding?: string | number;
  variant?: BadgeVariantEnum;
  display?: string;
  borderRadius?: string | number;
  borderWidth?: string | number;
  borderColor?: string;
  className?: string;
};

const defaultData = {
  textColor: theme.palette.grey[800],
  bgColor: theme.palette.grey[50],
};

const getVariant = (variant: BadgeVariantEnum) => {
  let { textColor } = defaultData;
  let { bgColor } = defaultData;

  switch (variant) {
    case BadgeVariantEnum.SUCCESS:
      textColor = "#2A6021";
      bgColor = "#B5FFA2";
      break;
    case BadgeVariantEnum.ERROR:
      textColor = "#931600";
      bgColor = "#FF9D9D";
      break;
    case BadgeVariantEnum.INFO:
      textColor = "#193549";
      bgColor = "#A3EDFD";
      break;
    case BadgeVariantEnum.WARNING:
      textColor = "#931600";
      bgColor = "#FF9D9D";
      break;

    default:
      break;
  }

  return {
    textColor,
    bgColor,
  };
};

export default function Badge({
  description,
  bgColor,
  textColor,
  fontSize = "1em",
  padding = "2px 6px",
  variant,
  display = "inline-block",
  borderRadius = "5px",
  borderColor = "",
  borderWidth = 0,
  className,
}: Props) {
  let background = defaultData.bgColor;
  let color = defaultData.textColor;

  if (variant) {
    const variantData = getVariant(variant);
    background = variantData.bgColor;
    color = variantData.textColor;
  }

  if (bgColor) {
    background = bgColor;
  }

  if (textColor) {
    color = textColor;
  }

  return (
    <Box
      component="span"
      className={className}
      style={{
        background,
        color,
        padding,
        display,
        borderRadius,
        fontSize,
        borderColor,
        borderWidth,
      }}
    >
      {description}
    </Box>
  );
}
