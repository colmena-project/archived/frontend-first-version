import React from "react";
import Button from "@material-ui/core/Button";
import { useTheme, withStyles } from "@material-ui/core/styles";
import { colorStringToColorTheme } from "@/utils/utils";
import { BaseButtonInterface } from "@/interfaces/index";
import SvgIcon from "@/components/ui/SvgIcon";

export default function BaseButton({
  title,
  color = "primary",
  disabled = false,
  textColor,
  borderColor,
  backgroundColor,
  handleClick,
  startIcon,
  endIcon,
  iconColor,
  size,
  iconSize,
  variant,
  fullWidth = false,
  borderRadius = 4,
  ...props
}: BaseButtonInterface) {
  const theme = useTheme();
  const defaultColor = colorStringToColorTheme(color, theme);
  const transformIconColor = colorStringToColorTheme(iconColor, theme);
  const transformTextColor = colorStringToColorTheme(textColor, theme);

  const CustomButton = withStyles(() => ({
    root: {
      color: transformTextColor || defaultColor,
      backgroundColor: colorStringToColorTheme(backgroundColor, theme) || defaultColor,
      boxShadow: "none",
      textTransform: "none",
      borderColor: colorStringToColorTheme(borderColor, theme) || defaultColor,
      textAlign: "left",
      borderRadius,
      minWidth: "auto",
    },
  }))(Button);

  return (
    <CustomButton
      onClick={handleClick}
      disabled={disabled}
      fullWidth={fullWidth}
      startIcon={
        startIcon && (
          <SvgIcon
            icon={startIcon}
            fontSize={iconSize}
            htmlColor={transformIconColor || defaultColor}
          />
        )
      }
      endIcon={
        endIcon && (
          <SvgIcon icon={endIcon} fontSize={size} htmlColor={transformIconColor || defaultColor} />
        )
      }
      size={size}
      variant={variant}
      {...props}
    >
      {title}
    </CustomButton>
  );
}
