import React from "react";
import { BaseButtonInterface } from "@/interfaces/index";
import BaseButton from "./BaseButton";

type Props = Omit<BaseButtonInterface, "backgroundColor" | "borderColor" | "variant">;

export default function TextButton(props: Props) {
  return <BaseButton {...props} backgroundColor="none" borderColor="none" />;
}
