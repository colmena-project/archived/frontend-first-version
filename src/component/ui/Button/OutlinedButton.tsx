import React from "react";
import { BaseButtonInterface } from "@/interfaces/index";
import BaseButton from "./BaseButton";
import { ButtonVariantEnum } from "@/enums/*";

type Props = Omit<BaseButtonInterface, "backgroundColor" | "variant">;

export default function OutlinedButton(props: Props) {
  return <BaseButton {...props} backgroundColor="none" variant={ButtonVariantEnum.OUTLINED} />;
}
