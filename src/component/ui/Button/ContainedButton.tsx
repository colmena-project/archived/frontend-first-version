import React from "react";
import { BaseButtonInterface } from "@/interfaces/index";
import BaseButton from "./BaseButton";
import { getContrastColorTextByTheme } from "@/utils/utils";
import { useTheme } from "@material-ui/core";
import { ButtonVariantEnum } from "@/enums/*";

type Props = Omit<BaseButtonInterface, "variant">;

export default function ContainedButton({
  iconColor,
  textColor,
  color = "primary",
  ...props
}: Props) {
  const theme = useTheme();
  let transformIconColor = iconColor;
  let transformTextColor = textColor;
  if (color && color.indexOf("#") <= 0 && color !== "none") {
    if (!transformTextColor) {
      transformTextColor = getContrastColorTextByTheme(color, theme);
    }

    if (!transformIconColor) {
      transformIconColor = getContrastColorTextByTheme(color, theme);
    }
  }

  return (
    <BaseButton
      color={color}
      iconColor={transformIconColor}
      textColor={transformTextColor}
      {...props}
      variant={ButtonVariantEnum.CONTAINED}
    />
  );
}
