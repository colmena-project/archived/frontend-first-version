import { withStyles } from "@material-ui/core/styles";
import BaseButton from "@/components/ui/Button/BaseButton";

export const IconButtonBase = withStyles(() => ({
  root: {
    background: "none",
    border: 0,
    padding: 0,
    margin: 0,
    "& .MuiButton-startIcon": {
      margin: "0",
    },
  },
}))(BaseButton);
