import React from "react";
import { BaseButtonInterface } from "@/interfaces/index";
import { AllIconProps } from "@/types/*";
import { IconButtonBase } from "@/components/ui/Button/styled";

interface Props extends Omit<BaseButtonInterface, "variant" | "startIcon" | "title" | "endIcon"> {
  icon: AllIconProps;
}

export default function IconButton({
  icon,
  backgroundColor = "none",
  borderColor = "none",
  ...props
}: Props) {
  return (
    <IconButtonBase
      {...props}
      backgroundColor={backgroundColor}
      borderColor={borderColor}
      startIcon={icon}
    />
  );
}
