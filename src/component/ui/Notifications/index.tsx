import React, { useState } from "react";
import { Box, useTheme } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import SvgIcon from "@/components/ui/SvgIcon";
import Drawer from "@material-ui/core/Drawer";
import { NotificationItemMemoized } from "./NotificationItem";
import { EmptyBoxMemoized } from "./EmptyBox";
import { HeaderBoxMemoized } from "./HeaderBox";
import { getNotifications } from "@/services/notifications/notification";
import { NotificationInterface } from "@/interfaces/notification";
import NotificationsLoading from "../skeleton/Notifications";
import { PulseBox } from "./styled";
import NoConnectionMessage from "@/components/ui/Notifications/NoConnectionMessage";
import OfflineOnly from "@/components/ui/ConnectionStatus/OfflineOnly";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";

function Notifications() {
  const theme = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const onClose = () => setIsOpen(false);
  const { data, mutate, isValidating } = getNotifications({
    revalidateIfStale: false,
    revalidateOnFocus: true,
    revalidateOnReconnect: false,
  });

  const isEmpty = () => data?.ocs?.data.length === 0;

  const notifications = (
    <Box>
      <Drawer anchor="right" open={isOpen} onClose={onClose}>
        <OfflineOnly>
          <NoConnectionMessage />
        </OfflineOnly>
        <OnlineOnly>
          {isEmpty() && <EmptyBoxMemoized />}
          {!isEmpty() && (
            <>
              <HeaderBoxMemoized mutate={mutate} />
              {isValidating && <NotificationsLoading amount={5} />}

              {!isValidating &&
                data?.ocs?.data.map((notification: NotificationInterface) => (
                  <NotificationItemMemoized
                    notification={notification}
                    key={notification.notification_id}
                    mutate={mutate}
                    data={data}
                  />
                ))}
            </>
          )}
        </OnlineOnly>
      </Drawer>
    </Box>
  );

  return (
    <>
      <IconButton
        key="open-notifications"
        edge="start"
        color="inherit"
        aria-label="menu"
        data-testid="open-notifications"
        onClick={() => setIsOpen(true)}
        style={{ padding: "0" }}
      >
        {!isEmpty() && <PulseBox data-testid="pulse" />}
        <SvgIcon icon="notifications" htmlColor={theme.palette.primary.main} fontSize="medium" />
      </IconButton>
      {isOpen && (
        <Drawer anchor="right" open={isOpen} onClose={onClose} data-testid="notifications">
          {notifications}
        </Drawer>
      )}
    </>
  );
}

export default Notifications;

export const NotificationsMemoized = React.memo(Notifications);
