import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Wrapper = styled(Box)(({ theme }: StyledProps) => ({
  [theme.breakpoints.down("sm")]: {
    width: "80vw",
  },
  [theme.breakpoints.up("sm")]: {
    width: "40vw",
  },

  height: "100vh",

  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  textAlign: "center",
}));

export const Title = styled(Text)(({ theme }: StyledProps) => ({
  color: theme.palette.gray.main,
}));
