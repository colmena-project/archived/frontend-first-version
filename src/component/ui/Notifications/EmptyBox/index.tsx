import React from "react";
import SvgIcon from "@/components/ui/SvgIcon";
import { useTranslation } from "next-i18next";
import { Wrapper, Title } from "./styled";
import theme from "@/styles/theme";

function EmptyBox() {
  const { t } = useTranslation("drawer");
  return (
    <Wrapper data-testid="empty-box">
      <SvgIcon icon="notifications" htmlColor={theme.palette.gray.main} fontSize="medium" />
      <Title>{t("notification.emptyTitle")}</Title>
      <Title>{t("notification.emptyCaption")}</Title>
    </Wrapper>
  );
}

export default EmptyBox;

export const EmptyBoxMemoized = React.memo(EmptyBox);
