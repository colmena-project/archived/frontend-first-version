import { styled } from "@material-ui/core/styles";
import { Box, Button } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Container = styled(Box)(({ theme }: StyledProps) => ({
  [theme.breakpoints.down("sm")]: {
    width: "80vw",
  },
  [theme.breakpoints.up("sm")]: {
    width: "40vw",
  },
}));

export const Wrapper = styled(Box)({
  display: "flex",
  gap: 5,
  borderTop: "1px solid #EEEEEE",
  borderBottom: "1px solid #EEEEEE",
  padding: "10px 15px",
  "&:hover": {
    background: "#F6F9F9",
  },
});

export const Content = styled(Box)(({ theme }: StyledProps) => ({
  fontStyle: "normal",
  fontWeight: 400,
  fontSize: "10px",
  lineHeight: "14px",
  color: theme.palette.icon.main,
}));

export const Body = styled(Box)({
  display: "flex",
  justifyContent: "space-between",
});

export const CloseBox = styled(Box)({
  width: "35px",
  textAlign: "right",

  "& svg": {
    cursor: "pointer",
  },
});

export const FooterBox = styled(Box)({
  display: "flex",
  justifyContent: "space-between",
  marginTop: "10px",
});

export const Description = styled(Text)({
  marginTop: "-4px",
  lineHeight: "22px",
});

export const TimeInfo = styled(Text)(({ theme }: StyledProps) => ({
  fontStyle: "normal",
  fontWeight: 400,
  fontSize: "10px",
  lineHeight: "15px",
  display: "flex",
  alignItems: "center",
  textAlign: "center",
  color: theme.palette.gray.main,
}));

export const MyButton = styled(Button)(({ theme }: StyledProps) => ({
  fontStyle: "normal",
  fontWeight: 700,
  fontSize: "9px",
  lineHeight: "15px",
  color: theme.palette.icon.contrastText,
  background: "#C4C4C4",
  borderRadius: "20px",
}));
