import React from "react";
import SvgIcon from "@/components/ui/SvgIcon";
import { useTranslation } from "next-i18next";
import {
  FullNotificationQueryResponseInterface,
  NotificationInterface,
} from "@/interfaces/notification";
import { NotificationAppEnum } from "@/enums/*";
import { removeNotification } from "@/services/notifications/notification";
import { parseCookies } from "nookies";
import { getFormattedDistanceDateFromNow, toTimestamp } from "@/utils/utils";
import { getSingleConversationAxios } from "@/services/talk/room";
import { useRouter } from "next/router";
import { AllIconProps } from "@/types/*";
import { toast } from "@/utils/notifications";
import theme from "@/styles/theme";
import {
  Container,
  Wrapper,
  Content,
  Body,
  Description,
  CloseBox,
  FooterBox,
  TimeInfo,
  MyButton,
} from "./styled";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getHoneycombLink } from "@/utils/offlineNavigation";

interface NotificationItemInterface {
  notification: NotificationInterface;
  mutate: (data?: any, shouldRevalidate?: boolean | undefined) => Promise<any>;
  data: FullNotificationQueryResponseInterface;
}

type PropRenderIcon = {
  [key: string]: AllIconProps;
};

const renderIcon: PropRenderIcon = {
  [NotificationAppEnum.SPREED]: "chat",
  [NotificationAppEnum.FILES_SHARING]: "file",
};

function NotificationItem({ notification, mutate, data }: NotificationItemInterface) {
  const router = useRouter();
  const cookies = parseCookies();
  const lang = cookies.NEXT_LOCALE || "en";
  const { t } = useTranslation("drawer");
  const { t: c } = useTranslation("common");
  const connectionStatus = useConnectionStatus();

  const onDeleteNotification = async (updateList = true) => {
    try {
      await removeNotification(notification.notification_id);

      if (updateList) {
        const filtered = data.ocs.data.filter(
          (row: NotificationInterface) => row.notification_id !== notification.notification_id,
        );

        const updatedData = {
          ocs: {
            ...data.ocs,
            data: filtered,
          },
        };

        const shouldRevalidate = false;
        mutate(updatedData, shouldRevalidate);
      }
    } catch (e) {
      toast(c("genericErrorMessage"), "error");
    }
  };

  const getFormattedDistanceDate = getFormattedDistanceDateFromNow(
    toTimestamp(notification.datetime),
    lang,
  );

  const openLink = async () => {
    try {
      const data = await getSingleConversationAxios(notification.object_id);
      const conversation = data.data.ocs.data;

      const updateList = false;
      await onDeleteNotification(updateList);

      router.push(
        getHoneycombLink(
          notification.object_id,
          conversation.displayName,
          Number(conversation.canDeleteConversation),
          connectionStatus,
        ),
      );
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Container data-testid="notification-item">
      <Wrapper>
        <SvgIcon
          icon={renderIcon[notification.app]}
          htmlColor={theme.palette.primary.main}
          fontSize="medium"
        />
        <Content>
          <Body>
            <Description>{notification.subject}</Description>
            <CloseBox>
              {connectionStatus && (
                <SvgIcon
                  onClick={() => onDeleteNotification()}
                  icon="close"
                  htmlColor="#9A9A9A"
                  fontSize="inherit"
                />
              )}
            </CloseBox>
          </Body>
          <FooterBox>
            <TimeInfo>{getFormattedDistanceDate}</TimeInfo>
            {notification.app === NotificationAppEnum.SPREED && notification.object_id && (
              <MyButton type="button" onClick={() => openLink()}>
                {t("notification.action")}
              </MyButton>
            )}
          </FooterBox>
        </Content>
      </Wrapper>
    </Container>
  );
}

export default NotificationItem;

export const NotificationItemMemoized = React.memo(NotificationItem);
