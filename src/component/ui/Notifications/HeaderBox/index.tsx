import React from "react";
import { TextVariantEnum } from "@/enums/*";
import { removeAllNotifications } from "@/services/notifications/notification";
import { useTranslation } from "next-i18next";
import { toast } from "@/utils/notifications";
import { Header, Title, MarkAsReadTitle } from "./styled";

const responseFormat = "?format=json";

interface HeaderBoxInterface {
  mutate: (data?: any, shouldRevalidate?: boolean | undefined) => Promise<any>;
}

function HeaderBox({ mutate }: HeaderBoxInterface) {
  const { t } = useTranslation("drawer");
  const { t: c } = useTranslation("common");

  const markAsRead = async () => {
    try {
      await removeAllNotifications();
      mutate(`/notifications${responseFormat}`);
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "error");
    }
  };

  return (
    <Header data-testid="header-box">
      <Title variant={TextVariantEnum.H6}>{t("notification.title")}</Title>
      <MarkAsReadTitle onClick={() => markAsRead()}>{t("notification.markAsRead")}</MarkAsReadTitle>
    </Header>
  );
}

export default HeaderBox;

export const HeaderBoxMemoized = React.memo(HeaderBox);
