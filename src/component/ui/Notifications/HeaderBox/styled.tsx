import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Header = styled(Box)(({ theme }: StyledProps) => ({
  [theme.breakpoints.down("sm")]: {
    width: "80vw",
  },
  [theme.breakpoints.up("sm")]: {
    width: "40vw",
  },

  display: "flex",
  justifyContent: "space-between",
  padding: "20px",
}));

export const Title = styled(Text)(({ theme }: StyledProps) => ({
  flexGrow: 1,
  color: theme.palette.primary.main,
  fontSize: 18,
  fontWeight: "bold",
  textAlign: "left",
}));

export const MarkAsReadTitle = styled(Text)(({ theme }: StyledProps) => ({
  fonFamily: "Nunito Sans",
  fontStyle: "normal",
  fontWeight: 700,
  fontSize: "10px",
  lineHeight: "15px",
  display: "flex",
  alignItems: "center",
  textAlign: "right",
  color: theme.palette.gray.main,
  cursor: "pointer",
}));
