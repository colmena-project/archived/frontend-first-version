import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const PulseBox = styled(Box)(({ theme }: StyledProps) => ({
  background: theme.palette.secondary.main,
  border: `2px solid ${theme.palette.icon.contrastText}`,
  width: "10px",
  height: "10px",
  borderRadius: "50%",
  position: "absolute",
  top: 0,
  right: 0,
}));
