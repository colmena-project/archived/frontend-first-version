import { Box, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import SvgIcon from "@/components/ui/SvgIcon";
import Text from "../Text";
import { useTranslation } from "next-i18next";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    [theme.breakpoints.down("sm")]: {
      width: "80vw",
    },
    [theme.breakpoints.up("sm")]: {
      width: "40vw",
    },

    height: "100vh",

    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },

  text: {
    color: theme.palette.gray.main,
  },
}));

function NoConnectionMessage() {
  const { t } = useTranslation("drawer");
  const theme = useTheme();
  const classes = useStyles();

  return (
    <Box className={classes.wrapper} data-testid="empty-box">
      <SvgIcon icon="off_cloud" htmlColor={theme.palette.gray.main} fontSize="medium" />
      <Text className={classes.text}>
        {t("notification.itsNoPossibleToSeeNotificationOfflineMode")}
      </Text>
    </Box>
  );
}

export default NoConnectionMessage;
