import React from "react";
import Button from "@material-ui/core/Button";
import { withStyles, Theme } from "@material-ui/core/styles";
import { AllIconProps, ButtonSizeProps, ButtonVariantProps } from "@/types/index";
import { ButtonSizeEnum, ButtonVariantEnum } from "@/enums/index";
import SvgIcon from "@/components/ui/SvgIcon";
import theme from "@/styles/theme";

type Props = {
  title: string | React.ReactNode;
  startIcon?: AllIconProps | undefined;
  endIcon?: AllIconProps | undefined;
  disabled?: boolean;
  color?: string;
  backgroundColor?: string;
  iconColor?: string;
  size?: ButtonSizeProps | undefined;
  variant?: ButtonVariantProps | undefined;
  // eslint-disable-next-line @typescript-eslint/ban-types
  style?: object;
  handleClick: () => void;
  [x: string]: any;
};

export default function ButtonV2({
  title,
  startIcon = undefined,
  endIcon = undefined,
  disabled = false,
  color = theme.palette.gray.dark,
  backgroundColor = theme.palette.primary.main,
  size = ButtonSizeEnum.LARGE,
  variant = ButtonVariantEnum.CONTAINED,
  handleClick,
  ...props
}: Props) {
  const CustomButton = withStyles((theme: Theme) => ({
    root: {
      color: !color ? theme.palette.getContrastText(backgroundColor) : color,
      backgroundColor: variant === ButtonVariantEnum.TEXT ? "none" : backgroundColor,
      boxShadow: "none",
      textTransform: "none",
      borderColor: variant === ButtonVariantEnum.OUTLINED ? color : "none",
      textAlign: "left",
      margin: theme.spacing(0.5),
    },
  }))(Button);

  return (
    <CustomButton
      onClick={handleClick}
      disabled={disabled}
      startIcon={
        startIcon && (
          <SvgIcon
            icon={startIcon}
            fontSize={size}
            htmlColor={!color ? theme.palette.getContrastText(backgroundColor) : color}
          />
        )
      }
      endIcon={
        endIcon && (
          <SvgIcon
            icon={endIcon}
            fontSize={size}
            htmlColor={!color ? theme.palette.getContrastText(backgroundColor) : color}
          />
        )
      }
      size={size}
      fullWidth={false}
      variant={variant}
      {...props}
    >
      {title}
    </CustomButton>
  );
}
