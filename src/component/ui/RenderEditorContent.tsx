/* eslint-disable prettier/prettier */
import useDebounce from "@/hooks/useDebounce";

import { Editor, EditorContent, useEditor } from "@tiptap/react";
import React, { useEffect, useState } from "react";
import ToolbarTextEditor from "./ToolbarTextEditor";
import CONSTANTS from "@/constants/index";

import StarterKit from "@tiptap/starter-kit";
import BulletList from "@tiptap/extension-bullet-list";
import Document from "@tiptap/extension-document";
import ListItem from "@tiptap/extension-list-item";
import Paragraph from "@tiptap/extension-paragraph";
import Text from "@tiptap/extension-text";
import Highlight from "@tiptap/extension-highlight";
import TextStyle from "@tiptap/extension-text-style";
import TextAlign from "@tiptap/extension-text-align";
import Link from "@tiptap/extension-link";
import Blockquote from "@tiptap/extension-blockquote";
import Image from "@tiptap/extension-image";

import { Color } from "@tiptap/extension-color";
import { WrapperEditorContent } from "./ToolbarTextEditor/styled";

interface RenderEditorContentProps {
  content: string;
  save: (editorHtmlContent: string) => void;
}

export default function RenderEditorContent({ content, save }: RenderEditorContentProps) {
  const [editorHtmlContent, setEditorHtmlContent] = useState(content.trim());
  const debouncedValue = useDebounce<string>(
    editorHtmlContent,
    CONSTANTS.DEBOUNCE_TIME_FILE_AUTO_SAVE,
  );

  useEffect(() => {
    save(editorHtmlContent);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedValue]);

  const editor = useEditor({
    extensions: [
      TextAlign.configure({
        types: ["heading", "paragraph"],
      }),
      Link.configure({
        validate: (href) => /^https?:\/\//.test(href),
        linkOnPaste: false,
        autolink: false,
        openOnClick: true,
        protocols: ["ftp", "mailto"],
      }),
      Image.configure({
        inline: true,
        allowBase64: true,
        HTMLAttributes: {
          class: "my-custom-class-image",
        },
      }),
      StarterKit,
      Document,
      Paragraph,
      Text,
      TextStyle,
      Color.configure({
        types: ["textStyle"],
      }),
      BulletList,
      ListItem,
      Highlight.configure({ multicolor: true }),
      Blockquote.configure({
        HTMLAttributes: {
          class: "custom-blockquote",
        },
      }),
    ],
    autofocus: "start",
    onUpdate: ({ editor }) => {
      setEditorHtmlContent(editor.getHTML());
    },
    content,
  }) as Editor;

  return (
    <>
      <ToolbarTextEditor editor={editor} />

      <WrapperEditorContent>
        <EditorContent editor={editor} />
      </WrapperEditorContent>
    </>
  );
}
