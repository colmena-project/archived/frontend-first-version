import Text from "@/components/ui/Text";
import { TextVariantEnum, TextAlignEnum } from "@/enums/index";

type Props = {
  dataTutTitle?: string;
  textColor: string;
  fontSizeTitle?: number;
  title: string;
  [x: string]: any;
};

export default function MainTitle({
  dataTutTitle,
  textColor,
  fontSizeTitle = 20,
  title,
  ...props
}: Props) {
  return (
    <Text
      variant={TextVariantEnum.H1}
      align={TextAlignEnum.LEFT}
      {...props}
      style={{
        fontSize: fontSizeTitle,
        color: textColor,
        fontWeight: 900,
        maxWidth: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
      }}
    >
      <span data-tut={dataTutTitle}>{title}</span>
    </Text>
  );
}
