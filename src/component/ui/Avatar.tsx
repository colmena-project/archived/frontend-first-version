import React from "react";
import { Avatar } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { getFirstLettersOfTwoFirstNames } from "@/utils/utils";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/index";

type Props = {
  size: number;
  image?: string | undefined | null;
  name?: string | undefined;
  borderRadius?: string | undefined;
  variant?: "square" | "rounded" | "circular";
};

function AvatarAux({ size, name = "", borderRadius, image, variant = "rounded" }: Props) {
  const useStyles = makeStyles((theme) => ({
    size: {
      width: theme.spacing(size),
      height: theme.spacing(size),
      borderRadius,
    },
    firstNames: {
      color: theme.palette.variation5.dark,
    },
  }));
  const classes = useStyles();

  if (!image || !navigator.onLine) {
    return (
      <Avatar variant={variant} className={classes.size}>
        <Text variant={TextVariantEnum.CAPTION} className={classes.firstNames}>
          {getFirstLettersOfTwoFirstNames(name)}
        </Text>
      </Avatar>
    );
  }

  return (
    <Avatar alt={`Avatar ${name}`} src={image} className={classes.size} variant={variant}>
      {getFirstLettersOfTwoFirstNames(name)}
    </Avatar>
  );
}

export default AvatarAux;
