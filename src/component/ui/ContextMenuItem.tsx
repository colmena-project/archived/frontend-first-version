import { AllIconProps } from "@/types/*";
import Text from "@/components/ui/Text";
import SvgIcon from "@/components/ui/SvgIcon";
import Box from "@material-ui/core/Box";
import { TextVariantEnum } from "@/enums/*";
import theme from "@/styles/theme";
import { styled } from "@material-ui/core/styles";

type Props = {
  title: string;
  icon?: AllIconProps | undefined;
  iconColor?: string;
  danger?: boolean;
  dataTut?: string;
  [x: string]: any;
};

export const Base = styled(Box)({
  display: "flex",
  flex: 1,
  flexDirection: "row",
  alignContent: "center",
  alignItems: "center",
  justifyContent: "flex-start",
});

export const ItemTitle = styled(Text)({
  marginLeft: 8,
});

export default function ContextMenuItem({
  title,
  icon,
  iconColor = theme.palette.variation6.main,
  danger = false,
  dataTut,
  ...props
}: Props) {
  return (
    <Base data-tut={dataTut}>
      {icon && (
        <SvgIcon
          icon={icon}
          htmlColor={!danger ? iconColor : theme.palette.danger.light}
          fontSize="small"
          {...props}
        />
      )}
      <ItemTitle
        style={{ color: !danger ? iconColor : theme.palette.danger.light }}
        variant={TextVariantEnum.SUBTITLE2}
      >
        {title}
      </ItemTitle>
    </Base>
  );
}
