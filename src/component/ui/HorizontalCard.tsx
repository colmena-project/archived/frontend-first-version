import React, { ReactElement, ReactNode } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import { Box, makeStyles } from "@material-ui/core";
import { TextVariantEnum } from "@/enums/*";
import Text from "./Text";
import classNames from "classnames";

const useStyles = makeStyles((theme) => ({
  card: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "nowrap",
    width: "100%",
    background: "#fff",
    padding: 8,
    borderRadius: 5,
    border: "1px solid",
    borderColor: theme.palette.grey[200],
    boxShadow: "none",
    "& .MuiCardContent-root:last-child": {
      padding: 0,
    },
  },
  description: {
    flexDirection: "column",
    flexGrow: 1,
    marginLeft: 5,
    padding: 0,
    alignSelf: "stretch",
    display: "flex",
    justifyContent: "center",
  },
  options: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
  },
  avatar: {
    minHeight: 50,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  },
  withEllipsis: {
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
  },
}));

type Props = {
  avatar?: ReactElement | ReactNode;
  primary?: ReactElement | ReactNode;
  secondary?: ReactElement | ReactNode;
  options?: ReactElement | ReactNode;
  handleClick?: (event: any) => any | undefined;
  noBorder?: boolean;
  noWrap?: boolean;
};

export default function HorizontalCard({
  avatar,
  primary,
  secondary,
  options,
  handleClick,
  noBorder,
  noWrap = true,
}: Props) {
  const classes = useStyles();
  let cardPrimary = primary;
  let cardSecondary = secondary;
  if (typeof primary === "string") {
    cardPrimary = <Text variant={TextVariantEnum.BODY1}>{primary}</Text>;
  }

  if (typeof secondary === "string") {
    cardSecondary = <Text variant={TextVariantEnum.CAPTION}>{secondary}</Text>;
  }

  return (
    <Card className={classes.card} style={{ border: noBorder ? 0 : undefined }}>
      <Box className={classes.avatar}>{avatar}</Box>
      <CardContent
        onClick={handleClick}
        className={classNames(classes.description, noWrap ? classes.withEllipsis : undefined)}
      >
        {primary && <Box>{cardPrimary}</Box>}
        {secondary && <Box>{cardSecondary}</Box>}
      </CardContent>
      {options && <CardActions className={classes.options}>{options}</CardActions>}
    </Card>
  );
}
