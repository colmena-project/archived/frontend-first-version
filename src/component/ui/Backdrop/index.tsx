/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { MyBackdrop } from "./styled";

type Props = {
  open: boolean;
  className?: string;
};

export default function SimpleBackdrop({ open, className = "" }: Props) {
  return (
    <MyBackdrop open={open}>
      <CircularProgress color="inherit" />
    </MyBackdrop>
  );
}
