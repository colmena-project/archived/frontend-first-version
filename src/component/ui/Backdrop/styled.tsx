import Backdrop from "@material-ui/core/Backdrop";
import { styled } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const MyBackdrop = styled(Backdrop)(({ theme }: StyledProps) => ({
  zIndex: theme.zIndex.drawer + theme.zIndex.appBar + 1,
  color: "#fff",
}));
