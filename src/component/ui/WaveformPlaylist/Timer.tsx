/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { useStopwatch } from "react-timer-hook";
import { makeStyles } from "@material-ui/core/styles";
import { TextVariantEnum } from "@/enums/*";
import Text from "@/components/ui/Text";

function format2digits(value: number) {
  return value < 10 ? `0${value.toString()}` : value;
}

const useStyles = makeStyles(() => ({
  timerTitle: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 15,
  },
}));

export default function Timer() {
  const { seconds, minutes } = useStopwatch({
    autoStart: true,
  });
  const classes = useStyles();

  return (
    <Text variant={TextVariantEnum.H4} data-testid="timer-text" className={classes.timerTitle}>
      <span>{format2digits(minutes)}</span>:<span>{format2digits(seconds)}</span>
    </Text>
  );
}
