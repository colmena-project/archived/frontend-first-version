import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import { AudioStateProps } from "@/types/*";
import { makeStyles } from "@material-ui/core/styles";
import SvgIcon from "@/components/ui/SvgIcon";
import { useTranslation } from "next-i18next";

type Props = {
  path: string;
  audioState: AudioStateProps;
};

const useStyles = makeStyles(() => ({
  root: {
    margin: 20,
  },
  container: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  mainTitle: {
    color: "#fff",
    fontSize: 18,
  },
  secondaryTitle: {
    color: "#A5ACC6",
    fontSize: 14,
  },
}));

export default function DisplayInformationStopped({ path, audioState }: Props) {
  const { t } = useTranslation("recording");
  const classes = useStyles();

  if (!["PLAYING", "STOPPED", "PAUSED"].includes(audioState))
    return <span data-testid="empty"></span>;

  return (
    <Box className={classes.root}>
      <Box className={classes.container}>
        <SvgIcon icon="tick" fontSize="large" htmlColor="#fff" />
        <Text variant={TextVariantEnum.CAPTION} className={classes.mainTitle}>
          {t("audioSavedSuccessfullyLocally")}
        </Text>
      </Box>
      <Box className={classes.container}>
        <Text variant={TextVariantEnum.CAPTION} className={classes.secondaryTitle}>
          {path}
        </Text>
      </Box>
    </Box>
  );
}
