/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useState, useEffect, useMemo } from "react";
import theme from "@/styles/theme";
import Backdrop from "@/components/ui/Backdrop";
import EventEmitter from "events";
import {
  createBlobFromAudioBuffer,
  setMicrophonePermission,
  createAudioRecorderIDB,
  getFilenameRecorder,
  shortStringByNumber,
} from "@/utils/utils";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import WaveformPlaylist from "colmena-waveform-playlist";
import {
  getTextInputValue,
  setTextInputValue,
  SAVE_AUDIO_FLAG,
  PLAYBACK_POSITION,
} from "@/utils/waveformUtils";
import { AudioControlsMemoized } from "@/components/ui/WaveformPlaylist/AudioControls";
import { removeExtension, splitFilename } from "@/utils/directory";
import { BoxContainerNode } from "./styled";
import { initAudioExporter } from "@/utils/audioExporter";
import { saveAs } from "file-saver";
import { AudioExportTypesEnum, AudioGlobalConfigEnum } from "@/enums/*";

type Props = {
  urlBlob?: string;
  filename?: string;
  ee: EventEmitter;
  context: "recorder" | "editing";
  setCurrentPath?: (currentPath: string) => void;
};

const Waveform = ({
  urlBlob = "",
  filename = "",
  ee,
  context = "recorder",
  setCurrentPath,
}: Props) => {
  const zoomLevels = [500, 1000, 3000, 5000];
  const fatorRewindForward = 10;
  const [showControls, setShowControls] = useState(true);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  let playlist: any;
  let userMediaStream: MediaStream;
  const showAudioPanelTrack = true;

  useEffect(() => {
    setTextInputValue(SAVE_AUDIO_FLAG, "pending");
  }, []);

  async function gotStream(stream: MediaStream) {
    userMediaStream = stream;
    playlist.initRecorder(userMediaStream);
    setShowControls(true);
    await setMicrophonePermission("yes");
  }

  async function logError(err: string) {
    await setMicrophonePermission("no");
    setShowControls(true);
  }

  const container = useCallback(
    (node) => {
      window.document.getElementById("waveform")?.firstChild?.remove();
      if (node !== null) {
        const constraints = {
          audio: {
            channelCount: 1,
          },
        };

        if (navigator.mediaDevices) {
          navigator.mediaDevices.getUserMedia(constraints).then(gotStream).catch(logError);
        }

        playlist = WaveformPlaylist(
          {
            samplesPerPixel: 500,
            mono: true,
            waveHeight: 135,
            sampleRate: AudioGlobalConfigEnum.SAMPLE_RATE,
            container: node,
            timescale: true,
            state: "cursor",
            barWidth: 4,
            isAutomaticScroll: true,
            barGap: 2,
            innerWidth: 200,
            colors: {
              waveOutlineColor: theme.palette.primary.main,
              timeColor: theme.palette.primary.light,
              fadeColor: "black",
            },
            controls: {
              show: showAudioPanelTrack,
              width: 150,
              widgets: {
                muterOrSolo: true,
                volume: true,
                collapse: true,
                remove: true,
                stereoPan: true,
              },
            },
            zoomLevels,
          },
          ee,
        );

        ee.on("timeupdate", (playbackPosition: number) => {
          setTextInputValue(PLAYBACK_POSITION, playbackPosition);
        });

        ee.on("audiosourcesloaded", () => {
          setShowControls(true);
          // customizeAudioPanel();
        });

        ee.on("audiorenderingfinished", async (type, data) => {
          if (type === "buffer") {
            const blob = createBlobFromAudioBuffer(data, data.length);
            const filename = await createAudioRecorderIDB(blob, userRdx.user.id);
            if (setCurrentPath) setCurrentPath(filename);
          } else if (type === AudioExportTypesEnum.WAV) {
            const filenameRecorder = await getFilenameRecorder();
            const fln = !filename
              ? splitFilename(removeExtension(filenameRecorder))
              : splitFilename(removeExtension(filename));
            saveAs(data, `${fln}.${type}`);
          } else if (
            [
              AudioExportTypesEnum.MP3,
              AudioExportTypesEnum.OPUS,
              AudioExportTypesEnum.AAC,
            ].includes(type)
          ) {
            if (context === "editing")
              await initAudioExporter(data, splitFilename(removeExtension(filename)), type);
            else {
              const filename = await getFilenameRecorder();
              await initAudioExporter(data, splitFilename(removeExtension(filename)), type);
            }
          }
        });

        if (urlBlob && filename) {
          playlist.load([
            {
              src: urlBlob,
              name: shortStringByNumber(filename.split("/").reverse()[0], 12),
              waveOutlineColor: theme.palette.primary.main,
              muted: false,
            },
          ]);
        }

        // initialize the WAV exporter.
        playlist.initExporter();
      }
    },
    [ee, showAudioPanelTrack],
  );

  const waveform = useMemo(
    () => <div id="waveform" data-testid="waveform-container" ref={container}></div>,
    [container],
  );

  const handleForward = () => {
    ee.emit("play", Number(getTextInputValue(PLAYBACK_POSITION)) + fatorRewindForward);
  };

  const handleRewind = () => {
    const playbackPosition = Number(getTextInputValue(PLAYBACK_POSITION));
    if (playbackPosition > 0 && playbackPosition > fatorRewindForward)
      ee.emit("play", playbackPosition - fatorRewindForward);
    else if (playbackPosition > 0 && playbackPosition < fatorRewindForward) ee.emit("play", 0);
  };

  return (
    <>
      <input type="hidden" id={PLAYBACK_POSITION} />
      <Backdrop open={!showControls} />
      <BoxContainerNode>{waveform}</BoxContainerNode>
      {showControls && (
        <AudioControlsMemoized
          ee={ee}
          context={context}
          handleRewind={handleRewind}
          handleForward={handleForward}
        />
      )}
    </>
  );
};

const Playlist = (props: Props) => <Waveform {...props}></Waveform>;

export const PlaylistMemoized = React.memo(Playlist);

export default Playlist;
