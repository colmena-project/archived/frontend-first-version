import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

export const BoxContainerNode = styled(Box)({
  paddingLeft: 20,
  paddingRight: 20,
  width: "100vw",
});
