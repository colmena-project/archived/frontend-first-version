import React, { useMemo, useState } from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import ButtonV2 from "@/components/ui/ButtonV2";
import theme from "@/styles/theme";
import { ButtonSizeEnum, ButtonVariantEnum, AudioExportTypesEnum } from "@/enums/index";
import { PropsUserSelector, PropsAudioSave, PropsRecordingSelector } from "@/types/index";
import EventEmitter from "events";
import { useDispatch, useSelector } from "react-redux";
import { updateRecordingState, setRecordingCounter } from "@/store/actions/recordings/index";
import { remove as removeLocalFile, findByFilename } from "@/store/idb/models/files";

import { toast } from "@/utils/notifications";
import { useTranslation } from "next-i18next";
import DialogExtraInfoAudio from "@/components/ui/WaveformPlaylist/DialogExtraInfoAudio";
import {
  convertUsernameToPrivate,
  getFilename,
  isHoneycombUrl,
  isLibraryUrl,
} from "@/utils/directory";
import { removeSpecialCharacters, getAccessedPages, awaiting } from "@/utils/utils";
import { existFile, deleteFile } from "@/services/webdav/files";
import { parseCookies } from "nookies";

import Backdrop from "@/components/ui/Backdrop";
import { useRouter } from "next/router";
import ActionConfirm from "../ActionConfirm";
import { saveFile } from "@/utils/recorder";
import classNames from "classnames";

type Props = {
  ee: EventEmitter;
  path: string;
  setSaved: (bool: boolean) => void;
  currentPath: string;
  filename: string;
  setCurrentPath: (currentPath: string) => void;
};

const useStyles = makeStyles(() => ({
  root: {
    margin: 10,
    "& div": {
      marginBottom: 5,
    },
  },
  container1: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
  },
  container2: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    width: 180,
  },
  whiteButton: {
    backgroundColor: "#fff",
  },
}));

export default function OptionsDone({
  ee,
  path,
  setSaved,
  currentPath,
  filename,
  setCurrentPath,
}: Props) {
  const fileTitle = useMemo(() => getFilename(filename), [filename]);

  const dispatch = useDispatch();
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { t } = useTranslation("recording");
  const { t: c } = useTranslation("common");
  const { t: e } = useTranslation("editAudio");
  const [openSaveAudio, setOpenSaveAudio] = useState(false);
  const [showBackdrop, setShowBackdrop] = useState(false);
  const [rendeModal, setRendeModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const cookies = parseCookies();
  const language = cookies.NEXT_LOCALE || "en";
  const defaultAudioType = AudioExportTypesEnum.WAV;
  const userId = userRdx.user.id;
  const router = useRouter();

  const recordingRdx = useSelector(
    (state: { recording: PropsRecordingSelector }) => state.recording,
  );
  const audioState = recordingRdx.activeRecordingState;

  const handleNewRecording = () => {
    ee.emit("clear");
    dispatch(updateRecordingState("NONE"));
    dispatch(setRecordingCounter(0));
  };

  const handleDiscardAudio = async () => {
    try {
      setIsLoading(true);
      const localFile = await findByFilename(userId, currentPath);
      if (localFile?.id) await removeLocalFile(localFile.id, userRdx.user.id);
      const exists = await existFile(userRdx.user.id, currentPath);
      if (exists) {
        await deleteFile(userRdx.user.id, currentPath);
      }
      toast(t("audioDiscardedSuccessfully"), "success");
      ee.emit("clear");
      dispatch(updateRecordingState("NONE"));
      dispatch(setRecordingCounter(0));

      setCurrentPath("");
      setSaved(false);
    } catch (e) {
      toast(c("genericErrorMessage "), "success");
    } finally {
      onCloseModal();
    }
  };

  const onCloseModal = () => {
    setIsLoading(false);
    setRendeModal(false);
  };

  const handleDeleteConfirm = () => {
    setRendeModal(true);
  };

  const handleCloseModalSaveAudio = () => {
    setOpenSaveAudio(false);
  };

  const handleSave = async (values: PropsAudioSave) => {
    try {
      const lastPages = await getAccessedPages();
      setSaved(true);
      const { path } = values;
      const pathUsrToPvt = convertUsernameToPrivate(path, userId);
      const currentFilename = `${pathUsrToPvt}/${removeSpecialCharacters(
        fileTitle,
      )}.${defaultAudioType}`;
      const audioLocalFile = await findByFilename(userId, currentFilename);
      const messages = {
        genericError: c("genericErrorMessage"),
        audioSavedSuccessfully: t("audioSavedSuccessfully"),
        storageError: t("errorStorageMessage"),
      };

      setShowBackdrop(true);
      if (audioLocalFile) {
        await saveFile(values, userId, fileTitle, language, messages, setCurrentPath);

        if (isHoneycombUrl(lastPages[1]) || isLibraryUrl(lastPages[1])) {
          await router.push(lastPages[1]);
          return;
        }
      } else {
        handleSaveLocally();
        await awaiting(1000);
        await saveFile(values, userId, fileTitle, language, messages, setCurrentPath);
      }
    } catch (e) {
      console.log(e);
    } finally {
      setShowBackdrop(false);
      setOpenSaveAudio(false);
    }
  };

  const handleSaveLocally = () => {
    ee.emit("startaudiorendering", "buffer");
  };

  const openModalSaveAudio = () => setOpenSaveAudio(true);

  if (!["PLAYING", "STOPPED", "PAUSED"].includes(audioState)) return null;

  return (
    <>
      <Backdrop open={showBackdrop} />
      {openSaveAudio && (
        <DialogExtraInfoAudio
          open={openSaveAudio}
          handleClose={handleCloseModalSaveAudio}
          handleSubmit={handleSave}
          pathLocationSave={path}
          handleBack={() => setOpenSaveAudio(false)}
          tempFileName={fileTitle}
        />
      )}
      <Box className={classes.root}>
        <Box className={classes.container1}>
          <ButtonV2
            title={e("contextOptions.saveTitle")}
            color={theme.palette.primary.dark}
            className={classNames(classes.button, classes.whiteButton)}
            size={ButtonSizeEnum.LARGE}
            startIcon="save"
            variant={ButtonVariantEnum.TEXT}
            handleClick={openModalSaveAudio}
            data-testid="save"
          />
        </Box>
        <Box className={classes.container2}>
          <ButtonV2
            title={t("newRecorderTitle")}
            startIcon="plus"
            variant={ButtonVariantEnum.TEXT}
            color="#fff"
            className={classes.button}
            size={ButtonSizeEnum.LARGE}
            handleClick={handleNewRecording}
            data-testid="new-recorder-button"
          />
          <ButtonV2
            title={t("discardButton")}
            startIcon="trash"
            variant={ButtonVariantEnum.TEXT}
            color={theme.palette.variation3.light}
            className={classes.button}
            handleClick={handleDeleteConfirm}
            data-testid="discard-recorder-button"
          />
        </Box>
        {rendeModal && (
          <ActionConfirm
            data-testid="delete-item-confirm"
            onOk={handleDiscardAudio}
            onClose={onCloseModal}
            isLoading={isLoading}
            title={t("deleteAudio")}
          />
        )}
      </Box>
    </>
  );
}
