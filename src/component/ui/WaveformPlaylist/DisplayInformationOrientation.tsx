import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { DirectoryNamesNCEnum, TextVariantEnum } from "@/enums/*";
import { AudioStateProps } from "@/types/*";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "next-i18next";
import ChangeFolderAudioSaved from "@/components/ui/WaveformPlaylist/ChangeFolderAudioSaved";
import RotateYourDevice from "@/components/pages/edit-audio/RotateYourDevice";

type Props = {
  path: string;
  audioState: AudioStateProps;
};

const useStyles = makeStyles(() => ({
  root: {
    top: 72,
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  information: {
    color: "#A5ACC6",
    fontSize: 14,
    width: "100%",
    textAlign: "center",
    marginTop: 15,
  },
}));

export default function DisplayInformationOrientation({ path, audioState }: Props) {
  const classes = useStyles();
  const { t: r } = useTranslation("recording");
  const { t } = useTranslation("editAudio");
  const { t: l } = useTranslation("library");

  if (!["NONE", "RECORDING", "STOPPED"].includes(audioState)) return null;

  return (
    <Box className={classes.root}>
      {audioState === "NONE" && (
        <Box>
          <ChangeFolderAudioSaved />
          <Text
            variant={TextVariantEnum.CAPTION}
            data-testid="recording-orientation"
            className={classes.information}
          >
            {r("recordingOrientation")}
          </Text>
        </Box>
      )}
      {audioState === "RECORDING" && (
        <Text
          variant={TextVariantEnum.CAPTION}
          data-testid="upload-location-title"
          className={classes.information}
        >
          {`${r("uploadLocationTitle")}: /${path.replace(
            DirectoryNamesNCEnum.TALK,
            l("talkFolderName"),
          )}`}
        </Text>
      )}
      {audioState === "STOPPED" && <RotateYourDevice title={t("rotateYourDeviceTitle")} />}
    </Box>
  );
}
