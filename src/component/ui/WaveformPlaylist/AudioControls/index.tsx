/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import theme from "@/styles/theme";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import EventEmitter from "events";
import IconButton from "@/components/ui/IconButton";
import { useTranslation } from "next-i18next";
import Timer from "../Timer";
import { AudioStateProps, PropsRecordingSelector } from "@/types/*";
import { useDispatch, useSelector } from "react-redux";
import { updateRecordingState, setRecordingCounter } from "@/store/actions/recordings/index";
import {
  getMicrophonePermission,
  getIDBEnable,
  getDefaultAudioName,
  setFilenameRecorder,
} from "@/utils/utils";
import { toast } from "@/utils/notifications";

import Slider from "@material-ui/core/Slider";
import VolumeDown from "@material-ui/icons/VolumeDown";
import VolumeUp from "@material-ui/icons/VolumeUp";
import { useWaveformEmitter } from "@/hooks/useWaveformEmitter";
import { useVerifyScreenOrientation } from "@/hooks/useVerifyScreenOrientation";

import {
  Root,
  ControlContainer,
  DisplayTime,
  ListControl,
  MarginButtons,
  MyDivider,
  BoxTime,
  MasterControl,
  MySliderBox,
} from "./styled";

type Props = {
  ee: EventEmitter;
  context: "recorder" | "editing";
  handleForward: () => void;
  handleRewind: () => void;
};

export default function AudioControls({ ee, context, handleForward, handleRewind }: Props) {
  const { isPortraitView } = useVerifyScreenOrientation();
  const [value, setValue] = useState(100);
  const { t } = useTranslation("recording");
  const { t: c } = useTranslation("common");

  const {
    emitterRecord,
    emitterZoomIn,
    emitterZoomOut,
    emitterMasterVolumeChange,
    emitterStop,
    emitterPlay,
    emitterPause,
  } = useWaveformEmitter({ ee });

  const recordingRdx = useSelector(
    (state: { recording: PropsRecordingSelector }) => state.recording,
  );

  const audioState = recordingRdx.activeRecordingState || "NONE";
  const recordingCounter = recordingRdx.recordingCounter || 0;

  const dispatch = useDispatch();
  const [audioStateLocal, setAudioStateLocal] = useState<AudioStateProps>(audioState);

  const iconRecordButtonStyle = { fontSize: 50 };
  const iconButtonStyle = {
    fontSize: 24,
    color: "#fff",
  };
  const textStyle = {
    color: "#fff",
    fontSize: 14,
  };

  const handleRecording = async () => {
    const micPer = await getMicrophonePermission();
    const idbEnable = await getIDBEnable();

    if (micPer === "no") {
      toast(t("micPermissionTitle"), "warning");
      return;
    }

    if (idbEnable === "no") {
      toast(t("idbEnableTitle"), "warning");
      return;
    }

    const totalRecording = recordingCounter + 1;

    dispatch(updateRecordingState("RECORDING"));
    dispatch(setRecordingCounter(totalRecording));

    if (context === "recorder" && totalRecording === 1) {
      await setFilenameRecorder(getDefaultAudioName());
    }

    emitterRecord();
  };

  const handlePlayPause = () => {
    if (["PAUSED", "STOPPED"].includes(audioStateLocal)) {
      setAudioStateLocal("PLAYING");
      emitterPlay();
    } else {
      setAudioStateLocal("PAUSED");
      emitterPause();
    }
  };

  const handleStop = () => {
    dispatch(updateRecordingState("STOPPED"));
    setAudioStateLocal("STOPPED");

    emitterStop();
  };

  const handleChangeMasterVolume = (newValue: number | number[]) => {
    setValue(newValue as number);
    emitterMasterVolumeChange(newValue);
  };

  const handleUndo = () => {
    toast(c("featureUnavailable"), "warning");
  };
  const handleRedo = () => {
    toast(c("featureUnavailable"), "warning");
  };

  const bottonsInHorizontalView = (
    <ControlContainer justifyContent="space-between">
      <DisplayTime>
        <MasterControl>
          <MySliderBox>
            <VolumeDown />
            <Slider
              defaultValue={50}
              valueLabelDisplay="auto"
              value={value}
              onChange={(_, newValue) => handleChangeMasterVolume(newValue)}
            />
            <VolumeUp />
          </MySliderBox>
        </MasterControl>
        <Box className="pr-4">
          <BoxTime id="playback-position-formatted">00:00:00</BoxTime>
        </Box>
      </DisplayTime>
      <ListControl>
        {/* <Box className={classes.marginButtons}>
          <IconButton
            disabled={audioState === "RECORDING"}
            size="small"
            icon="undo"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleUndo}
            textStyle={textStyle}
            data-testid="rewind-button"
          />
          <IconButton
            disabled={audioState === "RECORDING"}
            icon="redo"
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleRedo}
            textStyle={textStyle}
            data-testid="rewind-button"
          />
        </Box> */}
        {/* <Divider orientation="vertical" light flexItem className={classes.dividerColor} /> */}
        <MarginButtons>
          <IconButton
            disabled={audioState === "RECORDING"}
            icon="zoomout"
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={emitterZoomOut}
            textStyle={textStyle}
            data-testid="rewind-button"
          />
          <IconButton
            disabled={audioState === "RECORDING"}
            icon="zoomin"
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={emitterZoomIn}
            textStyle={textStyle}
            data-testid="rewind-button"
          />
        </MarginButtons>

        <MyDivider orientation="vertical" light flexItem />
        <MarginButtons>
          <IconButton
            disabled={audioState === "RECORDING"}
            icon="editor_rewind"
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleRewind}
            textStyle={textStyle}
            data-testid="rewind-button"
          />
          <IconButton
            disabled={audioState === "RECORDING"}
            icon={["PAUSED", "STOPPED"].includes(audioStateLocal) ? "editor_play" : "editor_pause"}
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handlePlayPause}
            textStyle={textStyle}
            data-testid={
              ["PAUSED", "STOPPED"].includes(audioStateLocal) ? "play-button" : "pause-button"
            }
          />
          <IconButton
            disabled={audioState === "RECORDING"}
            icon="record_outlined"
            size="small"
            iconColor={theme.palette.danger.light}
            iconStyle={iconButtonStyle}
            handleClick={handleRecording}
            textStyle={textStyle}
            data-testid="record-button"
          />
          <IconButton
            icon="stop_outlined"
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleStop}
            data-testid="stop-button"
          />
          <IconButton
            disabled={audioState === "RECORDING"}
            icon="editor_forward"
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleForward}
            data-testid="forward-button"
          />
        </MarginButtons>
      </ListControl>
    </ControlContainer>
  );

  const buttonPortraitView = (
    <ControlContainer justifyContent="center">
      {audioState !== "RECORDING" && (
        <>
          <IconButton
            icon="editor_rewind"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleRewind}
            textStyle={textStyle}
            data-testid="rewind-button"
          />
          <IconButton
            icon={["PAUSED", "STOPPED"].includes(audioStateLocal) ? "editor_play" : "editor_pause"}
            size="small"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handlePlayPause}
            textStyle={textStyle}
            data-testid={
              ["PAUSED", "STOPPED"].includes(audioStateLocal) ? "play-button" : "pause-button"
            }
          />
        </>
      )}
      <IconButton
        icon={audioState === "RECORDING" ? "stop_outlined" : "record_outlined"}
        size="small"
        iconColor={audioState === "RECORDING" ? iconButtonStyle.color : theme.palette.danger.light}
        iconStyle={iconRecordButtonStyle}
        handleClick={audioState === "RECORDING" ? handleStop : handleRecording}
        textStyle={textStyle}
        data-testid="record-button"
      />
      {audioState !== "RECORDING" && (
        <>
          <IconButton
            icon="stop_outlined"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleStop}
            textStyle={textStyle}
            data-testid="stop-button"
          />
          <IconButton
            icon="editor_forward"
            iconColor={iconButtonStyle.color}
            iconStyle={iconButtonStyle}
            handleClick={handleForward}
            textStyle={textStyle}
            data-testid="forward-button"
          />
        </>
      )}
    </ControlContainer>
  );

  useEffect(() => {
    setAudioStateLocal("PAUSED");

    return () => {
      setAudioStateLocal("NONE");
    };
  }, []);

  if (context === "recorder") {
    return (
      <Root>
        {audioState === "RECORDING" && <Timer />}
        {recordingCounter === 0 && (
          <>
            <ControlContainer justifyContent="center">
              {!["NONE", "RECORDING"].includes(audioState) && (
                <IconButton
                  icon="editor_rewind"
                  iconColor={iconButtonStyle.color}
                  iconStyle={iconButtonStyle}
                  handleClick={handleRewind}
                  textStyle={textStyle}
                  data-testid="rewind-button"
                />
              )}
              {!["NONE", "RECORDING"].includes(audioState) && (
                <IconButton
                  icon={
                    ["PAUSED", "STOPPED"].includes(audioStateLocal) ? "editor_play" : "editor_pause"
                  }
                  iconColor={iconButtonStyle.color}
                  iconStyle={iconButtonStyle}
                  handleClick={handlePlayPause}
                  textStyle={textStyle}
                  data-testid={
                    ["PAUSED", "STOPPED"].includes(audioStateLocal) ? "play-button" : "pause-button"
                  }
                />
              )}
              {["NONE", "STOPPED"].includes(audioState) && (
                <IconButton
                  icon="record_outlined"
                  iconColor={theme.palette.danger.light}
                  iconStyle={audioState === "NONE" ? iconRecordButtonStyle : iconButtonStyle}
                  handleClick={handleRecording}
                  title={audioState === "NONE" ? t("audioControls.record") : ""}
                  textStyle={textStyle}
                  data-testid="record-button"
                />
              )}
              {!["NONE"].includes(audioState) && (
                <IconButton
                  icon="stop_outlined"
                  iconColor={iconButtonStyle.color}
                  iconStyle={audioState === "RECORDING" ? iconRecordButtonStyle : iconButtonStyle}
                  handleClick={handleStop}
                  title={audioState === "RECORDING" ? t("audioControls.stop") : ""}
                  textStyle={textStyle}
                  data-testid="stop-button"
                />
              )}
              {!["NONE", "RECORDING"].includes(audioState) && (
                <IconButton
                  icon="editor_forward"
                  iconColor={iconButtonStyle.color}
                  iconStyle={iconButtonStyle}
                  handleClick={handleForward}
                  textStyle={textStyle}
                  data-testid="forward-button"
                />
              )}
            </ControlContainer>
          </>
        )}
        {recordingCounter > 0 && isPortraitView && buttonPortraitView}
        {recordingCounter > 0 && !isPortraitView && bottonsInHorizontalView}
      </Root>
    );
  }

  return <Root>{isPortraitView ? buttonPortraitView : bottonsInHorizontalView}</Root>;
}

export const AudioControlsMemoized = React.memo(AudioControls);
