import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";

export const Root = styled(Box)(({ theme }) => ({
  position: "fixed",
  left: 0,
  bottom: 0,
  width: "100%",
  display: "flex",
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  zIndex: 10,
  paddingTop: 5,
  backgroundColor: theme.palette.primary.main,
  // boxShadow:
  //   ["STOPPED", "PLAYING", "PAUSED"].includes(audioState) || context === "editing"
  //     ? "0px -5px 5px rgba(0, 0, 0, 0.1)"
  //     : "0px",

  "& .MuiButton-root": {
    minWidth: "35px",
  },
}));

export const ControlContainer = styled(Box)({
  display: "flex",
  width: "100%",
  flexDirection: "row",
  marginBottom: 10,
});

export const DisplayTime = styled(Box)({
  display: "flex",
  maxWidth: "250px",
  alignItems: "center",
});

export const ListControl = styled(Box)({
  display: "flex",
  alignItems: "center",
  paddingRight: 17,
});

export const MarginButtons = styled(Box)({
  display: "flex",
  "& > *": {
    marginLeft: 2,
    marginRight: 2,
  },
});

export const MyDivider = styled(Divider)(({ theme }) => ({
  backgroundColor: theme.palette.variation7.light,
  marginLeft: 8,
  marginRight: 8,
}));

export const BoxTime = styled(Box)(({ theme }) => ({
  fontWeight: 700,
  fontSize: 22,
  textAlign: "center",
  color: theme.palette.primary.contrastText,
  alignSelf: "center",
  display: "flex",
  flex: 1,
}));

export const MasterControl = styled(Box)(({ theme }) => ({
  width: "200px",
  display: "flex",
  flexDirection: "column",
  color: "#FFF",
  padding: "0 15px",
  "& .MuiSlider-root": {
    color: "#FFF",
  },
  "& .MuiSlider-thumb": {
    color: theme.palette.primary.dark,
  },
}));

export const MySliderBox = styled(Box)({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
});
