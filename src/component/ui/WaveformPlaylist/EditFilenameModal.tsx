/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import React from "react";
import { useTranslation } from "next-i18next";
import Modal from "@/components/ui/Modal";
import Button from "@/components/ui/Button";
import TextField from "@material-ui/core/TextField";
import Divider from "@/components/ui/Divider";
import { Formik, Form, Field, FieldProps } from "formik";
import { Grid } from "@material-ui/core";
import ErrorMessageForm from "@/components/ui/ErrorFormMessage";
import * as Yup from "yup";
import { toast } from "@/utils/notifications";
import { setFilenameRecorder, removeSpecialCharacters, getPathRecorder } from "@/utils/utils";
import { ButtonVariantEnum, AudioExportTypesEnum } from "@/enums/*";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import { updateFile as updateFileLocal, findByFilename } from "@/store/idb/models/files";
import { convertPrivateToUsername, convertUsernameToPrivate } from "@/utils/directory";
import { moveFile, existFile } from "@/services/webdav/files";

type Props = {
  fileTitle: string;
  open: boolean;
  handleClose: () => void;
  setCurrentPath: (currentPath: string) => void;
};

interface MyFormValues {
  title: string;
}

export default function EditFilenameModal({ fileTitle, open, handleClose, setCurrentPath }: Props) {
  const { t } = useTranslation("recording");
  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const initialValues = {
    title: fileTitle,
  };

  const maxLengthName = 100;

  const schemaValidation = Yup.object().shape({
    title: Yup.string()
      .required(c("form.requiredTitle"))
      .max(maxLengthName, c("form.passwordMaxLengthTitle", { size: maxLengthName })),
  });

  const handleSubmit = async (values: MyFormValues, setSubmitting: (flag: boolean) => void) => {
    const { title } = values;
    const userId = userRdx.user.id;
    setSubmitting(true);
    try {
      const defaultAudioType = AudioExportTypesEnum.WAV;
      const path = await getPathRecorder();
      const pathPvtToUsr = convertPrivateToUsername(path, userId);
      const pathUsrToPvt = convertUsernameToPrivate(path, userId);

      const aliasFilename = `${pathPvtToUsr}/${removeSpecialCharacters(title)}.${defaultAudioType}`;
      const filename = `${pathUsrToPvt}/${removeSpecialCharacters(title)}.${defaultAudioType}`;
      const recording = {
        title,
        path,
        filename,
        basename: title,
        aliasFilename,
      };

      const currentFilename = `${pathUsrToPvt}/${removeSpecialCharacters(
        fileTitle,
      )}.${defaultAudioType}`;

      const { id: audioId } = await findByFilename(userId, currentFilename);

      await updateFileLocal(audioId, recording);

      setCurrentPath(filename);

      await setFilenameRecorder(title);

      const fileExists = await existFile(userId, currentFilename);

      if (fileExists) {
        await moveFile(userId, currentFilename, filename);
      }

      toast(t("filenameUpdated"), "success");
      handleClose();
    } catch (e) {
      console.log(e);
      toast(c("genericErrorMessage"), "error");
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <Modal
      title={t("updateFilenameTitle")}
      data-testid="edit-filename-modal"
      handleClose={handleClose}
      open={open}
    >
      <Formik
        validationSchema={schemaValidation}
        initialValues={initialValues}
        onSubmit={(values: MyFormValues, { setSubmitting }: any) => {
          handleSubmit(values, setSubmitting);
        }}
      >
        {({ submitForm, isSubmitting, errors, touched }: any) => (
          <Form>
            <Field name="title" data-testid="filename-name" InputProps={{ notched: true }}>
              {({ field }: FieldProps) => (
                <TextField
                  id="title"
                  label={t("filenameLabel")}
                  variant="outlined"
                  inputProps={{
                    autoComplete: "off",
                    maxLength: maxLengthName,
                  }}
                  fullWidth
                  {...field}
                />
              )}
            </Field>
            {errors.title && touched.title ? <ErrorMessageForm message={errors.title} /> : null}
            <Divider marginTop={20} />
            <Grid container justifyContent="space-between">
              <Button
                handleClick={handleClose}
                variant={ButtonVariantEnum.OUTLINED}
                title={c("form.cancelButton")}
                data-testid="cancel-edit-filename"
              />
              <Button
                handleClick={submitForm}
                title={c("form.submitSaveTitle")}
                disabled={isSubmitting}
                isLoading={isSubmitting}
                data-testid="submit-edit-filename"
                type="submit"
              />
            </Grid>
          </Form>
        )}
      </Formik>
    </Modal>
  );
}
