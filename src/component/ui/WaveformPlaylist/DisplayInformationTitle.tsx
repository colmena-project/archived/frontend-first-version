import { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import { PropsRecordingSelector } from "@/types/*";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@/components/ui/IconButton";
import { useTranslation } from "next-i18next";
import { useSelector } from "react-redux";
import EditFilenameModal from "./EditFilenameModal";
import MainTitle from "@/components/ui/MainTitle";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type Props = {
  filename: string;
  saved: boolean;
  setCurrentPath: (currentPath: string) => void;
};

function customStyles(windowWidth: number) {
  return makeStyles(() => ({
    container: {
      padding: 0,
      margin: 0,
      display: "flex",
      flex: 1,
      justifyContent: "flex-start",
      flexDirection: "row",
      alignContent: "center",
      alignItems: "center",
      width: windowWidth < 700 ? 170 : "100%",
    },
    mainTitle: {
      color: "#fff",
      fontSize: 16,
      padding: 0,
      textAlign: "left",
      textOverflow: "ellipsis",
      whiteSpace: "nowrap",
      overflow: "hidden",
      fontWeight: 900,
    },
  }));
}

export default function DisplayInformationTitle({ filename, saved, setCurrentPath }: Props) {
  const { t: r } = useTranslation("recording");
  const [openEditModal, setOpenEditModal] = useState(false);
  const recordingRdx = useSelector(
    (state: { recording: PropsRecordingSelector }) => state.recording,
  );
  const audioState = recordingRdx.activeRecordingState;
  const [dynamicWidth, setDynamicWidth] = useState(400);
  const useStyles = customStyles(dynamicWidth);
  const connectionStatus = useConnectionStatus();
  const classes = useStyles();

  useEffect(() => {
    setDynamicWidth(window.innerWidth);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [window.innerWidth]);

  if (["NONE", "RECORDING"].includes(audioState))
    return (
      <MainTitle
        textColor="#fff"
        title={r("title")}
        fontSizeTitle={18}
        data-testid="recorder-title"
      />
    );

  return (
    <Box className={classes.container}>
      <Text
        variant={TextVariantEnum.BODY1}
        data-testid="recorder-title"
        className={classes.mainTitle}
      >
        {filename}
      </Text>
      {connectionStatus && saved && (
        <>
          <IconButton
            icon="edit"
            iconColor="#fff"
            iconStyle={{ fontSize: 22 }}
            style={{ minWidth: 25 }}
            textStyle={{
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
            }}
            data-testid="edit-recorder-filename-button"
            handleClick={() => setOpenEditModal(true)}
          />
          <EditFilenameModal
            fileTitle={filename}
            open={openEditModal}
            handleClose={() => setOpenEditModal(false)}
            setCurrentPath={setCurrentPath}
          />
        </>
      )}
    </Box>
  );
}
