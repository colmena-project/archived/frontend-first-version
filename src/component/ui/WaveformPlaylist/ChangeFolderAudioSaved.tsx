import { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import { makeStyles, Theme } from "@material-ui/core/styles";
import Text from "@/components/ui/Text";
import {
  TextVariantEnum,
  ButtonVariantEnum,
  ButtonSizeEnum,
  DirectoryNamesNCEnum,
} from "@/enums/*";
import { PropsUserSelector, PropsLibrarySelector } from "@/types/index";
import { useSelector } from "react-redux";
import { prepareUploadPath, setPathRecorder } from "@/utils/utils";
import { useTranslation } from "next-i18next";
import Button from "@/components/ui/Button";
import theme from "@/styles/theme";
import { useRouter } from "next/router";
import ChangeUploadLocationModal from "./ChangeUploadLocationModal";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";

const useStyles = makeStyles((theme: Theme) => ({
  bar: {
    backgroundColor: theme.palette.secondary.main,
    flex: 1,
    display: "flex",
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    marginBottom: 10,
  },
  title: {
    color: "#fff",
    fontSize: 14,
    textAlign: "left",
  },
}));

export default function ChangeFolderAudioSaved() {
  const { t } = useTranslation("recording");
  const { t: l } = useTranslation("library");
  const { t: c } = useTranslation("common");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const libraryRdx = useSelector((state: { library: PropsLibrarySelector }) => state.library);
  const [pathLocation, setPathLocation] = useState("");
  const classes = useStyles();
  const [openLibrary, setOpenLibrary] = useState(false);
  const router = useRouter();
  const chatPath = router?.query?.chatPath;

  const init = async () => {
    try {
      const path = await prepareUploadPath(libraryRdx.currentPath, userRdx.user.id, chatPath);
      setPathLocation(path);
      await setPathRecorder(path);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    setTimeout(() => {
      init();
    }, 500);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chatPath]);

  const handleChangeLocation = async (path: string) => {
    setPathLocation(path);
    await setPathRecorder(path);
    setOpenLibrary(false);
  };

  return (
    <Box className={classes.bar} data-testid="container-change-audio-location">
      <Text variant={TextVariantEnum.BODY2} data-testid="upload-location" className={classes.title}>
        <span>{t("uploadLocationTitle")}</span>:{` `}
        <strong>{pathLocation.replace(DirectoryNamesNCEnum.TALK, l("talkFolderName"))}</strong>
      </Text>
      <OnlineOnly>
        <Button
          title={t("changeUploadLocationTitle")}
          variant={ButtonVariantEnum.OUTLINED}
          size={ButtonSizeEnum.SMALL}
          borderColor={theme.palette.secondary.main}
          handleClick={() => setOpenLibrary(true)}
          data-testid="change-directory"
        />
        {openLibrary && (
          <ChangeUploadLocationModal
            title={c("changeLocationModalTitle")}
            open={openLibrary}
            handleClose={() => setOpenLibrary(false)}
            handleClick={(path: string) => handleChangeLocation(path)}
          />
        )}
      </OnlineOnly>
    </Box>
  );
}
