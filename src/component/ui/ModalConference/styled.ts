import Box from "@material-ui/core/Box";
import { styled } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles/createTheme.d";

export const BaseHeader = styled(Box)(({ theme }: { theme: Theme }) => ({
  display: "flex",
  flex: 1,
  flexDirection: "row",
  backgroundColor: theme.palette.primary.dark,
}));
