import React from "react";
import { withStyles, Theme, createStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import { currentDirection } from "@/utils/i18n";
import Box from "@material-ui/core/Box";
import ShareLinkVirtualStudioModal from "./ShareLinkVirtualStudioModal";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import { BaseHeader } from "./styled";

const styles = ({ palette }: Theme) =>
  createStyles({
    title: {
      padding: 8,
      margin: 8,
      border: "none",
      backgroundColor: palette.primary.dark,
      color: "#fff",
    },
    subtitle: {
      padding: 0,
      "& h6": {
        color: palette?.primary?.light,
        fontSize: 12,
      },
    },
  });

type Props = {
  title?: string;
  subtitle?: string | undefined;
  description?: string | null;
  open: boolean;
  handleClose?: (event: any, reason: string) => void | undefined;
  children: React.ReactNode;
  actions?: React.ReactNode;
  disableBackdropClick?: boolean;
  fullScreen?: boolean;
  type?: "virtual-studio" | "call";
  // eslint-disable-next-line @typescript-eslint/ban-types
  dialogContentStyle?: any | undefined;
  dialogTitleStyle?: any | undefined;
  rightElement?: React.ReactNode | undefined;
  publicVirtualStudioLink?: string;
  [x: string]: any;
};

function Modal({
  title,
  subtitle = undefined,
  description,
  open,
  handleClose,
  children,
  actions,
  fullScreen = false,
  dialogContentStyle = undefined,
  dialogTitleStyle = undefined,
  rightElement = undefined,
  type = "call",
  publicVirtualStudioLink = "",
  ...props
}: Props) {
  const DialogTitle = withStyles(styles)((props: any) => {
    const { children, classes } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.title}>
        <Text variant={TextVariantEnum.H5} style={{ fontWeight: "bold" }}>
          {children}
        </Text>
      </MuiDialogTitle>
    );
  });

  const DialogSubTitle = withStyles(styles)((props: any) => {
    const { children, classes, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.subtitle} {...other}>
        <Text variant={TextVariantEnum.H6}>{children}</Text>
      </MuiDialogTitle>
    );
  });

  const DialogContent = withStyles(() => ({
    root: {
      margin: 0,
      padding: 0,
    },
  }))(MuiDialogContent);

  return (
    <Dialog
      dir={currentDirection()}
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={open}
      fullWidth
      fullScreen={fullScreen}
      maxWidth="xs"
      {...props}
    >
      <BaseHeader>
        <Box display="flex" flex={1} flexDirection="column">
          {title && (
            <DialogTitle
              className={dialogTitleStyle}
              id="customized-dialog-title"
              onClose={handleClose}
            >
              {title}
              <br />
              {subtitle && <DialogSubTitle>{subtitle}</DialogSubTitle>}
            </DialogTitle>
          )}
        </Box>
        <Box paddingRight={2} display="flex" justifyContent="center" alignItems="center">
          {type === "virtual-studio" && (
            <ShareLinkVirtualStudioModal publicVirtualStudioLink={publicVirtualStudioLink} />
          )}
          {rightElement}
        </Box>
      </BaseHeader>
      <DialogContent className={dialogContentStyle}>
        {description && <DialogContentText>{description}</DialogContentText>}
        {children}
      </DialogContent>
      {actions && <DialogActions>{actions}</DialogActions>}
    </Dialog>
  );
}

export default Modal;
