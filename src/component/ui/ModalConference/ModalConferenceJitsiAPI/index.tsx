import React from "react";
import Modal from "@/components/ui/ModalConference";
import { JitsiAPIConferenceMemoized } from "@/components/pages/honeycomb/Call/JitsiAPIConference";
import { CallTypesProps } from "@/types/*";

type Props = {
  handleClose: () => void;
  rightElement: React.ReactNode | undefined;
  roomName: string;
  modalTitle: string;
  modalSubtitle?: string | undefined;
  moderator?: boolean;
  open: boolean;
  type?: CallTypesProps;
  publicVirtualStudioLink?: string;
  guestUsername?: string;
};

export default function ModalConferenceJitsiAPI({
  handleClose,
  rightElement,
  modalTitle,
  modalSubtitle,
  open,
  roomName,
  moderator = false,
  type,
  publicVirtualStudioLink = "",
  guestUsername = "",
}: Props) {
  return (
    <Modal
      title={modalTitle}
      subtitle={modalSubtitle}
      rightElement={rightElement}
      open={open}
      fullScreen
      handleClose={handleClose}
      type={type}
      publicVirtualStudioLink={publicVirtualStudioLink}
    >
      <JitsiAPIConferenceMemoized
        type={type}
        roomName={roomName}
        moderator={moderator}
        handleClose={handleClose}
        guestUsername={guestUsername}
      />
    </Modal>
  );
}

export const ModalConferenceJitsiAPIMemoized = React.memo(ModalConferenceJitsiAPI);
