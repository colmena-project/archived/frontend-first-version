import { useState } from "react";
import SvgIconAux from "@/components/ui/SvgIcon";
import Clickable from "@/components/ui/Clickable";
import ModalShareLink from "@/components/pages/call/ShareLink/SimpleDialog";
import { useTranslation } from "next-i18next";

type Props = {
  publicVirtualStudioLink: string;
};

export default function ShareLinkVirtualStudioModal({ publicVirtualStudioLink }: Props) {
  const [openShareLinkModal, setOpenShareLinkModal] = useState(false);
  const { t } = useTranslation("virtualStudio");

  return (
    <>
      <Clickable handleClick={() => setOpenShareLinkModal(true)} style={{ paddingRight: 20 }}>
        <SvgIconAux icon="share" htmlColor="#fff" fontSize="small" />
      </Clickable>
      <ModalShareLink
        open={openShareLinkModal}
        url={publicVirtualStudioLink}
        onClose={() => setOpenShareLinkModal(false)}
        titleShareLink={t("modalShareLinkTitle")}
      />
    </>
  );
}
