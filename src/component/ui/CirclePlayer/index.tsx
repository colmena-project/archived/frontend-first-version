/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useCallback, useEffect, useRef, useState } from "react";
import ReactPlayer from "react-player";
import theme from "@/styles/theme";
import { arrayBufferToBlob } from "blob-util";
import { DefaultTypesSafari, EnvironmentEnum } from "@/enums/*";
import { listFile } from "@/services/webdav/files";
import { useSelector } from "react-redux";
import { Environment, PropsUserSelector } from "@/types/*";
import {
  createFile as createQuickBlob,
  findByBasename as findQuickBlobByBasename,
  removeFile,
} from "@/store/idb/models/filesQuickBlob";
import { downloadFile, isSafari, removeSpecialCharacters, prepareBlobUrl } from "@/utils/utils";
import { toast } from "@/utils/notifications";
import { Box, makeStyles } from "@material-ui/core";
import SvgIconAux from "@/components/ui/SvgIcon";
import CircularProgress, { CircularProgressProps } from "@material-ui/core/CircularProgress";
import { useTranslation } from "next-i18next";
import { getFilename } from "@/utils/directory";
import { CirclePlayerWrapper, Control, PlayerWrapper } from "@/components/ui/CirclePlayer/styled";

const useStyles = makeStyles((theme) => ({
  controlPlaying: {
    left: 0,
  },
  bottom: {
    color: theme.palette.gray.main,
  },
  top: {
    color: theme.palette.secondary.main,
    animationDuration: "550ms",
    position: "absolute",
    left: 0,
    marginTop: -1,
  },
  circle: {
    strokeLinecap: "round",
  },
}));

type onProgress = {
  played: number;
  playedSeconds: number;
  loaded: number;
  loadedSeconds: number;
};

type Props = {
  file?: File | ArrayBuffer | Blob | undefined;
  environment: Environment;
  filename: string;
  download?: boolean;
  setDownload?: (download: boolean) => void;
};

export default function Index({ file, environment, filename, download, setDownload }: Props) {
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { t: c } = useTranslation("common");
  const player = useRef(null);
  const [playing, setPlaying] = useState(false);
  const [progress, setProgress] = useState<number | undefined>(undefined);
  const [loading, setLoading] = useState(false);
  const [blob, setBlob] = useState<Blob | ArrayBuffer | File | undefined>(file);
  const [audioElm, setAudioElm] = useState<HTMLAudioElement | undefined>();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // eslint-disable-next-line no-unused-vars
  const [url, setUrl] = useState<string | undefined>();
  const isSafariBrowser = isSafari();

  const resetPlayer = useCallback(() => {
    setPlaying(false);
    setProgress(undefined);
    setLoading(false);
    setBlob(undefined);
    setUrl(undefined);
  }, []);

  useEffect(() => {
    if (!playing) return;
    (async () => {
      let currentBlob = blob;
      if (environment !== EnvironmentEnum.LOCAL) {
        currentBlob = await prepareRemoteBlob();
      }

      if (currentBlob && !url) {
        setUrl(prepareBlobUrl(currentBlob));
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [playing]);

  const prepareRemoteBlob = useCallback(async () => {
    setLoading(true);
    let currentBlob: Blob;
    try {
      if (environment === EnvironmentEnum.LOCAL) throw new Error();

      const localFile = await findQuickBlobByBasename(
        userRdx.user.id,
        removeSpecialCharacters(filename),
      );
      if (!localFile) {
        const result: any = await listFile(userRdx.user.id, filename);
        removeFile(userRdx.user.id, removeSpecialCharacters(filename));
        await createQuickBlob({
          basename: removeSpecialCharacters(filename),
          userId: userRdx.user.id,
          arrayBufferBlob: result,
        });

        currentBlob = arrayBufferToBlob(result, isSafari() ? DefaultTypesSafari.AUDIO : undefined);
      } else {
        currentBlob = arrayBufferToBlob(
          localFile?.arrayBufferBlob,
          isSafari() ? DefaultTypesSafari.AUDIO : undefined,
        );
      }

      if (!currentBlob) throw new Error();
      setBlob(currentBlob);
      return Promise.resolve(currentBlob);
    } catch (e) {
      resetPlayer();
      toast(c("genericErrorMessage"), "error");
    } finally {
      setLoading(false);
    }

    return Promise.resolve(undefined);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [c, environment, filename, resetPlayer, userRdx.user.id]);

  useEffect(() => {
    if (audioElm) {
      audioElm.onended = () => {
        setPlaying(false);
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [audioElm]);

  useEffect(() => {
    (async () => {
      if (download) {
        let currentBlob = blob;
        if (!currentBlob) {
          currentBlob = await prepareRemoteBlob();
        }

        if (currentBlob) {
          downloadFile(currentBlob, getFilename(filename));
        }

        if (setDownload) setDownload(false);
      }
    })();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [download]);

  const handlePlayAudio = (playing: boolean) => {
    setPlaying(playing);
  };

  const createAudioElement = (blob: Blob | undefined) => {
    if (!blob) return;

    const audioObjectURL = window.URL.createObjectURL(blob);
    const audioElement = document.createElement("audio");
    document.body.appendChild(audioElement);
    const sourceElement = document.createElement("source");
    audioElement.appendChild(sourceElement);
    sourceElement.src = audioObjectURL;
    sourceElement.type = DefaultTypesSafari.AUDIO;
    setAudioElm(audioElement);
  };

  const size = 40;

  const handlePlayerState = (playerState: onProgress) => {
    setProgress(playerState.played * 100);
  };
  // {playedSeconds: 1.306671, played: 0.4358475650433622, loadedSeconds: 2.998, loaded: 1}
  const Player = (props: CircularProgressProps) => (
    <PlayerWrapper component="span">
      <CircularProgress
        variant="determinate"
        className={classes.bottom}
        size={size}
        thickness={1}
        value={100}
      />
      <CircularProgress
        className={classes.top}
        classes={{
          circle: classes.circle,
        }}
        size={size + 2}
        thickness={4}
        {...props}
      />
    </PlayerWrapper>
  );

  return (
    <CirclePlayerWrapper component="span">
      {blob && (
        <ReactPlayer
          ref={player}
          url={url}
          playing={playing}
          height="0"
          width="0"
          onProgress={handlePlayerState}
          progressInterval={1}
          onEnded={() => {
            setPlaying(false);
            setProgress(0);
          }}
        />
      )}
      <Player value={progress} variant={loading ? "indeterminate" : "determinate"} />
      {!loading && (
        <Control
          handleClick={() => handlePlayAudio(!playing)}
          className={playing ? classes.controlPlaying : undefined}
        >
          <SvgIconAux
            icon={playing ? "basic_pause" : "basic_play"}
            htmlColor={theme.palette.primary.main}
            fontSize={16}
          />
        </Control>
      )}
    </CirclePlayerWrapper>
  );
}
