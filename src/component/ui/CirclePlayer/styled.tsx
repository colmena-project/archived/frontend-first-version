import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Clickable from "@/components/ui/Clickable";

export const CirclePlayerWrapper = styled(Box)({
  position: "relative",
  display: "inline-flex",
  justifyContent: "space-around",
  alignItems: "center",
});

export const Control = styled(Clickable)({
  top: 0,
  left: 3,
  bottom: 0,
  right: 0,
  position: "absolute",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

export const PlayerWrapper = styled(Box)({
  position: "relative",
  display: "flex",
});
