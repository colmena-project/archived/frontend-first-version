import React, { useCallback } from "react";
import Grid from "@material-ui/core/Grid";
import { useTheme, makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { removeCornerSlash } from "@/utils/utils";
import Text from "../Text";
import { useTranslation } from "next-i18next";
import SvgIconAux from "../SvgIcon";
import Link from "next/link";

type Props = {
  page: string;
};

const useStyles = makeStyles(() => ({
  gridContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    zIndex: 1200,
    "& a.MuiButtonBase-root": {
      width: "100%",
    },
  },
  btn: {
    display: "block",
    padding: "5px 0",
    cursor: "pointer",
  },
  title: {
    fontSize: 10,
    lineHeight: 1.6,
    letterSpacing: 0.5,
  },
}));

function AppTab({ page }: Props) {
  // const library = useSelector((state: { library: PropsLibrarySelector }) => state.library);
  // const libraryPath = library.currentPath;
  const { t } = useTranslation("common");
  const classes = useStyles();
  const theme = useTheme();

  const iconSize = 28;

  const handleMenuColor = useCallback(
    (rootPath) => {
      const splitPage = removeCornerSlash(page).split("/");
      if (splitPage.length > 0 && splitPage[0] === rootPath) {
        return theme.palette.primary.main;
      }

      return theme.palette.gray.dark;
    },
    [page, theme.palette.gray.dark, theme.palette.primary.main],
  );

  return (
    <>
      <Grid container className={classes.gridContainer}>
        <Grid item xs={2}>
          <Link href="/home">
            <Box className={classes.btn}>
              <SvgIconAux icon="home" fontSize={iconSize} htmlColor={handleMenuColor("home")} />
              <Text className={classes.title} style={{ color: handleMenuColor("home") }}>
                {t("navTitles.home")}
              </Text>
            </Box>
          </Link>
        </Grid>
        <Grid item xs={2}>
          <Link href="/honeycomb">
            <Box className={classes.btn}>
              <SvgIconAux
                icon="panal"
                fontSize={iconSize}
                htmlColor={handleMenuColor("honeycomb")}
              />
              <Text className={classes.title} style={{ color: handleMenuColor("honeycomb") }}>
                {t("navTitles.honeycomb")}
              </Text>
            </Box>
          </Link>
        </Grid>
        <Grid item xs={2}>
          <Link href="/library">
            <Box className={classes.btn}>
              <SvgIconAux
                icon="library"
                fontSize={iconSize}
                htmlColor={handleMenuColor("library")}
              />
              <Text className={classes.title} style={{ color: handleMenuColor("library") }}>
                {t("navTitles.library")}
              </Text>
            </Box>
          </Link>
        </Grid>
        <Grid item xs={2}>
          <Link href="/publications">
            <Box className={classes.btn}>
              <SvgIconAux
                icon="global"
                fontSize={iconSize}
                htmlColor={handleMenuColor("publications")}
              />
              <Text className={classes.title} style={{ color: handleMenuColor("publications") }}>
                {t("navTitles.publications")}
              </Text>
            </Box>
          </Link>
        </Grid>
      </Grid>
    </>
  );
}

export default AppTab;
