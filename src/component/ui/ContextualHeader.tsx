import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core";
import theme from "@/styles/theme";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flex: 1,
    padding: 5,
    width: "100%",
    maxHeight: 45,
    justifyContent: "space-around",
    alignItems: "center",
  },
  title: {
    color: "white",
  },
}));

type TypeContextualHeader = "INFO" | "DANGER" | "WARNING";

type Props = {
  type: TypeContextualHeader;
  title: string;
  leftElement?: React.ReactNode | undefined;
  rightElement?: React.ReactNode | undefined;
};

export default function ContextualHeader({
  type = "INFO",
  title,
  leftElement = undefined,
  rightElement = undefined,
}: Props) {
  const classes = useStyles();

  const getBackgroundColor = (type: TypeContextualHeader) => {
    switch (type) {
      case "INFO":
        return theme.palette.variation1.main;
      case "DANGER":
        return theme.palette.danger.light;
      default:
        return theme.palette.variation1.main;
    }
  };

  return (
    <Box className={classes.root} style={{ backgroundColor: getBackgroundColor(type) }}>
      {leftElement && leftElement}
      <Text variant={TextVariantEnum.BODY1} className={classes.title}>
        {title}
      </Text>
      {rightElement && rightElement}
    </Box>
  );
}
