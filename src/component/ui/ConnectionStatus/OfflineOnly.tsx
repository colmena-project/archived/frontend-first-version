import React from "react";
import ConnectionStatus from "./index";

type Props = {
  children?: React.ReactNode | string;
};

export default function OfflineOnly({ children }: Props) {
  return <ConnectionStatus status={false}>{children}</ConnectionStatus>;
}
