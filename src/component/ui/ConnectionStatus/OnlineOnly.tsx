import React from "react";
import ConnectionStatus from "./index";

type Props = {
  children?: React.ReactNode | string;
};

export default function OnlineOnly({ children }: Props) {
  return <ConnectionStatus status>{children}</ConnectionStatus>;
}
