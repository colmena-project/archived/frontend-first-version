import React from "react";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type Props = {
  status: boolean;
  children?: React.ReactNode | string;
};
export default function ConnectionStatus({ status, children }: Props) {
  const connectionStatus = useConnectionStatus();
  if (connectionStatus === status) {
    return <>{children}</>;
  }

  return null;
}
