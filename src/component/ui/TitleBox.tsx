import React, { ReactElement } from "react";
import SvgIconAux from "@/components/ui/SvgIcon";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/*";
import { Box, makeStyles } from "@material-ui/core";
import { AllIconProps } from "@/types/*";
import { RoomAvatarInterface } from "@/interfaces/index";

const useStyles = makeStyles((theme) => ({
  boxTitle: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    background: "url(/images/svg/bg-media-profile.svg) repeat-y right",
    backgroundColor: theme.palette.variation3.main,
    height: 67,
    padding: "0 10px",
  },
  titleIcon: {
    marginRight: 8,
  },
  title: {
    color: "#fff",
    fontSize: "1.2rem",
    fontWeight: "bold",
  },
}));

type Props = {
  title: string;
  icon?: AllIconProps;
  align?: string;
  avatar?: ReactElement<RoomAvatarInterface>;
};

export default function TitleBox({ title, icon, avatar, align = "flex-start" }: Props) {
  const classes = useStyles();

  return (
    <Box className={classes.boxTitle} style={{ justifyContent: align }}>
      {icon && (
        <SvgIconAux icon={icon} htmlColor="#fff" className={classes.titleIcon} fontSize={22} />
      )}
      {avatar && avatar}
      <Text variant={TextVariantEnum.H1} className={classes.title}>
        {title}
      </Text>
    </Box>
  );
}
