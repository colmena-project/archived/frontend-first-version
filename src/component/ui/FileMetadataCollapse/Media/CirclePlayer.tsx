import React from "react";
import { EnvironmentEnum } from "@/enums/*";
import CirclePlayer from "@/components/ui/CirclePlayer";

type Props = {
  file: File;
  title: string;
};

export default function MediaCirclePlayer({ file, title }: Props) {
  return <CirclePlayer file={file} filename={title || ""} environment={EnvironmentEnum.LOCAL} />;
}
