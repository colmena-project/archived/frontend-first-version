import React, { useCallback, useMemo, Suspense } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";

type Props = {
  file?: File;
  type?: string;
  title?: string;
  extension?: string;
  mime?: string;
};

const useStyle = makeStyles((theme) => ({
  mediaWrapper: {
    marginRight: theme.spacing(1),
    width: 40,
    maxWidth: 40,
  },
}));

export default function MediaFile(props: Props) {
  const { type, file } = props;
  const theme = useTheme();
  const classes = useStyle();

  const getMedia = useCallback((type) => {
    const medias: any = {
      audio: React.lazy(() => import("./CirclePlayer")),
      image: React.lazy(() => import("./Image")),
      others: React.lazy(() => import("../../FileIcon")),
    };
    if (typeof medias[type] === "undefined") {
      return false;
    }

    return medias[type];
  }, []);

  const MediaComponent = useMemo(() => {
    const media = getMedia(type);

    return file && media ? media : getMedia("others");
  }, [getMedia, type, file]);

  return (
    <Box className={classes.mediaWrapper}>
      <Suspense fallback={<CircularProgress style={{ color: theme.palette.primary.main }} />}>
        <MediaComponent {...props} type="file" />
      </Suspense>
    </Box>
  );
}
