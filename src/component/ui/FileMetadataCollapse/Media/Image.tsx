import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { Box } from "@material-ui/core";

type Props = {
  file: File;
};

export default function MediaCirclePlayer({ file }: Props) {
  return (
    <Box>
      <Avatar src={typeof file === "string" ? file : URL.createObjectURL(file)} variant="rounded" />
    </Box>
  );
}
