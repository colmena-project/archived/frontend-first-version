import React, { memo, useState } from "react";
import { ButtonSizeEnum } from "@/enums/*";
import { Collapse, useTheme } from "@material-ui/core";
import { useTranslation } from "next-i18next";
import Metadata from "./Metadata";
import { FileMetadataInterface } from "@/interfaces/index";
import FileCard from "@/components/ui/FileMetadataCollapse/Card";
import MetaDataField from "@/components/pages/publications/files/MetadataField";

import { Wrapper, ActionButtonOutline, ActionsWrapper } from "./styled";

type Props = {
  actions?: React.ReactNode[];
  alignActions?: "flex-start" | "flex-end" | "center" | "space-between";
  open?: boolean;
  metadata: FileMetadataInterface;
  isCollapsed?: boolean;
  showCollapseButton?: boolean;
  content?: React.ReactNode;
  onSelectFile?: () => void;
  hideCheckBox?: boolean;
};

const FileMetadataCollapse = ({
  open = false,
  actions,
  alignActions = "space-between",
  metadata,
  isCollapsed = true,
  showCollapseButton = true,
  content,
  onSelectFile,
  hideCheckBox,
}: Props) => {
  const [isOpen, setIsOpen] = useState(open);
  const [collapse, setCollapse] = useState(isCollapsed);

  const { t } = useTranslation("publications");
  const theme = useTheme();
  const { title, description } = metadata;

  return (
    <Wrapper data-testid="file-medatada-collapse">
      <FileCard
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        metadata={metadata}
        onSelectFile={onSelectFile}
        hideCheckBox={hideCheckBox}
      />

      <Collapse in={isOpen}>
        {content || (
          <>
            {title && (
              <MetaDataField
                label={t("fileManager.uploadFile.metadataLabels.title")}
                value={title || ""}
                noWrap={false}
              />
            )}

            {description && (
              <MetaDataField
                label={t("fileManager.uploadFile.metadataLabels.description")}
                value={description || ""}
                noWrap={false}
              />
            )}
          </>
        )}

        <Collapse in={!collapse}>
          <Metadata {...metadata} />
        </Collapse>

        <ActionsWrapper display="flex" justifyContent={alignActions} alignItems="center">
          {showCollapseButton && (
            <ActionButtonOutline
              title={
                !collapse
                  ? t("fileManager.uploadFile.actions.hideMetadata")
                  : t("fileManager.uploadFile.actions.viewMetadata")
              }
              data-testid="view-details"
              iconColor={theme.palette.warning.main}
              handleClick={() => setCollapse((old) => !old)}
              color={theme.palette.warning.main}
              startIcon="view_in_file"
              size={ButtonSizeEnum.SMALL}
            />
          )}

          {actions && actions.map((action) => action)}
        </ActionsWrapper>
      </Collapse>
    </Wrapper>
  );
};

export default memo(FileMetadataCollapse);
