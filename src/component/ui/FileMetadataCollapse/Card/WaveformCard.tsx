import { fancyTimeFormat } from "@/utils/utils";
import { Box, makeStyles, useTheme } from "@material-ui/core";
import React, { useCallback, useEffect, useState } from "react";
import Clickable from "@/components/ui/Clickable";
import SvgIconAux from "@/components/ui/SvgIcon";
import Waves from "@/components/pages/file/AudioWave/Waves";
import { arrayBufferToBlob } from "blob-util";
import HorizontalCard from "@/components/ui/HorizontalCard";
import classNames from "classnames";
import theme from "@/styles/theme";
import CircularProgress from "@material-ui/core/CircularProgress";
import Text from "@/components/ui/Text";

const useClasses = makeStyles(() => ({
  control: {
    top: 0,
    left: 3,
    bottom: 0,
    right: 0,
    position: "absolute",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  controlPlaying: {
    left: 0,
  },
  player: {
    position: "relative",
    display: "flex",
    width: 40,
    height: 40,
    borderRadius: "50%",
    border: "1px solid",
    borderColor: theme.palette.grey["400"],
    marginRight: 7,
  },
  fileNamePlaying: {
    fontSize: 10,
    maxWidth: 130,
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
  durationPlaying: {
    fontSize: 10,
    color: "gray",
  },
  fileSize: {
    fontSize: 10,
  },
  fileNamePause: {
    maxWidth: 180,
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
  durationGridItemTitle: {
    fontSize: 10,
    textAlign: "center",
    color: "gray",
  },
  iconButton: {
    padding: 5,
  },
  fileData: {
    marginTop: theme.spacing(1),
    display: "flex",
    flex: 1,
    justifyContent: "space-between",
  },
}));

type Props = {
  file: File;
  title?: string | React.ReactNode;
  fileSizeFormated: string;
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
};

const WaveformCard = ({ file, title, fileSizeFormated, isOpen, setIsOpen }: Props) => {
  const [loading, setLoading] = useState(false);
  const [blob, setBlob] = useState<Blob>();
  const [playing, setPlaying] = useState(false);
  const [audioState, setAudioState] = useState<"play" | "pause" | "stop">("stop");
  const [duration, setDuration] = useState("");

  const fetchArrayBuffer = useCallback(async () => {
    setLoading(true);
    try {
      const data = await file.arrayBuffer();

      const reader = new FileReader();

      const audioContext = new window.AudioContext();

      reader.onload = (ev) => {
        audioContext.decodeAudioData(ev.target?.result as ArrayBuffer, (decode: any) => {
          const time = `${fancyTimeFormat(decode.duration)}`;
          setDuration(time);
        });
      };

      reader.readAsArrayBuffer(file);

      const blob = arrayBufferToBlob(data);

      setBlob(blob);
    } catch (err) {
      console.warn(err.message);
    } finally {
      setLoading(false);
    }
  }, [file]);

  useEffect(() => {
    (async () => {
      await fetchArrayBuffer();
    })();
  }, [fetchArrayBuffer]);

  const classes = useClasses();
  const theme = useTheme();

  const handleAudioState = (playing: boolean) => {
    setAudioState(playing ? "pause" : "play");
    setPlaying(playing);
  };

  const endAudio = () => {
    setAudioState("stop");
    setPlaying(false);
  };

  return (
    <Box>
      <HorizontalCard
        noBorder
        avatar={
          <Box className={classes.player}>
            <Clickable
              handleClick={() => handleAudioState(!playing)}
              className={classNames(classes.control, playing ? classes.controlPlaying : undefined)}
            >
              <SvgIconAux
                icon={playing ? "basic_pause" : "basic_play"}
                htmlColor={theme.palette.primary.main}
                fontSize={16}
              />
            </Clickable>
          </Box>
        }
        primary={
          audioState === "stop" ? (
            title
          ) : (
            <>
              {loading ? (
                <Box
                  display="flex"
                  flex={1}
                  height={45}
                  justifyContent="center"
                  alignItems="center"
                >
                  <CircularProgress size={20} />
                </Box>
              ) : (
                <>
                  {Boolean(blob) && (
                    <Waves
                      blob={blob}
                      config={{ height: 20 }}
                      play={playing}
                      pause={!playing}
                      handleAudioFinish={() => endAudio()}
                    />
                  )}
                </>
              )}
            </>
          )
        }
        secondary={
          audioState === "stop" ? (
            fileSizeFormated
          ) : (
            <Box className={classes.fileData}>
              <Text className={classes.fileSize}>{fileSizeFormated}</Text>
              <Text className={classes.durationPlaying}>{duration}</Text>
            </Box>
          )
        }
        data-testid={`publication-file-item-${title}`}
        options={
          <Clickable handleClick={() => setIsOpen(!isOpen)} className={classes.iconButton}>
            <SvgIconAux
              data-testid="view-metadata"
              icon={isOpen ? "chevron_bottom" : "chevron_right"}
              fontSize={11}
              htmlColor={theme.palette.gray.main}
            />
          </Clickable>
        }
      />
    </Box>
  );
};

export default WaveformCard;
