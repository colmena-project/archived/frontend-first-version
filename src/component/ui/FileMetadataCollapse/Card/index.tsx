import React, { useCallback, useEffect, useMemo, useState } from "react";
import MediaFile from "@/components/ui/FileMetadataCollapse/Media";
import SvgIconAux from "@/components/ui/SvgIcon";
import HorizontalCard from "@/components/ui/HorizontalCard";
import { FileMetadataInterface } from "@/interfaces/index";
import {
  formatBytes,
  getExtensionByMimetype,
  getUrlStrapiImage,
  isAudioFile,
  isImageFile,
  isSafari,
  removeSpecialCharacters,
} from "@/utils/utils";
import { createFile as createQuickBlob, findByBasename } from "@/store/idb/models/filesQuickBlob";
import axios from "axios";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { Checkbox, useTheme } from "@material-ui/core";
import getConfig from "next/config";
import WaveformCard from "@/components/ui/FileMetadataCollapse/Card/WaveformCard";
import { UserIdEnum } from "@/enums/*";
import { OpenButton, Title } from "./styled";

const { publicRuntimeConfig } = getConfig();

type Props = {
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
  metadata: FileMetadataInterface;
  onSelectFile?: () => void;
  hideCheckBox?: boolean;
};

export default function FileCard({
  isOpen,
  setIsOpen,
  metadata,
  onSelectFile,
  hideCheckBox = false,
}: Props) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx?.user?.id || UserIdEnum.PUBLIC;
  const { title, mime, size, file, media } = metadata;
  const [fileContent, setFileContent] = useState<File | undefined>();
  const extension = useMemo(() => (mime ? getExtensionByMimetype(mime) : ""), [mime]);
  const theme = useTheme();

  const type = useMemo(() => {
    if (isImageFile(mime)) {
      return "image";
    }

    if (isAudioFile(mime)) {
      return "audio";
    }

    return "document";
  }, [mime]);

  const fileSizeFormated = useMemo(() => (size ? formatBytes(size, 2) : ""), [size]);

  const createFileCache = useCallback(async () => {
    let fileContent = file;

    if (isImageFile(mime) && media) {
      const imgUrl = getUrlStrapiImage(media, "thumbnail");

      setFileContent(imgUrl);
      return;
    }

    if (!file) {
      const name = `${title}-${size}`;
      const basename = `publication-file-${removeSpecialCharacters(name)}`;

      const cachedFile = findByBasename(userId, basename);

      if (cachedFile) {
        fileContent = new File([cachedFile.arrayBufferBlob], title ?? "file", { type: mime });
      } else if (media) {
        const baseUrl = publicRuntimeConfig?.publicationsGraphQL?.baseUrl;
        const url = baseUrl + media.data.attributes.url;
        const file = await axios.get(url, { responseType: "arraybuffer" });
        if (file) {
          const arrayBufferBlob = file.data;
          await createQuickBlob({
            basename,
            userId,
            arrayBufferBlob,
          });

          fileContent = new File([arrayBufferBlob], title ?? "file", { type: mime });
        }
      }
    }

    setFileContent(fileContent);
  }, [file, title, mime, media, userId, size]);

  useEffect(() => {
    (async () => {
      await createFileCache();
    })();
  }, [createFileCache]);

  const cardTitle = useMemo(() => <Title>{title}</Title>, [title]);

  const isSafariBrowser = isSafari();

  if (type === "audio" && fileContent && isSafariBrowser) {
    return (
      <WaveformCard
        file={fileContent}
        title={cardTitle}
        fileSizeFormated={fileSizeFormated}
        isOpen={isOpen}
        setIsOpen={setIsOpen}
      />
    );
  }

  return (
    <HorizontalCard
      noBorder
      avatar={
        <MediaFile file={fileContent} extension={extension} type={type} mime={mime} title={title} />
      }
      primary={cardTitle}
      secondary={fileSizeFormated}
      data-testid={`publication-file-item-${title}`}
      options={
        <>
          {!hideCheckBox && Boolean(onSelectFile) && (
            <Checkbox
              onChange={onSelectFile}
              data-testid="file-checkbox"
              inputProps={
                {
                  "data-testid": "input-checkbox-file",
                } as any
              }
            />
          )}

          <OpenButton handleClick={() => setIsOpen(!isOpen)}>
            <SvgIconAux
              data-testid="view-metadata"
              icon={isOpen ? "chevron_bottom" : "chevron_right"}
              fontSize={11}
              htmlColor={theme.palette.gray.main}
            />
          </OpenButton>
        </>
      }
    />
  );
}
