import { Box, styled } from "@material-ui/core";
import Clickable from "@/components/ui/Clickable";
import Text from "@/components/ui/Text";

export const Wrapper = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.common.white,
  borderColor: theme.palette.grey[200],
  borderWidth: 1,
  width: "100%",
  marginTop: 2,
}));

export const OpenButton = styled(Clickable)({
  padding: 5,
});

export const Title = styled(Text)({
  alignSelf: "stretch",
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
});
