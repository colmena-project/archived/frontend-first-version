import { Box, styled } from "@material-ui/core";
import OutlinedButton from "../Button/OutlinedButton";

export const Wrapper = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.common.white,
  borderColor: theme.palette.grey[200],
  borderWidth: 1,
  width: "100%",
  marginTop: 2,
}));

export const ActionButtonOutline = styled(OutlinedButton)({
  borderRadius: 5,
  padding: "3px 4px !important",
});

export const ActionsWrapper = styled(Box)({
  padding: 10,
});
