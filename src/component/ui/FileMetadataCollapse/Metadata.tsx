import React, { useEffect, useState } from "react";
import MetaDataField from "@/components/pages/publications/files/MetadataField";
import { Box, makeStyles } from "@material-ui/core";
import { FileMetadataInterface } from "@/interfaces/index";
import { fancyTimeFormat, getAudioDuration, isAudioFile } from "@/utils/utils";
import { useTranslation } from "next-i18next";

const useStyles = makeStyles(() => ({
  metadata: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
}));

export default function Metadata({
  producedBy,
  creator,
  language,
  country,
  lastModification,
  tags,
  fileId,
  mime,
  file,
}: FileMetadataInterface) {
  const [duration, setDuration] = useState("");
  const { t } = useTranslation("publications");
  const classes = useStyles();

  useEffect(() => {
    (async () => {
      if (isAudioFile(mime) && file) {
        const duration = await getAudioDuration(file);

        setDuration(`${fancyTimeFormat(duration)}min`);
      }
    })();
  }, [file, mime]);

  return (
    <Box mt={1.5} data-testid="file-metadata">
      {mime && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.type")}
          value={mime}
          className={classes.metadata}
        />
      )}

      {duration && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.duration")}
          value={duration}
          className={classes.metadata}
        />
      )}

      {producedBy && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.producedBy")}
          value={producedBy}
          className={classes.metadata}
        />
      )}

      {creator && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.creator")}
          value={creator}
          className={classes.metadata}
        />
      )}

      {language && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.language")}
          value={language}
          className={classes.metadata}
        />
      )}

      {country && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.country")}
          value={country}
          className={classes.metadata}
        />
      )}

      {Boolean(lastModification) && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.lastModification")}
          value={lastModification as string}
          className={classes.metadata}
        />
      )}

      {Boolean(tags) && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.tags")}
          value={tags as string}
          className={classes.metadata}
        />
      )}

      {Boolean(fileId) && (
        <MetaDataField
          label={t("fileManager.uploadFile.metadataLabels.fileID")}
          value={fileId as string}
          className={classes.metadata}
        />
      )}
    </Box>
  );
}
