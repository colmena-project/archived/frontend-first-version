import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core";
import { getFirstLettersOfTwoFirstNames } from "@/utils/utils";
import Text from "@/components/ui/Text";
import { RoomAvatarInterface } from "@/interfaces/index";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
  octagonWrap: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    "-webkit-clip-path": "polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)",
    clipPath: "polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)",
    backgroundColor: theme.palette.secondary.main,
    position: "relative",
    padding: 2,
  },
  octagonSubWrap: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    "-webkit-clip-path": "polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)",
    clipPath: "polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)",
    backgroundColor: "#fff",
    position: "relative",
    minHeight: "100%",
    minWidth: "100%",
  },
  octagon: {
    position: "relative",
    "-webkit-clip-path": "polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)",
    clipPath: "polygon(25% 5%, 75% 5%, 100% 50%, 75% 95%, 25% 95%, 0% 50%)",
    minHeight: "100%",
    minWidth: "100%",
    "& div": {
      position: "unset!important",
    },
    textAlign: "center",
    backgroundColor: theme.palette.secondary.main,
    display: "flex",
  },
  letters: {
    display: "flex",
    alignContent: "center",
    alignItems: "center",
    textAlign: "center",
    justifyContent: "space-around",
    color: "#000",
    fontWeight: 600,
    letterSpacing: 1,
    height: "100%",
    width: "100%",
  },
  avatar: {
    position: "relative",
  },
  editAvatar: {
    position: "absolute",
    bottom: "0",
    right: "0",
  },
  image: {
    background: "none",
    border: "none!important",
    width: "100%",
    height: "100%",
  },
}));

interface PolygonAvatarInterface
  extends Pick<
    RoomAvatarInterface,
    "width" | "height" | "name" | "url" | "fontSize" | "className"
  > {
  borderColor: string;
}

const OctagonAvatar = ({
  url,
  name,
  width,
  height,
  fontSize,
  className,
  borderColor,
}: PolygonAvatarInterface) => {
  const classes = useStyles();
  const lettersAvatar = getFirstLettersOfTwoFirstNames(name);

  return (
    <Box className={classes.avatar} style={{ height }}>
      <Box
        width={width}
        className={[classes.octagonWrap, className].join(" ")}
        style={{ backgroundColor: borderColor, height }}
      >
        <Box
          className={classes.octagonSubWrap}
          style={{ height: height ? height - 4 : undefined, padding: url ? 0 : 2 }}
        >
          <Box className={classes.octagon}>
            <Avatar
              variant="square"
              src={url || undefined}
              className={classes.image}
              alt={lettersAvatar}
            >
              <Text className={classes.letters} style={{ fontSize }}>
                {lettersAvatar}
              </Text>
            </Avatar>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default OctagonAvatar;

export const OctagonAvatarMemoized = React.memo(OctagonAvatar);
