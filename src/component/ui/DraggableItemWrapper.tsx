import { makeStyles } from "@material-ui/core";
import React from "react";
import { DraggableProvided, DraggableStateSnapshot } from "react-beautiful-dnd";
import classnames from "classnames";
import { objectToString } from "@/utils/objectToString";

const useClasses = makeStyles(() => ({
  draggableItem: {
    userSelect: "none",
  },
}));

const DraggableItemWrapper: React.FC<{
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  children: React.ReactNode;
}> = ({ provided, snapshot, children }) => {
  const classes = useClasses();

  const style = snapshot.isDragging ? objectToString(provided.draggableProps.style) : "";

  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      className={classnames(classes.draggableItem, style)}
    >
      {children}
    </div>
  );
};

export default DraggableItemWrapper;
