import React from "react";
import { Box, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    width: "100%",
    maxWidth: 800,
    marginBottom: theme.spacing(1),
    height: "100%",
    justifyContent: "flex-start",
  },
}));

type Props = {
  children: React.ReactNode;
};

export default function LayoutContainer({ children }: Props) {
  const classes = useStyles();

  return <Box className={classes.content}>{children}</Box>;
}
