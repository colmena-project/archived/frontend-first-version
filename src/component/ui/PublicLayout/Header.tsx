import React, { useCallback, useMemo, useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";
import SvgIcon from "@/components/ui/SvgIcon";
import IconButton from "@material-ui/core/IconButton";
import { PositionProps } from "@/types/*";
import { PositionEnum, TextVariantEnum, TextAlignEnum } from "@/enums/*";
import Text from "@/components/ui/Text";
import GeneralMenuDrawer from "./GeneralMenuDrawer";
import { useRouter } from "next/router";
import theme from "@/styles/theme";
import { createStyles, makeStyles } from "@material-ui/core";
import classNames from "classnames";
import { v4 as uuid } from "uuid";
import LogoSvg from "../../../../public/images/svg/colmena_logo_1612.svg";

const useStyles = makeStyles(() =>
  createStyles({
    appBar: {
      boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
      borderColor: "red",
    },
    heightBar: {
      height: 70,
    },
    toolbar: {
      display: "flex",
      borderColor: "green",
    },
    leftContent: {
      display: "flex",
      alignItems: "center",
      flexDirection: "row",
    },
    centerContent: {
      marginLeft: "0.5rem",
      display: "flex",
      flexGrow: 1,
      flexDirection: "row",
      width: "100%",
    },
    rightContent: {
      marginLeft: "0.5rem",
      display: "flex",
      flexDirection: "row",
    },
    logo: {
      width: "100%",
      display: "flex",
      justifyContent: "space-around",
      "& svg": {
        width: 120,
      },
    },
  }),
);
export interface HeaderInterface {
  title?: string | React.ReactNode;
  fontSizeTitle?: number;
  subtitle?: string | React.ReactNode;
  fontSizeSubtitle?: number;
  drawer?: boolean;
  back?: boolean;
  logo?: boolean;
  headerPosition?: PositionProps | undefined;
  templateHeader?: "variation1" | "variation2" | "variation3" | "variation4";
  leftExtraElement?: React.ReactNode | undefined;
  centerExtraElement?: React.ReactNode | undefined;
  rightExtraElement?: React.ReactNode | undefined;
  className?: string;
}

export const tplHeader = new Map();
tplHeader.set("variation1", {
  backgroundAppBar: "#fff",
  textColor: theme.palette.icon.main,
  subTitleColor: theme.palette.primary.light,
});
tplHeader.set("variation2", {
  backgroundAppBar: theme.palette.primary.main,
  textColor: theme.palette.primary.contrastText,
  subTitleColor: theme.palette.primary.contrastText,
});
tplHeader.set("variation3", {
  backgroundAppBar: theme.palette.variation7.dark,
  textColor: theme.palette.variation5.contrastText,
  subTitleColor: theme.palette.primary.contrastText,
});
tplHeader.set("variation4", {
  backgroundAppBar: "none",
  textColor: theme.palette.variation5.contrastText,
  subTitleColor: theme.palette.primary.contrastText,
});

function AppBarSys({
  title,
  fontSizeTitle = 20,
  subtitle,
  fontSizeSubtitle = 15,
  headerPosition = PositionEnum.FIXED,
  drawer = true,
  back = false,
  logo = true,
  templateHeader = "variation1",
  leftExtraElement = undefined,
  centerExtraElement = undefined,
  rightExtraElement = undefined,
}: HeaderInterface) {
  const classes = useStyles();
  const router = useRouter();

  const [openDrawer, setOpenDrawer] = useState(false);

  const backTo = useCallback(
    (backPage: string | null) => {
      if (backPage) router.push(backPage);
      else router.back();
    },
    [router],
  );

  const handleBack = useCallback(async () => {
    let backPage: null | string = null;
    const isChangedLanguage = await localStorage.getItem("isChangedLanguage");
    if (isChangedLanguage === "yes") {
      const pages = await localStorage.getItem("accessedPages");
      const pagesAc: string[] = JSON.parse(String(pages));
      // eslint-disable-next-line prefer-destructuring
      backPage = pagesAc[1] || null;
      await localStorage.setItem("isChangedLanguage", "no");
    }

    backTo(backPage);
  }, [backTo]);

  const leftElement = useMemo(() => {
    const elements = [];
    if (!back && drawer) {
      elements.push(
        <IconButton
          key="open-burguer-menu"
          edge="start"
          color="inherit"
          aria-label="menu"
          data-testid="open-burguer-menu"
          style={{ marginLeft: 4 }}
          onClick={() => setOpenDrawer(!openDrawer)}
        >
          <SvgIcon
            icon="burger_menu"
            htmlColor={tplHeader.get(templateHeader).textColor}
            fontSize="medium"
          />
        </IconButton>,
      );
    }

    if (back) {
      elements.push(
        <IconButton
          key="back-btn"
          className="rtl:-scale-x-100"
          edge="start"
          color="inherit"
          aria-label="back"
          data-testid="back-btn"
          onClick={handleBack}
        >
          <SvgIcon icon="back" htmlColor={tplHeader.get(templateHeader).textColor} />
        </IconButton>,
      );
    }

    if (leftExtraElement) {
      elements.push(leftExtraElement);
    }

    if (elements.length === 0) {
      return null;
    }

    return <Box className={classes.leftContent}>{elements}</Box>;
  }, [back, classes.leftContent, drawer, handleBack, leftExtraElement, openDrawer, templateHeader]);

  const centerElement = useMemo(() => {
    const elements = [];

    if (title) {
      elements.push(
        <Box
          display="flex"
          key="container-title"
          flexDirection="column"
          justifyContent="flex-start"
        >
          <Text
            variant={TextVariantEnum.H3}
            align={TextAlignEnum.LEFT}
            style={{
              fontSize: fontSizeTitle,
              color: tplHeader.get(templateHeader).textColor,
              fontWeight: 900,
            }}
          >
            {title}
          </Text>
          {subtitle !== "" && (
            <Text
              variant={TextVariantEnum.H3}
              align={TextAlignEnum.LEFT}
              style={{
                fontSize: fontSizeSubtitle,
                color: tplHeader.get(templateHeader).subTitleColor,

                paddingTop: 2,
              }}
            >
              {subtitle}
            </Text>
          )}
        </Box>,
      );
    } else if (logo) {
      return (
        <Box key={uuid()} className={classes.logo}>
          <LogoSvg />
        </Box>
      );
    } else {
      elements.push(
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="flex-start"
          key="center-container"
        />,
      );
    }

    if (centerExtraElement) {
      elements.push(centerExtraElement);
    }

    if (elements.length === 0) {
      return null;
    }

    return <Box className={classes.centerContent}>{elements}</Box>;
  }, [
    centerExtraElement,
    classes.centerContent,
    classes.logo,
    fontSizeSubtitle,
    fontSizeTitle,
    logo,
    subtitle,
    templateHeader,
    title,
  ]);

  const rightElement = useMemo(() => {
    const elements = [];

    if (rightExtraElement) {
      elements.push(rightExtraElement);
    }

    if (elements.length === 0) {
      return null;
    }

    return <Box className={classes.rightContent}>{elements}</Box>;
  }, [classes.rightContent, rightExtraElement]);

  return (
    <header>
      <AppBar
        position={headerPosition}
        elevation={0}
        className={classNames(classes.appBar, classes.heightBar)}
      >
        <Toolbar
          className={classNames(classes.toolbar, classes.heightBar)}
          style={{ background: tplHeader.get(templateHeader).backgroundAppBar }}
        >
          {leftElement && leftElement}
          {centerElement && centerElement}
          {rightElement && rightElement}
        </Toolbar>
      </AppBar>
      {(headerPosition === PositionEnum.FIXED || headerPosition === PositionEnum.ABSOLUTE) && (
        <Toolbar style={{ height: 70 }} />
      )}

      <GeneralMenuDrawer
        open={openDrawer}
        // onOpen={() => setOpenDrawer(true)}
        onClose={() => setOpenDrawer(false)}
      />
    </header>
  );
}

export default AppBarSys;
