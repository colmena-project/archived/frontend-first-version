/* eslint-disable react/button-has-type */
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { ListItemIcon, List, ListItem, ListItemText, Box } from "@material-ui/core";
import Drawer from "@material-ui/core/Drawer";
import { useTranslation } from "next-i18next";
import Link from "next/link";
import Divider from "@material-ui/core/Divider";

import { useRouter } from "next/router";
import SvgIcon from "@/components/ui/SvgIcon";
import { parseCookies } from "nookies";
import LogoSvg from "../../../../public/images/svg/colmena_logo_1612.svg";
import { isSubadminProfile } from "@/utils/permissions";
import { currentDirection } from "@/utils/i18n";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

type ListItemProps = {
  id: string;
  url?: string | undefined;
  icon: any;
  color?: string | undefined;
  title?: string;
  handleClick?: () => void | undefined;
};

const useStyles = makeStyles((theme) => ({
  icon: {
    minWidth: 40,
  },
  margin: {
    margin: theme.spacing(1),
  },
  list: {
    color: "#666",
    [theme.breakpoints.down("sm")]: {
      width: "80vw",
    },
    [theme.breakpoints.up("sm")]: {
      width: "40vw",
    },
  },
  listItem: {
    display: "flex",
    flexDirection: "row-reverse",
  },
  logoBox: {
    display: "flex",
    flexDirection: "row",
    marginLeft: 20,
    marginTop: 20,
    alignItems: "center",
  },
}));

type Props = {
  open: boolean;
  onOpen?: () => void;
  onClose: () => void;
};

function DrawerAux({ open, onClose }: Props) {
  const [language, setLanguage] = useState("");
  useEffect(() => {
    setLanguage(currentDirection());
  }, [language]);
  const classes = useStyles();
  const router = useRouter();
  const cookies = parseCookies();

  const langCookies = cookies.NEXT_LOCALE;
  const defaultLangRouter = router.defaultLocale;

  const installRoute = defaultLangRouter === langCookies ? "/install" : `/${langCookies}/install`;

  const { t } = useTranslation("drawer");

  const openExternalLink = (url: string) => {
    const anchor = document.createElement("a");
    anchor.setAttribute("href", `${publicRuntimeConfig.blog.baseUrl}/${url}`);
    anchor.setAttribute("target", "blank");
    anchor.click();
  };

  const iconColor = "#666";
  const iconSize = "medium";

  const menuArray = [
    {
      id: "blog",
      icon: <SvgIcon icon="blog" fontSize={iconSize} htmlColor={iconColor} />,
      title: t("blog"),
      handleClick: () => {
        window.open(`${publicRuntimeConfig.blog.baseUrl}`, "_blank");
      },
      onlyAdmin: false,
    },
    {
      id: "help",
      icon: <SvgIcon icon="help" fontSize={iconSize} htmlColor={iconColor} />,
      title: t("supportTitle"),
      handleClick: () => openExternalLink("help"),
      onlyAdmin: false,
    },
    {
      id: "medias",
      icon: <SvgIcon icon="medias" fontSize={iconSize} htmlColor={iconColor} />,
      title: t("medias"),
      handleClick: () => router.push("/public/publications/media"),
      onlyAdmin: false,
    },
    {
      id: "info",
      icon: <SvgIcon icon="info" fontSize={iconSize} htmlColor={iconColor} />,
      title: t("aboutMaia"),
      handleClick: () => openExternalLink("#about"),
      onlyAdmin: false,
    },
    {
      id: "contract",
      icon: <SvgIcon icon="contract" fontSize={iconSize} htmlColor={iconColor} />,
      title: t("termsOfUse"),
      handleClick: () => openExternalLink("terms"),
      onlyAdmin: false,
    },
  ];

  const getListItemButton = (
    id: string,
    icon: string,
    color?: string | undefined,
    title?: string,
  ): React.ReactNode => (
    <ListItem key={id} button>
      <ListItemIcon className={classes.icon}>{icon}</ListItemIcon>
      <ListItemText primary={title} primaryTypographyProps={{ style: { fontSize: 14 } }} />
    </ListItem>
  );

  const drawerMenu = (): React.ReactNode => (
    <Box
      dir={currentDirection()}
      role="presentation"
      onClick={onClose}
      onKeyDown={onClose}
      key="drawer"
      className={classes.list}
    >
      <Box className={classes.logoBox}>
        <div style={{ width: 200 }}>
          <LogoSvg />
        </div>
      </Box>
      <Divider light style={{ backgroundColor: "white", marginTop: 8 }} />
      <List component="nav" style={{ width: "100%" }}>
        <ListItem
          key="listItem"
          onClick={() => {
            window.location.href = installRoute;
          }}
          button
        >
          <ListItemIcon className={classes.icon}>
            <SvgIcon icon="download" fontSize={iconSize} htmlColor={iconColor} />
          </ListItemIcon>
          <ListItemText
            primary={t("downloadTitle")}
            primaryTypographyProps={{ style: { fontSize: 14 } }}
          />
        </ListItem>
        {menuArray
          .filter((item) => {
            if (item.onlyAdmin) {
              return isSubadminProfile();
            }
            return true;
          })
          .map((item: ListItemProps) => {
            const { id, icon, color, title, url, handleClick } = item;
            if (url)
              return (
                <div key={id}>
                  <Link href={url}>{getListItemButton(id, icon, color, title)}</Link>
                </div>
              );

            return (
              <Box onClick={handleClick} key={id}>
                {getListItemButton(id, icon, color, title)}
              </Box>
            );
          })}
      </List>
    </Box>
  );

  return (
    <Box>
      <Drawer anchor="left" open={open} onClose={onClose}>
        <Box style={{ flex: 1, backgroundColor: "white" }}>{drawerMenu()}</Box>
      </Drawer>
    </Box>
  );
}

export default DrawerAux;
