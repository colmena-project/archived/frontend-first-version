import React from "react";
import AppBar, { HeaderInterface } from "./Header";
import FooterDW from "@/components/ui/FooterDW";
import LayoutContent from "@/components/ui/PublicLayout/LayoutContainer";
import Emergency from "@/components/statefull/Emergency";
import { BoxContent, WrapperContainer } from "./styled";

interface LayoutInterface extends HeaderInterface {
  backgroundColor?: string;
  children: React.ReactNode;
  footer?: boolean;
  blur?: boolean;
}

function PublicLayout({
  backgroundColor = "#fff",
  children,
  className,
  blur,
  footer = true,
  ...props
}: LayoutInterface) {
  return (
    <>
      <Emergency position="right" />
      <WrapperContainer
        className={className}
        extraStyle={{
          backgroundColor,
          filter: blur ? "blur(2.5px)" : "none",
        }}
      >
        <BoxContent component="main">
          <AppBar {...props} />
          <LayoutContent>{children}</LayoutContent>
        </BoxContent>
        {footer && <FooterDW position="relative" />}
      </WrapperContainer>
    </>
  );
}

export default PublicLayout;
