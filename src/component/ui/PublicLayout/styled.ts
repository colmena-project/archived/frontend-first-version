import { styled } from "@material-ui/core/styles";
import Container from "@/components/ui/Container";
import Box from "@material-ui/core/Box";

export const WrapperContainer = styled(Container)(() => ({
  padding: 0,
  display: "flex",
  flexDirection: "column",
  height: "auto!important",
  minHeight: "100vh",
  justifyContent: "space-between",
}));

export const BoxContent = styled(Box)(() => ({
  margin: 0,
  padding: 0,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  height: "100%",
  flex: 1,
}));
