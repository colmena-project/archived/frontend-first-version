import React, { useState, useEffect, useCallback, useRef } from "react";
import { Field, FieldProps, Form, Formik } from "formik";
import themeRelease from "@/styles/theme";
import { Root, MyPaper, MyButton, CancelButton, MyInputBase } from "./styled";
import SearchIcon from "@material-ui/icons/Search";
import DeleteIcon from "@material-ui/icons/Delete";
// import classNames from "classnames";

type FormValues = {
  keyword: string;
};

type Props = {
  search: (keyword: string) => void;
  keyword?: string;
  placeholder?: string;
  onFocus?: () => void;
  onBlur?: () => void;
  cancel?: () => void;
  searchIntervalMs?: number;
  focus?: boolean;
};

export default function SearchInput({
  search,
  keyword = "",
  placeholder,
  onFocus,
  onBlur,
  cancel,
  searchIntervalMs = 600,
  focus = false,
}: Props) {
  const inputRef = useRef<HTMLInputElement>();
  // eslint-disable-next-line no-undef
  const [isOnFocus, setIsOnFocus] = useState(false);
  // eslint-disable-next-line no-undef
  const [intervalSearch, setIntervalSearch] = useState<undefined | NodeJS.Timeout>();
  const [submitType, setSubmitType] = useState<"search" | "clear">("search");
  const initialValues: FormValues = {
    keyword,
  };

  const handleSearch = (values: FormValues) => {
    if (intervalSearch) {
      clearTimeout(intervalSearch);
      setIntervalSearch(undefined);
    }
    search(values.keyword);
  };

  const handleIntervalSearch = (submitForm: any) => {
    if (intervalSearch) {
      clearTimeout(intervalSearch);
    }

    setIntervalSearch(undefined);
    setIntervalSearch(
      setTimeout(() => {
        submitForm();
      }, searchIntervalMs),
    );
  };

  useEffect(() => {
    let buttonType: "clear" | "search" = "search";
    if (keyword !== "") {
      buttonType = "clear";
    }

    setSubmitType(buttonType);
  }, [keyword]);

  const handleOnBlur = useCallback(
    (e) => {
      const { path } = e as any;
      const totalPaths = path.length;
      let blur = true;
      // eslint-disable-next-line no-plusplus
      for (let index = 0; index < totalPaths; index++) {
        const element = path[index];
        if (element.classList?.contains("ignore-blur")) {
          blur = false;
          break;
        }
      }

      if (blur && typeof onBlur === "function") {
        setIsOnFocus(false);
        onBlur();
      }
    },
    [onBlur],
  );

  useEffect(() => {
    if (isOnFocus) {
      document.body.addEventListener("click", handleOnBlur);
    } else {
      document.body.removeEventListener("click", handleOnBlur);
    }
  }, [onBlur, isOnFocus, handleOnBlur]);

  const handleOnFocus = () => {
    if (typeof onFocus === "function") {
      setIsOnFocus(true);
      onFocus();
    }
  };

  const cancelSearch = () => {
    if (cancel) cancel();
  };

  return (
    <Formik initialValues={initialValues} onSubmit={handleSearch} id="form">
      {({ setFieldValue, submitForm }: any) => {
        useEffect(() => {
          setFieldValue("keyword", keyword);
          // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [setFieldValue, keyword]);

        return (
          <Root>
            <Form style={{ width: "100%" }}>
              {/* <MyPaper component="div" className={classNames(classes.paper, "ignore-blur")}> */}
              <MyPaper component="div">
                {submitType === "search" && (
                  <MyButton
                    aria-label="search"
                    onClick={submitForm}
                    size="small"
                    style={{ color: themeRelease.palette.variation5.light }}
                  >
                    <SearchIcon />
                  </MyButton>
                )}
                {submitType === "clear" && (
                  <MyButton
                    onClick={() => {
                      setFieldValue("keyword", "");
                      setSubmitType("search");
                      handleIntervalSearch(submitForm);
                    }}
                    size="small"
                    style={{ color: themeRelease.palette.gray.main }}
                  >
                    <DeleteIcon />
                  </MyButton>
                )}
                <Field name="keyword" InputProps={{ notched: true }} onChange>
                  {({ field }: FieldProps) => (
                    <MyInputBase
                      ref={inputRef}
                      placeholder={placeholder}
                      data-testid="search-input"
                      value={field.value}
                      onChange={(
                        event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
                      ) => {
                        setFieldValue("keyword", event.target.value);
                        setSubmitType("search");
                        handleIntervalSearch(submitForm);
                      }}
                      onFocus={handleOnFocus}
                      autoFocus={focus}
                    />
                  )}
                </Field>
              </MyPaper>
            </Form>
            {cancel && (
              <CancelButton data-testid="cancel-button" onClick={cancelSearch}>
                Cancel
              </CancelButton>
            )}
          </Root>
        );
      }}
    </Formik>
  );
}

export const SearchInputMemoized = React.memo(SearchInput);
