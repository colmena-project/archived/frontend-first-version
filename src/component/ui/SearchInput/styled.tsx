import { styled } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import Box from "@material-ui/core/Box";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const Root = styled(Box)({
  display: "flex",
});

export const MyPaper = styled(Paper)(({ theme }: StyledProps) => ({
  padding: 2,
  display: "flex",
  alignItems: "center",
  width: "100%",
  background: theme.palette.gray.light,
  border: `1px solid ${theme.palette.variation5.light}`,
  borderRadius: 5,
  boxShadow: "none",
}));

export const MyButton = styled(IconButton)({
  padding: "10px 8px",
  margin: 0,
  minWidth: "auto",
});

export const CancelButton = styled(Button)(({ theme }: StyledProps) => ({
  color: theme.palette.variation3.main,
  textTransform: "initial",
  fontWeight: 600,
  fontSize: 12,
}));

export const MyInputBase = styled(InputBase)(({ theme }: StyledProps) => ({
  flex: 1,
  color: theme.palette.variation6.dark,
  fontSize: 12,
}));
