import { styled, Theme } from "@material-ui/core/styles";
import SvgIconAux from "@/components/ui/SvgIcon";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import Dialog from "@material-ui/core/Dialog";

export const WrapperDialog = styled(Dialog)({
  "& .MuiDialog-paper": {
    overflowY: "unset",
  },
});

export const ButtonCloseModal = styled(SvgIconAux)({
  position: "absolute",
  top: "-30px",
  right: 5,
  zIndex: 999,
});

export const Title = styled(MuiDialogTitle)({
  margin: 0,
  paddingBottom: 0,
  border: "none",
});

export const Subtitle = styled(MuiDialogTitle)(({ theme }: { theme: Theme }) => ({
  padding: theme.spacing(2),
  paddingTop: theme.spacing(1),
  paddingBottom: 0,
  "& h6": {
    color: theme.palette?.variation2?.main,
    fontSize: 12,
  },
}));

export const DialogContentStyle = styled(MuiDialogContent)(({ theme }: { theme: Theme }) => ({
  paddingTop: theme.spacing(2.5),
  paddingBottom: theme.spacing(2.5),
}));
