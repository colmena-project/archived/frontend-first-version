import React, { useEffect, useRef } from "react";
import { PathArea } from "@/components/ui/DirectoryPathArea/styled";
import { moveScrollToRight } from "@/utils/utils";

type Props = {
  path: string;
};

export default function DirectoryPathArea({ path }: Props) {
  const areaRef = useRef<HTMLDivElement>(null);

  useEffect(() => moveScrollToRight(areaRef), [areaRef.current?.scrollWidth]);

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return <PathArea ref={areaRef}>{path}</PathArea>;
}
