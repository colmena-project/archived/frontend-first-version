import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const PathArea = styled(Box)(({ theme }: StyledProps) => ({
  fontSize: theme.typography.body2.fontSize,
  overflow: "hidden",
  overflowX: "auto",
  whiteSpace: "nowrap",
  display: "block",
}));
