/* eslint-disable react/button-has-type */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { getSession, signOut } from "next-auth/client";
import Container from "@/components/ui/Container";
import FlexBox from "@/components/ui/FlexBox";
import { AppBarSysMemoized, AppBarInterface } from "@/components/statefull/AppBar";
import FooterApp from "@/components/ui/FooterApp";
import { PropsUserSelector } from "@/types/index";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";
import { setCurrentAudioPlaying } from "@/store/actions/library/index";
import { updateRecordingState } from "@/store/actions/recordings/index";
import { getAccessedPages, setAccessedPages } from "@/utils/utils";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

interface LayoutInterface extends AppBarInterface {
  showFooter?: boolean;
  backgroundColor?: string;
  children: React.ReactNode;
}

function LayoutApp({
  showFooter = true,
  backgroundColor = "#fff",
  children,
  className,
  ...props
}: LayoutInterface) {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const dispatch = useDispatch();
  const router = useRouter();
  const connectionStatus = useConnectionStatus();

  const updateAccessedPages = async () => {
    const pages = await getAccessedPages();
    if (pages.length > 0) {
      pages.unshift(router.asPath);
      const pagesAcR = [...new Set(pages)];
      await setAccessedPages(pagesAcR);
    } else {
      await setAccessedPages([router.asPath]);
    }
  };

  useEffect(() => {
    dispatch(setCurrentAudioPlaying(""));
    dispatch(updateRecordingState("NONE"));
    updateAccessedPages();
    if (connectionStatus) {
      (async () => {
        try {
          const session = await getSession();
          if (!session || !userRdx.user) {
            await signOut({ redirect: false });
            router.push("/login?out");
          }
        } catch (e) {
          console.log(e);
        }
      })();
    }
  }, []);

  return (
    <Container className={className} extraStyle={{ padding: 0, backgroundColor }}>
      <FlexBox extraStyle={{ margin: 0, padding: 0 }}>
        <AppBarSysMemoized {...props} />
        <> {children}</>
        {showFooter && <FooterApp />}
      </FlexBox>
    </Container>
  );
}

export default LayoutApp;
