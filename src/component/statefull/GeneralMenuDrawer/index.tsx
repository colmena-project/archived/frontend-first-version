/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/button-has-type */
import React, { useState, useEffect } from "react";
import { useTheme } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import Drawer from "@material-ui/core/Drawer";
import { useTranslation } from "next-i18next";
import Link from "next/link";
import { useRouter } from "next/router";
import { signOut } from "next-auth/client";
import Backdrop from "@/components/ui/Backdrop";
import SvgIcon from "@/components/ui/SvgIcon";
import { parseCookies } from "nookies";
import LogoSvg from "../../../../public/images/svg/colmena_logo_1612.svg";
import { currentDirection } from "@/utils/i18n";
import getConfig from "next/config";

import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";

import useMediaGroupAvatar from "@/hooks/cms/useMediaGroupAvatar";
import useCollaboratorAvatar from "@/hooks/cms/useCollaboratorAvatar";
import { getFirstLettersOfTwoFirstNames } from "@/utils/utils";
import {
  AvatarMaterialUI,
  BaseMenu,
  Divider,
  Information,
  LogoWrapper,
  ProfileLinkWrapper,
  SimpleLinkLabel,
  SimpleLinkWrapper,
  Subtitle,
  Title,
} from "@/components/statefull/GeneralMenuDrawer/styled";

const { publicRuntimeConfig } = getConfig();

type Props = {
  open: boolean;
  onOpen?: () => void;
  onClose: () => void;
};

function DrawerAux({ open, onClose }: Props) {
  const { t } = useTranslation("drawer");
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const usrId = userRdx.user.id;
  const mediaGroupId = userRdx.user.media.id;
  const mediaName = userRdx.user.media.name;

  const { thumbnail: avatar } = useMediaGroupAvatar(mediaGroupId);
  const { thumbnail: avatarCollaborator } = useCollaboratorAvatar(usrId);

  const theme = useTheme();
  const router = useRouter();
  const [showBackdrop, setShowBackdrop] = useState(false);
  const cookies = parseCookies();

  const langCookies = cookies.NEXT_LOCALE;
  const defaultLangRouter = router.defaultLocale;

  const installRoute = defaultLangRouter === langCookies ? "/install" : `/${langCookies}/install`;

  useEffect(() => {
    router.prefetch("/faq");
    router.prefetch("/help");
  }, []);

  const logoutHandler = async () => {
    if (navigator.onLine) {
      setShowBackdrop(true);
      await signOut({ redirect: false });
      setShowBackdrop(false);
      router.push("/login?out");
    }
  };

  const arraySimpleLink = [
    {
      key: "blog",
      onClick: () => {
        window.open(`${publicRuntimeConfig.blog.baseUrl}`, "_blank");
      },
      label: t("blog"),
      color: theme.palette.icon.main,
      icon: <SvgIcon htmlColor={theme.palette.icon.main} icon="blog" />,
    },
    {
      key: "aboutFAQ",
      onClick: () => router.push("/faq"),
      label: t("aboutFAQ"),
      color: theme.palette.icon.main,
      icon: <SvgIcon htmlColor={theme.palette.icon.main} icon="about_faq" />,
    },
    {
      key: "helpSupport",
      onClick: () => router.push("/help"),
      label: t("helpSupport"),
      color: theme.palette.icon.main,
      icon: <SvgIcon htmlColor={theme.palette.icon.main} icon="help_support" />,
    },
    {
      key: "termsConditions",

      onClick: () => {
        window.open(`${publicRuntimeConfig.blog.baseUrl}/terms`, "_blank");
      },
      label: t("termsConditions"),
      color: theme.palette.icon.main,
      icon: <SvgIcon htmlColor={theme.palette.icon.main} icon="terms_conditions" />,
    },
  ];

  const divider = (): React.ReactNode => (
    <Box style={{ padding: "0 20px" }}>
      <Divider />
    </Box>
  );

  const drawerMenu = (): React.ReactNode => (
    <BaseMenu
      component="nav"
      role="navigation"
      key="presentation"
      dir={currentDirection()}
      onClick={onClose}
      onKeyDown={onClose}
    >
      <LogoWrapper>
        <div style={{ width: 200 }}>
          <Link href="/home">
            <LogoSvg />
          </Link>
        </div>
      </LogoWrapper>

      {divider()}
      {profileLink()}
      {mediaLink()}
      {divider()}

      {simpleLink(
        "installAndUpdate",
        () => {
          window.location.href = installRoute;
        },
        t("installAndUpdate"),
        theme.palette.variation1.main,
        <SvgIcon htmlColor={theme.palette.variation1.main} icon="install_app" />,
      )}

      {divider()}

      {arraySimpleLink.map(({ key, onClick, label, color, icon }) =>
        simpleLink(key, onClick, label, color, icon),
      )}

      {divider()}

      {simpleLink(
        "logoutTitle",
        () => logoutHandler(),
        t("logoutTitle"),
        theme.palette.variation3.main,
        <SvgIcon htmlColor={theme.palette.variation3.main} icon="logout" fontSize={30} />,
      )}
    </BaseMenu>
  );

  const simpleLink = (
    key: string | number,
    onClick: () => void | Window | any,
    label: string,
    color: string,
    icon: React.ReactNode,
  ): React.ReactNode => (
    <SimpleLinkWrapper key={key}>
      <Box width={60} justifyContent="center" display="flex" onClick={onClick}>
        {icon}
      </Box>
      <SimpleLinkLabel style={{ color }} onClick={onClick}>
        {label}
      </SimpleLinkLabel>
    </SimpleLinkWrapper>
  );

  const profileLink = (): React.ReactNode => (
    <Link href="/profile">
      <ProfileLinkWrapper style={{ padding: "10px 20px" }}>
        <Box>
          <AvatarMaterialUI
            alt={`User avatar ${userRdx.user.name}`}
            src={avatarCollaborator ?? undefined}
          >
            {getFirstLettersOfTwoFirstNames(userRdx.user.name)}
          </AvatarMaterialUI>
        </Box>
        <Information>
          <Title>{t("profile.title")}</Title>
          <Subtitle>{t("profile.subtitle")}</Subtitle>
        </Information>
      </ProfileLinkWrapper>
    </Link>
  );

  const mediaLink = (): React.ReactNode => (
    <Link href="/media-profile">
      <ProfileLinkWrapper marginTop="10px">
        <Box>
          <AvatarMaterialUI alt={`Media avatar ${mediaName}`} src={avatar ?? undefined} />
        </Box>
        <Information>
          <Title>{t("media.title")}</Title>
          <Subtitle>{t("media.subtitle")}</Subtitle>
        </Information>
      </ProfileLinkWrapper>
    </Link>
  );

  return (
    <Box>
      <Backdrop open={showBackdrop} />
      <Drawer anchor="left" open={open} onClose={onClose}>
        <Box style={{ flex: 1, backgroundColor: "white" }}>{drawerMenu()}</Box>
      </Drawer>
    </Box>
  );
}

export default DrawerAux;
