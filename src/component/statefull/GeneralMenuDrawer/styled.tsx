import { styled } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { Box } from "@material-ui/core";
import Text from "@/components/ui/Text";
import DividerBase from "@material-ui/core/Divider";
import { Theme } from "@material-ui/core/styles/createTheme";

type StyledProps = {
  theme: Theme;
};

export const BaseMenu = styled(Box)(({ theme }: StyledProps) => ({
  color: "#666",
  [theme.breakpoints.down("sm")]: {
    width: "80vw",
  },
  [theme.breakpoints.up("sm")]: {
    width: "40vw",
  },
}));

export const AvatarMaterialUI = styled(Avatar)(({ theme }: StyledProps) => ({
  width: theme.spacing(6),
  height: theme.spacing(6),
}));

export const LogoWrapper = styled(Box)({
  display: "flex",
  flexDirection: "row",
  marginLeft: 20,
  marginTop: 20,
  alignItems: "center",
  cursor: "pointer",
});

export const ProfileLinkWrapper = styled(Box)({
  padding: "0 20px",
  display: "flex",
  cursor: "pointer",
});

export const Title = styled(Text)(({ theme }: StyledProps) => ({
  fontStyle: "normal",
  fontWeight: 400,
  fontSize: "14px",
  lineHeight: "14px",
  display: "flex",
  alignItems: "center",
  color: theme.palette.primary.main,
}));

export const Subtitle = styled(Text)(({ theme }: StyledProps) => ({
  fontStyle: "normal",
  fontWeight: 400,
  fontSize: "8px",
  lineHeight: "14px",
  display: "flex",
  alignItems: "center",
  color: theme.palette.gray.main,
}));

export const SimpleLinkWrapper = styled(Box)({
  padding: "15px 20px 5px 20px",
  display: "flex",
  cursor: "pointer",
});

export const SimpleLinkLabel = styled(Text)({
  fontStyle: "normal",
  fontWeight: 400,
  fontSize: "14px",
  lineHeight: "14px",
  display: "flex",
  alignItems: "center",
});

export const Information = styled(Box)({
  display: "flex",
  flexDirection: "column",
  paddingLeft: "20px",
  justifyContent: "center",
});

export const Progress = styled(Box)(({ theme }: StyledProps) => ({
  display: "flex",
  height: "7px",
  background: "#FDD8D0",
  marginTop: "10px",

  "& div": {
    display: "flex",
    height: "7px",
    background: theme.palette.variation3.main,
  },
}));

export const Divider = styled(DividerBase)({
  marginTop: 8,
  borderBottom: "1px solid #C4C4C4",
});
