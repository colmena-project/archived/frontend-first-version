import { useTheme } from "@material-ui/core";
import SvgIcon from "@/components/ui/SvgIcon";
import Backdrop from "@/components/ui/Backdrop";

import { WrapperEmergency } from "./styled";
import useEmergency from "@/hooks/useEmergency";
import { useTranslation } from "next-i18next";
import ActionConfirm from "@/components/ui/ActionConfirm";
import useUsernameLocalStorage from "@/hooks/useUsernameLocalStorage";

export interface EmergencyInterface {
  position?: "top" | "right";
}

function Emergency({ position = "right" }: EmergencyInterface) {
  const { username } = useUsernameLocalStorage();
  const theme = useTheme();
  const { start, showBackdrop, isLoading, toggleConfirmModal, open } = useEmergency();
  const { t } = useTranslation("common");

  if (!username) {
    return null;
  }

  return (
    <>
      <Backdrop open={showBackdrop} />
      <WrapperEmergency
        role="navigation"
        data-testid="emergency-test-id"
        position={position}
        onClick={() => toggleConfirmModal()}
      >
        <SvgIcon fontSize={18} icon="emergency" htmlColor={theme.palette.primary.contrastText} />
        {t("emergency")}
      </WrapperEmergency>

      {open && (
        <ActionConfirm
          data-testid="emergency-delete-confirm"
          onOk={start}
          onClose={toggleConfirmModal}
          isLoading={isLoading}
          showMessage
          title={t("confirmationEmergenncyTitle")}
          message={t("confirmationEmergenncyMessage")}
        />
      )}
    </>
  );
}

export default Emergency;
