import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles/createTheme.d";

import { CreateCSSProperties } from "@material-ui/core/styles/withStyles";

interface ModifierFunctions {
  top: () => void;
  right: () => void;
}

interface Modifiers {
  [key: string]: () => void;
}

interface WrapperEmergencyInterface {
  position: keyof ModifierFunctions;
  theme: Theme;
}

export const WrapperEmergency = styled(Box)(({ position, theme }: WrapperEmergencyInterface) => {
  const styles: CreateCSSProperties<WrapperEmergencyInterface> = {
    display: "flex",
    position: "fixed",
    zIndex: 9999,
    padding: "5px 10px",
    color: theme.palette.primary.contrastText,
    lineHeight: "19px",
    fontSize: "14px",
    cursor: "pointer",
    fontStyle: "normal",
    fontWeight: 700,
    background: "linear-gradient(180deg, #FF7659 0%, #FB5B39 100%)",
    gap: 10,
  };

  const modifiers: Modifiers = {
    top: () => {
      styles.boxShadow = "0px 4px 4px rgba(0, 0, 0, 0.25)";
      styles.borderRadius = "0px 0px 5px 5px";
      styles.top = 0;
      styles.left = "50%";
      styles.transform = "translateX(-50%)";
    },
    right: () => {
      styles.transform = "rotate(-90deg)";
      styles.marginRight = "-45px";
      styles.borderRadius = "5px 5px 0px 0px";
      styles.right = 0;
      styles.top = "50%";
    },
  };

  if (position in modifiers) modifiers[position]();

  return styles;
});
