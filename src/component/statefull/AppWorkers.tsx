import useUploadPublicationFiles from "@/hooks/useUploadPublicationFiles";
import { CurrentPublication } from "@/interfaces/index";
import { getNotSentPublications } from "@/store/idb/models/publications";
import { useCallback, useEffect } from "react";

const AppWorkers = () => {
  const { worker, runWorker } = useUploadPublicationFiles();

  const fetchNotSentPublications = useCallback(async () => {
    if (worker) {
      const publications: CurrentPublication[] = await getNotSentPublications();
      publications.forEach((publication) => {
        const { cmsMode, id: publicationId } = publication;

        if (cmsMode && publicationId) {
          runWorker({ cmsMode, publicationId });
        }
      });
    }
  }, [worker, runWorker]);

  useEffect(() => {
    fetchNotSentPublications();
  }, [fetchNotSentPublications]);

  return <></>;
};

export default AppWorkers;
