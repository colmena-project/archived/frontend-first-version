import { styled } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

export const MyAppBar = styled(AppBar)({
  boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
  borderColor: "red",
  height: 70,
});

export const MyToolbar = styled(Toolbar)({
  display: "flex",
  borderColor: "green",
  height: 70,
});

export const LeftContentBox = styled(Box)({
  display: "flex",
  alignItems: "center",
  flexDirection: "row",
});

export const RightContentBox = styled(Box)({
  display: "flex",
  flexDirection: "row",
  gap: 10,
  alignItems: "center",
});

export const CenterContentBox = styled(Box)({
  marginLeft: "0.5rem",
  display: "flex",
  flexGrow: 1,
  flexDirection: "row",
  overflow: "hidden",
});

export const ActivatorBox = styled(Box)({
  position: "absolute",
  left: "50%",
  top: "20px",
  width: "10px",
  height: "10px",
  background: "red",
  cursor: "pointer",
  borderRadius: "50%",
  animation: "flash 3500ms linear 0s infinite normal forwards",
});
