import React, { useCallback, useMemo, useState } from "react";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";
import SvgIcon from "@/components/ui/SvgIcon";
import IconButton from "@material-ui/core/IconButton";
import { PositionProps, PropsRecordingSelector, PropsTransferSelector } from "@/types/index";
import { PositionEnum, TextVariantEnum, TextAlignEnum, TransferStatusEnum } from "@/enums/index";
import Text from "@/components/ui/Text";
import { useSelector, useDispatch } from "react-redux";
import GeneralMenuDrawer from "../GeneralMenuDrawer";
import { useRouter } from "next/router";
import theme from "@/styles/theme";
import { updateRecordingState } from "@/store/actions/recordings/index";
import { setOpenTransferModal } from "@/store/actions/transfers/index";
import { setBackAfterFinishRecording } from "@/utils/utils";
import { TransferItemInterface } from "@/interfaces/index";
import MainTitle from "@/components/ui/MainTitle";
import Notifications from "../../ui/Notifications";
import { useTour } from "@reactour/tour";
import {
  ActivatorBox,
  MyAppBar,
  MyToolbar,
  LeftContentBox,
  RightContentBox,
  CenterContentBox,
} from "./styled";
import Emergency from "../Emergency";

export interface AppBarInterface {
  title: string | React.ReactNode;
  fontSizeTitle?: number;
  subtitle?: string | React.ReactNode;
  fontSizeSubtitle?: number;
  drawer?: boolean;
  tuor?: boolean;
  back?: boolean;
  notifications?: boolean;
  headerPosition?: PositionProps | undefined;
  templateHeader?: "variation1" | "variation2" | "variation3" | "variation4";
  leftExtraElement?: React.ReactNode | undefined;
  centerExtraElement?: React.ReactNode | undefined;
  rightExtraElement?: React.ReactNode | undefined;
  showUploadProgress?: boolean;
  className?: string;
  dataTutTitle?: string;
}

export const tplHeader = new Map();
tplHeader.set("variation1", {
  backgroundAppBar: "#fff",
  textColor: theme.palette.icon.main,
  subTitleColor: theme.palette.primary.light,
});
tplHeader.set("variation2", {
  backgroundAppBar: theme.palette.primary.main,
  textColor: theme.palette.primary.contrastText,
  subTitleColor: theme.palette.primary.contrastText,
});
tplHeader.set("variation3", {
  backgroundAppBar: theme.palette.variation7.dark,
  textColor: theme.palette.variation5.contrastText,
  subTitleColor: theme.palette.primary.contrastText,
});
tplHeader.set("variation4", {
  backgroundAppBar: "none",
  textColor: theme.palette.variation5.contrastText,
  subTitleColor: theme.palette.primary.contrastText,
});

function AppBarSys({
  title,
  fontSizeTitle = 20,
  subtitle,
  fontSizeSubtitle = 15,
  headerPosition = PositionEnum.FIXED,
  drawer = true,
  tuor = false,
  notifications = true,
  back = false,
  templateHeader = "variation1",
  leftExtraElement = undefined,
  centerExtraElement = undefined,
  rightExtraElement = undefined,
  showUploadProgress = true,
  dataTutTitle,
}: AppBarInterface) {
  const router = useRouter();
  const { setIsOpen, isOpen } = useTour();
  const recordingRdx = useSelector(
    (state: { recording: PropsRecordingSelector }) => state.recording,
  );
  const transferRdx = useSelector((state: { transfer: PropsTransferSelector }) => state.transfer);
  const dispatch = useDispatch();

  const [openDrawer, setOpenDrawer] = useState(false);

  const backTo = useCallback(
    (backPage: string | null) => {
      if (backPage) router.push(backPage);
      else router.back();
    },
    [router],
  );

  const handleBack = useCallback(async () => {
    let backPage: null | string = null;
    const isChangedLanguage = await localStorage.getItem("isChangedLanguage");
    if (isChangedLanguage === "yes") {
      const pages = await localStorage.getItem("accessedPages");
      const pagesAc: string[] = JSON.parse(String(pages));
      // eslint-disable-next-line prefer-destructuring
      backPage = pagesAc[1] || null;
      await localStorage.setItem("isChangedLanguage", "no");
    }

    if (router.pathname === "/record") {
      if (recordingRdx.activeRecordingState === "RECORDING") {
        setBackAfterFinishRecording("yes");
        dispatch(updateRecordingState("STOPPED"));
      } else {
        backTo(backPage);
      }

      return;
    }

    if (router.pathname === "/publications/management") {
      const page3regex = /\/publications\/\d/i;

      const pages = await localStorage.getItem("accessedPages");

      const prevPage = JSON.parse(String(pages)).pop();

      if (page3regex.test(prevPage)) {
        backTo("/home");

        return;
      }
    }

    backTo(backPage);
  }, [backTo, dispatch, recordingRdx.activeRecordingState, router.pathname]);

  const transferWorkInProgress = transferRdx.files.some(
    (item: TransferItemInterface) => item.status === TransferStatusEnum.IN_PROGRESS,
  );

  const leftElement = useMemo(() => {
    const elements = [];

    if (!back && drawer) {
      elements.push(
        <IconButton
          key="open-burguer-menu"
          edge="start"
          color="inherit"
          aria-label="menu"
          data-testid="open-burguer-menu"
          style={{ marginLeft: 4 }}
          onClick={() => setOpenDrawer(!openDrawer)}
        >
          <SvgIcon
            icon="burger_menu"
            htmlColor={tplHeader.get(templateHeader).textColor}
            fontSize="medium"
          />
        </IconButton>,
      );
    }

    if (back) {
      elements.push(
        <IconButton
          key="back-btn"
          className="rtl:-scale-x-100"
          edge="start"
          color="inherit"
          aria-label="back"
          data-testid="back-btn"
          onClick={handleBack}
        >
          <SvgIcon icon="back" htmlColor={tplHeader.get(templateHeader).textColor} />
        </IconButton>,
      );
    }

    if (leftExtraElement) {
      elements.push(leftExtraElement);
    }

    if (elements.length === 0) {
      return null;
    }

    return <LeftContentBox>{elements}</LeftContentBox>;
  }, [back, drawer, handleBack, leftExtraElement, openDrawer, templateHeader]);

  const centerElement = useMemo(() => {
    const elements = [];

    if (title) {
      elements.push(
        <Box
          display="flex"
          key="container-title"
          flexDirection="column"
          justifyContent="flex-start"
          width="100%"
        >
          {typeof title === "string" ? (
            <MainTitle
              textColor={tplHeader.get(templateHeader).textColor}
              fontSizeTitle={fontSizeTitle}
              title={title}
              dataTutTitle={dataTutTitle}
            />
          ) : (
            title
          )}
          {subtitle && (
            <Text
              variant={TextVariantEnum.H2}
              align={TextAlignEnum.LEFT}
              style={{
                fontSize: fontSizeSubtitle,
                color: tplHeader.get(templateHeader).subTitleColor,

                paddingTop: 2,
                maxWidth: "100%",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
              }}
            >
              {subtitle}
            </Text>
          )}
        </Box>,
      );
    } else {
      elements.push(
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="flex-start"
          key="center-container"
        />,
      );
    }

    if (centerExtraElement) {
      elements.push(centerExtraElement);
    }

    if (elements.length === 0) {
      return null;
    }

    return <CenterContentBox>{elements}</CenterContentBox>;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fontSizeSubtitle, fontSizeTitle, subtitle, templateHeader, title, dataTutTitle]);

  const rightElement = useMemo(() => {
    const elements = [];

    if (notifications) {
      elements.push(
        <Box key="notifications">
          <Notifications />
        </Box>,
      );
    }

    if (rightExtraElement) {
      elements.push(rightExtraElement);
    }

    if (transferWorkInProgress && showUploadProgress) {
      elements.push(
        <IconButton
          key="transfer-btn"
          edge="start"
          color="inherit"
          aria-label="transfer"
          data-testid="open-transfer-modal"
          style={{ marginLeft: 2 }}
          onClick={() => dispatch(setOpenTransferModal(true))}
        >
          <SvgIcon
            icon="transfer"
            htmlColor={tplHeader.get(templateHeader).subTitleColor}
            fontSize="medium"
          />
        </IconButton>,
      );
    }

    if (elements.length === 0) {
      return null;
    }

    return <RightContentBox>{elements}</RightContentBox>;
  }, [
    dispatch,
    rightExtraElement,
    showUploadProgress,
    templateHeader,
    transferWorkInProgress,
    notifications,
  ]);

  const tuorElement = <ActivatorBox onClick={() => setIsOpen(true)}></ActivatorBox>;
  return (
    <Box>
      <Emergency position="right" />
      <MyAppBar position={headerPosition} elevation={0}>
        <MyToolbar style={{ background: tplHeader.get(templateHeader).backgroundAppBar }}>
          {leftElement && leftElement}
          {centerElement && centerElement}
          {tuor && !isOpen && tuorElement}
          {rightElement && rightElement}
        </MyToolbar>
      </MyAppBar>
      {(headerPosition === PositionEnum.FIXED || headerPosition === PositionEnum.ABSOLUTE) && (
        <Toolbar style={{ height: 70 }} />
      )}

      <GeneralMenuDrawer
        open={openDrawer}
        // onOpen={() => setOpenDrawer(true)}
        onClose={() => setOpenDrawer(false)}
      />
    </Box>
  );
}

export default AppBarSys;

export const AppBarSysMemoized = React.memo(AppBarSys);
