export interface CategoryQueryInterface {
  id: string;
  attributes: {
    name: string;
    language: string;
  };
}

export interface CategoryQueryCollectionInterface {
  categories: {
    data: Array<CategoryQueryInterface>;
  };
}

export interface SubjectInterface {
  data: CategoryQueryInterface;
}

export interface MediaInterfaceFormatsFileInterface {
  ext: string;
  hash: string;
  width: number;
  height: number;
  mime: string;
  name: string;
  path: null | string;
  size: number;
  url: string;
}

export interface MediaFormatsInterface {
  large?: MediaInterfaceFormatsFileInterface;
  medium?: MediaInterfaceFormatsFileInterface;
  small?: MediaInterfaceFormatsFileInterface;
  thumbnail: MediaInterfaceFormatsFileInterface;
}

export interface MediaInterface {
  data: {
    id: number;
    attributes: {
      name: string;
      alternativeText: string;
      createdAt: string;
      updatedAt: string;
      formats: MediaFormatsInterface;
      url: string;
      ext: string;
      size: string;
      mime: string;
    };
  };
}

export interface PublicationQueryInterface {
  id: string;
  attributes: {
    title: string;
    description: string;
    coverage: string;
    publisher: string;
    content: string;
    rights: string;
    creator: string;
    subject: SubjectInterface;
    tags: string;
    files: { data: PublicationFileQueryInterface[] };
    views: number;
    relation: MediaInterface;
    publishedAt: string;
    createdAt: string;
    date: string;
    disapprovedAt?: string;
    disapprovalReason?: string;
    language: string;
  };
}

export interface PublicationQueryCollectionInterface {
  publications: {
    data: Array<PublicationQueryInterface>;
    meta: {
      pagination: {
        page: number;
        pageCount: number;
        pageSize: number;
        total: number;
      };
    };
  };
}

export interface SinglePublicationInterface {
  publication: {
    data: PublicationQueryInterface;
  };
}

export interface CollaboratorQueryCollectionInterface {
  collaborators: {
    data: CollaboratorInterface[];
  };
}

export interface CollaboratorInterface {
  id: number;
  attributes: {
    username: string;
    avatar: MediaInterface;
  };
}

export interface RoomQueryCollectionInterface {
  rooms: {
    data: RoomInterface[];
  };
}

export interface RoomInterface {
  id: number;
  attributes: {
    token: string;
    avatar: MediaInterface;
  };
}

export interface PublicationFileQueryInterface {
  id: string;
  attributes: {
    title: string;
    description: string;
    language: string;
    format: string;
    // eslint-disable-next-line camelcase
    extent_size: number;
    // eslint-disable-next-line camelcase
    extent_duration: number;
    creator: string;
    tags: string;
    downloads: number;
    media: MediaInterface;
    position?: number;
  };
}

export interface PublicationFileQueryCollectionInterface {
  publicationFiles: {
    data: Array<PublicationFileQueryInterface>;
  };
}

export interface MediaGroupQueryCollectionInterface {
  mediaGroups: {
    data: MediaGroupInterface[];
  };
}

export interface MediaGroupInterface {
  id: number;
  attributes: {
    token: string;
    avatar: MediaInterface;
  };
}

export interface UpdatePublicationPayloadInterface {
  title: string;
  description: string;
  coverage: string;
  date: string;
  subject: string;
  rights: string;
  content: string;
  relation: string;
}
