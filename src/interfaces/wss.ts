interface UserWS {
  identity: string;
  id: string;
  socketId: string;
  roomId: string;
}

export interface RoomInterface {
  data: {
    roomExists: boolean;
    connectedUsers?: UserWS[];
  };
}

export interface SocketIdInterface {
  data: {
    user: UserWS | null;
  };
}

export interface UsersRoomInterface {
  data: {
    user: UserWS[];
  };
}
