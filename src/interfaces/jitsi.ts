export interface DataJitsiInterface {
  aud: string;
  context: {
    user: UserInfoJitsiInterface;
    features?: {
      livestreaming?: boolean;
      "outbound-call"?: boolean;
      transcription?: boolean;
      recording?: boolean;
    };
  };
  exp: number;
  iss: string;
  room: string;
  sub: string;
}

export interface UserInfoJitsiInterface {
  id: string;
  name: string;
  avatar?: string;
  email: string;
  moderator: boolean;
}
