/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable camelcase */

import { NotificationAppEnum } from "../enums";

export interface FullNotificationQueryResponseInterface {
  ocs: {
    meta: {
      status: string;
      statuscode: number;
      message: string;
    };
    data: NotificationInterface[];
  };
}

export interface NotificationInterface {
  notification_id: number | string;
  app: NotificationAppEnum.SPREED | NotificationAppEnum.FILES_SHARING;
  user: string;
  datetime: string;
  object_type: string;
  object_id: string;
  subject: string;
  message: string;
  link: string;
  actions: NotificationActionsInterface[];
}

interface NotificationActionsInterface {
  label: string;
  link: string;
  type: string;
  primary: boolean;
}
