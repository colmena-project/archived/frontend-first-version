/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable camelcase */

export interface KeycloakAccessTokenInterface {
  sub: string;
  resource_access: {
    [key: string]: {
      roles: string[];
    };
  };
  address?: {
    region?: string;
    country?: string;
  };
  name: string;
  groups: string[];
  language: string;
  email: string;
  position: string;
  phone: string;
}

export interface GroupAttributesInterface {
  slogan: string[];
  email: string[];
  url: string[];
  language: string[];
  country?: string[];
  audio_description_url: string[];
  social_media_whatsapp: string[];
  social_media_twitter: string[];
  social_media_facebook: string[];
  social_media_mastodon: string[];
  social_media_instagram: string[];
  social_media_telegram: string[];
}

export interface GroupInterface {
  id: string;
  name: string;
  attributes: GroupAttributesInterface;
}

export interface GroupsInterface {
  data: GroupInterface[];
  success: boolean;
}
