/* eslint-disable camelcase */
// eslint-disable-next-line import/no-cycle
import { AllPaletteColors } from "@/styles/theme";
import {
  NotificationStatusProps,
  NXTagsProps,
  Environment,
  AllIconProps,
  StatusTransferItemProps,
  FontSizeIconProps,
  ButtonSizeProps,
  PublicationStatusProps,
} from "@/types/index";
import { ButtonProps } from "@material-ui/core";
import React from "react";
import {
  ButtonVariantEnum,
  ContextMenuEventEnum,
  ContextMenuOptionEnum,
  ListTypeEnum,
  PlayerTypeEnum,
  RoleUserEnum,
  WizardFeaturesEnum,
} from "../enums";
import { MediaInterface } from "@/interfaces/cms";

export interface I18nInterface {
  locale: string;
}

export interface PublicationStaticPropsInterface extends I18nInterface {
  params: {
    id: string;
  };
}

export interface RecordingInterface {
  id: string;
  title?: string;
  arrayBufferBlob: ArrayBuffer;
  blob: Blob;
  audioUrl: string;
  tags: NXTagsProps[] | string[];
  audioType: string;
  createdAt?: Date;
  updatedAt?: Date;
  userId: string | number;
}
export interface NotificationDataInterface {
  message: string;
  status: NotificationStatusProps;
}

export interface TransferItemInterface {
  tempFilename: string;
  filename: string;
  status: StatusTransferItemProps;
  userId: string;
}

export interface UserProfileInterface {
  medias: string[];
  avatar: string;
  social_medias: string[];
}

export interface MediaInfoInterface {
  id: string;
  name: string;
  slogan: string;
  email: string;
  url: string;
  language: string;
  audio_description_url: string;
  social_media_whatsapp: string;
  social_media_twitter: string;
  social_media_facebook: string;
  social_media_mastodon: string;
  social_media_instagram: string;
  social_media_telegram: string;
}

export interface SocialMediaInfoInterface {
  name: string;
  url: string;
}

export interface UserQuotaInterface {
  free: number;
  used: number;
  total: number;
  relative: number;
  quota: number;
}

export interface UserInfoInterface {
  id: string;
  name: string;
  email: string;
  language: string;
  country: string;
  region: string;
  role: RoleUserEnum.ADMIN | RoleUserEnum.COLLABORATOR;
  position?: string;
  phone?: string;
  keycloakToken: string;
  nexcloudToken: string;
  media: MediaInfoInterface;
}

export interface FileInterface {
  id: number;
  name: string;
  category: {
    id: number;
    name: string;
  };
}

export interface GenericHorizontalItemInterface {
  id?: number;
  srcImg: string;
  title: string;
  subtitle: string;
  url: string;
}

// export interface LibraryItemWebDavInterface {
//   basename: string;
//   filename: string;
//   type?: string;
//   size?: number;
//   lastmod?: string;
//   mime?: string;
// }
export interface LibraryItemWebDavInterface {
  filename: string;
  basename: string;
  type: string;
  size: number;
  lastmod: string;
}

export interface LibraryItemOCSUserInterface {
  id: string;
  address: string;
  displayname: string;
  email: string;
  enable: boolean;
  lastmod: string;
  language: string;
}
export interface UserInvitationInterface {
  username: string;
  name?: string;
  sub: number;
  role: string;
  media: {
    id: number;
    name: string;
    description: string;
    image: string;
    status: string;
    createdAt: string;
    updatedAt: string;
  };
  iat: number;
  exp: number;
}
export interface LibraryInterface {
  listType?: ListTypeEnum;
  items?: LibraryItemInterface[];
  isLoading?: boolean;
  isDisabled?: boolean;
  options?: (cardItem: LibraryCardItemInterface) => React.ReactNode;
  bottomOptions?: (
    cardItem: LibraryCardItemInterface,
    badgeStatusGrid?: React.ReactNode | undefined,
  ) => React.ReactNode;
  handleItemClick?: (item: LibraryItemInterface) => void;
  itemsQuantitySkeleton?: number;
  playerType?: PlayerTypeEnum;
}
export interface LibraryItemInterface {
  id: string;
  filename: string;
  aliasFilename: string;
  basename: string;
  extension?: string | undefined;
  type?: string;
  tags?: NXTagsProps[] | string[];
  arrayBufferBlob?: ArrayBuffer;
  createdAt?: Date;
  createdAtDescription?: string | null;
  updatedAt?: Date;
  updatedAtDescription?: string | null;
  environment: Environment;
  path?: string;
  image?: string;
  mime?: string;
  size?: number;
  sizeFormatted?: string;
  contentLength?: number;
  ownerId?: string;
  ownerName?: string;
  title?: string;
  description?: string;
  language?: string;
  favorite?: number;
  commentsUnread?: number;
  fileId?: number;
  nextcloudId?: string;
  eTag?: string;
}

export interface LibraryCardItemInterface extends LibraryItemInterface {
  orientation: string | ["vertical", "horizontal"];
  options?: (item: LibraryItemInterface) => React.ReactNode;
  bottomOptions?: (
    item: LibraryItemInterface,
    badgeStatusGrid?: React.ReactNode | undefined,
  ) => React.ReactNode;
  handleOpenCard?: (item: LibraryItemInterface) => void;
  isDisabled?: boolean;
  subtitle?: string;
  playerType?: PlayerTypeEnum;
}

export interface LibraryItemContextMenuInterface extends LibraryItemInterface {
  availableOptions: Array<ContextMenuOptionEnum>;
  onChange: (
    cardItem: LibraryItemInterface,
    event: ContextMenuEventEnum,
    option: ContextMenuOptionEnum,
    extraInfo?: any,
  ) => void | Promise<void>;
}

export interface BreadcrumbItemInterface {
  description: string | undefined;
  path: string;
  isCurrent?: boolean;
  icon?: AllIconProps | undefined;
}

export interface TimeDescriptionInterface {
  singularYear: string;
  pluralYear: string;
  singularMonth: string;
  pluralMonth: string;
  singularHour: string;
  pluralHour: string;
  singularDay: string;
  pluralDay: string;
  singularMinute: string;
  pluralMinute: string;
  now: string;
}

export interface VerticalItemListInterface {
  avatar?: React.ReactElement;
  primary: string | React.ReactNode;
  secondary?: string | React.ReactNode;
  options?: React.ReactNode;
  isPlaying?: boolean;
  handleClick?: (event: any) => void | undefined;
  filename: string;
  basename: string;
  environment: Environment;
  size?: number;
  arrayBufferBlob?: ArrayBuffer | null;
  audioState?: "play" | "pause" | "stop";
  handleAudioFinish: () => void;
}

export interface TagInterface {
  id: number;
  tag: string;
}

export interface RoomAvatarInterface {
  token: string;
  name: string;
  displayName: string;
  canDeleteConversation: boolean;
  fontSize?: number;
  width?: number;
  height?: number;
  className?: string;
  canChangeAvatar?: boolean;
  roomType?: number;
  url?: string;
}

export interface DefaultConfigButtonInterface {
  color: {
    main: string;
    light: string;
  };
  fontSizeIcon: FontSizeIconProps;
  fontSize: number;
}
export interface BaseButtonInterface extends Omit<ButtonProps, "children" | "color"> {
  title?: string;
  startIcon?: AllIconProps;
  endIcon?: AllIconProps | undefined;
  disabled?: boolean;
  color?: AllPaletteColors | string;
  iconColor?: AllPaletteColors | string;
  textColor?: AllPaletteColors | string;
  borderColor?: AllPaletteColors | string;
  borderRadius?: number;
  backgroundColor?: AllPaletteColors | string;
  fullWidth?: boolean;
  size?: ButtonSizeProps;
  iconSize?: FontSizeIconProps | number;
  // eslint-disable-next-line @typescript-eslint/ban-types
  style?: object;
  variant?: ButtonVariantEnum;
  handleClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
}

export interface MemberInterface {
  id: string | undefined;
  createdTimestamp: number;
  username: string;
  enabled: boolean;
  totp: boolean;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
  email: string;
  attributes?: {
    country: [];
    language: [];
    position: [];
    region: [];
    phone: [];
  };
  disableableCredentialTypes: [];
  requiredActions: [];
  notBefore: number;
  permission: RoleUserEnum.ADMIN | RoleUserEnum.COLLABORATOR;
}

export interface CurrentPublication {
  title: string;
  description: string;
  content: string;
  category: number;
  status: PublicationStatusProps;
  thumbnail?: File;
  publisher: string;
  userId: string;
  rights: string;
  date: string;
  createdAt: string;
  categoryName: string;
  id?: number;
  files?: NewPublicationFile[];
  refId?: string;
  cmsMode?: "DRAFT" | "PUBLISH";
  mode?: "DRAFT" | "PUBLISH";
  sentAt?: Date;
  errorMessage?: string;
  thumbsentAt?: Date;
  language: string;
}

export interface NewPublicationFile {
  media: File;
  title: string;
  description: string;
  language?: string;
  creator?: string;
  format?: string;
  extentSize?: number;
  publicationId: number;
  tags?: string;
  status: PublicationStatusProps;
  localId: string;
  statusMessage?: string;
  duration?: number;
  isFromLibrary: boolean;
  creatorName?: string;
  position?: number;
  isFromCMS?: boolean;
}

export interface EditPublicationFile {
  id: string;
  media: MediaInterface;
  title: string;
  description: string;
  language?: string;
  creator?: string;
  format?: string;
  extentSize?: number;
  publicationId: number;
  tags?: string;
  status: PublicationStatusProps;
  localId: string;
  statusMessage?: string;
  duration?: number;
  isFromLibrary: boolean;
  creatorName?: string;
  position?: number;
  isFromCMS?: boolean;
}

export interface FileMetadataInterface {
  title?: string;
  description?: string;
  file?: File;
  producedBy?: string;
  creator?: string;
  language?: string;
  country?: string;
  lastModification?: string;
  tags?: string;
  fileId?: string;
  mime?: string;
  size?: number;
  media?: MediaInterface;
}

export interface MediaFiltersProps {
  country?: string;
  language?: string;
}

export interface UpdateWizardInterface {
  feature: WizardFeatures;
}

export interface WizardFeaturesInterface {
  feature: WizardFeatures;
}

export interface WizardInterface {
  honeycombList: boolean;
  honeycombChat: boolean;
  editAudio: boolean;
}

export type WizardFeatures = WizardFeaturesEnum;
