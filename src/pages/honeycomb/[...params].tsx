/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { GetStaticProps, GetStaticPaths } from "next";
import { I18nInterface } from "@/interfaces/index";
import { OfflinePagesEnum } from "@/enums/index";
import { useRouter } from "next/router";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import HoneycombPage from "@/components/pages/honeycomb/HoneycombPage";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { setCurrentOfflinePage } from "@/utils/offlineNavigation";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["honeycomb", "library", "call", "virtualStudio"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

function Honeycomb() {
  const router = useRouter();
  const { params } = router.query;
  if (!params) {
    router.back();
    return null;
  }

  const token = params[0];
  const displayName: string = params[1];
  const canDeleteConversation = Number(params[2]);

  const connectionStatus = useConnectionStatus();

  useEffect(() => {
    router.prefetch("/offline/honeycomb");
  }, []);

  useEffect(() => {
    if (!connectionStatus) {
      setCurrentOfflinePage({
        page: OfflinePagesEnum.HONEYCOMB_CHAT,
        data: {
          token,
          displayName,
          canDeleteConversation,
        },
      });
      router.replace(`/offline/honeycomb`);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connectionStatus]);

  return (
    <HoneycombPage
      token={token}
      displayName={displayName}
      canDeleteConversation={canDeleteConversation}
    />
  );
}

export default Honeycomb;
