/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from "react";
import LayoutApp from "@/components/statefull/LayoutApp";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import FlexBox from "@/components/ui/FlexBox";
import Box from "@material-ui/core/Box";
import { HoneycombFetchMemoized } from "@/components/pages/honeycomb/HoneycombFetch";
import { JustifyContentEnum, WizardFeaturesEnum } from "@/enums/index";
import { useTranslation } from "next-i18next";
import { RoomItemInterface } from "@/interfaces/talk";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { SearchInputMemoized } from "@/components/ui/SearchInput";
import { useTour } from "@reactour/tour";
import { useSelector } from "react-redux";
import { PropsWizardSelector } from "@/types/*";
import WizardWrapper from "@/components/ui/Wizard/WizardWrapper";
import { Content, SearchContent } from "@/components/pages/honeycomb/styled";
import OnlineOnly from "@/components/ui/ConnectionStatus/OnlineOnly";
import HeaderOptions from "@/components/pages/honeycomb/HeaderOptions";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["honeycomb"])),
  },
});

export const filterHoneycombs = (honeycombs: RoomItemInterface[]) => {
  if (!honeycombs) {
    return honeycombs;
  }

  return honeycombs.filter((honeycomb: RoomItemInterface) => honeycomb.type !== 4);
};

const stepsProps = {
  feature: WizardFeaturesEnum.HONEYCOMB_LIST,
};

export const steps = [
  {
    selector: `[data-tut="honeycomb-title-header"]`,
    content: (
      <WizardWrapper
        {...stepsProps}
        content="First Text lore, A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo real. Wikipédia"
      />
    ),
  },
  {
    selector: `[data-tut="honeycomb-search-header"]`,
    content: <WizardWrapper {...stepsProps} content="TWO Text 2" />,
  },
  {
    selector: `[data-tut="honeycomb-filter"]`,
    content: () => <WizardWrapper {...stepsProps} content="content 3" />,
  },
];

export default function Honeycomb() {
  const { wizard } = useSelector((state: { wizard: PropsWizardSelector }) => state.wizard);
  const [openSearch, setOpenSearch] = useState(false);
  const [keyword, setKeyword] = useState("");
  const { setSteps, setIsOpen } = useTour();
  const { t } = useTranslation("honeycomb");

  useEffect(() => {
    if (wizard.honeycombList) {
      setIsOpen(true);
    }
  }, [wizard.honeycombList, setIsOpen]);

  const handleOpenSearch = useCallback(() => {
    setKeyword("");
    setOpenSearch(!openSearch);
  }, []);

  useEffect(() => {
    setSteps(steps);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleCloseSearch = useCallback(() => {
    setOpenSearch(false);
    setKeyword("");
  }, []);

  return (
    <LayoutWrapper
      rightExtraElement={<HeaderOptions handleOpenSearch={handleOpenSearch} key="extra-options" />}
    >
      <Content>
        {openSearch && (
          <OnlineOnly>
            <SearchContent>
              <SearchInputMemoized
                search={setKeyword}
                cancel={handleCloseSearch}
                placeholder={t("searchLabel")}
                searchIntervalMs={600}
                focus
              />
            </SearchContent>
          </OnlineOnly>
        )}
        <HoneycombFetchMemoized searchKeyword={keyword} />
      </Content>
    </LayoutWrapper>
  );
}

type LayoutWrapperProps = {
  children: React.ReactNode;
  rightExtraElement?: React.ReactNode;
};

function LayoutWrapper({ children, rightExtraElement }: LayoutWrapperProps) {
  const { t } = useTranslation("honeycomb");

  return (
    <LayoutApp
      dataTutTitle="honeycomb-title-header"
      title={t("title")}
      // tuor
      drawer
      rightExtraElement={rightExtraElement}
    >
      <FlexBox justifyContent={JustifyContentEnum.FLEXSTART} extraStyle={{ padding: 0, margin: 0 }}>
        <Box width="100%" component="main">
          {children}
        </Box>
      </FlexBox>
    </LayoutApp>
  );
}
