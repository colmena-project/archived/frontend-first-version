/* eslint-disable indent */
/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import GlobalLayout from "@/components/ui/GlobalLayout";
import { appWithTranslation } from "next-i18next";
import type { AppProps } from "next/app";
import theme from "@/styles/theme";
import CONSTANTS from "@/constants/index";
import DateFnsUtils from "@date-io/date-fns";
import { useStore } from "@/store/index";
import { Provider } from "react-redux";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import { Provider as NextAuthProvider } from "next-auth/client";
import { verifyIndexedDBBrowserEnable } from "@/utils/utils";
import Transfer from "@/components/statefull/Transfer";
import "@/styles/waveform-playlist.scss";
import "@/styles/globals.scss";
import LoadingPage from "@/components/ui/LoadingPage";
import { ApolloProvider } from "@apollo/client";
import { DefaultSeo } from "next-seo";

import { client } from "@/apollo/client";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import AppWorkers from "@/components/statefull/AppWorkers";
import ConnectionStatusProvider from "@/providers/ConnectionStatusProvider";

import { TourProvider, useTour } from "@reactour/tour";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";

function MyApp({ Component, pageProps }: AppProps) {
  const { steps } = useTour();
  const store = useStore(pageProps.initialReduxState);
  const persistor = persistStore(store, {}, () => {
    persistor.persist();
  });

  const disableBody = (target: Element | HTMLElement) => disableBodyScroll(target);
  const enableBody = (target: Element | HTMLElement) => enableBodyScroll(target);

  React.useEffect(() => {
    verifyIndexedDBBrowserEnable();
    const jssStyles: Element | null = document.querySelector("#jss-server-side");
    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  type PublicationSEOProps = {
    url: string;
    title: string;
    description: string;
    image: string;
    siteName: string;
  };

  const openGraphObj = (publicationSEO: PublicationSEOProps) => ({
    url: publicationSEO.url,
    title: publicationSEO.title,
    description: publicationSEO.description,
    images: [
      {
        url: publicationSEO.image,
        alt: publicationSEO.title,
      },
    ],
    siteName: CONSTANTS.APP_NAME,
  });

  return (
    <>
      <DefaultSeo
        title={CONSTANTS.APP_NAME}
        description={CONSTANTS.APP_DESCRIPTION}
        openGraph={
          pageProps.publicationSEO ? { ...openGraphObj(pageProps.publicationSEO) } : undefined
        }
      />

      <ConnectionStatusProvider>
        <NextAuthProvider session={pageProps.session}>
          <AppWorkers />

          <Provider store={store}>
            <PersistGate
              loading={
                <div>
                  <LoadingPage open />
                </div>
              }
              persistor={persistor}
            >
              <ThemeProvider theme={theme}>
                <TourProvider
                  steps={steps}
                  afterOpen={disableBody}
                  beforeClose={enableBody}
                  showBadge={false}
                  showDots={false}
                >
                  <GlobalLayout>
                    <CssBaseline />
                    <ApolloProvider client={client}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Component {...pageProps} />
                      </MuiPickersUtilsProvider>
                      <Transfer />
                    </ApolloProvider>
                    <div id="audios_conference"></div>
                  </GlobalLayout>
                </TourProvider>
              </ThemeProvider>
            </PersistGate>
          </Provider>
        </NextAuthProvider>
      </ConnectionStatusProvider>
    </>
  );
}

export default appWithTranslation(MyApp);
