import React from "react";
import FlexBox from "@/components/ui/FlexBox";
import LayoutApp from "@/components/statefull/LayoutApp";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import { JustifyContentEnum } from "@/enums/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { makeStyles } from "@material-ui/core";
import Search from "@/components/pages/search";
import { useTranslation } from "next-i18next";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["library", "search"])),
  },
});

const useStyles = makeStyles((theme) => ({
  topSearch: {},
  container: {
    width: "100%",
    padding: "0 10px",
  },
  appBar: {
    display: "flex",
    justifyContent: "flex-start",
    position: "relative",
    backgroundColor: theme.palette.primary.main,
  },
  tabs: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    width: "100vw",
    color: theme.palette.icon.main,
  },
  avatar: {
    marginRight: 10,
  },
}));

function LibrarySearch() {
  const { t } = useTranslation("search");
  const classes = useStyles();

  const handleIsSearching = () => {
    // isSearching
  };

  return (
    <LayoutApp title={t("title")} back>
      <FlexBox
        justifyContent={JustifyContentEnum.FLEXSTART}
        className={classes.topSearch}
        padding={0}
        extraStyle={{ marginTop: "20px" }}
      >
        <Search setIsSearching={handleIsSearching} isSearching />
      </FlexBox>
    </LayoutApp>
  );
}

export default LibrarySearch;
