import React from "react";
import FlexBox from "@/components/ui/FlexBox";
import LayoutApp from "@/components/statefull/LayoutApp";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import { JustifyContentEnum } from "@/enums/index";
import HeaderMediaProfile from "@/components/pages/media-profile/Header";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import MediaProfileTabs from "@/components/pages/media-profile/MediaProfileTabs";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["mediaProfile", "home", "profile"])),
  },
});

function MediaProfile() {
  const { t } = useTranslation("mediaProfile");

  return (
    <LayoutApp title={t("title")} drawer>
      <FlexBox justifyContent={JustifyContentEnum.FLEXSTART} extraStyle={{ padding: 0, margin: 0 }}>
        <HeaderMediaProfile />
        <MediaProfileTabs />
      </FlexBox>
    </LayoutApp>
  );
}

export default MediaProfile;
