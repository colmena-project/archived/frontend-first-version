/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { PropsUserSelector } from "../types";
import { signOut, getSession } from "next-auth/client";
import ResourceUnavailable from "@/components/ui/ResourceUnavailable";
import FullCenterContainer from "@/components/ui/FullCenterContainer";
import { parseCookies } from "nookies";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getPathUrl } from "@/utils/utils";
import { Alert } from "@material-ui/lab";
import TextButton from "@/components/ui/Button/TextButton";

type Props = {
  statusCode: any;
};

function Error({ statusCode }: Props) {
  const router = useRouter();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const cookies = parseCookies();
  const dispatch = useDispatch();
  const [oldConnection, setOldConnection] = useState(true);
  const connectionStatus = useConnectionStatus();
  const locale = cookies.NEXT_LOCALE || "en";

  const init = async () => {
    let url = "/login";
    const currentPath = getPathUrl();
    if (currentPath) {
      url += `?redirect=${currentPath}`;
    }

    if (!statusCode) {
      try {
        const session = await getSession();
        if (!session || !userRdx?.user) {
          dispatch({ type: "USER_LOGGED_OUT" });
          await signOut({ redirect: false });
          router.push(url, "", {
            locale,
          });
        }
      } catch (e) {
        console.log("_error", e);
      }
    }
  };

  useEffect(() => {
    if (connectionStatus) {
      init();
    }
  }, []);

  useEffect(() => {
    if (connectionStatus && !oldConnection) {
      window.document.location.reload();
    }

    setOldConnection(connectionStatus);
  }, [connectionStatus]);

  if (!connectionStatus) {
    return (
      <FullCenterContainer>
        <Alert severity="error">The page is unavailable offline.</Alert>
        <TextButton title="Back" startIcon="back" handleClick={() => window.history.back()} />
      </FullCenterContainer>
    );
  }

  if (statusCode) {
    return (
      <FullCenterContainer>
        <ResourceUnavailable
          icon={statusCode === 500 ? "error" : "error_outline"}
          title={statusCode}
        />
      </FullCenterContainer>
    );
  }

  return null;
}

type InitialProps = {
  res: any;
  err: any;
};

Error.getInitialProps = ({ res, err }: InitialProps) => {
  // eslint-disable-next-line no-nested-ternary
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;

  return { statusCode };
};

export default Error;
