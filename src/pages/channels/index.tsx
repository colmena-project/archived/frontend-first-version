import React, { useEffect, useState } from "react";
import LayoutApp from "@/components/statefull/LayoutApp";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import FlexBox from "@/components/ui/FlexBox";
import Box from "@material-ui/core/Box";
import { JustifyContentEnum } from "@/enums/index";
import { getChannels } from "@/services/talk/room";
import { useTranslation } from "next-i18next";
import FileListSkeleton from "@/components/ui/skeleton/FileList";
import AlertInfoCenter from "@/components/ui/AlertInfoCenter";
import { RoomItemInterface } from "@/interfaces/talk";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import ChannelList from "@/components/pages/channel/ChannelList";
import IconButtonCtr from "@/components/ui/IconButton";
import SearchInput from "@/components/ui/SearchInput";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  searchContent: {
    marginTop: theme.spacing(3),
    marginLeft: "10px",
    marginRight: "10px",
  },
}));

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["channel", "search"])),
  },
});

export const filterHoneycombs = (honeycombs: RoomItemInterface[]) => {
  if (!honeycombs) {
    return honeycombs;
  }

  return honeycombs.filter((honeycomb: RoomItemInterface) => honeycomb.type !== 4);
};

export default function Channels() {
  const [openSearch, setOpenSearch] = useState(false);
  const [keyword, setKeyword] = useState("");
  const [tokenUuid, setTokenUuid] = useState("");
  const { data, error } = getChannels({}, tokenUuid);
  const { t } = useTranslation("channel");
  const { t: c } = useTranslation("common");
  const classes = useStyles();

  useEffect(() => {
    document.addEventListener("reload-honeycombs", (e: CustomEvent<{ uuid: string }>) => {
      if (!e.detail) return;
      setTokenUuid(e.detail.uuid);
    });
  }, []);

  const handleOpenSearch = () => {
    setOpenSearch(!openSearch);
  };

  const handleCloseSearch = () => {
    setOpenSearch(false);
    setKeyword("");
  };

  if (!data && !error)
    return (
      <LayoutWrapper>
        <FileListSkeleton />
      </LayoutWrapper>
    );

  if (error)
    return (
      <LayoutWrapper>
        <AlertInfoCenter title={c("noItemsFound")} />
      </LayoutWrapper>
    );

  return (
    <LayoutWrapper
      rightExtraElement={
        <IconButtonCtr
          icon="search"
          fontSizeIcon={20}
          handleClick={handleOpenSearch}
          key="search"
        />
      }
    >
      {openSearch && (
        <Box className={classes.searchContent}>
          <SearchInput
            search={setKeyword}
            cancel={handleCloseSearch}
            placeholder={t("searchLabel")}
            searchIntervalMs={0}
            focus
          />
        </Box>
      )}
      <ChannelList data={data.ocs.data} searchKeyword={keyword} />
    </LayoutWrapper>
  );
}

type LayoutWrapperProps = {
  children: React.ReactNode;
  rightExtraElement?: React.ReactNode;
};

function LayoutWrapper({ children, rightExtraElement }: LayoutWrapperProps) {
  const { t } = useTranslation("channel");

  return (
    <LayoutApp title={t("title")} drawer back rightExtraElement={rightExtraElement}>
      <FlexBox justifyContent={JustifyContentEnum.FLEXSTART} extraStyle={{ padding: 0, margin: 0 }}>
        <Box width="100%">{children}</Box>
      </FlexBox>
    </LayoutApp>
  );
}
