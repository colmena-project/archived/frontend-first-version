import React, { useState, useEffect, useCallback } from "react";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { GetStaticProps } from "next";
import {
  I18nInterface,
  LibraryCardItemInterface,
  LibraryItemInterface,
  TimeDescriptionInterface,
} from "@/interfaces/index";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import {
  ContextMenuEventEnum,
  ContextMenuOptionEnum,
  FilterEnum,
  ListTypeEnum,
  OrderEnum,
  PlayerTypeEnum,
} from "@/enums/index";
import {
  setLibraryFiles,
  setLibraryPathExists,
  setLibraryPath,
  editLibraryFile,
  addLibraryFile,
  removeLibraryFile,
} from "@/store/actions/library";
import Library, { filterItems, getItems, orderItems } from "@/components/pages/library";
import { PropsLibrarySelector, PropsUserSelector } from "@/types/*";
import ContextMenuOptions from "@/components/pages/library/contextMenu";
// import { toast } from "@/utils/notifications";
import { removeCornerSlash, removeFirstSlash, isSafari } from "@/utils/utils";
// import IconButton from "@/components/ui/IconButton";
import {
  // getAudioPath,
  getPath,
  hasExclusivePath,
  isRootPath,
  // pathIsInFilename,
} from "@/utils/directory";
import { Button } from "@material-ui/core";
import Image from "next/image";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { Alert } from "@material-ui/lab";
import DirectoryList from "@/components/ui/skeleton/DirectoryList";
import { getFileLink } from "@/utils/offlineNavigation";
import LibraryWrapper from "@/components/pages/library/LibraryWrapper";
// import { v4 as uuid } from "uuid";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["library"])),
  },
});

function MyLibrary() {
  const library = useSelector((state: { library: PropsLibrarySelector }) => state.library);
  const [rawItems, setRawItems] = useState<Array<LibraryItemInterface>>([]);
  const notFoundDir = !library.currentPathExists;
  const { libraryFiles } = library;
  const [isLoading, setIsLoading] = useState(false);
  const [listType, setListType] = useState(ListTypeEnum.LIST);
  const router = useRouter();
  const { libraryPath: path } = router.query;
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const [items, setItems] = useState<Array<LibraryItemInterface>>(
    [] as Array<LibraryItemInterface>,
  );
  const [currentPath, setCurrentPath] = useState("");
  const [order, setOrder] = useState<OrderEnum>(OrderEnum.LATEST_FIRST);
  const [filter, setFilter] = useState<FilterEnum>(FilterEnum.ALL);
  const { t } = useTranslation("common");
  const { t: l } = useTranslation("library");
  const dispatch = useDispatch();

  const connectionStatus = useConnectionStatus();

  const timeDescription: TimeDescriptionInterface = t("timeDescription", { returnObjects: true });

  // const unavailable = () => {
  //   toast(t("featureUnavailable"), "warning");
  // };

  const handleContextMenuUpdate = async (
    item: LibraryItemInterface,
    event: ContextMenuEventEnum,
    option: ContextMenuOptionEnum,
    extraInfo: any,
  ) => {
    switch (event) {
      case ContextMenuEventEnum.UPDATE:
        if (option === ContextMenuOptionEnum.AVAILABLE_OFFLINE) {
          dispatch(editLibraryFile(extraInfo.oldId, item));

          return;
        }

        dispatch(editLibraryFile(item.id, item));
        break;
      case ContextMenuEventEnum.CREATE:
        if (option === ContextMenuOptionEnum.DUPLICATE) {
          dispatch(addLibraryFile(item));

          return;
        }

        router.push(`/library/${removeFirstSlash(getPath(item.aliasFilename))}`);
        break;
      case ContextMenuEventEnum.DELETE:
        dispatch(removeLibraryFile(item.id));
        break;
      default:
        break;
    }
  };

  const options = (
    cardItem: LibraryCardItemInterface,
    playIconComp: React.ReactNode | undefined = undefined,
  ) => {
    const { filename, basename } = cardItem;
    const options = [];
    // const shareOption = (
    //   <IconButton
    //     key={`${uuid()}-share`}
    //     icon="share"
    //     color="#9A9A9A"
    //     style={{ padding: 0, margin: 0, minWidth: 30 }}
    //     fontSizeIcon="small"
    //     handleClick={unavailable}
    //   />
    // );

    if (playIconComp) options.push(playIconComp);

    if (!hasExclusivePath(filename) && removeCornerSlash(filename).split("/").length > 1) {
      // if (!pathIsInFilename(getAudioPath(), filename) && orientation === "vertical") {
      //   options.push(shareOption);
      // }

      options.push(
        <ContextMenuOptions
          key={`${basename}-more-options`}
          {...cardItem}
          availableOptions={[
            ContextMenuOptionEnum.EDIT,
            ContextMenuOptionEnum.COPY,
            ContextMenuOptionEnum.MOVE,
            ContextMenuOptionEnum.DETAILS,
            ContextMenuOptionEnum.AVAILABLE_OFFLINE,
            ContextMenuOptionEnum.DOWNLOAD,
            ContextMenuOptionEnum.DELETE,
            ContextMenuOptionEnum.DUPLICATE,
            ContextMenuOptionEnum.PUBLISH,
            ContextMenuOptionEnum.RENAME,
          ]}
          onChange={handleContextMenuUpdate}
        />,
      );
    }

    return options;
  };

  const bottomOptions = (
    cardItem: LibraryCardItemInterface,
    playIconComp: React.ReactNode | undefined = undefined,
    badgeStatusGrid: React.ReactNode | undefined = undefined,
  ) => {
    // const { filename, basename, orientation } = cardItem;
    const options = [];
    if (playIconComp) options.push(playIconComp);

    /* const shareOption = (
      <IconButton
        key={`${basename}-share`}
        icon="share"
        color="#9A9A9A"
        style={{ padding: 0, margin: 0, minWidth: 30 }}
        fontSizeIcon="small"
        handleClick={unavailable}
      />
    );

    if (!hasExclusivePath(filename) && removeCornerSlash(filename).split("/").length > 1) {
      if (!pathIsInFilename(getAudioPath(), filename) && orientation === "horizontal") {
        options.push(shareOption);
      }
    } */

    if (badgeStatusGrid) options.push(badgeStatusGrid);

    return options;
  };

  const mountItems = useCallback(
    async (path: string) => {
      setIsLoading(true);

      const { items, libraryPathExists } = await getItems(
        path,
        userRdx.user.id,
        timeDescription,
        l,
      );

      setIsLoading(false);
      if (!connectionStatus && items.length === 0 && path !== "/") {
        await router.push("/library");
        return;
      }

      dispatch(setLibraryPathExists(libraryPathExists));
      dispatch(setLibraryFiles(items));
      dispatch(setLibraryPath(path));
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dispatch, l, timeDescription, userRdx.user.id],
  );

  const handleOrder = useCallback(
    (order: OrderEnum) => {
      setOrder(order);
      setItems(orderItems(order, filterItems(filter, rawItems), userRdx.user));
    },
    [filter, rawItems, userRdx.user],
  );

  const handleFilter = useCallback(
    (filter: FilterEnum) => {
      setFilter(filter);
      setItems(orderItems(order, filterItems(filter, rawItems), userRdx.user));
    },
    [order, rawItems, userRdx.user],
  );

  useEffect(() => {
    setRawItems(libraryFiles);
  }, [libraryFiles]);

  useEffect(() => {
    let currentPath = "/";
    if (typeof path === "object") {
      currentPath = path.join("/");
    }

    setCurrentPath(currentPath);
    mountItems(currentPath);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [path, connectionStatus]);

  useEffect(() => {
    let currentOrder = order;
    if (isRootPath(currentPath)) {
      currentOrder = OrderEnum.HIGHLIGHT;
      setOrder(currentOrder);
    } else if (!isRootPath(currentPath) && currentOrder === OrderEnum.HIGHLIGHT) {
      currentOrder = OrderEnum.LATEST_FIRST;
      setOrder(currentOrder);
    }

    setItems(orderItems(currentOrder, filterItems(filter, rawItems), userRdx.user));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rawItems]);

  const handleItemClick = ({ type, aliasFilename, filename }: LibraryCardItemInterface) => {
    if (type === "directory" && router.query.path !== aliasFilename) {
      router.push(`/library/${aliasFilename}`);
    } else if (type === "file") {
      router.push(getFileLink(btoa(filename), connectionStatus));
    }
  };

  if (isLoading) {
    return (
      <LibraryWrapper
        path={path}
        currentPath={currentPath}
        filter={filter}
        order={order}
        items={items}
        handleFilter={handleFilter}
        handleOrder={handleOrder}
        listType={listType}
        setListType={setListType}
      >
        <DirectoryList quantity={4} />
      </LibraryWrapper>
    );
  }

  if (items.length === 0) {
    if (connectionStatus && !notFoundDir) {
      return (
        <LibraryWrapper
          path={path}
          currentPath={currentPath}
          filter={filter}
          order={order}
          items={items}
          handleFilter={handleFilter}
          handleOrder={handleOrder}
          listType={listType}
          setListType={setListType}
        >
          <Alert severity="info">{l("messages.noItemsFound")}</Alert>
        </LibraryWrapper>
      );
    }

    if (!connectionStatus && currentPath === "/") {
      return (
        <LibraryWrapper
          path={path}
          currentPath={currentPath}
          filter={filter}
          order={order}
          items={items}
          handleFilter={handleFilter}
          handleOrder={handleOrder}
          listType={listType}
          setListType={setListType}
        >
          <Alert severity="info">{l("messages.noOfflineItemsAvailable")}</Alert>
        </LibraryWrapper>
      );
    }

    if (connectionStatus && notFoundDir) {
      return (
        <LibraryWrapper
          path={path}
          currentPath={currentPath}
          filter={filter}
          order={order}
          items={items}
          handleFilter={handleFilter}
          handleOrder={handleOrder}
          listType={listType}
          setListType={setListType}
        >
          <Image alt="404 not found" src="/images/404 Error.png" width={500} height={500} />
          <Button color="primary" variant="outlined" onClick={() => router.back()}>
            {t("form.backButton")}
          </Button>
        </LibraryWrapper>
      );
    }
  }

  return (
    <LibraryWrapper
      path={path}
      currentPath={currentPath}
      filter={filter}
      order={order}
      items={items}
      handleFilter={handleFilter}
      handleOrder={handleOrder}
      listType={listType}
      setListType={setListType}
    >
      <Library
        items={items}
        options={options}
        bottomOptions={bottomOptions}
        handleItemClick={handleItemClick}
        listType={listType}
        playerType={isSafari() ? PlayerTypeEnum.WAVE : PlayerTypeEnum.CIRCLE}
      />
    </LibraryWrapper>
  );
}

export default MyLibrary;
