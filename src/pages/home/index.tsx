import React, { useState } from "react";
import FlexBox from "@/components/ui/FlexBox";
import LayoutApp from "@/components/statefull/LayoutApp";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import { JustifyContentEnum, TextVariantEnum } from "@/enums/index";
import { useTranslation } from "next-i18next";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import RecentFiles from "@/components/pages/home/RecentFiles";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import FilesInfoSection from "@/components/pages/home/FilesInfo";
import HoneyCombsList from "@/components/pages/home/Section3";

import Search from "@/components/pages/search";
import Avatar from "@/components/pages/profile/AvatarWithContextMenu";
import Box from "@material-ui/core/Box";

import { getFirstname } from "@/utils/utils";
import LogoSvg from "../../../public/images/svg/colmena_logo_1612.svg";

import Clickable from "@/components/ui/Clickable";
import { useRouter } from "next/router";
import ToolsMenu from "@/components/ui/ToolsMenu";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import {
  BoxAvatar,
  Contet,
  GreetingTitle,
  Header,
  Logo,
  MediaName,
  Spacing,
  SubHeader,
  UserName,
} from "../../component/pages/home/styled";
import RecentPublications from "@/components/pages/home/RecentPublications";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      "home",
      "invitation",
      "library",
      "common",
      "intro",
      "search",
      "publication",
    ])),
  },
});

function Home() {
  const router = useRouter();
  const { t } = useTranslation("home");
  const [isSearching, setIsSearching] = useState(false);
  const connectionStatus = useConnectionStatus();

  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const handleIsSearching = (isSearching: boolean) => setIsSearching(isSearching);

  const centerExtraElement = (
    <Logo key="logo">
      <LogoSvg />
    </Logo>
  );

  return (
    <LayoutApp title="" centerExtraElement={centerExtraElement} drawer notifications>
      <Header component="main" role="main">
        <Box height={28} width="100%">
          <Box>
            <GreetingTitle variant={TextVariantEnum.CAPTION}>{t("greetingTitle")},</GreetingTitle>{" "}
            <UserName variant={TextVariantEnum.CAPTION}>
              {getFirstname(userRdx.user.name)}!
            </UserName>
          </Box>
          <Clickable handleClick={() => router.push("media-profile")}>
            <MediaName>{userRdx.user?.media?.name}</MediaName>
          </Clickable>
        </Box>
        <BoxAvatar>
          <Avatar size={10} showEditImage={connectionStatus} />
        </BoxAvatar>
      </Header>
      <SubHeader>
        <FilesInfoSection />
        <Search
          setIsSearching={handleIsSearching}
          isSearching={isSearching}
          disabled={!connectionStatus}
        />
      </SubHeader>
      <FlexBox justifyContent={JustifyContentEnum.FLEXSTART} padding={0}>
        <Contet role="complementary" id="honeycombs-list" aria-labelledby="honeycombs-list">
          <HoneyCombsList />
        </Contet>
        <Spacing>
          <Contet id="recent-files" role="complementary" aria-labelledby="recent-files">
            <RecentFiles />
          </Contet>
        </Spacing>

        <Contet id="publications" role="complementary" aria-labelledby="publications">
          <RecentPublications />
        </Contet>
      </FlexBox>
      <ToolsMenu />
    </LayoutApp>
  );
}

export default Home;
