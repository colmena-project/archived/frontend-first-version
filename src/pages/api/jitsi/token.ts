/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { UserInfoJitsiInterface, DataJitsiInterface } from "@/interfaces/jitsi";
import { removeProtocolFromURL } from "@/utils/utils";
import CryptoJS from "crypto-js";
import getConfig from "next/config";
import { getUnixTime, add } from "date-fns";

const { serverRuntimeConfig } = getConfig();

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

type ReqBodyProps = {
  userInfo: UserInfoJitsiInterface;
  room: string;
};

function base64url(source: CryptoJS.lib.WordArray) {
  // Encode in classical base64
  let encodedSource = CryptoJS.enc.Base64.stringify(source);

  // Remove padding equal characters
  encodedSource = encodedSource.replace(/=+$/, "");

  // Replace characters according to base64url specifications
  encodedSource = encodedSource.replace(/\+/g, "-");
  encodedSource = encodedSource.replace(/\//g, "_");

  return encodedSource;
}

function generateUserData(userInfo: UserInfoJitsiInterface, room: string) {
  const domain = removeProtocolFromURL(serverRuntimeConfig.meet.baseUrl);
  const { id, name, email, moderator } = userInfo;
  const user: DataJitsiInterface = {
    aud: "colmena_app_id",
    context: {
      user: {
        id,
        name,
        // avatar: `https://claudio.colmena.network/avatar/${id}/145`,
        email,
        moderator,
      },
    },
    exp: getUnixTime(add(new Date(), { days: 1 })),
    iss: domain,
    room,
    sub: domain,
  };
  if (moderator) {
    user.context.features = {
      livestreaming: true,
      "outbound-call": true,
      transcription: false,
      recording: true,
    };
  }

  return user;
}

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "POST") {
    try {
      const { userInfo, room }: ReqBodyProps = req.body;

      const header = {
        alg: "HS256",
        typ: "JWT",
      };

      const user = generateUserData(userInfo, room);

      const stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
      const encodedHeader = base64url(stringifiedHeader);

      const stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(user));
      const encodedData = base64url(stringifiedData);

      const token = `${encodedHeader}.${encodedData}`;

      const secret = CryptoJS.SHA3(`${userInfo.id}-${userInfo.email}`);

      const signature = CryptoJS.HmacSHA256(token, secret);
      const signatureResult = base64url(signature);

      const signedToken = `${token}.${signatureResult}`;

      res.status(200).json({ success: true, data: signedToken });
    } catch (e) {
      console.log(e);
      res.status(200).json({ success: false, data: e.message });
    }
  }
}
