/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";
import getConfig from "next/config";

const cache = require("memory-cache");

const { serverRuntimeConfig } = getConfig();

type Data = {
  success?: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "GET") {
    try {
      let groupfolders = cache.get("groupfolders");
      if (!groupfolders) {
        const response = await axios.get(
          `${serverRuntimeConfig.api.baseUrl}/index.php/apps/groupfolders/folders?format=json`,
          {
            auth: {
              username: serverRuntimeConfig.adminInfo.username || "",
              password: serverRuntimeConfig.adminInfo.password || "",
            },
            headers: {
              "OCS-APIRequest": true,
            },
          },
        );
        const data = response?.data;
        groupfolders = Object.values(data.ocs.data).map((item: any) => item?.mount_point);
        if (groupfolders) {
          cache.put("groupfolders", groupfolders, 6000); // 1 minute in cache
        }
      }

      res.status(200).json({ success: true, data: groupfolders });
    } catch (e) {
      console.log(e);
      res.status(400).json({ success: false, error: e.message });
    }
  }
}
