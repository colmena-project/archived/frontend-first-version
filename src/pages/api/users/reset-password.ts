/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { ocsInstanceServerSide } from "@/services/ocs";
import { getServiceToken } from "@/services/keycloak/token";
import { resetPassword, getUserByField } from "@/services/keycloak/users";
import getConfig from "next/config";

const { serverRuntimeConfig } = getConfig();

const cache = require("memory-cache");

type Data = {
  success: boolean;
  error?: any;
  data?: any;
  message?: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "PUT") {
    try {
      const { username, password } = req.body;

      // UPDATE KEYCLOAK
      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;
      const response = await getUserByField(serviceAccessToken, "username", username);

      const keycloakUserId = response.data[0].id;
      const keycloakUserEmail = response.data[0].email;

      await resetPassword(serviceAccessToken, keycloakUserId, password);

      // UPDATE NEXTCLOUD
      try {
        await ocsInstanceServerSide().put(`/users/${username}?format=json`, {
          key: "password",
          value: password,
        });
      } catch (e) {
        await resetPassword(
          serviceAccessToken,
          keycloakUserId,
          serverRuntimeConfig.defaultNewUserPassword,
        );
        throw new Error(e.message);
      }
      cache.del(keycloakUserEmail);

      res.status(200).json({ success: true });
    } catch (e) {
      console.log(e);
      res.status(200).json({ success: false, message: e.message || "" });
    }
  }
}
