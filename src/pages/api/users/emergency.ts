/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { getServiceToken } from "@/services/keycloak/token";
import { disable as userDisableKeycloak, getUserByField } from "@/services/keycloak/users";

import { disableUserNextCloud } from "@/services/ocs/users";

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "PUT") {
    try {
      const { username } = req.body;
      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;
      const response = await getUserByField(serviceAccessToken, "username", username);
      const user = response.data[0];

      Promise.all([
        disableUserNextCloud(username),
        userDisableKeycloak(serviceAccessToken, user.id),
      ]);

      res.status(200).json({ success: true });
    } catch (e) {
      res.status(200).json({ success: false, data: e.response.data.errorMessage });
    }
  }
}
