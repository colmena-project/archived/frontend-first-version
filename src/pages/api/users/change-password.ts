/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { ocsInstanceServerSide } from "@/services/ocs";
import { getServiceToken } from "@/services/keycloak/token";
import { changePassword } from "@/services/keycloak/users";

const cache = require("memory-cache");

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "PUT") {
    try {
      const { newPassword, currentPassword, user } = req.body;
      // VERIFY CURRENT PASSWORD IS VALID
      await ocsInstanceServerSide({ username: user.id, password: currentPassword }).get(
        `/user?format=json`,
      );

      // UPDATE KEYCLOAK
      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;
      await changePassword(serviceAccessToken, user, newPassword);

      // UPDATE NEXTCLOUD
      try {
        await ocsInstanceServerSide().put(`/users/${user.id}?format=json`, {
          key: "password",
          value: newPassword,
        });
      } catch (e) {
        await changePassword(serviceAccessToken, user, currentPassword);
        throw new Error("unexpected error");
      }

      cache.del(user.email);

      res.status(200).json({ success: true });
    } catch (e) {
      console.log(e);
      res.status(200).json({ success: false });
    }
  }
}
