/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { ocsInstanceServerSide } from "@/services/ocs";
import { getServiceToken } from "@/services/keycloak/token";
import {
  deleteUserMemberPermission,
  updateUserMemberPermission,
  updateUserMember as userUpdateKeycloak,
} from "@/services/keycloak/users";
import { RoleUserEnum } from "@/enums/*";

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "PUT") {
    try {
      const { updatedName, updatedEmail, updatePermission, groupid, user } = req.body;

      const userId = user.id;
      const { firstName, lastName, email, permission, username } = user;
      let alreadyUpdated = true;

      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;

      // UPDATE KEYCLOAK
      if (updatedEmail || updatedName || updatePermission) {
        alreadyUpdated = false;
        const formated = {
          id: userId,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          attributes: {
            ...user.attributes,
          },
        };

        await userUpdateKeycloak(serviceAccessToken, formated);
      }

      // UPDATE PERMISSION
      if (updatePermission) {
        await deleteUserMemberPermission(serviceAccessToken, userId, permission);
        await updateUserMemberPermission(serviceAccessToken, userId, permission);

        if (permission === RoleUserEnum.COLLABORATOR) {
          ocsInstanceServerSide().delete(
            `/users/${username}/subadmins?groupid=${groupid}&format=json`,
          );
        }

        if (permission === RoleUserEnum.ADMIN) {
          const body = JSON.stringify({ groupid });
          ocsInstanceServerSide().post(`/users/${username}/subadmins?format=json`, body);
        }
      }

      // UPDATE NEXTCLOUD
      if (updatedName) {
        ocsInstanceServerSide().put(`/users/${username}?format=json`, {
          key: "displayname",
          value: `${firstName} ${lastName}`,
        });
      }
      if (updatedEmail) {
        ocsInstanceServerSide().put(`/users/${username}?format=json`, {
          key: "email",
          value: email,
        });
      }

      res.status(200).json({ success: true, data: { user, alreadyUpdated } });
    } catch (e) {
      console.log(e);
      res.status(200).json({ success: false, data: e.response.data.errorMessage });
    }
  }
}
