/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { createUser, deleteUser } from "@/services/ocs/users";
import { getServiceToken } from "@/services/keycloak/token";
import { CreateUserProps } from "@/services/internal/users";
import { addParticipantToConversation, getUsersConversationsAxios } from "@/services/talk/room";
import { create as createKeycloakUser, getUserByField } from "@/services/keycloak/users";
import { setRoleMapping } from "@/services/keycloak/roles";
import { CreateUserErrorEnum } from "@/enums/index";
import { prepareLanguageToNextcloud } from "@/utils/utils";
import getConfig from "next/config";

const { serverRuntimeConfig } = getConfig();

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "POST") {
    try {
      const { name, email, language, role, user }: CreateUserProps = req.body;

      // VERIFY IF EMAIL IS ALREADY REGISTERED
      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;
      const keycloakUsersSearchEmail = await getUserByField(serviceAccessToken, "email", email);
      if (keycloakUsersSearchEmail.data.length > 0) {
        throw new Error(CreateUserErrorEnum.ERROR3);
      }

      // CREATE NEXTCLOUD USER
      let responseNextcloud;
      const group = user.media.name;
      const password = serverRuntimeConfig.defaultNewUserPassword;
      try {
        responseNextcloud = await createUser(
          name,
          email,
          group,
          role,
          prepareLanguageToNextcloud(language),
          password,
        );
      } catch (e) {
        console.log(e);
        throw new Error(CreateUserErrorEnum.ERROR1);
      }
      const username = responseNextcloud.data.ocs.data.id;

      // ADD USER TO GROUP CHAT
      const responseRooms = await getUsersConversationsAxios(false);
      const rooms = responseRooms.data.ocs.data;
      const tokenChat = rooms.find(
        (item) => item.name.toLowerCase() === group.toLowerCase(),
      )?.token;
      if (typeof tokenChat === "string") {
        try {
          await addParticipantToConversation(tokenChat, username, false);
        } catch (e) {
          console.log(e);
          await deleteUser(username);
          throw new Error(CreateUserErrorEnum.ERROR1);
        }
      }

      // ADD USER TO KEYCLOAK
      let responseKeycloakUser;
      try {
        responseKeycloakUser = await createKeycloakUser(serviceAccessToken, {
          username,
          name,
          email,
          group,
          password,
          language: prepareLanguageToNextcloud(language),
        });
      } catch (e) {
        console.log(e);
        await deleteUser(username);
        throw new Error(CreateUserErrorEnum.ERROR2);
      }

      // ADD USER PERMISSION IN KEYCLOAK
      const response = await getUserByField(serviceAccessToken, "username", username);
      if (response.data.length > 0) {
        const userId = response.data[0].id;
        try {
          await setRoleMapping(serviceAccessToken, userId, role);
        } catch (e) {
          console.log("Error during set role mapping", e);
        }
      }

      res.status(200).json({ success: true });
    } catch (e) {
      console.log(e);
      res.status(200).json({ success: false, data: e.message });
    }
  }
}
