import type { NextApiRequest, NextApiResponse } from "next";
import { getUserByField } from "@/services/keycloak/users";
import { getServiceToken } from "@/services/keycloak/token";

type Data = {
  success: boolean;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "GET") {
    const { keyword } = req.query;
    try {
      let keywordForSearch = "";
      if (typeof keyword === "string") {
        keywordForSearch = keyword;
      }

      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;
      const keycloakUsers = await getUserByField(serviceAccessToken, "search", keywordForSearch);

      res.status(200).json(keycloakUsers.data);
    } catch (e) {
      res.status(400).json({ success: false });
    }
  }
}
