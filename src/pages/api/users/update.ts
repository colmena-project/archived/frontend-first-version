/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { ocsInstanceServerSide } from "@/services/ocs";
import { getServiceToken } from "@/services/keycloak/token";
import { update as userUpdateKeycloak } from "@/services/keycloak/users";
import { prepareLanguageToNextcloud } from "@/utils/utils";

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "PUT") {
    try {
      const { updatedName, updatedEmail, updatedLanguage, user } = req.body;
      const userId = user.id;
      const { name, email, language } = user;
      let alreadyUpdated = true;
      // UPDATE KEYCLOAK
      if (updatedEmail || updatedName || updatedLanguage || updatedLanguage) {
        alreadyUpdated = false;
        const responseServiceToken = await getServiceToken();
        const serviceAccessToken = responseServiceToken.data.access_token;
        await userUpdateKeycloak(serviceAccessToken, {
          ...user,
          language: prepareLanguageToNextcloud(user.language),
        });
      }

      // UPDATE NEXTCLOUD
      if (updatedName) {
        ocsInstanceServerSide().put(`/users/${userId}?format=json`, {
          key: "displayname",
          value: name,
        });
      }
      if (updatedEmail) {
        ocsInstanceServerSide().put(`/users/${userId}?format=json`, {
          key: "email",
          value: email,
        });
      }
      if (updatedLanguage) {
        await ocsInstanceServerSide().put(`/users/${userId}?format=json`, {
          key: "language",
          value: prepareLanguageToNextcloud(language),
        });
      }

      res.status(200).json({ success: true, data: { user, alreadyUpdated } });
    } catch (e) {
      console.log(e);
      res.status(200).json({ success: false, data: e.response.data.errorMessage });
    }
  }
}
