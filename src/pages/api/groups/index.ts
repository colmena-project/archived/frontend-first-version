/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { getServiceToken } from "@/services/keycloak/token";
import { getGroups } from "@/services/keycloak/groups";

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "GET") {
    try {
      const { name } = req.query;
      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;
      const responseGroup = await getGroups(serviceAccessToken, String(name));

      const { data } = responseGroup;
      if (!Array.isArray(data) || data.length === 0) throw new Error();

      res.status(200).json({ success: true, data });
    } catch (e) {
      res.status(200).json({ success: false });
    }
  }
}
