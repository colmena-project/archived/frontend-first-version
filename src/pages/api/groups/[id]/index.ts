/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { getServiceToken } from "@/services/keycloak/token";
import { show, update as updateGroup } from "@/services/keycloak/groups";
import { MediaInfoInterface } from "@/interfaces/index";

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  if (req.method === "GET") {
    try {
      const { id } = req.query;
      const responseServiceToken = await getServiceToken();
      const serviceAccessToken = responseServiceToken.data.access_token;
      const responseGroup = await show(serviceAccessToken, String(id));

      const { data } = responseGroup;

      if (!Array.isArray(data) || data.length !== 1) throw new Error();

      const group = data[0];

      res.status(200).json({ success: true, data: group });
    } catch (e) {
      res.status(200).json({ success: false, data: e.response.data.errorMessage });
    }
  }
  if (req.method === "PUT") {
    const { id } = req.query;
    const { data }: { data: MediaInfoInterface } = req.body;
    const responseServiceToken = await getServiceToken();
    const serviceAccessToken = responseServiceToken.data.access_token;
    const strId = String(id);
    try {
      await updateGroup(serviceAccessToken, strId, {
        id: strId,
        name: data.name,
        attributes: {
          slogan: [data.slogan],
          email: [data.email],
          url: [data.url],
          language: [data.language],
          audio_description_url: [data.audio_description_url],
          social_media_whatsapp: [data.social_media_whatsapp],
          social_media_twitter: [data.social_media_twitter],
          social_media_facebook: [data.social_media_facebook],
          social_media_mastodon: [data.social_media_mastodon],
          social_media_instagram: [data.social_media_instagram],
          social_media_telegram: [data.social_media_telegram],
        },
      });
      res.status(200).json({ success: true });
    } catch (e) {
      console.log(e);
      res.status(200).json({ success: false });
    }
  }
}
