/* eslint-disable no-restricted-syntax */
import type { NextApiRequest, NextApiResponse } from "next";
import { getServiceToken } from "@/services/keycloak/token";
import { members as getMembers, getUsersByPermission } from "@/services/keycloak/groups";
import { RoleUserEnum } from "@/enums/*";
import { memberIsAdmin } from "@/utils/memberIsAdmin";

type Data = {
  success: boolean;
  error?: any;
  data?: any;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  try {
    const { id } = req.query;
    const responseServiceToken = await getServiceToken();
    const serviceAccessToken = responseServiceToken.data.access_token;
    const responseGroup = await getMembers(serviceAccessToken, String(id));
    const responseAdministrators = await getUsersByPermission(
      serviceAccessToken,
      RoleUserEnum.ADMIN,
    );
    const { data: members } = responseGroup;
    const { data: administrators } = responseAdministrators;

    const modifiedList = members.map((row) => {
      const register: any = {
        ...row,
        permission: memberIsAdmin(row.id, administrators),
      };

      return register;
    });

    res.status(200).json({ success: true, data: modifiedList });
  } catch (e) {
    res.status(200).json({ success: false, data: e.response.data.errorMessage });
  }
}
