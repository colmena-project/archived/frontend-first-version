/* eslint-disable space-infix-ops */
/* eslint-disable prettier/prettier */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-param-reassign */
import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import axios, { AxiosRequestConfig } from "axios";
import constants from "@/constants/index";
import { AppPasswordInterface, UserInfoInterface } from "@/interfaces/ocs";
import { KeycloakAccessTokenInterface } from "@/interfaces/keycloak";
import { RoleUserEnum, AuthenticationErrorEnum } from "@/enums/index";
import jwt_decode from "jwt-decode";
import getConfig from "next/config";

const cache = require("memory-cache");
const qs = require("qs");

const { serverRuntimeConfig } = getConfig();

const baseUrl = `${serverRuntimeConfig.api.baseUrl}/ocs/v2.php`;

function getUserInfo(email: string, password: string): Promise<UserInfoInterface> {
  try {
    return axios.get(`${baseUrl}/cloud/user?format=json`, {
      auth: {
        username: email,
        password,
      },
      headers: {
        "OCS-APIRequest": true,
      },
    });
  } catch (e) {
    console.log(e);
    throw new Error(AuthenticationErrorEnum.ERROR6);
  }
}

function getAppPasswordUser(email: string, password: string): Promise<AppPasswordInterface> {
  try {
    return axios.get(`${baseUrl}/core/getapppassword?format=json`, {
      auth: {
        username: email,
        password,
      },
      headers: {
        "OCS-APIRequest": true,
      },
    });
  } catch (e) {
    console.log(e);
    throw new Error(AuthenticationErrorEnum.ERROR4);
  }
}

function doKeycloakAuthentication(email: string, password: string) {
  try {
    const data = qs.stringify({
      client_id: serverRuntimeConfig.keycloak.clientId,
      client_secret: serverRuntimeConfig.keycloak.clientSecret,
      username: email,
      grant_type: "password",
      password: password,
    });
    const config: AxiosRequestConfig = {
      method: "POST",
      url: `${serverRuntimeConfig.keycloak.baseUrl}/auth/realms/${serverRuntimeConfig.keycloak.realm}/protocol/openid-connect/token`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: data,
    };
    return axios(config);
  } catch (e) {
    console.log(e);
    throw new Error(AuthenticationErrorEnum.ERROR3);
  }
}

function prepareUserObject(user: KeycloakAccessTokenInterface) {
  const clientsArray = Object.keys(user.resource_access);
  let role = RoleUserEnum.COLLABORATOR;
  if (clientsArray.length > 0) {
    const clientIdEnv: string | undefined = process.env.KEYCLOAK_CLIENT_ID;
    if (clientIdEnv) {
      const clientId = clientsArray.find((item) => item === clientIdEnv);
      if (clientId && typeof clientId === "string")
        role = user.resource_access[clientId].roles.find(
          (item: string) => item === RoleUserEnum.ADMIN,
        )
          ? RoleUserEnum.ADMIN
          : RoleUserEnum.COLLABORATOR;
    }
  }

  const language = user.language;
  let userLang = constants.DEFAULT_LANGUAGE;
  if (Object.values(constants.LOCALES_NEXTCLOUD).includes(language)) {
    // TIRAR ISSO NO FUTURO! a pasta pt de locales precisa ser alterada para pt_br
    // userLang = language === "pt_BR" ? "pt" : language;
    userLang = language;
  }
  const { name, email, position, phone, address } = user;

  if (user.groups.length === 0) throw new Error(AuthenticationErrorEnum.ERROR1);
  if (user.groups.length > 1) throw new Error(AuthenticationErrorEnum.ERROR2);

  const groups: any = user.groups.map((item) => {
    if (item[0] === "/") return item.slice(1, item.length);
    return item;
  });

  return {
    name,
    email,
    language: userLang,
    country: address?.country || "",
    region: address?.region || "",
    role,
    position: position || "",
    phone: phone || "",
    media: {
      name: groups[0],
    },
  };
}

async function getInformationsFromCache(email: string, password: string) {
  const userCache = cache.get(email);
  let nexcloudToken, userId;
  if (!userCache) {
    const appPassword = await getAppPasswordUser(email, password);
    nexcloudToken = appPassword.data.ocs.data.apppassword;
    const userData = await getUserInfo(email, password);
    userId = userData.data.ocs.data.id;
    const userObj = {
      id: userData.data.ocs.data.id,
      nexcloudToken,
    };
    cache.put(email, JSON.stringify(userObj));
  } else {
    const userObj = JSON.parse(userCache);
    nexcloudToken = userObj.nexcloudToken;
    userId = userObj.id;
  }

  return {
    nexcloudToken,
    userId,
  };
}

export default NextAuth({
  providers: [
    Providers.Credentials({
      async authorize(credentials: { email: string; password: string }) {
        const { email, password } = credentials;
        try {
          const { nexcloudToken, userId } = await getInformationsFromCache(email, password);

          const keycloakResponse = await doKeycloakAuthentication(email, password);

          const jwtDecoded: KeycloakAccessTokenInterface = jwt_decode(
            keycloakResponse.data.access_token,
          );

          return {
            id: userId,
            ...prepareUserObject(jwtDecoded),
            nexcloudToken,
            keycloakToken: keycloakResponse.data.access_token,
          };
        } catch (e) {
          console.log(e);
          throw new Error(e.message);
        }
      },
    }),
  ],
  session: {
    // Use JSON Web Tokens for session instead of database sessions.
    // This option can be used with or without a database for users/accounts.
    // Note: `jwt` is automatically set to `true` if no database is specified.
    jwt: true,

    // Seconds - How long until an idle session expires and is no longer valid.
    // maxAge: 30 * 24 * 60 * 60, // 30 days

    // Seconds - Throttle how frequently to write to database to extend a session.
    // Use it to limit write operations. Set to 0 to always update the database.
    // Note: This option is ignored if using JSON Web Tokens
    // updateAge: 24 * 60 * 60, // 24 hours
  },
  callbacks: {
    // async signIn(user, account, profile) {
    //   console.log("EEI", user, account, profile);
    //   return true;
    // },
    // async redirect(url, baseUrl) { return baseUrl },
    async session(session, token) {
      if (token) {
        // @ts-ignore
        session.user = token.user;
        session.accessToken = token.accessToken;
        session.error = token.error;
      }
      return session;
    },
    async jwt(token, user, account, profile) {
      // SIGNIN
      if (account && profile) {
        const { accessToken } = profile;
        delete profile.accessToken;
        return {
          accessToken,
          accessTokenExpires: Date.now() + constants.TOKEN_EXPIRE_SECONDS * 1000,
          user: profile,
        };
      }

      // Subsequent use of JWT, the user has been logged in before
      // access token has not expired yet
      // if (Date.now() < token.accessTokenExpires) {
      return token;
      // }

      // return refreshAccessToken(token);
    },
  },

  // Enable debug messages in the console if you are having problems
  debug: false,
});

/**
 * Takes a token and returns a new one with updated
 * `accessToken` and `accessTokenExpires`. If an error occurs,
 * returns the old token with an error property.
 */
// async function refreshAccessToken(token) {
//   try {
//     const response = await axios.put(`${process.env.NEXT_PUBLIC_API_BASE_URL}/token/refresh`, {
//       oldToken: token.accessToken,
//     });

//     const result = response.data;
//     return {
//       ...token,
//       accessToken: result.access_token,
//       accessTokenExpires: Date.now() + constants.TOKEN_EXPIRE_SECONDS * 1000,
//     };
//   } catch (error) {
//     console.log("error refresh token", error);
//     return {
//       ...token,
//       error: "RefreshAccessTokenError", // This is used in the front-end,
// and if present, we can force a re-login, or similar
//     };
//   }
// }
