import React, { useState } from "react";
import { GetStaticProps, GetStaticPaths } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import SvgIconAux from "@/components/ui/SvgIcon";
import Clickable from "@/components/ui/Clickable";
import AlertInfoCenter from "@/components/ui/AlertInfoCenter";
import PublicLayout from "@/components/ui/PublicLayout";
import { ModalConferenceJitsiAPIMemoized } from "@/components/ui/ModalConference/ModalConferenceJitsiAPI";
import ContainedButton from "@/components/ui/Button/ContainedButton";
import { Base } from "@/components/pages/public/virtual-studio/styled";
import FormGuestInfo from "@/components/pages/public/virtual-studio/FormGuestInfo";
import Script from "next/script";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["virtualStudio", "call"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

export default function VirtualStudioWithParams() {
  const router = useRouter();
  const [openConference, setOpenConference] = useState(false);
  const [openFormGuestName, setOpenFormGuestName] = useState(false);
  const [endCall, setEndCall] = useState(false);
  const [guestUsername, setGuestUsername] = useState("");
  const { t } = useTranslation("virtualStudio");
  const { params } = router.query;

  if (!params) {
    router.back();
    return null;
  }

  const token = params[0];
  const roomName = params[1];

  const link = `${window.location.origin}/public/virtual-studio/${token}`;
  const publicVirtualStudioLink = t("inviteMessage", { link });

  const handleSubmit = (name: string) => {
    setGuestUsername(name);
    setOpenFormGuestName(false);
    setOpenConference(true);
  };

  const handleClose = () => {
    setGuestUsername("");
    setOpenConference(false);
    setEndCall(true);
  }

  return (
    <PublicLayout>
      <ModalConferenceJitsiAPIMemoized
        handleClose={handleClose}
        modalTitle={t("title")}
        modalSubtitle={atob(roomName)}
        open={openConference}
        roomName={token}
        rightElement={
          <Clickable handleClick={handleClose} style={{ paddingRight: 20 }}>
            <SvgIconAux icon="close" htmlColor="#fff" fontSize="small" />
          </Clickable>
        }
        type="virtual-studio"
        publicVirtualStudioLink={publicVirtualStudioLink}
        guestUsername={guestUsername}
      />
      {!endCall && (
        <Base>
          <ContainedButton fullWidth={false} title={t("joinButtonTitle")} handleClick={() => setOpenFormGuestName(true)} />
        </Base>
      )}
      {openFormGuestName && (
        <FormGuestInfo
          open={openFormGuestName}
          handleClose={() => setOpenFormGuestName(false)}
          handleSubmit={handleSubmit}
        />
      )}
      {endCall && <AlertInfoCenter icon="call_end" title={t("callEnded")} />}
      <Script src={`${publicRuntimeConfig.meet.baseUrl}/external_api.js`}></Script>
    </PublicLayout>
  );
}
