import React, { useState } from "react";
import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";

import PublicLayout from "@/components/ui/PublicLayout";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import PublicationList from "@/components/pages/publication/PublicationList";
import { Box, makeStyles } from "@material-ui/core";
import SearchInput from "@/components/ui/SearchInput";
import TitleBox from "@/components/ui/TitleBox";
import { useTheme } from "@material-ui/core/styles";
import PublicationFiltersDrawer from "@/components/pages/publication/FiltersDrawer";
import Clickable from "@/components/ui/Clickable";
import SvgIconAux from "@/components/ui/SvgIcon";
import { PublicationFiltersProps } from "@/services/cms/publications";
import { parseCookies } from "nookies";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publication", "publications"])),
  },
});

const useStyles = makeStyles((theme) => ({
  searchRoot: {
    display: "flex",
    padding: "10px",
    width: "100%",
  },
  searchWrapper: {
    width: "100%",
  },
  searchContent: {
    padding: "15px 10px",
    boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  filter: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "0 5px 0 13px",
  },
  filtersDescription: {
    color: theme.palette.secondary.main,
  },
}));

export default function Publications() {
  const { t } = useTranslation("publication");
  const classes = useStyles();
  const theme = useTheme();
  const [openSearch, setOpenSearch] = useState(false);
  const [openFilters, setOpenFilters] = useState(false);
  const [filters, setFilters] = useState<PublicationFiltersProps>({
    order: "publishedAt:desc",
    index: 0,
  });
  const cookies = parseCookies();

  const handleOpenSearch = () => {
    setOpenSearch(!openSearch);
  };

  const handleCloseSearch = () => {
    setOpenSearch(false);
    setFilters({ ...filters, keyword: undefined });
  };

  const handleOpenFilters = async () => {
    setOpenFilters(true);
  };

  const handleFilter = (filters: PublicationFiltersProps) => {
    setFilters(filters);
    setOpenSearch(false);
  };

  const handleSearch = (keyword: string) => {
    setFilters({ ...filters, keyword });
  };

  // const clearFilters = () => {
  //   setFilters({} as PublicationFiltersProps);
  // };

  return (
    <PublicLayout rightExtraElement={
      [
        <Clickable className={classes.filter} handleClick={handleOpenSearch} key="search">
          <SvgIconAux icon="search" fontSize={20} htmlColor={theme.palette.primary.main} />
        </Clickable>,
        <Clickable className={classes.filter} handleClick={handleOpenFilters} key="filter">
          <SvgIconAux icon="settings_adjust" fontSize={20} htmlColor={theme.palette.primary.main} />
        </Clickable>,
      ]
    }>
      <TitleBox title={t("publicationsTitle")} icon="global" />
      {openSearch && (
        <Box className={classes.searchRoot}>
          <Box className={classes.searchWrapper}>
            <SearchInput
              search={handleSearch}
              cancel={handleCloseSearch}
              placeholder={t("searchLabel")}
              searchIntervalMs={600}
              focus
            />
          </Box>
        </Box>
      )}
      <PublicationList filters={filters} height="calc(100vh - 244px)" baseUrl="/public/publications" />
      <PublicationFiltersDrawer
        open={openFilters}
        handleClose={() => setOpenFilters(false)}
        filterPublications={handleFilter}
        initialFilters={filters}
        language={cookies.NEXT_LOCALE}
      />
    </PublicLayout>
  );
}
