import React, { useEffect } from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import PublicLayout from "@/components/ui/PublicLayout";
import { PublicationStaticPropsInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import PublicationHeader from "@/components/pages/publication/PublicationHeader";
import PublicationContent from "@/components/pages/publication/PublicationContent";
import PublicationTags from "@/components/pages/publication/PublicationTags";
import { formatDateToLanguage, getBaseUrl, getUrlStrapiImage } from "@/utils/utils";
import { parseISO } from "date-fns";
import { parseCookies } from "nookies";
import PublicationHead from "@/components/pages/publications/common/PublicationHead";
import { getAllPublicationsWithoutFilter, getPublicPublication } from "@/services/cms/publicationsSSQuery";
import { MediaInterface, PublicationQueryInterface } from "@/interfaces/cms";
import getConfig from "next/config";
import PublicationFiles from "@/components/pages/publication/PublicationFiles";


export default function Publication({publication}: {publication: PublicationQueryInterface | null}) {
  const router = useRouter();
  const cookies = parseCookies();
  const language = cookies.NEXT_LOCALE || "en";

  useEffect(() => {
    if (!publication) {
      router.push("/public/publications");
    }
  }, [router, publication])

  if (!publication) {
    return <></>
  }


  const { attributes } = publication;
  const { publisher, subject, publishedAt, relation, creator, title, content, tags, description } = attributes;
  let formattedDate;
  if (publishedAt) {
    formattedDate = formatDateToLanguage(parseISO(publishedAt), language);
  }

  let categoryName;
  if (subject?.data?.attributes?.name) {
    categoryName = subject.data.attributes.name;
  }

  const image = getUrlStrapiImage(relation, "medium");
  const baseUrl = getBaseUrl();


  return (
   <>
      <PublicationHead publication={publication} baseUrl={baseUrl} imageUrl={image} />
      <PublicLayout back>
        <PublicationHeader
          image={image}
          creator={creator}
          publishedAt={formattedDate}
          categoryName={categoryName}
          publisher={publisher}
          publicationsUrl="/public/publications"
          baseUrl={baseUrl}
          publicationDescription={description}
          publicationId={publication.id}
          publicationTitle={title}
          showShareButton
        />
        <PublicationContent title={title} content={content} />
        <PublicationTags tags={tags} />
        <PublicationFiles files={publication?.attributes?.files?.data} />
      </PublicLayout>
   </>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale, params }: PublicationStaticPropsInterface) => {

  const publication = await getPublicPublication(params.id)

  const image = getUrlStrapiImage(publication?.attributes.relation as MediaInterface, "medium");

  return {
    props: {
      ...(await serverSideTranslations(locale, ["publication", "publications"])),
      publicationSEO: publication ? {
        image,
        url: `/public/publications/${publication?.id}`,
        title: publication.attributes.title,
        description: publication.attributes.description
      } : null,
      publication
    },
    revalidate: 20,
  }
};

export const getStaticPaths: GetStaticPaths = async () => {
  const { serverRuntimeConfig } = getConfig();
  if (serverRuntimeConfig.appEnv === "development") {
    return {
      paths: [],
      fallback: "blocking",
    }
  }

  const {data} = await getAllPublicationsWithoutFilter()

  const paths = data.publications.data.map((publication: PublicationQueryInterface) => ({
    params: { id: publication.id },
  }))

  return {
    paths,
    fallback: "blocking",
  }
};
