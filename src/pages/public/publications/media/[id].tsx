import React, { useEffect, useState } from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import PublicLayout from "@/components/ui/PublicLayout";
import IconButtonCtr from "@/components/ui/IconButton";
import LoadingPage from "@/components/ui/LoadingPage";
import { Box, makeStyles } from "@material-ui/core";
import SearchInput from "@/components/ui/SearchInput";
import PublicationList from "@/components/pages/publication/PublicationList";
import { useTranslation } from "next-i18next";
import MediaGroupHeader from "@/components/pages/publication/MediaGroupHeader";
import { getGroupByName } from "@/services/internal/groups";
import { GroupInterface } from "@/interfaces/keycloak";
import { PublicationFiltersProps } from "@/services/cms/publications";
import PublicationFiltersDrawer from "@/components/pages/publication/FiltersDrawer";
import { PublicationFiltersEnum } from "@/enums/*";
import { parseCookies } from "nookies";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publication", "publications"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

const useStyles = makeStyles(() => ({
  searchContent: {
    padding: "15px 10px",
    boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  container: {
    overflowY: "hidden"
  }
}));

export default function Media() {
  const classes = useStyles();
  const router = useRouter();
  const { id } = router.query;
  const { t } = useTranslation("publication");
  const [loadingMediaGroup, setLoadingMediaGroup] = useState(true);
  const [mediaGroup, setMediaGroup] = useState<GroupInterface | false>();

  if (typeof id !== "string") {
    return router.push("/public/publications");
  }

  const cookies = parseCookies();
  const [openFilters, setOpenFilters] = useState(false);
  const [openSearch, setOpenSearch] = useState(false);
  const [filters, setFilters] = useState<PublicationFiltersProps>({
    group: id,
    order: "publishedAt:desc",
    index: 0,
  });

  const handleOpenSearch = () => {
    setOpenSearch(!openSearch);
  };

  const handleCloseSearch = () => {
    setOpenSearch(false);
    setFilters({ ...filters, keyword: undefined });
  };

  const handleFilter = (filters: PublicationFiltersProps) => {
    setFilters(filters);
    setOpenSearch(false);
  };

  const handleSearch = (keyword: string) => {
    setFilters({ ...filters, keyword });
  };

  useEffect(() => {
    (async () => {
      setLoadingMediaGroup(true);
      const group = await getGroupByName(id);
      setMediaGroup(group);
      setLoadingMediaGroup(false);
    })()
  }, []);

  if (loadingMediaGroup) return <LoadingPage />;

  if (!mediaGroup) {
    router.push("/public/publications");
    return <LoadingPage />;
  }

  return (
    <PublicLayout back className={classes.container} rightExtraElement={
      <IconButtonCtr
        aria-label="search"
        icon="search"
        fontSizeIcon={20}
        handleClick={handleOpenSearch}
        key="search"
      />
    }>
      <MediaGroupHeader mediaName={mediaGroup.name} socialMedias={mediaGroup.attributes} token={mediaGroup.id} />
      {openSearch && (
        <Box className={classes.searchContent}>
          <SearchInput
            search={handleSearch}
            cancel={handleCloseSearch}
            placeholder={t("searchLabel")}
            focus
          />
        </Box>
      )}
      <PublicationList filters={filters} height="calc(100vh - 298px)" baseUrl="/public/publications" />
      <PublicationFiltersDrawer
        open={openFilters}
        handleClose={() => setOpenFilters(false)}
        filterPublications={handleFilter}
        initialFilters={filters}
        hideFilters={[PublicationFiltersEnum.GROUP]}
        language={cookies.NEXT_LOCALE}
      />
    </PublicLayout>
  );
}
