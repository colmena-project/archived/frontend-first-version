/* eslint-disable jsx-a11y/tabindex-no-positive */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import LayoutApp from "@/components/statefull/LayoutApp";
import SimpleBackdrop from "@/components/ui/Backdrop";
import FlexBox from "@/components/ui/FlexBox";
import ContextMenuFile from "@/components/pages/text-editor/contextMenuFile";
import { JustifyContentEnum } from "@/enums/*";
import { I18nInterface, LibraryItemInterface, TimeDescriptionInterface } from "@/interfaces/index";
import { Grid, Fade, Box } from "@material-ui/core";
import { GetStaticProps, GetStaticPaths } from "next";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";
import { existFile, getDataFile, getFileContents, putFile } from "@/services/webdav/files";
import { toast } from "@/utils/notifications";
import { findByFilename } from "@/store/idb/models/files";
import { applyLocalItemInterface, mergeEnvItems } from "@/components/pages/library";
import { availableOptions, useStyles } from "@/utils/textEditor";
import RenderEditorContent from "@/components/ui/RenderEditorContent";

import { getFilename, substitutePrivateWithUsername } from "@/utils/directory";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["library", "common", "file", "editAudio"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

const TextEditor = () => {
  const classes = useStyles();
  const { t: c } = useTranslation("common");
  const { t } = useTranslation("file");
  const [initialLoading, setInitialLoading] = useState(false);
  const [data, setData] = useState<LibraryItemInterface>({} as LibraryItemInterface);
  const [content, setContent] = useState("");
  const timeDescription: TimeDescriptionInterface = c("timeDescription", { returnObjects: true });
  const router = useRouter();
  const { id } = router.query;
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;
  const realPath = atob(String(id));
  const title = atob(String(id));

  useEffect(() => {
    setInitialData();
  }, []);

  const setInitialData = async () => {
    try {
      setInitialLoading(true);
      let newData = "";
      await existFile(userId, realPath);
      await getFile();
      const result: any = await getFileContents(userId, realPath);
      const bufferValue = await result.data;
      newData = bufferValue;
      const buffer = Buffer.from(newData, "utf8");
      setContent(buffer.toString());
    } catch (e) {
      errorNotFound();
    } finally {
      setInitialLoading(false);
    }
  };

  const errorNotFound = () => {
    toast(t("messages.fileNotFound"), "error");
    router.push("/library");
  };

  const getFile = async () => {
    if (!realPath) return;
    let remoteItem = null;
    let localItem = null;
    try {
      const localResult = await findByFilename(userId, realPath);
      if (localResult) localItem = applyLocalItemInterface(localResult, timeDescription);
    } catch (e) {
      console.log(e);
    }

    try {
      const remoteResult = await getDataFile(userId, realPath, timeDescription);

      if (remoteResult) {
        remoteItem = remoteResult;
      }
    } catch (e) {
      console.log(e);
    }

    const item = mergeEnvItems(localItem, remoteItem);
    if (item) {
      setData(item);
    } else {
      errorNotFound();
    }
  };

  const save = async (htmlContent: string) => {
    if (htmlContent !== content && htmlContent !== "") {
      try {
        await putFile(userId, realPath, htmlContent);
        await getFile();
      } catch (error) {
        toast(c("genericErrorMessage"), "error");
        console.log(error);
      }
    }
  };

  if (initialLoading) {
    return (
      <Fade in={initialLoading}>
        <SimpleBackdrop open={initialLoading} />
      </Fade>
    );
  }

  return (
    <LayoutApp
      showFooter={false}
      back
      notifications={false}
      title={getFilename(String(title))}
      subtitle={substitutePrivateWithUsername(String(title), userId)}
      rightExtraElement={
        <ContextMenuFile
          key="context-menu"
          data={data}
          setData={setData}
          availableOptions={availableOptions}
        />
      }
    >
      <FlexBox justifyContent={JustifyContentEnum.FLEXSTART} extraStyle={{ marginBottom: "50px" }}>
        <Grid container className="loalrica" style={{ textAlign: "left" }}>
          <Box className={classes.editorWrapper}>
            <RenderEditorContent content={content} save={save} />
          </Box>
        </Grid>
      </FlexBox>
    </LayoutApp>
  );
};

export default TextEditor;
