import React, { useEffect, useMemo } from "react";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { clearCurrentOfflinePage, getCurrentOfflinePage } from "@/utils/offlineNavigation";
import { useRouter } from "next/router";
import { OfflinePagesEnum } from "@/enums/*";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import HoneycombPage from "@/components/pages/honeycomb/HoneycombPage";
import { getHoneycombUrl } from "@/services/talk/chat";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["honeycomb", "library", "call", "virtualStudio"])),
  },
});

function Honeycomb() {
  const connectionStatus = useConnectionStatus();
  const router = useRouter();

  const data = useMemo(() => {
    const pageData = getCurrentOfflinePage();
    if (!pageData || pageData.page !== OfflinePagesEnum.HONEYCOMB_CHAT) {
      router.push("/");

      return { token: null, displayName: null, canDeleteConversation: null };
    }

    return pageData.data;
  }, [router]);

  useEffect(() => {
    if (connectionStatus) {
      router.replace(getHoneycombUrl(data.token, data.displayName, data.canDeleteConversation));
    }

    return () => {
      clearCurrentOfflinePage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connectionStatus]);

  if (!data) return null;

  return (
    <HoneycombPage
      token={data.token}
      displayName={data.displayName}
      canDeleteConversation={data.canDeleteConversation}
    />
  );
}

export default Honeycomb;
