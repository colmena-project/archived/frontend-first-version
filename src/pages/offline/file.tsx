import React, { useEffect, useMemo } from "react";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import FilePage from "@/components/pages/file/FilePage";
import { clearCurrentOfflinePage, getCurrentOfflinePage } from "@/utils/offlineNavigation";
import { useRouter } from "next/router";
import { OfflinePagesEnum } from "@/enums/*";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["file", "library"])),
  },
});

function File() {
  const connectionStatus = useConnectionStatus();
  const router = useRouter();

  const data = useMemo(() => {
    const pageData = getCurrentOfflinePage();
    if (!pageData || pageData.page !== OfflinePagesEnum.FILE) {
      router.push("/");

      return { id: null };
    }

    return pageData.data;
  }, [router]);

  useEffect(() => {
    if (connectionStatus) {
      router.replace(`/file/${data.id}`);
    }

    return () => {
      clearCurrentOfflinePage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connectionStatus]);

  if (!data) return null;

  return <FilePage id={String(data.id)} />;
}

export default File;
