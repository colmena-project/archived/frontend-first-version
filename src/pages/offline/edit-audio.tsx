import React, { useEffect, useMemo } from "react";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import {
  clearCurrentOfflinePage,
  getCurrentOfflinePage,
  getEditAudioLink,
} from "@/utils/offlineNavigation";
import { useRouter } from "next/router";
import { OfflinePagesEnum } from "@/enums/*";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import EditAudioPage from "@/components/pages/edit-audio/EditAudioPage";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["editAudio", "common", "recording"])),
  },
});

function EditAudio() {
  const connectionStatus = useConnectionStatus();
  const router = useRouter();

  const data = useMemo(() => {
    const pageData = getCurrentOfflinePage();
    if (!pageData || pageData.page !== OfflinePagesEnum.EDIT_AUDIO) {
      router.push("/");

      return { id: null };
    }

    return pageData.data;
  }, [router]);

  useEffect(() => {
    if (connectionStatus) {
      router.replace(getEditAudioLink(data.id, connectionStatus));
    }

    return () => {
      clearCurrentOfflinePage();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connectionStatus]);

  if (!data) return null;

  return <EditAudioPage id={String(data.id)} />;
}

export default EditAudio;
