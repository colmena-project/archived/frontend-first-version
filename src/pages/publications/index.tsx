import React, { useState } from "react";
import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";

import LayoutApp from "@/components/statefull/LayoutApp";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import PublicationList from "@/components/pages/publication/PublicationList";

import SearchInput from "@/components/ui/SearchInput";
import FlexBox from "@/components/ui/FlexBox";
import { JustifyContentEnum } from "@/enums/*";
import ToolsMenu from "@/components/ui/ToolsMenu";
import { useTheme } from "@material-ui/core/styles";
import PublicationFiltersDrawer from "@/components/pages/publication/FiltersDrawer";
import { PublicationFiltersProps } from "@/services/cms/publications";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import IconButton from "@/components/ui/IconButtonV2";
import { SearchRoot, SearchWrapper } from "@/components/pages/publication/styled";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publication"])),
  },
});

export default function Publications() {
  const { t } = useTranslation("publication");

  const theme = useTheme();
  const [openSearch, setOpenSearch] = useState(false);
  const [openFilters, setOpenFilters] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const language = userRdx.user.language || "en";
  const [filters, setFilters] = useState<PublicationFiltersProps>({
    order: "publishedAt:desc",
    index: 0,
  });

  const handleOpenSearch = () => {
    setOpenSearch(!openSearch);
  };

  const handleCloseSearch = () => {
    setOpenSearch(false);
    setFilters({ ...filters, keyword: undefined });
  };

  const handleOpenFilters = async () => {
    setOpenFilters(true);
  };

  const handleFilter = (filters: PublicationFiltersProps) => {
    setFilters(filters);
    setOpenSearch(false);
  };

  const handleSearch = (keyword: string) => {
    setFilters({ ...filters, keyword });
  };

  // const clearFilters = () => {
  //   setFilters({} as PublicationFiltersProps);
  // };

  return (
    <LayoutApp
      title={t("publicationsTitle")}
      rightExtraElement={[
        <IconButton
          aria-label="search"
          handleClick={handleOpenSearch}
          icon="search"
          color={theme.palette.primary.main}
        />,
        <IconButton
          aria-label="filter"
          handleClick={handleOpenFilters}
          icon="settings_adjust"
          color={theme.palette.primary.main}
        />,
      ]}
    >
      <FlexBox
        role="main"
        justifyContent={JustifyContentEnum.FLEXSTART}
        extraStyle={{ textAlign: "left" }}
        padding={0}
      >
        {openSearch && (
          <SearchRoot>
            <SearchWrapper>
              <SearchInput
                search={handleSearch}
                cancel={handleCloseSearch}
                placeholder={t("searchLabel")}
                searchIntervalMs={600}
                focus
              />
            </SearchWrapper>
          </SearchRoot>
        )}
        <PublicationList filters={filters} height="calc(100vh - 140px)" />
        <ToolsMenu />
      </FlexBox>
      <PublicationFiltersDrawer
        open={openFilters}
        handleClose={() => setOpenFilters(false)}
        filterPublications={handleFilter}
        initialFilters={filters}
        language={language}
      />
    </LayoutApp>
  );
}
