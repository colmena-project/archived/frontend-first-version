import React, { useCallback, useEffect, useMemo, useState } from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import LayoutApp from "@/components/statefull/LayoutApp";
import { I18nInterface, NewPublicationFile } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import FlexBox from "@/components/ui/FlexBox";
import {
  ContextMenuOptionEnum,
  CurrentPublicationStateEnum,
  EnvironmentEnum,
  JustifyContentEnum,
  TextVariantEnum,
} from "@/enums/*";
import { makeStyles } from "@material-ui/core";

import Text from "@/components/ui/Text";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { getPublication, updatePublication } from "@/store/idb/models/publications";
import PublicationFooterComponent from "@/components/pages/publications/common/PublicationFooter";
import { parseISO } from "date-fns";
import { formatDateToLanguage } from "@/utils/utils";
import NotAllowedToPublishModal from "@/components/pages/publications/NotAllowedToPublishModal";
import { useMutation } from "@apollo/client";
import { SAVE_PUBLICATION_AS_DRAFT } from "@/services/cms/publications";
import SuccessModal from "@/components/pages/publications/common/SuccessModal";
import { toast } from "@/utils/notifications";
import { isSubadminProfile } from "@/utils/permissions";
import Backdrop from "@/components/ui/Backdrop";
import PublicationHeader from "@/components/pages/publication/PublicationHeader";
import PublicationContent from "@/components/pages/publication/PublicationContent";
import useUploadPublicationFiles from "@/hooks/useUploadPublicationFiles";
import PublicationTags from "@/components/pages/publication/PublicationTags";
import FileMetadataCollapse from "@/components/ui/FileMetadataCollapse";
import Box from "@material-ui/core/Box";
import ContextMenuOptions from "@/components/pages/library/contextMenu";

type LocalPublication = {
  id: number;
  title: string;
  description: string;
  content: string;
  category: string;
  status: "DRAFT" | "SENT";
  thumbnail?: File;
  publisher: string;
  userId: string;
  rights: string;
  date: string;
  createdAt: string;
  categoryName: string;
  tags: string;
  files: NewPublicationFile[];
  language: string;
};

function ViewPublication() {
  const [inProgress, setInProgress] = useState(false);
  const [notAllowedModalOpen, setNotAllowedModalOpen] = useState(false);
  const [publication, setPublication] = useState<LocalPublication | null>(null);
  const [cmsMode, setCMSMode] = useState("DRAFT");

  const { t } = useTranslation("publications");
  const { t: c } = useTranslation("common");

  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const classes = useClasses();
  const router = useRouter();
  const { runWorker } = useUploadPublicationFiles();
  const { publicationId } = router.query;
  const [
    saveAsDraft,
    { loading: saving, data: saveAsDraftResponse, reset: resetDraft, error: errorOnDraft },
  ] = useMutation(SAVE_PUBLICATION_AS_DRAFT);

  const fetchPublication = useCallback(async () => {
    const publication = await getPublication(Number(publicationId));

    setPublication(publication);
  }, [publicationId]);

  useEffect(() => {
    fetchPublication();
  }, [fetchPublication]);

  const publishedAt = useMemo(() => {
    if (!publication) return "";

    const date = parseISO(publication.date);

    return formatDateToLanguage(date, userRdx.user.language);
  }, [publication, userRdx.user.language]);

  const isAdmin = isSubadminProfile();

  const dispatchWorker = useCallback(
    async (publicationId: number, refId: string) => {
      await updatePublication(publicationId, {
        status: CurrentPublicationStateEnum.NOT_SENT,
        refId,
        sentAt: new Date(),
        cmsMode,
      });
      runWorker({ publicationId, cmsMode });
    },
    [runWorker, cmsMode],
  );

  const handleSavePublication = useCallback(async () => {
    setNotAllowedModalOpen(false);

    if (publication) {
      setInProgress(true);

      await saveAsDraft({
        variables: {
          title: publication.title,
          description: publication.description,
          coverage: publication.description,
          publisher: publication.publisher,
          rights: publication.rights,
          date: parseISO(publication.date),
          content: publication.content,
          creator: publication.userId,
          subject: publication.category,
          relation: null,
          language: publication.language,
        },
      });

      setInProgress(false);
    }
  }, [publication, saveAsDraft]);

  const handleCloseAndBackHome = useCallback(() => {
    resetDraft();

    router.push("/home", "", {
      locale: userRdx.user.language,
    });
  }, [resetDraft, userRdx.user.language, router]);

  useEffect(() => {
    if (saveAsDraftResponse) {
      dispatchWorker(Number(publicationId), saveAsDraftResponse.createPublication.data.id);
    }
  }, [saveAsDraftResponse, dispatchWorker, publicationId]);

  useEffect(() => {
    if (errorOnDraft) {
      toast(t("preview.errorOnDraft"), "error");
      resetDraft();
    }
  }, [t, errorOnDraft, resetDraft]);

  const toggleNotAllowedModal = () => {
    setNotAllowedModalOpen((old) => !old);
  };

  const handleCheckPublicationStatus = useCallback(() => {
    router.push("/publications/management");
  }, [router]);

  const handleOnPublish = () => {
    if (isAdmin) {
      setCMSMode(CurrentPublicationStateEnum.PUBLISH);
      handleSavePublication();
    } else toggleNotAllowedModal();
  };

  if (!publication) {
    return (
      <LayoutApp title={t("preview.title")} showFooter={false} drawer={false} notifications={false}>
        <Backdrop open className={classes.backdrop} />
        <PublicationFooterComponent
          nextLabel={c("form.finishButton")}
          cancelLabel={c("form.backButton")}
          onClickCancel={() => router.back()}
          currentStep={3}
          totalStep={3}
          onPublish={handleOnPublish}
          paddingBottom={58}
          onSaveDraft={() => {
            handleSavePublication();
            setCMSMode(CurrentPublicationStateEnum.DRAFT);
          }}
          loading={saving}
        />
      </LayoutApp>
    );
  }

  let imageUrl;

  if (publication.thumbnail) {
    imageUrl = URL.createObjectURL(publication.thumbnail);
  }

  return (
    <LayoutApp
      title={t("preview.title")}
      showFooter={false}
      drawer={false}
      notifications={false}
      fontSizeTitle={18}
      rightExtraElement={
        <ContextMenuOptions
          basename={router.pathname}
          aliasFilename=""
          filename=""
          environment={"" as EnvironmentEnum}
          id=""
          availableOptions={[ContextMenuOptionEnum.MANAGE_PUBLICATIONS]}
          onChange={() => {
            //
          }}
        />
      }
    >
      <FlexBox
        justifyContent={JustifyContentEnum.FLEXSTART}
        padding={0}
        extraStyle={{ alignItems: "flex-start", textAlign: "left" }}
        paddingBottom={48}
      >
        <PublicationHeader
          image={imageUrl}
          creator={publication.userId}
          publishedAt={publishedAt}
          categoryName={publication.categoryName}
          publisher={publication.publisher}
          publicationsUrl="#"
          baseUrl=""
          showShareButton={false}
          publicationDescription={publication.description}
          publicationId={publicationId as string}
          publicationTitle={publication.title}
        />
        <PublicationContent title={publication.title} content={publication.content} />
        <PublicationTags tags={publication.tags} />
        <Box className={classes.files}>
          {publication.files &&
            publication.files.map(
              ({
                localId,
                media,
                tags,
                extentSize,
                creator,
                language,
                description,
                title,
              }: NewPublicationFile) => (
                <FileMetadataCollapse
                  isCollapsed={false}
                  showCollapseButton={false}
                  key={localId}
                  metadata={{
                    file: media,
                    fileId: localId,
                    size: extentSize,
                    tags,
                    mime: media.type,
                    producedBy: creator,
                    creator,
                    language,
                    description,
                    title,
                  }}
                />
              ),
            )}
        </Box>

        <Text variant={TextVariantEnum.CAPTION} className={classes.rights}>
          {t("createPublication.form.license.label")}: {publication?.rights}
        </Text>
      </FlexBox>

      <NotAllowedToPublishModal
        open={notAllowedModalOpen}
        onCancel={toggleNotAllowedModal}
        onSaveAsDraft={() => {
          handleSavePublication();
          setCMSMode(CurrentPublicationStateEnum.DRAFT);
        }}
      />

      <SuccessModal
        onClickPrimaryAction={handleCheckPublicationStatus}
        onClickSecondaryAction={handleCloseAndBackHome}
        onClose={handleCloseAndBackHome}
        open={Boolean(saveAsDraftResponse)}
        primaryLabel={t("preview.goToManagement")}
        secondaryLabel={t("preview.savedModal.secodaryAction")}
      />

      <Box className={classes.clearFix}></Box>
      <PublicationFooterComponent
        nextLabel={c("form.finishButton")}
        cancelLabel={c("form.backButton")}
        onClickCancel={() => router.back()}
        currentStep={3}
        totalStep={3}
        onPublish={handleOnPublish}
        onSaveDraft={() => {
          handleSavePublication();
          setCMSMode(CurrentPublicationStateEnum.DRAFT);
        }}
        loading={saving}
      />
      <Backdrop open={saving || inProgress} className={classes.backdrop} />
    </LayoutApp>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publications", "publication"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

const useClasses = makeStyles((theme) => ({
  rights: {
    color: theme.palette.gray.main,
    margin: "10px",
  },
  backdrop: {
    zIndex: 999999,
  },
  files: {
    padding: "0 10px",
    width: "100%",
  },
  clearFix: {
    minHeight: 107,
    width: "100%",
    display: "flex",
  },
}));

export default ViewPublication;
