/* eslint-disable indent */
import React, { useCallback, useEffect, useState } from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import LayoutApp from "@/components/statefull/LayoutApp";
import { CurrentPublication, I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import PublicationFilesForm from "@/components/pages/publications/files/PublicationFilesForm";
import { isSubadminProfile } from "@/utils/permissions";
import ContextMenuOptions from "@/components/pages/library/contextMenu";
import { ContextMenuOptionEnum, EnvironmentEnum } from "@/enums/*";
import { getPublication } from "@/store/idb/models/publications";
import SimpleBackdrop from "@/components/ui/Backdrop";

function PublicationFile() {
  const [isLoading, setIsLoading] = useState(false);
  const [currentPublication, setCurrentPublication] = useState<CurrentPublication | null>(null);
  const { t } = useTranslation("publications");

  const router = useRouter();
  const { publicationId } = router.query;

  const onSubmit = useCallback(async () => {
    setIsLoading(false);
    router.push(`/publications/${publicationId}`);
  }, [publicationId, router]);

  useEffect(() => {
    if (Number(publicationId)) {
      (async () => {
        setIsLoading(true);

        const publication = await getPublication(Number(publicationId));

        setCurrentPublication(publication);
        setIsLoading(false);
      })();
    }
  }, [publicationId]);

  const isAdmin = isSubadminProfile();

  return (
    <LayoutApp
      title={t("fileManager.title")}
      showFooter={false}
      drawer={false}
      fontSizeTitle={18}
      notifications={false}
      rightExtraElement={
        isAdmin ? (
          <ContextMenuOptions
            basename={router.pathname}
            aliasFilename=""
            filename=""
            environment={"" as EnvironmentEnum}
            id=""
            availableOptions={[ContextMenuOptionEnum.MANAGE_PUBLICATIONS]}
            onChange={() => {
              //
            }}
          />
        ) : undefined
      }
    >
      <SimpleBackdrop open={isLoading} />
      {currentPublication && (
        <PublicationFilesForm
          onSubmit={onSubmit}
          isLoading={isLoading}
          publicationId={Number(publicationId)}
          currentPublication={currentPublication}
        />
      )}
    </LayoutApp>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publications", "common"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

export default PublicationFile;
