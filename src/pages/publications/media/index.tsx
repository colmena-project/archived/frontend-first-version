import React, { useEffect, useState } from "react";
import { GetStaticProps } from "next";
import { I18nInterface, MediaFiltersProps } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { Box, makeStyles } from "@material-ui/core";
import SearchInput from "@/components/ui/SearchInput";
import { useTranslation } from "next-i18next";
import { getGroups } from "@/services/internal/groups";
import { GroupInterface } from "@/interfaces/keycloak";
import LayoutApp from "@/components/statefull/LayoutApp";
import Clickable from "@/components/ui/Clickable";
import SvgIconAux from "@/components/ui/SvgIcon";
import { useTheme } from "@material-ui/core/styles";
import { LanguageProps } from "@/types/*";
import MediaList from "@/components/pages/publication/media/MediaList";
import FlexBox from "@/components/ui/FlexBox";
import { JustifyContentEnum } from "@/enums/*";
import MediaFiltersDrawer from "@/components/pages/publication/media/FiltersDrawer";
import { empty, getSystemLanguages, prepareSearchString } from "@/utils/utils";
import Loading from "@/components/ui/Loading";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publication", "publications"])),
  },
});

const useStyles = makeStyles((theme) => ({
  searchContent: {
    padding: "15px 10px",
    boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    width: "100%",
  },
  filter: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "0 5px 0 13px",
  },
  filtersDescription: {
    color: theme.palette.secondary.main,
  },
  select: {
    minWidth: 200,
    width: "100%",
  },
}));

export default function Medias() {
  const classes = useStyles();
  const theme = useTheme();
  const { t } = useTranslation("publication");
  const { t: c } = useTranslation("common");
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");
  const [medias, setMedias] = useState<GroupInterface[] | undefined>();
  const [items, setItems] = useState<GroupInterface[]>([]);
  const [languages, setLanguages] = useState<LanguageProps[] | undefined>();

  const [openFilters, setOpenFilters] = useState(false);
  const [openSearch, setOpenSearch] = useState(false);
  const [filters, setFilters] = useState<MediaFiltersProps>({
    country: "",
    language: "",
  });

  const handleOpenSearch = () => {
    setOpenSearch(!openSearch);
  };

  const handleCloseSearch = () => {
    setOpenSearch(false);
    setSearch("");
  };

  const handleFilter = (filters: MediaFiltersProps) => {
    setFilters(filters);
  };

  const filterItems = (medias: GroupInterface[], filters: MediaFiltersProps) => {
    let filteredMedias = medias;
    if (filters.country) {
      filteredMedias = filteredMedias.filter(
        (media) =>
          media.attributes?.country &&
          media.attributes.country.length > 0 &&
          !empty(media.attributes.country[0]) &&
          media.attributes.country[0] === filters.country,
      );
    }

    if (filters.language) {
      filteredMedias = filteredMedias.filter(
        (media) =>
          media.attributes?.language &&
          media.attributes.language.length > 0 &&
          !empty(media.attributes.language[0]) &&
          media.attributes.language[0] === filters.language,
      );
    }

    return filteredMedias;
  };

  const searchItems = (medias: GroupInterface[], keyword: string) =>
    medias.filter((item: GroupInterface) => {
      const value = prepareSearchString(keyword);
      const groupName = prepareSearchString(item.name);
      return groupName.indexOf(value) >= 0;
    });

  useEffect(() => {
    (async () => {
      setLoading(true);
      const groups = await getGroups();
      if (groups?.data?.data) {
        setMedias(groups.data.data);

        const allLanguages = getSystemLanguages(c);
        const languages: Array<LanguageProps> = [];
        groups.data.data.forEach((group) => {
          const { attributes } = group;
          if (
            attributes?.language &&
            attributes.language.length > 0 &&
            !empty(attributes.language[0]) &&
            !languages.find((language) => language.abbr === attributes.language[0])
          ) {
            const language = allLanguages.find((row) => row.abbr.includes(attributes.language[0]));
            if (language) {
              languages.push(language);
            }
          }
        });

        setLanguages(languages);
      }
      setLoading(false);
    })();
  }, [c]);

  useEffect(() => {
    if (medias) {
      let filteredMedias = medias;
      filteredMedias = filterItems(filteredMedias, filters);

      if (search) {
        filteredMedias = searchItems(filteredMedias, search);
      }

      filteredMedias.sort((a, b) => (a.name > b.name ? -1 : 0));

      setItems(filteredMedias);
    }
  }, [search, filters, medias]);

  return (
    <LayoutApp
      title={t("mediasTitle")}
      rightExtraElement={[
        <Clickable className={classes.filter} handleClick={handleOpenSearch} key="search">
          <SvgIconAux icon="search" fontSize={20} htmlColor={theme.palette.primary.main} />
        </Clickable>,
        <Clickable className={classes.filter} handleClick={() => setOpenFilters(true)} key="filter">
          <SvgIconAux icon="settings_adjust" fontSize={20} htmlColor={theme.palette.primary.main} />
        </Clickable>,
      ]}
    >
      <FlexBox
        justifyContent={JustifyContentEnum.FLEXSTART}
        extraStyle={{ textAlign: "left" }}
        padding={0}
      >
        {openSearch && (
          <Box className={classes.searchContent}>
            <SearchInput
              search={setSearch}
              cancel={handleCloseSearch}
              placeholder={t("searchLabel")}
              focus
            />
          </Box>
        )}

        {loading && (
          <Box padding={1}>
            <Loading />
          </Box>
        )}

        {medias && !loading && <MediaList medias={items} />}
      </FlexBox>
      <MediaFiltersDrawer
        open={openFilters}
        handleClose={() => setOpenFilters(false)}
        filterMedias={handleFilter}
        initialFilters={filters}
        languages={languages}
      />
    </LayoutApp>
  );
}
