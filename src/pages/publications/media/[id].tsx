import React, { useEffect, useState } from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import LoadingPage from "@/components/ui/LoadingPage";
import { Box, makeStyles } from "@material-ui/core";
import SearchInput from "@/components/ui/SearchInput";
import PublicationList from "@/components/pages/publication/PublicationList";
import { useTranslation } from "next-i18next";
import MediaGroupHeader from "@/components/pages/publication/MediaGroupHeader";
import { getGroupByName } from "@/services/internal/groups";
import { GroupInterface } from "@/interfaces/keycloak";
import LayoutApp from "@/components/statefull/LayoutApp";
import { PublicationFiltersProps } from "@/services/cms/publications";
import PublicationFiltersDrawer from "@/components/pages/publication/FiltersDrawer";
import Clickable from "@/components/ui/Clickable";
import SvgIconAux from "@/components/ui/SvgIcon";
import { useTheme } from "@material-ui/core/styles";
import { PublicationFiltersEnum } from "@/enums/*";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publication", "publications"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

const useStyles = makeStyles((theme) => ({
  searchContent: {
    padding: "15px 10px",
    boxShadow: "0px 5px 5px 0px rgba(0,0,0,0.1)",
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  filter: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "0 5px 0 13px",
  },
  filtersDescription: {
    color: theme.palette.secondary.main,
  },
}));

export default function Media() {
  const classes = useStyles();
  const router = useRouter();
  const theme = useTheme();
  const { id } = router.query;
  const { t } = useTranslation("publication");
  const [loadingMediaGroup, setLoadingMediaGroup] = useState(true);
  const [mediaGroup, setMediaGroup] = useState<GroupInterface | false>();
  if (typeof id !== "string") {
    return router.push("/publications");
  }

  const [openFilters, setOpenFilters] = useState(false);
  const [openSearch, setOpenSearch] = useState(false);
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const language = userRdx.user.language || "en";
  const [filters, setFilters] = useState<PublicationFiltersProps>({
    group: id,
    order: "publishedAt:desc",
    index: 0,
  });

  const handleOpenSearch = () => {
    setOpenSearch(!openSearch);
  };

  const handleCloseSearch = () => {
    setOpenSearch(false);
    setFilters({ ...filters, keyword: undefined });
  };

  const handleOpenFilters = async () => {
    setOpenFilters(true);
  };

  const handleFilter = (filters: PublicationFiltersProps) => {
    setFilters(filters);
    setOpenSearch(false);
  };

  const handleSearch = (keyword: string) => {
    setFilters({ ...filters, keyword });
  };

  useEffect(() => {
    (async () => {
      setLoadingMediaGroup(true);
      const group = await getGroupByName(id);
      setMediaGroup(group);
      setLoadingMediaGroup(false);
    })();
  }, [id]);

  if (loadingMediaGroup) return <LoadingPage />;

  if (!mediaGroup) {
    router.push("/publications");
    return <LoadingPage />;
  }

  return (
    <LayoutApp
      title={t("publicationsTitle")}
      rightExtraElement={[
        <Clickable className={classes.filter} handleClick={handleOpenSearch} key="search">
          <SvgIconAux icon="search" fontSize={20} htmlColor={theme.palette.primary.main} />
        </Clickable>,
        <Clickable className={classes.filter} handleClick={handleOpenFilters} key="filter">
          <SvgIconAux icon="settings_adjust" fontSize={20} htmlColor={theme.palette.primary.main} />
        </Clickable>,
      ]}
    >
      <MediaGroupHeader
        mediaName={mediaGroup.name}
        socialMedias={mediaGroup.attributes}
        token={mediaGroup.id}
      />
      {openSearch && (
        <Box className={classes.searchContent}>
          <SearchInput
            search={handleSearch}
            cancel={handleCloseSearch}
            placeholder={t("searchLabel")}
            focus
          />
        </Box>
      )}
      <PublicationList filters={filters} height="calc(100vh - 244px)" />
      <PublicationFiltersDrawer
        open={openFilters}
        handleClose={() => setOpenFilters(false)}
        filterPublications={handleFilter}
        initialFilters={filters}
        hideFilters={[PublicationFiltersEnum.GROUP]}
        language={language}
      />
    </LayoutApp>
  );
}
