import React, { useEffect } from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import PublicationHeader from "@/components/pages/publication/PublicationHeader";
import PublicationContent from "@/components/pages/publication/PublicationContent";
import PublicationTags from "@/components/pages/publication/PublicationTags";
import LayoutApp from "@/components/statefull/LayoutApp";
import { useTranslation } from "next-i18next";
import FlexBox from "@/components/ui/FlexBox";
import { JustifyContentEnum } from "@/enums/*";
import usePublication from "@/hooks/cms/usePublication";
import LoadingPage from "@/components/ui/LoadingPage";
import { formatDateToLanguage, getBaseUrl, getUrlStrapiImage } from "@/utils/utils";
import { parseISO } from "date-fns";
import PublicationFiles from "@/components/pages/publication/PublicationFiles";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publication", "publications"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

export default function Publication() {
  const router = useRouter();
  const { id } = router.query;

  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  if (typeof id !== "string") {
    return router.push("/publications");
  }

  const { publication, loading } = usePublication(id, userRdx.user);
  const { t } = useTranslation("publication");

  useEffect(() => {
    if (!loading && !publication) {
      router.push("/publications");
    }
  }, [router, loading, publication]);

  if (loading) return <LoadingPage />;

  if (!publication) {
    return <></>;
  }

  const baseUrl = getBaseUrl();

  const { attributes } = publication;
  const {
    publisher,
    subject,
    publishedAt,
    relation,
    creator,
    title,
    content,
    tags,
    files,
    description,
  } = attributes;
  let formattedDate = "";

  if (publishedAt) {
    formattedDate = formatDateToLanguage(parseISO(publishedAt), userRdx.user.language);
  }

  let categoryName;
  if (subject?.data?.attributes?.name) {
    categoryName = subject.data.attributes.name;
  }

  const image = getUrlStrapiImage(relation, "medium");

  return (
    <>
      <LayoutApp title={t("publicationTitle")} notifications={false} back>
        <FlexBox
          component="main"
          role="main"
          justifyContent={JustifyContentEnum.FLEXSTART}
          padding={0}
          extraStyle={{
            alignItems: "flex-start",
            textAlign: "left",
          }}
        >
          <PublicationHeader
            image={image}
            creator={creator}
            publishedAt={formattedDate}
            categoryName={categoryName}
            publisher={publisher}
            data-testid="publication-header"
            baseUrl={baseUrl}
            publicationDescription={description}
            publicationTitle={title}
            publicationId={publication.id}
          />
          <PublicationContent title={title} content={content} />
          <PublicationTags tags={tags} />
          <PublicationFiles files={files?.data} />
        </FlexBox>
      </LayoutApp>
    </>
  );
}
