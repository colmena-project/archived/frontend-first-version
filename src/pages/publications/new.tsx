/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable indent */
import React, { useCallback, useEffect, useRef, useState } from "react";
import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import LayoutApp from "@/components/statefull/LayoutApp";
import { CurrentPublication, I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import FlexBox from "@/components/ui/FlexBox";
import {
  AlignItemsEnum,
  ContextMenuOptionEnum,
  CurrentPublicationStateEnum,
  EnvironmentEnum,
  JustifyContentEnum,
  TextAlignEnum,
} from "@/enums/*";
import PublicationFooterComponent from "@/components/pages/publications/common/PublicationFooter";
import { useRouter } from "next/router";
import NewPublicationForm, {
  FormValues,
} from "@/components/pages/publications/NewPublicationForm.component";
import { PropsUserSelector } from "@/types/*";
import {
  createPublication,
  getCurrentPublication,
  updatePublication,
} from "@/store/idb/models/publications";
import { FormikHelpers } from "formik";
import { useSelector } from "react-redux";
import SimpleBackdrop from "@/components/ui/Backdrop";
import { reduceImageSize, getImageSize } from "@/utils/utils";
import ContextMenuOptions from "@/components/pages/library/contextMenu";

type CreatePublication = {
  title: string;
  description: string;
  content: string;
  category: number;
  status: "CREATING";
  thumbnail?: File;
  publisher: string;
  userId: string;
  rights: string;
  date: string;
  createdAt: string;
  categoryName: string;
  mode: "DRAFT";
  cmsMode: undefined;
  language: string;
};

function NewPublication() {
  const [loadingPage, setLoadingPage] = useState(true);
  const [publicationRecovered, setPublicationRecovered] = useState<CurrentPublication | null>(null);
  const [loading, setLoading] = useState(false);
  const { t } = useTranslation("publications");
  const { t: c } = useTranslation("common");

  const submitButtonRef = useRef<HTMLButtonElement | null>(null);

  const route = useRouter();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const fetchCurrentPublication = useCallback(async () => {
    setLoadingPage(true);

    const recovered = await getCurrentPublication();

    setPublicationRecovered(recovered);
    setLoadingPage(false);
  }, []);

  const onSubmit = useCallback(
    async (values: FormValues, { resetForm }: FormikHelpers<FormValues>) => {
      setLoading(true);
      const [categoryid, categoryName] = values.category.split("-");

      let thumbnail: File;
      if (values?.image) {
        const image = values.image as File;
        const { width } = await getImageSize(image);

        if (width > 1200) {
          thumbnail = await reduceImageSize(image, 90);
        } else {
          thumbnail = image;
        }
      }

      const createPublicationLocal: CreatePublication = {
        title: values.title,
        description: values.description,
        content: values.content,
        category: Number(categoryid),
        rights: values.license,
        date: values.publicationDate.toISOString(),
        publisher: userRdx.user.media?.name,
        userId: userRdx.user.id,
        status: CurrentPublicationStateEnum.CREATING,
        // @ts-ignore
        thumbnail,
        createdAt: new Date().toISOString(),
        categoryName,
        mode: CurrentPublicationStateEnum.DRAFT,
        cmsMode: undefined,
        language: userRdx.user.language,
      };

      let currentPublicationId: number | null = publicationRecovered?.id ?? null;
      const currentPublicationData = publicationRecovered ?? {};

      if (currentPublicationId) {
        await updatePublication(currentPublicationId, {
          ...currentPublicationData,
          ...createPublicationLocal,
        });
      } else {
        currentPublicationId = (await createPublication({
          ...currentPublicationData,
          ...createPublicationLocal,
        })) as number;
      }

      resetForm();

      route.push(`/publications/files/${currentPublicationId}`);
    },
    [userRdx, route, publicationRecovered],
  );

  useEffect(() => {
    fetchCurrentPublication();
  }, [fetchCurrentPublication]);

  if (loadingPage) {
    return (
      <LayoutApp title={t("createPublication.title")} showFooter={false} drawer={false}>
        <SimpleBackdrop open={loading} />

        <PublicationFooterComponent
          nextLabel={c("form.nextIntro")}
          cancelLabel={c("form.cancelButton")}
          onClickCancel={() => route.back()}
          currentStep={1}
          totalStep={3}
          onClickNext={() => submitButtonRef?.current?.click()}
          paddingBottom={58}
          loading={loading}
        />
      </LayoutApp>
    );
  }

  return (
    <LayoutApp
      title={t("createPublication.title")}
      showFooter={false}
      fontSizeTitle={18}
      drawer
      notifications={false}
      rightExtraElement={
        <ContextMenuOptions
          basename={route.pathname}
          aliasFilename=""
          filename=""
          environment={"" as EnvironmentEnum}
          id=""
          availableOptions={[ContextMenuOptionEnum.MANAGE_PUBLICATIONS]}
          onChange={() => {
            //
          }}
        />
      }
    >
      <SimpleBackdrop open={loading} />
      <FlexBox
        justifyContent={JustifyContentEnum.FLEXSTART}
        alignItems={AlignItemsEnum.STRETCH}
        textAlign={TextAlignEnum.LEFT}
        extraStyle={{ paddingBottom: 120 }}
      >
        <NewPublicationForm
          submitButtonRef={submitButtonRef}
          onSubmit={onSubmit}
          language={userRdx.user.language}
          userId={userRdx.user.id}
          currentPublication={publicationRecovered}
        />
      </FlexBox>

      <PublicationFooterComponent
        nextLabel={c("form.nextIntro")}
        cancelLabel={c("form.cancelButton")}
        onClickCancel={() => route.back()}
        currentStep={1}
        totalStep={3}
        onClickNext={() => submitButtonRef?.current?.click()}
        paddingBottom={58}
        loading={loading}
      />
    </LayoutApp>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publications"])),
  },
});

export default NewPublication;
