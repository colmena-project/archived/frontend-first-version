/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable indent */
import React from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import EditPublication from "@/components/pages/publications/edit/EditPublication";
import useCategories from "@/hooks/cms/useCategories";
import usePublication from "@/hooks/cms/usePublication";
import { PublicationLayout } from "@/components/pages/publications/common/styled";
import SimpleBackdrop from "@/components/ui/Backdrop";
import { PublicationQueryInterface } from "@/interfaces/cms";
import { useSelector } from "react-redux";
import { PropsUserSelector } from "@/types/*";

function EditPublicationPage({ publication }: { publication: PublicationQueryInterface }) {
  const router = useRouter();
  const { id } = router.query;

  const { t } = useTranslation("publications");

  const { categories, loading: loadingCategories } = useCategories(publication.attributes.language);

  if (loadingCategories) {
    return (
      <PublicationLayout title={t("edit.titles.step1")} showFooter={false} drawer={false}>
        <SimpleBackdrop open />
      </PublicationLayout>
    );
  }

  return (
    <EditPublication
      publicationId={id as string}
      publication={publication}
      categories={categories}
    />
  );
}

function EditPublicationPageContainer() {
  const router = useRouter();
  const { id } = router.query;

  const { t } = useTranslation("publications");
  const { user } = useSelector((state: { user: PropsUserSelector }) => state.user);
  const { publication, loading } = usePublication(id as string, user);

  return loading || !publication ? (
    <PublicationLayout title={t("edit.titles.step1")} showFooter={false} drawer={false}>
      <SimpleBackdrop open />
    </PublicationLayout>
  ) : (
    <EditPublicationPage publication={publication} />
  );
}

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publications", "publication"])),
  },
});

export default EditPublicationPageContainer;
