/* eslint-disable react/jsx-indent */
/* eslint-disable indent */
import React, { useCallback, useEffect, useState } from "react";
import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import LayoutApp from "@/components/statefull/LayoutApp";
import { I18nInterface, CurrentPublication } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import FlexBox from "@/components/ui/FlexBox";
import { AlignItemsEnum, JustifyContentEnum, TextAlignEnum } from "@/enums/*";
import { Box, makeStyles, useTheme } from "@material-ui/core";
import { toast } from "@/utils/notifications";
import { getNotSentPublications, removeFileFromPublication } from "@/store/idb/models/publications";
import SimpleBackdrop from "@/components/ui/Backdrop";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import PublicationItem from "@/components/pages/publications/management/PublicationItem";
import ManagementTabs from "@/components/pages/publications/management/ManagementTabs";
import FinishedTab from "@/components/pages/publications/management/FinishedTab";
import Clickable from "@/components/ui/Clickable";
import SvgIconAux from "@/components/ui/SvgIcon";
import AlertInfoCenter from "@/components/ui/AlertInfoCenter";

function Management() {
  const [activeTab, setActiveTab] = useState(0);
  const [openSearch, setOpenSearch] = useState(false);
  const [openFilters, setOpenFilters] = useState(false);
  const [publications, setPublications] = useState<CurrentPublication[]>([]);
  const [loading, setLoading] = useState(true);

  const { t } = useTranslation("publications");
  const { t: c } = useTranslation("common");

  const fetchPublications = useCallback(async () => {
    try {
      const data = await getNotSentPublications();

      setPublications(data);
    } catch (err) {
      toast(err.message, "error");
    } finally {
      setLoading(false);
    }
  }, []);

  const onCloseSearch = useCallback(() => {
    setOpenSearch(false);
  }, []);

  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const toggleOpenFilters = useCallback(() => {
    setOpenFilters((old) => !old);
  }, []);

  useEffect(() => {
    setLoading(true);
    fetchPublications();
  }, [fetchPublications]);

  const classes = useClasses();
  const theme = useTheme();

  const onConfirmeDelete = useCallback(
    async (publicationId: number, fileId: string) => {
      setLoading(true);

      try {
        await removeFileFromPublication(publicationId, fileId);

        fetchPublications();
      } catch (err) {
        toast(err.message, "error");
      }
    },
    [fetchPublications],
  );

  const onChangeTab = (newActive: number) => {
    setActiveTab(newActive);
  };

  return (
    <LayoutApp
      title={t("management.title")}
      showFooter
      fontSizeTitle={16}
      fontSizeSubtitle={12}
      drawer={false}
      notifications={false}
      className={classes.appBar}
      rightExtraElement={
        activeTab === 1
          ? [
              <Clickable
                className={classes.filter}
                handleClick={() => setOpenSearch(true)}
                key="search"
              >
                <SvgIconAux icon="search" fontSize={20} htmlColor={theme.palette.primary.main} />
              </Clickable>,

              <Clickable className={classes.filter} handleClick={toggleOpenFilters} key="filter">
                <SvgIconAux
                  icon="settings_adjust"
                  fontSize={20}
                  htmlColor={theme.palette.primary.main}
                />
              </Clickable>,
            ]
          : undefined
      }
      back
    >
      <Box width="100%">
        <ManagementTabs activeTab={activeTab} onChangeTab={onChangeTab} />
      </Box>
      <SimpleBackdrop open={loading} />

      <FlexBox
        justifyContent={JustifyContentEnum.FLEXSTART}
        alignItems={AlignItemsEnum.STRETCH}
        textAlign={TextAlignEnum.LEFT}
        extraStyle={{ paddingBottom: 120 }}
      >
        {activeTab === 0 ? (
          <>
            {publications.map((publication) => (
              <PublicationItem
                publication={publication}
                mediaName={userRdx.user.media.name}
                key={publication.id}
                onConfirmeDelete={onConfirmeDelete}
              />
            ))}

            {publications.length === 0 && !loading && <AlertInfoCenter title={c("noItemsFound")} />}
          </>
        ) : (
          <FinishedTab
            user={userRdx.user}
            openSearch={openSearch}
            onCloseSearch={onCloseSearch}
            openFilters={openFilters}
            onCloseFilters={toggleOpenFilters}
          />
        )}
      </FlexBox>
    </LayoutApp>
  );
}

const useClasses = makeStyles(() => ({
  appBar: {
    "& .MuiTypography-h3": { fontSize: "20px !important", fontWeight: 900 },
    "& .MuiPaper-root": {
      boxShadow: "none",
    },
  },
  filter: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "0 5px 0 13px",
  },
}));

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["publications"])),
  },
});

export default Management;
