/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState } from "react";
import FlexBox from "@/components/ui/FlexBox";
import LayoutApp from "@/components/statefull/LayoutApp";
// import { useTranslation } from "next-i18next";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import { JustifyContentEnum } from "@/enums/index";
import { useDispatch, useSelector } from "react-redux";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import Box from "@material-ui/core/Box";
import { PropsRecordingSelector } from "@/types/*";
import { PlaylistMemoized } from "@/components/ui/WaveformPlaylist/WaveformPlaylist";
import { makeStyles, Theme, useTheme } from "@material-ui/core/styles";
import EventEmitter from "events";
import DisplayInformationOrientation from "@/components/ui/WaveformPlaylist/DisplayInformationOrientation";
import DisplayInformationTitle from "@/components/ui/WaveformPlaylist/DisplayInformationTitle";
import HeaderControls from "@/components/pages/edit-audio/HeaderControls";
import OptionsDone from "@/components/ui/WaveformPlaylist/OptionsDone";
import { updateRecordingState, setRecordingCounter } from "@/store/actions/recordings/index";
import Script from "next/script";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["editAudio", "recording", "common"])),
  },
});

const useStyles = makeStyles((theme: Theme) => ({
  frameWaveform: {
    width: "100%",
    display: "flex",
    paddingBottom: 86,
    flexDirection: "column",
    "& .device-orientation-edit-audio": {
      width: "100%",
      backgroundColor: theme.palette.primary.main,
    },
  },
  flexboxWrapper: {
    padding: "0 !important",
    margin: "0 !important",
    backgroundColor: theme.palette.primary.main,
  },
  centerLoading: {
    display: "flex",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
}));

function Record() {
  const [saved, setSaved] = useState(false);
  const recordingRdx = useSelector(
    (state: { recording: PropsRecordingSelector }) => state.recording,
  );
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const [ee] = useState(new EventEmitter());
  const [path, setPath] = useState("");
  const [filename, setFilename] = useState<string>("");
  const [currentPath, setCurrentPath] = useState("");

  const audioState = recordingRdx.activeRecordingState;

  useEffect(() => {
    dispatch(updateRecordingState("NONE"));
    dispatch(setRecordingCounter(0));

    document.addEventListener("update-path-recorder", (e: CustomEvent<{ path: string }>) => {
      if (!e.detail) return;
      setPath(e.detail.path);
    });
    document.addEventListener(
      "update-filename-recorder",
      (e: CustomEvent<{ filename: string }>) => {
        if (!e.detail) return;
        setFilename(e.detail.filename);
      },
    );
  }, []);

  return (
    <LayoutApp
      templateHeader="variation2"
      showFooter={false}
      title={
        <DisplayInformationTitle
          filename={filename}
          saved={saved}
          setCurrentPath={setCurrentPath}
        />
      }
      rightExtraElement={
        <HeaderControls
          key="audio-controls"
          ee={ee}
          audioState={audioState}
          filename={currentPath}
          context="recorder"
          saved={saved}
        />
      }
      back
      notifications={false}
      backgroundColor={theme.palette.primary.main}
    >
      <FlexBox justifyContent={JustifyContentEnum.FLEXSTART} className={classes.flexboxWrapper}>
        <Box className={classes.frameWaveform}>
          <DisplayInformationOrientation path={path} audioState={audioState} />
          <PlaylistMemoized ee={ee} context="recorder" setCurrentPath={setCurrentPath} />
          <OptionsDone
            ee={ee}
            path={path}
            setSaved={setSaved}
            currentPath={currentPath}
            filename={filename}
            setCurrentPath={setCurrentPath}
          />
        </Box>
      </FlexBox>
      <Script src="https://kit.fontawesome.com/ef69927139.js" />
    </LayoutApp>
  );
}

export default Record;
