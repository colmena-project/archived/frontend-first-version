import React from "react";
import FlexBox from "@/components/ui/FlexBox";
import LayoutApp from "@/components/statefull/LayoutApp";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import { JustifyContentEnum } from "@/enums/index";
import HeaderProfile from "@/components/pages/profile/HeaderProfile";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { makeStyles } from "@material-ui/core/styles";
import ProfileTabs from "@/components/pages/profile/ProfileTabs";
import { Box } from "@material-ui/core";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["profile", "settings"])),
  },
});

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.background.default,
    width: "100%",
  },
}));

function Profile() {
  const { t } = useTranslation("profile");

  const classes = useStyles();
  return (
    <LayoutApp drawer title={t("textEditButton")}>
      <Box
        className={classes.container}
        component="main"
        padding={0}
        margin={0}
        display="flex"
        flexGrow={1}
      >
        <FlexBox
          justifyContent={JustifyContentEnum.FLEXSTART}
          extraStyle={{ padding: 0, margin: 0 }}
        >
          <HeaderProfile />
          <ProfileTabs />
        </FlexBox>
      </Box>
    </LayoutApp>
  );
}

export default Profile;
