import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { GetStaticProps, GetStaticPaths } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import FilePage from "@/components/pages/file/FilePage";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { setCurrentOfflinePage } from "@/utils/offlineNavigation";
import { OfflinePagesEnum } from "@/enums/*";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["file", "library"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

function File() {
  const connectionStatus = useConnectionStatus();
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    router.prefetch("/offline/file");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!connectionStatus) {
      setCurrentOfflinePage({
        page: OfflinePagesEnum.FILE,
        data: {
          id,
        },
      });
      router.replace(`/offline/file`);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connectionStatus]);

  return <FilePage id={String(id)} />;
}

export default File;
