import React, { useState } from "react";
import LayoutApp from "@/components/statefull/LayoutApp";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { GetStaticProps } from "next";
import { I18nInterface, LibraryItemInterface } from "@/interfaces/index";
import { ButtonSizeEnum, JustifyContentEnum } from "@/enums/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import { useTheme } from "@material-ui/core/styles";
import LibraryModal from "@/components/ui/LibraryModal";
import { isAudioFile } from "@/utils/utils";
import SvgIconAux from "@/components/ui/SvgIcon";
import { getEditAudioLink } from "@/utils/offlineNavigation";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import {
  FrameWaveform,
  FlexboxWrapper,
  Description,
  MyClickable,
  MyButton,
} from "@/components/pages/edit-audio/RecordAudioPage/styled";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["editAudio", "common"])),
  },
});

function SelectAudio() {
  const theme = useTheme();
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const [libraryOpen, setLibraryOpen] = useState(false);

  const { t } = useTranslation("editAudio");
  const [loading, setLoading] = useState<string | undefined>();

  const openModal = () => {
    setLibraryOpen(true);
  };

  const actions = (item: LibraryItemInterface) => {
    if (item.type === "file" && isAudioFile(item.mime)) {
      return (
        <MyButton
          handleClick={() => handleClick(item)}
          title={t("editFile")}
          size={ButtonSizeEnum.SMALL}
          isLoading={loading === item.filename}
          disabled={loading !== undefined}
        />
      );
    }

    return null;
  };

  const handleClick = ({ filename }: LibraryItemInterface) => {
    setLoading(filename);
    router.push(getEditAudioLink(btoa(filename), connectionStatus));
  };

  const filterItems = (items: LibraryItemInterface[]) =>
    items.filter((item) => item.type !== "file" || isAudioFile(item.mime));

  return (
    <LayoutApp
      templateHeader="variation2"
      showFooter={false}
      title={t("title")}
      back
      drawer={false}
      notifications={false}
      backgroundColor={theme.palette.primary.main}
    >
      <FlexboxWrapper justifyContent={JustifyContentEnum.CENTER}>
        <FrameWaveform>
          <MyClickable handleClick={openModal}>
            <Description>{t("chooseAudio")}</Description>
            <SvgIconAux icon="add_file" fontSizeIcon={32} htmlColor={theme.palette.primary.light} />
          </MyClickable>
        </FrameWaveform>
      </FlexboxWrapper>

      <LibraryModal
        title={t("selectAudioFile")}
        handleClose={() => setLibraryOpen(false)}
        open={libraryOpen}
        onlyDirectories={false}
        options={actions}
        filterItems={filterItems}
      />
    </LayoutApp>
  );
}

export default SelectAudio;
