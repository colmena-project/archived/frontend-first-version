import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { GetStaticProps, GetStaticPaths } from "next";
import { I18nInterface } from "@/interfaces/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import EditAudioPage from "@/components/pages/edit-audio/EditAudioPage";
import { setCurrentOfflinePage } from "@/utils/offlineNavigation";
import { OfflinePagesEnum } from "@/enums/*";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["editAudio", "common", "recording"])),
  },
});

export const getStaticPaths: GetStaticPaths = async () => ({
  paths: [],
  fallback: "blocking",
});

function EditAudio() {
  const router = useRouter();
  const connectionStatus = useConnectionStatus();
  const { id } = router.query;

  useEffect(() => {
    router.prefetch("/offline/edit-audio");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!connectionStatus) {
      setCurrentOfflinePage({
        page: OfflinePagesEnum.EDIT_AUDIO,
        data: {
          id,
        },
      });
      router.replace(`/offline/edit-audio`);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connectionStatus]);

  return <EditAudioPage id={String(id)} />;
}

export default EditAudio;
