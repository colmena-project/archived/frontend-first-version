import React from "react";
import { Container } from "@material-ui/core";
import Text from "@/components/ui/Text";
import { TextVariantEnum } from "@/enums/index";
import CONSTANTS from "@/constants/index";
import { makeStyles } from "@material-ui/core/styles";
import { getSession } from "next-auth/client";
import { GetServerSideProps } from "next";
import getConfig from "next/config";

const { serverRuntimeConfig } = getConfig();

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);

  if (!session?.user) {
    return {
      redirect: {
        destination: serverRuntimeConfig.blog.baseUrl || "https://blog.colmena.media",
        permanent: false,
      },
    };
  }

  return {
    redirect: {
      destination: "/home",
      permanent: false,
    },
  };
};

const useStyles = makeStyles(() => ({
  root: {
    height: "100vh",
    display: "flex",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  title: {
    marginBottom: 20,
    textAlign: "center",
  },
}));

function Index() {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Text variant={TextVariantEnum.H4} gutterBottom>
        {CONSTANTS.APP_NAME}
      </Text>
      <Text variant={TextVariantEnum.BODY1} gutterBottom className={classes.title}>
        {CONSTANTS.APP_DESCRIPTION}
      </Text>
    </Container>
  );
}

export default Index;
