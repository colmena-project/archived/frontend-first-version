import React, { useCallback } from "react";
import FlexBox from "@/components/ui/FlexBox";
import LayoutApp from "@/components/statefull/LayoutApp";
import { useTranslation } from "next-i18next";
import { GetStaticProps } from "next";
import { I18nInterface } from "@/interfaces/index";
import { AlignItemsEnum, JustifyContentEnum } from "@/enums/index";
import serverSideTranslations from "@/extensions/next-i18next/serverSideTranslations";
import theme from "@/styles/theme";
import { Box } from "@material-ui/core";
import IconButtonCtr from "@/components/ui/IconButton";
import Text from "@/components/ui/Text";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { PropsUserSelector } from "@/types/*";
import { useSelector } from "react-redux";
import { toast } from "@/utils/notifications";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import WaveformPlaylist from "colmena-waveform-playlist";

export const getStaticProps: GetStaticProps = async ({ locale }: I18nInterface) => ({
  props: {
    ...(await serverSideTranslations(locale, ["stream"])),
  },
});

const useStyles = makeStyles((theme: Theme) => ({
  controls: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    maxWidth: "400px",
    justifyContent: "space-around",
    alignItems: "flex-end",
    height: "90px",
  },
  description: {
    color: theme.palette.primary.light,
    fontSize: 12,
  },
  boxContainerNode: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 15,
    "& .playlist-time-scale .time": {
      color: theme.palette.primary.light,
    },
    "& .playlist-tracks .cursor": {
      backgroundColor: theme.palette.primary.light,
    },
    "& .playlist-tracks .channel-wrapper": {
      backgroundColor: theme.palette.primary.main,
    },
    "& .playlist-tracks": {
      backgroundColor: theme.palette.primary.main,
    },
    "& .playlist .channel": {
      background:
        "linear-gradient(0deg, rgba(246,91,58,1) 0%, rgba(243,155,16,1) 50%, rgba(246,91,58,1) 100%)",
    },
    "& .playlist-tracks .channel-wrapper .waveform": {
      border: "none !important",
    },
  },
  time: {
    color: theme.palette.primary.contrastText,
    fontWeight: 550,
    fontSize: "1.5em",
    letterSpacing: 1,
  },
  locationDescription: {
    color: theme.palette.primary.contrastText,
    fontWeight: 550,
  },
  location: {
    color: theme.palette.primary.light,
  },
  footerStream: {
    width: "100%",
    textAlign: "left",
  },
}));

function Stream() {
  const { t } = useTranslation("stream");
  const { t: c } = useTranslation("common");
  const classes = useStyles();
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const container = useCallback((node) => {
    if (node !== null) {
      const playlist = WaveformPlaylist({
        samplesPerPixel: 1000,
        mono: true,
        waveHeight: 200,
        container: node,
        timescale: true,
        state: "cursor",
        barWidth: 4,
        isAutomaticScroll: true,
        barGap: 2,
        innerWidth: 200,
        colors: {
          waveOutlineColor: theme.palette.primary.main,
          timeColor: theme.palette.primary.light,
          fadeColor: "black",
        },
        zoomLevels: [1000],
      });

      playlist.load([
        {
          src: "/audio.ogg",
          name: "audio",
          waveOutlineColor: theme.palette.primary.main,
        },
      ]);
      // initialize the WAV exporter.
      playlist.initExporter();
    }
  }, []);

  const unavailable = () => {
    toast(c("featureUnavailable"), "warning");
  };

  return (
    <LayoutApp
      backgroundColor={theme.palette.primary.main}
      title={t("title")}
      showFooter={false}
      showUploadProgress={false}
      drawer={false}
      notifications={false}
      back
    >
      <FlexBox
        extraStyle={{ backgroundColor: theme.palette.primary.main }}
        justifyContent={JustifyContentEnum.SPACEBETWEEN}
        alignItems={AlignItemsEnum.CENTER}
      >
        <Box className={classes.controls}>
          <Box>
            <IconButtonCtr
              icon="signal_tower"
              iconColor="#fff"
              data-testid="record-and-pause-button"
              fontSizeIcon={35}
              handleClick={unavailable}
            />
            <Text className={classes.description}>{t("goLive")}</Text>
          </Box>
          <Box>
            <IconButtonCtr
              icon="record_outlined"
              iconColor="#F65B3A"
              data-testid="record-and-pause-button"
              fontSizeIcon={50}
              handleClick={unavailable}
            />
            <Text className={classes.description}>{t("record")}</Text>
          </Box>
          <Box>
            <IconButtonCtr
              icon="share"
              iconColor="#fff"
              data-testid="record-and-pause-button"
              fontSizeIcon={35}
              handleClick={unavailable}
            />
            <Text className={classes.description}>{t("share")}</Text>
          </Box>
        </Box>
        <Box className={classes.boxContainerNode}>
          <div ref={container}></div>
          <Text className={classes.time}>00:00:00</Text>
        </Box>
        <Box className={classes.footerStream}>
          <Text className={classes.locationDescription}>{t("locationDescription")}:</Text>
          <Text className={classes.location}>{userRdx.user.id}/stream</Text>
        </Box>
      </FlexBox>
    </LayoutApp>
  );
}

export default Stream;
