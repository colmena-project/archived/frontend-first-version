// eslint-disable-next-line import/no-cycle
import { ChatMessageItemInterface, RoomItemInterface, RoomParticipant } from "@/interfaces/talk";
import {
  UserInfoInterface,
  RecordingInterface,
  LibraryItemInterface,
  TransferItemInterface,
  CurrentPublication,
  WizardInterface,
} from "@/interfaces/index";
import {
  NotificationStatusEnum,
  ButtonColorEnum,
  ButtonVariantEnum,
  ButtonSizeEnum,
  PositionEnum,
  SizeScreensEnum,
  SelectVariantEnum,
  TextAlignEnum,
  TextColorEnum,
  TextDisplayEnum,
  TextVariantEnum,
  AlignItemsEnum,
  JustifyContentEnum,
  FlexDirectionEnum,
  TextfieldVariantEnum,
  EnvironmentEnum,
  TransferStatusEnum,
  PermissionTalkMemberEnum,
  FileStatusPublicationEnum,
  CurrentPublicationStateEnum,
  AudioExportTypesEnum,
} from "@/enums/index";

export type PropsUserSelector = {
  user: UserInfoInterface;
};

export type PropsAudioEditorSelector = {
  isCursorSelected: boolean;
};

export type PropsHoneycombSelector = {
  honeycombs: RoomItemInterface[];
  clearChatMessages: string[];
  chatMessages: ChatMessageItemInterface[];
  reloadChatLocalMessage: boolean;
  honeycombsArchived: string[];
};

export type PropsChannelSelector = {
  channels: RoomItemInterface[];
};

export type ParticipantConference = {
  identity: string;
  id: string;
  socketId: string;
  roomId: string;
};

export type PropsConferenceSelector = {
  identity: string | null;
  roomId: string | null;
  isRoomHost: boolean;
  showOverlay: boolean;
  participants: ParticipantConference[];
  participantsNC: RoomParticipant[];
};

export type PropsPublicationsSelector = {
  currentPublication: CurrentPublication | null;
};

export type PropsTransferSelector = {
  openTransferModal: boolean;
  files: TransferItemInterface[];
};

export type PossibleOptionsInProfileProps = "initial" | "language" | "cloudAndStorage";
export type PossibleOptionsInMediaProfileProps = "initial" | "mediaInfo" | "editTeam";

export type StatusTransferItemProps =
  | TransferStatusEnum.PENDING
  | TransferStatusEnum.IN_PROGRESS
  | TransferStatusEnum.CANCELED
  | TransferStatusEnum.COMPLETE;

export type PropsAudioData = {
  blob: Blob;
  blobUrl: string;
  type?: string;
};

export type PropsAudioSave = {
  name: string;
  tags: string[];
  path: string;
  availableOffline: boolean;
};

export type PropsLibrarySelector = {
  libraryFiles: LibraryItemInterface[];
  currentPath: string;
  currentPathExists: boolean;
  currentAudioPlaying: string;
};

export type NXTagsProps = {
  id?: number;
  title: string;
};

export type TagsConference = {
  "display-name": string;
  "user-visible": boolean;
  "user-assignable": boolean;
  id: number;
};

export type SelectOptionItem = {
  id: number | string;
  value: string;
  disabled?: boolean;
};

export type PropsRecordingSelector = {
  recordings: RecordingInterface[];
  activeRecordingState: AudioStateProps;
  allowBrowserRecording: boolean;
  backAfterFinishRecording: boolean;
  recordingCounter: number;
};

export type NotificationStatusProps =
  | NotificationStatusEnum.SUCCESS
  | NotificationStatusEnum.ERROR
  | NotificationStatusEnum.WARNING
  | NotificationStatusEnum.INFO;

export type ButtonColorProps =
  | ButtonColorEnum.DEFAULT
  | ButtonColorEnum.INHERIT
  | ButtonColorEnum.PRIMARY
  | ButtonColorEnum.SECONDARY;

export type ButtonVariantProps =
  | ButtonVariantEnum.CONTAINED
  | ButtonVariantEnum.OUTLINED
  | ButtonVariantEnum.TEXT;

export type SelectVariantProps =
  | SelectVariantEnum.FILLED
  | SelectVariantEnum.OUTLINED
  | SelectVariantEnum.STANDARD;

export type ButtonSizeProps = ButtonSizeEnum.LARGE | ButtonSizeEnum.MEDIUM | ButtonSizeEnum.SMALL;

export type PositionProps =
  | PositionEnum.FIXED
  | PositionEnum.ABSOLUTE
  | PositionEnum.STICKY
  | PositionEnum.STATIC
  | PositionEnum.RELATIVE;

export type SizeScreensProps =
  | SizeScreensEnum.LG
  | SizeScreensEnum.MD
  | SizeScreensEnum.SM
  | SizeScreensEnum.XL
  | SizeScreensEnum.XS;

export type TextAlignProps =
  | TextAlignEnum.LEFT
  | TextAlignEnum.RIGHT
  | TextAlignEnum.CENTER
  | TextAlignEnum.JUSTIFY
  | TextAlignEnum.INHERIT;

export type TextColorProps =
  | TextColorEnum.INITIAL
  | TextColorEnum.INHERIT
  | TextColorEnum.PRIMARY
  | TextColorEnum.SECONDARY
  | TextColorEnum.TEXTPRIMARY
  | TextColorEnum.TEXTSECONDARY
  | TextColorEnum.ERROR;

export type TextDisplayProps =
  | TextDisplayEnum.INITIAL
  | TextDisplayEnum.BLOCK
  | TextDisplayEnum.INLINE;

export type TextVariantProps =
  | TextVariantEnum.H1
  | TextVariantEnum.H2
  | TextVariantEnum.H3
  | TextVariantEnum.H4
  | TextVariantEnum.H5
  | TextVariantEnum.H6
  | TextVariantEnum.SUBTITLE1
  | TextVariantEnum.SUBTITLE2
  | TextVariantEnum.BODY1
  | TextVariantEnum.BODY2
  | TextVariantEnum.CAPTION
  | TextVariantEnum.BUTTON
  | TextVariantEnum.OVERLINE
  | TextVariantEnum.SRONLY
  | TextVariantEnum.INHERIT;

export type TextVariantMappingProps = {
  h1: TextVariantEnum.H1;
  h2: TextVariantEnum.H2;
  h3: TextVariantEnum.H3;
  h4: TextVariantEnum.H4;
  h5: TextVariantEnum.H5;
  h6: TextVariantEnum.H6;
  subtitle1: TextVariantEnum.H6;
  subtitle2: TextVariantEnum.H6;
  body1: TextVariantEnum.P;
  body2: TextVariantEnum.P;
};

export type FlexDirectionProps =
  | FlexDirectionEnum.ROW
  | FlexDirectionEnum.ROWREVERSE
  | FlexDirectionEnum.COLUMN
  | FlexDirectionEnum.COLUMNREVERSE
  | FlexDirectionEnum.INITIAL
  | FlexDirectionEnum.INHERIT;

export type AlignItemsProps =
  | AlignItemsEnum.STRETCH
  | AlignItemsEnum.CENTER
  | AlignItemsEnum.FLEXSTART
  | AlignItemsEnum.FLEXEND
  | AlignItemsEnum.BASELINE
  | AlignItemsEnum.INITIAL
  | AlignItemsEnum.INHERIT;

export type JustifyContentProps =
  | JustifyContentEnum.FLEXSTART
  | JustifyContentEnum.FLEXEND
  | JustifyContentEnum.CENTER
  | JustifyContentEnum.SPACEBETWEEN
  | JustifyContentEnum.SPACEAROUND
  | JustifyContentEnum.SPACEEVENLY
  | JustifyContentEnum.INITIAL
  | JustifyContentEnum.INHERIT;

export type TextfieldVariantProps =
  | TextfieldVariantEnum.CONTAINED
  | TextfieldVariantEnum.OUTLINED
  | TextfieldVariantEnum.TEXT;

export type FontSizeIconProps = "medium" | "inherit" | "large" | "small" | undefined;

export type AllIconProps =
  | "settings"
  | "edit"
  | "microphone"
  | "library"
  | "account_profile"
  | "world_map"
  | "home"
  | "cut"
  | "check_cloud"
  | "phone"
  | "off_cloud"
  | "more_vertical"
  | "search"
  | "equalize"
  | "back"
  | "language"
  | "share"
  | "add_user"
  | "dropdown_checklist"
  | "close"
  | "record"
  | "pause"
  | "stop"
  | "download"
  | "plus"
  | "arrow_right_up"
  | "burger_menu"
  | "global"
  | "download_circle"
  | "user_group"
  | "user"
  | "help"
  | "contract"
  | "info"
  | "panal"
  | "gradient_panal"
  | "logout"
  | "grid"
  | "settings_adjust"
  | "checklist"
  | "add_folder"
  | "clould_upload"
  | "info_circle"
  | "panal_flat"
  | "tick"
  | "chat"
  | "send"
  | "upload"
  | "edit_text"
  | "headphone"
  | "stream"
  | "audio_editor"
  | "clip"
  | "question"
  | "speaker"
  | "plus_circle"
  | "art_gallery"
  | "music"
  | "folder"
  | "folder_outlined"
  | "private"
  | "offline"
  | "file"
  | "faq"
  | "warning"
  | "record_outlined"
  | "play"
  | "play_flat"
  | "pause_flat"
  | "trash"
  | "html_tag"
  | "hashtag"
  | "update"
  | "disabled"
  | "camera"
  | "edit_circle"
  | "facebook"
  | "twitter"
  | "sync_file"
  | "generic_file"
  | "play_outlined"
  | "pause_outlined"
  | "stop_outlined"
  | "edit_filled"
  | "copy"
  | "duplicate"
  | "move"
  | "rename"
  | "details"
  | "banner_1"
  | "banner_2"
  | "banner_3"
  | "banner_4"
  | "gradient_plus"
  | "delete"
  | "stop_flat"
  | "forward"
  | "rewind"
  | "sync"
  | "cursor_select"
  | "audio_shift"
  | "save"
  | "audio_settings"
  | "start_cursor"
  | "end_cursor"
  | "recently_viewed"
  | "arrow_up_left"
  | "trim"
  | "instagram"
  | "show"
  | "arrow_down_circle"
  | "call"
  | "transfer"
  | "cancel"
  | "clean"
  | "sync_success"
  | "sync_canceled"
  | "archive"
  | "microphone_fill_disable"
  | "microphone_fill_enable"
  | "end_call"
  | "results"
  | "chevron_right"
  | "chevron_bottom"
  | "dots_three_horizontal"
  | "speaker_fill_disable"
  | "speaker_fill_enable"
  | "online_file"
  | "social_media"
  | "people"
  | "add_person"
  | "sound"
  | "call_headphone"
  | "panal_folder"
  | "basic_play"
  | "basic_pause"
  | "transmit"
  | "editor_pause"
  | "editor_stop"
  | "editor_play"
  | "editor_rewind"
  | "editor_forward"
  | "signal_tower"
  | "add_file"
  | "heart"
  | "heart_outlined"
  | "star"
  | "channel"
  | "star_outlined"
  | "new_channel"
  | "two_users"
  | "contrast"
  | "storage_solid"
  | "monitoring"
  | "settings_applications"
  | "group_add"
  | "hub"
  | "sell"
  | "podcasts"
  | "publication"
  | "add_circle"
  | "mastodon"
  | "whatsapp"
  | "telegram"
  | "newspaper"
  | "document"
  | "draft"
  | "publish"
  | "zoomin"
  | "zoomout"
  | "warning_outline"
  | "success_circle"
  | "view_in_file"
  | "search_in_library"
  | "fadeIn"
  | "fadeOut"
  | "undo"
  | "redo"
  | "import_audio"
  | "audio_file"
  | "circle_check"
  | "check_outline"
  | "error_outline"
  | "peding_outline"
  | "finished_list"
  | "pending_list"
  | "publications"
  | "publications_management"
  | "use_in_publication"
  | "split"
  | "edit_outlined"
  | "call_end"
  | "notifications"
  | "arrow_down"
  | "call_end"
  | "install_app"
  | "about_faq"
  | "help_support"
  | "terms_conditions"
  | "data_base"
  | "medias"
  | "doublecheck"
  | "wav_icon"
  | "mp3_icon"
  | "share_circle"
  | "facebook_outline"
  | "twitter_outline"
  | "whatsapp_outline"
  | "telegram_outline"
  | "interview"
  | "circle_plus"
  | "circle_close"
  | "save_and_next"
  | "emergency"
  | "blog"
  | "webm_icon";

export type Environment = EnvironmentEnum.LOCAL | EnvironmentEnum.REMOTE | EnvironmentEnum.BOTH;

export type LanguagesAvailableProps = "es" | "en" | "fr" | "pt_BR" | "ar" | "sw" | "uk" | "ru";

export type LanguageProps = {
  abbr: LanguagesAvailableProps;
  language: string;
};

export type SocialMediasAvailable =
  | "twitter"
  | "facebook"
  | "instagram"
  | "mastodon"
  | "whatsapp"
  | "telegram";

export type SocialMediasAvailableKeys =
  | "social_media_whatsapp"
  | "social_media_twitter"
  | "social_media_facebook"
  | "social_media_mastodon"
  | "social_media_instagram"
  | "social_media_telegram";

export type ParticipantTypeProps =
  | PermissionTalkMemberEnum.OWNER
  | PermissionTalkMemberEnum.MODERATOR
  | PermissionTalkMemberEnum.USER
  | PermissionTalkMemberEnum.GUEST
  | PermissionTalkMemberEnum.GUEST_WITH_MODERATOR_PERMISSIONS
  | PermissionTalkMemberEnum.USER_PUBLIC_LINK;

export type AudioStateProps = "NONE" | "RECORDING" | "STOPPED" | "PAUSED" | "PLAYING";

export type ImageSizes = "thumbnail" | "small" | "medium" | "large";

export type AudioStateWaveformProps = "shift" | "cursor" | "fadein" | "fadeout" | "select";

export type FileStatusPublicationProps =
  | FileStatusPublicationEnum.PENDING
  | FileStatusPublicationEnum.UPLOADED
  | FileStatusPublicationEnum.IN_PROGRESS
  | FileStatusPublicationEnum.ERROR;

export type PublicationStatusProps =
  | CurrentPublicationStateEnum.PENDING
  | CurrentPublicationStateEnum.IN_PROGRESS
  | CurrentPublicationStateEnum.UPLOADED
  | CurrentPublicationStateEnum.ERROR
  | CurrentPublicationStateEnum.CREATING
  | CurrentPublicationStateEnum.SENT
  | CurrentPublicationStateEnum.NOT_SENT
  | CurrentPublicationStateEnum.DRAFT
  | CurrentPublicationStateEnum.PUBLISH;

export type PropsWizardSelector = {
  wizard: WizardInterface;
};

export type CallTypesProps = "virtual-studio" | "call";

export type AudioExportTypesProps =
  | AudioExportTypesEnum.MP3
  | AudioExportTypesEnum.WAV
  | AudioExportTypesEnum.WEBA
  | AudioExportTypesEnum.OPUS
  | AudioExportTypesEnum.AAC;
