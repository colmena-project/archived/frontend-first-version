/* eslint-disable @typescript-eslint/ban-types */
import axios, { AxiosRequestConfig } from "axios";
import { initializeStore } from "@/store/index";
import getConfig from "next/config";
import { parseXML, RequestDataPayload } from "webdav";
import { encodeURLAxios } from "@/utils/utils";

const { publicRuntimeConfig } = getConfig();

const cancelTokenSources = new Map();

export default async function axiosConnection(
  data: RequestDataPayload | null | {},
  context = "systemtags",
  method = "PROPFIND",
  extraHeaders: {} = { "Content-Type": "application/xml; charset=utf-8" },
  rootContext = false,
) {
  const { nexcloudToken } = initializeStore({}).getState().user.user;
  let path = `${publicRuntimeConfig.api.baseUrl}/remote.php`;
  if (!rootContext) {
    path += "/dav";
  }

  const config: any = {
    method,
    url: `${path}/${encodeURLAxios(context)}`,
    headers: {
      "OCS-APIRequest": true,
      Authorization: `Bearer ${nexcloudToken}`,
      ...extraHeaders,
    },
    data,
  };

  const api = axios.create();

  api.interceptors.request.use((config: AxiosRequestConfig) => {
    if (!config?.cancelToken) {
      const { token, cancel } = axios.CancelToken.source();
      cancelTokenSources.set(token, cancel);

      // eslint-disable-next-line no-param-reassign
      config.cancelToken = token;
    }

    return config;
  });

  api.interceptors.response.use(
    (response) => {
      if (response.config?.cancelToken) {
        cancelTokenSources.delete(response.config.cancelToken);
      }

      return response;
    },
    (error) => {
      if (axios.isCancel(error)) {
        cancelTokenSources.delete(error.message);
      }

      return Promise.reject(error);
    },
  );

  const resp = await api.request(config);

  const responseData: string = resp.data.toString();

  return parseXML(responseData);
}

export { cancelTokenSources };
