import { createClient, AuthType } from "webdav";
import { initializeStore } from "@/store/index";
import getConfig from "next/config";
import axios from "axios";
import { encodeURLAxios } from "@/utils/utils";

const { publicRuntimeConfig } = getConfig();

export default function webdav(context = "files") {
  const path = encodeURLAxios(context);
  const { nexcloudToken } = initializeStore({}).getState().user.user;
  const client = createClient(`${publicRuntimeConfig.api.baseUrl}/remote.php/dav/${path}/`, {
    authType: AuthType.Token,
    token: {
      access_token: nexcloudToken,
      token_type: "Bearer",
    },
  });
  return client;
}

export const webdavAxios = () => {
  const { nexcloudToken } = initializeStore({}).getState().user.user;
  const api = axios.create({
    baseURL: `${publicRuntimeConfig.api.baseUrl}/remote.php/dav`,
    headers: {
      "OCS-APIRequest": true,
      Authorization: `Bearer ${nexcloudToken}`,
    },
  });
  return api;
};
