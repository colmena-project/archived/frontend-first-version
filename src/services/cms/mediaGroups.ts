import { gql } from "@apollo/client";

export const CREATE_MEDIA_GROUP = gql`
  mutation CreateMediaGroup($token: String, $avatar: ID) {
    createMediaGroup(data: { token: $token, avatar: $avatar }) {
      data {
        id
        attributes {
          token
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
              }
            }
          }
        }
      }
    }
  }
`;

export const UPDATE_MEDIA_GROUP = gql`
  mutation UpdateMediaGroup($id: ID!, $token: String, $avatar: ID) {
    updateMediaGroup(id: $id, data: { token: $token, avatar: $avatar }) {
      data {
        id
        attributes {
          token
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_MEDIA_GROUP = gql`
  query GetMediaGroup($token: String!) {
    mediaGroups(filters: { token: { eq: $token } }) {
      data {
        id
        attributes {
          token
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
              }
            }
          }
        }
      }
    }
  }
`;
