import { gql } from "@apollo/client";

export const CREATE_COLLABORATOR = gql`
  mutation CreateCollaborator($username: String, $avatar: ID) {
    createCollaborator(data: { username: $username, avatar: $avatar }) {
      data {
        id
        attributes {
          username
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
              }
            }
          }
        }
      }
    }
  }
`;

export const UPDATE_COLLABORATOR = gql`
  mutation UpdateCollaborator($id: ID!, $username: String, $avatar: ID) {
    updateCollaborator(id: $id, data: { username: $username, avatar: $avatar }) {
      data {
        id
        attributes {
          username
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_COLLABORATOR = gql`
  query GetCollaborator($username: String!) {
    collaborators(filters: { username: { eq: $username } }) {
      data {
        id
        attributes {
          username
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
        }
      }
    }
  }
`;
