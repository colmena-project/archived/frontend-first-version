import { client } from "@/apollo/client";
import { PublicationQueryInterface } from "@/interfaces/cms";
import {
  GET_ALL_PUBLICATIONS,
  GET_ALL_PUBLICATIONS_WITHOUT_FILTERS,
  GET_PUBLICATION,
  GET_PUBLIC_PUBLICATION,
  PublicationFiltersProps,
} from "./publications";

export async function getAllPublicationsWithoutFilter() {
  return client.query({
    query: GET_ALL_PUBLICATIONS_WITHOUT_FILTERS,
    fetchPolicy: "network-only",
  });
}

export default async function getPublications({
  group,
  index,
  keyword,
  itemsPerPage,
  category,
  order,
}: PublicationFiltersProps) {
  return client.query({
    query: GET_ALL_PUBLICATIONS,
    variables: { index, itemsPerPage, keyword, group, category, order },
    fetchPolicy: "network-only",
  });
}

export async function getPublication(
  publicationId: string,
): Promise<PublicationQueryInterface | null> {
  try {
    const { data } = await client.query({
      query: GET_PUBLICATION,
      variables: { id: publicationId },
      fetchPolicy: "network-only",
    });

    return data.publication.data;
  } catch (err) {
    return null;
  }
}

export async function getPublicPublication(
  publicationId: string,
): Promise<PublicationQueryInterface | null> {
  try {
    const { data } = await client.query({
      query: GET_PUBLIC_PUBLICATION,
      variables: { id: publicationId },
      fetchPolicy: "network-only",
    });

    const publication = data.publication.data;
    if (!publication || !publication?.attributes.publishedAt) {
      return null;
    }

    return publication;
  } catch (err) {
    return null;
  }
}
