/* eslint-disable camelcase */
import { cmsHttpClient } from "./cmsHttpClient";

type PublicationFileCreate = {
  creator: string;
  description: string;
  extent_duration?: number;
  extent_size?: number;
  format: string;
  language: string;
  media: number;
  publication: number;
  tags?: string;
  title: string;
  identifier?: string;
  position?: number;
};

export const uploadFileToCMS = async (formData: FormData) => {
  const { data } = await cmsHttpClient.post<Array<{ id: number; url: string }>>(
    "/upload",
    formData,
  );

  return data;
};

export const createPublicationFile = async (publicationFile: PublicationFileCreate) => {
  const { data } = await cmsHttpClient.post("/publication-files", { data: publicationFile });

  return data;
};

export const removeFile = async (id: number) => {
  const { data } = await cmsHttpClient.delete(`/upload/files/${id}`);

  return data;
};

export const unpublishPublication = async (id: number | string, thumbId: number | null) =>
  cmsHttpClient.put(`/publications/${id}`, {
    data: {
      publishedAt: null,
      relation: thumbId,
    },
  });

export const publishPublication = async (id: number | string, thumbId: number | null) =>
  cmsHttpClient.put(`/publications/${id}`, {
    data: {
      publishedAt: new Date().toISOString(),
      relation: thumbId,
    },
  });
