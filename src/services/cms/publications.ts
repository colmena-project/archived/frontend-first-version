import { PublicationFilterStatusEnum } from "@/enums/*";
import { gql } from "@apollo/client";
import { cmsHttpClient } from "./cmsHttpClient";

export const SAVE_PUBLICATION_AS_DRAFT = gql`
  mutation SavePublicationAsDraft(
    $title: String!
    $description: String
    $coverage: String!
    $publisher: String!
    $rights: String!
    $date: DateTime!
    $content: String!
    $creator: String
    $subject: ID!
    $relation: ID
    $language: String
  ) {
    createPublication(
      data: {
        title: $title
        description: $description
        coverage: $coverage
        publisher: $publisher
        rights: $rights
        date: $date
        content: $content
        publishedAt: null
        creator: $creator
        subject: $subject
        relation: $relation
        language: $language
      }
    ) {
      data {
        id
        attributes {
          title
          description
          coverage
          publisher
          date
          content
          rights
          creator
          language
          subject {
            data {
              id
              attributes {
                name
              }
            }
          }
          relation {
            data {
              id
            }
          }
          publishedAt
        }
      }
    }
  }
`;

export const APPROVE_PUBLICATION = gql`
  mutation ApprovePublication($id: ID!, $publishedAt: DateTime!) {
    updatePublication(id: $id, data: { publishedAt: $publishedAt }) {
      data {
        id
        attributes {
          publishedAt
        }
      }
    }
  }
`;

export const UNPUBLISH_PUBLICATION = gql`
  mutation UnpublishPublication($id: ID!) {
    updatePublication(id: $id, data: { publishedAt: null }) {
      data {
        id
        attributes {
          publishedAt
        }
      }
    }
  }
`;

export const DISAPPROVE_PUBLICATION = gql`
  mutation ApprovePublication($id: ID!, $disapprovalReason: String!, $disapprovedAt: DateTime!) {
    updatePublication(
      id: $id
      data: { disapprovalReason: $disapprovalReason, disapprovedAt: $disapprovedAt }
    ) {
      data {
        id
        attributes {
          disapprovalReason
          disapprovedAt
        }
      }
    }
  }
`;

export const PUBLISH_PUBLICATION = gql`
  mutation PublishPublication(
    $title: String!
    $description: String
    $coverage: String!
    $publisher: String!
    $rights: String!
    $date: DateTime!
    $content: String!
    $creator: String
    $subject: ID!
    $relation: ID
    $publishedAt: DateTime!
  ) {
    createPublication(
      data: {
        title: $title
        description: $description
        coverage: $coverage
        publisher: $publisher
        rights: $rights
        date: $date
        content: $content
        publishedAt: $publishedAt
        creator: $creator
        subject: $subject
        relation: $relation
      }
    ) {
      data {
        id
        attributes {
          title
          description
          coverage
          publisher
          date
          content
          rights
          creator
          subject {
            data {
              id
            }
          }
          relation {
            data {
              id
            }
          }
          publishedAt
        }
      }
    }
  }
`;

export const GET_ALL_PUBLICATIONS = gql`
  query GetAllPublications(
    $index: Int
    $itemsPerPage: Int
    $group: String
    $category: ID
    $order: [String]
    $keyword: String
  ) {
    publications(
      publicationState: LIVE
      filters: {
        title: { containsi: $keyword }
        publisher: { eq: $group }
        subject: { id: { eq: $category } }
      }
      sort: $order
      pagination: { page: $index, pageSize: $itemsPerPage }
    ) {
      data {
        id
        attributes {
          title
          description
          coverage
          publisher
          date
          content
          rights
          creator
          publishedAt
          tags
          subject {
            data {
              id
              attributes {
                name
                language
              }
            }
          }
          relation {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
          publishedAt
        }
      }

      meta {
        pagination {
          page
          pageSize
          pageCount
          total
        }
      }
    }
  }
`;

export const GET_PUBLICATION = gql`
  query getPublication($id: ID!) {
    publication(id: $id) {
      data {
        id
        attributes {
          title
          description
          coverage
          publisher
          date
          content
          rights
          creator
          publishedAt
          tags
          language
          files {
            data {
              id
              attributes {
                title
                description
                language
                creator
                tags
                format
                extent_size
                extent_duration
                downloads
                media {
                  data {
                    id
                    attributes {
                      name
                      alternativeText
                      formats
                      createdAt
                      updatedAt
                      url
                      ext
                      size
                      mime
                    }
                  }
                }
              }
            }
          }
          subject {
            data {
              id
              attributes {
                name
                language
              }
            }
          }
          relation {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_PUBLIC_PUBLICATION = gql`
  query getPublication($id: ID!) {
    publication(id: $id) {
      data {
        id
        attributes {
          title
          description
          coverage
          publisher
          date
          content
          rights
          creator
          publishedAt
          tags
          files {
            data {
              id
              attributes {
                title
                description
                language
                creator
                tags
                format
                extent_size
                extent_duration
                downloads
                media {
                  data {
                    id
                    attributes {
                      name
                      alternativeText
                      formats
                      createdAt
                      updatedAt
                      url
                      ext
                      size
                      mime
                    }
                  }
                }
              }
            }
          }
          subject {
            data {
              id
              attributes {
                name
                language
              }
            }
          }
          relation {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
        }
      }
    }
  }
`;

export interface PublicationFiltersProps {
  group?: string;
  category?: string;
  order?: string;
  keyword?: string;
  index?: number;
  itemsPerPage?: number;
  status?: PublicationFilterStatusEnum;
}

export const GET_PUBLICATIONS_BY_COLABORATOR = gql`
  query getPublicationsByColaborator(
    $creator: String!
    $publisher: String!
    $keyword: String
    $order: [String]
    $category: ID
    $page: Int
    $pageSize: Int
  ) {
    publications(
      publicationState: PREVIEW
      filters: {
        creator: { eq: $creator }
        publisher: { eq: $publisher }
        subject: { id: { eq: $category } }
        or: [{ description: { containsi: $keyword } }, { title: { containsi: $keyword } }]
      }
      sort: $order
      pagination: { page: $page, pageSize: $pageSize }
    ) {
      data {
        id
        attributes {
          title
          description
          publisher
          date
          creator
          publishedAt
          disapprovedAt
          disapprovalReason
          createdAt
          subject {
            data {
              id
              attributes {
                name
              }
            }
          }
          relation {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
        }
      }

      meta {
        pagination {
          page
          pageSize
          pageCount
          total
        }
      }
    }
  }
`;

export const GET_PUBLICATIONS_BY_ADMIN = gql`
  query getPublicationsByAdmin(
    $publisher: String!
    $keyword: String
    $order: [String]
    $category: ID
    $published: Boolean
    $disapproved: Boolean
    $page: Int
    $pageSize: Int
  ) {
    publications(
      publicationState: PREVIEW
      filters: {
        publisher: { eq: $publisher }
        subject: { id: { eq: $category } }
        publishedAt: { null: $published }
        disapprovedAt: { null: $disapproved }
        or: [{ description: { containsi: $keyword } }, { title: { containsi: $keyword } }]
      }
      sort: $order
      pagination: { page: $page, pageSize: $pageSize }
    ) {
      data {
        id
        attributes {
          title
          description
          publisher
          date
          creator
          publishedAt
          createdAt
          disapprovedAt
          disapprovalReason
          subject {
            data {
              id
              attributes {
                name
              }
            }
          }
          relation {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
        }
      }

      meta {
        pagination {
          page
          pageSize
          pageCount
          total
        }
      }
    }
  }
`;

export const DELETE_PUBLICATION = gql`
  mutation deletePublication($id: ID!) {
    deletePublication(id: $id) {
      data {
        id
        attributes {
          title
          relation {
            data {
              id
            }
          }

          files {
            data {
              id
            }
          }
        }
      }
    }
  }
`;

export const GET_ALL_PUBLICATIONS_WITHOUT_FILTERS = gql`
  query GetAllPublicationsWithoutFilters {
    publications(
      publicationState: LIVE
      sort: "publishedAt:desc"
      pagination: { page: 1, pageSize: 100 }
    ) {
      data {
        id
        attributes {
          publishedAt
        }
      }
    }
  }
`;

export const UPDATE_PUBLICATION = gql`
  mutation UpdatePublication(
    $id: ID!
    $title: String!
    $description: String
    $coverage: String!
    $rights: String!
    $date: DateTime!
    $content: String!
    $subject: ID!
    $relation: ID
  ) {
    updatePublication(
      id: $id
      data: {
        title: $title
        description: $description
        coverage: $coverage
        rights: $rights
        date: $date
        content: $content
        subject: $subject
        relation: $relation
      }
    ) {
      data {
        id
      }
    }
  }
`;

export const destroyPublication = async (id: string) => cmsHttpClient.delete(`/publications/${id}`);
