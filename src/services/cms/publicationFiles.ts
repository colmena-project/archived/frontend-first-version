import { gql } from "@apollo/client";

export const GET_PUBLICATION_FILES = gql`
  query GetPublicationFiles($publicationId: ID) {
    publicationFiles(filters: { publication: { id: { eq: $publicationId } } }) {
      data {
        id
        attributes {
          title
          description
          language
          creator
          tags
          format
          extent_size
          extent_duration
          downloads
          media {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
        }
      }
    }
  }
`;

export const DELETE_PUBLICATION_FILE = gql`
  mutation deletePublicationFile($id: ID!) {
    deletePublicationFile(id: $id) {
      data {
        id
      }
    }
  }
`;

export const UPDATE_PUBLICATION_FILE = gql`
  mutation UpdatePublicationFile($id: ID!, $title: String!, $description: String) {
    updatePublicationFile(id: $id, data: { title: $title, description: $description }) {
      data {
        id
      }
    }
  }
`;
