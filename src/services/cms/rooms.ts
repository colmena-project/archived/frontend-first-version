import { gql } from "@apollo/client";

export const CREATE_ROOM = gql`
  mutation CreateRoom($token: String, $avatar: ID) {
    createRoom(data: { token: $token, avatar: $avatar }) {
      data {
        id
        attributes {
          token
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
              }
            }
          }
        }
      }
    }
  }
`;

export const UPDATE_ROOM = gql`
  mutation UpdateRoomr($id: ID!, $token: String, $avatar: ID) {
    updateRoom(id: $id, data: { token: $token, avatar: $avatar }) {
      data {
        id
        attributes {
          token
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_ROOM = gql`
  query GetRoom($token: String!) {
    rooms(filters: { token: { eq: $token } }) {
      data {
        id
        attributes {
          token
          avatar {
            data {
              id
              attributes {
                name
                alternativeText
                formats
                createdAt
                updatedAt
                url
                ext
                size
                mime
              }
            }
          }
        }
      }
    }
  }
`;
