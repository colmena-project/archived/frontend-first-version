import { gql } from "@apollo/client";

export const GET_CATEGORIES = gql`
  query GetCategories($language: String) {
    categories(
      filters: { language: { eq: $language } }
      sort: "name:asc"
      pagination: { limit: -1 }
    ) {
      data {
        id
        attributes {
          name
        }
      }
    }
  }
`;
