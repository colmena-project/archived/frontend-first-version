import axios from "axios";
import getConfig from "next/config";

const { serverRuntimeConfig } = getConfig();

const keycloakInstance = (token: string, customHeaders = {}) => {
  const api = axios.create({
    baseURL: `${serverRuntimeConfig.keycloak.baseUrl}`,
    headers: {
      Authorization: `Bearer ${token} `,
      ...customHeaders,
    },
  });
  return api;
};

export default keycloakInstance;
