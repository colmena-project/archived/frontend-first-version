import axios, { AxiosRequestConfig } from "axios";
import getConfig from "next/config";

const { serverRuntimeConfig } = getConfig();

const qs = require("qs");

export function getServiceToken() {
  const data = qs.stringify({
    client_id: serverRuntimeConfig.keycloak.serviceClientId,
    client_secret: serverRuntimeConfig.keycloak.serviceClientSecret,
    grant_type: "client_credentials",
  });
  const config: AxiosRequestConfig = {
    method: "post",
    url: `${serverRuntimeConfig.keycloak.baseUrl}/auth/realms/${serverRuntimeConfig.keycloak.realm}/protocol/openid-connect/token`,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data,
  };

  return axios(config);
}
