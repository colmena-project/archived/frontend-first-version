/* eslint-disable camelcase */
import internal from "@/services/keycloak";
import { UserInfoInterface } from "@/interfaces/index";
import { KeycloakAccessTokenInterface } from "@/interfaces/keycloak";
import jwt_decode from "jwt-decode";
import getConfig from "next/config";
import { RoleUserEnum } from "@/enums/*";

const { serverRuntimeConfig } = getConfig();

const getKeycloakUserId = (token: string) => {
  const jwtDecoded: KeycloakAccessTokenInterface = jwt_decode(token);
  return jwtDecoded.sub;
};

export function disable(serviceToken: string, userIdHash: string) {
  return internal(serviceToken).put(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${userIdHash}`,
    {
      enabled: false,
    },
  );
}

export function update(serviceToken: string, user: UserInfoInterface) {
  const nameArr = user.name.split(" ");

  return internal(serviceToken).put(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${getKeycloakUserId(
      user.keycloakToken,
    )}`,
    {
      firstName: nameArr[0].trim(),
      lastName: nameArr.slice(1, nameArr.length).join(" ").trim(),
      email: user.email,
      attributes: {
        language: [user.language || ""],
        region: [user.region || ""],
        country: [user.country || ""],
        position: [user.position || ""],
        phone: [user.phone || ""],
      },
    },
  );
}

export interface UpdateUserMemberInterface {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  attributes: {
    country: [];
    language: [];
    position: [];
    region: [];
    phone: [];
  };
}

export function updateUserMember(serviceToken: string, user: UpdateUserMemberInterface) {
  return internal(serviceToken).put(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${user.id}`,
    {
      ...user,
    },
  );
}

export function updateUserMemberPermission(
  serviceToken: string,
  userId: string,
  permission: RoleUserEnum.COLLABORATOR | RoleUserEnum.ADMIN,
) {
  const roleId =
    permission === RoleUserEnum.ADMIN
      ? process.env.KEYCLOAK_ADMINISTRATOR_ROLE_ID
      : process.env.KEYCLOAK_COLLABORATOR_ROLE_ID;

  const data = JSON.stringify([
    {
      clientRole: true,
      composite: false,
      containerId: serverRuntimeConfig.keycloak.colmenaPwaId,
      id: roleId,
      name: permission,
    },
  ]);

  return internal(serviceToken, { "Content-Type": "application/json" }).post(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${userId}/role-mappings/clients/${serverRuntimeConfig.keycloak.colmenaPwaId}`,
    data,
  );
}

export function deleteUserMemberPermission(
  serviceToken: string,
  userId: string,
  permission: RoleUserEnum.COLLABORATOR | RoleUserEnum.ADMIN,
) {
  const roleId =
    permission === RoleUserEnum.ADMIN
      ? process.env.KEYCLOAK_COLLABORATOR_ROLE_ID
      : process.env.KEYCLOAK_ADMINISTRATOR_ROLE_ID;

  const roleName =
    permission === RoleUserEnum.COLLABORATOR ? RoleUserEnum.ADMIN : RoleUserEnum.COLLABORATOR;

  const data = JSON.stringify([
    {
      clientRole: true,
      composite: false,
      containerId: serverRuntimeConfig.keycloak.colmenaPwaId,
      id: roleId,
      name: roleName,
    },
  ]);

  return internal(serviceToken, { "Content-Type": "application/json" }).delete(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${userId}/role-mappings/clients/${serverRuntimeConfig.keycloak.colmenaPwaId}`,
    { data },
  );
}

type CreateUserProps = {
  username: string;
  name: string;
  email: string;
  group: string;
  password: string;
  language: string;
};

export function create(serviceToken: string, data: CreateUserProps) {
  const { username, name, email, group, password, language } = data;
  const arrName = name.split(" ");
  const firstName = arrName[0];
  arrName.shift();
  const lastName = arrName.join(" ");
  return internal(serviceToken).post(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users`,
    {
      enabled: true,
      username,
      email,
      firstName,
      lastName,
      credentials: [
        {
          type: "password",
          value: password,
          temporary: false,
        },
      ],
      requiredActions: ["CONFIGURE_TOP", "VERIVY_EMAIL"],
      groups: [group],
      attributes: {
        language: [language],
        region: [""],
        country: [""],
        position: [""],
        phone: [""],
      },
    },
  );
}

export function changePassword(serviceToken: string, user: UserInfoInterface, newPassword: string) {
  return internal(serviceToken).put(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${getKeycloakUserId(
      user.keycloakToken,
    )}/reset-password`,
    {
      type: "password",
      value: newPassword,
      temporary: false,
    },
  );
}

export function resetPassword(serviceToken: string, userId: string, password: string) {
  return internal(serviceToken).put(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${userId}/reset-password`,
    {
      type: "password",
      value: password,
      temporary: false,
    },
  );
}

export function getUserByField(
  serviceToken: string,
  field: "email" | "username" | "search",
  value: string,
) {
  return internal(serviceToken).get(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users?${field}=${value}&exact=true`,
  );
}
