/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable camelcase */
import internal from "@/services/keycloak";
import getConfig from "next/config";
import { RoleUserEnum } from "@/enums/index";

const { serverRuntimeConfig } = getConfig();

type ListGroupsProps = {
  data: {
    id: string;
    name: string;
    path: string;
    subGroups: string[];
  }[];
};

export function setRoleMapping(
  serviceToken: string,
  userId: string,
  role: RoleUserEnum.COLLABORATOR | RoleUserEnum.ADMIN | "",
): Promise<ListGroupsProps> {
  const { collaboratorId, administratorId, colmenaPwaId } = serverRuntimeConfig.keycloak;
  const data = JSON.stringify([
    {
      clientRole: true,
      composite: false,
      containerId: colmenaPwaId,
      id: role === RoleUserEnum.COLLABORATOR ? collaboratorId : administratorId,
      name: role,
    },
  ]);
  return internal(serviceToken, { "Content-Type": "application/json" }).post(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/users/${userId}/role-mappings/clients/${colmenaPwaId}`,
    data,
  );
}
