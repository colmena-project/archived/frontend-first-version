/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable camelcase */
import internal from "@/services/keycloak";
import { GroupInterface } from "@/interfaces/keycloak";
import getConfig from "next/config";
import { RoleUserEnum } from "@/enums/*";

const { serverRuntimeConfig } = getConfig();

type ListGroupsProps = {
  data: {
    id: string;
    name: string;
    path: string;
    subGroups: string[];
  }[];
};

export function list(serviceToken: string): Promise<ListGroupsProps> {
  return internal(serviceToken).get(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/groups`,
  );
}

type GroupProps = {
  id: string;
  name: string;
  path: string;
  attributes: {
    audio_description_url: string[];
    social_media_whatsapp: string[];
    social_media_twitter: string[];
    social_media_facebook: string[];
    social_media_mastodon: string[];
    slogan: string[];
    social_media_instagram: string[];
    social_media_telegram: string[];
    email: string[];
    url: string[];
  };
  realmRoles: string[];
  clientRoles: {};
  subGroups: string[];
  access: {
    view: boolean;
    manage: boolean;
    manageMembership: boolean;
  };
};

type ResponseGroupProps = {
  data: GroupProps[];
};

export function show(serviceToken: string, id: string): Promise<ResponseGroupProps> {
  return internal(serviceToken).get(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/groups?briefRepresentation=false&search=${id}`,
  );
}

export function update(
  serviceToken: string,
  id: string,
  data: GroupInterface,
): Promise<GroupProps> {
  return internal(serviceToken).put(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/groups/${id}`,
    {
      ...data,
    },
  );
}

export function members(serviceToken: string, id: string): Promise<ResponseGroupProps> {
  return internal(serviceToken).get(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/groups/${id}/members`,
  );
}

export function getUsersByPermission(
  serviceToken: string,
  permission: RoleUserEnum.ADMIN | RoleUserEnum.COLLABORATOR,
): Promise<any> {
  return internal(serviceToken).get(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/clients/${serverRuntimeConfig.keycloak.colmenaPwaId}/roles/${permission}/users?first=0&max=999999`,
  );
}

export function getGroups(serviceToken: string, username = ""): Promise<ResponseGroupProps> {
  return internal(serviceToken).get(
    `/auth/admin/realms/${serverRuntimeConfig.keycloak.realm}/groups`,
    {
      params: {
        search: username,
        first: 0,
        max: 999999,
        briefRepresentation: false,
      },
    },
  );
}
