/* eslint-disable no-restricted-globals */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
import { CurrentPublicationStateEnum } from "@/enums/*";
import { CurrentPublication } from "@/interfaces/index";
import {
  getPublication,
  setPublicationAsSent,
  setPublicationErrorMessage,
  updatePublicationFileStatus,
} from "@/store/idb/models/publications";
import { createPublicationFile, uploadFileToCMS } from "../cms/file";

const worker = self;

worker.onmessage = async (event: MessageEvent<{ publicationId: number }>) => {
  const { publicationId } = event.data;

  const publication: CurrentPublication | null = await getPublication(publicationId);

  if (!publication) {
    return;
  }

  if (publication?.files) {
    const toUpload = publication.files.filter(
      (file) =>
        file.status === CurrentPublicationStateEnum.PENDING ||
        file.status === CurrentPublicationStateEnum.IN_PROGRESS,
    );

    let failureCounter = 0;

    for (let counter = 0; counter < toUpload.length; counter++) {
      const formData = new FormData();
      const file = toUpload[counter];

      const publicationVerification: CurrentPublication | null = await getPublication(
        publicationId,
      );

      if (publicationVerification?.files) {
        const checkStatus = publicationVerification.files?.find(
          (item) =>
            item.localId === toUpload[counter].localId &&
            [CurrentPublicationStateEnum.PENDING, CurrentPublicationStateEnum.IN_PROGRESS].includes(
              item.status,
            ),
        );

        if (!checkStatus) {
          // eslint-disable-next-line no-continue
          continue;
        }
      }

      formData.append("files", file.media);

      let fileId = 0;

      try {
        await updatePublicationFileStatus(
          publicationId,
          file.localId,
          CurrentPublicationStateEnum.IN_PROGRESS,
        );

        const [response] = await uploadFileToCMS(formData);

        if (!response?.id) {
          throw new Error("upload did not return id");
        }

        fileId = response.id;
      } catch (err) {
        const errMessage = err?.response?.data?.message ?? err.message;

        await updatePublicationFileStatus(
          publicationId,
          file.localId,
          CurrentPublicationStateEnum.ERROR,
          errMessage,
        );

        failureCounter++;

        // eslint-disable-next-line no-continue
        continue;
      }

      const creator = (file?.creator ?? "").replace(/[\W\s_\-*.]/gi, "");

      const identifier = `${creator}${new Date().getTime()}`;

      try {
        await createPublicationFile({
          title: file.title,
          description: file.description,
          media: fileId,
          publication: Number(publication.refId),
          creator: file.creator as string,
          language: file.language ?? "",
          format: file.format as string,
          extent_size: file.extentSize,
          extent_duration: file.duration,
          tags: file.tags,
          identifier,
          position: file.position,
        });

        await updatePublicationFileStatus(
          publicationId,
          file.localId,
          CurrentPublicationStateEnum.UPLOADED,
        );
      } catch (err) {
        await updatePublicationFileStatus(
          publicationId,
          file.localId,
          CurrentPublicationStateEnum.PENDING,
        );

        failureCounter++;

        await setPublicationErrorMessage(publicationId, "fail to upload");

        // eslint-disable-next-line no-continue
        continue;
      }
    }

    if (failureCounter > 0) {
      return;
    }

    const publicationVerify: CurrentPublication | null = await getPublication(publicationId);
    const filesVerify = publicationVerify?.files;

    if (
      filesVerify &&
      filesVerify.every((item) => item.status === CurrentPublicationStateEnum.UPLOADED)
    ) {
      await setPublicationAsSent(publicationId);
    }
  } else {
    await setPublicationAsSent(publicationId);
  }
};
