import ocsInstance, { ocsInstanceServerSide } from "@/services/ocs";
import { CreateUserInterface, UserInfoInterface } from "@/interfaces/ocs";
import { RoleUserEnum } from "@/enums/index";

interface UserObjParams {
  userid: string;
  displayName: string;
  email: string;
  groups: string[];
  password: string;
  subadmin?: string[];
  language: string;
}

export function createUser(
  displayName: string,
  email: string,
  group: string,
  permission: string,
  language: string,
  password: string,
): Promise<CreateUserInterface> {
  const userObj: UserObjParams = {
    userid: email.split("@")[0],
    displayName,
    email,
    groups: [group],
    password,
    language,
  };

  if (permission === RoleUserEnum.ADMIN) userObj.subadmin = [group];

  return ocsInstanceServerSide().post(`/users`, userObj);
}

export function deleteUser(userId: string | number) {
  return ocsInstanceServerSide().delete(`/users/${userId}`);
}

export function getUserInfo(): Promise<UserInfoInterface> {
  return ocsInstance().get("/user");
}

export async function disableUserNextCloud(userId: string) {
  return ocsInstanceServerSide().put(`/users/${userId}/disable?format=json`);
}
