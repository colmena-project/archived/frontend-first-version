/* eslint-disable @typescript-eslint/ban-types */
import useOcsFetch from "@/hooks/useOcsFetch";
import { GroupsItemInterface } from "@/interfaces/ocs";

export function listUsersByGroup(group: string, uuid = "", options?: {}): GroupsItemInterface {
  return useOcsFetch(`/groups/${group}?format=json&uuid=${uuid}`, {}, options);
}

export function getSpecificGroup(group: string): GroupsItemInterface {
  return useOcsFetch(`/groups/${group}?format=json`);
}
