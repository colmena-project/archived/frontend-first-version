import axios from "axios";
import { initializeStore } from "@/store/index";
import getConfig from "next/config";

const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();

const ocsInstance = () => {
  const { nexcloudToken } = initializeStore({}).getState().user.user;
  const api = axios.create({
    baseURL: `${publicRuntimeConfig.api.baseUrl}/ocs/v2.php/cloud`,
    headers: {
      "OCS-APIRequest": true,
      Authorization: `Bearer ${nexcloudToken}`,
    },
  });
  return api;
};

type CredentialsProps = {
  username: string;
  password: string;
};

export const ocsInstanceServerSide = (credentials: CredentialsProps | null = null) => {
  const api = axios.create({
    baseURL: `${publicRuntimeConfig.api.baseUrl}/ocs/v2.php/cloud`,
    auth: {
      username: !credentials ? serverRuntimeConfig.adminInfo.username : credentials.username,
      password: !credentials ? serverRuntimeConfig.adminInfo.password : credentials.password,
    },
    headers: {
      "OCS-APIRequest": true,
      "Content-Type": "application/json",
    },
  });
  return api;
};

export default ocsInstance;
