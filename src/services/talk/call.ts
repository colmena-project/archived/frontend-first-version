/* eslint-disable @typescript-eslint/ban-types */
import { JoinConversationParticipantsInterface, JoinCallInterface } from "@/interfaces/talk";
import talkInstance from "@/services/talk";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

const responseFormat = "?format=json";

const version = publicRuntimeConfig.ncTalkVersion || "v3";

export function joinAconversationToCallOrChat(
  roomName: string,
): Promise<JoinConversationParticipantsInterface> {
  return talkInstance(version).post(`room/${roomName}/participants/active${responseFormat}`);
}

export function joinCall(roomName: string, flag = 2): Promise<JoinCallInterface> {
  return talkInstance(version).post(`call/${roomName}${responseFormat}`, {
    flag,
  });
}

export function leaveCall(roomName: string): Promise<JoinCallInterface> {
  return talkInstance(version).delete(`call/${roomName}${responseFormat}`);
}
