/* eslint-disable @typescript-eslint/ban-types */
import { RoomTypeEnum, HoneycombTypesEnum, DirectoryNamesNCEnum } from "@/enums/*";
import useTalkFetch from "@/hooks/useTalkFetch";
import {
  RoomListInterface,
  RoomInterface,
  RoomParticipantsInterface,
  ReadOnlyRoomInterface,
  RoomCreateInterface,
  ParticipantCreateInterface,
  DeleteConversationInterface,
  RoomItemInterface,
  RoomListInterfaceV2,
} from "@/interfaces/talk";
import talkInstance, { talkInstanceServerSide } from "@/services/talk";
import getConfig from "next/config";
import { prepareSearchString } from "@/utils/utils";
import { useDispatch, useSelector } from "react-redux";
import { setHoneycombs } from "@/store/actions/honeycomb";
import { useMemo } from "react";
import { PropsHoneycombSelector } from "@/types/*";

const { publicRuntimeConfig } = getConfig();

const responseFormat = "?format=json";

const version = publicRuntimeConfig.ncTalkVersion || "v3";

export function getUsersConversations(options?: {}, tokenUuid = ""): RoomListInterfaceV2 {
  const dispatch = useDispatch();
  const honeycombRdx = useSelector(
    (state: { honeycomb: PropsHoneycombSelector }) => state.honeycomb,
  );

  if (!navigator.onLine) {
    return {
      data: honeycombRdx.honeycombs,
      error: null,
      /* eslint-disable-next-line */
      mutate: (data: any, shouldRevalidate: boolean | undefined) =>
        new Promise((resolve) => {
          resolve(null);
        }),
    };
  }

  const { data, mutate } = useTalkFetch(version)(
    `/room${responseFormat}&tokenUuid=${tokenUuid}`,
    {},
    options,
  );

  return useMemo(() => {
    const honeycombs = data?.ocs?.data ? data.ocs.data : undefined;

    if (honeycombs) {
      dispatch(setHoneycombs(honeycombs));
    }

    return {
      data: honeycombs,
      error: null,
      mutate,
    };
  }, [data, dispatch, mutate]);
}

export function getUsersConversationsAxios(clientside = true): Promise<RoomListInterface> {
  if (clientside) return talkInstance(version).get(`room${responseFormat}`);
  return talkInstanceServerSide(version).get(`room${responseFormat}`);
}

export function getOpenConversations(): RoomListInterface {
  return useTalkFetch(version)(`/listed-room${responseFormat}`);
}

export function getSingleConversation(token: string): RoomInterface {
  return useTalkFetch(version)(`/room/${token + responseFormat}`);
}

export function getSingleConversationAxios(token: string): Promise<RoomInterface> {
  return talkInstance(version).get(`/room/${token + responseFormat}`);
}

export function getChannels(options?: {}, uuid = ""): RoomListInterface {
  return useTalkFetch(version)(`/listed-room${responseFormat}&uuid${uuid}`, {}, options);
}

export function getRoomParticipants(
  token: string,
  uuid = "",
  options?: {},
): RoomParticipantsInterface {
  return useTalkFetch(version)(
    `/room/${token}/participants${responseFormat}&uuid=${uuid}`,
    {},
    options,
  );
}

export function setReadOnlyConversation(
  token: string,
  status: number,
): Promise<ReadOnlyRoomInterface> {
  return talkInstance(version).put(`room/${token}/read-only${responseFormat}`, {
    status,
  });
}

type Room = {
  /**
    1 = One to one, 2 = group, 3 = public, 4 = changelog
  */
  roomType?: RoomTypeEnum;

  /**
    user id (roomType = 1),
    group id (roomType = 2 - optional),
    circle id (roomType = 2, source = 'circles'],
    only available with circles-support capability))
  */
  invite?: string;

  /**
    only supported on roomType = 2 for groups and circles
  */
  source?: string;

  /**
    Not available for roomType = 1
  */
  roomName?: string;
};

export function createRoom(room: Room): Promise<RoomCreateInterface> {
  return talkInstance(version).post(`room${responseFormat}`, room);
}

export function createGroup(roomName: string): Promise<RoomCreateInterface> {
  return createRoom({
    roomType: RoomTypeEnum.GROUP,
    roomName,
  });
}

export function createConversation(userId: string): Promise<RoomCreateInterface> {
  return createRoom({
    roomType: RoomTypeEnum.CONVERSATION,
    invite: userId,
  });
}

export async function listableRoom(token: string): Promise<boolean> {
  try {
    await talkInstance(version).put(`room/${token}/listable${responseFormat}`, {
      scope: 2,
    });
    return true;
  } catch (e) {
    console.log("listableRooms", e);
    return false;
  }
}

export function removeYourselfFromAConversation(
  token: string,
): Promise<DeleteConversationInterface> {
  return talkInstance(version).delete(`room/${token}/participants/self${responseFormat}`);
}

export function removeConversation(token: string) {
  return talkInstance(version).delete(`room/${token}`);
}

export function addDescriptionConversation(
  token: string,
  description: string,
): Promise<ParticipantCreateInterface> {
  return talkInstance(version).put(`room/${token}/description${responseFormat}`, {
    description,
  });
}

export function addParticipantToConversation(
  token: string,
  newParticipant: string,
  clientside = true,
): Promise<ParticipantCreateInterface> {
  if (clientside)
    return talkInstance(version).post(`room/${token}/participants${responseFormat}`, {
      newParticipant,
      source: "users",
    });

  return talkInstanceServerSide(version).post(`room/${token}/participants${responseFormat}`, {
    newParticipant,
    source: "users",
  });
}

export async function findTokenChatByPath(
  path: string,
): Promise<string | boolean | HoneycombTypesEnum.ONE_TO_ONE> {
  const arr = path.split("/");
  const honeycombName =
    arr[0].toString().toLowerCase() === DirectoryNamesNCEnum.TALK.toLowerCase() ? arr[1] : arr[0];
  if (honeycombName === DirectoryNamesNCEnum.TALK) return HoneycombTypesEnum.ONE_TO_ONE;

  const response = await getUsersConversationsAxios();
  const rooms = response.data.ocs.data;
  // eslint-disable-next-line max-len
  const token = rooms.find(
    (item) => item.name.toLowerCase() === honeycombName.toLowerCase(),
  )?.token;

  if (!token) return false;

  return token;
}

export async function addToFavorite(token: string): Promise<boolean> {
  try {
    const favorited = await talkInstance(version).post(`room/${token}/favorite`);
    return favorited.status === 200;
  } catch (e) {
    console.log("error", e);
    return false;
  }
}

export async function removeFromFavorites(token: string): Promise<boolean> {
  try {
    const favorited = await talkInstance(version).delete(`room/${token}/favorite`);
    return favorited.status === 200;
  } catch (e) {
    console.log("error", e);
    return false;
  }
}

export function searchRooms(items: RoomItemInterface[], keyword: string) {
  if (!items) {
    return [];
  }

  return items.filter((item: RoomItemInterface) => {
    const roomName = prepareSearchString(item.displayName);
    const value = prepareSearchString(keyword);

    return roomName.indexOf(value) >= 0;
  });
}

export async function verifyDeleteAccessFromUserOnChat(token: string): Promise<boolean> {
  const response = await getSingleConversationAxios(token);
  const { data } = response.data.ocs;

  return data.canDeleteConversation;
}
