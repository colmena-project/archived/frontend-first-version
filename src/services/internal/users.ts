import internal from "@/services/internal";
import { MemberInterface, UserInfoInterface } from "@/interfaces/index";
import { RoleUserEnum } from "@/enums/*";
import useInternalFetch from "@/hooks/useInternalFetch";

type UpdateUserProps = {
  user: UserInfoInterface;
  updatedName: boolean;
  updatedEmail: boolean;
  updatePermission?: boolean;
  updatedLanguage?: boolean;
};

type ChangePasswordProps = {
  user: UserInfoInterface;
  currentPassword: string;
  newPassword: string;
};

export type CreateUserProps = {
  name: string;
  email: string;
  language: string;
  role: RoleUserEnum.ADMIN | RoleUserEnum.COLLABORATOR;
  user: UserInfoInterface;
};

type ResetPasswordProps = {
  username: string;
  password: string;
};

type UpdateUserMemberProps = {
  user: {
    id: string | undefined;
    username: string | undefined;
    permission: RoleUserEnum.ADMIN | RoleUserEnum.COLLABORATOR;
    firstName: string;
    lastName: string;
    email: string;
  };
  groupid: string;
  updatedName: boolean;
  updatedEmail: boolean;
  updatePermission?: boolean;
};

type SearchProps = {
  keyword: string;
};

const rootPath = "/users";

export function emergency(username: string) {
  return internal().put(`${rootPath}/emergency`, { username });
}

export function changePassword(body: ChangePasswordProps) {
  return internal().put(`${rootPath}/change-password`, { ...body });
}

export function update(body: UpdateUserProps) {
  return internal().put(`${rootPath}/update`, { ...body });
}

export function resetPassword(body: ResetPasswordProps) {
  return internal().put(`${rootPath}/reset-password`, { ...body });
}

export function create(body: CreateUserProps) {
  return internal().post(`${rootPath}/create`, { ...body });
}

export function updateUserMember(body: UpdateUserMemberProps) {
  return internal().put(`${rootPath}/update-member`, { ...body });
}

export function allUsers(options: any = {}) {
  return useInternalFetch(`${rootPath}/search`, { params: { keyword: "" } }, options);
}

export async function search(body: SearchProps): Promise<Array<MemberInterface>> {
  try {
    const result = await internal().get(`${rootPath}/search`, { params: body });
    return result.data;
  } catch (e) {
    return [];
  }
}
