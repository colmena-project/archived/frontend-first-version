/* eslint-disable @typescript-eslint/ban-types */
import internal from "@/services/internal";
import { UserInfoJitsiInterface } from "@/interfaces/jitsi";

export function generateTokenJwt(userInfo: UserInfoJitsiInterface, room: string) {
  return internal().post(`/jitsi/token`, {
    userInfo,
    room,
  });
}
