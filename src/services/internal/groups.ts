/* eslint-disable @typescript-eslint/ban-types */
import internal from "@/services/internal";
import { MediaInfoInterface } from "@/interfaces/index";
import useInternalFetch from "@/hooks/useInternalFetch";
import { GroupsInterface } from "@/interfaces/keycloak";
import { AxiosPromise } from "axios";

export function getGroups(name = ""): AxiosPromise<GroupsInterface> {
  return internal().get(`/groups`, {
    params: {
      name,
    },
  });
}

export async function getGroupByName(username: string) {
  const groups = await getGroups(username);

  if (!groups.data?.data) return false;

  return groups.data.data.find(({ name }) => name === username);
}

export function show(id: string) {
  return internal().get(`/groups/${id}`);
}

export function update(id: string, data: MediaInfoInterface) {
  return internal().put(`/groups/${id}`, {
    data,
  });
}

export function members(groupId: string, options?: {}) {
  return useInternalFetch(`/groups/${groupId}/members`, {}, options);
}

export function getMembers(groupId: string) {
  return internal().put(`/groups/${groupId}/members`);
}
