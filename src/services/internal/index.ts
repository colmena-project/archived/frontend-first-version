import axios from "axios";
// import { initializeStore } from "@/store/index";

const internalInstance = () => {
  // const { nexcloudToken, keycloakToken } = initializeStore({}).getState().user.user;
  const api = axios.create();
  return api;
};

export default internalInstance;
