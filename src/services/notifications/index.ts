import axios from "axios";
import { initializeStore } from "@/store/index";
import getConfig from "next/config";

const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();

const noficationsInstance = (version = "v1") => {
  const { nexcloudToken } = initializeStore({}).getState().user.user;

  const api = axios.create({
    baseURL: `${publicRuntimeConfig.api.baseUrl}/ocs/v2.php/apps/notifications/api/${version}`,
    headers: {
      "OCS-APIRequest": true,
      Authorization: `Bearer ${nexcloudToken}`,
    },
  });
  return api;
};

export const noficationsInstanceServerSide = (version = "v1") => {
  const api = axios.create({
    baseURL: `${publicRuntimeConfig.api.baseUrl}/ocs/v2.php/apps/notifications/api/${version}`,
    auth: {
      username: serverRuntimeConfig.adminInfo.username,
      password: serverRuntimeConfig.adminInfo.password,
    },
    headers: {
      "OCS-APIRequest": true,
    },
  });
  return api;
};

export default noficationsInstance;
