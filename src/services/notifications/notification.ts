import useNotificationFetch from "@/hooks/useNotificationFetch";
import noficationsInstance from ".";

const responseFormat = "?format=json";
const versionOne = "v1";
const versionTwo = "v2";

export function getNotifications(options: any) {
  return useNotificationFetch(versionOne)(`/notifications${responseFormat}`, {}, options);
}

export function removeNotification(id: string | number) {
  return noficationsInstance(versionOne).delete(`notifications/${id}?format=json`);
}

export function removeAllNotifications() {
  return noficationsInstance(versionTwo).delete(`notifications`);
}
