import axios from "axios";
import { initializeStore } from "@/store/index";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

const coreInstance = () => {
  const { nexcloudToken } = initializeStore({}).getState().user.user;
  const api = axios.create({
    baseURL: `${publicRuntimeConfig.api.baseUrl}/ocs/v2.php/core`,
    headers: {
      "OCS-APIRequest": true,
      Authorization: `Bearer ${nexcloudToken}`,
    },
  });
  return api;
};
export default coreInstance;
