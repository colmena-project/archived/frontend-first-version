import ocs from "@/services/wss";
import { RoomInterface, SocketIdInterface, UsersRoomInterface } from "@/interfaces/wss";

export function verifyRoomExists(roomId: string): Promise<RoomInterface> {
  return ocs().get(`/api/room-exists/${roomId}`);
}

export function verifySocketIdExists(socketId: string): Promise<SocketIdInterface> {
  return ocs().get(`/api/socketId-exists/${socketId}`);
}

export function verifyRoomsByUser(userId: string): Promise<UsersRoomInterface> {
  return ocs().get(`/api/user-rooms/${userId}`);
}
