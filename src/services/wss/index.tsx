import axios from "axios";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

const wssInstance = () => {
  const api = axios.create({
    baseURL: `${publicRuntimeConfig.wss.baseUrl}`,
  });
  return api;
};

export default wssInstance;
