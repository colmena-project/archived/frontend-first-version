import axios from "axios";

const api = () => {
  const api = axios.create({
    baseURL: "/api",
    headers: {
      Accept: "application/json",
    },
  });

  return api;
};

export default api;
