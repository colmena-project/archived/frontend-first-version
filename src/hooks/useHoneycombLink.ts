import { useMemo } from "react";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getHoneycombLink } from "@/utils/offlineNavigation";

export default function useHoneycombLink(
  token: string,
  displayName: string,
  canDeleteConversation: number,
) {
  const connectionStatus = useConnectionStatus();
  return useMemo(
    () => getHoneycombLink(token, displayName, canDeleteConversation, connectionStatus),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [connectionStatus],
  );
}
