import notifications from "@/services/notifications";
import useSWR from "swr";

// eslint-disable-next-line @typescript-eslint/ban-types
const useNotificationFetch = (version: string) => (uri: string, params?: {}, options?: {}) => {
  const { data, error, mutate, revalidate, isValidating } = useSWR(
    uri,
    async (uri) => {
      const response = await notifications(version).get(uri, { ...params });

      return response.data;
    },
    {
      ...options,
    },
  );

  return { data, error, mutate, revalidate, isValidating };
};

export default useNotificationFetch;
