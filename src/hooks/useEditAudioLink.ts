import { useMemo } from "react";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getEditAudioLink } from "@/utils/offlineNavigation";

export default function useEditAudioLink(id: string) {
  const connectionStatus = useConnectionStatus();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return useMemo(() => getEditAudioLink(id, connectionStatus), [connectionStatus]);
}
