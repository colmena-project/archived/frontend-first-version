import { useCallback, useState } from "react";
import { useMutation } from "@apollo/client";
import { DELETE_PUBLICATION_FILE } from "@/services/cms/publicationFiles";

export function useDeletePublicationFile() {
  const [deleting, setDeleting] = useState(false);

  const [mutation, { data: response, reset, error }] = useMutation(DELETE_PUBLICATION_FILE);

  const deleteFile = useCallback(
    async (fileId: string, onCompleted: () => void) => {
      setDeleting(true);

      try {
        await mutation({
          variables: {
            id: fileId,
          },
          onCompleted,
        });
      } catch (err) {
        //
      } finally {
        setDeleting(false);
      }
    },
    [mutation],
  );

  return {
    deleteFile,
    deleting,
    response,
    reset,
    error,
  };
}
