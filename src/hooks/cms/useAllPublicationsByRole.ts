import { useQuery } from "@apollo/client";

import {
  GET_PUBLICATIONS_BY_COLABORATOR,
  GET_PUBLICATIONS_BY_ADMIN,
} from "@/services/cms/publications";
import { PublicationQueryCollectionInterface } from "@/interfaces/cms";
import { UserInfoInterface } from "@/interfaces/index";
import { RoleUserEnum } from "@/enums/*";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export default function useAllPublicationsByRole(user: UserInfoInterface) {
  const connectionStatus = useConnectionStatus();

  if (user.role !== RoleUserEnum.ADMIN) {
    const { data, loading, refetch, fetchMore } = useQuery<PublicationQueryCollectionInterface>(
      GET_PUBLICATIONS_BY_COLABORATOR,
      {
        fetchPolicy: "network-only",
        variables: {
          creator: user.id,
          publisher: String(user.media.name),
          keyword: "",
          order: "createdAt:desc",
          category: undefined,
          published: undefined,
          disapproved: undefined,
          page: 1,
          pageSize: 10,
          skip: !connectionStatus,
        },
      },
    );

    return {
      publications: data?.publications?.data ?? [],
      loading,
      refetch,
      pagination: data?.publications.meta.pagination,
      fetchMore,
    };
  }

  const { data, loading, refetch, fetchMore } = useQuery<PublicationQueryCollectionInterface>(
    GET_PUBLICATIONS_BY_ADMIN,
    {
      fetchPolicy: "network-only",
      variables: {
        publisher: String(user.media.name),
        keyword: "",
        order: "createdAt:desc",
        category: undefined,
        published: undefined,
        disapproved: undefined,
        page: 1,
        pageSize: 10,
      },
    },
  );

  return {
    publications: data?.publications?.data ?? [],
    loading,
    refetch,
    pagination: data?.publications.meta.pagination,
    fetchMore,
  };
}
