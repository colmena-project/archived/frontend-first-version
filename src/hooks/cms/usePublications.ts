import { useQuery } from "@apollo/client";

import { GET_ALL_PUBLICATIONS } from "@/services/cms/publications";
import { useMemo } from "react";
import { PublicationQueryCollectionInterface } from "@/interfaces/cms";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export default function usePublications(publisher: string | undefined = undefined) {
  const connectionStatus = useConnectionStatus();
  const { data, loading } = useQuery<PublicationQueryCollectionInterface>(GET_ALL_PUBLICATIONS, {
    variables: { publisher },
    fetchPolicy: "network-only",
    skip: !connectionStatus,
  });

  const publications = useMemo(() => {
    if (data && !loading) {
      return data.publications.data;
    }

    return [];
  }, [data, loading]);

  return { publications, loading };
}
