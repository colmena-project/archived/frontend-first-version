import { useQuery } from "@apollo/client";

import { GET_CATEGORIES } from "@/services/cms/categories";
import { CategoryQueryCollectionInterface } from "@/interfaces/cms";
import { useMemo } from "react";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export default function useCategories(language: string | undefined = undefined) {
  const connectionStatus = useConnectionStatus();

  const { data, loading } = useQuery<CategoryQueryCollectionInterface>(GET_CATEGORIES, {
    variables: { language },
    fetchPolicy: connectionStatus ? "cache-and-network" : "cache-only",
  });

  const categories = useMemo(() => {
    if (data && !loading) {
      return data.categories.data.map((category) => ({
        id: category.id,
        name: category.attributes.name,
      }));
    }

    return [];
  }, [data, loading]);

  return { categories, loading };
}
