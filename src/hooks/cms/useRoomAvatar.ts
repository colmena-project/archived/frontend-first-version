import { useMemo } from "react";
import getConfig from "next/config";
import useRoom from "@/hooks/cms/useRoom";
import { getStrapiImageFormats } from "@/utils/utils";

const { publicRuntimeConfig } = getConfig();

export default function useRoomAvatar(token: string) {
  const baseUrl = publicRuntimeConfig.publicationsGraphQL?.baseUrl;
  const { room, loading } = useRoom(token);
  return useMemo(() => {
    let formats: any = {};
    if (room) {
      if (typeof room?.attributes.avatar.data === "object") {
        formats = getStrapiImageFormats(room.attributes.avatar);
        if (formats && !formats?.thumbnail) {
          formats.thumbnail = formats.default;
        }
      }
    }

    return { ...formats, baseUrl, loading };
  }, [baseUrl, room, loading]);
}
