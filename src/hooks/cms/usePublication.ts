import { useQuery } from "@apollo/client";

import { GET_PUBLICATION } from "@/services/cms/publications";
import { useMemo } from "react";
import { PublicationQueryInterface, SinglePublicationInterface } from "@/interfaces/cms";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { UserInfoInterface } from "@/interfaces/index";
import { RoleUserEnum } from "@/enums/*";

export default function usePublication(id: string, user: UserInfoInterface) {
  const connectionStatus = useConnectionStatus();

  const { data, loading } = useQuery<SinglePublicationInterface>(GET_PUBLICATION, {
    variables: { id },
    fetchPolicy: connectionStatus ? "cache-and-network" : "cache-only",
  });

  const publication = useMemo<null | PublicationQueryInterface>(() => {
    const isAdmin = user.role === RoleUserEnum.ADMIN;

    if (!data || loading) {
      return null;
    }

    const publicationData = data.publication.data;

    if (publicationData?.attributes?.publishedAt) {
      return publicationData;
    }

    if (isAdmin || publicationData.attributes.creator === user.id) {
      return publicationData;
    }

    return null;
  }, [data, loading, user]);

  return { publication, loading };
}
