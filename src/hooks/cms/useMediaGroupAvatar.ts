import { useMemo } from "react";
import getConfig from "next/config";
import useMediaGroup from "@/hooks/cms/useMediaGroup";
import { getStrapiImageFormats } from "@/utils/utils";

const { publicRuntimeConfig } = getConfig();

export default function useMediaGroupAvatar(token: string) {
  const baseUrl = publicRuntimeConfig.publicationsGraphQL?.baseUrl;
  const { mediaGroup, loading } = useMediaGroup(token);
  return useMemo(() => {
    let formats: any = {};
    if (mediaGroup) {
      if (typeof mediaGroup?.attributes.avatar.data === "object") {
        formats = getStrapiImageFormats(mediaGroup.attributes.avatar);
        if (formats && !formats?.thumbnail) {
          formats.thumbnail = formats.default;
        }
      }
    }

    return { ...formats, baseUrl, loading };
  }, [baseUrl, mediaGroup, loading]);
}
