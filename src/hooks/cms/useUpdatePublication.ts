import { EditPublicationFormValues } from "@/components/pages/publications/edit/EditPublicationForm";
import { uploadFileToCMS } from "@/services/cms/file";
import { UPDATE_PUBLICATION } from "@/services/cms/publications";
import { useMutation } from "@apollo/client";
import { useCallback, useState } from "react";

export function useUpdatePublication() {
  const [updating, setUpdating] = useState(false);

  const [mutation, { data: response, reset, error }] = useMutation(UPDATE_PUBLICATION);

  const update = useCallback(
    async (publicationId: string, payload: EditPublicationFormValues, onCompleted: () => void) => {
      let thumbId = payload.relation;

      setUpdating(true);

      try {
        if (payload.image && typeof payload.image !== "string") {
          const formData = new FormData();

          formData.append("files", payload.image);

          const [response] = await uploadFileToCMS(formData);

          thumbId = response.id;
        }

        await mutation({
          variables: {
            id: publicationId,
            title: payload.title,
            description: payload.description,
            coverage: payload.description,
            rights: payload.license,
            date: payload.publicationDate,
            content: payload.content,
            relation: thumbId,
            subject: payload.category,
          },
          onCompleted,
        });
      } catch (err) {
        //
      } finally {
        setUpdating(false);
      }
    },
    [mutation],
  );

  return {
    update,
    updating,
    response,
    reset,
    error,
  };
}
