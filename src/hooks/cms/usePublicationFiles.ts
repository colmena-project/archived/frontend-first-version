import { useQuery } from "@apollo/client";

import { GET_PUBLICATION_FILES } from "@/services/cms/publicationFiles";
import { useMemo } from "react";
import {
  PublicationFileQueryCollectionInterface,
  PublicationFileQueryInterface,
} from "@/interfaces/cms";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export default function usePublicationFiles(publicationId: string) {
  const connectionStatus = useConnectionStatus();
  const { data, loading, refetch } = useQuery<PublicationFileQueryCollectionInterface>(
    GET_PUBLICATION_FILES,
    {
      variables: { publicationId },
      fetchPolicy: connectionStatus ? "cache-and-network" : "cache-only",
    },
  );

  const publicationFiles = useMemo<false | PublicationFileQueryInterface[]>(() => {
    if (data && !loading) {
      return data.publicationFiles.data;
    }

    return false;
  }, [data, loading]);

  return { publicationFiles, loading, refetch };
}
