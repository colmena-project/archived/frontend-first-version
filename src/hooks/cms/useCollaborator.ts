import { useQuery } from "@apollo/client";

import { useMemo } from "react";
import { CollaboratorInterface, CollaboratorQueryCollectionInterface } from "@/interfaces/cms";
import { GET_COLLABORATOR } from "@/services/cms/collaborators";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export default function useCollaborator(username: string) {
  const connectionStatus = useConnectionStatus();
  const { data, loading, refetch } = useQuery<CollaboratorQueryCollectionInterface>(
    GET_COLLABORATOR,
    {
      variables: { username },
      fetchPolicy: "network-only",
      nextFetchPolicy: "cache-first",
      skip: !connectionStatus,
    },
  );

  const collaborator = useMemo<false | CollaboratorInterface>(() => {
    if (data && !loading) {
      return data.collaborators.data[0];
    }

    return false;
  }, [data, loading]);

  return { collaborator, loading, refetch };
}
