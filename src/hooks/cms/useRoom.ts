import { useQuery } from "@apollo/client";

import { useMemo } from "react";
import { RoomInterface, RoomQueryCollectionInterface } from "@/interfaces/cms";
import { GET_ROOM } from "@/services/cms/rooms";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

type PropsReturn = {
  room: false | RoomInterface;
  loading: boolean;
  refetch: () => Promise<any> | false;
};
export default function useRoom(token: string): PropsReturn {
  const connectionStatus = useConnectionStatus();
  const { data, loading, refetch } = useQuery<RoomQueryCollectionInterface>(GET_ROOM, {
    variables: { token },
    fetchPolicy: connectionStatus ? "cache-and-network" : "cache-only",
  });

  const room = useMemo<false | RoomInterface>(() => {
    if (data && !loading) {
      return data.rooms.data[0];
    }

    return false;
  }, [data, loading]);

  return { room, loading, refetch };
}
