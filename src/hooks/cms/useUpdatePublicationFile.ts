import { useCallback, useState } from "react";
import { useMutation } from "@apollo/client";
import { UPDATE_PUBLICATION_FILE } from "@/services/cms/publicationFiles";

export function useUpdatePublicationFile() {
  const [updating, setUpdating] = useState(false);

  const [mutation, { data: response, reset, error }] = useMutation(UPDATE_PUBLICATION_FILE);

  const update = useCallback(
    async (
      fileId: string,
      payload: { description: string; title: string },
      onCompleted: () => void,
    ) => {
      setUpdating(true);

      try {
        await mutation({
          variables: {
            id: fileId,
            title: payload.title,
            description: payload.description,
          },
          onCompleted,
        });
      } catch (err) {
        //
      } finally {
        setUpdating(false);
      }
    },
    [mutation],
  );

  return {
    update,
    updating,
    response,
    reset,
    error,
  };
}
