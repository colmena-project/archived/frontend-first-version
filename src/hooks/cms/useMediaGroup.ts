import { useQuery } from "@apollo/client";

import { useMemo } from "react";
import { MediaGroupInterface, MediaGroupQueryCollectionInterface } from "@/interfaces/cms";
import { GET_MEDIA_GROUP } from "@/services/cms/mediaGroups";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";

export default function useMediaGroup(token: string) {
  const connectionStatus = useConnectionStatus();
  const { data, loading, refetch } = useQuery<MediaGroupQueryCollectionInterface>(GET_MEDIA_GROUP, {
    variables: { token },
    fetchPolicy: connectionStatus ? "cache-and-network" : "cache-only",
    nextFetchPolicy: connectionStatus ? "cache-first" : "cache-only",
  });

  const mediaGroup = useMemo<false | MediaGroupInterface>(() => {
    if (data && !loading) {
      return data.mediaGroups.data[0];
    }

    return false;
  }, [data, loading]);

  return { mediaGroup, loading, refetch };
}
