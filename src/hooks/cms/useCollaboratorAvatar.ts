import { useMemo } from "react";
import getConfig from "next/config";
import useCollaborator from "@/hooks/cms/useCollaborator";
import { getStrapiImageFormats } from "@/utils/utils";

const { publicRuntimeConfig } = getConfig();

export default function useCollaboratorAvatar(username: string) {
  const baseUrl = publicRuntimeConfig.publicationsGraphQL?.baseUrl;
  const { collaborator, loading } = useCollaborator(username);
  return useMemo(() => {
    let formats: any = {};
    if (collaborator) {
      if (typeof collaborator?.attributes.avatar.data === "object") {
        formats = getStrapiImageFormats(collaborator.attributes.avatar);
        if (formats && !formats.thumbnail) {
          formats.thumbnail = formats.default;
        }
      }
    }

    return { ...formats, baseUrl, loading };
  }, [baseUrl, collaborator, loading]);
}
