import api from "@/services/internal";
import useSWR from "swr";

// eslint-disable-next-line @typescript-eslint/ban-types
const useFetch = (uri: string, params?: {}, options?: {}) => {
  const { data, error, mutate, isValidating } = useSWR(
    uri,
    async (uri) => {
      const response = await api().get(uri, { ...params });

      return response.data;
    },
    {
      ...options,
    },
  );

  return { data, error, mutate, isValidating };
};

export default useFetch;
