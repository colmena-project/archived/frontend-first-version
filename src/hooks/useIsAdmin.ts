import { useMemo } from "react";
import { useSelector } from "react-redux";
import { RoleUserEnum } from "../enums";
import { PropsUserSelector } from "../types";

export default function useIsAdmin() {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);

  const isAdmin = useMemo(() => userRdx.user.role === RoleUserEnum.ADMIN, [userRdx.user.role]);

  return isAdmin;
}
