import { useState } from "react";
import { signOut } from "next-auth/client";
import { toast } from "@/utils/notifications";
import {
  cleanAccessedPages,
  cleanAllLibraryItems,
  setFilenameRecorder,
  setPathRecorder,
} from "@/utils/utils";

import { clearCurrentOfflinePage } from "@/utils/offlineNavigation";

import { useTranslation } from "next-i18next";

import { emergency } from "@/services/internal/users";
import useUsernameLocalStorage from "./useUsernameLocalStorage";

const useEmergency = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [showBackdrop, setShowBackdrop] = useState(false);
  const [open, setOpen] = useState(false);
  const { t: c } = useTranslation("common");

  const { username } = useUsernameLocalStorage();

  const toggleConfirmModal = () => setOpen(!open);

  const start = async () => {
    try {
      toggleConfirmModal();
      setShowBackdrop(true);
      setIsLoading(true);

      await logout();
      await emergency(String(username));

      cleanIndexedDB();
      cleanLocalStorage();
      cleanCookie();
      setShowBackdrop(false);
      setIsLoading(false);
      window.location.href = "/login";
    } catch (e) {
      setShowBackdrop(false);
      setIsLoading(false);
      toast(c("genericErrorMessage "), "error");
    }
  };

  const cleanCookie = () => {
    const cookies = document.cookie.split(";");
    cookies.forEach((cookie) => {
      const cookieName = cookie.trim().split("=")[0];
      document.cookie = `${cookieName}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/`;
    });
  };

  const cleanLocalStorage = async () => {
    Promise.all([setFilenameRecorder(""), setPathRecorder(""), cleanAccessedPages()]);

    localStorage.setItem("recentSearches", "");
    clearCurrentOfflinePage();
    cleanAllLibraryItems();
  };

  const cleanIndexedDB = async () => {
    const dbName = "colmenaDatabase";
    const deleteRequest = window.indexedDB.deleteDatabase(dbName);

    deleteRequest.onerror = () => {
      toast(c("genericErrorMessage"), "error");
    };
  };

  const logout = async () => {
    await signOut({ redirect: false });
  };

  return { start, showBackdrop, isLoading, toggleConfirmModal, open };
};

export default useEmergency;
