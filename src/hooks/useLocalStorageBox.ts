import { useEffect, useState } from "react";

export default function useLocalStorageBox() {
  const [quota, setQuota] = useState<number | undefined>(undefined);
  const [usage, setUsage] = useState<number | undefined>(undefined);

  const getUsedQuota = () => {
    if (navigator.storage && navigator.storage.estimate) {
      navigator.storage.estimate().then((quota) => {
        setQuota(quota.quota);
        setUsage(quota.usage);
      });
    }
  };

  useEffect(() => {
    getUsedQuota();
  }, []);

  return { quota, usage, getUsedQuota };
}
