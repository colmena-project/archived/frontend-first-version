import { useState } from "react";
import { deleteAllMessages } from "@/store/idb/models/chat";
import { removeAllFiles } from "@/store/idb/models/files";
import { removeAllFilesQuick } from "@/store/idb/models/filesQuickBlob";
import { toast } from "@/utils/notifications";
import { useTranslation } from "next-i18next";
import useLocalStorageBox from "./useLocalStorageBox";

export const useCloudAndStorageOptions = () => {
  const { getUsedQuota } = useLocalStorageBox();
  const { t: c } = useTranslation("common");
  const [isOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showBackdrop, setShowBackdrop] = useState(false);

  const confirmCleanLocalStorage = async () => {
    try {
      setIsLoading(true);
      setShowBackdrop(true);

      await removeAllFiles();
      await deleteAllMessages();
      await removeAllFilesQuick();

      getUsedQuota();

      toast(c("genericSuccessMessage"), "success");
    } catch (e) {
      toast(c("genericErrorMessage"), "error");
    } finally {
      onClose();
    }
  };

  const handleCleanLocalStorage = () => setIsOpen(true);

  const onClose = () => {
    setIsOpen(false);
    setIsLoading(false);
    setShowBackdrop(false);
  };

  return {
    isOpen,
    showBackdrop,
    isLoading,

    handleCleanLocalStorage,
    confirmCleanLocalStorage,
    onClose,
  };
};
