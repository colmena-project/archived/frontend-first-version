import EventEmitter from "events";

type Props = {
  ee: EventEmitter;
};

export const useWaveformEmitter = ({ ee }: Props) => {
  const emitterZoomIn = () => ee.emit("zoomin");
  const emitterZoomOut = () => ee.emit("zoomout");
  const emitterStop = () => ee.emit("stop");
  const emitterPlay = () => ee.emit("play");
  const emitterPause = () => ee.emit("pause");
  const emitterRecord = () => ee.emit("record");

  const emitterMasterVolumeChange = (newValue: number | number[]) =>
    ee.emit("mastervolumechange", newValue);

  return {
    emitterRecord,
    emitterPause,
    emitterPlay,
    emitterStop,
    emitterZoomIn,
    emitterZoomOut,
    emitterMasterVolumeChange,
  };
};
