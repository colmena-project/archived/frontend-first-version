import { useContext } from "react";
import { ConnectionStatusContext } from "@/providers/ConnectionStatusProvider";

export const useConnectionStatus = () => useContext(ConnectionStatusContext);
