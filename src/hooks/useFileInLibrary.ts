import { useCallback, useState } from "react";

import { getDataFile, listFile } from "@/services/webdav/files";
import { LibraryItemInterface, TimeDescriptionInterface } from "@/interfaces/index";
import { useTranslation } from "next-i18next";
import { arrayBufferToBlob } from "blob-util";
import { createFile, findByFilename } from "@/store/idb/models/files";

export default function useFileInLibrary(userId: string) {
  const [fetching, setFetching] = useState<null | string>(null);

  const { t: c } = useTranslation("common");

  const timeDescription: TimeDescriptionInterface = c("timeDescription", { returnObjects: true });

  const fetchFile = useCallback(
    async (file: LibraryItemInterface) => {
      setFetching(file.id);

      let fileData: false | LibraryItemInterface;

      try {
        fileData = await getDataFile(userId, file.filename, timeDescription);
      } catch (err) {
        setFetching(null);

        return null;
      }

      if (!fileData) {
        setFetching(null);

        return null;
      }

      const { filename, basename } = fileData as LibraryItemInterface;
      const filen = filename.split("/");
      const defaultTitle = filen[filen.length - 1];
      const localFile = await findByFilename(userId, filename);

      let blob: Blob;

      try {
        if (!localFile) {
          const result: any = await listFile(userId, filename);

          blob = arrayBufferToBlob(result);

          await createFile({
            title: defaultTitle,
            basename,
            filename,
            arrayBufferBlob: result,
            type: blob?.type,
            size: blob?.size,
            createdAt: new Date(),
            userId,
          });
        } else {
          blob = arrayBufferToBlob(localFile?.arrayBufferBlob);
        }
      } catch (err) {
        setFetching(null);
        return null;
      }

      let tags = "";

      if (fileData.tags) {
        tags = fileData.tags.join(", ");
      }

      const mediaFile = new File([blob], fileData.filename, { type: fileData.mime as string });

      const title = fileData.title ?? fileData.basename.split(".")[0].replace(/-/g, " ");

      setFetching(null);

      return {
        description: fileData.description ?? "",
        title,
        id: fileData.id,
        size: fileData.size,
        format: fileData.sizeFormatted,
        language: fileData.language,
        creator: fileData.ownerId,
        tags,
        media: mediaFile,
        creatorName: fileData.ownerName,
      };
    },
    [userId, timeDescription],
  );

  return {
    fetchFile,
    fetching,
  };
}
