import { useCallback, useMemo } from "react";

export default function useUploadPublicationFiles() {
  const worker = useMemo(() => {
    if (typeof Worker === "undefined") {
      return null;
    }

    return new Worker(new URL("../services/workers/upload-publication-files.ts", import.meta.url));
  }, []);

  const runWorker = useCallback(
    (data: { publicationId: number; cmsMode: string }) => {
      if (worker) {
        worker.postMessage(data);
      }
    },
    [worker],
  );

  return {
    runWorker,
    worker,
  };
}
