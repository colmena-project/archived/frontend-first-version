import React, { useEffect, useState } from "react";

type UseUsernameLocalStorageHook = {
  username: string | null;
  setUsername: React.Dispatch<React.SetStateAction<string | null>>;
};
const useUsernameLocalStorage = (): UseUsernameLocalStorageHook => {
  const [username, setUsername] = useState(() => localStorage.getItem("username"));

  useEffect(() => {
    localStorage.setItem("username", username ?? "");
  }, [username]);

  return {
    username,
    setUsername,
  };
};

export default useUsernameLocalStorage;
