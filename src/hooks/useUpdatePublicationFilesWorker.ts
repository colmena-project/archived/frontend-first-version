import { useCallback, useMemo } from "react";

export default function useUpdatePublicationFilesWorker() {
  const worker = useMemo(() => {
    if (typeof Worker === "undefined") {
      return null;
    }

    return new Worker(new URL("../services/workers/update-publication-files.ts", import.meta.url));
  }, []);

  const runWorker = useCallback(
    (data: { publicationId: number }) => {
      if (worker) {
        worker.postMessage(data);
      }
    },
    [worker],
  );

  return {
    runWorker,
    worker,
  };
}
