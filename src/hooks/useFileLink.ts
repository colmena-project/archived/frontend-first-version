import { useMemo } from "react";
import { useConnectionStatus } from "@/hooks/useConnectionStatus";
import { getFileLink } from "@/utils/offlineNavigation";

export default function useFileLink(id: string) {
  const connectionStatus = useConnectionStatus();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return useMemo(() => getFileLink(id, connectionStatus), [connectionStatus]);
}
