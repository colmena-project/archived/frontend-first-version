import {
  SET_CURRENT_PUBLICATION,
  RESET_CURRENT_PUBLICATION,
  UPDATE_CURRENT_PUBLICATION,
} from "@/store/actions/index";
import { CurrentPublication } from "@/interfaces/index";

type InitialStateProps = {
  currentPublication: CurrentPublication | null;
};

const initialState: InitialStateProps = {
  currentPublication: null,
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case SET_CURRENT_PUBLICATION:
      return { ...state, currentPublication: action.payload };
    case RESET_CURRENT_PUBLICATION:
      return { ...state, currentPublication: null };
    case UPDATE_CURRENT_PUBLICATION:
      return {
        ...state,
        currentPublication: {
          ...state.currentPublication,
          ...action.payload,
        },
      };
    default:
      return state;
  }
};

export default reducer;
