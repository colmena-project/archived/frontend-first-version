import { RoomItemInterface } from "@/interfaces/talk";
import { SET_CHANNELS, ADD_CHANNEL, EDIT_CHANNEL, REMOVE_CHANNEL } from "@/store/actions/index";

type initialStateProps = {
  channels: RoomItemInterface[];
};

const myInitialState: initialStateProps = {
  channels: [],
};

const reducer = (state = myInitialState, action: any) => {
  switch (action.type) {
    case SET_CHANNELS: {
      return { ...state, channels: action.channels };
    }
    case ADD_CHANNEL:
      return { ...state, channels: state.channels.concat(action.honeycomb) };
    case EDIT_CHANNEL:
      // eslint-disable-next-line no-case-declarations
      const updatedChannels = state.channels.map((item: RoomItemInterface) => {
        if (item.id === action.honeycomb.id) {
          return action.honeycomb;
        }

        return item;
      });
      return { ...state, channels: updatedChannels };
    case REMOVE_CHANNEL:
      return {
        ...state,
        channels: state.channels.filter(
          (item: RoomItemInterface) => item.token !== action.honeycomb.token,
        ),
      };
    default:
      return state;
  }
};

export default reducer;
