import { MARK_AS_READ } from "@/store/actions/index";

type initialStateProps = {
  wizard: { [key: string]: any };
};

const initialState: initialStateProps = {
  wizard: {
    honeycombList: false,
    honeycombChat: false,
    editAudio: false,
  },
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case MARK_AS_READ:
      return {
        ...state,
        wizard: { ...state.wizard, [action.payload]: !state.wizard[action.payload] },
      };

    default:
      return state;
  }
};

export default reducer;
