import { USER_UPDATE, USER_INFO_UPDATE, MEDIA_INFO_UPDATE } from "@/store/actions/index";
import { UserInfoInterface } from "@/interfaces/index";

type initialStateProps = {
  user: UserInfoInterface | null;
};

const initialState: initialStateProps = {
  user: null,
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case USER_UPDATE:
      return { ...state, user: action.payload.user };
    case USER_INFO_UPDATE:
      return { ...state, user: { ...state.user, ...action.payload } };
    case MEDIA_INFO_UPDATE:
      return { ...state, user: { ...state.user, media: { ...action.payload } } };
    default:
      return state;
  }
};

export default reducer;
