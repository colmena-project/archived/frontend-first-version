import {
  SET_IS_ROOM_HOST,
  SET_ROOM_ID,
  SET_IDENTITY,
  SET_SHOW_OVERLAY,
  SET_PARTICIPANTS,
  SET_PARTICIPANTS_NC,
  REMOVE_ALL_PARTICIPANTS,
  REMOVE_PARTICIPANT_BY_ROOM_ID,
} from "@/store/actions/index";
import { ParticipantConference } from "@/types/index";
import { RoomParticipant } from "@/interfaces/talk";

type initialStateProps = {
  identity: string | null;
  roomId: string | null;
  isRoomHost: boolean;
  showOverlay: boolean;
  participants: ParticipantConference[];
  participantsNC: RoomParticipant[];
};

const myInitialState: initialStateProps = {
  identity: null,
  roomId: null,
  isRoomHost: false,
  showOverlay: true,
  participants: [],
  participantsNC: [],
};

const reducer = (state = myInitialState, action: any) => {
  switch (action.type) {
    case SET_IS_ROOM_HOST:
      return { ...state, isRoomHost: action.isRoomHost };
    case SET_ROOM_ID:
      return { ...state, roomId: action.roomId };
    case SET_IDENTITY:
      return { ...state, identity: action.identity };
    case SET_SHOW_OVERLAY:
      return { ...state, showOverlay: action.showOverlay };
    case SET_PARTICIPANTS:
      return { ...state, participants: action.participants };
    case SET_PARTICIPANTS_NC:
      return { ...state, participantsNC: action.participantsNC };
    case REMOVE_ALL_PARTICIPANTS:
      return {
        ...state,
        participants: state.participants.filter((item) => item.roomId !== action.roomId),
      };
    case REMOVE_PARTICIPANT_BY_ROOM_ID: {
      return {
        ...state,
        participants: state.participants.filter(
          (item) =>
            item.roomId !== action.payload.roomId && item.identity !== action.payload.participant,
        ),
      };
    }
    default:
      return state;
  }
};

export default reducer;
