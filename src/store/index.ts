import { useMemo } from "react";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { persistReducer } from "redux-persist";
// import storage from "redux-persist/lib/storage";
import createWebStorage from "redux-persist/lib/storage/createWebStorage";

// eslint-disable-next-line import/no-cycle
import reducers from "@/store/reducers/index";

let store: any;

const createNoopStorage = () => ({
  // eslint-disable-next-line no-unused-vars
  getItem(_key: string) {
    return Promise.resolve(null);
  },
  setItem(_key: string, value: any) {
    return Promise.resolve(value);
  },
  // eslint-disable-next-line no-unused-vars
  removeItem(_key: string) {
    return Promise.resolve();
  },
});

const storage = typeof window !== "undefined" ? createWebStorage("local") : createNoopStorage();

export const persistConfigKey = "rootv2.3";

const persistConfig = {
  key: persistConfigKey,
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

function makeStore(initialState: any = {}) {
  return createStore(persistedReducer, initialState, composeWithDevTools(applyMiddleware()));
}

export const initializeStore = (preloadedState: any) => {
  // eslint-disable-next-line no-underscore-dangle
  let _store = store ?? makeStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = makeStore({
      ...store.getState(),
      ...preloadedState,
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === "undefined") return _store;
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export function useStore(initialState: any) {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
}

// const makeStore = () => {
//   const store = createStore(reducers, composeWithDevTools());
//   return store;
// };
// export const storeWrapper = createWrapper(makeStore, { debug: false });
