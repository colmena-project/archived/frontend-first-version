import db from "@/store/idb/index";
import { removeCornerSlash, removeSpecialCharacters } from "@/utils/utils";

export function createFile(file) {
  return db.files.add(file);
}

export function getLastAudioRecordedByUser(userId) {
  return db.files.where({ userId }).reverse().sortBy("createdAt");
}

export function getAllFiles(userId) {
  return db.files.where({ userId }).sortBy("createdAt");
}

export function remove(id, userId) {
  return db.files.where({ id, userId }).delete();
}

export function removeAllFiles() {
  return db.files.where("id").above(0).delete();
}

export function removeByBasename(basename, userId) {
  return db.files.where({ basename, userId }).delete();
}

export function removeByNextCloudId(nextcloudId, userId) {
  return db.files.where({ nextcloudId, userId }).delete();
}

export function updateFile(id, data) {
  return db.files.update(id, data);
}

export function getFile(id) {
  return db.files.where({ id }).first();
}

export function getFilesByPath(userId, path) {
  return db.files
    .where("[userId+path]")
    .equals([userId, removeCornerSlash(path)])
    .sortBy("createdAt");
}

export function getTreeByPath(userId, path) {
  const regex = new RegExp(`${path}($|/*)`, "i");
  return db.files
    .filter((item) => userId === item.userId && regex.test(item.path))
    .sortBy("createdAt");
}

export function findByFilename(userId, filename) {
  return db.files
    .where("[userId+filename]")
    .equals([userId, removeSpecialCharacters(filename)])
    .first();
}

export function getRecentFiles(userId) {
  return db.files.where("userId").equals(userId).reverse().sortBy("createdAt");
}
