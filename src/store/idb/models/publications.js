import { CurrentPublicationStateEnum } from "@/enums/*";
import db from "@/store/idb/index";

export function createPublication(publication) {
  return db.publications.add(publication);
}

export async function updatePublication(id, data) {
  const publication = await getPublication(id);

  return db.publications.update(id, { ...publication, ...data });
}

export function getPublication(id) {
  if (!id) {
    return null;
  }

  return db.publications.where({ id }).first();
}

export function remove(id) {
  return db.publications.where({ id }).delete();
}

export function getCurrentPublication() {
  return db.publications.where({ status: CurrentPublicationStateEnum.CREATING }).first();
}

export function updateCurrentPublicationFiles(publicationId, publicationFiles) {
  return updatePublication(publicationId, { files: publicationFiles });
}

export async function updatePublicationFileStatus(
  publicationId,
  fileId,
  status,
  statusMessage = "",
) {
  const publication = await getPublication(publicationId);

  const newFilesState = publication.files.map((file) => {
    if (file.localId === fileId) {
      return {
        ...file,
        status,
        statusMessage,
      };
    }

    return file;
  });

  const newPublicationState = {
    ...publication,
    files: newFilesState,
  };

  return updatePublication(publicationId, newPublicationState);
}

export function setPublicationAsSent(publicationId) {
  return updatePublication(publicationId, { status: CurrentPublicationStateEnum.SENT, files: [] });
}

export function getNotSentPublications() {
  return db.publications.where({ status: CurrentPublicationStateEnum.NOT_SENT }).toArray();
}

export async function removeFileFromPublication(publicationId, fileId) {
  const publication = await getPublication(publicationId);

  const newFilesState = publication.files.filter((file) => file.localId !== fileId);

  const newPublicationState = {
    ...publication,
    files: newFilesState,
  };

  return updatePublication(publicationId, newPublicationState);
}

export function setPublicationAsDraft(publicationId) {
  return updatePublication(publicationId, { mode: CurrentPublicationStateEnum.DRAFT });
}

export function setPublicationAsPublic(publicationId) {
  return updatePublication(publicationId, { mode: CurrentPublicationStateEnum.PUBLISH });
}

export function setPublicationErrorMessage(publicationId, errorMessage) {
  return updatePublication(publicationId, { errorMessage });
}

export function setPublicationThumbIsSent(publicationId, sentAt) {
  return updatePublication(publicationId, { thumbsentAt: sentAt });
}
