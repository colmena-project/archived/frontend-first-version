import { MARK_AS_READ } from "@/store/actions/index";
import { WizardFeatures } from "@/interfaces/index";

export const updateWizard = (wizard: WizardFeatures) => ({
  type: MARK_AS_READ,
  payload: wizard,
});
