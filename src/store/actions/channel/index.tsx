import { SET_CHANNELS, ADD_CHANNEL, EDIT_CHANNEL, REMOVE_CHANNEL } from "@/store/actions/index";
import { RoomItemInterface } from "@/interfaces/talk";

export const setChannels = (channels: Array<RoomItemInterface>) => ({
  type: SET_CHANNELS,
  channels,
});

export const addChannel = (channel: RoomItemInterface) => ({
  type: ADD_CHANNEL,
  channel,
});

export const editChannel = (channel: RoomItemInterface) => ({
  type: EDIT_CHANNEL,
  channel,
});

export const removeChannel = (channel: RoomItemInterface) => ({
  type: REMOVE_CHANNEL,
  channel,
});
