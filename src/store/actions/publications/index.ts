import {
  SET_CURRENT_PUBLICATION,
  RESET_CURRENT_PUBLICATION,
  UPDATE_CURRENT_PUBLICATION,
} from "@/store/actions/index";
import { CurrentPublication } from "@/interfaces/index";

export const setCurrentPublication = (currentPublication: CurrentPublication) => ({
  type: SET_CURRENT_PUBLICATION,
  payload: currentPublication,
});

export const resetCurrentPublication = () => ({
  type: RESET_CURRENT_PUBLICATION,
  payload: {},
});

export const updateCurrentPublication = (currentPublication: CurrentPublication) => ({
  type: UPDATE_CURRENT_PUBLICATION,
  payload: currentPublication,
});
