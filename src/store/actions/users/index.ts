import { USER_UPDATE, USER_INFO_UPDATE, MEDIA_INFO_UPDATE } from "@/store/actions/index";
import { UserInfoInterface, MediaInfoInterface } from "@/interfaces/index";

export const userUpdate = (user: any) => ({
  type: USER_UPDATE,
  payload: user,
});

export const userInfoUpdate = (userInfo: UserInfoInterface | any) => ({
  type: USER_INFO_UPDATE,
  payload: userInfo,
});

export const mediaInfoUpdate = (mediaInfo: MediaInfoInterface) => ({
  type: MEDIA_INFO_UPDATE,
  payload: mediaInfo,
});
