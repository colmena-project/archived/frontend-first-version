import {
  SET_IS_ROOM_HOST,
  SET_ROOM_ID,
  SET_IDENTITY,
  SET_SHOW_OVERLAY,
  SET_PARTICIPANTS,
  SET_PARTICIPANTS_NC,
  REMOVE_ALL_PARTICIPANTS,
  REMOVE_PARTICIPANT_BY_ROOM_ID,
} from "@/store/actions/index";
import { ParticipantConference } from "@/types/index";
import { RoomParticipant } from "@/interfaces/talk";

export const setIsRoomHost = (isRoomHost: boolean) => ({
  type: SET_IS_ROOM_HOST,
  isRoomHost,
});

export const setIdentity = (identity: string) => ({
  type: SET_IDENTITY,
  identity,
});

export const setRoomId = (roomId: string) => ({
  type: SET_ROOM_ID,
  roomId,
});

export const setShowOverlay = (showOverlay: boolean) => ({
  type: SET_SHOW_OVERLAY,
  showOverlay,
});

export const setParticipants = (participants: ParticipantConference[]) => ({
  type: SET_PARTICIPANTS,
  participants,
});

export const setParticipantsNC = (participantsNC: RoomParticipant[]) => ({
  type: SET_PARTICIPANTS_NC,
  participantsNC,
});

export const removeAllParticipants = (roomId: string) => ({
  type: REMOVE_ALL_PARTICIPANTS,
  roomId,
});

export const removeParticipantByIDAndRoomId = (participant: string, roomId: string) => ({
  type: REMOVE_PARTICIPANT_BY_ROOM_ID,
  payload: {
    roomId,
    participant,
  },
});
