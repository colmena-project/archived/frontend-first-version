/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { initializeStore } from "@/store/index";
import { setShowOverlay } from "@/store/actions/conference";
import * as wss from "@/utils/wss";
import { RoomParticipant } from "@/interfaces/talk";

const Peer = require("simple-peer");

const defaultConstraints = {
  audio: {
    autoGainControl: false,
    echoCancellation: true,
    channelCount: 1,
  },
  video: false,
};

let localStream: MediaStream;

export const getLocalAudioPreview = async (
  isRoomHost: boolean,
  identity: string,
  roomId: string,
  participantsNC: RoomParticipant[],
) => {
  navigator.mediaDevices
    .getUserMedia(defaultConstraints)
    .then((stream) => {
      // console.log("successfuly received local stream");
      localStream = stream;
      showLocalAudioPreview(localStream, identity, participantsNC);

      // eslint-disable-next-line no-unused-expressions
      isRoomHost ? wss.createNewRoom(roomId, identity) : wss.joinRoom(roomId, identity);
    })
    .catch((err) => {
      console.log("error occurred when trying to get an access to local stream");
      document.dispatchEvent(
        new CustomEvent("error-permission-audio", {
          detail: { error: true },
        }),
      );
    });
};

const peers: any = {};

const getConfiguration = () => ({
  iceServers: [
    {
      urls: "stun:stun.nextcloud.com:443",
    },
    {
      urls: "stun:turn.colmena.network:5349",
      username: "turntest",
      credentials: "turn12345",
    },
  ],
});

export const prepareNewPeerConnection = (connUserSocketId: string, isInitiator: boolean) => {
  const configuration = getConfiguration();

  peers[connUserSocketId] = new Peer({
    initiator: isInitiator,
    config: configuration,
    stream: localStream,
  });

  peers[connUserSocketId].on("signal", (data: any) => {
    // webRTC offer, webRTC Answer (SDP informations), ice candidates

    const signalData = {
      signal: data,
      connUserSocketId,
    };

    wss.signalPeerData(signalData);
  });

  peers[connUserSocketId].on("stream", (stream: MediaStream) => {
    // console.log("new stream came");

    addStream(stream, connUserSocketId);
    // streams = [...streams, stream];
  });
};

const showLocalAudioPreview = (
  stream: MediaStream,
  identity: string,
  participantsNC: RoomParticipant[],
) => {
  const part = participantsNC.find((item: RoomParticipant) => item.actorId === identity);

  const audiosContainer = document.getElementById("audios_conference");
  const audioContainer = document.createElement("div");
  const audioElement = document.createElement("audio");
  audioElement.autoplay = true;
  audioElement.muted = true;
  audioElement.controls = false;
  audioElement.srcObject = stream;
  audioElement.style.display = "none";
  audioElement.id = `${identity}-audio`;

  audioElement.onloadedmetadata = () => {
    audioElement.play();
  };

  audioContainer.appendChild(audioElement);
  audiosContainer?.appendChild(audioContainer);
  document.dispatchEvent(
    new CustomEvent("add-local-participant", {
      detail: {
        userId: identity,
        displayName: part ? part.displayName : "unknow",
        profile: part ? part.participantType : 3,
        muted: true,
      },
    }),
  );

  toggleMic(false);
};

const addStream = (stream: MediaStream, connUserSocketId: string) => {
  // display incoming stream
  const audiosContainer = document.getElementById("audios_conference");
  const audioContainer = document.createElement("div");
  audioContainer.id = connUserSocketId;

  const audioElement = document.createElement("audio");
  audioElement.autoplay = true;
  audioElement.muted = false;
  audioElement.controls = false;
  audioElement.srcObject = stream;
  audioElement.id = `${connUserSocketId}-audio`;
  audioElement.style.display = "none";

  audioElement.onloadedmetadata = () => {
    audioElement.play();
  };

  audioContainer.appendChild(audioElement);
  audiosContainer?.appendChild(audioContainer);

  // document.dispatchEvent(
  //   new CustomEvent("add-remote-participant", {
  //     detail: {
  //       muted: false,
  //       socketId: connUserSocketId,
  //     },
  //   }),
  // );
};

export const handleSignalingData = (data: { signal: string; connUserSocketId: string }) => {
  // add signaling data to peer connection
  peers[data.connUserSocketId].signal(data.signal);
};

export const removePeerConnection = (data: { socketId: string }) => {
  const { socketId } = data;
  const audioContainer = document.getElementById(`container-${socketId}`);
  const audioEl = document.getElementById(`${socketId}-audio`) as HTMLAudioElement;

  if (audioContainer && audioEl) {
    const { srcObject } = audioEl;
    if (srcObject) {
      if ("getTracks" in srcObject) {
        const tracks = srcObject.getTracks();

        tracks.forEach((t: any) => t.stop());

        audioEl.srcObject = null;
        audioContainer.removeChild(audioEl);

        audioContainer?.parentNode?.removeChild(audioContainer);

        if (peers[socketId]) {
          peers[socketId].destroy();
        }
        delete peers[socketId];
      }
    }
  }
};

/// /////////////////////////////// Buttons logic //////////////////////////////////

export function toggleMic(isMuted: boolean) {
  localStream.getAudioTracks()[0].enabled = isMuted;
}
