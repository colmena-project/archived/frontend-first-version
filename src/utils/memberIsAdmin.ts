import { RoleUserEnum } from "../enums";
import { MemberInterface } from "../interfaces";

export const memberIsAdmin = (id: string, administrators: []) => {
  const exist = administrators.find((row: MemberInterface) => row.id === id);
  if (exist) return RoleUserEnum.ADMIN;
  return RoleUserEnum.COLLABORATOR;
};
