import io from "socket.io-client";
import { ParticipantConference } from "@/types/index";
import getConfig from "next/config";
import * as webRTCHandler from "@/utils/webRTCHandler";

const { publicRuntimeConfig } = getConfig();

const WS_SERVER = publicRuntimeConfig.wss.baseUrl;
// const WS_SERVER = "https://colmena.network:5002";

let socket: any = null;

export const connectWithSocketIOServer = () => {
  socket = io(WS_SERVER, {
    reconnectionDelay: 1000,
    reconnection: true,
    reconnectionAttempts: 10,
    // transports: ["polling", "websocket"],
    agent: false,
    upgrade: false,
    rejectUnauthorized: false,
  });

  socket.on("connect", () => {
    // console.log("succesfully wss connection", socket.id);
  });
  socket.on("room-created", (data: { roomId: string }) => {
    const { roomId } = data;
    document.dispatchEvent(
      new CustomEvent("room-create-rdx", {
        detail: { roomId },
      }),
    );
  });
  socket.on("room-update", (data: { connectedUsers: ParticipantConference[] }) => {
    const { connectedUsers } = data;
    document.dispatchEvent(
      new CustomEvent("room-update-rdx", {
        detail: { connectedUsers },
      }),
    );
  });
  socket.on("room-removed", (data: { roomId: string }) => {
    const { roomId } = data;
    document.dispatchEvent(
      new CustomEvent("room-removed-rdx", {
        detail: { roomId },
      }),
    );
  });
  socket.on("conn-prepare", (data: { connUserSocketId: string }) => {
    const { connUserSocketId } = data;

    webRTCHandler.prepareNewPeerConnection(connUserSocketId, false);

    // inform the user which just join the room that we have prepared for incoming connection
    socket.emit("conn-init", { connUserSocketId });
  });
  socket.on("conn-signal", (data: { signal: string; connUserSocketId: string }) => {
    webRTCHandler.handleSignalingData(data);
  });

  socket.on("conn-init", (data: { connUserSocketId: string }) => {
    const { connUserSocketId } = data;
    webRTCHandler.prepareNewPeerConnection(connUserSocketId, true);
  });
  socket.on("user-disconnected", (data: { socketId: string }) => {
    webRTCHandler.removePeerConnection(data);
  });
};

export const createNewRoom = (roomId: string | null, identity: string | null) => {
  const data = {
    roomId,
    identity,
  };

  socket.emit("create-new-room", data);
};

export const joinRoom = (roomId: string | null, identity: string | null) => {
  const data = {
    roomId,
    identity,
  };

  socket.emit("join-room", data);
};

export const leaveRoom = (roomId: string | null, identity: string | null) => {
  const data = {
    roomId,
    identity,
  };

  socket.emit("leave-room", data);
};

export const signalPeerData = (data: { signal: string; connUserSocketId: string }) => {
  socket.emit("conn-signal", data);
};
