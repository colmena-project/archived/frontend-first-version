import { ChatMessageInterface, RoomItemInterface } from "@/interfaces/talk";
import { TFunction } from "next-i18next";
import { RoomFilterEnum, RoomOrderEnum, RoomTypeEnum } from "../enums";
import { PropsUserSelector } from "../types";

export function getRoomSubtitle(userId: string, room: RoomItemInterface, translate: TFunction) {
  const {
    description,
    lastMessage: { actorId, actorDisplayName, message },
  } = room;
  const authorName = userId === actorId ? translate("youTitle") : actorDisplayName;
  if (message) {
    return `${authorName}: ${convertMaskMessageToFinalMessage(room.lastMessage)}`;
  }

  return description || "";
}

export function convertMaskMessageToFinalMessage(chatMessage: ChatMessageInterface) {
  const { message, messageParameters } = chatMessage;
  let msg = message;

  if (msg.indexOf("{actor}") !== -1) {
    msg = msg.replace("{actor}", messageParameters.actor.name);
  }

  if (msg.indexOf("{user}") !== -1) {
    const m = (messageParameters && messageParameters.user && messageParameters.user.name) || "";
    msg = msg.replace("{user}", m);
  }

  if (msg.indexOf("{file}") !== -1) {
    msg = msg.replace(
      "{file}",
      (messageParameters && messageParameters.file && messageParameters.file.name) || "",
    );
  }

  return msg;
}

export function orderItems(
  user: PropsUserSelector,
  roomOrder: RoomOrderEnum,
  rooms: RoomItemInterface[],
) {
  switch (roomOrder) {
    case RoomOrderEnum.ASC_ALPHABETICAL:
      rooms.sort((a, b) => (a.displayName.toLowerCase() > b.displayName.toLowerCase() ? 1 : -1));
      break;
    case RoomOrderEnum.DESC_ALPHABETICAL:
      rooms.sort((a, b) => (a.displayName.toLowerCase() < b.displayName.toLowerCase() ? 1 : -1));
      break;
    case RoomOrderEnum.NEWESTS:
      rooms.sort((a, b) => (a.lastActivity < b.lastActivity ? 1 : -1));
      break;
    case RoomOrderEnum.OLDEST:
      rooms.sort((a, b) => (a.lastActivity > b.lastActivity ? 1 : -1));
      break;
    case RoomOrderEnum.HIGHLIGHT:
    default:
      rooms
        .sort((a: RoomItemInterface, b: RoomItemInterface) => b.lastActivity - a.lastActivity)
        .sort((a: RoomItemInterface, b: RoomItemInterface) => {
          const isFavoriteA = a.isFavorite ? 1 : 0;
          const isFavoriteB = b.isFavorite ? 1 : 0;
          return isFavoriteB - isFavoriteA;
        })
        .sort((a: RoomItemInterface) => {
          if (user.user.media && user.user.media.name === a.displayName) {
            return -1;
          }
          return 1;
        });
  }

  return rooms;
}

export function filterItems(roomFilter: RoomFilterEnum, honeycombs: RoomItemInterface[]) {
  let items = honeycombs;
  switch (roomFilter) {
    case RoomFilterEnum.FAVORITES:
      items = items.filter((item) => item.isFavorite);
      break;
    case RoomFilterEnum.ONLY_CHANNELS:
      items = items.filter((item) => item.listable > 0);
      break;
    case RoomFilterEnum.ONLY_GROUPS:
      items = items.filter((item) => item.type === RoomTypeEnum.GROUP && item.listable <= 0);
      break;
    case RoomFilterEnum.ONLY_CONVERSATIONS:
      items = items.filter((item) => item.type === RoomTypeEnum.CONVERSATION);
      break;
    case RoomFilterEnum.UNREADED:
      items = items.filter((item) => item.unreadMessages > 0);
      break;
    case RoomFilterEnum.ALL:
    default:
      break;
  }

  return items;
}
