import { parseCookies } from "nookies";

const cookies = parseCookies();
const language = cookies.NEXT_LOCALE || "en";

export const currentDirection = () => (["ar", "he"].includes(language) ? "rtl" : "ltr");
