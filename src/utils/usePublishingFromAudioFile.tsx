import { v4 as uuid } from "uuid";
import {
  createPublication,
  getCurrentPublication,
  updatePublication,
} from "@/store/idb/models/publications";
import { findByFilename } from "@/store/idb/models/files";
import { PropsUserSelector, TagsConference } from "../types";
import { parseCookies } from "nookies";
import { TimeDescriptionInterface } from "../interfaces";
import { useTranslation } from "next-i18next";
import { getDataFile, listFile } from "@/services/webdav/files";
import { getFileTags } from "@/services/webdav/tags";
import { useSelector } from "react-redux";
import { CurrentPublicationStateEnum } from "../enums";

function usePublishingFromAudioFile() {
  const userRdx = useSelector((state: { user: PropsUserSelector }) => state.user);
  const userId = userRdx.user.id;

  const cookies = parseCookies();
  const language = cookies.NEXT_LOCALE || "en";
  const { t: c } = useTranslation("common");

  const timeDescription: TimeDescriptionInterface = c("timeDescription", { returnObjects: true });

  const initPublishing = async (filename: string) => {
    const currentPublicationData = await getCurrentPublication();
    const currentPublicationId = currentPublicationData?.id ?? null;

    const localFile = await findByFilename(userId, String(filename));
    let mergeFile;

    if (localFile) {
      mergeFile = { ...localFile, ownerId: localFile.userId, filename: localFile.title };
    }

    try {
      const resultGetDataFile: any = await getDataFile(userId, filename, timeDescription);
      const resultListFile = await listFile(userId, filename);
      const ncTags = await getFileTags(userId, filename, resultGetDataFile.fileId);

      const mergeRemoteData = {
        ...resultGetDataFile,
        tags: handleTags(ncTags),
        description: resultGetDataFile.description ?? "",
        arrayBufferBlob: resultListFile,
      };

      mergeFile = { ...mergeFile, ...mergeRemoteData };
    } catch (e) {
      console.log(e);
    }

    await handlePublication(
      currentPublicationId,
      assemblePublication(mergeFile, filename, userId, currentPublicationData),
    );
  };

  const handlePublication = async (currentPublicationId: any, currentPublicationData: any) => {
    if (currentPublicationId) {
      await updatePublication(currentPublicationId, {
        ...currentPublicationData,
      });

      return;
    }

    await createPublication({
      ...currentPublicationData,
    });
  };

  const handleTags = (ncTags: any) => {
    const tags = ncTags.map((tag: TagsConference) => tag["display-name"]);

    return tags.join(", ");
  };

  const assemblePublication = (
    file: any,
    filename: string,
    userId: string,
    currentPublicationData: any,
  ) => {
    const media = {
      title: file.title,
      description: file.description ?? "",
      language,
      creator: file.ownerId ?? "",
      status: "PENDING",
      extentSize: file.size,
      isFromLibrary: true,
      media: new File([file.arrayBufferBlob], String(filename), { type: "audio" }),
      format: file.filename.split(".").pop(),
      localId: uuid(),
    };

    if (currentPublicationData) {
      return {
        ...currentPublicationData,
        tags: file.tags ?? "",
        title: file.title ?? "",
        files: [media],
        description: file.description ?? "",
        content: "",
        publisher: userRdx.user.media?.name,
        userId,
        status: CurrentPublicationStateEnum.CREATING,
      };
    }

    return {
      files: [media],
      title: file.title ?? "",
      description: file.description ?? "",
      content: "",
      category: "",
      rights: "",
      date: "",
      publisher: userRdx.user.media?.name,
      userId: userRdx.user.id,
      status: CurrentPublicationStateEnum.CREATING,
      thumbnail: undefined,
      createdAt: new Date().toISOString(),
      categoryName: "",
      mode: CurrentPublicationStateEnum.DRAFT,
      cmsMode: undefined,
    };
  };

  return {
    initPublishing,
  };
}

export default usePublishingFromAudioFile;
