import $ from "jquery";
import { clockFormatWaveformPlaylist } from "./utils";

export function removeElement(id: string) {
  document.getElementById(id)?.remove();
}

export function createCursorElementSelectPosition(id: string, left: string) {
  removeElement(id);
  const div = document.createElement("DIV");
  div.style.position = "absolute";
  div.style.width = "1px";
  div.style.bottom = "0";
  div.style.top = "0";
  div.style.left = left;
  div.style.zIndex = "4";
  // div.style.backgroundColor = "yellow";
  div.classList.add("selection");
  div.classList.add("point");
  div.setAttribute("id", id);
  document.getElementsByClassName("waveform")[0].appendChild(div);
}

export function getTextInputValue(id: string): number | string {
  const elem = document.getElementById(id) as HTMLInputElement;

  if (elem === null) return 0;

  const val = String(elem.value);

  if (Number.isNaN(Number(val))) return String(val);

  return Number(val);
}

export function setTextInputValue(id: string, val: number | string) {
  $(`#${id}`).val(String(val));
  $(`#playback-position-formatted`).text(String(clockFormatWaveformPlaylist(Number(val), 0)));
}

export const SAVE_AUDIO_FLAG = "save-audio-flag";
export const PLAYBACK_POSITION = "playback-position";
