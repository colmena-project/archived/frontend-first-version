/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable no-bitwise */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable import/no-duplicates */
import { enUS } from "date-fns/locale";
import Resizer from "react-image-file-resizer";
import {
  differenceInCalendarMonths,
  differenceInMinutes,
  format,
  formatDistanceToNow,
  Locale,
} from "date-fns";
import {
  BreadcrumbItemInterface,
  LibraryItemInterface,
  TimeDescriptionInterface,
  UserInvitationInterface,
} from "@/interfaces/index";
import constants from "@/constants/index";
import {
  AllIconProps,
  CallTypesProps,
  ImageSizes,
  LanguageProps,
  LanguagesAvailableProps,
  SocialMediasAvailableKeys,
} from "@/types/*";
import { arrayBufferToBlob, blobToArrayBuffer, createObjectURL } from "blob-util";
import { AudioExportTypesEnum, DirectoryNamesNCEnum } from "@/enums/*";
import {
  convertUsernameToPrivate,
  convertPrivateToUsername,
  getPrivatePath,
} from "@/utils/directory";
import {
  createFile,
  findByFilename,
  updateFile as updateFileLocal,
} from "@/store/idb/models/files";
import { Theme } from "@material-ui/core";
import axios from "axios";
import { MediaInterface } from "@/interfaces/cms";
import getConfig from "next/config";
import getBlobDuration from "get-blob-duration";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import shorthash from "short-hash";
import { v4 as uuid } from "uuid";
import { TFunction } from "next-i18next";

const { publicRuntimeConfig } = getConfig();

const byPassObject = new Map();
const dateFnsLocales = constants.LOCALES_DATE_FNS;
Object.keys(dateFnsLocales).forEach((item: LanguagesAvailableProps) => {
  byPassObject.set(item, dateFnsLocales[item]);
});

export function getUsagePercent(quota: number, usage: number) {
  const usagePercent = (usage / quota) * 100;

  return usagePercent.toFixed(2);
}

export function formatBytes(bytes: number, decimals = 2) {
  if (bytes === 0) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / k ** i).toFixed(dm))} ${sizes[i]}`;
}

export function getLocale(locale: string): Locale {
  return byPassObject.get(locale) || enUS;
}

export function removeSpecialCharacters(str: string) {
  const withoutAccent = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  return withoutAccent.replace(/[#,+()$~%'"*!?<>{}]/g, "");
}

export function capitalizeFirstLetter(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function encodeURLAxios(path: string) {
  const contextEncodedArray = path.split("/");
  let filename = contextEncodedArray.pop();
  filename = encodeURIComponent(String(filename));
  contextEncodedArray.push(filename);
  return removeCornerSlash(contextEncodedArray.join("/"));
}

export const isAudioFile = (mime: string | undefined) => {
  if (!mime) return false;

  return /^audio/.test(mime);
};

export const isImageFile = (mime: string | undefined) => {
  if (!mime) return false;

  return /^image/.test(mime);
};

export const isPNGImage = (mime: string | undefined) => {
  if (!mime) return false;

  return mime === "image/png";
};

export const isJPGImage = (mime: string | undefined) => {
  if (!mime) return false;

  return mime === "image/jpeg" || mime === "image/jpg";
};

export const getLanguageDateFormat = (language: string) => {
  switch (language) {
    case "pt":
    case "pt_BR":
    case "fr":
    case "ar":
    case "es":
      return "dd/MM/yyyy";
    case "en":
    case "sw":
    case "ru":
    case "uk":
    default:
      return "yyyy-MM-dd";
  }
};

export const formatDateToLanguage = (date: Date, language: string) =>
  format(date, getLanguageDateFormat(language));

export const isValidUrl = (url: string) => {
  const pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i",
  ); // fragment locator
  return !!pattern.test(url);
};

export const validateURLMediaSocialGroup = (url: string) => {
  let urlSend = url;
  if (urlSend.indexOf("http://") === -1 && urlSend.indexOf("https://") === -1)
    urlSend = `http://${urlSend}`;
  return isValidUrl(urlSend);
};

export function parseJwt<Type>(token: string | undefined): Type | undefined {
  if (!token) return undefined;

  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map((c: string) => `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`)
      .join(""),
  );

  return JSON.parse(jsonPayload);
}

export function isJWTValidInvitation(tkn: string | string[] | undefined) {
  const token: string | undefined = Array.isArray(tkn) ? tkn[0] : tkn;
  const tokenObj = parseJwt<UserInvitationInterface | undefined>(token);

  const result: {
    valid: boolean;
    token: string | undefined;
  } = {
    valid: false,
    token: undefined,
  };

  if (!token) return result;

  const arrToken = token?.split(".");
  if (arrToken?.length !== 3) {
    return result;
  }

  if (tokenObj) {
    if (!tokenObj?.media || !tokenObj?.username) return result;
  } else return result;

  result.token = token;
  result.valid = true;
  return result;
}

export function getFirstLettersOfTwoFirstNames(word: string | undefined): string {
  if (!word) return "";

  return word
    .replace(/^(.).+?\s(.).+$|^(..).+$/gi, "$1$2$3")
    .toString()
    .toUpperCase();
}

export function getFirstname(word: string | undefined): string {
  if (!word) return "";

  const arr = String(word).split(" ");

  if (arr.length > 1) return capitalizeFirstLetter(arr[0]);

  return word;
}

export const empty = (value: any) => value === null || value === "" || value === undefined;

export function getOnlyFilename(filename: string) {
  if (filename.indexOf(".") < 0) {
    return filename;
  }

  return filename.replace(/(.*)\..+?$/, "$1");
}

export function getExtensionFilename(filename: string) {
  if (!filename) {
    return undefined;
  }

  if (filename.indexOf(".") < 0) {
    return undefined;
  }

  return filename.substring(filename.lastIndexOf(".") + 1, filename.length);
}

export function getExtensionByMimetype(mimetype: string) {
  if (mimetype.indexOf("/") < 0) {
    return undefined;
  }

  return mimetype.substring(mimetype.lastIndexOf("/") + 1, mimetype.length);
}

export function generateBreadcrumb(
  path: string | Array<string> | string[] | undefined,
  initialPath?: string,
): Array<BreadcrumbItemInterface> {
  const directories: BreadcrumbItemInterface[] = [];
  let breadcrumb = initialPath ?? "/";

  if (empty(path)) {
    return directories;
  }

  if (typeof path === "string") {
    breadcrumb += `/${initialPath}/${path}`;
    const directory: BreadcrumbItemInterface = {
      description: path,
      path: breadcrumb,
      isCurrent: true,
    };

    directories.push(directory);

    return directories;
  }

  if (path) {
    path.forEach((dir: string, index) => {
      if (breadcrumb === "/") {
        breadcrumb = `/${dir}`;
      } else {
        breadcrumb += `/${dir}`;
      }

      const directory: BreadcrumbItemInterface = {
        description: dir,
        path: breadcrumb,
        isCurrent: index === path?.length - 1,
      };

      directories.push(directory);
    });
  }

  return directories;
}

export function moveScrollToRight(element: any) {
  if (element !== null) {
    const width = element.current.scrollWidth;
    element.current.scrollTo(width, 0);
  }
}

export function dateDescription(date: Date | undefined, timeDescription: TimeDescriptionInterface) {
  if (date === undefined) {
    return "";
  }

  const singularYearDescription = timeDescription.singularYear;
  const pluralYearDescription = timeDescription.pluralYear;
  const singularMonthDescription = timeDescription.singularMonth;
  const pluralMonthDescription = timeDescription.pluralMonth;
  const singularDayDescription = timeDescription.singularDay;
  const pluralDayDescription = timeDescription.pluralDay;
  const singularHourDescription = timeDescription.singularHour;
  const pluralHourDescription = timeDescription.pluralHour;
  const singularMinuteDescription = timeDescription.singularMinute;
  const pluralMinuteDescription = timeDescription.pluralMinute;

  const today = new Date();
  const months = differenceInCalendarMonths(today, date);
  if (months > 0 && months <= 12) {
    const monthDescription = months > 1 ? pluralMonthDescription : singularMonthDescription;

    return `${months} ${monthDescription}`;
  }

  const minutes = differenceInMinutes(today, date);
  const hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);
  const years = Math.floor(days / 365);

  if (years > 0) {
    const yearDescription = years > 1 ? pluralYearDescription : singularYearDescription;

    return `${years} ${yearDescription}`;
  }

  if (days > 0) {
    const dayDescription = days > 1 ? pluralDayDescription : singularDayDescription;

    return `${days} ${dayDescription}`;
  }

  if (hours > 0) {
    const hourDescription = hours > 1 ? pluralHourDescription : singularHourDescription;

    return `${hours} ${hourDescription}`;
  }

  if (minutes > 1) {
    const minuteDescription = minutes > 1 ? pluralMinuteDescription : singularMinuteDescription;

    return `${minutes} ${minuteDescription}`;
  }

  return timeDescription.now;
}

export function trailingSlash(path: string) {
  return `${removeLastSlash(path)}/`;
}

export function removeFirstSlash(path: string | null | undefined): string {
  if (!path) {
    return "";
  }

  return path.replace(/^[/]*/, "");
}

export function removeLastSlash(path: string | null | undefined): string {
  if (!path) {
    return "";
  }

  return path.replace(/[/]*$/, "");
}

export function removeCornerSlash(path: string | null | undefined): string {
  if (!path) {
    return "";
  }

  return removeFirstSlash(removeLastSlash(path));
}

export function awaiting(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function getRandomInt(min: number, max: number) {
  const intMin = Math.ceil(min);
  const intMax = Math.floor(max);

  return Math.floor(Math.random() * (intMax - intMin)) + min;
}

export function getFormattedDistanceDateFromNow(timestamp: number, locale = "en") {
  if (!timestamp) return "";

  return formatDistanceToNow(new Date(timestamp * 1000), {
    locale: byPassObject.get(locale) || enUS,
  });
}

export function getDefaultAudioName() {
  const tempFileName = new Date().toISOString();
  return tempFileName.substr(0, tempFileName.length - 5);
}

export function formatCookies(cookies: string[], existentCookies = "") {
  if (!cookies || cookies.length === 0) return "";

  let cookiesKeys: string[] = [];

  if (existentCookies) {
    cookiesKeys = existentCookies.split(";").map((item: string) => item.split("=")[0]);
  }

  cookiesKeys = [
    ...cookiesKeys,
    ...cookies.map((item: string) => item.split(";")[0]).map((item: string) => item.split("=")[0]),
  ];

  let cookiesPre: string[] = [];
  if (existentCookies) {
    cookiesPre = existentCookies.split(";");
  }

  cookiesPre = [...cookiesPre, ...cookies.map((item: string) => item.split(";")[0])];

  cookiesPre = cookiesPre.reverse();

  const cookiesMap = new Map();
  cookiesKeys.forEach((element: string) => {
    const value = cookiesPre.find((item: string) => item.indexOf(element) !== -1)?.split("=")[1];
    cookiesMap.set(element, value);
  });

  const cookiesR: string[] = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const [key, value] of cookiesMap) {
    cookiesR.push(`${key}=${value}`);
  }

  return cookiesR.join(";");
}

export function prepareBlobUrl(blob: Blob | ArrayBuffer | File) {
  let blobUrl: string;
  if (blob instanceof ArrayBuffer) {
    const blobFile = arrayBufferToBlob(blob);
    blobUrl = URL.createObjectURL(blobFile);
  } else blobUrl = URL.createObjectURL(blob);

  return blobUrl;
}

export function downloadFile(
  data: Blob | File | ArrayBuffer | null | undefined,
  name = "file",
  type = "text/plain",
) {
  if (!data) return false;

  const {
    URL: { revokeObjectURL },
    setTimeout,
  } = window;

  const url = prepareBlobUrl(data);

  const anchor = document.createElement("a");
  anchor.setAttribute("href", url);
  anchor.setAttribute("download", name);
  anchor.click();

  setTimeout(() => {
    revokeObjectURL(url);
  }, 100);

  return true;
}

export function fancyTimeFormat(duration: number) {
  // Hours, minutes and seconds
  const hrs = Math.floor(duration / 3600);
  const mins = Math.floor((duration % 3600) / 60);
  const secs = Math.floor(duration % 60);
  // Output like "1:01" or "4:03:59" or "123:03:59"
  let ret = "";

  if (hrs > 0) {
    ret += `${hrs}:${mins < 10 ? "0" : ""}`;
  }

  ret += `${mins}:${secs < 10 ? "0" : ""}`;
  ret += `${secs}`;
  return ret;
}

export function uniqueId() {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
}

export async function findGroupFolderByPath(path: string): Promise<boolean> {
  const arr = path.split("/");
  const honeycombName =
    arr[0].toString().toLocaleLowerCase() === DirectoryNamesNCEnum.TALK.toLocaleLowerCase()
      ? arr[1]
      : arr[0];
  const groupFolders = await axios.get("/api/list-group-folder");
  return groupFolders.data.data
    .map((item: string) => item.toLowerCase().trim())
    .includes(honeycombName.toLowerCase().trim());
}

export async function getHoneycombPath(
  conversationName: string,
  canDeleteConversation: boolean,
  talkFolderName: string,
): Promise<string> {
  let honeycombPath = conversationName;
  if (!canDeleteConversation) {
    const isGroupFolder = await findGroupFolderByPath(conversationName);
    if (!isGroupFolder) {
      honeycombPath = `${talkFolderName}/${conversationName}`;
    }
  }

  return honeycombPath;
}

export function setBackAfterFinishRecording(flag: string) {
  localStorage.setItem("backAfterFinishRecording", flag);
}

export function getBackAfterFinishRecording() {
  return localStorage.getItem("backAfterFinishRecording");
}

export function treatTagName(value: string) {
  return value.toLowerCase();
}

export function getSystemLanguages(t: any): LanguageProps[] {
  const locales = Object.values(constants.LOCALES);
  return locales
    .map((item: LanguagesAvailableProps) => ({
      abbr: item,
      language: t(`languagesAllowed.${item}`),
    }))
    .sort((a, b) => {
      if (a.language > b.language) return 1;

      if (a.language < b.language) return -1;

      return 0;
    });
}

export function getLanguageTranslate(t: TFunction, language: string): LanguageProps | undefined {
  return getSystemLanguages(t).find((row) => row.abbr.includes(language));
}

export function fileSizeConvert(bytes: number) {
  const sizes = [
    {
      unit: "TB",
      // eslint-disable-next-line no-restricted-properties
      value: Math.pow(1024, 4),
    },
    {
      unit: "GB",
      // eslint-disable-next-line no-restricted-properties
      value: Math.pow(1024, 3),
    },
    {
      unit: "MB",
      // eslint-disable-next-line no-restricted-properties
      value: Math.pow(1024, 2),
    },
    {
      unit: "KB",
      // eslint-disable-next-line no-restricted-properties
      value: 1024,
    },
    {
      unit: "B",
      // eslint-disable-next-line no-restricted-properties
      value: 1,
    },
  ];

  type Props = {
    size?: number | null;
    unit?: string | null;
    description?: string | null;
  };
  const result: Props = {
    size: 0,
    unit: null,
    description: "0 B",
  };

  const size = sizes.find((item) => bytes >= item.value);
  if (size) {
    result.size = bytes / size.value;
    result.unit = size.unit;
    result.description = `${Math.round(result.size)} ${size.unit}`;
  }

  return result;
}

export async function getAccessedPages(): Promise<string[]> {
  const pages = await localStorage.getItem("accessedPages");

  if (!pages) return [];

  return JSON.parse(pages);
}

export async function setAccessedPages(pages: string[]) {
  await localStorage.setItem("accessedPages", JSON.stringify(pages.slice(0, 2)));
}

export async function cleanAccessedPages() {
  await localStorage.setItem("accessedPages", "");
}

export async function getMicrophonePermission(): Promise<string> {
  const permission = await localStorage.getItem("micPermission");

  if (!permission) return "yes";

  return permission;
}

export async function setMicrophonePermission(permission: "yes" | "no") {
  await localStorage.setItem("micPermission", permission);
}

export async function setIDBEnable(enable: "yes" | "no") {
  await localStorage.setItem("idbEnable", enable);
}

export async function getIDBEnable(): Promise<string> {
  const res = await localStorage.getItem("idbEnable");

  if (!res) return "no";

  return res;
}

export async function setPathRecorder(path: string) {
  document.dispatchEvent(
    new CustomEvent("update-path-recorder", {
      detail: { path },
    }),
  );
  await localStorage.setItem("pathRecorder", path);
}

export async function getPathRecorder(): Promise<string> {
  const res = await localStorage.getItem("pathRecorder");

  if (!res) return "";

  return res;
}

export async function setFilenameRecorder(filename: string) {
  document.dispatchEvent(
    new CustomEvent("update-filename-recorder", {
      detail: { filename },
    }),
  );
  await localStorage.setItem("filenameRecorder", filename);
}

export async function getFilenameRecorder(): Promise<string> {
  const res = await localStorage.getItem("filenameRecorder");

  if (!res) return "";

  return res;
}

export function verifyIndexedDBBrowserEnable() {
  const db = indexedDB.open("colmenaDatabase");
  db.onerror = async () => {
    await setIDBEnable("no");
  };
  db.onsuccess = async () => {
    await setIDBEnable("yes");
  };
}

function checkEncodeURI(str: string) {
  return /%/i.test(str);
}

export function decodeURI(encodedURI: string | null | undefined) {
  if (!encodedURI || !checkEncodeURI(encodedURI)) return encodedURI;

  return decodeURIComponent(encodedURI);
}

export function clockFormatWaveformPlaylist(seconds: number, decimals = 3) {
  const hours: number = parseInt(String(seconds / 3600), 10) % 24;
  const minutes: number = parseInt(String(seconds / 60), 10) % 60;
  const secs = Number((seconds % 60).toFixed(decimals));

  return `${hours < 10 ? `0${hours}` : hours}:${minutes < 10 ? `0${minutes}` : minutes}:${
    secs < 10 ? `0${secs}` : secs
  }`;
}

export function createBlobFromAudioBuffer(abuffer: AudioBuffer, totalSamples: number): Blob {
  // Generate audio file and assign URL
  return bufferToWave(abuffer, totalSamples);
}

// Convert AudioBuffer to a Blob using WAVE representation
function bufferToWave(abuffer: AudioBuffer, len: number) {
  const numOfChan = abuffer.numberOfChannels;
  const length = len * numOfChan * 2 + 44;
  const buffer = new ArrayBuffer(length);
  const view = new DataView(buffer);
  const channels: Float32Array[] = [];
  let i;
  let sample;
  let offset = 0;
  let pos = 0;

  // write WAVE header
  setUint32(0x46464952); // "RIFF"
  setUint32(length - 8); // file length - 8
  setUint32(0x45564157); // "WAVE"

  setUint32(0x20746d66); // "fmt " chunk
  setUint32(16); // length = 16
  setUint16(1); // PCM (uncompressed)
  setUint16(numOfChan);
  setUint32(abuffer.sampleRate);
  setUint32(abuffer.sampleRate * 2 * numOfChan); // avg. bytes/sec
  setUint16(numOfChan * 2); // block-align
  setUint16(16); // 16-bit (hardcoded in this demo)

  setUint32(0x61746164); // "data" - chunk
  setUint32(length - pos - 4); // chunk length

  // write interleaved data
  for (i = 0; i < abuffer.numberOfChannels; i++) channels.push(abuffer.getChannelData(i));

  while (pos < length) {
    for (i = 0; i < numOfChan; i++) {
      // interleave channels
      sample = Math.max(-1, Math.min(1, channels[i][offset])); // clamp
      // eslint-disable-next-line max-len
      sample = (0.5 + sample < 0 ? sample * 32768 : sample * 32767) | 0; // scale to 16-bit signed int
      view.setInt16(pos, sample, true); // write 16-bit sample
      pos += 2;
    }
    offset++; // next source sample
  }

  // create Blob
  return new Blob([buffer], { type: "audio/wav" });

  function setUint16(data: any) {
    view.setUint16(pos, data, true);
    pos += 2;
  }

  function setUint32(data: any) {
    view.setUint32(pos, data, true);
    pos += 4;
  }
}

export function getAvailableSocialMedias(): AllIconProps[] {
  return ["facebook", "twitter", "instagram", "mastodon", "whatsapp", "telegram"];
}

export function getAvailableSocialMediasKeysObject(): string[] {
  return [
    "social_media_whatsapp",
    "social_media_twitter",
    "social_media_facebook",
    "social_media_mastodon",
    "social_media_instagram",
    "social_media_telegram",
  ];
}

export function getMediaGroupIcon(socialMedia: SocialMediasAvailableKeys) {
  const result = getAvailableSocialMedias().find(
    (item) => socialMedia.replace("social_media_", "") === item,
  );
  if (!result) return "facebook";
  return result;
}

export function getRandomChunkFileName() {
  let tempFilename = "file";
  // eslint-disable-next-line no-plusplus
  for (let count = 1; count < 4; count++) {
    tempFilename += `-${getRandomInt(1000, 9999)}`;
  }
  return tempFilename;
}

export function cleanAllLibraryItems() {
  localStorage.setItem("allLibraryItems", "");
}

export function setAllLibraryItems(items: LibraryItemInterface[]) {
  localStorage.setItem("allLibraryItems", JSON.stringify(items));
}

export function getAllLibraryItems() {
  const items = localStorage.getItem("allLibraryItems");
  if (!items) {
    return [];
  }

  return JSON.parse(items);
}

export function prepareLanguageToNextcloud(language: string) {
  switch (language) {
    case "sw":
      return "en";
    default:
      return language;
  }
}

export function getBaseUrl() {
  return document.location.href.replace(/^(.+?\/\/[^/]+).+/, "$1");
}

export function getPathUrl() {
  return document.location.href.replace(/^.+?\/\/[^/]+(.+)/, "$1");
}

export function removeAllChildNodes(parent: HTMLElement) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}

// element: document.getElementsByClassName("ignore-blur")
export function clickElement(element: any, callback: () => void) {
  if (!element) {
    return;
  }

  const totalElements = element.length;
  if (totalElements > 0) {
    // eslint-disable-next-line no-plusplus
    for (let index = 0; index < totalElements; index++) {
      const e = element[index];
      e.addEventListener("click", callback);

      if (e.children.length > 0) {
        clickElement(e.children, callback);
      }
    }
  }
}

export function shortStringByNumber(str: string, num: number) {
  if (str.length > num) return `${str.substring(0, num)}...`;

  return str;
}

export async function createAudioRecorderIDB(blob: Blob, userId: string) {
  const ext = AudioExportTypesEnum.WAV;
  const name = await getFilenameRecorder();
  const path = await getPathRecorder();
  const filename = `${convertUsernameToPrivate(path, userId)}/${removeSpecialCharacters(
    name,
  )}.${ext}`;
  const arrayBuffer = await blobToArrayBuffer(blob);

  if (filename) {
    try {
      const localFile = await findByFilename(userId, filename);
      if (!localFile) {
        await createFile({
          title: name,
          basename: name,
          filename,
          arrayBufferBlob: arrayBuffer,
          type: blob?.type,
          size: blob?.size,
          createdAt: new Date(),
          userId,
        });
      } else {
        await updateFileLocal(localFile.id, {
          arrayBufferBlob: arrayBuffer,
          updatedAt: new Date(),
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  return filename;
}

export function convertHexToRGBA(hex: string, opacity = 1): number[] {
  const hx = hex.substr(1, hex.length);
  if (hx.length !== 6) {
    return [];
  }

  const aRgbHex = hx.match(/.{1,2}/g);

  if (!aRgbHex) return [];

  return [parseInt(aRgbHex[0], 16), parseInt(aRgbHex[1], 16), parseInt(aRgbHex[2], 16), opacity];
}

export function getOriginLocation() {
  return window.location.origin;
}

export function colorStringToColorTheme(color: string | undefined, theme: Theme) {
  if (!color || color.indexOf("#") >= 0 || color === "none" || color.indexOf("rgb") >= 0) {
    return color;
  }

  let { palette } = theme as any;
  const path = color.split(".");
  if (!palette[path[0]]) {
    return color;
  }

  if (path.length === 1) {
    return palette[path[0]].main;
  }

  path.forEach((pathName: string) => {
    palette = palette[pathName];
  });

  return palette;
}

export function getContrastColorTextByTheme(color: string | undefined, theme: Theme) {
  if (!color) {
    return null;
  }

  const { palette } = theme as any;
  const path = color.split(".");
  if (!palette[path[0]]) {
    return null;
  }

  return palette[path[0]].contrastText;
}

export function isAvalableFileExtension(fileName: string) {
  const extension = getExtensionFilename(fileName) ?? "---";

  const PERMITED_EXTENSION = [
    "JPEG",
    "PNG",
    "GIF",
    "SVG",
    "TIFF",
    "ICO",
    "DVU",
    "MP3",
    "WAV",
    "OGG",
    "CSV",
    "ZIP",
    "PDF",
    "XML",
    "JSON",
    "TXT",
    "XLSX",
    "DOC",
    "DOCX",
    "PPT",
    "PPTX",
    "JPG",
  ];

  return PERMITED_EXTENSION.includes(extension.toUpperCase());
}

type Formats = {
  default: string;
  thumbnail: string;
  small: string;
  medium: string;
  large: string;
};

export function getStrapiImageFormats(imageObj: MediaInterface) {
  if (!imageObj?.data?.id) {
    return null;
  }

  const baseUrl = publicRuntimeConfig?.publicationsGraphQL?.baseUrl;
  const sizes = ["thumbnail", "small", "medium", "large"];
  const sizesExists: any = { default: baseUrl + imageObj.data.attributes.url };
  sizes.forEach((size) => {
    if (imageObj.data.attributes.formats && size in imageObj.data.attributes.formats) {
      const formats = imageObj.data.attributes.formats as any;
      sizesExists[size] = baseUrl + formats[size].url;
    } else {
      sizesExists[size] = null;
    }
  });

  return sizesExists as Formats;
}

export function getUrlStrapiImage(imageObj: MediaInterface, size: ImageSizes) {
  if (!imageObj?.data?.id) {
    return null;
  }

  const formats = getStrapiImageFormats(imageObj) as any;
  const sizes = ["large", "medium", "small", "thumbnail", "default"];
  const slicedSizes = sizes.slice(sizes.indexOf(size));
  let foundSize;
  try {
    foundSize = slicedSizes.find((sizeName: string) => formats[sizeName] !== null);
  } catch (e) {
    return null;
  }

  if (foundSize) {
    return formats[foundSize];
  }

  return null;
}

export async function getAudioDuration(file: File) {
  const buffer = await file.arrayBuffer();

  const blob = arrayBufferToBlob(buffer);
  const urlBlob = createObjectURL(blob);
  const duration = await getBlobDuration(urlBlob);

  return duration;
}

export async function getAudioDurationFromArrayBuffer(buffer: ArrayBuffer) {
  const blob = arrayBufferToBlob(buffer);
  const urlBlob = createObjectURL(blob);
  const duration = await getBlobDuration(urlBlob);

  return duration;
}

export function prepareSearchString(str: string) {
  return str
    .toString()
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "");
}

export async function prepareUploadPath(
  currentPath: string,
  userId: string,
  chatPath: string | string[] | undefined,
) {
  const accessedPages = await getAccessedPages();
  const urlOrigin = accessedPages[1];
  let url = "";

  if (/^[/]library/.test(urlOrigin)) {
    const blackList = [`/library/${DirectoryNamesNCEnum.TALK}`];
    if ((urlOrigin.match(/[/]/g) || []).length > 1 && !blackList.includes(urlOrigin)) {
      const path = currentPath;
      url = convertPrivateToUsername(path, userId).replace(/[/]library[/]/, "");
    }
  }

  if (/^[/]honeycomb/.test(urlOrigin)) {
    if ((urlOrigin.match(/[/]/g) || []).length > 1) {
      // eslint-disable-next-line no-underscore-dangle
      url = !chatPath ? convertPrivateToUsername(getPrivatePath(), userId) : String(chatPath);
    }
  }

  return url || convertPrivateToUsername(getPrivatePath(), userId);
}

export const reorderItemList = (list: any[], startPosition: number, newPostion: number) => {
  const result = Array.from(list);

  const [removed] = result.splice(startPosition, 1);

  result.splice(newPostion, 0, removed);

  return result;
};

export const isSafari = () => /^((?!chrome|android).)*safari/i.test(window.navigator.userAgent);

export const getImageSize = async (file: File): Promise<{ width: number; height: number }> => {
  const url = URL.createObjectURL(file);

  const img = document.createElement("img");

  const promise = new Promise((resolve, reject) => {
    img.onload = () => {
      const width = img.naturalWidth;
      const height = img.naturalHeight;

      resolve({ width, height });
    };

    img.onerror = reject;
  });

  img.src = url;

  return promise as Promise<{ width: number; height: number }>;
};

export const reduceImageSize = async (
  file: File,
  quality: number,
  toWidth?: number,
  toHeight?: number,
  format = "JPEG",
): Promise<File> => {
  const { width, height } = await getImageSize(file);

  const reducedWidth = toWidth ?? Math.round((quality * width) / 100);
  const reducedHeight = toHeight ?? Math.round((quality * height) / 100);

  return new Promise((resolve) => {
    Resizer.imageFileResizer(
      file,
      reducedWidth,
      reducedHeight,
      format,
      quality,
      0,
      (uri) => {
        // @ts-ignore
        resolve(uri);
      },
      "file",
    );
  });
};

export function generateVirtualStudioRoomHash(roomToken: string, recordingPath: string) {
  const hash = btoa(`${roomToken}-${btoa(recordingPath)}-${shorthash(uuid())}`);
  return hash;
}

export function generateVirtualStudioPublicLink(virtualStudioHash: string, displayName: string) {
  const link = `${window.location.origin}/public/virtual-studio/${virtualStudioHash}/${btoa(
    displayName,
  )}`;
  return link;
}

export function toTimestamp(strDate: string) {
  const datum = Date.parse(strDate);
  return datum / 1000;
}

export function removeProtocolFromURL(url: string) {
  return url.replace(/(http([s]|):\/\/)/, "");
}
