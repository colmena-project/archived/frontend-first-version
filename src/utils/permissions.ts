import { initializeStore } from "@/store/index";
import { PermissionTalkMemberEnum, RoleUserEnum } from "@/enums/*";
import { RoomParticipant } from "@/interfaces/talk";

export function isSubadminProfile() {
  const { role } = initializeStore({}).getState().user.user;
  return role === RoleUserEnum.ADMIN;
}

export function getUserGroup() {
  const { media } = initializeStore({}).getState().user.user;

  return media.name;
}

export const isModerator = (participantsIn: RoomParticipant[], userId: string) => {
  const result = participantsIn.find(
    (item) =>
      item.actorId === userId &&
      (item.participantType === PermissionTalkMemberEnum.OWNER ||
        item.participantType === PermissionTalkMemberEnum.MODERATOR),
  );
  return result;
};
