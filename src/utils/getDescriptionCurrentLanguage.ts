import { getSystemLanguages } from "./utils";

export default function getDescriptionCurrentLanguage(c: any, nextLocale: string) {
  const currentLanguage = getSystemLanguages(c).find((row) => row.abbr === nextLocale);

  return currentLanguage?.language;
}
