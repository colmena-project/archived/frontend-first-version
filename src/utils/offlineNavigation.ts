import { OfflinePagesEnum } from "@/enums/*";
import { getHoneycombUrl } from "@/services/talk/chat";

const currentOfflinePageKey = "currentOfflinePage";
type CurrentOfflinePageData = {
  page: string;
  data: any;
};

export function getCurrentOfflinePage(): CurrentOfflinePageData | false {
  const data = localStorage.getItem(currentOfflinePageKey);
  if (!data) {
    return false;
  }

  return JSON.parse(data);
}

export function setCurrentOfflinePage(data: CurrentOfflinePageData) {
  localStorage.setItem(currentOfflinePageKey, JSON.stringify(data));
}

export function clearCurrentOfflinePage() {
  localStorage.setItem(currentOfflinePageKey, "");
}

export function getFileLink(id: string, connectionStatus: boolean): string {
  if (connectionStatus) {
    clearCurrentOfflinePage();
    return `/file/${id}`;
  }

  setCurrentOfflinePage({
    page: OfflinePagesEnum.FILE,
    data: {
      id,
    },
  });

  return "/offline/file";
}

export function getHoneycombLink(
  token: string,
  displayName: string,
  canDeleteConversation: number,
  connectionStatus: boolean,
): string {
  if (connectionStatus) {
    clearCurrentOfflinePage();
    return getHoneycombUrl(token, displayName, canDeleteConversation.toString());
  }

  setCurrentOfflinePage({
    page: OfflinePagesEnum.HONEYCOMB_CHAT,
    data: {
      token,
      displayName: encodeURI(displayName),
      canDeleteConversation,
    },
  });

  return "/offline/honeycomb";
}

export function getEditAudioLink(id: string, connectionStatus: boolean): string {
  if (connectionStatus) {
    clearCurrentOfflinePage();
    return `/edit-audio/${id}`;
  }

  setCurrentOfflinePage({
    page: OfflinePagesEnum.EDIT_AUDIO,
    data: {
      id,
    },
  });

  return "/offline/edit-audio";
}
