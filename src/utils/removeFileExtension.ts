export const removeFileExtension = (fileName: string) => {
  if (!fileName) return fileName;

  return fileName.replace(/\.[^/.]+$/, "");
};
