/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
export const objectToString = (obj: any) => {
  return Object.entries(obj)
    .map(([key, value]) => `${key}: ${value};`)
    .join("");
};
