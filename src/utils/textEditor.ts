import { ContextMenuOptionEnum, FileExtensionEnum } from "@/enums/*";
import { makeStyles } from "@material-ui/core";
import { convertUsernameToPrivate } from "./directory";

export const buttonsOptions = [
  "|",
  "italic",
  "bold",
  "|",
  "ul",
  "ol",
  "|",
  "fontsize",
  "right",
  "center",
  "left",
  "brush",
  "paragraph",
  "|",
  "table",
  "link",
  "undo",
  "redo",
  "eraser",
  "fullsize",
];

export const removeButtonsOptions = [
  "fullsize",
  "file",
  "link",
  "image",
  "brush",
  "table",
  "font",
  "eraser",
];

export const availableOptions = [
  ContextMenuOptionEnum.DELETE,
  ContextMenuOptionEnum.DOWNLOAD,
  ContextMenuOptionEnum.RENAME,
];

export const objectForStyling = {
  editor: {
    width: "100%",
    height: "100%",
    borderRadius: "10px",
  },
  gridButton: {
    marginBottom: "20px",
    marginTop: "20px",
  },
};

export const useStyles = makeStyles(() => ({
  editorWrapper: {
    width: "100%",
    margin: "auto",
    "& .ProseMirror": {
      minHeight: "70vh",
      border: "2px solid #e5e5e5",
      marginBottom: "20px",
      padding: "15px",
      "&:focus, img": {
        outline: "none",
      },
    },
    "& .ProseMirror .ProseMirror-selectednode": {
      outline: "2px solid #68cef8",
    },
    "& .ProseMirror p.is-editor-empty:first-child::before": {
      float: "left",
      color: "#ced4da",
      pointerEvents: "none",
      height: 0,
    },
  },
}));

export const getRealPathOfTextFile = (filename: string, userId: string, currentPath: string) => {
  const path = `${currentPath}/${String(filename)}.${FileExtensionEnum.MD}`;

  return convertUsernameToPrivate(path, userId);
};
