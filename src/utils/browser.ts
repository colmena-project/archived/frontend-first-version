export const getBrowserName = (userAgent: any) => {
  let browserName = "Unknown";

  switch (true) {
    case userAgent.indexOf("Firefox") !== -1:
      browserName = "Firefox";
      break;
    case userAgent.indexOf("Chrome") !== -1:
      browserName = "Chrome";
      break;
    case userAgent.indexOf("Safari") !== -1:
      browserName = "Safari";
      break;
    case userAgent.indexOf("MSIE") !== -1 || userAgent.indexOf("Trident") !== -1:
      browserName = "Internet Explorer";
      break;
    case userAgent.indexOf("Edge") !== -1:
      browserName = "Microsoft Edge";
      break;
    case userAgent.indexOf("Opera") !== -1 || userAgent.indexOf("OPR") !== -1:
      browserName = "Opera";
      break;
    case userAgent.indexOf("YaBrowser") !== -1:
      browserName = "Yandex Browser";
      break;
    default:
      browserName = "Unknown";
  }

  return browserName;
};

export const getBrowserVersion = (userAgent: any) => {
  let browserVersion = "";

  switch (true) {
    case userAgent.indexOf("Firefox") !== -1:
      browserVersion = userAgent.substring(userAgent.indexOf("Firefox") + 8);
      break;
    case userAgent.indexOf("Chrome") !== -1:
      browserVersion = userAgent.substring(userAgent.indexOf("Chrome") + 7);
      break;
    case userAgent.indexOf("Safari") !== -1:
      browserVersion = userAgent.substring(userAgent.indexOf("Version") + 8);
      break;
    case userAgent.indexOf("MSIE") !== -1:
      browserVersion = userAgent.substring(userAgent.indexOf("MSIE") + 5);
      break;
    case userAgent.indexOf("Trident") !== -1:
      browserVersion = userAgent.substring(userAgent.indexOf("rv") + 3);
      break;
    case userAgent.indexOf("Edge") !== -1:
      browserVersion = userAgent.substring(userAgent.indexOf("Edge") + 5);
      break;
    case userAgent.indexOf("Opera") !== -1 || userAgent.indexOf("OPR") !== -1:
      browserVersion =
        userAgent.substring(userAgent.indexOf("OPR") + 4) ||
        userAgent.substring(userAgent.indexOf("Opera") + 5);
      break;
    case userAgent.indexOf("YaBrowser") !== -1:
      browserVersion = userAgent.substring(userAgent.indexOf("YaBrowser") + 10);
      break;
    default:
      browserVersion = "";
  }

  const versionIndex = browserVersion.indexOf(" ");
  if (versionIndex !== -1) {
    browserVersion = browserVersion.substring(0, versionIndex);
  }

  return browserVersion;
};
