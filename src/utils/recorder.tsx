import { toast } from "@/utils/notifications";
import { SystemTagsInterface } from "@/interfaces/tags";
import { assignTagFile, createAndAssignTagFile, listTags } from "@/services/webdav/tags";
import {
  AudioExportTypesEnum,
  DirectoryNamesNCEnum,
  EnvironmentEnum,
  HoneycombTypesEnum,
} from "../enums";
import { PropsAudioSave, SelectOptionItem } from "../types";
import {
  convertPrivateToUsername,
  convertUsernameToPrivate,
  getHoneycombTokenByUrl,
} from "./directory";
import {
  findGroupFolderByPath,
  getAccessedPages,
  prepareLanguageToNextcloud,
  removeSpecialCharacters,
  setFilenameRecorder,
  setPathRecorder,
} from "./utils";

import {
  updateFile as updateFileLocal,
  getFile as getFileLocal,
  remove as removeLocalFile,
  findByFilename,
} from "@/store/idb/models/files";

import { findTokenChatByPath, verifyDeleteAccessFromUserOnChat } from "@/services/talk/room";

import {
  putFile as putFileOnline,
  getFileId as getFileOnlineId,
  setDataFile,
} from "@/services/webdav/files";

import { createShare } from "@/services/share/share";
import { AxiosResponse } from "axios";

export async function saveFile(
  values: PropsAudioSave,
  userId: string,
  fileTitle: string,
  language: string,
  messages: {
    genericError: string;
    audioSavedSuccessfully: string;
    storageError: string;
  },
  setCurrentPath?: (currentPath: string) => void,
) {
  const { name: title, tags, path, availableOffline } = values;
  try {
    const defaultAudioType = AudioExportTypesEnum.WAV;
    const pathPvtToUsr = convertPrivateToUsername(path, userId);
    const pathUsrToPvt = convertUsernameToPrivate(path, userId);

    const filename = `${pathUsrToPvt}/${title}.${defaultAudioType}`;
    const aliasFilename = `${pathPvtToUsr}/${title}.${defaultAudioType}`;
    const basename = `${pathUsrToPvt}/${removeSpecialCharacters(title)}.${defaultAudioType}`;

    await setPathRecorder(pathPvtToUsr);
    await setFilenameRecorder(title);

    const recording = {
      title,
      tags,
      path,
      filename,
      basename,
      aliasFilename,
      environment: EnvironmentEnum.LOCAL,
    };

    const currentFilename = `${pathUsrToPvt}/${removeSpecialCharacters(
      fileTitle,
    )}.${defaultAudioType}`;

    const { id: audioId } = await findByFilename(userId, currentFilename);

    await updateFileLocal(audioId, recording);
    const localFile = await getFileLocal(audioId);

    try {
      if (navigator.onLine) {
        let tokenChat: string | null | boolean = await findTokenChatByPath(pathUsrToPvt);
        let talkDir = "";
        let filenameWithTalkDir = "";
        if (tokenChat && typeof tokenChat === "string") {
          if (tokenChat !== HoneycombTypesEnum.ONE_TO_ONE) {
            const isGroupFolder = await findGroupFolderByPath(pathUsrToPvt);
            const canDelete = await verifyDeleteAccessFromUserOnChat(tokenChat);
            if (!canDelete && !isGroupFolder) {
              talkDir = `${DirectoryNamesNCEnum.TALK}/`;
            }
          } else {
            const lastPages = await getAccessedPages();
            tokenChat = getHoneycombTokenByUrl(lastPages[1] || lastPages[0]);
          }
        }

        filenameWithTalkDir = `${talkDir}${filename}`;

        await putFileOnline(userId, `${filenameWithTalkDir}`, localFile.arrayBufferBlob);

        const fileId = await getFileOnlineId(userId, `${filenameWithTalkDir}`);

        await setDataFile(
          { customtitle: title, language: prepareLanguageToNextcloud(language) },
          `${filenameWithTalkDir}`,
        );

        await handleTags(tags, fileId);

        if (!availableOffline) {
          await removeLocalFile(audioId, userId);
        } else {
          await updateFileLocal(audioId, {
            filename: filenameWithTalkDir,
            environment: EnvironmentEnum.BOTH,
          });
        }

        await updateFileLocal(audioId, { nextcloudId: fileId });
        await createChatMessageFileNotification(filenameWithTalkDir, tokenChat);

        if (setCurrentPath) {
          setCurrentPath(filenameWithTalkDir);
        }
      }
    } catch (e) {
      console.log(e);
      toast(messages.genericError, "error");
    }
    toast(messages.audioSavedSuccessfully, "success");
  } catch (e) {
    console.log(e);
    toast(messages.storageError, "error");
  }
}

async function handleTags(tags: any, fileId: any) {
  const res = await listTags();
  const optionsTag: SelectOptionItem[] = res
    .filter((_, idx) => idx !== 0)
    .map((item: any | SystemTagsInterface) => ({
      id: item.propstat.prop.id,
      value: String(item.propstat.prop["display-name"]).toLowerCase(),
    }));

  const tagsFoundOnline = optionsTag
    .filter((item) => tags.includes(item.value))
    .map((item) => item.id);

  const tagsOnlineValues = optionsTag.map((item) => item.value);
  const tagsNotFoundOnline = tags.filter((item: any) => !tagsOnlineValues.includes(item));

  const promises: Promise<AxiosResponse<any>>[] = [];
  tagsFoundOnline.forEach((item: number) => {
    promises.push(assignTagFile(Number(fileId), item));
  });

  tagsNotFoundOnline.forEach((item: string) => {
    promises.push(createAndAssignTagFile(Number(fileId), item));
  });

  await Promise.all(promises);
}

async function createChatMessageFileNotification(path: string, token: boolean | string | null) {
  if (!token || typeof token !== "string") return;

  try {
    await createShare(token, path);
    return;
  } catch (e) {
    console.log(e);
  }
}
