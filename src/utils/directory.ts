import { removeFirstSlash, removeCornerSlash, trailingSlash, getPathUrl } from "./utils";

const PRIVATE_PATH = "private";
const PUBLIC_PATH = "public";
const AUDIO_PATH = "private/audios";
const TALK_PATH = "Talk";

export function getPrivatePath(): string {
  return PRIVATE_PATH;
}

export function getPublicPath(): string {
  return PUBLIC_PATH;
}

export function getRootPath(): string {
  return PRIVATE_PATH;
}

export function getAudioPath(): string {
  return AUDIO_PATH;
}

export function getTalkPath(): string {
  return TALK_PATH;
}

export function localPaths(): Array<string> {
  return [getAudioPath()];
}

export function isPanal(path: string): boolean {
  const pathSplited = removeFirstSlash(path).split("/");
  const initialFolder = pathSplited[0];
  const ignorePaths = [PRIVATE_PATH, PUBLIC_PATH];

  return !ignorePaths.includes(initialFolder);
}

export function isLibraryUrl(path: string): boolean {
  const pathSplited = removeFirstSlash(path).split("/");
  let initialFolder = pathSplited[0];

  // Verify whether first route is language
  if (initialFolder.length === 2 && typeof pathSplited[1] === "string") {
    // eslint-disable-next-line prefer-destructuring
    initialFolder = pathSplited[1];
  }

  return initialFolder === "library";
}

export function isHoneycombUrl(path: string): boolean {
  const pathSplited = removeFirstSlash(path).split("/");
  let initialFolder = pathSplited[0];

  // Verify whether first route is language
  if (initialFolder.length === 2 && typeof pathSplited[1] === "string") {
    // eslint-disable-next-line prefer-destructuring
    initialFolder = pathSplited[1];
  }

  return initialFolder === "honeycomb";
}

export function inHoneycomb(path: string): boolean {
  const pathSplited = removeFirstSlash(path).split("/");
  let initialFolder = pathSplited[0];
  if (initialFolder.length === 2 && typeof pathSplited[1] === "string") {
    // eslint-disable-next-line prefer-destructuring
    initialFolder = pathSplited[1];
  }

  return initialFolder === "honeycomb" && pathSplited.length > 1;
}

export function getHoneycombTokenByUrl(url: string | null = null, urlPath = "/"): string | null {
  const basePath = url || urlPath;
  if (!inHoneycomb(basePath)) {
    return null;
  }

  const pathSplited = removeFirstSlash(basePath).split("/");
  let token = pathSplited[1];
  if (token.length === 2 && typeof pathSplited[1] === "string") {
    // eslint-disable-next-line prefer-destructuring
    token = pathSplited[2];
  }

  return token;
}

export function isPrivate(path: string): boolean {
  const pathSplited = removeFirstSlash(path).split("/");
  const initialFolder = pathSplited[0];

  return initialFolder === PRIVATE_PATH;
}

export function hasLocalPath(path: string | undefined | null): boolean {
  return localPaths().includes(removeFirstSlash(path));
}

export function exclusivePaths(): Array<string> {
  return [getAudioPath(), getPrivatePath(), getPublicPath()];
}

export function hasExclusivePath(path: string | undefined | null): boolean {
  return exclusivePaths().includes(removeFirstSlash(path));
}

export function handleDirectoryName(name: string) {
  return name.replace(/[^à-ú0-9\w\-\s]*/g, "");
}

export function handleFileName(name: string) {
  return name.replace(/[^à-ú0-9\w\-_\s]*/g, "");
}

export function getPathName(path: string): string {
  return path.replace(/.+?\/(.*)$/g, "$1");
}

export function getFilename(path: string): string {
  return path.replace(/.*\/(.+?\..+?)$/, "$1");
}

export function substitutePrivateWithUsername(path: string, username: string): string {
  const filePath = getFilePath(path);

  return transformPrivateToUsername(filePath, username);
}

function transformPrivateToUsername(filePath: string, username: string) {
  return filePath.replace("private", username);
}

export function getFilePath(path: string) {
  const pathArray = path.split("/");
  pathArray.pop();

  return pathArray.join("/");
}

export function getPath(path: string): string {
  return path.replace(/(.*)\/.*$/, "$1");
}

export function isRootPath(path: string | undefined | null): boolean {
  return path === "/" || path === "";
}

export function pathIsInFilename(path: string, filename: string | undefined): boolean {
  if (!filename) {
    return false;
  }

  const handledPath = trailingSlash(path).replace(/\/.+?\..*$/, "");
  const handledFilename = trailingSlash(filename).replace(/\/.+?\..*$/, "");

  const regex = new RegExp(`^${removeFirstSlash(handledPath)}.*`, "g");

  return removeFirstSlash(handledFilename).replace(regex, "") === "";
}

export function removeInitialPath(fullPath: string, initialPath: string): string {
  const regex = new RegExp(`^${removeCornerSlash(initialPath)}`, "g");

  return removeCornerSlash(fullPath).replace(regex, "");
}

export function convertPrivateToUsername(path: string, username: string): string {
  const regex = new RegExp(`^${trailingSlash(getPrivatePath())}`);

  return removeCornerSlash(trailingSlash(path).replace(regex, trailingSlash(username)));
}

export function convertUsernameToPrivate(path: string, username: string): string {
  const regex = new RegExp(`^${trailingSlash(username)}`);

  return removeCornerSlash(trailingSlash(path).replace(regex, trailingSlash(getPrivatePath())));
}

export function convertTalkToShareWithMe(path: string, talkFolderName: string): string {
  const regex = new RegExp(`^${trailingSlash(getTalkPath())}`);

  return removeCornerSlash(trailingSlash(path).replace(regex, trailingSlash(talkFolderName)));
}

export function convertSharedWithMeToTalk(path: string, talkFolderName: string): string {
  const regex = new RegExp(`^${trailingSlash(talkFolderName)}`);
  return removeCornerSlash(trailingSlash(path).replace(regex, trailingSlash(getTalkPath())));
}

export function convertRealPathToAliasPath(path: string, username: string, talkFolderName: string) {
  return convertTalkToShareWithMe(convertPrivateToUsername(path, username), talkFolderName);
}

export function convertAliasPathToRealPath(path: string, username: string, talkFolderName: string) {
  return convertSharedWithMeToTalk(convertUsernameToPrivate(path, username), talkFolderName);
}

export function removeExtension(filename: string) {
  return filename.substring(0, filename.lastIndexOf(".")) || filename;
}

export function splitFilename(filename: string) {
  return filename.split("/").reverse()[0];
}

export const splitName = (fullName: string, ref: "firstName" | "lastName") => {
  const arrName = fullName.split(" ");

  if (ref === "firstName") {
    return arrName[0];
  }

  return arrName.slice(1, arrName.length).join(" ");
};

export function isAllowToShowCurrentLibraryPath(urlPath: string | undefined = undefined) {
  let path: string | undefined = urlPath;
  if (path === undefined) {
    path = getPathUrl();
  }

  const currentPath = removeFirstSlash(path);
  const splitedCurrentPath = currentPath.split("/");
  if (splitedCurrentPath.length === 0) {
    return false;
  }

  switch (splitedCurrentPath[0]) {
    case "honeycomb":
      return inHoneycomb(currentPath);
    case "library":
      if (
        typeof splitedCurrentPath[1] === "undefined" ||
        (splitedCurrentPath[1] === TALK_PATH && typeof splitedCurrentPath[2] === "undefined")
      ) {
        return false;
      }

      return true;
    default:
      return false;
  }
}

export function handlePathLocalization(
  userId: string,
  path: string,
  pathExists: boolean,
  urlPath: string,
) {
  const rootPath = getRootPath();
  const realPath = convertUsernameToPrivate(path, userId);
  if (!isAllowToShowCurrentLibraryPath(urlPath) || !pathExists || !realPath || realPath === "/") {
    return convertPrivateToUsername(rootPath, userId);
  }

  return path;
}
