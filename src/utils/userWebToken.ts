import CryptoJS from "crypto-js";
import { DataJitsiInterface } from "@/interfaces/jitsi";

function base64url(source: CryptoJS.lib.WordArray) {
  // Encode in classical base64
  let encodedSource = CryptoJS.enc.Base64.stringify(source);

  // Remove padding equal characters
  encodedSource = encodedSource.replace(/=+$/, "");

  // Replace characters according to base64url specifications
  encodedSource = encodedSource.replace(/\+/g, "-");
  encodedSource = encodedSource.replace(/\//g, "_");

  return encodedSource;
}

export function createUserWebToken(data: DataJitsiInterface) {
  const header = {
    alg: "HS256",
    typ: "JWT",
  };

  const stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
  const encodedHeader = base64url(stringifiedHeader);

  const stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
  const encodedData = base64url(stringifiedData);

  const token = `${encodedHeader}.${encodedData}`;

  const secret = data.context.user.id;

  const signature = CryptoJS.HmacSHA256(token, secret);
  const signatureResult = base64url(signature);

  const signedToken = `${token}.${signatureResult}`;

  return signedToken;
}
