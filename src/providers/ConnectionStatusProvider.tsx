import React, { useState, useEffect, useCallback } from "react";

export const ConnectionStatusContext = React.createContext(true);

type Props = {
  children: React.ReactNode;
  initialStatus?: boolean;
};

const PING_RESOURCE = "/ping.txt";
const TIMEOUT_TIME_MS = 3000;
const onlinePollingInterval = 5000;

const timeout = (time: number, promise: Promise<any>) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error("Request timed out."));
    }, time);
    promise.then(resolve, reject);
  });

const monitor: any = async () => {
  const controller = new AbortController();
  const { signal } = controller;
  // If the browser has no network connection return offline
  if (!navigator.onLine) {
    return false;
  }

  try {
    await timeout(
      TIMEOUT_TIME_MS,
      fetch(PING_RESOURCE, {
        method: "GET",
        signal,
      }),
    );

    return true;
  } catch (error) {
    // Error Log
    console.error(error);

    // This can be because of request timed out
    // so we abort the request for any case
    controller.abort();
    return false;
  }
};

const ConnectionStatusProvider = ({ children, initialStatus = true }: Props) => {
  const [onlineStatus, setOnlineStatus] = useState<boolean>(initialStatus);
  const [execution, setExecution] = useState<any>();

  useEffect(() => {
    (async () => {
      if (!navigator.onLine) {
        await checkStatus();
      }
    })();

    window.addEventListener("offline", async () => {
      await checkStatus();
    });

    return () => {
      window.removeEventListener("offline", async () => {
        setOnlineStatus(false);
      });
    };

    /* eslint-disable-next-line react-hooks/exhaustive-deps */
  }, []);

  const checkStatus = useCallback(async () => {
    if (execution) {
      clearInterval(execution);
    }

    const online = await monitor();
    setOnlineStatus(online);
    if (!online) {
      setExecution(setTimeout(async () => checkStatus(), onlinePollingInterval));
    }
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
  }, []);

  return (
    <ConnectionStatusContext.Provider value={onlineStatus}>
      {children}
    </ConnectionStatusContext.Provider>
  );
};

export default ConnectionStatusProvider;
