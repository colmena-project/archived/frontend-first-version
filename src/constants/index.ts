import { enUS, es, fr, ptBR, arDZ, uk, ru } from "date-fns/locale";

export default {
  APP_NAME: "Colmena",
  APP_DESCRIPTION: "Open collaborative tools to create and share",
  LOCALES: { en: "en", es: "es", fr: "fr", ar: "ar", pt_BR: "pt_BR", sw: "sw", uk: "uk", ru: "ru" },
  LOCALES_NEXTCLOUD: {
    en: "en",
    es: "es",
    fr: "fr",
    ar: "ar",
    pt_BR: "pt_BR",
    sw: "en",
    uk: "uk",
    ru: "ru",
  },
  LOCALES_DATE_FNS: { en: enUS, es, fr, ar: arDZ, pt_BR: ptBR, sw: enUS, uk, ru },
  DEFAULT_LANGUAGE: "en",
  TOKEN_EXPIRE_SECONDS: 60 * 60,
  MAX_CHUNK_FILE: 1 * 1000000, // 1mb
  DEBOUNCE_TIME_FILE_AUTO_SAVE: 1000,
  HEIGHT_REDUCED_HEADER_JITSI: 78,
};
